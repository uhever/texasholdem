--[[
    登陆SCENE
]]

local SCENE = {}

local loginScene = nil
local sceneWidget = nil

-- SDK模块(loginSceneSdk.lua)
local SDKinstance = nil

-- 服务器列表数据
local serverListData = nil

-- 登陆用户名
local loginUsername = nil
local loginPassword = nil

local phoneEditBox = nil
local authcodeEditBox = nil
local sendAuthCodeBtn = nil

function SCENE.create( params )

    loginScene = zc.createMyScene(SCENE.prepare)

    return loginScene
end


function SCENE.prepare(  )
    sceneWidget = zc.prepareMyScene(loginScene)
    
    SCENE.init(  )

    SCENE.createUI(  )
    
    SCENE.initProgress(  )

end


-- 初始化数据
function SCENE.init(  )

    -- 获取IP地址
    zc._getIPAddress()
    zc.getLocationInfo()
    
    zc.currentMusic = ""

    -- 随机数种子
    math.newrandomseed()

    -- 加载自定义变量(开发者工具)
    zc.loadUserCFG(  )

    -- 加载常驻内存的贴图
    zc.loadMemoryTexture()

    -- 准备下载目录
    SCENE.prepareDownloadPath()

    -- 初始化SDK模块
    SDKinstance = zc.require("src/app/scene/loginSceneSdk")
    SDKinstance.init(SCENE)

    if(device.platform == "android") then
        SDKinstance._regisiterCommonSDKcallbackAndroid()
    else
        SDKinstance._regisiterCommonSDKcallbackIOS()
    end

end

-- 网络请求、热更新等
function SCENE.initProgress(  )

    -- 1.检查是否需要加载附加文件
    SCENE._initSubzip(function()

        -- 2.检查热更新
        SCENE._initHotUpdate(function()

            SCENE.initDone()

        end)


    end)

end


function SCENE.initDone()


    if LOGIN_MODE == kLoginModeNormal then -- 普通

        -- 获取服务器列表，填充第一个服务器
        SCENE._getServerList(function()
            -- 初始化完成
            SCENE.createNormalLoginUI()
            
        end)


    elseif LOGIN_MODE == kLoginModeZCSDK then -- ZCSDK


        if zc.SDKHadInit == false then
            zc.SDKHadInit = true
            SDKinstance.autoLoginBySDK()
            
        else
            zc.loginSDK()
        end

    elseif LOGIN_MODE == kLoginModeInApp then -- InApp

        -- zc.loginSDKInApp()

    elseif LOGIN_MODE == kLoginModeLiveSDK then -- 直播SDK
        SCENE.createLiveSDKLoginUI( )

    end

    -- 独立包统计
    if(zcAdTrack) then
        zcAdTrack() -- 所有渠道都需要统计
    end


end


function SCENE.prepareDownloadPath()
    local saveURI =  "download" .. VERSION_CACHE_ID .. "/tmp.zip"
    os.prepareDir(saveURI)
end


--[[
    附加文件下载的处理
]]
function SCENE._initSubzip(next)

    -- 需要加载(部分版本需要先下载额外资源包，再检查游戏内更新)
    if(IS_EXTRARESOURCES) then
        SCENE.checkZipUpdate(function()
            next()
        end)
    -- 无需加载
    else
        next()
    end

end

-- 检查初始化zip包是否下载成功 http://lcdn.mhdl.zhanchenggame.com/packageResources/manhuangSub1.0.zip
function SCENE.checkZipUpdate(callback)
    local isZipSuccess = cc.UserDefault:getInstance():getStringForKey("zipStatus" .. VERSION_CACHE_ID)
    if(isZipSuccess and isZipSuccess == "ok") then
        callback()
    else
        -- 打开界面
        zc.createPop("src/app/pop/loaderPop", 1)
        local loaderPopInstance = zc.getPop("src/app/pop/loaderPop")


        local zipURI = CDN_URL .. "cdn/packageResources/manhuangSub" .. VERSION_CACHE_ID .. ".zip"
        print("subzipURI:", zipURI)

        -- 安卓java下载器
        if(device.platform == "android") then
            -- 注册下载完成回调
            local zipSuccessParams = {
                "zipSuccess",
                function()
                    cc.UserDefault:getInstance():setStringForKey("zipStatus" .. VERSION_CACHE_ID, "ok")
                    cc.UserDefault:getInstance():flush()

                    if(loaderPopInstance) then
                        loaderPopInstance.clean()
                    end
                    loaderPopInstance = nil

                    callback()
                end
            }
            callJava("RegisterCallBackByName", zipSuccessParams, "(Ljava/lang/String;I)V")

            -- 注册进度回调
            local downloadExtraResourcesCallback = {
                "downloadExtraResourcesCallback",
                function(res)
                    local _tmp = string.split(res, ",")
                    local eventName   = _tmp[1]
                    local eventValue  = _tmp[2]

                    local callbackMsg = nil

                    if eventName == "downloadStart" then
                        callbackMsg = "tips_downloadmanager_start"

                    elseif eventName == "downloadLoading" then
                        if(eventValue) then
                            loaderPopInstance.setPercent(eventValue, zc.lang("loginScene_UpdtaResources", "正在为您更新资源包..."))
                            callbackMsg = "tips_downloadmanager_loading"
                        end

                    elseif eventName == "downloadSuccess" then
                        callbackMsg = "tips_downloadmanager_finish"
                        loaderPopInstance.setPercent("99", zc.lang("loginScene_EndUpdata", "更新完成,正在为您加载资源..."))

                    elseif eventName == "downloadFail" then
                        callbackMsg = "tips_downloadmanager_fail"

                        local data = {}
                        data.btnNum = 2

                        data.text = callbackMsg

                        data.okText = "tips_downloadmanager_retry_all"
                        data.okCallback = function()
                            callJava("downloadExtraResourcesRetry", {}, "()V")
                        end

                        data.cancelText = "tips_downloadmanager_retry"
                        data.cancelCallback = function()
                            callJava("downloadRetry", {}, "()V")
                        end
                        data.touchClose = false

                        zc.confirm(data)

                    elseif eventName == "unzipProccess" then
                        callbackMsg = "tips_downloadmanager_in_unzip"
                        loaderPopInstance.setPercent("99", zc.lang("loginScene_UnzipResources", "正在为您解压资源..."))


                    elseif eventName == "unzipFail" then
                        callbackMsg = "tips_downloadmanager_fail_unzip"

                        local data = {}
                        data.btnNum = 2

                        data.text = callbackMsg

                        data.okText = "tips_downloadmanager_retry_all"
                        data.okCallback = function()
                            callJava("downloadExtraResourcesRetry", {}, "()V")
                        end

                        data.cancelText = "tips_downloadmanager_reunzip"
                        data.cancelCallback = function()
                            callJava("unzipRetry", {}, "()V")
                        end
                        data.touchClose = false

                        zc.confirm(data)

                    end

                    if callbackMsg then
                        loaderPopInstance.setTitle(callbackMsg)
                    end

                    -- loaderPopInstance.setUpdateInfo("Downloading Extra Resources")


                end
            }
            callJava("RegisterCallBackByName", downloadExtraResourcesCallback, "(Ljava/lang/String;I)V")

            -- 开始下载
            local paramsArray = {
                zipURI, -- http://cdn.bdwsw.hanjiangsanguo.com/packageResources/bdwsw1.1.3.zip
                "download" .. VERSION_CACHE_ID, -- download1.1.3
            }
            local sig = "(Ljava/lang/String;Ljava/lang/String;)V"
            callJava("downloadExtraResources", paramsArray, sig)
        end
    end
end

--[[
    游戏热更新的处理
]]
function SCENE._initHotUpdate(next)
    print("UPDATE_OPEN",UPDATE_OPEN)
    if(UPDATE_OPEN == false) then
        print("更新系统已经关闭！")
        next()
        return
    end

    --[[
        两分钟内检查过且FILE_DOWNLOAD_URL已经有值了，可以不重复检查更新，直接进入游戏
    ]]
    local needCheck = true

    local updateCheckTime = cc.UserDefault:getInstance():getStringForKey("updateCheckTime")
    if(updateCheckTime ~= nil and updateCheckTime ~= "" ) then
        updateCheckTime = tonumber(updateCheckTime)
        if(os.time() - updateCheckTime <= 3 and FILE_DOWNLOAD_URL ~= nil and FILE_DOWNLOAD_URL ~= "") then
            needCheck = false
        end
    end

    if(needCheck == false) then
        print("无需重新检查更新")
        next()
        return
    end

    -- 记录下当前检查时间
    cc.UserDefault:getInstance():setStringForKey("updateCheckTime", tostring(os.time()))
    cc.UserDefault:getInstance():flush()


    -- 初始化本地版本
    local _local_version = cc.UserDefault:getInstance():getStringForKey("local_version" .. VERSION_CACHE_ID)
    if(_local_version and _local_version ~= "") then
        LOCAL_VERSION = _local_version
    end

    local successCallback = function(res)


        SERVER_VERSION = tostring(res.data.server_version)

        print("isreview=",res.isreview)
        -- 后台控制客户端审核状态
        if res.isreview then
            APP_REVIEW = res.isreview
        end

        -- http://cdn.xxx.com/20150101/
        FILE_DOWNLOAD_URL = CDN_URL .. "cdn/poker/update/" .. SERVER_VERSION .. "/"
        print("FILE_DOWNLOAD_URL", FILE_DOWNLOAD_URL)


        -- VERSION_NAME = res.data.server_versionname
        if(CLIENT_VERSION ~= SERVER_VERSION) then --服务器代码与本地不一致
            print("服务器代码与本地原始代码不一致-NO")
            if(LOCAL_VERSION ~= SERVER_VERSION) then --需要从服务器更新
                print("本地代码是旧的，需要更新-NO")

                local _allUpdate = res.data.all_update
                -- 显示文件大小
                local allSize = _allUpdate.allsize
                print("更新文件总共" .. _allUpdate.allsize)
                print("更新文件除语言" .. _allUpdate.excludesize)

                -- 资源和代码更新顺序重排
                local sortList = {}
                for i,v in ipairs(_allUpdate.filelist) do
        
                    if string.find(v, ".lua") then
                        table.insert(sortList, v)
                    else
                        table.insert(sortList, 1, v)
                    end
                end


                _allUpdate.filelist = sortList

                -- dump(_allUpdate.filelist, "_allUpdate.filelist")

                -- 整理要更新的文件
                local loadList = {}
                for i = 1, #_allUpdate.filelist do

                    table.insert(loadList, {
                        path = _allUpdate.filelist[i] .. ".bak", -- res/Image/main/xxx.png
                        url  = FILE_DOWNLOAD_URL .. _allUpdate.filelist[i] -- http://cdn.xxx.com/2015/res/Image/main/xxx.png
                    })
                    
                end

                
                zc.fileLoader.addFiles(zc.lang("loginScene_VersionUpdate", "版本更新中"), loadList, true, allSize, res.data.updateinfo)
                zc.fileLoader.load(
                    function()

                        print(socket.gettime(), "文件移动开始")
                        for i,v in ipairs(_allUpdate.filelist) do
                            local pathBak = device.writablePath .. "download" .. VERSION_CACHE_ID .. "/" .. v .. ".bak"
                            local pathBase = device.writablePath .. "download" .. VERSION_CACHE_ID .. "/" .. v

                            local ret, err = os.rename(pathBak, pathBase)
                            if err then
                                zc.confirm({
                                    btnNum         = 1,
                                    text           = zc.lang("loginScene_UpdataFileOverlayFailure", "更新文件覆盖失败，请稍后重试！"),
                                    okCallback     = function() SCENE._initHotUpdate(next) end,
                                    godMode  = true,
                                })
                                return
                            end
                        end

                        print(socket.gettime(), "文件移动结束")

                        -- 保存版本记录
                        LOCAL_VERSION = tostring(SERVER_VERSION)
                        cc.UserDefault:getInstance():setStringForKey("local_version" .. VERSION_CACHE_ID, tostring(SERVER_VERSION))
                        cc.UserDefault:getInstance():flush()

                        io.writefile(device.writablePath .. "download" .. VERSION_CACHE_ID .. "/latest_version.txt", tostring(SERVER_VERSION))

                        -- 重启
                        SCENE.restartGame()
                    end,
                    nil,
                    1 -- 进度条UI
                )

            else--本地已经是最新版本
                print("本地代码已更新到最新版-YES")

                next()
            end
        else
            print("服务器代码与本地原始代码完全一致-YES")

            next()
        end


    end
    local failCallback = function()

        zc.confirm({
            btnNum         = 1,
            text           = zc.lang("loginScene_UpdataSeverConnectionFailure", "更新服务器 连接失败，请稍后重试！"),
            okCallback     = function() SCENE._initHotUpdate(next) end,
            godMode  = true,
        })

    end

    zc.http(UPDATE_API, {
            client_version = CLIENT_VERSION,
            local_version = LOCAL_VERSION,
            base_local_version = BASE_LOCAL_VERSION,
            channel = CHANNELID,
            lang =  LANGUAGE,
            clientid = UPDATE_ID,
            debug = UPDATE_DEBUG
        },
        successCallback,
        failCallback)

end




function SCENE.createUI(  )

    local bgImgSrc = "res/Image/login/login_bg.jpg"
    

    local bgImg = zc.createImageView({
        src = bgImgSrc,
        pos = cc.p(zc.cx, zc.cy),
        fitScreen = true,
        parent = sceneWidget,
        })

end




-- 输入用户名后获取服务器列表
function SCENE._getServerList( callback )

    local successCallback = function(data)
        if(tonumber(data.status) == 1) then
            -- 从UC里获取的token，待会儿登录游戏服务器时需要
            serverListData = data

            if callback then
                callback()
            end

        end
    end

    local failCallback = function()
        zc.confirm({
            btnNum         = 1,
            text           = zc.lang("loginScene_GetSeverListFailure", "获取最新服务器列表失败，请稍后重试！"),
            okCallback     = function() SCENE._getServerList(callback) end,
            godMode  = true,
        })
    end

    zc.http(UCAPI .. "c=user&m=get_server_list", {
            channel = CHANNELID,
            mac = zc.device.mac,
            idfa = zc.device.idfa,
            android_id = zc.device.android_id,
            u = loginUsername,
            p = loginPassword,
        }, successCallback, failCallback)

end



-- 登录用户中心逻辑
function SCENE.loginUC(callback)

    local successCallback = function(data)
        local resultStatus = tonumber(data.status)
        if(resultStatus == 1) then

            -- 从UC里获取的token，待会儿登录游戏服务器时需要
            serverListData = data

            if callback then
                callback()
            end

        else
            zc.getErrorMsg(data.status)
        end
    end

    -- 请求UC登录
    zc.http(UCAPI .. "c=user&m=login", {
            u = loginUsername,
            p = loginPassword,
            mac = zc.device.mac,
            idfa = zc.device.idfa,
            android_id = zc.device.android_id,
            channel = CHANNELID,
            language = zc.language,
        },
        successCallback)

end


--[[
    SDK登录用户中心逻辑
    SDKloginAPI:  c=userdk&m=login
    SDKloginParams:  { ppuid = 11111, token = "xxxxxx"}
    sdkValue: 从java端获得的原始数据信息( { sdkUid = 1111, sdkSid = "xxxxxx" ...})
]]
function SCENE.loginUCbySDK(_loginAPI, _loginParams, sdkValue)

    dump(sdkValue)

    local SDKloginAPI = _loginAPI
    local SDKloginParams = _loginParams

    -- 补充设备信息、渠道信息等
    SDKloginParams.mac = zc.device.mac
    SDKloginParams.idfa = zc.device.idfa
    SDKloginParams.android_id = zc.device.android_id
    SDKloginParams.channel = CHANNELID
    SDKloginParams.language = zc.language

    local successCallback = function(data)

        local resultStatus = tonumber(data.status)
        if(resultStatus == 1) then

            --[[
                saveSdkInfo
                登录验证后（用户中心登录完成），保存各平台信息到 APP_SDK_INFO
            ]]

            APP_SDK_INFO = SDKinstance.buildSDKLoginUCSuccessInfo(data, sdkValue)

            if(APP_SDK_INFO) then

                serverListData = data

                -- 线上强行设置服务器ID
                SERVERID = serverListData.recommend

                -- 这里显示服务器列表
                SCENE.loginGameServer()

            end

            -- sdk新用户的注册统计
            if data.exist and tonumber(data.exist) ~= 1 then

                zc.sendDataEvent({
                    eventname = "Register",
                    userid    = tostring(data.uid)
                })

            end

        else
            zc.hideLoading()

            zc.confirm({
                btnNum         = 1,
                text           = zc.lang("loginScene_LoginUserCenterFailure", "登录用户中心失败，请稍后重试！") .. resultStatus,
                okCallback     = function() SDKinstance.buildLoginUcParams(sdkValue) end,
                godMode  = true,
            })

        end

    end

    -- 请求UC登录
    zc.http(UCAPI .. SDKloginAPI, SDKloginParams, successCallback)

end



-- 选择服务器后user_login
function SCENE.loginGameServer()

    for i,v in ipairs(serverListData.serverlist) do
        if tonumber(v.id) == tonumber(SERVERID) then
            SERVERADDR = v.ip
            SERVERPORTS = v.port
            SERVERTCPPORTS = v.gate_port
            break
        end
    end


    local loginAPI = "m=user_login"

    -- gm登陆
    if(GM_LOGIN_UID and GM_LOGIN_UID ~= "") then
        loginAPI = "m=gm_login&uid=" .. GM_LOGIN_UID
    end

    local successCallback = function(data)
        if(tonumber(data.status) == 1) then

            -- 这里主要获得token和VIP
            dump(data)
            -- 游戏服务器登录成功
            -- 更新token

            GAMETOKEN = data.token
            GAMEUID = data.uid

            cc.UserDefault:getInstance():setStringForKey("loginUsername", loginUsername)
            cc.UserDefault:getInstance():setStringForKey("loginPassword", loginPassword)

            -- 更新静态信息表
            SCENE.updateStaticTable()

        else
            zc.getErrorMsg(data.status)
            
        end
    end

    -- 服务器登录
    zc.loadUrl(loginAPI, {
            token = serverListData.token,
            mac = zc.device.mac,
            idfa = zc.device.idfa,
            channel = CHANNELID,
            serverid = SERVERID,
        },
        successCallback)

end

function SCENE.joinGame()

    zc.require("src/app/data/PlayerData")
    zc.require("src/app/data/GameManager")

    local loginDoneCallback = function()

        zc.switchScene("src/app/scene/mainScene")

        -- 登录统计
        zc.sendDataEvent({
            eventname = "Login",
            userid    = tostring(PlayerData.base.roleid)
        })

    end

    local loginErrorCallback = function(status)
        zc.getErrorMsg(status)
    end
    -- 获取玩家总信息 全局变量 PlayerData 如果没有选角色，会直接进入 roleScene
    PlayerData:init(loginDoneCallback, loginErrorCallback)

end



--[[
    更新静态表开始
]]
function SCENE.updateStaticTable()

    -- 初始化本地预置的静态表
    local localStableData = cc.FileUtils:getInstance():getStringFromFile("db/" .. zc.language .. "/" .. "localStableData.dat")
    if(localStableData and localStableData ~= "") then
        local temp = string.split(localStableData, "=")
        LOCAL_STABLE_VERSION = temp[1]
        LOCAL_STABLE_LIST = temp[2]
    end

    -- 检查本地静态table的版本
    local tableVersion = nil
    local tableList = nil

    -- 本地保存的静态表版本号
    local savedTableVersion = CCUserDefault:getInstance():getStringForKey("staticVersion" .. VERSION_CACHE_ID .. zc.language)
    if (savedTableVersion and savedTableVersion ~= "") then
        tableVersion = savedTableVersion
    else
        tableVersion = LOCAL_STABLE_VERSION
    end

    -- 本地保存的静态表列表
    local savedTableList = CCUserDefault:getInstance():getStringForKey("staticList" .. VERSION_CACHE_ID .. zc.language)
    print("savedTableList", savedTableList)
    if (savedTableList and savedTableList ~= "") then -- 默认用预制的静态表
        tableList = json.decode(savedTableList)
    else
        tableList = json.decode(LOCAL_STABLE_LIST)
    end

    if(tableVersion == nil or tableList == nil) then
        print("检查本地静态表版本异常，清理缓存数据!")
        -- 清理数据
        SCENE.rmAllStaticTable()

        -- 初始化数据
        tableVersion = ""
        tableList = {}
    end

    print("当前静态表版本：" .. tableVersion)
    print("当前已经缓存的静态表：")
    print(json.encode(tableList))

    -- 获取到静态表，保存到本地
    local successCallback = function(data)
        if(data.status == 1) then

            -- (服务器最新静态表版本号)本次更新的最终版本号
            local serverTableVersion = data.version

            --[[
                整理要更新的文件
            ]]
            local _getTableFileName = function(path) -- "/tableversion/1/2015120401/item_config.txt"
                local _arr = string.split(path, "/")
                local _arr2 = string.split(_arr[#_arr], ".")
                return _arr2[1] -- item_config
            end

            local loadList = {}
            for i = 1, #data.list do

                local _tableName = _getTableFileName(data.list[i])

                table.insert(loadList, {
                        path = "res/db/" .. zc.language .. "/" .. _tableName .. ".json", -- res/db/xxx.png
                        url  = STABLE_URL .. "cdn" .. data.list[i] -- http://cdn.xxx.com/2015/res/Image/main/xxx.png
                    })

                -- 将新增的表保存，用于维护静态表的列表缓存
                if(table.keyof(tableList, _tableName) == nil) then
                    print("新表GET：" .. _tableName)
                    table.insert(tableList, _tableName)
                end
            end
            zc.fileLoader.addFiles(zc.lang("loginScene_DataUpdata", "数据更新中"), loadList, true)
            zc.fileLoader.load(
                function() -- success

                    print("恭喜，静态表更新完成")

                    -- fileLoader下载完成后会自动关闭遮罩，此处要重新打开遮罩

                    -- 将新增的表保存，用于维护静态表的列表缓存
                    for i = 1, #loadList do

                        local _tableName = _getTableFileName(loadList[i].path)
                        if(table.keyof(tableList, _tableName) == nil) then
                            print("新表GET：" .. _tableName)
                            table.insert(tableList, _tableName)
                        end
                    end

                    -- 版本号写入新版本
                    tableVersion = serverTableVersion
                    CCUserDefault:getInstance():setStringForKey("staticVersion" .. VERSION_CACHE_ID .. zc.language, tableVersion)

                    -- tableList转化为json存入本地缓存
                    local listJson = json.encode(tableList)
                    CCUserDefault:getInstance():setStringForKey("staticList" .. VERSION_CACHE_ID .. zc.language, listJson)

                    
                    -- 写入当前静态信息到localStableData.dat
                    SCENE._writeLocalStableData(tableVersion, tableList)

                    -- 清理文件路径缓存
                    cc.FileUtils:getInstance():purgeCachedEntries()

                    print("更新完成，开始加载静态表")
                    SCENE._loadStaticTable(tableVersion, tableList)

                end,
                function() -- fail

                    zc.confirm({
                        btnNum         = 1,
                        text           = zc.lang("loginScene_StaticDataSeverFailure", "静态数据服务器 加载失败，请稍后重试！"),
                        okCallback     = function() SCENE.showLastLogin() end,
                        godMode = true,
                    })

                end,
                1, -- 正常显示进度UI
                STABLE_CRYPTO -- 是否加密静态数据
            )

        elseif(data.status == -2) then
            print("2.版本一致，进入下一环节(加载静态表)")
            SCENE._loadStaticTable(tableVersion, tableList)
        else
            zc.confirm({
                btnNum         = 1,
                text           = zc.lang("loginScene_StaticDataSeverReturnabnormal", "静态数据服务器 返回异常，请稍后重试！"),
                okCallback     = function() SCENE.showLastLogin() end,
                godMode = true,
            })
        end
    end
    local failCallback = function()
        zc.confirm({
            btnNum         = 1,
            text           = zc.lang("loginScene_StaticDataSeverConnectionFailure", "静态数据服务器 连接失败，请稍后重试！"),
            okCallback     = function() SCENE.showLastLogin() end,
            godMode  = true,
        })

    end

    zc.http(STABLE_URL .. "index.php?c=tableversion&m=index", {
            version = tableVersion,
            channel = CHANNELID,
            language = zc.language,
        },
        successCallback,
        failCallback
    )

end



function SCENE._writeLocalStableData( newVersion, newData )
    local saveURI =  "download" .. VERSION_CACHE_ID .. "/"

    local filename = "res/db/" .. zc.language .. "/" .. "localStableData.dat"

    saveURI = saveURI .. filename
    -- 写文件
    os.prepareDir(saveURI)

    local writeData = newVersion .. "=" .. json.encode(newData)

    io.writefile(device.writablePath .. saveURI, writeData)
end



function SCENE._getStaticTable(tableName)


    local jsonData = nil
    local data = cc.FileUtils:getInstance():getStringFromFile("db/" .. zc.language .. "/" .. tableName .. ".json")
    if(data and data ~= "") then
        -- if(STABLE_CRYPTO) then
        --     local decodeResponse = function()
        --         local deStr = decryptXXTEA(data, APP_XXTEA_KEY)
        --         jsonData =  json.decode(deStr)
        --     end
        --     local errorHandler = function()
        --         print("静态表" .. tableName .. "加载失败！")
        --     end
        --     xpcall(decodeResponse, errorHandler)
        -- else
            jsonData =  json.decode(data)
        -- end
    end

    return jsonData

end

-- 加载本地静态表 到全局变量 DB_STATICTABLE
function SCENE._loadStaticTable(tableVersion, tableList)

    DB_STATICTABLE = {}

    -- 发新版本时，修改db/localStableData.json需要是数据
    print("tableVersion:", tableVersion)
    print("json:", json.encode(tableList))

    local errorNums = nil
    if tableList and type(tableList) == "table" then
        errorNums = 0
        --do
        for index = 1, #tableList do
            local tableName = tableList[index]
            print("当前加载的静态表：" .. tableName)

            local tmp = SCENE._getStaticTable(tableName)
            if(tmp and type(tmp) == "table") then
                DB_STATICTABLE[tableName] = tmp
            else
                errorNums = errorNums + 1
            end
        end

    end
    if(errorNums == nil or errorNums > 0) then
        -- 清除本地缓存的所有静态表，并重新尝试加载

        local okCallback = function()
            SCENE.rmAllStaticTable()
            SCENE.updateStaticTable()
        end
        zc.confirm({
            btnNum         = 1,
            text           = zc.lang("loginScene_NeedAgainLoadStaticData", "需要重新加载静态数据。"),
            okCallback     = okCallback,
            godMode  = true,
        })

        return
    end

    SCENE.joinGame()

end

function SCENE.rmAllStaticTable()
    os.rmdir(device.writablePath .. "download" .. VERSION_CACHE_ID .. "/res/db/" .. zc.language .. "/")

    for i,v in ipairs(zc.langCfg) do
        CCUserDefault:getInstance():setStringForKey("staticList" .. VERSION_CACHE_ID .. v["key"], "")
        CCUserDefault:getInstance():setStringForKey("staticVersion" .. VERSION_CACHE_ID .. v["key"], "")
    end
end


-- 重度清除
function SCENE.cleanAllCache()

    -- 删除文件
    os.rmdir(device.writablePath .. "download" .. VERSION_CACHE_ID .. "/")

    -- 删除缓存的版本等信息
    LOCAL_VERSION = CLIENT_VERSION .. ""
    local backLocalVersion = LOCAL_VERSION
    if(BASE_LOCAL_VERSION) then
        backLocalVersion = BASE_LOCAL_VERSION
    end

    cc.UserDefault:getInstance():setStringForKey("local_version" .. VERSION_CACHE_ID, backLocalVersion)

    cc.UserDefault:getInstance():setStringForKey("updateCheckTime", tostring(os.time() - 3000) )

    for i,v in ipairs(zc.langCfg) do

        cc.UserDefault:getInstance():setStringForKey("staticList" .. VERSION_CACHE_ID .. v["key"], "")
        cc.UserDefault:getInstance():setStringForKey("staticVersion" .. VERSION_CACHE_ID .. v["key"], "")
        
    end

    print("数据已经清除完成！")
    SCENE.restartGame()
end


function SCENE.showLastLogin()
    zc.switchScene("src/app/scene/loginScene")
end

--[[
    更新静态表结束
]]


-- 普通登陆(测试UI)
function SCENE.createNormalLoginUI(  )

    -- 获取本地保存的账号密码
    loginUsername = cc.UserDefault:getInstance():getStringForKey("loginUsername")
    loginPassword = cc.UserDefault:getInstance():getStringForKey("loginPassword")

    -- 登陆Layout
    local normalLoginLayout = zc.createLayout({
        -- pos = cc.p(zc.cx, zc.cy),
        parent = sceneWidget,
        })

    local loginUsernameEdit = zc.createEditBox({
        text = loginUsername,
        bgSrc = "res/Image/login/login_input_img.png",
        size = cc.size(490, 70),
        pos = cc.p(zc.cx, zc.cy),
        placeHolder = zc.lang("loginScene_InputUserName", "输入用户名"),
        fontSize = 32,
        parent = normalLoginLayout
        })

    local loginPasswordEdit = zc.createEditBox({
        text = loginPassword,
        bgSrc = "res/Image/login/login_input_img.png",
        size = cc.size(350, 70),
        pos = cc.p(zc.cx - 70, zc.cy - 80),
        placeHolder = zc.lang("loginScene_InputPassword", "输入密码"),
        fontSize = 32,
        inputFlag = cc.EDITBOX_INPUT_FLAG_PASSWORD,
        parent = normalLoginLayout
        })


    local registBtn = zc.createButton({
        text = zc.lang("loginScene_Register", "注册"),
        -- fontColor = cc.c3b(220, 199, 159),
        normal = "res/Image/login/login_small_btn_n.png",
        pressed = "res/Image/login/login_small_btn_p.png",
        -- scale9 = true,
        -- size = cc.size(130, 60),
        pos = cc.p(zc.cx + 180, zc.cy - 80),
        parent = normalLoginLayout,
        listener = function ( sender, eventType )
            if eventType == ccui.TouchEventType.ended then
                zc.createPop("src/app/pop/registAccountPop")
            end
        end
        })


    for i,v in ipairs(serverListData.serverlist) do
        -- 进入按钮
        local enterBtnT1 = zc.createButton({
            -- fontColor = cc.c3b(220, 199, 159),
            normal = "res/Image/login/login_small_btn_n.png",
            pressed = "res/Image/login/login_small_btn_p.png",
            size = cc.size(240, 83),
            text = v.name,
            pos = cc.p(zc.cx + 120 - (i%2) * 240, zc.cy - 70 -math.ceil(i / 2) * 100),
            tag = tonumber(v.id),
            parent = normalLoginLayout,
            listener = function ( sender, eventType )
                if eventType == ccui.TouchEventType.ended then

                    SERVERID = sender:getTag()

                    loginUsername = loginUsernameEdit:getText()
                    loginPassword = loginPasswordEdit:getText()

                    -- 第一次进入游戏，没有登录，需要去UC拉取uctoken再到单服登录
                    if serverListData.token == "" then
                        SCENE.loginUC(function (  )
                            SCENE.loginGameServer(  )
                        end)
                    else
                        SCENE.loginGameServer()
                    end

                end
            end
            })
    end

end


--[[
    直播SDK界面
]]
function SCENE.createLiveSDKLoginUI( )

    local liveSDKLoginLayout = zc.createLayout({
        parent = sceneWidget,
        })


    zc.createLabel({
        text = "CN +86",
        ap = cc.p(0, 0.5),
        pos = cc.p(zc.cx - 250, zc.cy + 35),
        fontSize = 24,
        color = cc.c3b(223, 196, 143),
        parent = liveSDKLoginLayout,
        })

    zc.createImageView({
        src = "res/Image/login/login_arrow_img.png",
        pos = cc.p(zc.cx - 140, zc.cy + 35),
        parent = liveSDKLoginLayout,
        })

    phoneEditBox = zc.createEditBox({
        text = nil,
        bgSrc = "res/Image/common/img_empty.png",
        size = cc.size(350, 60),
        pos = cc.p(zc.cx + 60, zc.cy + 35),
        placeHolder = zc.lang("loginScene_PhoneNum", "手机号码"),
        fontSize = 24,
        fontColor = cc.c3b(223, 196, 143),
        placeHolderFontColor = cc.c3b(189, 155, 118),
        parent = liveSDKLoginLayout
        })

    zc.createImageView({
        src = "res/Image/login/login_input_seg.png",
        pos = cc.p(zc.cx, zc.cy + 10),
        parent = liveSDKLoginLayout,
        })

    zc.createLabel({
        text = zc.lang("loginScene_VerificationCode", "验证码"),
        ap = cc.p(0, 0.5),
        pos = cc.p(zc.cx - 250, zc.cy - 60),
        fontSize = 24,
        color = cc.c3b(223, 196, 143),
        parent = liveSDKLoginLayout,
        })


    authcodeEditBox = zc.createEditBox({
        text = nil,
        bgSrc = "res/Image/common/img_empty.png",
        size = cc.size(250, 60),
        pos = cc.p(zc.cx + 10, zc.cy - 60),
        placeHolder = zc.lang("loginScene_TipInfo", "6-12个字符"),
        fontSize = 24,
        fontColor = cc.c3b(223, 196, 143),
        placeHolderFontColor = cc.c3b(189, 155, 118),
        parent = liveSDKLoginLayout
        })

    zc.createImageView({
        src = "res/Image/login/login_input_seg.png",
        pos = cc.p(zc.cx, zc.cy - 90),
        parent = liveSDKLoginLayout,
        })

    sendAuthCodeBtn = zc.createButton({
        text = zc.lang("loginScene_Send", "发送"),
        fontColor = cc.c3b(81, 73, 76),
        normal = "res/Image/login/login_small_btn_n.png",
        pressed = "res/Image/login/login_small_btn_p.png",
        -- scale9 = true,
        -- size = cc.size(130, 60),
        pos = cc.p(zc.cx + 200, zc.cy - 50),
        parent = liveSDKLoginLayout,
        listener = SCENE.sendAuthCodeCallback
        })

    zc.createButton({
        name = "phone",
        text = zc.lang("loginScene_Login", "登 录"),
        fontColor = cc.c3b(81, 73, 76),
        normal = "res/Image/login/login_big_btn_n.png",
        pressed = "res/Image/login/login_big_btn_p.png",
        -- scale9 = true,
        -- size = cc.size(490, 80),
        pos = cc.p(zc.cx, zc.cy - 170),
        parent = liveSDKLoginLayout,
        listener = SCENE.sdkLoginCallback,
        })
    
    

    if APP_REVIEW then
        zc.createLabel({
            text = zc.lang("loginScene_UserProtocol", "用户协议"),
            link = true,
            shadow = true,
            -- ap = cc.p(0, 0),
            pos = cc.p(zc.cx, zc.cy - 260),
            fontSize = 30,
            listener = function ( sender, eventType )
                if eventType == ccui.TouchEventType.ended then
                    cc.Application:getInstance():openURL("http://jingyutexas.lofter.com/post/1ec60566_ed77fb4") 
                end
            end,
            parent = liveSDKLoginLayout,
            })

        zc.createImageView({
            name = "visitor",
            src = "res/Image/login/login_img_visitor.png",
            pos = cc.p(zc.cx, zc.cy - 400),
            parent = liveSDKLoginLayout,
            listener = SCENE.sdkLoginCallback,
            })

    else
        zc.createImageView({
            name = "wechat",
            src = "res/Image/login/login_img_wechat.png",
            pos = cc.p(zc.cx, zc.cy - 400),
            parent = liveSDKLoginLayout,
            listener = SCENE.sdkLoginCallback,
            })
    end
    

end


function SCENE.sendAuthCodeCallback( sender, eventType )
    if eventType == ccui.TouchEventType.ended then
        print("发送验证码", phoneEditBox:getText())

        if phoneEditBox:getText() == "" then
            zc.tipInfo(zc.lang("loginScene_InputPhoneNum", "请输入手机号"))
            return
        end
        
        zc.sendAuthcode( {phoneNumber = phoneEditBox:getText()} )
    end
end

-- 验证码回调
function SCENE.authCodeResult( status )
    if tolua.isnull(sendAuthCodeBtn) then
        return
    end

    if status == 1 then
        sendAuthCodeBtn:setTouchEnabled(false)
        SCENE.startAuthCodeCD( 60 )
    else
        SCENE.resetSendAuthCodeBtn()
        zc.tipInfo(zc.lang("loginScene_SendVerificaFailure", "验证码发送失败，请稍后重试"))
    end
end

function SCENE.startAuthCodeCD( time )
    zc.updateCDSimple("sendAuthCodeCD", time, function ( delta )

        if tolua.isnull(sendAuthCodeBtn) then
            zc.cleanCDSimple("sendAuthCodeCD")
            return
        end
        
        if delta >= 0 then
            sendAuthCodeBtn:setTitleText(delta)
        else
            SCENE.resetSendAuthCodeBtn()
        end

    end)
end


-- 重置验证码Label
function SCENE.resetSendAuthCodeBtn(  )
    zc.cleanCDSimple("sendAuthCodeCD")
    sendAuthCodeBtn:setTouchEnabled(true)
    sendAuthCodeBtn:setTitleText(zc.lang("loginScene_Send", "发送"))
end


function SCENE.sdkLoginCallback( sender, eventType )
    if eventType == ccui.TouchEventType.ended then

        local btnName = sender:getName()
        
        if btnName == "visitor" then -- 游客
            zc.showLoading()

            if APP_REVIEW then

                zc.loginSDKLive({
                    method = 2, 
                    phoneNumber = tostring(math.random(18680993331, 18680993335)), 
                    authcode = "111111"
                    })
            else
                zc.loginSDKLive({method = 1})    
            end

      
        elseif btnName == "phone" then -- 手机
            if phoneEditBox:getText() == "" then
                zc.tipInfo(zc.lang("loginScene_InputPhoneNumPlease", "请输入手机号"))
                return
            end

            if authcodeEditBox:getText() == "" then
                zc.tipInfo(zc.lang("loginScene_InputVerificationPlease", "请输入验证码"))
                return
            end
            zc.showLoading()
            zc.loginSDKLive({method = 2, phoneNumber = phoneEditBox:getText(), authcode = authcodeEditBox:getText()})
        elseif btnName == "wechat" then -- 微信
            zc.loginSDKLive({method = 3})
        end


        
    end
end


-- 重启游戏
function SCENE.restartGame()
    zc.switchScene("src/app/scene/restartScene")
end


function SCENE.clean(  )

    zc.cleanCDSimple("sendAuthCodeCD")

    if sceneWidget then
        sceneWidget:removeFromParent()
        sceneWidget = nil
    end

    -- SDK模块(loginSceneSdk.lua)
    SDKinstance = nil

    -- 服务器列表数据
    serverListData = nil

    -- 登陆用户名
    loginUsername = nil
    loginPassword = nil

end

return SCENE