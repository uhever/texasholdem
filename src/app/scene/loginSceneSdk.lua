--[[
    SDK独立模块
]]

local SDK = {}
local LoginScene = nil

function SDK.init(instance)
    LoginScene = instance
end

function SDK.clean()
    LoginScene = nil
end

function SDK._regisiterCommonSDKcallbackIOS()

    -- 公共 微信 需要调用后端加奖励
    local shareResultCallback = function(data)
        if(data and data.status and tonumber(data.status) == 1) then
            zc.tipInfo(zc.lang("loginSceneSdk_ShareSuccess", "分享成功"))
        else
            zc.tipInfo(zc.lang("loginSceneSdk_ShareFailure", "分享失败"))
        end 
    end
    callOc("registerCallBackByName", { shareResult = shareResultCallback })

    -- 登录结果回调
    local loginResultCallback = function(data)

        zc.hideLoading()

        if(data and data.status and tonumber(data.status) == 1) then

            dump(data, "loginResultCallback")

            SDK.buildLoginUcParams({ 
                sdkUid = data.sdkUid or "", 
                sdkSid = data.sdkSid or "" , 
                sdkAuthkey = data.sdkAuthkey or "",
                appid = data.appid or "",
                name = data.name or "",
                icon = data.icon or "",
                sex = data.sex or "",
                })

        else
            zc.confirm({text = zc.lang("loginSceneSdk_LoginFailure", "登录失败，请重新登录！")})
        end
    end
    callOc("registerCallBackByName", { loginResult = loginResultCallback })

    -- 在平台里点了退出
    local logoutOffCallback = function(data)
        if(APP_SDK_INFO == nil) then
            print("登录界面退出")
        elseif(APP_SDK_INFO ~= nil and (zc.nowScene == "src/app/scene/loginScene")) then

            print("在上次登录界面退出(登录完UC后没有登录服务器)")
            LoginScene.cleanSDKloginInfo()
        else
            print("在游戏中退出")
            if(zc.videoPlaying == true) then
                callOc("stopVideo", {})
                zc.logoutProcess()
                zc.isReconnection = false
            else
                zc.logoutProcess()
                zc.isReconnection = false
            end
        end
    end
    callOc("registerCallBackByName", { logoutOffResult = logoutOffCallback })

    -- 刷新页面（支付完成、账号改变等等）
    local payResultCallback = function(data)

        if tonumber(data.status) == 1 then
            if(PlayerData ~= nil and PlayerData.base ~= nil) then -- 未登录（极少情况触发，例如PP）
                zc.commonPayCallback(true)
            end
        else
            zc.commonPayCallback(false)    
        end
        
    end
    callOc("registerCallBackByName", { payResult = payResultCallback })



    -- 从平台获取地理位置
    local locationResultCallback = function(data)
        local statusCode = tonumber(data.status)

        if statusCode == 1 then
            -- zc.tipInfo("获取地理位置成功")
            dump(data, "获取地理位置成功")
            
            LOCATE_INFO = data

            -- 同步
            zc._syncLocationInfo()
            
        elseif statusCode == -1 then -- 用户拒绝
            -- zc.tipInfo("用户拒绝")
        elseif statusCode == -2 then -- 无网络
            -- zc.tipInfo("无网络")
        elseif statusCode == -3 then -- 地理位置不可用
            -- zc.tipInfo("地理位置不可用")
        else -- 其他错误
            -- zc.tipInfo("获取地理位置失败")
        end

    end
    callOc("registerCallBackByName", { locationResult = locationResultCallback })


    -- 验证码回调
    local authCodeResultCallback = function(data)

        dump(data, "authCodeResultCallback")
        if (data and data.status and tonumber(data.status) == 1) then
            -- 验证码发送成功
            zc.getScene("src/app/scene/loginScene").authCodeResult(1)
        else
            zc.getScene("src/app/scene/loginScene").authCodeResult(0)
        end
    end
    callOc("registerCallBackByName", { authCodeResult = authCodeResultCallback })


    -- 网络状态更改
    --[[
        NotReachable = 0,
        ReachableViaWiFi = 1,
        ReachableViaWWAN = 2
    ]]
    local networkStatusChangeCallback = function(data)

        -- dump(data, "networkStatusChangeCallback")
        if (data and data.status and tonumber(data.status) == 1) then
            
            
        end
    end
    callOc("registerCallBackByName", { networkStatusChange = networkStatusChangeCallback })
    callOc("sdkGetNetworkStatus", {})

    -- 电池电量
    local batteryChangeCallback = function(data)

        -- dump(data, "batteryChangeCallback")
        if (data and data.status and tonumber(data.status) == 1) then
            
            
        end
    end
    callOc("registerCallBackByName", { batteryChange = batteryChangeCallback })
    callOc("sdkGetBatteryLevel", {})

    -- 蜂窝网络信号
    local signalStrengthChangeCallback = function(data)

        -- dump(data, "signalStrengthChangeCallback")
        if (data and data.status and tonumber(data.status) == 1) then
            
            
        end
    end
    callOc("registerCallBackByName", { signalStrengthChange = signalStrengthChangeCallback })
    callOc("sdkGetSignalStrengthLevel", {})

    -- 录音回调
    --[[
        -1 : 中断
        0 : 失败
        1 : 成功
    ]]
    local recorderResultCallback = function(data)

        dump(data, "recorderResultCallback")
        if (data and data.status and tonumber(data.status) == 1) then

            
        end
    end
    callOc("registerCallBackByName", { recorderResult = recorderResultCallback })

    -- 通讯录
    local contactResultCallback = function(data)

        dump(data, "contactResultCallback")
        if (data and data.status and tonumber(data.status) == 1) then
            local contact = json.decode(data.contact)
            dump(contact, "contact")
            
        end
    end
    callOc("registerCallBackByName", { contactResult = contactResultCallback })

    -- 获取图片
    local fetchImageResultCallback = function(data)
        dump(data, "fetchImageResultCallback")
        if (data and data.status and tonumber(data.status) == 1) then
            zc.uploadImage({
                iconPath = data.iconPath,
                type = tonumber(data.type),
                })
        else
            zc.tipInfo(zc.lang("loginSceneSdk_GetPictureFailure", "获取图片失败！"))
        end
    end
    callOc("registerCallBackByName", { fetchImageResult = fetchImageResultCallback })


    -- 上传回调
    local uploadImageResultCallback = function(data)
        dump(data, "uploadImageResultCallback")
        if (data and data.status and tonumber(data.status) == 1) then

            zc._resolveUploadImage({
                icon_small = data.icon_small,
                type = tonumber(data.type),
                })
        else
            zc.tipInfo(zc.lang("loginSceneSdk_UploadPictureFailure", "头像上传失败！"))
        end
    end
    callOc("registerCallBackByName", { uploadImageResult = uploadImageResultCallback })


    -- 关闭客服中心
    local closeServiceCenterCallback = function(data)
        zc.resumeMusic()
    end
    callOc("registerCallBackByName", { closeServiceCenter = closeServiceCenterCallback })

end



function SDK._regisiterCommonSDKcallbackAndroid()


    -- 通用：增加登录成功回调
    local loginParams = {
        "loginResult",
        function(jsonData) -- json
            print("loginResult = "..jsonData)
            local data = json.decode(jsonData)

            zc.hideLoading()

            if tonumber(data.status) == 1 then
                SDK.buildLoginUcParams({ 
                    sdkUid = data.sdkUid or "", 
                    sdkSid = data.sdkSid or "" , 
                    sdkAuthkey = data.sdkAuthkey or "",
                    appid = data.appid or "",
                    name = data.name or "",
                    icon = data.icon or "",
                    sex = data.sex or "",
                    })
            else
                zc.confirm({text = zc.lang("loginSceneSdk_LoginFailure", "登录失败，请重新登录！")})
            end
            
        end
    }
    callJava("RegisterCallBackByName", loginParams, "(Ljava/lang/String;I)V")



    -- 通用：支付回调
    local payResultParams = {
        "payResult",
        function(javaValue)
            print("payResult = "..javaValue)
            local data = json.decode(javaValue)
            dump(data, "payResult")

            if tonumber(data.status) == 1 then
                if(PlayerData ~= nil and PlayerData.base ~= nil) then -- 未登录（极少情况触发，例如PP）
                    zc.commonPayCallback(true)
                end
            else
                zc.commonPayCallback(false)    
            end
            
        end
    }
    callJava("RegisterCallBackByName", payResultParams, "(Ljava/lang/String;I)V")


    -- 获取地理位置
    local locationResultParams = {
        "locationResult",
        function(javaValue)
            print("javaValue = " .. javaValue)
            local list = string.split(javaValue, ",")
            dump(list, "locationResult")

            if tonumber(list[1]) == 61 or tonumber(list[1]) == 161 then -- GPS定位/网络定位

                LOCATE_INFO = {}
                LOCATE_INFO["latitude"] = list[2] -- 纬度
                LOCATE_INFO["longitude"] = list[3] -- 经度
                LOCATE_INFO["addressStr"] = list[4] -- 位置描述
                LOCATE_INFO["countryCode"] = list[5] -- 国家码
                LOCATE_INFO["country"] = list[6] -- 国家
                LOCATE_INFO["province"] = list[7] -- 省
                LOCATE_INFO["cityCode"] = list[8] -- 城市码
                LOCATE_INFO["city"] = list[9] -- 城市
                LOCATE_INFO["district"] = list[10] -- 区
                LOCATE_INFO["street"] = list[11] -- 街道/路
                LOCATE_INFO["streetNumber"] = list[12] -- 街道号

                -- 同步
                zc._syncLocationInfo()


                if tonumber(list[1]) == 61 then
                    -- zc.tipInfo("GPS获取地理位置成功")
                else
                    -- zc.tipInfo("网络获取地理位置成功")    
                end
                
            else -- 其他错误
                -- zc.tipInfo("获取地理位置失败")
            end

        end
    }
    callJava("RegisterCallBackByName", locationResultParams, "(Ljava/lang/String;I)V")


    -- 验证码发送成功
    local autoCodeParams = {
        "authCodeResult",
        function(javaValue) -- 固定格式：  ddddddddddddddddd,100000
            print("javaValue = "..javaValue)

            local list = string.split(javaValue, ",")
            dump(list, "authCodeResult")

            if tonumber(list[1]) == 1 then
                -- 验证码发送成功
                zc.getPop("src/app/pop/loginPhonePop").authCodeResult(1)
            else
                zc.getPop("src/app/pop/loginPhonePop").authCodeResult(0)
                
            end

        end
    }
    callJava("RegisterCallBackByName", autoCodeParams, "(Ljava/lang/String;I)V")


    -- 网络状态更改
    --[[
        wifi,
        2g,
        3g,
        4g,
        wap,
        unknown,
        disconnect
    ]]
    local networkStatusChangeParams = {
        "networkStatusChange",
        function(javaValue)
            print("networkStatusChange = "..javaValue)
            local list = string.split(javaValue, ",")
            dump(list, "networkStatusChangeParams")

        end
    }
    callJava("RegisterCallBackByName", networkStatusChangeParams, "(Ljava/lang/String;I)V")
    callJava("sdkGetNetworkStatus", { }, "()V")


    -- 电池电量
    local batteryChangeParams = {
        "batteryChange",
        function(javaValue)
            print("batteryChange = "..javaValue)
            local list = string.split(javaValue, ",")
            -- dump(list, "batteryChangeParams")

        end
    }
    callJava("RegisterCallBackByName", batteryChangeParams, "(Ljava/lang/String;I)V")
    callJava("sdkGetBatteryLevel", { }, "()V")


    -- 蜂窝网络信号
    -- Get the GSM Signal Strength, valid values are (0-31, 99) as defined in TS 27.007 8.5
    local signalStrengthChangeParams = {
        "signalStrengthChange",
        function(javaValue)
            print("signalStrengthChange = "..javaValue)
            local list = string.split(javaValue, ",")
            -- dump(list, "signalStrengthChangeParams")

        end
    }
    callJava("RegisterCallBackByName", signalStrengthChangeParams, "(Ljava/lang/String;I)V")
    callJava("sdkGetSignalStrengthLevel", { }, "()V")


    local recorderResultParams = {
        "recorderResult",
        function(javaValue)
            print("recorderResult = "..javaValue)
            local list = string.split(javaValue, ",")
            -- dump(list, "signalStrengthChangeParams")

        end
    }
    callJava("RegisterCallBackByName", recorderResultParams, "(Ljava/lang/String;I)V")


    -- 通讯录
    local contactResultParams = {
        "contactResult",
        function(javaValue)
            print("contactResult = "..javaValue)
            local data = json.decode(javaValue)
            dump(data, "contactResult")
            -- local list = string.split(javaValue, ",")
            -- dump(list, "contactResultParams")

        end
    }
    callJava("RegisterCallBackByName", contactResultParams, "(Ljava/lang/String;I)V")


    -- 获取图片
    local fetchImageResultParams = {
        "fetchImageResult",
        function(javaValue)
            print("fetchImageResult = "..javaValue)
            local data = json.decode(javaValue)
            dump(data, "fetchImageResult")

            if tonumber(data.status) == 1 then
                zc.uploadImage({
                    iconPath = data.iconPath,
                    type = tonumber(data.type),
                    })
            else
                zc.tipInfo(zc.lang("loginSceneSdk_GetPictureFailure", "获取图片失败！"))
            end

        end
    }
    callJava("RegisterCallBackByName", fetchImageResultParams, "(Ljava/lang/String;I)V")


    -- 上传图片
    local uploadImageResultParams = {
        "uploadImageResult",
        function(javaValue)
            print("uploadImageResult = "..javaValue)
            local data = json.decode(javaValue)
            dump(data, "uploadImageResult")

            if tonumber(data.status) == 1 then
                zc._resolveUploadImage({
                    icon_small = data.icon_small,
                    type = tonumber(data.type),
                    })
            else
                zc.tipInfo(zc.lang("loginSceneSdk_UploadPictureFailure", "头像上传失败！"))
            end

        end
    }
    callJava("RegisterCallBackByName", uploadImageResultParams, "(Ljava/lang/String;I)V")


    -- 分享结果
    local shareResultParams = {
        "shareResult",
        function(javaValue)
            print("javaValue = " .. javaValue)
            local list = string.split(javaValue, ",")
            dump(list, "shareResult")

            if list[1] == "1" then
                -- zc.tipInfo("分享成功")
            else
                -- zc.tipInfo("分享失败")
            end
        end
    }
    callJava("RegisterCallBackByName", shareResultParams, "(Ljava/lang/String;I)V")


end



function SDK.autoLoginBySDK()

    zc.showLoading()

    zc.loginSDK()

end


--[[

    【接入SDK第一步】登录UC

    构建SDK玩家登录到UC的"接口"和"参数"等信息，然后由LoginScene去登录
    sdkValue: 从java端获得的原始数据信息( { sdkUid = 1111, sdkSid = "xxxxxx" ...})
]]
function SDK.buildLoginUcParams(sdkValue)

    local SDKloginAPI = "" -- "c=user&m=login"
    local SDKloginParams = {}

    if(APP_PLATFORM == "ios-zc" or APP_PLATFORM == "android-zc") then

        SDKloginAPI = "c=userzc&m=login"
        SDKloginParams.session = sdkValue.sdkSid
        SDKloginParams.uId = sdkValue.sdkUid
        SDKloginParams.authkey = sdkValue.sdkAuthkey
        SDKloginParams.appid = sdkValue.appid
    end

    if(SDKloginAPI == "") then

        zc.tipInfo("错误：未配置登录UC的参数[buildLoginUcParams]")
        return

    end


    zc.hideLoading()
    
    -- 通知loginScene去登录拉
    LoginScene.loginUCbySDK(SDKloginAPI, SDKloginParams, sdkValue)
end

--[[
    【接入SDK第二步】登录完UC后，保存用户SDK信息

    在UC登录完成后，需要根据不同的渠道，购买用户SDK信息（APP_SDK_INFO）
    data: 登录UC时服务器返回的数据信息
    sdkValue: 从java端获得的原始数据信息( { sdkUid = 1111, sdkSid = "xxxxxx" ...})

]]
function SDK.buildSDKLoginUCSuccessInfo(data, sdkValue)


    --[[
        saveSdkInfo
        登录验证后（用户中心登录完成），保存各平台信息到 APP_SDK_INFO
    ]]
    local saveSdkInfo = nil

    if(APP_PLATFORM == "ios-zc" or APP_PLATFORM == "android-zc") then
        saveSdkInfo = {
            uid = data.uid,
            sdkUid = sdkValue.sdkUid,
            sdkSid = sdkValue.sdkSid,
            sdkAuthkey = sdkValue.sdkAuthkey,
            appid = sdkValue.appid,
            name = sdkValue.name,
            icon = sdkValue.icon,
            sex = sdkValue.sex,

        }
    end


    if(saveSdkInfo == nil) then

        zc.tipInfo(zc.lang("loginSceneSdk_ErrorTip", "错误：未配置登录UC成功后的参数[buildSDKLoginUCSuccessInfo]"))
        return nil

    end

    return saveSdkInfo

end



--[[
  支付第一步：创建预定订单（所有平台都一致）
  orderParams: 配置参数
]]
function SDK.createPrepayOrderAndPay(orderParams)
    local orderAPI = ""

    if(APP_PLATFORM == "ios-zc" or APP_PLATFORM == "android-zc") then
        orderAPI = "c=payzc&m=create_prepay_order"
    end

    if(orderAPI ~= "") then
        zc.http(
            -- http://pay.bdwsw.zhanchenggame.com/index.php?c=pay360&m=create_pay_order&token=GameTOKEN
            PAYAPI .. "index.php?" .. orderAPI .. "&token=" .. GAMETOKEN,
            {
                id = orderParams.id, -- 1,2,3,4,5,6
                check_type_str = 2, -- 验证类型：1账号密码 2token
                serverid = SERVERID, -- 服务器编号
                type = orderParams.type -- 0.普通 1.月卡 2.物资月卡
            },
            function(data)
                dump(data)
                local resultStatus = tonumber(data.status)
                if(resultStatus == 1) then

                    -- 保存当前的订单信息，以备待会儿检查(common里zc.lastPayCheck())

                    zc.createLastPayOrder({
                        productId = data.product_id, -- 商品Id
                        productName = data.product_name, -- 商品名称
                        amount = data.amount, -- 商品价格
                        type = orderParams.type, -- 0.普通 1.月卡 2.物资月卡
                        orderId = data.app_order_id, -- 预订单id
                        uid = PlayerData.base.uid,
                    })
                    dump(zc.lastPayOrder, "zc.lastPayOrder==>")

                    zc.showPayTip()

                    SDK.launchSDKpay(data, orderParams)
                else
                    zc.tipInfo(zc.lang("loginSceneSdk_OrderFailure", "订单创建失败，请联系客服处理"))
                end
            end
        )
    else
        print("错误：创建订单接口没有设置")
    end
end

--[[
    调用SDK的支付
]]
function SDK.launchSDKpay(orderData, orderParams)

    -- 安全的获取名字
    local nicknameStr = string.urlencode(PlayerData.base.nickname)

    --[[
        安卓
    ]]
    if(device.platform == "android") then

        -- zcsdk
        if(APP_PLATFORM == "android-zc") then

            local params = {
                productId = orderData.product_id, -- 商品ID
                orderId = orderData.app_order_id,   -- 商户订单id
                amount = orderData.amount, -- 金额
                productName = orderData.product_name, -- 商品名
                callbackUrl = PAYAPI .. "payzc.php", -- 回调地址
                payType = orderParams.payType -- 充值方式
            }
            -- google play支付的API特殊处理
            callJava("doSdkPay", { json.encode(params) }, "(Ljava/lang/String;)V")

        end

    end

    --[[
        IOS
    ]]
    if(device.platform ~= "android") then

        -- IOS zcsdk & 其他渠道
        local params = {
            productId = orderData.product_id, -- 商品ID
            orderId = orderData.app_order_id,   -- 商户订单id
            amount = orderData.amount, -- 金额
            productName = orderData.product_name, -- 商品名
            callbackUrl = PAYAPI .. "payzc.php", -- 回调地址
            payType = orderParams.payType -- 充值方式
        }

        callOc("doSdkPay", params)

    end
end

return SDK