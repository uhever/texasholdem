--[[
	主场景
]]

local SCENE = {}

local mainScene = nil
local sceneWidget = nil

-- local bottomBar = nil

-- local bottomCfg = {
-- 	{name = "record", src = "bottom_btn_record.png", pressed = "bottom_btn_record_pressed.png", posY = 8},-- posY为纵坐标偏移值
-- 	{name = "play", src = "bottom_btn_play.png", pressed = "bottom_btn_play_pressed.png", posY = 2},
-- 	{name = "mine", src = "bottom_btn_mine.png", pressed = "bottom_btn_mine_pressed.png", posY = 8},
-- }


function SCENE.create( params )

    mainScene = zc.createMyScene(SCENE.prepare)


    return mainScene
end


function SCENE.prepare(  )
	sceneWidget = zc.prepareMyScene(mainScene)

	SCENE.init(  )

	SCENE.createUI()
end


function SCENE.init(  )

	zc.playMusic("dz_bgm", true)
 	
end


function SCENE.createUI(  )
	local mainBg = zc.createImageView({
		src = "res/Image/main/main_bg.jpg",
		pos = cc.p(zc.cx, zc.cy),
		parent = sceneWidget,
		})

	zc.createPop("src/app/pop/enterMainPop")


	-- SCENE.bottomUI(  )
end



-- -- 下部UI
-- function SCENE.bottomUI(  )
-- 	bottomBar = zc.createImageView({
-- 		src = "res/Image/main/bottom_bar.png",
-- 		ap = cc.p(0.5, 0),
-- 		pos = cc.p(zc.cx, 0),
-- 		parent = zc.getTouchLayer(),
-- 		zOrder = zc.Z_ORDER_TITLE,
-- 		})

-- 	local selectedIndex = bottomBar.selectedIndex or 2
-- 	bottomBar.selectedIndex = selectedIndex
	

-- 	for i,v in ipairs(bottomCfg) do

-- 		local touchLayout = zc.createLayout({
-- 			tag = i,
-- 			name = v.name,
-- 			size = cc.size(170, 120),
-- 			ap = cc.p(0.5, 0.5),
-- 			pos = cc.p(bottomBar:getContentSize().width / 2 + 200 * (i-2), bottomBar:getContentSize().height / 2 - v.posY),
-- 			parent = bottomBar,
-- 			listener = SCENE.onBottomCallback
-- 			})

-- 		local iconImg = zc.createImageView({
-- 			name = "iconImg",
-- 			src = "res/Image/main/" .. (i == selectedIndex and v.pressed or v.src),
-- 			pos = cc.p(touchLayout:getContentSize().width / 2, touchLayout:getContentSize().height / 2),
-- 			parent = touchLayout,
-- 			})

-- 	end
	
-- end



-- function SCENE.onChangeTab( nowIndex, newIndex )
-- 	bottomBar:getChildByTag(nowIndex):getChildByName("iconImg"):loadTexture("res/Image/main/" .. bottomCfg[nowIndex].src)
-- 	bottomBar:getChildByTag(newIndex):getChildByName("iconImg"):loadTexture("res/Image/main/" .. bottomCfg[newIndex].pressed)

-- 	bottomBar.selectedIndex = newIndex
-- end


-- function SCENE.onBottomCallback( sender, eventType )
-- 	if eventType == ccui.TouchEventType.ended then

-- 		local name = sender:getName()
-- 		local tag = sender:getTag()
-- 		local nowIndex = bottomBar.selectedIndex

-- 		-- 点的是同一个
-- 		if nowIndex == tag then
-- 			return
-- 		end

-- 		if name == "record" then

-- 			local successCallback = function(data)
--                 if(tonumber(data.status) == 1) then
--                 	zc.closeAllPops()
-- 	            	zc.createPop("src/app/pop/recordPop", data) 
-- 	            	SCENE.onChangeTab(nowIndex, tag)
  
--                 else
--                     zc.getErrorMsg(data.status)
--                 end
--             end
--             zc.loadUrl("m=texas_holdem_achieve", {
--                 }, successCallback)

			
-- 		elseif name == "play" then
-- 			zc.closeAllPops()
-- 			zc.createPop("src/app/pop/enterMainPop")
-- 			SCENE.onChangeTab(nowIndex, tag)
			

-- 		elseif name == "mine" then
-- 			zc.closeAllPops()
-- 			zc.createPop("src/app/pop/minePop")
-- 			SCENE.onChangeTab(nowIndex, tag)

-- 		end

-- 	end
-- end




function SCENE.clean(  )


	if sceneWidget then
		sceneWidget:removeFromParent()
		sceneWidget = nil
	end

	-- bottomBar = nil
	
end

return SCENE