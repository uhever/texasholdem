--[[
    重启游戏，专门为重启做的一个scene
]]

local SCENE = {}

local restartScene = nil
local sceneWidget = nil

function SCENE.create( params )

    restartScene = zc.createMyScene(SCENE.prepare)

    return restartScene
end


function SCENE.prepare(  )

    sceneWidget = zc.prepareMyScene(restartScene)

    local waitLabel = zc.createLabel({
            text = "Please Wait",
            color = display.COLOR_WHITE,
            pos = cc.p(zc.cx, zc.cy),
        })
    sceneWidget:addChild(waitLabel)

    -- 清理全局变量
    DB_STATICTABLE = nil
    LOCAL_STABLE_VERSION = "0"
    LOCAL_STABLE_LIST = "[]"

    for i = 1, #zc.loadedFiles do
        local k = zc.loadedFiles[i]
        -- print("清理文件" .. k)
        if(package.loaded[k]) then
            package.loaded[k] = nil
        end
    end

    -- 清除常驻内存的贴图
    zc.freeMemoryTexture()

    -- 清理json
    ccs.GUIReader:destroyInstance()

    -- 清理文件路径缓存
    cc.FileUtils:getInstance():purgeCachedEntries()

    -- 清理所有注册的通知
    zc.removeEventListenerQueue()

    -- 随机数种子
    math.newrandomseed()

    -- 重新加载initScene实现重启
    zc.require("src/initScene")
      

end


function SCENE.clean(  )
    restartScene = nil
end


return SCENE