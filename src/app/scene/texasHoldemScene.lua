--[[
	德州扑克场次选择
]]

local SCENE = {}

local texasholdemScene = nil
local sceneWidget = nil

local bgImg = nil
local btnTab = {}
local delayFun = nil
-- 开始游戏
function SCENE.startGame(params)
	zc.switchScene("src/app/scene/gameScene", {
		groundPath = "src/app/game/texasholdem/PlayGround", 
		data = params
		})
end


function SCENE.create( params )
	if  zc.currentMusic ~= "mainbgm" then
		zc.playMusic("mainbgm", true)
	end

    texasholdemSceneScene = zc.createMyScene(SCENE.prepare)

    return texasholdemSceneScene
end


function SCENE.prepare(  )
	sceneWidget = zc.prepareMyScene(texasholdemSceneScene)

    SCENE.init(  )

end


function SCENE.init(  )

	SCENE.createUI()
end

function SCENE.getPlayerNum( )
	local function successCallback(data)

		if data.status == 1 then
			for k,v in pairs(data.list) do
				if btnTab and (not tolua.isnull(btnTab[v.id]:getChildByTag(33)))  then
					btnTab[v.id]:getChildByTag(33):setString(zc.getWan(v.player_num,4))
				end
			end
		else
			zc.getErrorMsg(data.status)
		end
	end
	
	zc.loadUrl("m=texas_holdem_info", {
		}, successCallback)

end


function SCENE.createUI(  )

	bgImg = zc.createImageView({
		src = "res/Image/game/session/dz_session_bg.jpg",
		pos = cc.p(zc.cx, zc.cy),
		fitScreen = true,
		parent = sceneWidget,
		})

	zc.createImageView({
			src = "res/Image/game/session/dz_sprite9bg",
			scale9 = true,
			parent = bgImg,
			size = cc.size(zc.width ,120),
			ap = cc.p(0.5, 0.5),
			pos = cc.p(zc.cx, zc.height - 190),
			})

 
 	zc.createLabel({
		text = zc.lang("texasHoldemScene_GameName", "真人德州"),
		color = cc.c3b(255,255,0),
		pos = cc.p(zc.cx, zc.height - 190),
		fontSize = 24,
		parent = bgImg,
		tag = 33,
		})

	local backBtn = zc.createImageView({
		src = "res/Image/game/session/arrow_return_img.png",
		shaderObj = 1,
		pos = cc.p(60, zc.height - 190),
		parent = bgImg,
		listener = SCENE.closeCallback,
		})

	zc.createLabel({
		text = zc.lang("texasHoldemScene_Return", "返回"),
		color = cc.c3b(255,255,0),
		pos = cc.p(50,backBtn:getContentSize().height/2 ),
		fontSize = 24,
		parent = backBtn,
		})

	--商店
	local shopBtn = zc.createImageView({
		src = "res/Image/game/session/dz_shop_img.png",
		shaderObj = 1,
		pos = cc.p(zc.width-130, zc.height - 190),
		parent = bgImg,
		listener = SCENE.closeCallback,
		})
	zc.createLabel({
		text = zc.lang("texasHoldemScene_Shop", "商店"),
		color = cc.c3b(255,255,0),
		pos = cc.p(85,shopBtn:getContentSize().height/2 ),
		fontSize = 24,
		parent = shopBtn,
		})

	btnTab = {}
	-- 6个场次
	for i=1,3 do -- 行
		for j=1,2 do -- 列
			local idx = (i - 1) * 2 + j
			local posX = zc.cx - 190 + (j-1) * 360
			local posY = zc.cy + 300 - (i-1) * 320
			local sessionBtn = zc.createButton({
				tag = idx,
				normal = "res/Image/game/session/dz_session.png",
				pressed = "res/Image/game/session/dz_session.png",
				pos = cc.p(posX, posY),
				parent = bgImg,
				listener = SCENE.selectCallback,
				})

			--选择场次读表
			local over_wan = false

			local limitMoney = DB_STATICTABLE["proto_texas_holdem"]["list"][tostring(idx)]["max"]
			if tonumber(limitMoney) >= 10000 then
				over_wan = true
				limitMoney = tostring(math.ceil(tonumber(limitMoney)/10000))
			end 
			local startPosX = 120-(string.len(limitMoney)-1)*27
			if over_wan then
				startPosX = 120-string.len(limitMoney)*27
			end
			for ii=1,string.len(limitMoney) do
				local fontnumber = string.sub(limitMoney, ii,ii)
				local numImg = zc.createImageView({
					src = "res/Image/game/session/dz_session_font_" .. tonumber(fontnumber) .. ".png",
					pos = cc.p(startPosX+(ii-1)*54, 160+(ii-1)*3),
					parent = sessionBtn,
					})
			end
			if over_wan then
				zc.createImageView({
				src = "res/Image/game/session/dz_session_font_wan.png",
				pos = cc.p(startPosX+10+string.len(limitMoney)*54, 160+string.len(limitMoney)*3),
				parent = sessionBtn,
				})
			end

			zc.createLabel({
				text = "0",
				color = display.COLOR_WHITE,
				pos = cc.p(120, 70 ),
				ap = cc.p(0, 0.5),
				fontSize = 24,
				parent = sessionBtn,
				tag = 33,
				})

			sessionBtn.endPos = cc.p(posX, posY)
			btnTab[idx] = sessionBtn

			--大下盲注
 			local sb = DB_STATICTABLE["proto_texas_holdem"]["list"][tostring(idx)]["sb"]
 			local bb = DB_STATICTABLE["proto_texas_holdem"]["list"][tostring(idx)]["bb"]
			local betBg = zc.createImageView({
				src = "res/Image/game/session/dz_label_bg.png",
				pos = cc.p(sessionBtn:getContentSize().width/2,-20),
				scale = 1,
				parent = sessionBtn,
				})

			zc.createLabel({
				text = zc.lang("texasHoldemScene_Blind", "盲注: ") .. sb .. "/" .. bb,
				color = cc.c3b(255,255,0),
				pos = cc.p(sessionBtn:getContentSize().width/2,-20),
				fontSize = 24,
				parent = sessionBtn,
				})

		end
		
	end
	--刷新在线人数
	SCENE:getPlayerNum()

	local startbnt_bg = zc.createImageView({
		src = "res/Image/common/green_bigellipse_btn_n.png",
		pos = cc.p(zc.cx, 80),
		shaderObj = 1,
		parent = sceneWidget,
		listener = SCENE.startCallback,
	})
	zc.createImageView({
		src = "res/Image/game/session/dz_start_btn_label.png",
		pos = cc.p(startbnt_bg:getContentSize().width/2, startbnt_bg:getContentSize().height/2),
		shaderObj = 1,
		swallowTouches = false,
		parent = startbnt_bg,
		listener = SCENE.startCallback2,
		})


	--SCENE.showAction()
end

-- function SCENE.showAction()
-- 	if not btnTab then return end
-- 	for i=1,#btnTab do
-- 		if tolua.isnull(btnTab[i])  then return end
-- 	end
	 
-- 	--重置坐标
-- 	for i=1,3 do -- 行
-- 		for j=1,2 do -- 列
-- 			local idx = (i - 1) * 2 + j
-- 			local starPosX = zc.cx - 600 + (j-1) * 1200
-- 			local starPosY = zc.cy + 300 - (i-1) * 320
-- 			btnTab[idx]:setPosition(starPosX,starPosY)
-- 		end
-- 	end
-- 	--移动动作
-- 	local moveIndex = 1
-- 	moveFun = function( curIndex)
-- 		if not btnTab[curIndex] then return end
-- 		local action = {}
-- 		action[#action+1] = cc.EaseExponentialOut:create(cc.MoveTo:create(1,btnTab[curIndex].endPos))
-- 		btnTab[curIndex]:runAction(cc.Sequence:create(action))

-- 	end
-- 	--dealytime延时
-- 	delayFun = function (  )
-- 		if not btnTab[moveIndex] then return end
-- 		local action = {}
-- 		action[#action+1] = cc.DelayTime:create(0.1)
-- 		action[#action+1] = cc.CallFunc:create(function ()
-- 			moveFun(moveIndex)
-- 			moveFun(moveIndex+1)
-- 			if moveIndex then
-- 				moveIndex = moveIndex + 2
-- 				delayFun()
-- 			end
-- 		end)
-- 		bgImg:runAction(cc.Sequence:create(action))
-- 	end
-- 	delayFun()

-- end


-- 选择场次
function SCENE.selectCallback( sender, eventType )
	if eventType == ccui.TouchEventType.ended then
		local subType = sender:getTag()
		local limitTab = DB_STATICTABLE["proto_texas_holdem"]["list"]
		if PlayerData.base.money < tonumber(limitTab[tostring(subType)].min) then
			local needMoney = tonumber(limitTab[tostring(subType)].min) - PlayerData.base.money
			zc.confirm({
	            btnNum         = 2,
	            text           = string.format(zc.lang("texasHoldemScene_JoinRoomErrorMsg", "您还需要%s金币才能进入房间，是否购买？"),tostring(zc.getWan(needMoney,2))), 
	            okCallback = function()
	            	zc.createSCENE("src/app/SCENE/shopSCENE")
	        	end
	        })
			return
		end

		local successCallback = function(data)
	        if(tonumber(data.status) == 1) then
	            zc.showLoading()
	        else
	            zc.getErrorMsg(data.status)
	        end
	    end

	    zc.loadUrl("m=th_join_room", {
	    		type = 4,
	    		room_type = subType,
	            id = 0,
	        }, successCallback)
	end
end

-- 快速开始
function SCENE.startCallback( sender, eventType )
	if eventType == ccui.TouchEventType.ended then
		local successCallback = function(data)
	        if(tonumber(data.status) == 1) then
	            zc.showLoading()
	        else
	            zc.getErrorMsg(data.status)
	        end
	    end

	    zc.loadUrl("m=join_room", {
	    		type = 1,
	    		room_type = 0,
	            id = 0,
	        }, successCallback)
	end
end



function SCENE.closeCallback( sender, eventType )
	if eventType == ccui.TouchEventType.ended then
		zc.showMask(0.5)
		GameManager:unload()
	end
end


function SCENE.clean(  )
	if sceneWidget then
		sceneWidget:removeFromParent()
		sceneWidget = nil
	end
	btnTab = nil
	bgImg = nil
	delayFun = nil
end

return SCENE