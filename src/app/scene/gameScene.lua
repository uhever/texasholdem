--[[
	游戏主场景
]]

local SCENE = {}

local gameScene = nil
local sceneWidget = nil

local groundPath = nil

local curData = nil


function SCENE.create( params )

    gameScene = zc.createMyScene(SCENE.prepare)

    -- 游戏单例文件路径
    groundPath = params.groundPath
    -- 上层参数
    curData = params.data

    return gameScene
end


function SCENE.prepare(  )
	sceneWidget = zc.prepareMyScene(gameScene)

	SCENE.init(  )
end


function SCENE.init(  )
	local PlayGroundClass = zc.require(groundPath)

	local playGroundInstance = PlayGroundClass.new(curData)

	GameManager:setGameInstance(playGroundInstance)

	sceneWidget:addChild(playGroundInstance)
	
end


function SCENE.clean(  )

	GameManager:stopGame()

	if sceneWidget then
		sceneWidget:removeFromParent(true)
		sceneWidget = nil
	end

	groundPath = nil

	curData = nil

	
	
end

return SCENE