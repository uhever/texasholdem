print("commonConfig")

-- 服务器通用标示：
-- op_flag  //标识(0:无,1:新的,2:更新,3:删除)  例：当使用道具op_flag=2, 道具全用完op_flag=3、
zc.OP_FLAG_NORMAL_0 = 0 -- 无
zc.OP_FLAG_NEW_1    = 1 -- 新的
zc.OP_FLAG_UPDATE_2 = 2 -- 更新
zc.OP_FLAG_DELETE_3 = 3 -- 删除

-- 框架zOrder初始值（基于touchLayer）
zc.Z_ORDER_SCENE = 0
zc.Z_ORDER_POP = 100
zc.Z_ORDER_TITLE = 101

zc.Z_ORDER_TIP = 1000
zc.Z_ORDER_MASK = 8888 -- mask遮罩层级
zc.Z_ORDER_SMALL_LOADING = 9999
zc.Z_ORDER_CONFIRM_SYSTEM = 10000

zc.CONFIRM_WEIGHT_NORMAL = 1    -- 确认框weight 一般弹框 0-100 系统级>100 新权重>=旧权重 会覆盖
zc.CONFIRM_WEIGHT_SYSTEM = 100  -- 确认框weight 系统级>100 新权重>=旧权重 会覆盖

zc.IOS_USE_ACTION = 0  -- 1 采用界面动画 0不采用界面动画

zc.GOTYEROOMID = 0

-- 遮罩的透明度
zc.OPACITY_MASK = 100

zc.langCfg = {
    { key = "cn", display = zc.lang("commonConfig_Font", "简体") },
}

-- 自定义Listener的队列
zc.EventListenerQueue = {}

function zc.removeEventListenerQueue(  )

    local eventDispatcher = cc.Director:getInstance():getEventDispatcher()

    for k,v in pairs(zc.EventListenerQueue) do
        eventDispatcher:removeEventListener(v)
    end
end

-- 加载自定义的变量(开发工具)
function zc.loadUserCFG(  )
    zc.getPop("src/app/pop/developerSettingPop").loadUserCFG()
end


-- 游戏类型
zc.GAME_TYPE_TEXAS_NORMAL = 4 -- 德州金币房
zc.GAME_TYPE_TEXAS_FRIEND = 401 -- 德州好友房



-- 聊天类型
zc.CHAT_TYPE_WORLD_1  = 1 -- 世界
zc.CHAT_TYPE_SYSTEM_2 = 2 -- 系统
zc.CHAT_TYPE_ROOM_3   = 3 -- 房间
zc.CHAT_TYPE_PRIVATE_4  = 4 -- 私聊


zc.dataTypeMoney = 1 -- 钱币
zc.dataTypeGold = 2 -- 钻石
zc.dataTypeID = 3
zc.dataTypeRoomCard = 4

--[[
    自定义类型
]]
zc.dataTypeNickName = 10001 -- 用户名
zc.dataTypeHeadImage = 10002 -- 头像


-- socket推送和用户数据更新
function zc.updateUserData(data)
  --  dump(data)
    if (PlayerData == nil or PlayerData.base == nil) then
        return
    end

    if PRC_REVIEW then
        -- 检测防沉迷
        zc.checkAntiAddiction(data)
    end
    
    if data.change and data.change.chat_list then
        for i,v in ipairs(data.change.chat_list) do
            PlayerData:updateChat(v)
            

        end
        
    end

    -- 更新GameManager
    GameManager:updateGameData(data)
    
    if data.change then
        

        -- !!!!有base的change必须写在这里面
        if data.change.base then
            PlayerData:updateBase(data.change.base)


            if zc.nowScene == "src/app/scene/mainScene" and data.change.base.new_guide then
                local gfData = data.change.base.new_guide["1"]
                if gfData == nil then
                    zc.createPop("src/app/pop/gfGuidePop", {id = 1})
                elseif gfData < 4 then

                    zc.createPop("src/app/pop/gfGuidePop", {id = gfData + 1})
                end 
            end
        end
        
        -- 判断好友房状态是否改变
        if data.change.friend_room_id then
            PlayerData.friend_room_id = data.change.friend_room_id
        end
        

        if data.change.mail_receive_list then
            for i, v in pairs(data.change.mail_receive_list) do
                if v.op_flag == zc.OP_FLAG_NEW_1 then
                    PlayerData:insertMailList(v)
                elseif v.op_flag == zc.OP_FLAG_UPDATE_2 then
                    PlayerData:updateMail(v)
                elseif v.op_flag == zc.OP_FLAG_DELETE_3 then
                    PlayerData:deleteMailList(v)
                end
            end
        end
        if data.change.online_reward then
            PlayerData:updateOnlineReward(data.change.online_reward)
        end

        -- 结算时候金币不足 弹出爱心加油站
        if data.change.heart_reward then
            PlayerData:updateheart_reward(data.change.heart_reward)
        end

        if data.change.game_state then
            for k,v in pairs(data.change.game_state) do
                if v == 1 then
                    GameManager:startGame()
                elseif v == 2 then
                    GameManager:startGame({gameState = v})
                elseif v == 0 and (k == tostring(zc.GAME_TYPE_TEXAS_NORMAL) or k == tostring(zc.GAME_TYPE_TEXAS_FRIEND)) then
                    if zc.nowScene == "src/app/scene/gameScene" then
                        if GotyeApi then
                            GotyeApi:leaveRoom(zc.GOTYEROOMID)
                        end
                        zc.switchScene("src/app/scene/mainScene")
                    end
                end
            end
        end

    end
    
end


function zc.loadMemoryTexture()

    -- cc.SpriteFrameCache:getInstance():addSpriteFramesWithFile("Images/bugs/circle.plist")

    -- display.addArmatureFileInfo("Main/common/effect/leaf.csb")
    -- display.addArmatureFileInfo("Fight/common/bones/bones.csb")

    -- display.loadAnim("res/Image/common/effect/", "title_gold", 1, 24, 1/12)
    -- display.loadAnim("res/Image/common/effect/", "title_money", 1, 24, 1/12)
    -- display.loadAnim("res/Image/common/effect/", "title_money1", 1, 19, 1/12)
    -- display.loadAnim("res/Image/common/effect/", "title_gold1", 1, 24, 1/12)

end

function zc.freeMemoryTexture()
    -- CCSpriteFrameCache:getInstance():removeSpriteFramesFromFile("Main/common/common.plist")
    -- CCSpriteFrameCache:getInstance():removeSpriteFramesFromFile("Main/common/common_1.plist")
    -- CCTextureCache:sharedTextureCache():removeTextureForKey("Main/common/common_1.png")
    -- CCSpriteFrameCache:getInstance():removeSpriteFramesFromFile("Main/common/common_2.plist")
    -- CCTextureCache:sharedTextureCache():removeTextureForKey("Main/common/common_2.png")
    -- CCSpriteFrameCache:getInstance():removeSpriteFramesFromFile("Main/common/common_3.plist")
    -- CCTextureCache:sharedTextureCache():removeTextureForKey("Main/common/common_3.png")
    -- CCSpriteFrameCache:getInstance():removeSpriteFramesFromFile("Main/common/common_4.plist")
    -- CCTextureCache:sharedTextureCache():removeTextureForKey("Main/common/common_4.png")

    -- display.removeArmatureFileInfo("Main/common/effect/leaf.csb")
    -- display.removeArmatureFileInfo("Fight/common/bones/bones.csb")

    -- display.unloadAnim("Main/common/effect/", "button")
    -- display.unloadAnim("Main/common/effect/", "suit_light")
    -- display.unloadAnim("Main/common/effect/", "sandglass")
    -- display.unloadAnim("Main/common/effect/", "stone_effect")
    -- display.unloadAnim("Main/common/effect/", "diamond_effect")
    -- display.unloadAnim("Main/common/effect/", "adventureLight")

    -- display.unloadAnim("res/Image/common/effect/", "title_gold")
    -- display.unloadAnim("res/Image/common/effect/", "title_money")
    -- display.unloadAnim("res/Image/common/effect/", "title_money1")
    -- display.unloadAnim("res/Image/common/effect/", "title_gold1")

end



-- 通过资源类型获得小图标路径
function zc.getAttrResPath( mType )
    local mType = tonumber(mType)

    local attrIconSrc = ""
    
    if mType == zc.dataTypeMoney then
        attrIconSrc = "res/Image/common/icon_silver_small.png"
    elseif mType == zc.dataTypeGold then
        attrIconSrc = "res/Image/common/icon_gold_small.png"
    elseif mType == zc.dataTypeID then
        attrIconSrc = "res/Image/common/icon_id_small.png"
    elseif mType == zc.dataTypeRoomCard then
        attrIconSrc = "res/Image/common/icon_roomCard_small.png"
    end

    return attrIconSrc

end