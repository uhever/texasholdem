-- 发布状态下屏蔽log
if(APP_ENV == "dist") then
    function print()
    end

    function dump()
    end
end

-- 加载附加文件(配置等)
zc.require("src/app/commonConfig")
-- 加载自定义UI组件
zc.require("src/app/commonUI")
-- 加载Helper
zc.require("src/app/commonHelper")
-- 加载错误信息提示模块
zc.require("src/app/commonErrorMsg")

-- 解决bug
zc.require("src/app/commonBug")

-- 是否在播放视频
zc.videoPlaying = false

-- 判断是否是断线重连的情况
zc.isReconnection = false

-- 记录SDK是否已经初始化
zc.SDKHadInit = false

-----------------运营数据统计相关-------------------------

--[[
    发送数据统计事件API
    params = {
        eventname, 必填
        userid, 必填
        amount,
        orderid
    }

    eventname = {Login, Register, Purchase}
]]
function zc.sendDataEvent(params)
    
    if(device.platform ~= "android") then
        callOc("sendDataEvent", params)
    else

        if params.userid then
            local parmasStr = "" -- eventname, userid, orderid, amount, serverid

            parmasStr = params.eventname .. "," .. params.userid
            if(params.amount) then
                parmasStr = parmasStr  .. "," .. params.orderid .. "," .. params.amount
            else
                parmasStr = parmasStr  .. ",0,0"
            end
            -- 增加更多信息(PPS等渠道)
            parmasStr = parmasStr .. "," .. SERVERID
            callJava("sendDataEvent", { parmasStr }, "(Ljava/lang/String;)V")
        end

    end

    zc.sendDataEventZCUC(params)

end

-- 推广统计，发送激活信息给展程用户中心
function zc.sendDataEventZCUC(data)
    local eventName = data.eventname

    if not table.inArray(eventName,{"Register","Account","Level","Purchase"})  then
        return
    end

    local params = {
        appid        = "", -- 苹果市场id
        product      = ZC_TRACK_CFG.product,
        mac          = zc.device.mac or "",
        idfa         = zc.device.idfa or "",
        channel      = CHANNELID,
        device_name  = zc.device.model or "",
        os_name      = device.platform,
        os_version   = zc.device.os_version or "",
        jailbreak    = zc.device.jailbreak or "",
        ssid         = zc.device.ssid or "",
        android_id   = zc.device.android_id or "",
        advertising_id = zc.device.advertising_id or "",
        level        = "",
        money        = "",
        sign         = ""
    }

    local appkey
    local httpUrl
    if(APP_PLATFORM == "ios" or APP_PLATFORM == "ios-zc") then
        appkey = ZC_TRACK_CFG.ios.appkey
        params.appid = ZC_TRACK_CFG.ios.appid
        httpUrl = ZC_TRACK_API .. "c=iostrack"
    else
        appkey = ZC_TRACK_CFG.android.appkey
        params.appid = ZC_TRACK_CFG.android.appid
        httpUrl = ZC_TRACK_API .. "c=android_track"
    end

    if tostring(eventName) == "Account" then -- 注册账号
        httpUrl = httpUrl.."&m=reg"
    elseif tostring(eventName) == "Register" then -- 建立角色
        httpUrl = httpUrl.."&m=start"
    elseif tostring(eventName) == "Level" then -- 等级到达7,10,15,20
        httpUrl = httpUrl.."&m=level"
        params.level = data.level
    elseif tostring(eventName) == "Purchase" then -- 支付
        httpUrl = httpUrl.."&m=pay"
        params.money = data.amount
    end


    local signStr = params.android_id..params.appid ..params.channel..params.device_name..params.idfa..params.jailbreak..params.level..params.mac..params.money.. params.os_name .. params.os_version .. params.product..params.ssid .. appkey
    params.sign = md5.sumhexa(signStr)
    dump(params)
    zc.http(httpUrl, { data = json.encode(params) }, function(data)
        dump(data)
        print(zc.lang("common_", "推广统计：")..eventName..zc.lang("common_", "激活信息已发送到后端"))
    end,function (  )
        print("推广统计：sendDataEventZCUC发送失败")
    end,true)

end
-----------------运营数据统计相关 end-------------------------


-----------------native/SDK 相关封装-------------------------

-- 针对部分SDK渠道需要提交用户信息的通用方法(android)
-- uid,name,level,gender,serverid,servername,gold,vip,country
function zc.submitUserInfo()

    if device.platform ~= "android" then

        callOc("submitUserInfo", zc._getUserInfoTab())
    else
        local params = zc._getUserInfoString()
        if params then

            callJava("submitUserInfo", { params } , "(Ljava/lang/String;)V")

        end
    end

end

function zc._getUserInfoString()

    local playerInfo = PlayerData.base

    local paramsStr = playerInfo.uid

    paramsStr = paramsStr .. "," .. string.urlencode(playerInfo.nickname)
    paramsStr = paramsStr .. "," .. tostring(playerInfo.level)
    paramsStr = paramsStr .. "," .. tostring(playerInfo.image)
    paramsStr = paramsStr .. "," .. tostring(SERVERID)
    paramsStr = paramsStr .. "," .. SERVERNAME
    paramsStr = paramsStr .. "," .. tostring(playerInfo.gold)
    paramsStr = paramsStr .. "," .. tostring(playerInfo.vip)
    paramsStr = paramsStr .. "," .. zc.lang("none", "无") -- not need zc.lang

    print(paramsStr)
    return paramsStr
end

function zc._getUserInfoTab()

    local playerInfo = PlayerData.base

    local paramsTab = {
        uid        = tostring(playerInfo.uid),
        gender     = tostring(playerInfo.image),
        nickname   = string.urlencode(playerInfo.nickname),
        serverid   = tostring(SERVERID),
        servername = SERVERNAME,
        vip        = tostring(playerInfo.vip),
        level      = tostring(playerInfo.level),
        gold       = tostring(playerInfo.gold),
    }
    dump(paramsTab, "paramsTab")

    return paramsTab
end



--[[
    调用native部分的二次封装
]]
-- 构建SDK参数string, 按指定符号分割参数
function zc.buildSDKParams(params, diyFlag)

    local flag = "&"
    if(diyFlag) then -- , | ...
        flag = diyFlag
    end
    local paramsStr = ""

    for i = 1, #params do
        if(i == 1) then
            paramsStr = params[i]
        else
            paramsStr = paramsStr .. flag .. params[i]   -- xxx&bbb
        end
    end

    print("zc.paramsStr:", paramsStr)

    return paramsStr
end


-- 取消所有本地通知
function zc.cancelAllNotifications()
    if device.platform ~= "android" then
        callOc("cancelAllNotifications", {})
    else
        callJava("cancelAllNotifications", {}, "()V")
    end
end

-- 添加一个本地通知，支持字符串&table
-- paramsStr => noticeData.id .. "=" .. noticeData.date .. "=" .. noticeData.msg .. "=" .. noticeData.myRepeat
function zc.addNotification(data)

    local paramsStr = ""
    if(type(data) == "table") then
        if(data.id == nil or data.date == nil or data.msg == nil or data.myRepeat == nil) then
            print("addNotification: error params")
            return
        end
        dump(data)

        paramsStr = data.id .. "=" .. data.date .. "=" .. data.msg .. "=" .. data.myRepeat
    else
        paramsStr = data
    end

    print("paramsStr:", paramsStr)

    if device.platform ~= "android" then
        callOc("addNotification", { noticeStr = paramsStr })
    else
        callJava("addNotification", { paramsStr }, "(Ljava/lang/String;)V")
    end
end

-- 取消一个本地通知
-- noticeId => "tili"/"1"
function zc.cancelNotification(noticeId)
    if device.platform ~= "android" then
        callOc("cancelNotification", { noticeId = noticeId })
    else
        callJava("cancelNotification", { noticeId }, "(Ljava/lang/String;)V")
    end
end

-- SDK登录
function zc.loginSDK()
    if device.platform ~= "android" then
        callOc("loginSDK", { })
    else
        callJava("loginSDK", { }, "()V")
    end
end

-- 退出SDK
function zc.exitSDK()
    if device.platform ~= "android" then
        callOc("exitSDK", { })
    else
        callJava("exitSDK", { }, "()V")
    end
end

-- 切换账号
function zc.switchAccount()
    if device.platform ~= "android" then
        callOc("switchAccount", { })
    else
        callJava("switchAccount", { }, "()V")
    end
end


-- 发送验证码
function zc.sendAuthcode( params )
    if device.platform ~= "android" then
        callOc("sendAuthCode", params)
    else
        callJava("sendAuthCode", {zc.buildSDKParams({params.phoneNumber}) }, "(Ljava/lang/String;)V")
    end
end


--[[
    通用SDK处理
]]

--[[
    直播SDK通信
]]
function zc.loginSDKLive( params )

    -- zc.showLoading()

    if device.platform ~= "android" then
        callOc("loginSDKLive", params)
    else
        callJava("loginSDKLive", {zc.buildSDKParams({params.method, params.phoneNumber, params.authcode}) }, "(Ljava/lang/String;)V")
    end
end

-- 微信分享
function zc.doSdkShare( params )
    if device.platform ~= "android" then
        callOc("share", params)
    else
        local sdkParams = {}
        if params.contentType == "text" then
            sdkParams = zc.buildSDKParams({params.contentType, params.wxScene, params.contentText})
        elseif params.contentType == "image" then
            sdkParams = zc.buildSDKParams({params.contentType, params.wxScene, params.imageURL})
        elseif params.contentType == "link" then
            sdkParams = zc.buildSDKParams({params.contentType, params.wxScene, params.contentURL, params.contentTitle, params.contentDescription})
        end

        callJava("share", {sdkParams}, "(Ljava/lang/String;)V")
    end
end

-- 获取地理位置
function zc.sdkGetLocation( )
    if device.platform ~= "android" then
        callOc("sdkGetLocation", {})
    else
        callJava("sdkGetLocation", { }, "()V")
    end
end

-- 获取通讯录
function zc.sdkGetContact( )
    if device.platform ~= "android" then
        callOc("sdkGetContact", {})
    else
        callJava("sdkGetContact", { }, "()V")

    end
end

-- 开始录音
function zc.sdkStartRecorder( )
    if device.platform ~= "android" then
        callOc("sdkStartRecorder", {})
    else
        callJava("sdkStartRecorder", { }, "()V")

    end
end

-- 结束录音
function zc.sdkStopRecorder( )
    if device.platform ~= "android" then
        callOc("sdkStopRecorder", {})
    else
        callJava("sdkStopRecorder", { }, "()V")

    end
end

-- 打开相册(类型(type)1.头像 2.俱乐部头像)
function zc.sdkFetchImage( params )
    if device.platform ~= "android" then
        callOc("sdkFetchImage", params)
    else
        callJava("sdkFetchImage", {zc.buildSDKParams({params.type, params.width})}, "(Ljava/lang/String;)V")

    end
end

-- 上传图片
function zc.uploadImage( params )
    if params.iconPath == "" then return end
    if PlayerData == nil or PlayerData.base == nil then return end

    local iconPath = params.iconPath

    PlayerData.iconPath = iconPath -- reg

    local rd = math.random(1, #SERVERPORTS)
    local serverUrl = SERVERADDR .. ":" .. SERVERPORTS[rd]

    zc.sdkUploadImage({ 
        uploadUrl = "https://cdnt-poker.zhanchenggame.com/index.php?c=user_image&m=upload",
        server_url = base64.encode(serverUrl),
        userfile = iconPath,
        -- userfile = cc.FileUtils:getInstance():fullPathForFilename("res/Image/rolehead/icon.jpg"),
        token = GAMETOKEN,
        server_id = tostring(SERVERID),
        type = tostring(params.type),
        })

end

-- 上传图片
function zc.sdkUploadImage( params )
    if device.platform ~= "android" then
        callOc("sdkUploadImage", params)
    else
        callJava("sdkUploadImage", {json.encode(params)}, "(Ljava/lang/String;)V")

    end
end

-- 
function zc._resolveUploadImage( params )
    if params.icon_small == nil or params.icon_small == "" or PlayerData == nil or PlayerData.iconPath == nil then
        zc.tipInfo(zc.lang("loginSceneSdk_UploadPictureFailure", "头像上传失败！"))
        return
    end

    local md5FileName = md5.sumhexa(params.icon_small)

    local downloadPath = "tmp/head/" .. md5FileName .. ".jpg"

    os.prepareDir(downloadPath)

    local pathSource = PlayerData.iconPath
    local pathDirect = device.writablePath .. downloadPath

    local ret, err = os.rename(pathSource, pathDirect)
    if not err then

        if params.type == 1 then -- 玩家头像
            zc.tipInfo(zc.lang("common_UpdataSuccess", "头像上传成功！"))
        elseif params.type == 2 then
            -- 接下来的步骤
            zc.tipInfo(zc.lang("common_ClubUpdataSuccess", "俱乐部头像上传成功!"))
        end
    end

end


-- 上传图片
function zc.sdkPhoneShake(  )
    if device.platform ~= "android" then
        callOc("sdkPhoneShake", {})
    else
        callJava("sdkPhoneShake", { }, "()V")
    end
end


-- 显示客服中心
function zc.showServiceCenter(  )

    if not PlayerData or not PlayerData.base then return end

    zc.pauseMusic()

    local imageUrl = PlayerData.base.image
    local userId = PlayerData.base.uid
    local jsonData = json.encode({
        {key = "real_name", value = PlayerData.base.nickname},
        {key = "mobile_phone", value = 0},
        {key = "email", value = userId .. "@gmail.com"},
        {key = "avatar", value = imageUrl},
        {label = "UID", key = "uid", value = userId},
        {label = "游戏币", key = "money", value = PlayerData.base.money},
        })

    local params = { 
        imageUrl = imageUrl,
        userId = userId,
        jsonData = jsonData,
        }

    if device.platform ~= "android" then
        callOc("showServiceCenter", params)
    else
        callJava("showServiceCenter", {json.encode(params)}, "(Ljava/lang/String;)V")

    end
end

-----------------native/SDK 相关封装 end-------------------------


function zc.setLangueType( langKey )
    zc.language = langKey
    cc.UserDefault:getInstance():setStringForKey("devLanguage", zc.language)
    cc.UserDefault:getInstance():flush()

    -- zc.loadLangConfig() -- 加载语言包放在了initScene
end


----------------震动管理------------------
zc.phoneShakeOpen = true
-- 本地保存的信息
local cachePhoneShakeOpenOpen = cc.UserDefault:getInstance():getStringForKey("cachePhoneShakeOpenOpen")
if cachePhoneShakeOpenOpen and cachePhoneShakeOpenOpen == "false" then
    zc.phoneShakeOpen = false
end

function zc.setPhoneShakeStatus(status)
    if(zc.phoneShakeOpen == status) then
        return
    end

    if(status == true) then
        zc.phoneShakeOpen = true
        zc.phoneShake()
    else
        zc.phoneShakeOpen = false
    end

    cc.UserDefault:getInstance():setStringForKey("cachePhoneShakeOpenOpen", tostring(zc.phoneShakeOpen))
    cc.UserDefault:getInstance():flush()
end

function zc.phoneShake()
    if zc.phoneShakeOpen == true then
        zc.sdkPhoneShake()
    end
end

----------------音效管理------------------
-- 音乐列表
zc.audioList = zc.require("src/app/commonAudio")
-- 背景音乐 开关
zc.bgMusicOpen = true
-- 音效开关 开关
zc.audioOpen = true

-- 本地保存的信息
local cacheBgMusicOpen = cc.UserDefault:getInstance():getStringForKey("bgMusicOpen")
if cacheBgMusicOpen and cacheBgMusicOpen == "false" then
    zc.bgMusicOpen = false
end

local cacheAudioOpen = cc.UserDefault:getInstance():getStringForKey("audioOpen")
if cacheAudioOpen and cacheAudioOpen == "false" then
    zc.audioOpen = false
end

local cacheEffectOpen = cc.UserDefault:getInstance():getStringForKey("effectOpen")
if cacheEffectOpen and cacheEffectOpen == "false" then
    zc.effectOpen = false
end

-- 当前播放的音乐
zc.currentMusic = ""

function zc.pauseMusic( )
    if zc.bgMusicOpen == true then
        audio.pauseMusic()
    end
end

function zc.resumeMusic( )
    if zc.bgMusicOpen == true then
        audio.resumeMusic()
    end
end

function zc.pauseAllEffects()
    if zc.audioOpen == true then
        audio.stopAllEffects()
    end
end

function zc.resumeAllEffects()
    if zc.audioOpen == true then
        audio.resumeAllEffects()
    end
end

function zc.playEffect(audioName, stopAll)
    if zc.audioOpen == true then
        if stopAll then
            audio.stopAllSounds()
        end
        
        return audio.playSound(zc.audioList.effect[audioName])
    end
end

function zc.playEffectCustomer(audioName, stopAll)
    if zc.audioOpen == true then
        if stopAll then
            audio.stopAllSounds()
        end
        
        return audio.playSound(audioName)
    end
end

function zc.playMusic(musicName, isLoop)
    local loop = isLoop or false

    zc.currentMusic = musicName

    if zc.bgMusicOpen == true then
        return audio.playMusic(zc.audioList.bgMusic[musicName], loop)
    end
end

function zc.playMusicCustomer(musicName, isLoop)
    local loop = isLoop or false

    zc.currentMusic = musicName

    if zc.bgMusicOpen == true then
        return audio.playMusic(musicName, loop)
    end
end

function zc.setMusicStatus(status)
    if(zc.bgMusicOpen == status) then
        return
    end


    if(status == true) then
        zc.bgMusicOpen = true
        if(zc.currentMusic ~= "") then
            zc.playMusic(zc.currentMusic, true)
        end
    else
        zc.bgMusicOpen = false
        audio.stopMusic(true)
    end

    cc.UserDefault:getInstance():setStringForKey("bgMusicOpen", tostring(zc.bgMusicOpen))
    cc.UserDefault:getInstance():flush()
end

function zc.setAudioStatus( status )
    if zc.audioOpen == status then
        return
    end
    zc.audioOpen = status
    cc.UserDefault:getInstance():setStringForKey("audioOpen", tostring(zc.audioOpen))
    cc.UserDefault:getInstance():flush()
end

function zc.setEffectStatus(status)
    if(zc.effectOpen == status) then
        return
    end


    if(status == true) then
        zc.effectOpen = true
    else
        zc.effectOpen = false
    end

    cc.UserDefault:getInstance():setStringForKey("effectOpen", tostring(zc.effectOpen))
    cc.UserDefault:getInstance():flush()


end


----------------音效管理 end------------------


-- 文件下载器初始化
zc.fileLoader = zc.require("src/app/component/fileLoader")


--[[
    获取touchLayer的单例
    注：每个场景切换后，实例会被销毁，在新场景需要重新获取。
]]
local touchLayerInstance = nil
function zc.getTouchLayer(forceNew)
    if(forceNew) then
        zc.relaseTouchLayer()
    end

    if(touchLayerInstance == nil) then
        touchLayerInstance = ccui.Widget:create()
        touchLayerInstance:setContentSize(zc.width, zc.height)
        touchLayerInstance:setPosition(zc.cx, zc.cy)
    end
    return touchLayerInstance
end
function zc.relaseTouchLayer()
    touchLayerInstance = nil
end


--[[
    切换场景
]]
zc.preScene = ""
zc.nowScene = ""

function zc.switchScene( objSceneName , temp_data )

    if(zc.nowScene ~= "") then

        -- 切换场景时关闭所有pop
        zc.closeAllPops( true )

        -- 清理场景
        zc.cleanScene( zc.nowScene )
    end

    
    cc.Director:getInstance():getTextureCache():removeUnusedTextures()

    if(objSceneName == "src/app/scene/loginScene") then
        -- 清理json文件缓存(更新)
        ccs.GUIReader:destroyInstance()
    end

    -- 清除loading
    zc.getPop("src/app/pop/smallLoadingPop").clean()

    local scene = zc.require(objSceneName)
    
    zc.preScene = zc.nowScene
    -- 保存当前场景ID
    zc.nowScene = objSceneName

    zc.cleanGlobalUI()
    
    cc.Director:getInstance():replaceScene(scene.create(temp_data))

    zc.clearTips()

end

-- 清理场景
function zc.cleanScene( objSceneName )

    local oldScene = zc.require(objSceneName)
    oldScene.clean()

end
-- 获取场景的实例
function zc.getScene( objSceneName )
    local scene = zc.require(objSceneName)
    return scene
end

--- 创建一个空场景，并绑定onEnter事件
-- @param callback
-- @return CCScene实例
function zc.createMyScene(callback)
    local newScene = cc.Scene:create()
    newScene:registerScriptHandler(function(state)
        if state == "enter" then
            if(callback) then

                callback()

            end
        end
    end)

    return newScene
end

--- 为使用场景做准备（创建触摸层、容器）
-- @param sceneInstance CCScene实例
-- @return 主场景容器Widget
-- @see zc.createMyScene
function zc.prepareMyScene(sceneInstance)
    local touchLayer = zc.getTouchLayer(true)
    sceneInstance:addChild(touchLayer)

    -- 主场景容器
    local sceneWidget = ccui.Widget:create()
    sceneWidget:setPosition(cc.p(zc.sx, zc.sy))
    touchLayer:addChild(sceneWidget, zc.Z_ORDER_SCENE)


    return sceneWidget
end


--[[
    当前显示的POP列表
    进入时创建，离开时删除
]]
zc.activePops = {}
function zc.addToActivePops(popName)
    table.insert(zc.activePops, popName)
end
function zc.removeFromActivePops(popName)
    for i = #zc.activePops, 1, -1 do
        if(zc.activePops[i] == popName) then
            table.remove(zc.activePops, i)
            break
        end
    end
end

function zc.popIsActive( popName )
    for i = #zc.activePops, 1, -1 do
        if(zc.activePops[i] == popName) then
            return true
        end
    end
    return false
end

--[[
    当前显示的全屏POP列表
    每一个全屏POP都会用栈管理，同时只会允许一个POP显示（最上层的）
]]
-- 是否打开此功能
zc.fullPopSingle = true
-- 已激活的全屏弹窗列表
zc.activeFullPops = {}

function zc.activeFullPopsIn(popName)
    if(zc.fullPopSingle == false) then
        return
    end

    -- 隐藏最后一个pop
    zc.updateLastAfPop("hide")

    table.insert(zc.activeFullPops, popName)
end

function zc.activeFullPopsOut(popName)
    if(zc.fullPopSingle == false) then
        return
    end

    for i = 1, #zc.activeFullPops do
        if popName == zc.activeFullPops[i] then
            table.remove(zc.activeFullPops, i)
        end
    end

    -- 重新显示最后一个pop
    zc.updateLastAfPop("show")

end

-- 在 zc.closeAllPops 里需要用到
function zc.cleanAllActiveFullPop()
    zc.activeFullPops = {}
end

-- status = show/hide
function zc.updateLastAfPop(status)
    -- 隐藏上一个POP
    local n = #zc.activeFullPops
    if(n > 0) then
        local lastName = zc.activeFullPops[n]
        local lastPop = zc.getPop(lastName)

        if(status == "show") then
            if(lastPop.show) then
                lastPop.show()
            end
        end

        if(status == "hide") then
            if(lastPop.hide) then
                lastPop.hide()
            end
        end


    else
        local curScene = zc.getScene(zc.nowScene)
        if curScene.show and curScene.hide  then
            if(status == "show") then
                if(curScene.show) then
                    curScene.show()
                end
            end

            if(status == "hide") then
                if(curScene.hide) then
                    curScene.hide()
                end
            end

            
        end
    end
end

-- 这里定义的pop列表，都不走模块加载的判断。
local specialPops = {}

local tmp_g_tb = {}

local save_g_tb = {}

-- 显示弹出层（独占型，不可能同时弹出多个）
function zc.createPop(popName, params)

    if GLOBAL_VARIABLE_TEST then
        local G_num = table.nums(_G)

        -- 拿到当前的变量
        if table.nums(tmp_g_tb) == 0 then
            for k,v in pairs(_G) do
                tmp_g_tb[k] = true
            end
        end
    end

    zc.createPopProcess(popName, params)

end


function zc.createPopProcess(popName, params)
    if zc.popIsActive(popName) then
        return
    end

    local pop = zc.require(popName)
    if( pop.isFullScreen and pop.isFullScreen() ) then
        zc.activeFullPopsIn(popName)
    end

    pop.create(params)


end

--[[
    关闭弹窗
    popName: 弹窗名字
    NO_EMIT_FULL_SCREEN_LOGIC: 不触发全屏POP的逻辑，默认为nil，在closeAllPops时会触发
]]
function zc.closePop(popName, NO_EMIT_FULL_SCREEN_LOGIC)

    zc.updateTitle(popName, nil, true)
    zc.updateBottom(popName, nil, true)

    local pop = zc.require(popName)

    pop.clean()

    if GLOBAL_VARIABLE_TEST then
        local G_num = table.nums(_G)
        print("数量", G_num)

        for k,v in pairs(_G) do
            if tmp_g_tb[k] then

            else
                if save_g_tb[popName] == nil then
                    save_g_tb[popName] = {}
                end

                save_g_tb[popName][k] = true

                tmp_g_tb[k] = true

            end
        end

        dump(save_g_tb)
    end


    if(NO_EMIT_FULL_SCREEN_LOGIC == nil) then
        if( pop.isFullScreen and pop.isFullScreen() ) then
            zc.activeFullPopsOut(popName)
        end
    end

end

function zc.closePopSafe(popName)
    local isActive = false
    for i = #zc.activePops, 1, -1 do
        if(popName == zc.activePops[i]) then
            isActive = true
        end
    end

    if(isActive) then
        zc.closePop(popName)
    end
end

function zc.getPop(popName)

    local pop = zc.require(popName)

    return pop
end

function zc.closeAllPops( showFlag )
    local t = {}
    for i = #zc.activePops, 1, -1 do
        table.insert(t, zc.activePops[i])
    end
    for i = 1, #t do
        local popName = t[i]
        zc.closePop(popName, true)
        print("closeAllPops[ " .. popName .. " ]")
    end
    t = nil

    -- 针对全屏弹窗的逻辑清理
    if(zc.fullPopSingle) then
        zc.cleanAllActiveFullPop()
        if not showFlag then
            local myScene = zc.getScene(zc.nowScene)
            if(myScene.show) then
                myScene.show()
            end
        end
    end

end

--- 创建一个空的POP，返回一个POP的主容器
-- @param popName pop的标志
-- @param params WithMask, zOrder, showBg, showTitle(bool/table), showClose,
-- @return POP的主容器
-- @see zc.closePop
-- @usage zc.createMyPop(popName, {
--        withMask = true,
--        showClose = true
--        })
function zc.createMyPop(popName, params)

    local touchLayer = zc.getTouchLayer()

    -- 主节点
    local myPopWidget = ccui.Widget:create()
    myPopWidget:setName(popName)
    myPopWidget:setPosition(cc.p(zc.sx, zc.sy))
    touchLayer:addChild(myPopWidget, zc.Z_ORDER_POP) -- 默认Zorder

    -- 触摸节点
    local mask = ccui.Layout:create()
    mask:setContentSize(cc.size(zc.width, zc.height))
    mask:setBackGroundColorType(LAYOUT_COLOR_SOLID)
    mask:setBackGroundColor(cc.c3b(0, 0, 0))
    mask:setBackGroundColorOpacity(zc.OPACITY_MASK)
    myPopWidget:addChild(mask)

    -- 设置触摸
    mask:setTouchEnabled(true)
    if type(params.mask) == "boolean" then
        mask:setTouchEnabled(params.mask)
    end
    

    myPopWidget.maskLayout = mask -- reg


    if params.opacity then
        mask:setBackGroundColorOpacity(params.opacity)
    end    

    -- 触摸关闭
    if params.touchClose then
        mask:setTouchEnabled(true)
        mask:addTouchEventListener(function ( sender, eventType )
            if eventType == ccui.TouchEventType.ended then
                if zc.getPop(popName).closeCallback then
                    zc.getPop(popName).closeCallback(sender, eventType)
                end
            end
        end)
    end

    -- 触摸吞噬
    if type(params.swallowTouches) == "boolean" then
        mask:setSwallowTouches(params.swallowTouches)
    end


    -- 背景类型
    if params.bgType then

        mask:setBackGroundColorOpacity(0)

        if params.bgType == 0 then -- 自定义路径

            local bgImg = zc.createImageView({
                src = params.bgSrc,
                pos = cc.p(zc.cx, zc.cy),
                fullScreen = true,
                parent = myPopWidget
                })


        elseif params.bgType == 1 then

            local texture = ZCTool:getInstance():getGaussianTexture(touchLayer)
            local sp = cc.Sprite:createWithTexture(texture)
            sp:setPosition(cc.p(zc.cx, zc.cy))
            sp:setScale(4)
            myPopWidget:addChild(sp)

        end
    end

    if params.title then
        zc.updateTitle( popName, params.title, false )
    else
        myPopWidget:setLocalZOrder(zc.Z_ORDER_TITLE)
    end

    -- Bottom
    -- if params.bottom then
        zc.updateBottom( popName, params.bottom, false )
    -- end

    if params.zOrder then
        myPopWidget:setLocalZOrder(params.zOrder)
    end

    return myPopWidget, height

end



----------------通用组件 提示框 遮罩等------------------

-- Loading状态显示隐藏
function zc.showLoading(params)
    zc.createPop("src/app/pop/smallLoadingPop", params)
end


function zc.hideLoading(params)
    zc.getPop("src/app/pop/smallLoadingPop").close(params)
end



--[[
    local myParmas = {
        btnNum = btnNum, -- 1确定/2确定取消/3确定取消X
        text = text,
        okCallback = okCallback,
        cancelCallback = cancelCallback,
        okText = okText,
        cancelText = cancelText,
        closeCallback = closeCallback, -- 当只需要关闭confirm时
        richData = textData -- 富文本形式,
        touchClose = true, -- 当触摸框外的区域 是否关闭弹窗
    }
]]
-- confirm权重
zc.confirmWeight = zc.CONFIRM_WEIGHT_NORMAL

function zc.confirm(params)
    local weight = params.weight or zc.CONFIRM_WEIGHT_NORMAL

    if params.godMode then -- 系统层Confirm
        params.touchClose = false
        weight = zc.CONFIRM_WEIGHT_SYSTEM
    end

    
    if zc.popIsActive("src/app/pop/confirmPop") and weight >= zc.confirmWeight  then
        zc.closePop("src/app/pop/confirmPop")
    end

    zc.confirmWeight = weight

    zc.createPop("src/app/pop/confirmPop", params)
end

function zc.closeConfirm()
    if zc.popIsActive("src/app/pop/confirmPop") then
        zc.closePop("src/app/pop/confirmPop")
    end

    zc.confirmWeight = zc.CONFIRM_WEIGHT_NORMAL
end

----------------通用组件 提示框 遮罩等 end------------------


-----------------------网络请求部分-----------------------------------

--[[
    网络请求列表处理
    每个请求都加到数组里，完成后删除。
]]
-- 总的列表
local _requestList = {}
-- 每次请求的唯一ID
local _httpUDID = 0
function zc._getHttpUdid()
    _httpUDID = _httpUDID + 1
    return _httpUDID
end

function zc._getHttpUdid()
    _httpUDID = _httpUDID + 1
    return _httpUDID
end

function zc._setHttpUdid(udid)
    _httpUDID = udid
end

--[[
    r.id
    r.s
    r.requestUrl
    r.params
    r.isSilent
    r.onRequestFinished
    r.faildTimes
]]
function zc._addRequestToList(udid, rq)
    -- print("_addRequestToList", udid)
    table.insert(_requestList, {
            id = udid,
            s = 1
        })
end

function zc._getRequest(udid)
    for i = #_requestList, 1, -1 do
        if(_requestList[i] and _requestList[i].id == udid) then
            -- print("_cleanRequest", udid)
            return _requestList[i]
        end
    end
end

function zc._setRequestInfo( udid , info )
    local r = zc._getRequest(udid)
    if(r) then
        for k, v in pairs(info) do
            r[k] = v
        end
    else
        print("error: request not found (" .. udid .. ")")
    end
end

function zc._increaseFaildTimes( udid )

    local r = zc._getRequest(udid)
    if(r) then
        r.faildTimes = r.faildTimes + 1

        return r.faildTimes
    else -- 如果没有找到请求数据，那么返回错误次数3，直接走网络出错逻辑。
        return 3
    end

end

function zc._cleanRequest(udid, requestData)
    -- 获取request对象
    local r = requestData
    if(r == nil) then
        r = zc._getRequest(udid)
    end

    -- 清理网络请求的所有属性
    if(r) then
        r.s = 0
        r.requestUrl = nil
        r.params = nil
        r.isSilent = nil
        r.onRequestFinished = nil
        r.faildTimes = nil

        -- print(zc.lang("common_", "网络请求已清理："), udid)
    end
end

function zc.checkRequestEnabled(udid, modify)

    local r = zc._getRequest(udid)
    if(r and r.s == 1) then
        if(modify) then
            r.s = 0
        end
        -- print("zc.checkRequestEnabled", true)
        return true
    end

    -- print("zc.checkRequestEnabled", false)
    return false
end

function zc.cancelAllRequest()
    print("_cleanAllRequest  [old data]")
    dump(_requestList)
    for i = #_requestList, 1, -1 do
        local r = _requestList[i]
        print("_force_cleanRequest", r.id, r.s)

        zc._cleanRequest(r.id, r)
    end
    _requestList = {}
end



-- 调用此方法时，request有可能已经被释放
function zc.retryHttp(requestUdid)
    scheduler.performWithDelayGlobal(function( )
        print(zc.lang("common_", "请求重试ing:"), requestUdid)
        zc.createOneHttpRequest( requestUdid )
    end, 1)
end

-- 判断没有加密的API
zc.noCryptAPIs = {
    ZC_TRACK_API,
    UPDATE_API
}
function zc.isNoCryptAPI(url)

    local result = false

    for i = 1, #zc.noCryptAPIs do
        local list = zc.noCryptAPIs[i]
        local _s, _e = string.find(url, list)
        if(_s == nil) then
            result = true
        end
    end

    return result
end

-- 网络
function zc.http(requestUrl, params, successCallback, failCallback, isSilent, cancelCallback, timeout)
    local params = params or {}
    local isSilent = isSilent or false
    local timeout = timeout or 20

    -- 唯一ID
    local requestUdid = zc._getHttpUdid()

    -- 网络请求完成后 成功回调
    local successFunction
    if(successCallback ~= nil) then
        successFunction = successCallback
    else
        successFunction = function() end
    end

    --[[
        网络请求失败的回调
        此部分逻辑较多，针对不同的错误类型进行不同的处理，有部分错误提示即可，部分错误必须要求玩家重新登录
    ]]
    

    -- 通用错误处理，资源清理、界面关闭、提示等流程
    local commonFailFunction = function(httpCode)

        local goout = false -- 是否强制退出

        -- 遮罩
        zc.hideLoading()
        -- 清理所有请求
        zc.cancelAllRequest()
        -- 停止CD等
        zc.stopPlayer()

        -- 错误提示
        local errorMsg = zc.lang("common_NetFailureMsg", "网络连接失败，请稍后重试:") .. httpCode


        -- 检查是否有登陆。如果没有登陆，则强制退出
        if(PlayerData == nil or PlayerData.base == nil) then
            goout = true
        end


        -- 使用重新连接即可
        if(goout == false) then

            local reconnectCallback = function()
                -- 清理用户信息
                zc.cleanPlayer()
                zc.reconnect()
                zc.isReconnection = true
            end
            local backCallback = function()
                zc.logout()
                zc.isReconnection = false
            end

            zc.confirm({
                btnNum         = 2,
                text           = errorMsg,
                okCallback     = reconnectCallback,
                cancelCallback = backCallback,
                okText         = zc.lang("common_Reconnect", "重新连接"),
                cancelText     = zc.lang("common_ReturnLogin", "返回登录"),
                godMode = true
            })

        -- 必须要求退到登陆界面
        else

            local clickCallback = function()
                zc.logout()
                zc.isReconnection = true
            end

            zc.confirm({
                btnNum         = 1,
                text           = errorMsg,
                okCallback     = clickCallback,
                godMode = true
            })
            

        end
    end
    -- 如果网络请求指定了错误处理方法，那么根据方法的返回值来确定是否走通用的错误处理
    -- (用在一些 “允许出现错误的且对游戏没有影响的” 场景)
    
    -- 在通信、解析层出现的错误，会调用它；它会针对特定的错误进行重新发请求。
    local failFunction = function(enableRetry, httpCode)

        -- *********** 如果是因为网络抖动等原因或操作太频繁而出错，增加重试机制 **********
        if(enableRetry and zc._increaseFaildTimes( requestUdid ) < 3) then
            zc.retryHttp(requestUdid)
            return
        end

        
        if failCallback then
            -- 执行被指定的错误callback。
            failCallback(httpCode)
        else
            -- 走正常错误
            commonFailFunction(httpCode)
        end
        

        if(isSilent == false) then
            zc.hideLoading()
        end
        -- 清理网络请求信息
        zc._cleanRequest(requestUdid)

    end


    -- 网络请求完成回调
    local onRequestFinished = function(request)

        print("request.readyState is:", request.readyState, "request.status is: ",request.status, "request.statusText is: ",request.statusText)
        if request.readyState == 4 and (request.status >= 200 and request.status < 207) then

            if(zc.checkRequestEnabled(requestUdid, true) == false) then
                print(requestUdid, "请求已经完成或被清理。不再执行后续的逻辑。")
                return
            end

            local responseDecode = request.response

            print("responseDecode", responseDecode)

            -- 解析JSON
            local result = nil
            local errorHandler = function()
                print("后端返回数据 不是合法的JSON", requestUrl, responseDecode)
                failFunction(false, request.status)
            end


            local decodeResponse = function()
                result = json.decode(responseDecode)
            end

            local xpcallStatus, xpcallMsg = xpcall(decodeResponse, errorHandler)

            -- 得到了结果table，进行具体的逻辑处理
            if(xpcallStatus == true) then

                if(type(result) ~= "table") then
                    print("请求错误：JSON解析完成后，结构与预期不符1")
                    print("response:", responseDecode)
                    failFunction(false, request.status)
                    return
                end


                if(isSilent == false) then
                    zc.hideLoading()
                end

                successFunction(result)

                -- 处理成功后清理request
                zc._cleanRequest(requestUdid)
                
            else
                print("后端返回数据 不是合法的JSON", requestUrl)
                print("response:", responseDecode)
                failFunction(false, request.status)
            end


        else
            
            print("request.readyState is:", request.readyState, "request.status is: ",request.status)
            failFunction(false, request.status)
        end
        request:unregisterScriptHandler()

    end


    if(requestUrl == nil) then
        return
    end
    -- 保存请求udid
    zc._addRequestToList(requestUdid)

    -- 保存网络请求相关信息
    zc._setRequestInfo( requestUdid , {
            requestUrl = requestUrl,
            params = params,
            isSilent = isSilent,
            onRequestFinished = onRequestFinished,
            timeout = timeout,
            faildTimes = 0, -- 统计失败次数
        } )

    -- 发起网络请求
    zc.createOneHttpRequest( requestUdid )

    return requestUdid
end

--[[
    httpUid 为请求唯一id
]]

function zc.createOneHttpRequest( httpUid )

    local httpInfo = zc._getRequest( httpUid )
    if(httpInfo == nil) then
        print("WARNING: 网络请求句柄出现异常，请检查日志和逻辑！", httpUid)
        return
    end

    local isSilent = httpInfo.isSilent or false
    local requestUrl =  httpInfo.requestUrl
    local onRequestFinished = httpInfo.onRequestFinished
    local params = table.copy(httpInfo.params)

    -- 2 去掉特殊的k值
    params["accesstoken"] = nil

    -- 创建一个请求，并以 POST 方式发送数据到服务端(增加请求随机数，避免缓存)
    local osTime = os.time()
    local url = requestUrl .. "&time=" .. osTime .. "&"

    if(APP_ENV == "dev" and (httpInfo.isSilent == nil or httpInfo.isSilent == false)) then
        print(url)
    end

    local request = cc.XMLHttpRequest:new()
    if httpInfo.timeout then
        request.timeout = httpInfo.timeout
    end

    request.responseType = cc.XMLHTTPREQUEST_RESPONSE_BLOB

    
    local postString = ""

    for k , v in pairs(params) do
        if(APP_ENV == "dev") then
            print(k, v)
        end
        postString = postString .. k .. "=" .. v .. "&"
    end

    if(CMDKEY and CMDKEY ~= "" ) then
        local md5Str = md5.sumhexa(GAMETOKEN .. "#" .. CMDKEY .. "#" .. osTime)
        
        local uniStr = md5Str .. "#" .. httpUid
        local base64UniUrl = zc._encodeBase64(uniStr)

        postString = postString .. "sign=" .. base64UniUrl .. "&"
    end

    if(GAMETOKEN and GAMETOKEN ~= "") then
        postString = postString .. "&token=" .. GAMETOKEN .. "&"
    end

    if(GAMEUID and GAMEUID ~= "") then
        postString = postString .. "&token_uid=" .. GAMEUID .. "&"
    end



    if(isSilent == false) then
        zc.showLoading()
    end

    -- 开始请求。当请求完成时会调用 callback() 函数
    local function onReadyStateChanged(  )
        onRequestFinished(request)
    end
    
    
    request:open("POST", url)
    request:registerScriptHandler(onReadyStateChanged)
    request:send(postString)

end

function zc._encodeBase64(text)
    local base64Str = base64.encode(text)

    local len = string.len(base64Str)
    local step = math.ceil(len / 4)

    local requestEncode = ""
    for i = 1, 4 do
        local p2 = len - step * (i - 1)
        local p = (len - (len - step * i)) * -1
        requestEncode = requestEncode .. string.sub(base64Str, p, p2)
        
    end

    return requestEncode
end




-- 后端错误信息 处理
function zc.getErrorMsg(errorCode)
    local errorCode = tonumber(errorCode)

    local errorMsg = zc.errorList.errorMsgList[errorCode] or zc.lang("common_OperationFailed", "操作失败，请稍后重试！")

    -- 强制退出到登陆
    local forceLogout = false

    -- 退出错误
    if errorCode < 500 then -- 直接退到登陆界面
        forceLogout = true

        zc.confirm({
            btnNum         = 1,
            text           = errorMsg,
            godMode = true,
            okCallback = function()
                zc.logout()
            end,
        })

    elseif errorCode >= 501 and errorCode <= 600 then -- 退出到主界面
        
        zc.confirm({
            btnNum         = 1,
            text           = errorMsg,
            godMode = true,
            okCallback = function()
                GameManager:unload()
            end,
        })

    else
        zc.tipInfo(errorMsg)
    end


    return errorMsg, forceLogout

end

--[[ HTTP测试
    local successFn = function(data)
        print("callback, my status is: " .. data.status)
    end
    local failFn = function()
        print("something bad happend.")
    end
    zc.http('http://localhost/http/index.php?c=ios&m=update_world', {name = "solo"}, successFn, failFn)--nil
]]--

function zc.refreshServerAPI()
    local rd = math.random(1, #SERVERPORTS)
    GAMEAPI = "http://" .. SERVERADDR .. ":" .. SERVERPORTS[rd] .. "/zc_game?"
end

-- 请求当前登录的服务器
function zc.loadUrl(api, params, successCallback, failCallback, isSilent, timeout, cancelCallback)
    zc.refreshServerAPI()

    local url = GAMEAPI .. api .. "&"

    return zc.http(url, params, successCallback, failCallback, isSilent, cancelCallback, timeout)
end

-- 重新连接
function zc.reconnectDoneCallback(data)
    -- hide
    zc.hideLoading()
    -- 重置音乐
    audio.stopMusic(true)

    -- 关闭所有POP
    zc.closeAllPops()


end

function zc.reconnect()
    zc.showLoading()
    scheduler.performWithDelayGlobal(function()
        PlayerData:init(zc.reconnectDoneCallback)
    end, 0.1)
end


-----------------------网络请求部分  end-----------------------------------


-----------------------进入游戏(登陆或者异常流程重新连接的)-----------------------------------
function zc.continueGame(  )
    print("continueGame")

    zc.switchScene("src/app/scene/mainScene")

end

-----------------------从游戏内退出(退出sdk，清理用户信息)-----------------------------------
-- 退出状态
zc.logoutRestart = false -- 0:正常退出 1.重启退出

-- 退出(检查平台)
function zc.logout(bRestart)

    -- 重置音乐
    audio.stopMusic(true)

    -- 重启
    if bRestart then
        zc.logoutRestart = bRestart
    end

    -- 重置loading网络判断
    zc.getPop("src/app/pop/smallLoadingPop").resetState()


    -- 普通版本，直接调用退出
    if LOGIN_MODE == kLoginModeNormal then -- 普通
        zc.logoutProcess()
    elseif LOGIN_MODE == kLoginModeZCSDK then -- ZCSDK
        if(device.platform == "android") then
            callJava("logout", {}, "()V")
        else
            callOc("logout", {})
        end
    elseif LOGIN_MODE == kLoginModeInApp then -- InApp
        zc.logoutProcess()
    elseif LOGIN_MODE == kLoginModeLiveSDK then -- 直播SDK
        zc.logoutProcess()
    end
end

-- 退出的真正流程
function zc.logoutProcess()
    -- 清理所有数据、请求、公共节点等

    zc.closeAllPops()

    zc.cleanPlayer()

    zc.cancelAllRequest()

    zc.cleanLastPayOrder()

    zc.cleanFileLoader()

    zc.cleanGameManager()

    -- 清理登录状态
    GAMEAPI = ""
    GAMEUID = 0
    GAMETOKEN = ""
    SERVERID = "0"
    -- SDK登录信息
    APP_SDK_INFO = nil


    if LOGIN_MODE == kLoginModeInApp then -- InApp

        if zc.logoutRestart == true then
            zc.switchScene("src/app/scene/loginScene")
        else
            -- 跳转到直播app
            if(device.platform == "android") then
                callJava("exitGame", {}, "()V")
            else
                cc.Director:getInstance():getEventDispatcher():dispatchCustomEvent("EVENT_GAME_EXIT")
            end
        end
    else

        if zc.logoutRestart == true then
            zc.switchScene("src/app/scene/restartScene")
        else
            zc.switchScene("src/app/scene/loginScene")
        end
    end


    zc.logoutRestart = false

end



function zc.stopPlayer()

    -- 清理CD
    if(PlayerData ~= nil and PlayerData.base ~= nil) then
        PlayerData:stop()
    end
end

function zc.cleanPlayer()
    -- 清理当前玩家信息
    if(PlayerData ~= nil and PlayerData.base ~= nil) then
        PlayerData:clean()
    end
end

function zc.cleanFileLoader()
    zc.fileLoader:clean()
end

function zc.cleanGameManager()
    if GameManager then
        GameManager:clean()
    end
end

-----------------------从游戏内退出 end-----------------------------------
-- TODO 需要做的层级比confirm低
function zc.showPayTip()
    zc.showLoading({name = "pay"})
end

function zc.hidePayTip()
    zc.hideLoading({name = "pay"})
end


----------------------通用支付结果检查------------------------------
--[[
    支付完成回调 - 订单检查
    (主动向后端检查上次订单的状态，如果有完成的，则弹窗告知；如果是月卡，则更新界面)
]]
-- 上次申请支付的订单信息
zc.lastPayOrder = nil -- { id: 1, money: 1, orderId: "", type: 0 }
-- 检查次数（有订单的情况下，超过2次还没有检查到则放弃）
zc.lastPayCheckTimes = 0
-- 检查的请求标志（用于取消）
zc.lastPayRequestUdid = nil
-- 检查的请求定时器handler
zc.lastPayScheduler = nil


-- 创建支付
function zc.createPrepayOrderAndPay(orderParams)
    dump(orderParams, "创建支付订单")

    local SDKinstance = zc.require("src/app/scene/loginSceneSdk")

    SDKinstance.createPrepayOrderAndPay(orderParams)

end

function zc.createLastPayOrder(orderInfo)
    if(orderInfo) then
        zc.lastPayOrder = orderInfo
        cc.UserDefault:getInstance():setStringForKey("lastPayOrder", json.encode(zc.lastPayOrder))
        cc.UserDefault:getInstance():flush()
    end
end

function zc.fetchLastPayOrder()
    local result = nil
    if(zc.lastPayOrder == nil) then
        local localPayOrder = cc.UserDefault:getInstance():getStringForKey("lastPayOrder")
        if(localPayOrder and localPayOrder ~= "") then
            result = json.decode(localPayOrder)
        end

        if(result ~= nil) then
            zc.lastPayOrder = result
        end
    end



    return result
end

function zc.cleanLastPayOrder()
    if(zc.lastPayOrder ~= nil) then
        zc.lastPayOrder = nil
        cc.UserDefault:getInstance():setStringForKey("lastPayOrder", "")
        cc.UserDefault:getInstance():flush()
    end
end

function zc.updateLastPayOrder(params)
    if(zc.lastPayOrder ~= nil) then
        for k,v in pairs(params) do
            zc.lastPayOrder[k] = v
        end
        cc.UserDefault:getInstance():setStringForKey("lastPayOrder", json.encode(zc.lastPayOrder))
        cc.UserDefault:getInstance():flush()
    end
end

-- 通用充值完成后的回调（不完全代表付款成功、不代表充值完成）
function zc.commonPayCallback(status)

    if(status == true) then

        -- 获取最近订单信息。如果内存里没有订单信息，从userdefault里找
        zc.fetchLastPayOrder()

        if(zc.lastPayOrder) then

            print("离开SDK，延时检查支付结果")
            zc.lastPayCheck()
            -- 在recharge里，已经针对月卡做了特殊处理：当检查期间，月卡不可再购买。
        else
            zc.hidePayTip()
        end

    else -- 支付失败（清理lastOrder）

        zc.lastPayClean()
        zc.hidePayTip()

    end
end

-- 检查支付
function zc.lastPayCheck()
    -- 超过检查次数，放弃
    if(zc.lastPayCheckTimes >= 2 or zc.lastPayOrder == nil) then
        zc.stopLastPayCheck()
        zc.hidePayTip()
        return
    end

    -- 检查次数+1
    zc.lastPayCheckTimes = zc.lastPayCheckTimes + 1
    print("开始第" .. zc.lastPayCheckTimes .. "次支付检查")

    zc.lastPayCheckProcess()

end

function zc.lastPayCheckProcess()

    -- 模拟成功
    zc.lastPayScheduler = scheduler.performWithDelayGlobal(function()
        zc.lastPayScheduler = nil
        if not zc.lastPayOrder then
            return
        end

        zc.loadUrl(
            "m=get_order_status",
            { orderid = zc.lastPayOrder.orderId },
            -- 请求成功
            function(data)
                dump(data)

                local resultStatus = tonumber(data.status)
                if(resultStatus == 1) then -- 支付完成

                    zc.lastPayCheckSuccessCallback(data)
                    zc.hidePayTip()

                else -- 没有支付完成，继续检查
                    zc.lastPayCheck()
                end
            end,
            -- 请求失败
            function()
                print("请求失败")
                zc.lastPayCheck()
            end,
            -- 隐藏loading
            true,
            10,
            -- 请求被cancel
            function()
                print("请求cancel")
                zc.lastPayClean()
                zc.hidePayTip()
            end
        )

    end, 1)

end


-- 账单检查成功的处理
function zc.lastPayCheckSuccessCallback(data)

    -- 弹出结果
    local msg = ""

    if (data.gold and tonumber(data.gold) > 0) then
       msg = msg .. zc.lang("common_PayMsg1", "恭喜您充值成功，共获得") .. data.gold .. zc.lang("common_PayMsg2", "钻石。")
    end

    if (data.money and tonumber(data.money) > 0) then
       msg = msg .. zc.lang("common_PayMsg1", "恭喜您充值成功，共获得") .. data.money .. zc.lang("common_PayMsg3", "游戏币。")
    end

    if(data.vip and tonumber(data.vip) > 0) then
        msg = msg .. zc.lang("common_PayMsg4", "恭喜您成为VIP") .. data.vip
    end

    if msg ~= "" then
        zc.confirm({ text = msg })
    end

    zc.updateUserData(data)


    -- 支付统计
    zc.sendDataEvent({
        eventname = "Purchase",
        userid    = zc.lastPayOrder.uid,
        amount    = zc.lastPayOrder.amount,
        orderid   = zc.lastPayOrder.orderId
    })

    -- 停止检查(并清理了数据)
    zc.stopLastPayCheck()
end


-- 检查取消后，状态的清理（有可能是被动触发。例如退出、网络断掉等等）
function zc.lastPayClean()
    zc.cleanLastPayOrder()
    zc.lastPayCheckTimes = 0
    zc.lastPayRequestUdid = nil
    print(zc.lang("common_", "支付检查已被清理"))
end

-- 强制取消支付检查(当重新创建新订单时，在recharge里处理的)
function zc.stopLastPayCheck()
    if(zc.lastPayScheduler) then
        scheduler.unscheduleGlobal(zc.lastPayScheduler)
        zc.lastPayScheduler = nil
    end
    if(zc.lastPayRequestUdid) then
        zc._cleanRequest(zc.lastPayRequestUdid) -- 会触发zc.lastPayClean()
    else
        zc.lastPayClean()
    end
end


---------------------通用支付结果检查 end-------------------------------

