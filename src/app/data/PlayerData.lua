--[[
    核心数据对象：用户信息
    全局静态数据表对象为 DB_STATICTABLE["item"]
]]
PlayerData = nil
local Player = class("Player")

function Player:ctor(  )
	
	self:prepare()
end

-- 登陆获取数据
function Player:init( loginDoneCallback, loginErrorCallback )
	self:prepare()

    -- SDK返回的数据
    
    local name = zc.device.model
    local icon = ""
    local sex = 0

    if APP_SDK_INFO then
        name = APP_SDK_INFO.name or zc.device.model
        icon = APP_SDK_INFO.icon or ""
        sex = tonumber(APP_SDK_INFO.sex) or 0
    end

	zc.loadUrl("m=user_join_game", {
        name = name,
        gender = sex,
        idfa = zc.device.idfa,
        android_id = zc.device.android_id,
        mac = zc.device.mac,
        channel = CHANNELID,
        region = device.region,
        language = zc.language,
        is_guest = 0,
        image = icon,
        ip_str = zc.getIPAddress(), -- IP地址，可能为0.0.0.0
        location = json.encode(zc.getLocationInfo()),
        }, 
        function(data)
            local resultStatus = tonumber(data.status)

            -- 设置httpUdid
            if data.time then
                zc._setHttpUdid(tonumber(data.time))
            end

            if(resultStatus == 10005) then -- 角色不存在

                -- 不存在角色登陆
                zc.continueGame( false )



            elseif(resultStatus == 1) then

                -- 玩家资料信息
                self:initData( data )

                -- 登录完成后的回调
                if(loginDoneCallback ~= nil) then
                    loginDoneCallback()
                end


            else

                if loginErrorCallback then
                    loginErrorCallback(data.status)
                end
            end

        end)

end

-- 所有数据必须在这里初始化
function Player:prepare()

    --[[
        后端数据    
    ]]
	self.base = nil

    self.time = nil

    self.chat_list = {}
    
    self.online_reward = {}

    self.heart_reward = {}

    self.mail_receive_list = {}
end



-- 获取数据
function Player:initData( data )
    -- 开始Socket
    zc.socket.scheduleConnect()

    -- 获取IP地址
    zc._getIPAddress()

    -- 获取位置
    zc.sdkGetLocation()

    -- 语音登陆
    if GotyeApi then
        GotyeApi:login(data.info.base.uid)
    end

    -- 服务器时间
    self.time = data.time

    -- dump(data, "data")
	self.base = data.info.base

    -- 是否在房间内，暂时只能在31服务器
    self.friend_room_id = data.info.friend_room_id
    self.my_invitor = data.info.my_invitor
    self.new_invitor_got = data.info.new_invitor_got
    self.chatList = self:buildChatList( data.info.chat_list )
    self.sysMsgTemplates = self:_buildTempLates()
    self.base.new_guide = {}
    self.base.new_guide["1"] = 4

    self.online_reward = data.info.online_reward
    self.heart_reward = data.info.heart_reward

   -- self.mail_receive_list = data.info.mail_receive_list
    self:buildMailList(data.info.mail_receive_list)
end


function Player:getMailList()

    if table.nums(self.mail_receive_list) == 0 then
        return self.mail_receive_list
    end

    local function sortFunc(obj, obj1)
        return obj.is_read < obj1.is_read
    end
    table.sort(self.mail_receive_list, sortFunc)

    local is_read = self.mail_receive_list[1].is_read
    local tab = {}
    local tab1 = {}
    for key, value in pairs(self.mail_receive_list) do
        if is_read == value.is_read then
            table.insert(tab, value)
        else
            table.insert(tab1, value)
        end
    end

    local function sortFunc(obj, obj1)
        return tonumber(obj.send_time) > tonumber(obj1.send_time)
    end
    table.sort(tab, sortFunc)
    table.sort(tab1, sortFunc)
    self.mail_receive_list = {}
    for k, v in ipairs(tab) do
        table.insert(self.mail_receive_list, v)
    end

    for k, v in ipairs(tab1) do
        table.insert(self.mail_receive_list, v)
    end

    return self.mail_receive_list
end
function Player:buildMailList(data)
    for key, value in pairs(data) do
        table.insert(self.mail_receive_list, value)
    end
   
end


function Player:insertMailList(data)
    for key, value in pairs(self.mail_receive_list) do
        if value.id == data.id then
            return
        end
    end
    table.insert(self.mail_receive_list, data)
end

function Player:updateMail(data)
    for key,value in pairs(self.mail_receive_list) do
        if value.id == data.id then
            for key1, value1 in pairs(data) do
                value[key1] = value1
                break
            end
            break
        end
    end

   
end

function Player:deleteMailList(data)
    for key, value in pairs(self.mail_receive_list) do
        if value.id == data.id then
            table.remove(self.mail_receive_list, key)
            break
        end
    end
end



function Player:updateOnlineReward(params)
    for k,v in pairs(params) do
        self.online_reward[k] = v
    end
end


function Player:updateBase( params )
    
    for k,v in pairs(params) do
        self.base[k] = v
    end

    if params.nickname then
        zc.updateFlashNode(zc.dataTypeNickName)
    end

    if params.image then
        zc.updateFlashNode(zc.dataTypeHeadImage)
    end

    if params.money then
        zc.updateFlashNode(zc.dataTypeMoney)
    end

    if params.gold then
        zc.updateFlashNode(zc.dataTypeGold)
    end

    if params.card then
        zc.updateFlashNode(zc.dataTypeRoomCard)
    end



end

function Player:updateheart_reward( params )
    
    for k,v in pairs(params) do
        self.heart_reward[k] = v
    end
    
    if self.heart_reward.status == 1 then
        zc.createPop("src/app/pop/loveRescuePop")
    end

end

--[[
    初始化时获得列表
]]
function Player:buildChatList( data )
    local chatList = {
        ["1"] = {},
        ["2"] = {},
        ["3"] = {},
        ["4"] = {},
        ["5"] = {},
        ["6"] = {},
        ["7"] = {},
        ["9"] = {},
        ["10"] = {},
    }

    for i = 1, #data do
        local chatItem = self:_buildChatItem(data[i])
        
        table.insert(chatList[tostring(chatItem.type)], chatItem)

    end


    return chatList
end




function Player:_buildChatItem(data)

    local chat = data
    chat["id"] = data.id or 0
    chat["image"] = data.image or 1
    chat["is_self"] = data.is_self or 0
    chat["level"] = data.level or 0
    chat["msginfo"] = data.msginfo
    chat["nick_name"] = data.nick_name
    chat["quick_msg"] = data.quick_msg
    chat["region"] = data.region
    chat["roleid"] = data.roleid
    chat["stringid"] = data.stringid or 0
    chat["time"] = os.date("%X", data.time)
    chat["type"] = data.type or 1
    chat["vip"] = data.vip or 0
    chat["gender"] = data.gender or 0
    return chat
end

-- 按类型清除聊天
function Player:clearChatByType( mType )
    if self.chatList and self.chatList[tostring(mType)] then
        self.chatList[tostring(mType)] = {}
    end

end

function Player:updateChat( data )
    local chatItem = self:_buildChatItem(data)

    table.insert(self.chatList[tostring(chatItem.type)], chatItem)
end

-- 根据聊天类型获取聊天内容
function Player:getChatListByType( mType )
    return self.chatList[tostring(mType)]
end


function Player:getItemCount( itemType, itemId )
    local nowCount = 0

    local itemType = tonumber(itemType) or 0
    local itemId = tonumber(itemId) or 0

    if itemType == zc.dataTypeMoney then -- 金币
        nowCount = self.base.money

    elseif itemType == zc.dataTypeGold then -- 钻石
        nowCount = self.base.gold
        
    elseif itemType == zc.dataTypeID then -- ID
        nowCount = zc.cutRoleId(self.base.roleid) 

    elseif itemType == zc.dataTypeRoomCard then -- 房卡
        nowCount = self.base.card

    end

    return nowCount
end

-- 服务端文字表 
function Player:_buildTempLates(  )
    
  
    local sysMsgTemplates = {}
    local strList, tmp, template, orderList
    for k,v in pairs(DB_STATICTABLE["proto_server_word"]["list"]) do
        strList = string.split(v.content, "#")
        if strList and #strList == 2 then
            orderList = {}
            for i = 1, string.len(strList[2]) do
                table.insert(orderList, tonumber(string.sub(strList[2],i,i)))
            end
            if orderList[1] == 0 then
                orderList = {}
            end
            strList = string.split(strList[1], "*")
            if strList and #strList > 0 then
                template = {}
                for j,v2 in ipairs(strList) do
                    tmp = string.split(v2, "&")
                    if tmp and #tmp > 0 then
                        if tonumber(tmp[1]) == 1 then
                            template[j] = {c = tmp[3], t = 1, s = tmp[2]}
                        else
                            template[j] = 0
                        end
                    else
                        zc.tipInfo(zc.lang("PlayerData_TipInfo", "模版格式错误:")..v)
                    end
                end
            end
            sysMsgTemplates[tonumber(k)] = {template = template, title = v.title, orderList = orderList, image = v.image, name = v.name}
        end
    end
    return sysMsgTemplates
end

function Player:getSysMsgTemplates(  )
    return self.sysMsgTemplates
end

function Player:stop()
    -- 清理CD
    zc.cleanCDAll()

end


-- 退出前，清理数据
function Player:clean()

    self:stop()

    self:prepare()

    zc.socket.stopServerPush()

    print("player clean.")
end


PlayerData = Player.new()
