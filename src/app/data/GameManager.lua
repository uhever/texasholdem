--[[
	游戏管理单例
]]

GameManager = nil

local Manager = class("GameManager")

-- 游戏入口定义
Manager.GAME_NAME_CONFIG = {
	"texasHoldem",
}


-- 所有游戏定义
Manager.GAME_CONFIG = {}

Manager.GAME_CONFIG["texasHoldem"] = {

	orentation = "portrait", -- 转向

	targetScene = "src/app/scene/texasHoldemScene", -- 跳转的Scene 德州扑克
}

function Manager:ctor( params )
	
	-- 游戏单例
	self.gameInstance = nil

	-- 当前游戏配置
	self.gameCfg = nil


end

-- 设置当前Instance
function Manager:setGameInstance( instance )
	self.gameInstance = instance
end

-- 得到当前Instance
function Manager:getGameInstance()
	return self.gameInstance
end

-- 运行模块
function Manager:load( gameName, params )

	params = params or {}


	print("GameManager:load - ", gameName)

	if Manager.GAME_CONFIG[gameName] then

		self.gameCfg = Manager.GAME_CONFIG[gameName]

		-- 设置转向
		display.setOrientation(self.gameCfg.orentation)
		zc.switchScene(self.gameCfg.targetScene, params)
	end

end

-- 退出游戏模块，返回大厅
function Manager:unload(  )
	self:clean()

	zc.switchScene("src/app/scene/mainScene")
end


-- 开始游戏场景
function Manager:startGame( params )

	-- zc.getScene(self.gameCfg.targetScene).startGame(params)
	zc.getScene("src/app/scene/texasHoldemScene").startGame(params)
	

end


-- 结束游戏场景
function Manager:stopGame(  )

	if self.gameInstance and self.gameInstance.clean then
		self.gameInstance:clean()	
	end

	self.gameInstance = nil

end

--[[
	通过GameManger更新Instance ------ start ------
]]

-- 更新游戏数据
function Manager:updateGameData( data )
	if self.gameInstance and self.gameInstance.updateGameData then
		self.gameInstance:updateGameData(data)
	end

end

-- Socket断线重连尝试次数
function Manager:onTCPDisconnect( errNum )
	print("Manager:onTCPDisconnect", errNum)
	if self.gameInstance and self.gameInstance.onTCPDisconnect then
		self.gameInstance:onTCPDisconnect(errNum)
	end
end

-- Socket重连成功
function Manager:onTCPReconnected(  )
	print("Manager:onTCPReconnected")
	if self.gameInstance and self.gameInstance.onTCPReconnected then
		self.gameInstance:onTCPReconnected()
	end
end

-- 切到后台
function Manager:onEventToBackground(  )
	print("Manager:onEventToBackground")
	if self.gameInstance and self.gameInstance.onEventToBackground then
		self.gameInstance:onEventToBackground()
	end
end

-- 切到前台
function Manager:onEventToForeground( interval )
	print("Manager:onEventToForeground")
	if self.gameInstance and self.gameInstance.onEventToForeground then
		self.gameInstance:onEventToForeground(interval)
	end
end


-- 充值成功回调
function Manager:onChargeSuccess( data )
	if self.gameInstance and self.gameInstance.onChargeSuccess then
		self.gameInstance:onChargeSuccess(data)
	end
end

--[[
	兑换成功回调
]]
-- "data" = {
--     "change" = {
--         "base" = {
--             	"uid":1						//uid
-- 				"roleid":1					//角色id
-- 				"nickname":xxx				//昵称
-- 				"image":1					//图像
-- 				"level":1					//等级
-- 				"exp":1						//经验
-- 				"levelupexp":1				//升级经验
-- 				"gold":1					//钻石
-- 				"money":1					//金币
-- 				"vip":1						//vip等级
--         }
--     }
--     "reward" = {
--         1 = {
--             "count" = 315000
--             "flag"  = 0
--             "id"    = 0
--             "type"  = 1
--         }
--     }
--     "status" = 1
-- }
function Manager:onExchangeSuccess( data )
	if self.gameInstance and self.gameInstance.onExchangeSuccess then
		self.gameInstance:onExchangeSuccess(data)
	end
end

--[[
	通过GameManger更新Instance ------ end ------
]]


-- 清理GameManager
function Manager:clean(  )

	display.setOrientation("portrait")

	self.gameCfg = nil

	self:stopGame()

end


GameManager = Manager.new()