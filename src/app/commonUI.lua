
--- 创建Layout
-- @param params pos, size, mask, opacity, listener, name, tag, actionTag,
-- @return Layout实例
-- @usage zc.createLayout({name = "guideDialogReal", size = CCSize(zc.width, zc.height), opacity = 130})
function zc.createLayout( params )
    local layout = ccui.Layout:create()

    if params.scale then
        layout:setScale(params.scale)
    end

    if params.pos then
        layout:setPosition(params.pos)
    end

    if params.ap then
        layout:setAnchorPoint(params.ap)
    end

    if params.size then
        layout:setContentSize(params.size)
    end

    if params.listener then
        layout:setTouchEnabled(true)
        layout:addTouchEventListener(function ( sender, eventType )
            if eventType == ccui.TouchEventType.began then
                -- if params.effect then
                --     zc.playEffect(params.effect)
                -- else
                --     zc.playEffect("click")
                -- end

            end

            params.listener(sender, eventType)

        end)
    end


    if params.mask then
        layout:setBackGroundColorType(ccui.LayoutBackGroundColorType.solid)
        layout:setBackGroundColor(display.COLOR_BLACK)
        layout:setBackGroundColorOpacity(100)
        if params.opacity then
            layout:setBackGroundColorOpacity(params.opacity)
        end
        if params.bgColor then
            layout:setBackGroundColor(params.bgColor)
        end

        if params.touchClose then
            layout:setTouchEnabled(true)
            layout:addTouchEventListener(function ( sender, eventType )
                if eventType == ccui.TouchEventType.ended then
                    sender:removeFromParent(true)
                end
            end)
        end
    end

    -- 触摸吞噬
    if type(params.swallowTouches) == "boolean" then
        layout:setSwallowTouches(params.swallowTouches)
    end

    -- 可点击
    if type(params.touchEnabled) == "boolean" then
        layout:setTouchEnabled(params.touchEnabled)
    end

    if params.name then
        layout:setName(params.name)
    end

    if params.tag then
        layout:setTag(params.tag)
    end

    if params.actionTag then
        layout:setActionTag(params.actionTag)
    end

    if params.zOrder then
        layout:setLocalZOrder(params.zOrder)
    end

    if params.parent then
        params.parent:addChild(layout)
    end


    return layout
end


--- 创建ImageView
-- @param params src, pos, ap, scale, flipX, flipY, scale9, size, name, tag, actionTag, listener, opacity ... shaderObj
-- @return 返回ImageView实例
-- @usage local innerBg = zc.createImageView({plist = 1, src = "scale9_inner_bg.png", scale9 = true, size = CCSizeMake(300, 40), pos = cc.p(235, 115)})
function zc.createImageView( params )
    local imageView = ccui.ImageView:create()
    if params.plist then
        imageView:loadTexture(params.src,params.plist)
    else
        imageView:loadTexture(params.src)
    end

    if params.pos then
        imageView:setPosition(params.pos)
    end

    if params.ap then
        imageView:setAnchorPoint(params.ap)
    end

    if params.flipX then
        imageView:setFlippedX(params.flipX)
    end

    if params.flipY then
        imageView:setFlippedY(params.flipY)
    end
    

    if params.scale then
        imageView:setScale(params.scale)
    end

    if params.scaleX then
        imageView:setScaleX(params.scaleX)
    end

    if params.scaleY then
        imageView:setScaleY(params.scaleY)
    end

    if params.scale9 then
        imageView:setScale9Enabled(true)
        if params.capInsets then
            imageView:setCapInsets(params.capInsets)
        end
    end

    if params.size then
        imageView:setContentSize(params.size)
    end
    if params.name then
        imageView:setName(params.name)
    end

    if params.tag then
        imageView:setTag(params.tag)
    end

    if params.actionTag then
        imageView:setActionTag(params.actionTag)
    end


    if params.shaderObj == 1 then
        local shader = cc.GLProgram:createWithFilenames("res/Shader/SpriteGray.vsh","res/Shader/Sprite.fsh")
        local state  = cc.GLProgramState:create(shader)
        imageView:setGLProgramState(state)
        state:setUniformFloat("time", 0.0)
        imageView.state = state
        imageView.time = 0.0
        imageView.shaderType = 1
    elseif params.shaderObj == 2 then
        local shader = cc.GLProgram:createWithFilenames("res/Shader/SpriteGray.vsh","res/Shader/SpriteHigh.fsh")
        local state  = cc.GLProgramState:create(shader)
        imageView:setGLProgramState(state)
        imageView.shaderType = 2
        imageView.state = state
        state:setUniformFloat("high", 1.0)
        imageView.setTouchEnabled1 = imageView.setTouchEnabled
        imageView.state:setUniformInt("type", 0)
        imageView.setTouchEnabled = function(self, b)
            if b == true then
                imageView.state:setUniformInt("type", 0)
            else
                imageView.state:setUniformInt("type", 1)
            end
            imageView.setTouchEnabled1(imageView, b)
        end
    elseif params.shaderObj == 3 then
        local shader = cc.GLProgram:createWithFilenames("res/Shader/SpriteGray.vsh","res/Shader/Sprite.fsh")
        local state  = cc.GLProgramState:create(shader)
        imageView:setGLProgramState(state)
        imageView.shaderType = 3
        imageView.state = state
        state:setUniformFloat("time", 0.0)
    elseif params.shaderObj == 4 then
        local shader = cc.GLProgram:createWithFilenames("res/Shader/SpriteGray.vsh","res/Shader/spriteGround.fsh")
        local state  = cc.GLProgramState:create(shader)
        imageView:setGLProgramState(state)
        imageView.shaderType = 4
        imageView.state = state
        state:setUniformFloat("time", -180.0)
        imageView.time = -180.0
    end
    if params.listener then
        imageView:setTouchEnabled(true)
        imageView:addTouchEventListener(function ( sender, eventType )
            if eventType == ccui.TouchEventType.began then
                -- 按下之后放大
                if imageView.shaderType == 1 then
                    
                    local nodeScheduler = imageView:getScheduler() -- Node绑定的Scheduler

                    if imageView.labelScheduler then
                        nodeScheduler:unscheduleScriptEntry(imageView.labelScheduler)
                        imageView.labelScheduler = nil
                    end

                    local labelScheduler = nil

                    labelScheduler = nodeScheduler:scheduleScriptFunc(function ( delta )

                        if tolua.isnull(imageView) then
                            nodeScheduler:unscheduleScriptEntry(labelScheduler)
                            return
                        end
                        imageView.time = imageView.time + 0.3
                        if imageView.time > 2.0 then
                            imageView.time = 2.0
                        end
                        imageView:setScale(1 - imageView.time * 0.1 / 2)


                        imageView.state:setUniformFloat("time", imageView.time)
                    end, 0.01, false)
                    imageView.labelScheduler = labelScheduler -- reg
                    
                elseif imageView.shaderType == 2 then
                    imageView.state:setUniformFloat("high", 1.5)

                elseif imageView.shaderType == 3 then
                   imageView.state:setUniformFloat("time", 2.0)

                end

                if type(params.effect) == "string" then
                    if params.effect ~= "" then
                        zc.playEffect(params.effect)
                    end
                else
                    zc.playEffect("click")
                end
                params.listener(sender, eventType)
            elseif eventType == ccui.TouchEventType.moved then
                params.listener(sender, eventType)

            elseif eventType == ccui.TouchEventType.ended or eventType == ccui.TouchEventType.canceled then
                if imageView.shaderType == 2 then
                    imageView.state:setUniformFloat("high", 1.0)

                
                elseif imageView.shaderType == 3 then
                   imageView.state:setUniformFloat("time", 0.0)

                
                elseif imageView.shaderType == 1 then
                    if imageView.labelScheduler  then
                        local nodeScheduler = imageView:getScheduler() 
                        nodeScheduler:unscheduleScriptEntry(imageView.labelScheduler)
                        imageView.labelScheduler = nil
                        imageView.state:setUniformFloat("time", 0.0)
                        imageView.time = 0.0
                        imageView:setScale(1)
                       
                    end
                end
                 params.listener(sender, eventType)
            end


        end)
    end

    -- 触摸吞噬
    if type(params.swallowTouches) == "boolean" then
        imageView:setSwallowTouches(params.swallowTouches)
    end

    -- 可点击
    if type(params.touchEnabled) == "boolean" then
        imageView:setTouchEnabled(params.touchEnabled)
    end

    if params.opacity then
        imageView:setOpacity(params.opacity)
    end

    if params.gray then
        imageView:setGray(params.gray)
    end

    if params.rotate then
        imageView:setRotation(params.rotate)
    end

    if params.zOrder then
        imageView:setLocalZOrder(params.zOrder)
    end

    -- 以长或宽最小值铺满全屏
    if params.fitScreen then
        local scale = zc.width / imageView:getContentSize().width
        if scale < zc.height / imageView:getContentSize().height then
            scale = zc.height / imageView:getContentSize().height
        end
        imageView:setScale(scale)
    end

    -- 图片是否拉伸全屏
    if params.fullScreen then
        imageView:setScaleX(zc.width / imageView:getContentSize().width)
        imageView:setScaleY(zc.height / imageView:getContentSize().height)
    end

    if params.parent then
        params.parent:addChild(imageView)
    end

    return imageView
end



-- 快速创建一个Label
function zc.createLabel( params )

    local label = ccui.Text:create()

    local color = params.color or display.COLOR_WHITE
    local fontSize = params.fontSize or 18
    local fontName = params.fontName or display.FONT1

    label:setColor(color)
    label:setFontSize(fontSize)
    label:setFontName(fontName)

    if params.text then
        label:setString(params.text)
    end

    if params.pos then
        label:setPosition(params.pos)
    end

    if params.ap then
        label:setAnchorPoint(params.ap)
    end

    if params.alignV then -- cc.VERTICAL_TEXT_ALIGNMENT_TOP
        label:setTextVerticalAlignment(params.alignV)
    end

    if params.alignH then
        label:setTextHorizontalAlignment(params.alignH)
    end

    if params.name then
        label:setName(params.name)
    end

    if params.tag then
        label:setTag(params.tag)
    end

    if params.opacity then
        label:setOpacity(params.opacity)
    end

    if params.link then
        if(label.setDrawBottomLine) then
            label:setDrawBottomLine(true)
        end
    end

    if params.shadow then

        local shadowSize = cc.size(2, -2)
        if params.shadowSize then
            shadowSize = params.shadowSize
        end
        local shadowColor = cc.c4b(0, 0, 0, 255)
        if params.shadowColor then
            shadowColor = params.shadowColor
        end

        label:enableShadow(shadowColor, shadowSize)
    end

    if params.outline then
        local outlineSize = 1
        if params.outlineSize then
            outlineSize = params.outlineSize
        end
        local outlineColor = cc.c4b(0, 0, 0, 255)
        if params.outlineColor then
            outlineColor = params.outlineColor
        end
        label:enableOutline(outlineColor, outlineSize)
    end

    if params.glow then -- TODO
        label:enableGlow(cc.c4b(0, 0, 0, 255))
    end

    if params.zOrder then
        label:setLocalZOrder(params.zOrder)
    end

    -- 换行的话
    if params.size then
        label:setTextAreaSize(params.size)
    end


    if params.listener then
       label:setTouchEnabled(true)
       label:addTouchEventListener(function ( sender, eventType )
           if eventType == ccui.TouchEventType.began then
                -- if params.effect then
                --     zc.playEffect(params.effect)
                -- else
                --     zc.playEffect("click")
                -- end
           end
           params.listener(sender, eventType)
       end)
    end

    if params.parent then
        params.parent:addChild(label)
    end


    function label:setActionString( text1, text2, pattern )
        if tolua.isnull(label) then
            return
        end

        text2 = text2 or ""
        pattern = pattern or ""

        local nodeScheduler = label:getScheduler() -- Node绑定的Scheduler

        if label.labelScheduler then
            nodeScheduler:unscheduleScriptEntry(label.labelScheduler)
            label.labelScheduler = nil
        end


        local oldText1 = ""
        local oldText2 = ""

        -- 三位数
        if pattern ~= "" then
            local oldTextTb = string.split(label:getString(), pattern)
            oldText1 = oldTextTb[1]
            oldText2 = oldTextTb[2] or ""
            if text2 == "" then
                text2 = oldText2
            end
        else
            oldText1 = label:getString()
            oldText2 = ""
        end

        local oldValue1 = tonumber(zc.reverseWan( oldText1 )) -- 老的数据
        local oldValue2 = tonumber(zc.reverseWan( oldText2 )) -- 老的数据

        local newValue1 = tonumber(zc.reverseWan( text1 ) ) -- 新数据
        local newValue2 = tonumber(zc.reverseWan( text2 ) ) -- 新数据

        if (oldValue1 == newValue1 or oldValue1 == nil or newValue1 == nil) and (pattern ~= "" and oldValue2 and newValue2 and oldValue2 == newValue2) then
            return
        end


        local ratio = 0

        if newValue1 - oldValue1 < 0 then
            ratio = math.floor((newValue1 - oldValue1) / 15)
        else
            ratio = math.ceil((newValue1 - oldValue1) / 15)
        end


        zc._runTitleAction( label ) -- 播放放大动画

        local curValue1 = oldValue1


        local labelScheduler = nil

        labelScheduler = nodeScheduler:scheduleScriptFunc(function ( delta )
            curValue1 = curValue1 + ratio

            if tolua.isnull(label) then
                nodeScheduler:unscheduleScriptEntry(labelScheduler)
                return
            end

            -- 开始逻辑判断
            if ratio > 0 then -- 增加

                if newValue1 > curValue1 then

                    label:setString(zc.getWan(curValue1, 2) .. pattern .. zc.getWan(newValue2, 2))
                else
                    nodeScheduler:unscheduleScriptEntry(labelScheduler)
                    label:setString(zc.getWan(newValue1, 2) .. pattern .. zc.getWan(newValue2, 2))
                end
            elseif ratio < 0 then -- 减少
                if newValue1 < curValue1 then
                    label:setString(zc.getWan(curValue1, 2) .. pattern .. zc.getWan(newValue2, 2))
                else

                    nodeScheduler:unscheduleScriptEntry(labelScheduler)
                    label:setString(zc.getWan(newValue1, 2) .. pattern .. zc.getWan(newValue2, 2))
                end

            else -- 相同
                nodeScheduler:unscheduleScriptEntry(labelScheduler)
                label:setString(zc.getWan(newValue1, 2) .. pattern .. zc.getWan(newValue2, 2))
            end

        end, 0.01, false)

        label.labelScheduler = labelScheduler -- reg

    end


    function label:stopActionText(  )
        local nodeScheduler = label:getScheduler() -- Node绑定的Scheduler
        if label.labelScheduler then
            nodeScheduler:unscheduleScriptEntry(label.labelScheduler)
            label.labelScheduler = nil
        end
    end

    return label
end

-- 创建一个labelAtlas
function zc.createLabelAtlas( params )
    assert(params.imagName,    "imagName must be include")
    assert(params.fontWidth,   "fontWidth must be include")
    assert(params.fontHeight,  "fontHeight must be include")
    assert(params.firstChar,   "firstChar must be include")

    local text = params.text or 0
    local labelAtlas = cc.LabelAtlas:_create(text,params.imagName, params.fontWidth, params.fontHeight, string.byte(tostring(params.firstChar)))

    if params.tag then
        labelAtlas:setTag(params.tag)
    end

    if params.opacity then
        labelAtlas:setOpacity(params.opacity)
    end

    if params.pos then
        labelAtlas:setPosition(params.pos)
    end

    if params.ap then
        labelAtlas:setAnchorPoint(params.ap)
    end


    if params.parent then
        params.parent:addChild(labelAtlas)
    end

    return labelAtlas

end


-- 创建一个粒子特效
function zc.createParticle( params )
    assert(params.src,    "src must be include")

    local particle = cc.ParticleSystemQuad:create(params.src)

    if params.scale then
        particle:setScale(params.scale)
    end

    if params.remove then
        particle:setAutoRemoveOnFinish(params.remove)
    end

    if params.duration then
        particle:setDuration(params.duration)
    end

    if params.tag then
        particle:setTag(params.tag)
    end

    if params.opacity then
        particle:setOpacity(params.opacity)
    end

    if params.pos then
        particle:setPosition(params.pos)
    end

    if params.ap then
        particle:setAnchorPoint(params.ap)
    end


    if params.parent then
        params.parent:addChild(particle)
    end

    return particle

end

-- 创建一个金币粒子特效
function zc.createCoinParticle( params )
    params.src = "res/Image/main/loginAware/effect/coinEff.plist"
    return zc.createParticle( params )
end


--创建一个滑动条
function zc.createSlider( params )
    assert(params.barsrc,       "barsrc must be include")
    assert(params.slidballsrc,      "slidballsrc must be include")
    assert(params.progressbarsrc,   "progressbarsrc must be include")

    local slider = ccui.Slider:create()

    slider:loadBarTexture(params.barsrc)
    slider:loadSlidBallTextures(params.slidballsrc, params.slidballsrc, "")
    slider:loadProgressBarTexture(params.progressbarsrc)

    if params.pos then
        slider:setPosition(params.pos)
    end

    if params.ap then
        slider:setAnchorPoint(params.ap)
    end

    if params.zOrder then
        slider:setLocalZOrder(params.zOrder)
    end

    if params.rotate then
        slider:setRotation(params.rotate)
    end

    if params.percent then
        slider:setPercent(params.percent)
    end

    if params.tag then
        slider:setTag(params.tag)
    end

    if params.parent then
        params.parent:addChild(slider)
    end

    if params.listener then
        slider:addEventListener(function ( sender, eventType )
            if eventType == ccui.SliderEventType.percentChanged then

            end
            params.listener(sender, eventType)
        end)

    end

    return slider
end



function zc.createCheckBox(params )

    assert(params.boxSrc,       "boxSrc must be include")
    assert(params.selectSrc,      "selectSrc must be include")

    local checkBox = ccui.CheckBox:create()
    --可点击
    if type(params.touchEnabled) == "boolean" then
        checkBox:setTouchEnabled(params.touchEnabled)
    end
    --是否选中
    if type(params.selected) == "boolean" then
        checkBox:setSelected(params.selected)
    end

    if params.pos then
        checkBox:setPosition(params.pos)
    end

    if params.parent then
        params.parent:addChild(checkBox)
    end

    if params.name then
        checkBox:setName(params.name)
    end

    checkBox:loadTextures(params.boxSrc,params.boxSrc,params.selectSrc, "","",0)

    if params.listener then
        checkBox:addEventListener(function ( sender, eventType )
            params.listener(sender, eventType)
        end)
    end

    return checkBox
end


--- 创建Button
-- @param params normalSrc, pressedSrc, size, name, pos, ap, zOrder, listener, tag, actionTag, isDisabled
-- @return 返回Button实例
-- @usage zc.createButton({name = "titleGold", actionTag = zc.dataTypeGold, normal = "Main/mainCity/mainCity_diamond_n.png", <br/>
    -- pressed = "Main/mainCity/mainCity_diamond_h.png", pos = cc.p(0, 20), listener = zc._titleClickCallback})<br/>
function zc.createButton( params )
    local button = ccui.Button:create()
    -- button:setPressedActionEnabled(true)
    -- button:setZoomScale(0.05)

    local fontColor = params.fontColor or display.COLOR_BLACK
    local fontSize = params.fontSize or 26
    local fontName = params.fontName or display.FONT1
    local textOffset = params.textOffset or cc.p(0, 0)

    button:setTitleFontSize(fontSize)
    button:setTitleColor(fontColor)
    button:setTitleFontName(fontName)

    if params.text then
        button:setTitleText(params.text)
    end

    -- 按钮部分
    local normalSrc = "res/Image/record/record_detail_btn_n.png"
    local pressedSrc = "res/Image/record/record_detail_btn_p.png"
    local disabledSrc = ""
    if params.normal and params.pressed then
        normalSrc = params.normal
        pressedSrc = params.pressed
        disabledSrc = params.disabled or ""
    end

    if params.plist then
        button:loadTextures(normalSrc, pressedSrc, disabledSrc, plist)
    else
        button:loadTextures(normalSrc, pressedSrc, disabledSrc)
    end

    if params.size then
        button:setScale9Enabled(true)
        button:setContentSize(params.size)
    end

    if params.name then
        button:setName(params.name)
    end

    if params.pos then
        button:setPosition(params.pos)
    end

    if params.ap then
        button:setAnchorPoint(params.ap)
    end

    if params.zOrder then
        button:setLocalZOrder(params.zOrder)
    end

    if params.scale then
        button:setScale(params.scale)
    end

    if params.scaleX then
        button:setScaleX(params.scaleX)
    end

    if params.scaleY then
        button:setScaleY(params.scaleY)
    end

    if params.listener then
        button:addTouchEventListener(function ( sender, eventType )
            if eventType == ccui.TouchEventType.began then
                if type(params.effect) == "string" and params.effect ~= "" then
                    zc.playEffect(params.effect)
                else
                    zc.playEffect("click")
                end
            end

            params.listener(sender, eventType)


        end)

    end
    if params.tag then
        button:setTag(params.tag)
    end
    if params.actionTag then
        button:setActionTag(params.actionTag)
    end

    if params.isDisabled and params.isDisabled == true then
        button:setTouchEnabled(false)
        button:setBright(false)
    end

    -- 图案
    if params.imgSrc then
        local img = ccui.ImageView:create()
        img:loadTexture(params.imgSrc)
        img:setPosition(cc.p(button:getContentSize().width / 2, button:getContentSize().height / 2))
        button:addChild(img)
    end

    if params.parent then
        params.parent:addChild(button)
    end

    -- if params.richImg then -- 此时为图文混排
    --     -- text 文本，详见createLabel
    --     -- image   src为图片路径    详见createImageView

    --     local attrParams = {
    --         {image = true, src = params.richImg, scale = 0.8 },
    --         {text = params.text, fontSize = params.fontSize, shadow = true}
    --     }
                            
    --     local labelLayout = zc.createAttrLabel(attrParams)
    --     button:addChild(labelLayout)
    --     labelLayout:setPosition(cc.p(30,20))
    -- else
    --     -- 不采用自带的Label，使用UIText
    --     local titleText = ccui.Text:create()
    --     button:addChild(titleText)

    --     titleText:setFontSize(fontSize)
    --     titleText:setColor(fontColor)
    --     titleText:setFontName(fontName)

    --     if params.text then
    --         titleText:setString(params.text)
    --         titleText:enableShadow()
    --     end

    --     titleText:setPosition(cc.p(button:getContentSize().width / 2 + textOffset.x, button:getContentSize().height / 2 + textOffset.y))

    --     function button:setTitleText( ... )
    --         titleText:setString(...)
    --     end

    --     function button:setTitleFontSize( ... )
    --         titleText:setFontSize(...)
    --     end
    -- end
    



    return button
end



--- 创建EditBox
-- 宏定义
--[[
    inputMode
    cc.EDITBOX_INPUT_MODE_ANY = 0
    cc.EDITBOX_INPUT_MODE_EMAILADDR = 1
    cc.EDITBOX_INPUT_MODE_NUMERIC = 2
    cc.EDITBOX_INPUT_MODE_PHONENUMBER = 3
    cc.EDITBOX_INPUT_MODE_URL = 4
    cc.EDITBOX_INPUT_MODE_DECIMAL = 5
    cc.EDITBOX_INPUT_MODE_SINGLELINE = 6

    returnType
    cc.KEYBOARD_RETURNTYPE_DEFAULT = 0
    cc.KEYBOARD_RETURNTYPE_DONE = 1
    cc.KEYBOARD_RETURNTYPE_SEND = 2
    cc.KEYBOARD_RETURNTYPE_SEARCH = 3
    cc.KEYBOARD_RETURNTYPE_GO = 4
    
    inputFlag
    cc.EDITBOX_INPUT_FLAG_PASSWORD = 0
    cc.EDITBOX_INPUT_FLAG_SENSITIVE = 1
    cc.EDITBOX_INPUT_FLAG_INITIAL_CAPS_WORD = 2
    cc.EDITBOX_INPUT_FLAG_INITIAL_CAPS_SENTENCE = 3
    cc.EDITBOX_INPUT_FLAG_INITIAL_CAPS_ALL_CHARACTERS = 4

]]
-- @param params params
-- @return 返回EditBox实例
-- @usage zc.createEditBox({placeHolder = zc.lang("commonHelper_", "输入内容"), ap = cc.p(0, 0.5), size = CCSize(390, 45), pos = cc.p(65, 56),handler = POP.editHandler,fontSize = 24})
function zc.createEditBox( params )

    local bgSrc = params.bgSrc
    local size = params.size or cc.size(160, 35)

    local editBox = ccui.EditBox:create(size, bgSrc)

    local inputMode = params.inputMode or cc.EDITBOX_INPUT_MODE_SINGLELINE -- 默认单行
    local returnType = params.returnType or cc.KEYBOARD_RETURNTYPE_DONE -- 默认DONE
    local fontColor = params.fontColor or display.COLOR_WHITE
    local fontName = params.fontName or display.FONT1
    local placeHolderFontName = params.placeHolderFontName or display.FONT1
    local placeHolderFontSize = params.placeHolderFontSize or params.fontSize
    local placeHolderFontColor = params.placeHolderFontColor or display.COLOR_WHITE

    editBox:setInputMode(inputMode)
    editBox:setReturnType(returnType)
    editBox:setFontColor(fontColor)
    editBox:setFontName(fontName)

    if params.text then
        editBox:setText(params.text)
    end

    if params.pos then
        editBox:setPosition(params.pos)
    end

    if params.ap then
        editBox:setAnchorPoint(params.ap)    
    end

    if params.tag then
        editBox:setTag(params.tag)
    end

    if params.name then
        editBox:setName(params.name)
    end

    if params.fontSize then
        editBox:setFontSize(params.fontSize)
    end

    if params.handler then
        editBox:registerScriptEditBoxHandler(params.handler)
    end

    if params.placeHolder then
        editBox:setPlaceHolder(params.placeHolder)
        editBox:setPlaceholderFontSize(placeHolderFontSize)
        editBox:setPlaceholderFontName(placeHolderFontName)
        editBox:setPlaceholderFontColor(placeHolderFontColor)
    end

    if params.maxLength then
        editBox:setMaxLength(params.maxLength)
    end

    if params.inputFlag then -- cc.EDITBOX_INPUT_FLAG_PASSWORD
        editBox:setInputFlag(params.inputFlag)
    end

    if params.parent then
        params.parent:addChild(editBox)
    end
    
    return editBox

end


-- 创建ProgressTimer
function zc.createProgressTimer( params )

    local src = params.src

    local progressTimer = cc.ProgressTimer:create(cc.Sprite:create(src))

    -- cc.PROGRESS_TIMER_TYPE_RADIAL
    local barType = params.type or cc.PROGRESS_TIMER_TYPE_BAR

    progressTimer:setType(barType)

    if params.midPoint then
        progressTimer:setMidpoint(params.midPoint)
    end

    if params.barChangeRate then
        progressTimer:setBarChangeRate(params.barChangeRate)
    end

    if params.pos then
        progressTimer:setPosition(params.pos)
    end

    if params.percent then
        progressTimer:setPercentage(params.percent)
    end

    if params.scale then
        progressTimer:setScale(params.scale)
    end

    if params.rotate then
        progressTimer:setRotation(params.rotate)
    end

    if params.parent then
        params.parent:addChild(progressTimer)
    end

    if params.tag then
        progressTimer:setTag(params.tag)
    end


    return progressTimer
end

function zc.createScrollView( params )
    local scrollView = ccui.ScrollView:create()

    if params.mask == true then
        scrollView:setBackGroundColorType(ccui.LayoutBackGroundColorType.solid)
        scrollView:setBackGroundColor(display.COLOR_WHITE)
        scrollView:setBackGroundColorOpacity(160)
    end
    
    if params.scrollBarDisEnabled then
        scrollView:setScrollBarEnabled(false)
    end

    
    if params.size then
        scrollView:setContentSize(params.size)
    end

    if params.direction then
        scrollView:setDirection(params.direction) -- ccui.ScrollViewDir.vertical/ccui.ScrollViewDir.horizontal
    end

    if type(params.bounce) == "boolean" then
        scrollView:setBounceEnabled(params.bounce)
    end

    if type(params.inertiaScroll) == "boolean" then
        scrollView:setInertiaScrollEnabled(params.inertiaScroll)
    end

    if params.innerSize then
        scrollView:setInnerContainerSize(params.innerSize)
    end
    
    if params.pos then
        scrollView:setPosition(params.pos)
    end
    
    if params.ap then
        scrollView:setAnchorPoint(params.ap)
    end

    if params.name then
        scrollView:setName(params.name)
    end

    if params.parent then
        params.parent:addChild(scrollView)
    end

    return scrollView
end

function zc.createListView( params )
    local listView = ccui.ListView:create()

    if params.mask == true then
        listView:setBackGroundColorType(ccui.LayoutBackGroundColorType.solid)
        listView:setBackGroundColor(display.COLOR_WHITE)
        listView:setBackGroundColorOpacity(160)
    end

    if params.scrollBarDisEnabled then
        listView:setScrollBarEnabled(false)
    end

    if params.size then
        listView:setContentSize(params.size)
    end

    if params.direction then
        listView:setDirection(params.direction) -- ccui.ScrollViewDir.none/ccui.ScrollViewDir.vertical/ccui.ScrollViewDir.horizontal
    end

    if type(params.bounce) == "boolean" then
        listView:setBounceEnabled(params.bounce)
    end

    if type(params.inertiaScroll) == "boolean" then
        listView:setInertiaScrollEnabled(params.inertiaScroll)
    end
    
    if params.pos then
        listView:setPosition(params.pos)
    end
    
    if params.ap then
        listView:setAnchorPoint(params.ap)
    end

    if params.name then
        listView:setName(params.name)
    end

    if params.margin then
        listView:setItemsMargin(params.margin)
    end

    if params.parent then
        params.parent:addChild(listView)
    end

    return listView
end


--- 创建PageView
-- @param params 
-- @return PageView实例
-- @usage zc.createPageView(params)
function zc.createPageView( params )
    local pageView = ccui.PageView:create()

    if params.mask == true then
        pageView:setBackGroundColorType(ccui.LayoutBackGroundColorType.solid)
        pageView:setBackGroundColor(display.COLOR_WHITE)
        pageView:setBackGroundColorOpacity(160)
    end

    
    if params.size then
        pageView:setContentSize(params.size)
    end

    if params.direction then
        pageView:setDirection(params.direction) -- ccui.ScrollViewDir.vertical/ccui.ScrollViewDir.horizontal
    end

    if type(params.bounce) == "boolean" then
        pageView:setBounceEnabled(params.bounce)
    end

    if type(params.inertiaScroll) == "boolean" then
        pageView:setInertiaScrollEnabled(params.inertiaScroll)
    end
    
    if params.pos then
        pageView:setPosition(params.pos)
    end
    
    if params.ap then
        pageView:setAnchorPoint(params.ap)
    end

    if params.name then
        pageView:setName(params.name)
    end

    if params.eventListener then
        pageView:addEventListener(params.eventListener)
    end

    if params.parent then
        params.parent:addChild(pageView)
    end

    return pageView
end


function zc.createTableView( params )
    local tableView = ccui.TableView:create(params.size)
    tableView:setVerticalFillOrder(cc.TABLEVIEW_FILL_TOPDOWN)
    if params.mask == true then
        tableView:setBackGroundColorType(ccui.LayoutBackGroundColorType.solid)
        tableView:setBackGroundColor(display.COLOR_WHITE)
        tableView:setBackGroundColorOpacity(160)
    end

    if params.scrollBarDisEnabled then
        tableView:setScrollBarEnabled(false)
    end
    
    if params.verticalFillOrder then
        tableView:setVerticalFillOrder(params.verticalFillOrder)
    end

    if params.direction then
        tableView:setDirection(params.direction) -- ccui.ScrollViewDir.vertical/ccui.ScrollViewDir.horizontal
    end

    if params.scrollBarColor then
        tableView:setScrollBarColor(params.scrollBarColor)
    end

    if type(params.bounce) == "boolean" then
        tableView:setBounceEnabled(params.bounce)
    end

    if type(params.inertiaScroll) == "boolean" then
        tableView:setInertiaScrollEnabled(params.inertiaScroll)
    end
    
    if params.pos then
        tableView:setPosition(params.pos)
    end
    
    if params.ap then
        tableView:setAnchorPoint(params.ap)
    end

    if params.name then
        tableView:setName(params.name)
    end

    if params.HANDLER_NUMBER_OF_CELLS_IN_TABLEVIEW then
        tableView:registerScriptHandler(params.HANDLER_NUMBER_OF_CELLS_IN_TABLEVIEW, cc.NUMBER_OF_CELLS_IN_TABLEVIEW)
    end

    if params.HANDLER_TABLECELL_TOUCHED then
        tableView:registerScriptHandler(params.HANDLER_TABLECELL_TOUCHED, cc.TABLECELL_TOUCHED)
    end

    if params.HANDLER_TABLECELL_SIZE_FOR_INDEX then
        tableView:registerScriptHandler(params.HANDLER_TABLECELL_SIZE_FOR_INDEX, cc.TABLECELL_SIZE_FOR_INDEX)
    end

    if params.HANDLER_TABLECELL_SIZE_AT_INDEX then
        tableView:registerScriptHandler(params.HANDLER_TABLECELL_SIZE_AT_INDEX, cc.TABLECELL_SIZE_AT_INDEX)
    end

    if params.zOrder then
        tableView:setLocalZOrder(params.zOrder)
    end

    if params.parent then
        params.parent:addChild(tableView)
    end

    return tableView
end

--[[
    创建一个fntLabel
]]
function zc.createFntLabel(params)
    local params = params or {}
    local label = ccui.TextBMFont:create()
    label:setFntFile(params.fnt or "")
    label:setAnchorPoint(params.ap or cc.p(0.5, 0.5))
    -- label:setAdditionalKerning(params.addwidth or 0)
    label:setPosition(params.pos or cc.p(0, 0))
    label:setLocalZOrder(params.zorder or 1)
    label:setScale(params.scale or 1)

    label:setString(params.text)

    function label:updateInfo( params )
        -- body
        if params.fnt then
            label:setFntFile(params.fnt)
        end
    end
    return label
end


--- 创建LoadingBar
-- @param params src, pos, ap, percent, direction
-- @return LoadingBar实例
-- @usage local expLoadingBar = zc.createLoadingBar({name = "expLoadingBar", plist = 1, src = "loading_bar.png",<br/>
 -- percent = percent, pos = cc.p(headColorImg:getContentSize().width / 2, -17)})
function zc.createLoadingBar( params )
    local loadingBar = ccui.LoadingBar:create()
    local src = params.src or "loading_bar.png"

    loadingBar:loadTexture(src)

    loadingBar:setPosition(params.pos or cc.p(0, 0))
    loadingBar:setAnchorPoint(params.ap or cc.p(0.5, 0.5))
    loadingBar:setPercent(params.percent or 0)
    loadingBar:setDirection(params.direction or LoadingBarTypeLeft)

    -- function loadingBar:setPercentByAction( percent, time, callback, frameTime )
    --     self:stopAllActions()
    --     time = time or 0
    --     frameTime = frameTime or 0.1
    --     local curPercent = self:getPercent()
    --     if curPercent == percent then return end
    --     if time <= 0 then
    --         self:setPercent(percent)
    --     else
    --         local speed = (percent - curPercent)/ time * frameTime
    --         self:runAction(
    --             CCSequence:createWithTwoActions(
    --                 CCRepeat:create(
    --                     CCSequence:createWithTwoActions(
    --                         CCDelayTime:create(frameTime),
    --                         CCCallFunc:create(
    --                             function (  )
    --                                 curPercent = curPercent + speed
    --                                 if (speed > 0 and curPercent > percent) or (speed < 0 and curPercent < percent) then
    --                                     curPercent = percent
    --                                 end
    --                                 self:setPercent(curPercent)
    --                             end
    --                         )
    --                     ),
    --                     math.ceil(time/frameTime) + 1
    --                 ),
    --                 CCCallFuncN:create(
    --                     function (  )
    --                         if callback then
    --                             callback()
    --                         end
    --                     end
    --                 )
    --             )
    --         )
    --     end
    -- end

    if params.rotate then
        loadingBar:setRotation(params.rotate)
    end

    if params.name then
        loadingBar:setName(params.name)
    end

    if params.tag then
        loadingBar:setTag(params.tag)
    end

    -- 可以设置Scale9
    if params.size then
        loadingBar:setScale9Enabled(true)
        loadingBar:setSize(params.size)
    end

    if params.opacity then
        loadingBar:setOpacity(params.opacity)
    end

    if params.parent then
        params.parent:addChild(loadingBar)
    end

    return loadingBar
end


-- 显示提示Tip
function zc.tipInfo( params )

    local mParams = {}

    if type(params) == "string" or type(params) == "number" then
        mParams.text = params
    end

    zc.createPop("src/app/pop/tipPop", mParams)
end



function zc.createAttrLabel( params, extraParams )
    -- 主节点
    local baseLayout = ccui.Layout:create()
    local contentLayout = ccui.Layout:create()
    baseLayout:addChild(contentLayout)

    baseLayout.contentLayout = contentLayout -- reg


    local preItem = nil
    local nowItem = nil
    local maxWidth = 0
    local maxHeight = 0

    for i,v in ipairs(params) do
        
        if v.text then
            local label = zc.createLabel( v )
           
            nowItem = label -- reg

        elseif v.image then
            local img = nil

            if v.type then
                v.src = zc.getAttrResPath( v.type )
                img = zc.createImageView(v)
            elseif v.src then
                img = zc.createImageView(v)
            end

            nowItem = img -- reg

        end

        nowItem:setAnchorPoint(cc.p(0, 0))
        nowItem:setTag(i)

        local offsetX = v.offsetX or 0
        nowItem.offsetX = offsetX -- reg更新使用

        local offsetY = v.offsetY or 0
        nowItem.offsetY = offsetY -- reg更新使用

        if i == 1 then
            nowItem:setPosition(0, 0) -- 第一个值设置offset无用，所有的偏移X都基于前一个值,偏移Y基于第一个值
        else
            nowItem:setPosition(cc.p(preItem:getContentSize().width * preItem:getScale() + preItem:getPositionX() + offsetX, offsetY))
        end

        -- 获得高度（不以offsetY作为执行）
        if nowItem:getContentSize().height * nowItem:getScaleY() > maxHeight then
            maxHeight = nowItem:getContentSize().height * nowItem:getScaleY()
        end


        preItem = nowItem
        contentLayout:addChild(nowItem)

    end

    if extraParams then
        
        if extraParams.ap then -- 内容做偏移
            baseLayout:setAnchorPoint(extraParams.ap)
        end

        if extraParams.pos then
           baseLayout:setPosition(extraParams.pos)
        end

        if extraParams.zOrder then
            baseLayout:setLocalZOrder(extraParams.zOrder)
        end

        if extraParams.parent then
           extraParams.parent:addChild(baseLayout)
        end

    end

    local maxWidth = preItem:getContentSize().width * preItem:getScale() + preItem:getPositionX()

    contentLayout:setPosition(maxWidth * -baseLayout:getAnchorPoint().x, maxHeight * -baseLayout:getAnchorPoint().y)

    baseLayout.maxWidth = maxWidth -- reg
    baseLayout.maxHeight = maxHeight -- reg

    return baseLayout
end


function zc.updateAttrLabel( layout, params, extraParams )

    -- 主节点
    local baseLayout = layout
    local contentLayout = baseLayout.contentLayout

    local preItem = contentLayout:getChildByTag(1)
    local nowItem = contentLayout:getChildByTag(1)
    local maxWidth = baseLayout.maxWidth
    local maxHeight = baseLayout.maxHeight

    for i,v in ipairs(params) do
        nowItem = contentLayout:getChildByTag(i)

        if v.text then
            nowItem:setString(v.text)

            if v.color then
                nowItem:setColor(v.color)
            end
            if v.outline then
                local outlineSize = 1
                if v.outlineSize then
                    outlineSize = v.outlineSize
                end
                local outlineColor = cc.c4b(0, 0, 0, 255)
                if v.outlineColor then
                    outlineColor = v.outlineColor
                end
                nowItem:enableOutline(outlineColor, outlineSize)
            end

        elseif v.image then

            if v.type then
                v.src = zc.getAttrResPath( v.type )
                nowItem:loadTexture(v.src)
            elseif v.src then
                nowItem:loadTexture(v.src)
            end

        end

        local offsetX = nowItem.offsetX or v.offsetX
        nowItem.offsetX = offsetX -- reg

        local offsetY = v.offsetY or 0 -- 默认偏移量
        nowItem.offsetY = offsetY -- reg
       
        if i == 1 then
            nowItem:setPosition(0, 0)
        else
            nowItem:setPosition(preItem:getContentSize().width * preItem:getScale() + preItem:getPositionX() + offsetX, offsetY)
        end

        -- 获得高度
        if nowItem:getContentSize().height * nowItem:getScaleY() > maxHeight then
            maxHeight = nowItem:getContentSize().height * nowItem:getScaleY()
        end

        preItem = nowItem

    end

    -- 
    if extraParams then
        
        if extraParams.ap then -- 内容做偏移
            baseLayout:setAnchorPoint(extraParams.ap)
        end

        if extraParams.pos then
           baseLayout:setPosition(extraParams.pos)
        end

    end

    local maxWidth = preItem:getContentSize().width * preItem:getScale() + preItem:getPositionX()

    contentLayout:setPosition(maxWidth * -baseLayout:getAnchorPoint().x, maxHeight * -baseLayout:getAnchorPoint().y)

    baseLayout.maxWidth = maxWidth -- reg
    baseLayout.maxHeight = maxHeight -- reg

end


--[[
    游戏内UI组件
]]
function zc.createHeadIcon( params )
    params.shapeType = params.shapeType or 0
    params.src = "res/Image/rolehead/role_head_0.png"

    local layout = zc.createLayout({
            pos = params.pos,
            parent = params.parent,
            scale = params.scale,
        })

    local headIcon = nil
    if params.shapeType == 1 then --1：圆 其它：方
        -- zc.createImageView({
        --     src = "res/Image/rolehead/role_head_box_1.png",
        --     pos = cc.p(0, 0),
        --     parent = layout
        -- })

        local circleDrawNode = cc.Sprite:create("res/Image/rolehead/head_mask_bg_1.png")
        local clippingNode = cc.ClippingNode:create(circleDrawNode)
        clippingNode:setPosition(0, 0)
        clippingNode:setInverted(false)
        clippingNode:setAlphaThreshold(0.5)
        layout:addChild(clippingNode)

        params.pos = nil
        params.parent = clippingNode
        params.scale = nil
        headIcon = zc.createImageView(params)
    else
        params.parent = layout
        params.pos = cc.p(0, 0)
        headIcon = zc.createImageView(params)
        if params.border then
            -- 名字背景
            zc.createImageView({
                src = "res/Image/rolehead/role_head_box.png",
                pos = cc.p(headIcon:getContentSize().width / 2, headIcon:getContentSize().height / 2),
                parent = headIcon
            })
        end
        layout.headIcon = headIcon
    end

    layout.loadTexture = function(self, img)
        if not tolua.isnull(headIcon) then
            headIcon:loadTexture(img)
        end
    end
    
    zc.updateHeadIcon(headIcon, params)

    return layout
end

-- 更新图标
function zc.updateHeadIcon(headIcon, params)

    if tolua.isnull(headIcon) then
        return
    end

    if type(tonumber(params.image)) == "number" then
        headIcon:loadTexture("res/Image/rolehead/role_head_" .. params.image .. ".png")
    elseif type(params.image) == "string" then
        -- 网络图片
        if string.find(params.image, "http") then

            -- MD5作为文件名
            local md5Str = md5.sumhexa(params.image)

            local downloadPath = "tmp/head/" .. md5Str .. ".jpg"

            if cc.FileUtils:getInstance():isFileExist(downloadPath) then
                
                headIcon:loadTexture(downloadPath)
            else

                zc.simpleDownload({
                    url = params.image,
                    path = downloadPath,
                    callback = function (  )
                        if not tolua.isnull(headIcon) then
                            headIcon:loadTexture(downloadPath)
                        end
                    end
                    })
            end
        end

    end
    
end


function zc.simpleDownload(params)

    local callback = params.callback
    local url = params.url
    local path = params.path or ""

    local request = cc.XMLHttpRequest:new()

    request.timeout = 5

    -- request.responseType = cc.XMLHTTPREQUEST_RESPONSE_ARRAY_BUFFER
    -- request.responseType = cc.XMLHTTPREQUEST_RESPONSE_STRING
    -- request.responseType = cc.XMLHTTPREQUEST_RESPONSE_JSON
    request.responseType = cc.XMLHTTPREQUEST_RESPONSE_BLOB
    -- request.responseType = cc.XMLHTTPREQUEST_RESPONSE_DOCUMENT
    

    request:open("GET", url)

    print("发送开始 - downloadFunc", url)


    local function onReadyStateChanged(  )
        if request.readyState == 4 and (request.status >= 200 and request.status < 207) then
            print("发送结束 - downloadFunc", url)

            --保存到可写目录下的download目录
            -- local saveURI =  "download" .. VERSION_CACHE_ID .. "/" .. path

            -- os.prepareDir(saveURI)
            os.prepareDir(path)

            -- 新版本
            local response = request.response

            io.writefile(device.writablePath .. path, response)

            if callback then callback() end
        end

        request:unregisterScriptHandler()
    end

    request:registerScriptHandler(onReadyStateChanged)
    request:send()

end


--- 提示框抖动
-- @param node 要抖动的节点
-- @param callback 动作结束后的回调
function zc.elasticInAction(widget, callback, maxScale)
    if widget ~= nil then
        widget:setScale(0.3)
        local scale = 1
        if maxScale then
            scale = maxScale
        end
        local action1 = cc.ScaleTo:create(0.15, scale)
        local action2 = cc.ScaleTo:create(0.1, scale - 0.1)
        local action3 = cc.ScaleTo:create(0.1, scale)

        local actionArray = {action1, action2, action3}

        if(callback ~= nil) then
            table.insert(actionArray, cc.CallFunc:create(callback))
        end

        widget:runAction(cc.Sequence:create(actionArray))
    end
end


function zc._runTitleAction( title )
    local titleArray = {
        cc.ScaleTo:create(0.1, 1.5),
        cc.DelayTime:create(0.1),
        cc.ScaleTo:create(0.2, 1.0),
    }
    title:runAction(cc.Sequence:create(titleArray))
end

-- -- 弹力进入动画
-- function zc.elasticInAction( widget )
--     widget:setScale(0.1)

--     local scaleAct = cc.EaseBounceOut:create(cc.ScaleTo:create(1, 1))

--     widget:runAction(scaleAct)
-- end


local maskInstance = nil
--- 显示一个全屏遮罩，吞噬触摸
-- @param delayTime 持续时间
-- @param opacity 透明度(100)
-- @param rect 显示区域
-- @see zc.hideMask
-- @usage zc.showMask(0.5, 180, CCRect(0, 0, 100, 100))
function zc.showMask(delayTime, opacity)

    -- 持续时间
    -- local delayTime = delayTime or 0
    -- 透明度
    local opacity = opacity or 0

    local touchLayer = zc.getTouchLayer()

    local maskWidget = ccui.Widget:create()
    maskWidget:setLocalZOrder(zc.Z_ORDER_MASK)
    touchLayer:addChild(maskWidget)

    local maskLayout = zc.createLayout({mask = true, opacity = opacity, size = cc.size(zc.width, zc.height)})
    maskLayout:setTouchEnabled(true)
    maskWidget:addChild(maskLayout)


    if delayTime then -- 这个会自动清除
        local action = {}
        action[#action+1] = cc.DelayTime:create(delayTime)
        action[#action+1] = cc.CallFunc:create(function ()
            maskWidget:removeFromParent(true)
        end)
        maskWidget:runAction(cc.Sequence:create(action))
    else -- 这个是单例
        zc.hideMask()
        maskInstance = maskWidget -- reg
    end

end

-- 滑动按钮,位置暂时固定，只用于来趣
--[[
    bg 父控件
    index  进入显示按钮
    tabsSrcTable 按钮图片
    callBackTable 回调表
    fixPos 按钮位置调整 BOOL默认为小窗口，传YES为大窗口
]]
function zc.createScrollTabBtns(bg,index,tabsSrcTable,callBackTable,fixPos)
    local tabImageTable = {}

    local scrollViewSize = cc.size(650, 100)
    local scrollViewPos = cc.p(35, bg:getContentSize().height - 30)
    if fixPos then
        scrollViewSize = cc.size(698, 100)
        scrollViewPos = cc.p(61, bg:getContentSize().height - 10)
    end

    local scrollView = zc.createScrollView({
            inertiaScroll = true,
            bounce = true,
            direction = ccui.ScrollViewDir.horizontal,
            size = scrollViewSize,
            innerSize = cc.size(200 * (#tabsSrcTable - 1) + 253, 100),
            pos = scrollViewPos,
            ap = cc.p(0,1),
            scrollBarDisEnabled = true,
            parent = bg
            })
     local function tabBtnCallBack(sender, eventType )
        if eventType == ccui.TouchEventType.ended then
            local tag = sender:getTag()
            if index == tag then
                print("même")
                return 
            end
            print("deference")
            for i = 1, #tabsSrcTable do
                if i == tag then
                    index = i
                    sender:loadTexture(tabsSrcTable[i][2])
                    sender:setZOrder(#tabsSrcTable)
                else
                    tabImageTable[i]:loadTexture(tabsSrcTable[i][1])
                    tabImageTable[i]:setZOrder(#tabsSrcTable - i)
                end
            end
            if callBackTable then callBackTable(tag) end
        end
    end
    
    for i = 1, #tabsSrcTable do
        tabImageTable[i] = zc.createImageView({
            src = (i == index) and tabsSrcTable[i][2] or tabsSrcTable[i][1],
            pos = (#tabsSrcTable == 1) and cc.p(335 ,50)  or ((#tabsSrcTable ~= 2) and cc.p(125 + 200 * (i - 1), 50) or cc.p(200 + 250 * (i - 1) ,50)),
            listener = tabBtnCallBack,
            tag = i,
            parent = scrollView,
            })
        if i == index then
            tabImageTable[i]:setZOrder(#tabsSrcTable)

        else
            tabImageTable[i]:setZOrder(#tabsSrcTable - i)
        end 
    end

end

--- 关闭全屏遮罩
-- @see zc.showMask
function zc.hideMask(  )
    if maskInstance and tolua.isnull(maskInstance) == false then
        maskInstance:removeFromParent(true)
    end

    maskInstance = nil

end

--显示提示条 只需要调用zc.addTips
zc.tipsData = {}
zc._isInShowTips = false

function zc.showTips()
    if not zc.tipsData or #zc.tipsData <= 0 then return end
    if zc._isInShowTips then return end

    zc._isInShowTips = true
    local tipsText = "" .. zc.tipsData[1]
    table.remove(zc.tipsData, 1)

    local layout = zc.createLayout({
        size = cc.size(zc.width, 127),
        ap = cc.p(0.5, 0.5),
        parent = zc.getTouchLayer(),
        pos = cc.p(zc.width/2, zc.height-63),
        zOrder = 9999,
        })
    local bg = zc.createImageView({
        src = "res/Image/common/tipsBg.png",
        pos = cc.p(zc.width/2, 127/2),
        parent = layout,
        scale9 = true,
        capInsets = cc.rect(10, 35, 100, 100),
        size = cc.size(zc.width, 127),
        })
    local textLabel = zc.createLabel({
        text = tipsText or "",
        color = cc.c3b(255, 255, 255),
        outline = true,
        alignH = cc.TEXT_ALIGNMENT_CENTER,
        ap = cc.p(0.5, 0.5),
        pos = cc.p(zc.width/2+15, 127/2),
        fontSize = 30,
        parent = layout,
        })
    local hornIcon = zc.createImageView({
        src = "res/Image/common/horn.png",
        parent = layout,
        pos = cc.p(zc.width/2-(textLabel:getContentSize().width/2)+15, 127/2),
        ap = cc.p(1, 0.5),
        })

    layout:setOpacity(0)
    local action = {}
    action[#action+1] = cc.FadeTo:create(0.5, 255)
    action[#action+1] = cc.DelayTime:create(3)
    action[#action+1] = cc.FadeTo:create(0.5, 0)
    action[#action+1] = cc.CallFunc:create(function()
        layout:removeFromParent(true)
        zc._isInShowTips = false
        if #zc.tipsData > 0 then
            zc.showTips()
        end
    end)
    layout:runAction(cc.Sequence:create(action))
end

function zc.addTips(tipsText)
    if not tipsText then return end
    table.insert(zc.tipsData, tipsText)
    zc.showTips()
end

function zc.clearTips()
    zc._isInShowTips = false
    zc.tipsData = {}
end
