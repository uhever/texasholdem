--[[
	德州扑克扑克牌
]]

local Poker = class("Poker", ccui.Layout)

-- 扑克类型
Poker.POKER_TYPE_HEITAO   = 4
Poker.POKER_TYPE_HONGTAO  = 3
Poker.POKER_TYPE_MEIHUA   = 2
Poker.POKER_TYPE_FANGKUAI = 1

-- 当前牌面
Poker.POKER_BACK  = 0
Poker.POKER_FRONT = 1


function Poker:ctor( params )

	-- 牌类型
	self.type = params.type or Poker.POKER_TYPE_HEITAO
	-- 牌id
	self.id = params.id or 2

	-- 正反面
	self.direction = params.direction or Poker.POKER_BACK

	-- 牌面
	self.pokerImg = nil

	self.backRes = "res/Image/game/poker_back.png"
	self.maskRes = "res/Image/game/poker_mask.png"

	self:init()
end


function Poker:init(  )
	self.pokerImg = zc.createImageView({
		src = "res/Image/game/poker_back.png",
		zOrder = 1,
		parent = self
		})

	local selectedImg = zc.createImageView({
		src = "res/Image/game/eye_poker.png",
		parent = self.pokerImg,
		pos = cc.p(self.pokerImg:getContentSize().width /2,40),
		tag = 11,
		})
	selectedImg:setVisible(false)

	local lightImg = zc.createImageView({
		src = "res/Image/game/lightPokerImg.png",
		parent = self.pokerImg,
		pos = cc.p(self.pokerImg:getContentSize().width /2,self.pokerImg:getContentSize().height /2+3),
		scale9 = true,
		size = cc.size(110,148),
		tag = 12,
		})
	lightImg:setVisible(false)
	--保险选择高亮框
	local insuranceLightImg = zc.createImageView({
		src = "res/Image/game/insurance/check_green_img.png",
		parent = self.pokerImg,
		pos = cc.p(self.pokerImg:getContentSize().width /2,self.pokerImg:getContentSize().height /2+3),
		scaleX = 1.6,
		scaleY = 1.6,
		tag = 13,
		})
	insuranceLightImg:setVisible(false)
end

function Poker:update( updateInfo )

	if updateInfo.type then
		self.type = updateInfo.type
	end

	if updateInfo.id then
		self.id = updateInfo.id
	end

end

-- 获取正面资源
function Poker:_getFrontRes(  )
	return "res/Image/game/poker/poker_" .. self.type .. "_" .. self.id .. ".png"
end


function Poker:_flip( fType )
	self.pokerImg:loadTexture(fType == Poker.POKER_FRONT and self:_getFrontRes() or self.backRes)
	self.direction = fType
end

function Poker:_flipAction( fType )

	local seq = {
		cc.ScaleTo:create(0.1, 0, 1),
		cc.CallFunc:create(function (  )
			self:_flip( fType )
		end),
		cc.ScaleTo:create(0.1, 1, 1),
	}

	self.pokerImg:runAction(cc.Sequence:create(seq))

end

-- 添加遮罩
function Poker:addPokerMask(  )
	if self.pokerImg:getChildByTag(33) then 
		self.pokerImg:getChildByTag(33):setOpacity(150)
		return 
	end
	zc.createImageView({
		src = self.maskRes,
		pos = cc.p(self.pokerImg:getContentSize().width / 2, self.pokerImg:getContentSize().height / 2),
		parent = self.pokerImg,
		opacity = 150,
		tag = 33,
		})
end

-- 添加遮罩
function Poker:removePokerMask(  )
	if self.pokerImg:getChildByTag(33) then
		self.pokerImg:getChildByTag(33):setOpacity(0)
	end
end


-- 显示牌
function Poker:show(  )
	self:_flip( Poker.POKER_FRONT )
end

function Poker:showWithAction(  )
	self:_flipAction( Poker.POKER_FRONT )
end

function Poker:hide(  )
	self:_flip( Poker.POKER_BACK )
end

function Poker:hideWithAction(  )
	self:_flipAction( Poker.POKER_BACK )
end


return Poker