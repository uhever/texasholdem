--[[
	玩家
]]
local Player = class("Player", ccui.Layout)


local PLAYER_STATE_SAT_0          = 0 -- 已坐下
local PLAYER_STATE_READY_1        = 1 -- 已准备
local PLAYER_STATE_WAIT_2         = 2 -- 等待中
local PLAYER_STATE_OPT_3          = 3 -- 操作中
local PLAYER_STATE_DROP_4         = 4 -- 已弃牌
local PLAYER_STATE_COMPARE_LOSE_5 = 5 -- 比牌输
local PLAYER_STATE_COMPARE_EQUAL_6 = 6 -- 比牌一致
local PLAYER_STATE_WIN_7          = 7 -- 赢牌
local PLAYER_STATE_NEXT_8        = 8 -- 等待下一局
local PLAYER_STATE_WATCH_9  	= 9 -- 观战中
local PLAYER_STATE_ALLIN_10  	= 10 -- allin

local PLAYER_STATE_OPT_1 = 1 	--保险模式操作中
local PLAYER_STATE_WAIT_2 = 2	--保险模式等待
local PLAYER_STATE_BINGO_3 = 3	--保险模式买中
local PLAYER_STATE_VAIN_4 = 4	--保险模式没买中



local IS_BANKER_NO_0  = 0
local IS_BANKER_YES_1 = 1

local PAY_TYPE_DEFAULT_0			= 0 -- 底注
local PAY_TYPE_SMALL_BLIND_1		= 1 -- 小盲注
local PAY_TYPE_BIG_BLIND_2			= 2 -- 大盲注
local PAY_TYPE_CHECK_3				= 3 -- 过牌
local PAY_TYPE_ADD_4				= 4 -- 加注
local PAY_TYPE_FOLLOW_5				= 5 -- 跟注
local PAY_TYPE_ALLIN_6				= 6 -- 跟注

local GAME_STATE_WAIT = 0
local GAME_STATE_START = 1
local GAME_STATE_ON = 2
local GAME_STATE_THREE = 3
local GAME_STATE_TURN = 4
local GAME_STATE_RIVER = 5
local GAME_STATE_END = 6


local JOIN_TYPE_NORMAL = 0 --正常加入 原来就在牌桌
local JOIN_TYPE_JOIN = 1 --第一次加入
local JOIN_TYPE_REJOIN = 2 -- 断线重连
local JOIN_TYPE_SIT_DOWN = 3 -- 坐下

local POKER_POS_OFFSET = {
	{cc.p(-49, -20), cc.p(49, -20)},
	{cc.p(-20, -10),  cc.p(-25, -15)},
	{cc.p(-20, -10),  cc.p(-25, -15)},
	{cc.p(-20, -10),  cc.p(-25, -15)},
	{cc.p(-20, -10),  cc.p(-25, -15)},
	{cc.p(20, -10), cc.p(25, -15)},
	{cc.p(20, -10), cc.p(25, -15)},
	{cc.p(20, -10),  cc.p(25, -15)},
	{cc.p(20, -10),  cc.p(25, -15)},
}

local POKER_ROTATE_OFFSET = {
	{0,0},
	{-20,-30},
	{-20,-30},
	{-20,-30},
	{-20,-30},
	{20,30},
	{20,30},
	{20,30},
	{20,30},
}

local POKER_SHOW_POS_OFFSET = {
	{cc.p(-49, -20), cc.p(49, -20)},

	{cc.p(-25, -2),  cc.p(25, -2)},
	{cc.p(-25, -2),  cc.p(25, -2)},
	{cc.p(-25, -2),  cc.p(25, -2)},
	{cc.p(-25, -2),  cc.p(25, -2)},

	{cc.p(-25, -2),  cc.p(25, -2)},
	{cc.p(-25, -2),  cc.p(25, -2)},
	{cc.p(-25, -2),  cc.p(25, -2)},
	{cc.p(-25, -2),  cc.p(25, -2)},

}


local UNREADY_OPACITY = 120

local POKER_SCALE = {0.9,0.4,0.4,0.4,0.4,0.4,0.4,0.4,0.4,0.4}
local POKER_SHOW_SCALE = {0.9,0.65,0.65,0.65,0.65,0.65,0.65,0.65,0.65,0.65}

-- 状态栏配置
local STATUS_LAYOUT_CONFIG = {
	{POS_OFFSET = cc.p(70, 20), AP = cc.p(0, 0), FLIP_X = -1}, -- 自己（无作用

	{POS_OFFSET = cc.p(25, 15), AP = cc.p(0, 0), FLIP_X = -1},
	{POS_OFFSET = cc.p(25, 15), AP = cc.p(0, 0), FLIP_X = -1},
	{POS_OFFSET = cc.p(25, 15), AP = cc.p(0, 0), FLIP_X = -1},
	{POS_OFFSET = cc.p(25, 15), AP = cc.p(0, 0), FLIP_X = -1},

	{POS_OFFSET = cc.p(-25, 15), AP = cc.p(1, 0), FLIP_X = 1},
	{POS_OFFSET = cc.p(-25, 15), AP = cc.p(1, 0), FLIP_X = 1},
	{POS_OFFSET = cc.p(-25, 15), AP = cc.p(1, 0), FLIP_X = 1},
	{POS_OFFSET = cc.p(-25, 15), AP = cc.p(1, 0), FLIP_X = 1},

}


--牌型偏移配置
local POKER_TYPE_POS_OFFSET = {
	cc.p(0, -110),
	cc.p(0, 80),
	cc.p(0, 80),
	cc.p(0, 80),
	cc.p(0, 80),

	cc.p(0, 80),
	cc.p(0, 80),
	cc.p(0, 80),
	cc.p(0, 80),
}

--加时偏移配置
local OVERTIME_POS_OFFSET = {
	cc.p(-245,-5),
	cc.p(50,50),
	cc.p(50,50),
	cc.p(50,50),
	cc.p(50,50),
	cc.p(-50,50),
	cc.p(-50,50),
	cc.p(-50,50),
	cc.p(-50,50),

}

--礼物偏移配置
local GIFT_POS_OFFSET = {
	cc.p(0, 0),
	cc.p(-55, 50),
	cc.p(-55, 50),
	cc.p(-55, 50),
	cc.p(-55, 50),
	cc.p(55, 50),
	cc.p(55, 50),
	cc.p(55, 50),
	cc.p(55, 50),
	cc.p(55, 50),
}
local Z_ORDER_INSURANCE_LAYOUT = 60 -- 保险

local swiitchName = {nil,2,3,4,5,6,7,8,9,10,"J","Q","K","A"}

-- 聊天位置 

function Player:ctor( params )
	print("德州玩家进入房间 Player:ctor")
	self.ground = params.ground

	self.pos = params.pos or cc.p(0, 0)
	-- 座位号，用于前端UI显示（需根据自身位置转换后端发回seat）
	self.seatIndex = params.seatIndex or 1
	-- 扑克牌
	self.cards = params.cards or {}
	-- 筹码

	self.money = params.money or 0
	-- 金币
	self.gold = params.gold or 0
	-- 头像id
	self.image = params.image or 1
	-- 是否庄家
	self.is_banker = params.is_banker or 0
	-- 昵称
	self.nickname = params.nickname or ""
	-- 当前压注
	self.pay_money = params.pay_money or 0
	-- 国家
	self.region = params.region or ""
	-- id
	self.roleid = params.roleid or 0
	-- 服务器记录的位置
	self.seat = params.seat or 1
	-- 是否显示牌
	self.show_cards = params.show_cards or {}
	-- 状态
	self.state = params.state or -1
	-- 状态持续时间
	self.state_time = params.state_time or 0
	-- VIP
	self.vip = params.vip or 0

	self.insurance_state = params.insurance_state or 0
	self.insurance_state_time = params.insurance_state_time or 0
	self.operatorTime = self.insurance_state_time
	self.insuranceTimer = nil --购买保险倒计时
	self.round_money = params.round_money or 0

	self.self_money = params.self_money or 0

	if params.join_type and params.money and params.join_type == JOIN_TYPE_JOIN and self:isSelf() then
		local str = string.format(zc.lang("Player_Tip", "成功兑换%d筹码,退出房间返还对应金币"),zc.getWan(params.money,4))
		zc.tipInfo(str)
	end

	--[[
		UI
	]]
	-- 进度框
	self.progressImg = nil
	self.progressTimer = nil

	-- 玩家名字
	self.roleNameLabel = nil

	-- 头像
	self.headImg = nil

	-- 玩家点数
	self.chipLabel = nil
	self.pokerTypeBg = nil 	--牌类型

	self.operate_effect = nil

	self.allinEffect = nil

	--[[
		管理上层界面
	]]

	-- 扑克列表
	self.pokerList = {}
	--赢了img
	self.winImg = params.winImg

	-- 是否看牌、弃牌等
	self.statusLayout = params.statusLayout
	self.statusBg = nil
	self.statusLabel = nil

	self.roundChipLabel = nil

	self.lastSingleMoney = 0
	self.singleMoney = params.single_money or 0
	self.win_rate = params.win_rate or 0

	self.total_match = params.total_match or 0
	self.state_tips = params.state_tips or 0
	self:init()

end

-- 更新UI
function Player:update( updateInfo )
	if updateInfo.seatIndex and updateInfo.pos then
		self.seatIndex = updateInfo.seatIndex
		self.pos = updateInfo.pos

		-- 更新statuslayout
		self:updateStatusLayoutUI()
		
		-- 更新庄家
		if self.ground.dealerImg:isVisible() then
			if IS_BANKER_YES_1 == self.is_banker then
				self.ground:updateBanker(self.seatIndex)
			end
		end
		--如果seatindex变了，一起更新牌的位置
		if self.pokerList and #self.pokerList > 0 then
			self:sortPokers( true )
		end
		--更新下注的筹码pos
		self.ground:updateChipImgBg(nil)
		--更新牌桌筹码p
		self.ground:updateSelfDeskBet(self.seatIndex,self.seat)
	end

	-- 扑克牌
	if updateInfo.cards then
		
		if #self.cards ~= 2 then  
			self.cards = updateInfo.cards
			if self.pokerList and #self.pokerList > 0 then 
				for i,v in ipairs(self.cards) do
					self.pokerList[i]:update({type = math.floor(v / 100), id = v % 100})
				end
				self:flipCardsAction()
			end
		end	

		if not self:isSelf() then
			self.ground:updatePlayerPoker(self.roleid)
		end
	end
	local changeMoney = 0
	local changeSelfMoney = 0
	-- 金币变化
	if updateInfo.base_info and updateInfo.base_info.money then

		-- 筹码减少特效 (打赏)
		-- if self.money >  updateInfo.base_info.money and updateInfo.pay_type == 2  then
		-- 	self.ground:createChipAction(self.seatIndex, self.money - updateInfo.base_info.money, updateInfo.pay_type)
		-- end
		changeMoney = updateInfo.base_info.money - self.money
		self.money = updateInfo.base_info.money
		self.chipLabel:setString(zc.getWan(self.money,4))
	end

	--钻石
	if updateInfo.base_info and updateInfo.base_info.gold then
		self.gold = updateInfo.base_info.gold
	end

	--玩家金币总数
	if updateInfo.base_info and updateInfo.base_info.self_money then
		changeSelfMoney = self.self_money - updateInfo.base_info.self_money
		self.self_money = updateInfo.base_info.self_money
	end

	if math.abs(changeSelfMoney) == math.abs(changeMoney) and changeSelfMoney ~= 0 and changeMoney ~= 0 and self:isSelf() then
		local str = string.format(zc.lang("Player_Tip", "成功兑换%d筹码,退出房间返还对应金币"),zc.getWan(math.abs(changeSelfMoney),4))
		zc.tipInfo(str)
	end

	-- 庄家
	if updateInfo.is_banker then
		self.is_banker = updateInfo.is_banker
		if self.is_banker == IS_BANKER_YES_1 then
			self.ground:updateBanker(self.seatIndex)
		end
	end

	-- 当前押注
	if updateInfo.pay_money then
		self.pay_money = updateInfo.pay_money
	end

	-- 展示牌
	if updateInfo.show_cards then
		self.show_cards = updateInfo.show_cards
		if self.pokerList and #self.pokerList > 0 then
			self:showCardsAction()
		end
	end

	-- 当前操作盈亏
	if updateInfo.single_money then
		-- 筹码
		if self.singleMoney > updateInfo.single_money  then
			self.ground:createChipAction(self.seatIndex, self.singleMoney - updateInfo.single_money, 1,self.seat)
		elseif self.singleMoney < updateInfo.single_money and self.ground.dataPool.state == 6 then
			self.ground:createWinNumAction(self.seatIndex,self.singleMoney,updateInfo.single_money)
		end

		self.lastSingleMoney = self.singleMoney
		self.singleMoney = updateInfo.single_money
	end
	--本轮下注筹码
	if updateInfo.round_money then

		if updateInfo.round_money ~= 0 and  self.round_money > updateInfo.round_money then
			self.ground:createChipAction(self.seatIndex,self.round_money - updateInfo.round_money,2)
		end
		self.round_money = updateInfo.round_money
		self.ground:updateChipImgBg(self.seatIndex,self.round_money)
	end

	if updateInfo.overtime then
		self.overtimeBg:setVisible(true)
	 	-- 重新开启loading
		if self.state == PLAYER_STATE_WAIT_2 or self.state == PLAYER_STATE_OPT_3 then
			self.state_time = updateInfo.overtime
			if true then
				self:startProgress( self.state_time )
			end
		else
			self:stopProgress(  )
		end
	end

	-- 状态
	if updateInfo.state then

		self.state = updateInfo.state
		self.overtimeBg:setVisible(false)
		self.operate_effect:setVisible(false)

		if self.allinEffect and self.allinEffect:isVisible() then
			self.allinEffect:removeFromParent(true)
			self.allinEffect = nil 
		end

		-- 开启loading
		if (self.state == PLAYER_STATE_WAIT_2 or self.state == PLAYER_STATE_OPT_3) and updateInfo.state_time then -- 开始计时
			self.state_time = updateInfo.state_time
			if true then
				self:startProgress( self.state_time )
			end
		else
			self:stopProgress(  )
		end

		-- 弃牌
		if self.state == PLAYER_STATE_DROP_4 then
			zc.playEffect("dz_fold_1")
			self:dropPokerAction()
		end

		--平
		if self.state == PLAYER_STATE_COMPARE_EQUAL_6 then
			if not self:isSelf() then
				self.winImg:setPosition(self.pos.x,self.pos.y-10)
				self.winImg:setScale(0.4)
				self.winImg:setVisible(true)
				self.winImg:getAnimation():playWithIndex(0)
			else
				-- 胜利特效
				zc.playEffect("dz_win")
				self.winImg:setScale(1)
				self.winImg:setVisible(true)
				self.winImg:setPosition(zc.cx, 420)
			    self.winImg:getAnimation():playWithIndex(0)
			end
		end

		--赢
		if self.state == PLAYER_STATE_WIN_7  then
			if not self:isSelf() then
				self.winImg:setPosition(self.pos.x,self.pos.y-10)
				self.winImg:setScale(0.4)
				self.winImg:setVisible(true)
				self.winImg:getAnimation():playWithIndex(0)
			else
				-- 胜利特效
				zc.playEffect("dz_win")
				self.winImg:setVisible(true)
				self.winImg:setScale(1)
				self.winImg:setPosition(zc.cx, 420)
			    self.winImg:getAnimation():playWithIndex(0)
			end
		end
		self:updateStatusLayout()
	end

	if updateInfo.insurance_state_time then
		self.insurance_state_time = updateInfo.insurance_state_time
		self.operatorTime = self.insurance_state_time
	end
	if updateInfo.insurance_state then
		self.insurance_state = updateInfo.insurance_state
		self:updateInsuranceState()
	end

	--牌置灰
	if self.ground.dataPool.state == 6 then
		if self.state == 2 then
			for i,v in ipairs(self.pokerList) do
				if not tolua.isnull(v) then
					v:addPokerMask()
				end
			end
		end
	end

	--音效和显示
	if updateInfo.pay_type then
		if updateInfo.pay_type == 3 then
			self.statusLayout:setVisible(true)
			self.statusBg:loadTexture("res/Image/game/status_follow_bg.png")
		 	self.statusLabel:setString(zc.lang("Player_Pass", "过牌"))
		 	self.statusLabel:setColor(cc.c3b(39,94,62))
		 	self.statusLabel:setPositionY(self.statusBg:getContentSize().height / 2 + 15)
		 	zc.playEffect("cheackSound")
		elseif updateInfo.pay_type ==4 then
			self.statusLayout:setVisible(true)
			self.statusBg:loadTexture("res/Image/game/status_add_bg.png")
		 	self.statusLabel:setString(zc.lang("Player_Filling", "加注"))
		 	self.statusLabel:setColor(cc.c3b(105,78,30))
		 	self.statusLabel:setPositionY(self.statusBg:getContentSize().height / 2 + 15)
		 	zc.playEffect("dz_add")
		elseif updateInfo.pay_type == 5 then
			self.statusLayout:setVisible(true)
			self.statusBg:loadTexture("res/Image/game/status_follow_bg.png")
		 	self.statusLabel:setString(zc.lang("Player_Following", "跟注"))
		 	self.statusLabel:setColor(cc.c3b(39,94,62))
		 	self.statusLabel:setPositionY(self.statusBg:getContentSize().height / 2 + 15)
		 	zc.playEffect("dz_follow_1")
		elseif updateInfo.pay_type == 6 then
			self.statusLayout:setVisible(true)
			self.statusBg:loadTexture("res/Image/game/status_allin_bg.png")
		 	self.statusLabel:setString("ALL IN")
		 	self.statusLabel:setPositionY(self.statusBg:getContentSize().height / 2 + 20)
		 	self.statusLabel:setColor(cc.c3b(255,255,255))
		 	zc.playEffect("dz_allin_1")
		 	--allin 特效
			local allinEffect, allinAnimation = display.getAnimation("effect_all_in", 1)
		   	allinEffect:setPosition(cc.p(-1,0))
		   	allinEffect:setScale(1.1)
		   	allinEffect:setBlendFunc(cc.blendFunc(gl.ONE, gl.ONE))
		   	if self:isSelf() then
				allinEffect:setPosition(cc.p(-300,-60))
			end
		   	self:addChild(allinEffect)
			display.playAnimationForever( allinEffect,allinAnimation)
			self.allinEffect = allinEffect

		 else
			if self.state ~= PLAYER_STATE_DROP_4 then
				self.statusLayout:setVisible(false)
			end
		end
	end

	-- --总胜率
	if updateInfo.win_rate then
		self.win_rate = updateInfo.win_rate
	end
	--总局数
	if updateInfo.total_match then
		self.total_match = updateInfo.total_match
	end

end

function Player:init(  )

	self.roleNameLabel = zc.createLabel({
		text =  self.nickname,
		shadow = true,
		color = cc.c3b(130, 170, 195),
		pos = cc.p(0, 70),
		parent = self,
		fontSize = 24,
		})
	self.roleNameLabel:setString(zc.subName(self.nickname))


	-- 头像
	self.headImg = zc.createImageView({
		src = "res/Image/game/game_head_icon.png",
		parent = self,
		})

	zc.createHeadIcon({
		image = self.image,
		parent = self.headImg,
		shapeType = 1,
		pos = cc.p(self.headImg:getContentSize().width / 2, self.headImg:getContentSize().height / 2),
		listener = function ( sender, eventType )
			if eventType == ccui.TouchEventType.ended then
				zc.createPop("src/app/pop/dzRoleInfoPop", {
					roleid = self.roleid,
					money = self.money,
					statistics = self.ground.dataPool.statistics,
				})
			end
		end
		})

	local chipLabelBg = zc.createImageView({
		src = "res/Image/game/chip_bg.png",
		pos = cc.p(0,  - 80),
		parent = self
		})

	if self:isSelf() then
		self.headImg:setPosition(cc.p(-300,-60))
		self.roleNameLabel:setVisible(false)
		chipLabelBg:setPosition(cc.p(0,80))
	end

	self.chipLabel = zc.createLabel({
		text = zc.getWan(self.money,4),
		shadow = true,
		fontSize = 24,
		color = cc.c3b(147,164,184),
		pos = cc.p(chipLabelBg:getContentSize().width/2, chipLabelBg:getContentSize().height/2),
		parent = chipLabelBg,
		})

	self.pokerTypeBg = zc.createImageView({
		src = "res/Image/game/pokerType_bg.png",
		pos = POKER_TYPE_POS_OFFSET[self.seatIndex],
		parent = self,
		scaleX = 1.2,
		})
	if self:isSelf() then
		self.pokerTypeBg:loadTexture("res/Image/main/player_bet_bg.png")
	end

	self.pokerTypeBg:setVisible(false)
	self.pokerTypelabel = zc.createLabel({
		text = " ",
		fontSize = 22,
		color = cc.c3b(57,80,102),
		pos = POKER_TYPE_POS_OFFSET[self.seatIndex],
		parent = self,
		})

	--加时
	self.overtimeBg = zc.createButton({
		text = zc.lang("Player_AddTime", "加时"),
		fontSize = 20,
		normal = "res/Image/game/add_time_bg.png",
		pressed = "res/Image/game/add_time_bg.png",
		pos = OVERTIME_POS_OFFSET[self.seatIndex],
		parent = self
		})
	self.overtimeBg:setVisible(false)

	self.progressImg = zc.createProgressTimer({
		src = "res/Image/game/timer_other_bg.png",
		type = cc.PROGRESS_TIMER_TYPE_RADIAL,
		percent = 0,
		parent = self,
		})
	self.progressImg:setLocalZOrder(1)
	if self:isSelf() then
		self.progressImg:setPosition(cc.p(-300,-60))
	end

	local operate_effect, operateAnimation = display.getAnimation("effect_operate", 1)
   	self:addChild(operate_effect)
   	operate_effect:setLocalZOrder(1)
	display.playAnimationForever( operate_effect,operateAnimation)
	operate_effect:setVisible(false)
	self.operate_effect = operate_effect
	-- if self.state == PLAYER_STATE_SAT_0 or self.state == PLAYER_STATE_COMPARE_LOSE_5 or self.state == PLAYER_STATE_NEXT_10 then
	-- 	self.headImg:setOpacity(UNREADY_OPACITY)
	-- end
	--断线重连 自己面前押注筹码

	self.ground:updateChipImgBg(self.seatIndex,self.round_money)
	--断线重连 更新庄家
	if IS_BANKER_YES_1 == self.is_banker then
		self.ground:updateBanker(self.seatIndex)
	end

	if self.state == PLAYER_STATE_OPT_3 and self.state_time - 1 > 0 then -- 开始计时
		if true then
			self:startProgress( self.state_time - 1 )
		end
	else
		self:stopProgress(  )
	end

	self:initStatusLayout()

	self:updateStatusLayout()

	--保险模式 断线重连
	-- if self.insurance_state ~= 0 then
	-- 	self:updateInsuranceState()
	-- end

end

function Player:updateInsuranceState()
	if self.insurance_state == 1 then
		--弹出购买保险提示
		if self:isSelf() then
			--倒计时
			if self.operatorTime and self.operatorTime > 0 then
				if self.insuranceTimer then
					scheduler.unscheduleGlobal(self.insuranceTimer)
					self.insuranceTimer = nil
				end
				self.insuranceTimer = scheduler.scheduleUpdateGlobal(function ( delta )
					if nil == self.operatorTime then
						return
					end
					self.operatorTime = self.operatorTime - delta
					if self.operatorTime <= 0 then
						scheduler.unscheduleGlobal(self.insuranceTimer)
						self.insuranceTimer = nil
					end
				end)
			end
			self.ground:initinsuranceLayout()
		end
	elseif self.insurance_state == 2 then
		if self:isSelf() then
			if self.ground.insuranceSelected then
				self.insuranceSelected = {}
			end
			if self.ground.insuranceLayout then
				self.ground.insuranceLayout:removeFromParent()
				self.ground.insuranceLayout = nil
			end
			if zc.popIsActive( "src/app/pop/buyInsurancePop" ) then
				zc.closePop("src/app/pop/buyInsurancePop")
			end
		end
	elseif self.insurance_state == 4 and self.ground.deskPokers and #self.ground.deskPokers > 0 then
		if self:isSelf() then
			local posX,posY = self.ground.deskPokers[#self.ground.deskPokers]:getPosition()
			local buy_succeed = zc.createImageView({
				src = "res/Image/game/insurance/buy_succeeds.png",
				pos = cc.p(posX,  posY + 120),
				scale = 0.1,
				zOrder = Z_ORDER_INSURANCE_LAYOUT,
				parent = self.ground
				})
				local action = {}
				action[#action+1] = cc.ScaleTo:create(0.3, 1)
				action[#action+1] = cc.DelayTime:create(0.8)
				action[#action+1] = cc.CallFunc:create(function (  )
					buy_succeed:removeFromParent()
				end)
				buy_succeed:runAction(cc.Sequence:create(action))
		else
			local buy_succeed = zc.createImageView({
				src = "res/Image/game/insurance/buy_succeeds.png",
				pos = cc.p(self.pos.x, self.pos.y+30),
				scale = 0.1,
				zOrder = Z_ORDER_INSURANCE_LAYOUT,
				parent = self.ground
				})

			local action = {}
			action[#action+1] = cc.ScaleTo:create(0.3, 1)
			action[#action+1] = cc.DelayTime:create(0.8)
			action[#action+1] = cc.CallFunc:create(function (  )
				buy_succeed:removeFromParent()
			end)
			buy_succeed:runAction(cc.Sequence:create(action))

		end
	elseif self.insurance_state == 3 and self.ground.deskPokers and #self.ground.deskPokers > 0 then
		if self:isSelf() then
			local posX,posY = self.ground.deskPokers[#self.ground.deskPokers]:getPosition()
			local buy_faild = zc.createImageView({
				src = "res/Image/game/insurance/buy_faild.png",
				pos = cc.p(posX,  posY + 90),
				scale = 0.1,
				zOrder = Z_ORDER_INSURANCE_LAYOUT,
				parent = self.ground
				})
			local action = {}
			action[#action+1] = cc.ScaleTo:create(0.3, 1)
			action[#action+1] = cc.DelayTime:create(0.8)
			action[#action+1] = cc.CallFunc:create(function (  )
				buy_faild:removeFromParent()
			end)
			buy_faild:runAction(cc.Sequence:create(action))
		else
			local buy_faild = zc.createImageView({
				src = "res/Image/game/insurance/buy_faild.png",
				pos = cc.p(self.pos.x, self.pos.y),
				scale = 0.1,
				zOrder = Z_ORDER_INSURANCE_LAYOUT,
				parent = self.ground
				})

			local action = {}
			action[#action+1] = cc.ScaleTo:create(0.3, 1)
			action[#action+1] = cc.DelayTime:create(0.8)
			action[#action+1] = cc.CallFunc:create(function (  )
				buy_faild:removeFromParent()
			end)
			buy_faild:runAction(cc.Sequence:create(action))
		end
	end
end


--更新牌型
function Player:updatePokerType(_pokerType)
	if #self.cards < 2 then return end
	self.pokerTypeBg:setVisible(true)
	self.pokerTypelabel:setString(_pokerType)
	if not self:isSelf() then
		self.roleNameLabel:setVisible(false)
	end
end

function Player:initStatusLayout(  )

	local posX = self.pos.x + STATUS_LAYOUT_CONFIG[self.seatIndex]["POS_OFFSET"].x
	local posY = self.pos.y + STATUS_LAYOUT_CONFIG[self.seatIndex]["POS_OFFSET"].y

	-- 看牌、弃牌等状态
	self.statusLayout:setPosition(cc.p(posX, posY))

	self.statusLayout:setVisible(false)

	self.statusBg = zc.createImageView({
		src = "res/Image/game/status_add_bg.png",
		ap = STATUS_LAYOUT_CONFIG[self.seatIndex]["AP"],
		scaleX = STATUS_LAYOUT_CONFIG[self.seatIndex]["FLIP_X"],
		parent = self.statusLayout
		})
	self.statusBg:setPositionY(10)
	if STATUS_LAYOUT_CONFIG[self.seatIndex]["FLIP_X"] == -1 then
		self.statusBg:setPosition(cc.p(self.statusBg:getContentSize().width, 10))
	end

	if self:isSelf() then
		self.statusLayout:setPosition(cc.p(self.statusLayout:getPositionX()-350,self.statusLayout:getPositionY()-90))
	end

	self.statusLabel = zc.createLabel({
		text = zc.lang("Player_LookCards", "看牌"),
		fontSize = 20,
		color = cc.c3b(39, 94, 64),
		pos = cc.p((self.statusBg:getContentSize().width / 2) * -STATUS_LAYOUT_CONFIG[self.seatIndex]["FLIP_X"], self.statusBg:getContentSize().height / 2 + 15),
		parent = self.statusLayout,
		})
end

-- 更新UI
function Player:updateStatusLayoutUI(  )

	local posX = self.pos.x + STATUS_LAYOUT_CONFIG[self.seatIndex]["POS_OFFSET"].x
	local posY = self.pos.y + STATUS_LAYOUT_CONFIG[self.seatIndex]["POS_OFFSET"].y
	self.statusLayout:setPosition(cc.p(posX, posY))

	self.statusBg:setAnchorPoint(STATUS_LAYOUT_CONFIG[self.seatIndex]["AP"])
	self.statusBg:setScaleX(STATUS_LAYOUT_CONFIG[self.seatIndex]["FLIP_X"])

	if STATUS_LAYOUT_CONFIG[self.seatIndex]["FLIP_X"] == -1 then
		self.statusBg:setPosition(cc.p(self.statusBg:getContentSize().width, 10))
	else
		self.statusBg:setPosition(cc.p(0, 10))
	end

	if self:isSelf() then
		self.statusLayout:setPosition(cc.p(self.statusLayout:getPositionX()-350,self.statusLayout:getPositionY()-90))
	end

	self.statusLabel:setPosition(cc.p((self.statusBg:getContentSize().width / 2) * -STATUS_LAYOUT_CONFIG[self.seatIndex]["FLIP_X"], self.statusBg:getContentSize().height / 2 + 15))

end

-- 更新提示框
function Player:updateStatusLayout(  )

	if self.state == PLAYER_STATE_DROP_4 then
		self.statusLayout:setVisible(true)
		self.statusBg:loadTexture("res/Image/game/status_pickup_bg.png")
	 	self.statusLabel:setString(zc.lang("Player_DiscardCard", "弃牌"))
	 	self.statusLabel:setColor(cc.c3b(80,94,123))
	 	self.statusLabel:setPositionY(self.statusBg:getContentSize().height / 2 + 15)
	 else
	 	self.statusLayout:setVisible(false)
	end
end

-- 判断是否自己
function Player:isSelf(  )
	if self.roleid == PlayerData.base.roleid then
		return true
	end
	return false
end

--点击牌 更新亮牌选择状态
function Player:updatePokerSelectType( poker )
	if not self:isSelf() then return end
	if poker.selected == 0 then
		poker.selected = 1
		poker.pokerImg:getChildByTag(11):setVisible(true)
		if self.ground.dataPool.state == GAME_STATE_END then
			zc.socket.sendTable({m = "texas_holdem_show_cards",index = poker.pokerIndex})
		end
	elseif poker.selected == 1 then
		if self.ground.dataPool.state ~= GAME_STATE_END then
			poker.selected = 0
			poker.pokerImg:getChildByTag(11):setVisible(false)
		end
	end
	local showStr = ""
	--增加亮牌提示
	for i,v in ipairs(self.pokerList) do
		if not tolua.isnull(v) and v.selected == 1 then
			showStr = showStr .. " " .. swiitchName[self.cards[i] % 100]
		end
	end
	if string.len(showStr) > 0 and self.ground.dataPool.state ~= GAME_STATE_END then
		zc.tipInfo(zc.lang("Player_EndAndShow", "结束后亮出") .. showStr)
	end

end

-- 发牌（添加）
function Player:dealOnePoker( poker, pokerIndex )

	if tolua.isnull(poker) then return end
	if self.roleid == PlayerData.base.roleid then
		zc.playEffect("dz_deal")
	end
	poker:stopAllActions()
	local moveAct = cc.MoveTo:create(0.2, self:_getCardPos(pokerIndex))
	local scale = POKER_SCALE[self.seatIndex]
	-- if self.ground:getSitBtn():isVisible() == true then
	-- 	scale = POKER_SCALE[2]
	-- end
	
	local endrRotate = POKER_ROTATE_OFFSET[self.seatIndex][pokerIndex]
	local scaleAct = cc.EaseExponentialOut:create(cc.ScaleTo:create(0.2, scale))
	local rotateAct = cc.EaseExponentialOut:create(cc.RotateTo:create(0.2, endrRotate))
	local spawn = cc.Spawn:create({moveAct, scaleAct, rotateAct})
	poker:runAction(spawn)
	poker.pokerIndex = pokerIndex
	poker.selected = 0
	poker.pokerImg:setTouchEnabled(true)
	poker.pokerImg:addTouchEventListener(function ( sender, eventType )
        if eventType == ccui.TouchEventType.ended then
            self:updatePokerSelectType(poker)
        end
    end)

	if self.cards[1] and self.cards[2] then
		if self.cards[1] % 100 == self.cards[2] % 100 then
			for i,v in ipairs(self.pokerList) do
				v.pokerImg:getChildByTag(12):setVisible(true)
			end
		end
	end

end

-- 直接发牌
function Player:dealOnePokerFast( poker )

	if tolua.isnull(poker) then return end
	local scale = POKER_SCALE[self.seatIndex]
	-- if self.ground:getSitBtn():isVisible() == true then
	-- 	scale = POKER_SCALE[2]
	-- end
	poker:setScale(scale)
	poker:setRotation(POKER_ROTATE_OFFSET[self.seatIndex][#self.pokerList+1])
	poker:setPosition(self:_getCardPos(#self.pokerList+1))
	--断线重连 自己有牌的话
	if self.cards[#self.pokerList+1] then
		poker:update({type = math.floor(self.cards[#self.pokerList+1] / 100), id = self.cards[#self.pokerList+1] % 100})
		poker:setRotation(0)
		poker:setScale(POKER_SHOW_SCALE[self.seatIndex])
		poker:setPosition(cc.p(self.pos.x + POKER_SHOW_POS_OFFSET[self.seatIndex][#self.pokerList+1].x, self.pos.y + POKER_SHOW_POS_OFFSET[self.seatIndex][#self.pokerList+1].y))
		poker:show()
	end

	poker.selected = 0
	poker.pokerIndex = #self.pokerList+1
	poker.pokerImg:setTouchEnabled(true)
	poker.pokerImg:addTouchEventListener(function ( sender, eventType )
        if eventType == ccui.TouchEventType.ended then
            self:updatePokerSelectType(poker)
        end
    end)

	if self.cards[1] and self.cards[2] then
		if self.cards[1] % 100 == self.cards[2] % 100 then
			for i,v in ipairs(self.pokerList) do
				v.pokerImg:getChildByTag(12):setVisible(true)
			end
		end
	end

end

-- 扑克排序
function Player:sortPokers( isStopAction )

	if self.seat <= 0 then return end
	if self.seatIndex <= 0 then return end

	for i,v in ipairs(self.pokerList) do
		if not tolua.isnull(v) then
			if isStopAction then
				v:stopAllActions()
			end

			local scale = POKER_SCALE[self.seatIndex]
			-- if self.seat ~= 0 then
			-- 	scale = POKER_SCALE[2]
			-- end
			v:setScale(scale)
			v:setRotation(POKER_ROTATE_OFFSET[self.seatIndex][i])
			v:setPosition(self:_getCardPos(i))
		end
	end
end

-- 获取扑克位置
function Player:_getCardPos( posId )
	local xTable = POKER_POS_OFFSET[self.seatIndex]
	if self.seatIndex and self.pos and self.seatIndex > 0 then
		return cc.p(self.pos.x + xTable[posId].x, self.pos.y + POKER_POS_OFFSET[self.seatIndex][posId].y)
	end
	return cc.p(0,0)
end

-- 开始转圈
function Player:startProgress( time, callback )
	-- 先停止
	self:stopProgress(  )
	local reminder = 0
	local percent = 0
	local tintAct = cc.EaseExponentialIn:create(cc.TintTo:create(time, 255, 0, 0))
	self.progressImg:runAction(tintAct)

	self.progressTimer = scheduler.scheduleUpdateGlobal(function ( delta )

		if nil == self.progressImg then
			return
		end

		if percent >= time then
			self:stopProgress()
			self.operate_effect:setVisible(false)
			self.overtimeBg:setVisible(false)
			if callback then -- 完毕执行回调
				callback()
			end
		else
			percent = percent + delta
			self.progressImg:setPercentage(percent * 100/time )

			if self:isSelf() and reminder == 0 and percent * 100/time > 70 then
				zc.playEffect("dz_reminder")
				reminder = 1
			end

			local radius = 50
		    local _angle = 3.141592 / 180 * (90- percent/time *360)
		    local posx  = radius * math.cos(_angle)
		    local posy  = radius * math.sin(_angle)
		    if self:isSelf() then
		   		posx = posx - 300
		   		posy = posy - 60
		   	end
			--例子特效
			self.operate_effect:setVisible(true)
		   	self.operate_effect:setPosition(cc.p(posx,posy))
		end

	end)
end

-- 停止转圈
function Player:stopProgress(  )
	if self.progressTimer then
		scheduler.unscheduleGlobal(self.progressTimer)
	end
	if self.progressImg then
		self.progressImg:setPercentage(0)
		self.progressImg:setColor(cc.c3b(255, 255, 255))
	end
end

-- 弃牌动画
function Player:dropPokerAction(  )
	if self:isSelf() then
		for i,v in ipairs(self.pokerList) do
			if not tolua.isnull(v) then
				v:addPokerMask()
			end
		end
	else
		for i,v in ipairs(self.pokerList) do
			if not tolua.isnull(v) then
				v:stopAllActions()
				v:_flip(0)
				v:setRotation(0)
				local _cardBasePos = self:_getCardPos(i)
				local moveAct1 = cc.EaseExponentialOut:create(cc.MoveTo:create(0.8, cc.p(zc.cx, zc.cy)))
				local fadeAct1 = cc.FadeOut:create(0.8)
				local spawnAct1 = cc.Spawn:create({moveAct1, fadeAct1})
				v:runAction(spawnAct1)
			end
		end
	end
end

-- 显示扑克动画
function Player:showCardsAction(  )
	self.statusLayout:setVisible(false)
	for i,v in ipairs(self.pokerList) do
		if self.show_cards[tostring(i)]  then
			if not self:isSelf() then
				v:stopAllActions()
				v:update({type = math.floor(self.show_cards[tostring(i)] / 100), id = self.show_cards[tostring(i)] % 100})
				v:setScale(POKER_SHOW_SCALE[self.seatIndex])
				v:setOpacity(255)
				v:setRotation(0)
				v:setPosition(cc.p(self.pos.x + POKER_SHOW_POS_OFFSET[self.seatIndex][i].x, self.pos.y + POKER_SHOW_POS_OFFSET[self.seatIndex][i].y))
			end
			v:showWithAction()
		end
	end
end

-- 看牌动画
function Player:flipCardsAction(  )
	for i,v in ipairs(self.pokerList) do
		v:stopAllActions()
		v:setScale(POKER_SHOW_SCALE[self.seatIndex])
		v:setOpacity(255)
		v:setRotation(0)
		v:setPosition(cc.p(self.pos.x + POKER_SHOW_POS_OFFSET[self.seatIndex][i].x, self.pos.y + POKER_SHOW_POS_OFFSET[self.seatIndex][i].y))
		v:showWithAction()
	end
end

-- 重置所有状态
function Player:resetAll(  )
	self:removePokers()
	self.statusLayout:setVisible(false)

	self.pokerTypeBg:setVisible(false)
	self.pokerTypelabel:setString(" ")

	self.lastSingleMoney = 0
	self.singleMoney = 0
	self.winImg:setVisible(false)
	if self.allinEffect and self.allinEffect:isVisible() then
		self.allinEffect:removeFromParent(true)
		self.allinEffect = nil
	end	
	if not self:isSelf() then
		self.roleNameLabel:setVisible(true)
	end
	--还有名字 筹码都要移除掉，现在没时间写，再说

end


-- 洗牌（删除全部）
function Player:removePokers(  )
	for i,v in ipairs(self.pokerList) do
		v:removeFromParent()
		v = nil
	end
	self.pokerList = {}
	self.cards = {}
end

function Player:clean(  )
	self:stopProgress(  )
end

return Player