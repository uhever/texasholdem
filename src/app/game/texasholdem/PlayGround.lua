--[[
	德州扑克游戏场景
]]
local PlayGround = class("PlayGround", ccui.Layout)

local PokerClass = zc.require("src/app/game/texasholdem/Poker")

local PlayerClass = zc.require("src/app/game/texasholdem/Player")

-- 扑克最大数
local CARD_NUM_LIMIT = 2
-- 座位最大数
local SEAT_NUM_LIMIT = 9
-- 自己默认的位置INDEX
local SEAT_OWN_INDEX = 1

local PLAYER_STATE_SAT_0          = 0 -- 已坐下
local PLAYER_STATE_READY_1        = 1 -- 已准备
local PLAYER_STATE_WAIT_2         = 2 -- 等待中
local PLAYER_STATE_OPT_3          = 3 -- 操作中
local PLAYER_STATE_DROP_4         = 4 -- 已弃牌
local PLAYER_STATE_COMPARE_LOSE_5 = 5 -- 比牌输
local PLAYER_STATE_COMPARE_EQUAL_6 = 6 -- 比牌一致
local PLAYER_STATE_WIN_7          = 7 -- 赢牌
local PLAYER_STATE_NEXT_8        = 8 -- 等待下一局
local PLAYER_STATE_WATCH_9  	= 9 -- 观战中
local PLAYER_STATE_ALLIN_10  	= 10 -- allin



local POKER_TYPE_SINGLE_1         	= 1 	-- 散牌
local POKER_TYPE_ONE_PAIR_2       	= 2 	-- 对子
local POKER_TYPE_TWO_PAIR_3       	= 3 	-- 两对
local POKER_TYPE_THREE_KIND_4     	= 4 	-- 三条
local POKER_TYPE_STRAIGHT_5       	= 5 	-- 顺子
local POKER_TYPE_FLUSH_6          	= 6 	-- 同花
local POKER_TYPE_FULL_HOUSE_7     	= 7 	-- 葫芦
local POKER_TYPE_FOUR_KIND_8 	  	= 8 	-- 四条
local POKER_TYPE_STRAIGHT_FLUSH_9 	= 9 	-- 同花顺
local POKER_TYPE_ROYAL_FLUSH_10 	= 10 	-- 皇家同花顺


--保险模式
local GAME_STATE_WAIT = 0				-- 等待开始
local GAME_STATE_START = 1 				-- 发牌
local GAME_STATE_ON = 2 				-- 游戏中
local GAME_STATE_THREE_INSUREANCE = 3 	-- 翻牌轮保险
local GAME_STATE_THREE = 4 				-- 翻牌轮保险
local GAME_STATE_TURN_INSUREANCE = 5 	-- 转牌轮保险
local GAME_STATE_TURN = 6 				-- 转牌
local GAME_STATE_RIVER_INSUREANCE = 7 	-- 河牌轮保险
local GAME_STATE_RIVER = 8				-- 河牌
local GAME_STATE_INSUREANCE = 9 		-- 进入保险模式
local GAME_STATE_END = 10				-- 游戏结束


--普通模式
local GAME_STATE_WAIT = 0 	--等待开始
local GAME_STATE_START = 1 	--发牌
local GAME_STATE_ON = 2 	--游戏中
local GAME_STATE_THREE = 3 	--翻牌
local GAME_STATE_TURN = 4 	--转牌
local GAME_STATE_RIVER = 5	--河牌
local GAME_STATE_END = 6 	--游戏结束

-- 座位位置
local SEAT_POS = {
	{OUT = cc.p(zc.cx, -150), IN = cc.p(zc.cx, 135)},
	{OUT = cc.p(-80, zc.cy - 220), IN = cc.p(80, zc.cy - 220)},
	{OUT = cc.p(-80, zc.cy +50), IN = cc.p(80, zc.cy +50)},
	{OUT = cc.p(-80, zc.cy + 260), IN = cc.p(80, zc.cy + 260)},
	{OUT = cc.p(-80, zc.cy + 480), IN = cc.p(175, zc.cy + 480)},
	{OUT = cc.p(zc.width + 80, zc.cy + 480), IN = cc.p(zc.width - 175, zc.cy + 480)},
	{OUT = cc.p(zc.width + 80, zc.cy + 260), IN = cc.p(zc.width - 80, zc.cy + 260)},
	{OUT = cc.p(zc.width + 80, zc.cy +50), IN = cc.p(zc.width - 80, zc.cy+50 )},
	{OUT = cc.p(zc.width + 80, zc.cy - 220), IN = cc.p(zc.width - 80, zc.cy - 220)},
}

-- 庄家基于玩家位置便宜
local BANKER_POS_OFFSET = {
	cc.p(130, -60),
	cc.p(85, -70),
	cc.p(85, -70),
	cc.p(85, -70),
	cc.p(85, -70),
	cc.p(-85, -70),
	cc.p(-85, -70),
	cc.p(-85, -70),
	cc.p(-85, -70),
}


local DESK_POKER_POS = {
	cc.p(zc.cx - 170, zc.cy-53),
	cc.p(zc.cx - 85, zc.cy-53),
	cc.p(zc.cx, zc.cy-53),
	cc.p(zc.cx + 85, zc.cy-53),
	cc.p(zc.cx + 170, zc.cy-53),
}

--8个边池位置
local SIDE_POT_POS = {
	cc.p(zc.cx-70,zc.cy+370),
	cc.p(zc.cx+70,zc.cy+370),

	cc.p(zc.cx-70,zc.cy+420),
	cc.p(zc.cx+70,zc.cy+420),

	cc.p(zc.cx-70,zc.cy+470),
	cc.p(zc.cx+70,zc.cy+470),

	cc.p(zc.cx-70,zc.cy+520),
	cc.p(zc.cx+70,zc.cy+520),

}

-- 下注筹码基于玩家位置偏移
local CHIP_POS_OFFSET = {
	cc.p(122, 145),
	cc.p(100, -30),
	cc.p(100, -30),
	cc.p(100, -30),
	cc.p(100, -30),
	cc.p(-100, -30),
	cc.p(-100, -30),
	cc.p(-100, -30),
	cc.p(-100, -30),
}

-- 下注筹码基于玩家位置SCAle
local CHIP_AP_OFFSET = {1,1,1,1,1,-1,-1,-1,-1}
-- 下注飞筹码位置偏移
local CHIP_FLY_POS_OFFSET = {-33,-33,-33,-33,-33,33,33,33,33}

local Z_ORDER_PLAYER = 10 -- 玩家
local Z_ORDER_CHIP = 25 -- 筹码
local Z_ORDER_POKER = 20 -- 扑克
local Z_ORDER_STATUS_LAYOUT = 30 -- 状态(弃牌、看牌等)
local Z_ORDER_MAGIC_ANIMATION = 40 -- 魔法表情
local Z_ORDER_POKER_TYPE = 40 -- 扑克类型
local Z_ORDER_WIN_LABEL = 50 -- 赢了
local Z_ORDER_BIG_CARD_LAYOUT = 100 -- 大牌展示
local Z_ORDER_CONTROL_LAYOUT = 50 -- 控制器
local Z_ORDER_INSURANCE_LAYOUT = 60 -- 保险
local Z_ORDER_VOICE_LAYOUT = 200 -- 语音聊天


local VOICE_LIMIT_TIME = 10


-- 筹码图片，可扩展
local CHIP_COUNTS = {0, 1, 10, 100, 1000, 10000, 100000, 1000000, 10000000, 100000000}

local EMOTION_CONFIG = {4,10,5,2,5,5,2,13,11,8,2,5,2,9,12,2,7,6,2,2,4,9,2,2}
local EMOTION_CONFIG_DELAY = {8,4,6,6,7,10,1,10,6,6,3,6,6,6,6,6,6,9,6,9,4,6,9,6}

function PlayGround:ctor( params )
	self.jsonData = nil

	self.dataPool = {}

	-- 玩家列表
	self.playerList = {}
	self.dataPool.players = nil
	-- 锅底钱目列表
	self.chipPoolList = {} 	--主池和边池的筹码
	self.chipPoolImg = {}	--主池和边池的背景图 
	self.chipPoolLabel = {}	--主池和边池的筹码数量

	--单轮下注列表
	self.CurrentchipsList = {}
	-- 桌面5张牌
	self.deskPokers = {}

	--牌桌筹码
	self.bet_pools = {}

	-- 玩家自己的服务器位置
	self.ownSeatIndex = nil

	-- 当前操作者idx
	self.operatorSeat = 1

	-- 加注限额
	self.limitLabel = nil

	-- 等待Label
	self.waitingLabel = nil

	--牌桌底池总数
	self.potImg = nil

	-- 庄家图片
	self.dealerImg = nil

	-- 控制条
	self.controlBar = nil

	self.maxPokerType = 0

	-- 菜单按钮
	self.menuBtn = nil

	-- 大牌
	self.bigCardLayout = nil

	-- 坐下按钮
	self.sitBtn = nil

	--游戏是否暂停(断线重连用,切换后台)
	self.stop = true

	self.progressTimer = nil --弃牌和过牌按钮倒计时
	self.progressImg = nil -- 弃牌和过牌按钮倒计时图片

	self.type = nil -- 游戏模式（金币房/好友房）

	self.room_type = nil --游戏场次类型
	self.room_id = nil 	--房间号
	self.roomIdLabel = nil 	--房间号label
	self.roomLabel = nil --房间类型
	self.typeLabel = nil --玩法类型
	self.blindLabel = nil --大小盲注

	-- 语音模块
	self.voiceLayout = nil

	--跟注
	self.last_money = 0
	self.cur_money = 0
	self.equal_money = 0

	self.bb = 0 --大盲注
	self.sb = 0 --小盲注

	self.pot = 0 -- 底池

	self.round = 0

	self.add_roleid = 0

	self.finalPoker = {}
	self.RoundLayput = {}

	self.insuranceLayout = nil

	self.insuranceSelected = {}

	self.inviteBtn = nil

	self:init()

	self:createUI()
	if params and params.gameState and params.gameState == 2 then
		zc.socket.sendTable({m = "th_join_room_ok"})
	else
		zc.socket.sendTable({m = "th_join_room_ok"})
	end
end


-- 数据驱动
function PlayGround:updateGameData( gameData, b)
	if gameData and gameData and gameData.texas_holdem then
		if gameData.texas_holdem.room_id then
			self.stop = false
		end
	end

	if self.stop then return end
	if gameData and gameData and gameData.texas_holdem then
		local nowData = gameData.texas_holdem
		--牌桌信息
		if nowData.room_type then
			self.room_type = nowData.room_type
			--self.jsonData = DB_STATICTABLE["proto_texas_holdem"]["list"][tostring(self.room_type)]
		 	self.roomLabel:setString("")

		 	if nowData.room_type > 6 then
		 		--保险模式
				GAME_STATE_WAIT = 0				-- 等待开始
				GAME_STATE_START = 1 			-- 发牌
				GAME_STATE_ON = 2 				-- 游戏中
				GAME_STATE_THREE_INSUREANCE = 3 -- 翻牌轮保险
				GAME_STATE_THREE = 4 			-- 翻牌轮保险
				GAME_STATE_TURN_INSUREANCE = 5 	-- 转牌轮保险
				GAME_STATE_TURN = 6 			-- 转牌
				GAME_STATE_RIVER_INSUREANCE = 7 -- 河牌轮保险
				GAME_STATE_RIVER = 8			-- 河牌
				GAME_STATE_INSUREANCE = 9 		-- 进入保险模式
				GAME_STATE_END = 10				-- 游戏结束
			end

		end

		if nowData.room_id then
			self.room_id = nowData.room_id
		 	self.roomIdLabel:setString(self.room_id .. zc.lang("PlayGround_Info", "号桌"))
		end

		if nowData.type then
			self.type = nowData.type
		end

		if nowData.last_money then
			self.last_money = nowData.last_money 
		end

		if nowData.cur_money then
			self.cur_money = nowData.cur_money 
		end


		--初始化筹码 有的话
		if nowData.bet_pools then
			self.bet_pools = nowData.bet_pools

			--新一轮开始隐藏预选框
			for i,v in ipairs(self.controlBar.autoBtnList) do
				v:setBrightStyle(0)
				v.selected = 0
			end

			if #self.bet_pools > 0 then
		   		if not tolua.isnull(self) then
					self:updateDeskAllChip()
					self:updateRoundMoney()
				end
			end

		end

		--整理筹码中
		if nowData.end_info then
			self:settlementChip(nowData.end_info)
		end

		if nowData.state then
			if nowData.state == GAME_STATE_WAIT then
				self.dataPool.state = nowData.state --头像飘加金币用
			end

		end

		if nowData.chat_room then
			zc.GOTYEROOMID = nowData.chat_room
		end

		--上一个加注的人
		if nowData.add_roleid then
			self.add_roleid = nowData.add_roleid
		end

		--保险模式
		if nowData.insurance then
			self.dataPool.insurance = nowData.insurance
			local canTip = false
			for i,v in ipairs(nowData.insurance) do
				if  zc.isSelf(v.roleid) then
					if v.outs == 0 or v.outs > 15 then
						canTip = true
					end
				end
				
				if canTip then
					zc.tipInfo("outs大于15或outs等于0,不能购买保险")
				end
			end

		end
		
		-- 玩家信息
		if nowData.players then
			if nil == self.dataPool.players then self.dataPool.players = {} end

			table.sort( nowData.players, function ( a,b )
				return a.base_info.roleid == PlayerData.base.roleid
			end )

			-- 先找出自己在服务器哪个位置
			if nil == self.ownSeatIndex then 
				for i,v in ipairs(nowData.players) do
					if v.base_info.roleid == PlayerData.base.roleid then
						self.ownSeatIndex = v.seat
						break
					end
				end
			end

			for i,v in ipairs(nowData.players) do
				
				if v.op_flag == zc.OP_FLAG_NORMAL_0 or v.op_flag == zc.OP_FLAG_NEW_1 then -- 新增
					-- 先添加数据在更新玩家
					table.insert(self.dataPool.players, v)

					-- 添加新玩家
					self:addPlayer(v)

					-- 更新控制器UI
					if v.base_info.roleid == PlayerData.base.roleid then
						self:updateController(v)
					end
			
				elseif v.op_flag == zc.OP_FLAG_UPDATE_2 then -- 更新
					
					for i1,v1 in ipairs(self.dataPool.players) do

						if v1.base_info.roleid == v.base_info.roleid then
							-- 更新控制器UI
							if v.base_info.roleid == PlayerData.base.roleid then
								self:updateController(v)
							end
							self:updatePlayer(v) -- 玩家UI刷新

							break
						end
					end

					for i1,v1 in ipairs(self.dataPool.players) do
					
						if v1.base_info.roleid == v.base_info.roleid then
							-- 先更新数据在更新玩家
							for key,value in pairs(v) do
								if key ~= "base_info" then
									self.dataPool.players[i1][key] = value
								else
									for ii,vv in pairs(value) do
										self.dataPool.players[i1][key][ii] = vv
									end
								end
							end
						end

					end
				elseif v.op_flag == zc.OP_FLAG_DELETE_3 then -- 删除
					for i1,v1 in ipairs(self.dataPool.players) do
						if v1.base_info.roleid == v.base_info.roleid then
							table.remove(self.dataPool.players, i1)
							self:deletePlayer(v.base_info.roleid, true) -- 删除玩家并且删除数据
							break
						end
					end
					
				end
			end
		end

		-- 游戏状态
		if nowData.state then

			if nowData.state == GAME_STATE_WAIT then 	 --游戏准备
				self:resetGround()
			elseif nowData.state == GAME_STATE_START then 	--发牌
				-- 发牌
				self:dealAllPokers()
			elseif nowData.state == GAME_STATE_ON then 	--游戏进行

			elseif nowData.state == GAME_STATE_THREE then 	--发三张牌

			elseif nowData.state == GAME_STATE_TURN then 		--发第四张牌

			elseif nowData.state == GAME_STATE_RIVER then 	--发第5张牌

			elseif nowData.state == GAME_STATE_INSUREANCE then --进入保险模式
				--提示进入保险模式
				zc.tipInfo("进入保险模式")
				for i,v in ipairs(self.controlBar.autoBtnList) do
					v:setVisible(false)
					v:setBrightStyle(0)
					v.selected = 0
				end
			elseif nowData.state == GAME_STATE_END then --游戏结束
				for i,v in ipairs(self.controlBar.autoBtnList) do
					v:setVisible(false)
					v:setBrightStyle(0)
					v.selected = 0
				end

				--发送亮牌消息
				local sendpoker = -1
				local sendNum = 0
				local sendeIndex = 0
				local myInfo = self:getPlayerById(PlayerData.base.roleid)
				if myInfo then
					for i,v in ipairs(myInfo.pokerList) do
						if v.selected == 1 then
							sendNum = sendNum + 1
							sendeIndex = v.pokerIndex
						end
					end
				end
				if sendNum == 1 then
					zc.socket.sendTable({m = "texas_holdem_show_cards",index = sendeIndex})
				elseif sendNum == 2 then
					zc.socket.sendTable({m = "texas_holdem_show_cards",index = 0})
				end
				if myInfo and myInfo.seat ~= 0 then
					-- 显示查看下一圈牌按钮				
					if #self.deskPokers == 0 then
						self.want_lookImg:setVisible(true)
						self.want_lookImg:getChildByTag(33):setString(zc.lang("PlayGround_Flop", "翻牌"))
					elseif #self.deskPokers == 3 then
						self.want_lookImg:setVisible(true)
						self.want_lookImg:getChildByTag(33):setString(zc.lang("PlayGround_TurnCard", "转牌"))
					elseif #self.deskPokers == 4 then
						self.want_lookImg:setVisible(true)
						self.want_lookImg:getChildByTag(33):setString(zc.lang("PlayGround_RiverCard", "河牌"))
					end
				end
			end

			self.dataPool.state = nowData.state
		end

		-- 魔法表情
		if nowData.magic then
			self.dataPool.magic = nowData.magic
			self:runMagicAction()
		end

		if nowData.last_game then
			self.dataPool.last_game = nowData.last_game
		end

		if nowData.statistics then
			self.dataPool.statistics = nowData.statistics
		end

		if nowData.want_look then
			self.dataPool.want_look = nowData.want_look
		end
		--牌桌五张牌
		if nowData.first_three then
			for i=1,#nowData.first_three do
				self:updateDeskPoker(nowData.first_three[i])
			end

			if self.dataPool.state == GAME_STATE_END then
				if nowData.want_look then
					zc.tipInfo(zc.lang("PlayGround_Player", "玩家 ") ..  nowData.want_look .. zc.lang("PlayGround_WantCardInfo", " 想看翻牌圈的牌"))
				end
				self.want_lookImg:setVisible(true)
				self.want_lookImg:getChildByTag(33):setString(zc.lang("PlayGround_TurnCard", "转牌"))
			else
				self:updatePlayerPoker(PlayerData.base.roleid)
			end

		end
		if nowData.fourth then
			self:updateDeskPoker(nowData.fourth)
			if self.dataPool.state == GAME_STATE_END then
				if nowData.want_look then
					zc.tipInfo(zc.lang("PlayGround_Player", "玩家 ") ..  nowData.want_look .. zc.lang("PlayGround_WantTurnCardInfo", " 想看转牌圈的牌"))
				end
				self.want_lookImg:setVisible(true)
				self.want_lookImg:getChildByTag(33):setString(zc.lang("PlayGround_RiverCard", "河牌"))
			else
				self:updatePlayerPoker(PlayerData.base.roleid)
			end

		end
		if nowData.fivth then
			self:updateDeskPoker(nowData.fivth)
			if self.dataPool.state == GAME_STATE_END then
				if nowData.want_look then
					zc.tipInfo(zc.lang("PlayGround_Player", "玩家 ") ..  nowData.want_look .. zc.lang("PlayGround_WantRiverCardInfo", " 想看河牌圈的牌"))
				end
			else
				self:updatePlayerPoker(PlayerData.base.roleid)
			end
		end

		--保险模式
		if nowData.insurance then
			--增加等待购买提示	
			local cantip = false
			local myInsuranceState = self:getPlayerById(PlayerData.base.roleid).insurance_state
			for i,v in ipairs(self.playerList) do
				if v.insurance_state and v.insurance_state == 1 and v.roleid ~= PlayerData.base.roleid  then
					cantip = true
				end
			end
			if myInsuranceState and myInsuranceState == 2 and cantip then
				zc.tipInfo("等待其他玩家购买保险")
			end
			--断线重连 此时所有玩家手牌和桌面牌等信息都加载完毕，因为要得到其他玩家的牌和桌面牌计算outs，所以写在这了
            if nowData.room_id then 
            	local myInfo = self:getPlayerById(PlayerData.base.roleid)
				for i,v in ipairs(self.playerList) do
					if v.roleid == PlayerData.base.roleid then
						v:updateInsuranceState()
						break
					end
				end
			end

		end



		if nowData.bb then
			self.bb = nowData.bb
		end

		if nowData.sb then
			self.sb = nowData.sb
		end

		if nowData.bb and nowData.sb then
		 	self.blindLabel:setString(zc.lang("texasHoldemScene_Blind", "盲注: ") .. self.sb .. "/" .. self.bb)
		end

		if nowData.pot then
			self.pot = nowData.pot
			self:updatePotLabel(nowData.pot)
		end

		if nowData.equal_money then
			self.equal_money = nowData.equal_money 
			self:updateAutoAdd()
		end
	end

end

function PlayGround:getSelfState( )
	local selfState = 0
	for i,v in ipairs(self.playerList) do
		if v.roleid == PlayerData.base.roleid and v.seat ~= 0 then
			selfState = v.state
		end
	end
	return selfState
end

function PlayGround:init( )
	self.insurance_mutiple_json = DB_STATICTABLE["proto_insurance_mutiple"]["list"]
end

function PlayGround:createUI(  )

	for i=1,24 do
		display.loadAnim("res/Image/main/emotion/effect/", "emotion_" .. (i-1), 1, EMOTION_CONFIG[i], 1 / EMOTION_CONFIG_DELAY[i])
	end

	display.loadAnim("res/Image/effect/", "effect_all_in", 1, 12, 1/12)
	display.loadAnim("res/Image/effect/", "effect_operate", 1, 10, 1/10)

	display.loadAnim("res/Image/game/roleinfo/effect/", "toast_emotion_1", 1, 35, 1/24)
	display.loadAnim("res/Image/game/roleinfo/effect/", "toast_emotion_2", 1, 49, 1/24)
	display.loadAnim("res/Image/game/roleinfo/effect/", "toast_emotion_3", 1, 42, 1/24)
	display.loadAnim("res/Image/game/roleinfo/effect/", "toast_emotion_4", 1, 37, 1/24)
	display.loadAnim("res/Image/game/roleinfo/effect/", "toast_emotion_5", 1, 38, 1/24)
	display.loadAnim("res/Image/game/roleinfo/effect/", "toast_emotion_6", 1, 34, 1/24)
	display.loadAnim("res/Image/game/roleinfo/effect/", "toast_emotion_7", 1, 36, 1/24)
	display.loadAnim("res/Image/game/roleinfo/effect/", "toast_emotion_8", 1, 15, 1/24)
	display.loadAnim("res/Image/game/roleinfo/effect/", "toast_emotion_8_pre", 1, 36, 1/24)
	display.loadAnim("res/Image/game/roleinfo/effect/", "toast_emotion_9", 1, 30, 1/24)
	display.addArmatureFileInfo("res/Image/effect/wineffect/WIN.csb")

	zc.playMusic("dz_bgm", true)

	local bgImg = zc.createImageView({
		src = "res/Image/game/desk_bg_1.jpg",
		pos = cc.p(zc.cx, zc.cy),
		-- fullScreen = true,
		parent = self,
		})


	for i=1,5 do
		zc.createImageView({
		src = "res/Image/main/poker_box.png",
		pos = DESK_POKER_POS[i],
		parent = self,
		})
	end

 	self.menuBtn = zc.createImageView({
		src = "res/Image/main/ground_menu.png",
		pos = cc.p(50, zc.height - 100),
		shaderObj = 1,
		parent = self,
		listener = function ( sender, eventType )
			if eventType == ccui.TouchEventType.ended then
				zc.createPop("src/app/pop/dzMenuPop",{ state = self:getSelfState()})
			end
		end
		})
 	-- 添加商城入口
    local shopImg = zc.createImageView({
        src = "res/Image/main/ground_shop.png",
        pos = cc.p(zc.width - 50, zc.height - 100),
        parent = self,
        listener = function ( sender, eventType )
            if eventType == ccui.TouchEventType.began then
                sender:setScale(1.1)
            elseif eventType == ccui.TouchEventType.ended then
                sender:setScale(1.0)

                local myInfo = self:getPlayerInfoById(PlayerData.base.roleid)
  
				local minMoney = DB_STATICTABLE["proto_texas_holdem"]["list"][tostring(self.room_type)].min
				local maxMoney = DB_STATICTABLE["proto_texas_holdem"]["list"][tostring(self.room_type)].max

               	zc.createPop("src/app/pop/dzBuyChipPop",{myInfo = myInfo,minMoney = minMoney,maxMoney = maxMoney})

            elseif eventType == ccui.TouchEventType.canceled then
                sender:setScale(1.0)
            end
        end
        })

 	-- 添加上局回顾入口
    local lastGameImg = zc.createImageView({
        src = "res/Image/main/ground_lastGame.png",
        pos = cc.p(zc.width, 290),
        ap = cc.p(1,0.5),
        parent = self,
        listener = function ( sender, eventType )
            if eventType == ccui.TouchEventType.began then

            elseif eventType == ccui.TouchEventType.ended then
            	zc.createPop("src/app/pop/lastGamePop",{ data = self.dataPool.last_game})
            end
        end
        })

 	-- 添加牌局统计入口
    local statisticsGameImg = zc.createImageView({
        src = "res/Image/main/ground_statistics.png",
        pos = cc.p(0, 290),
        ap = cc.p(0,0.5),
        parent = self,
        listener = function ( sender, eventType )
			if eventType == ccui.TouchEventType.ended then
            	zc.createPop("src/app/pop/statisticsGamePop",{ data = self.dataPool.statistics})
            end
        end
        })

    --庄家
    self.dealerImg = zc.createImageView({
		src = "res/Image/game/first_round_img.png",
		pos = cc.p(zc.cx, zc.cy),
		parent = self
		})
	self.dealerImg:setVisible(false)

	--表情
	zc.createImageView({
        src = "res/Image/main/common_emotion.png",
        pos = cc.p(zc.width - 195, 60),
        parent = self,
        listener = function ( sender, eventType )
            if eventType == ccui.TouchEventType.ended then
            	zc.createPop("src/app/pop/emotionPop")
            end
        end
        })

	--语音
	local voiceBeganPosY = 0
    zc.createImageView({
        src = "res/Image/main/common_speak.png",
        pos = cc.p(zc.width - 80, 60),
        parent = self,
        listener = function ( sender, eventType )

            if eventType == ccui.TouchEventType.began then
                voiceBeganPosY = sender:getTouchBeganPosition().y

                self:createVoiceLayout()

                if GotyeApi then
                    GotyeApi:talkToRoom(zc.GOTYEROOMID, 0, false, VOICE_LIMIT_TIME * 1000)
                end
            elseif eventType == ccui.TouchEventType.moved then
                local voiceMovedPosY = sender:getTouchMovePosition().y

                if not self.voiceLayout then
                	if GotyeApi then
                        GotyeApi:cancelTalk()
                    end
                    return
                end

                if voiceMovedPosY - voiceBeganPosY > 130 then
                    self.voiceLayout.cancelImg:loadTexture("res/Image/game/voice_cancel_p.png")
                else
                    self.voiceLayout.cancelImg:loadTexture("res/Image/game/voice_cancel_n.png")
                end

            elseif eventType == ccui.TouchEventType.ended or eventType == ccui.TouchEventType.canceled then
                local voiceEndedPosY = sender:getTouchEndPosition().y

                if (not self.voiceLayout) or (voiceEndedPosY - voiceBeganPosY > 130) or (self.voiceLayout.passTime > VOICE_LIMIT_TIME - 1) then
                    -- zc.tipInfo("取消发送语音")
                    if GotyeApi then
                        GotyeApi:cancelTalk()
                    end
                else

                    -- zc.tipInfo("开始发送语音")
                    if GotyeApi then
                        GotyeApi:stopTalk()
                    end
                    
                end

                self:removeVoiceLayout( )

            end
        end
        })

	-- 牌桌信息
	self.roomLabel = zc.createLabel({
		text = zc.lang("PlayGround_Field", "场"),
		fontSize = 22,
		color = cc.c3b(52, 77, 97),
		pos = cc.p(zc.cx, zc.height - 180),
		parent = self,
		-- outline = true
		})

	self.roomIdLabel = zc.createLabel({
		text = zc.lang("PlayGround_Info", "号桌"),
		fontSize = 22,
		color = cc.c3b(52, 77, 97),
		pos = cc.p(zc.cx, zc.height - 80),
		parent = self,
		-- outline = true
		})

	self.typeLabel = zc.createLabel({
		text = "",
		fontSize = 22,
		color = cc.c3b(52, 77, 97),
		pos = cc.p(zc.cx, zc.height - 230),
		parent = self,
		-- outline = true
		})

	self.blindLabel = zc.createLabel({
		text = "",
		fontSize = 22,
		color = cc.c3b(52, 77, 97),
		pos = cc.p(zc.cx, zc.height - 130),
		parent = self,
		-- outline = true
		})

	self.waitingLabel = zc.createLabel({
		text = zc.lang("等待下一局开始..."),
		-- shadow = true,
		fontSize = 26,
		color = cc.c3b(171, 171, 171),
		pos = cc.p(zc.cx, zc.cy - 80),
		parent = self
		})
	self.waitingLabel:setVisible(false)

	--主池
	local awardImg = zc.createImageView({
        src = "res/Image/main/ground/main_betpool_bg.png",
        pos = cc.p(zc.cx,zc.cy + 310),
        parent = self,
        })
	awardImg:setVisible(false)

	table.insert(self.chipPoolImg, awardImg)

	local awardLabel = zc.createLabel({
		text = "0",
		fontSize = 46,
		color = cc.c3b(255, 255, 255),
        pos = cc.p(75,awardImg:getContentSize().height/2-18),
        ap = cc.p(0,0.5),
		parent = awardImg
		})
	table.insert(self.chipPoolLabel, awardLabel)

	-- 边池
	for i=1,8 do
		local sideChipImg = zc.createImageView({
	        src = "res/Image/game/pickchip_bg.png",
	        pos = SIDE_POT_POS[i],
	        parent = self,
	        })

		local chipSideLabel = zc.createLabel({
			text = "0",
			fontSize = 20,
			ap = cc.p(0,0.5),
			color = cc.c3b(255, 255, 255),
			pos = cc.p(46,sideChipImg:getContentSize().height/2),
			parent = sideChipImg
			})
		sideChipImg:setVisible(false)

		table.insert(self.chipPoolImg, sideChipImg)
		table.insert(self.chipPoolLabel, chipSideLabel)

	end

	--底池
	self.potImg = zc.createLabel({
		text = "0",
		fontSize = 30,
		color = cc.c3b(129, 172, 207),
		pos = cc.p(zc.cx, zc.cy+240),
		parent = self,
		})
	self.potImg:setVisible(false)

	--根据牌桌最大限制人数创建空桌
	for i=1,SEAT_NUM_LIMIT do
		local voidChair = zc.createImageView({
	        src = "res/Image/game/voidChair.png",
	        pos = SEAT_POS[i]["IN"],
	        zOrder = Z_ORDER_PLAYER,
	        parent = self,
	        tag = 100 + i,
	        listener =  function ( sender, eventType )
            if eventType == ccui.TouchEventType.ended then
            	local myInfo = self:getPlayerInfoById(PlayerData.base.roleid)
				local minMoney = DB_STATICTABLE["proto_texas_holdem"]["list"][tostring(self.room_type)].min
				local maxMoney = DB_STATICTABLE["proto_texas_holdem"]["list"][tostring(self.room_type)].max
				if myInfo and myInfo.seat == 0 then
					local pos  = string.sub(tostring(sender:getTag()), 3, 3)
               		zc.createPop("src/app/pop/dzBuyChipPop",{myInfo = myInfo,minMoney = minMoney,maxMoney = maxMoney,pos = tonumber(pos)})
               	end
            end
        end,
	        })
	end
	--查看下一圈牌
	self.want_lookImg = zc.createImageView({
        src = "res/Image/game/want_look_bg.png",
        pos = cc.p(zc.width - 130,180),
        parent = self,
        listener = function ( sender, eventType )
            if eventType == ccui.TouchEventType.ended then
                zc.socket.sendTable({m = "texas_holdem_look_next"})
                sender:setVisible(false)
            end
        end,
        })
	zc.createLabel({
		text = zc.lang("PlayGround_SeeNextCards", "查看下一圈牌"),
		fontSize = 20,
		color = cc.c3b(255, 255, 255),
		pos = cc.p(self.want_lookImg:getContentSize().width/2, self.want_lookImg:getContentSize().height - 20),
		parent = self.want_lookImg,
		})

	zc.createLabel({
		text = zc.lang("PlayGround_Flop", "翻牌"),
		fontSize = 26,
		color = cc.c3b(195, 166, 89),
		pos = cc.p(self.want_lookImg:getContentSize().width/2, self.want_lookImg:getContentSize().height / 2),
		parent = self.want_lookImg,
		tag = 33,
		})
	local look_cost_num = DB_STATICTABLE["proto_server_value"]["list"][tostring(303)]["value"]
	zc.createAttrLabel({
		{image = true, src = "res/Image/main/mine_chip_icon.png"},
		{text = look_cost_num, fontSize = 24, color = cc.c3b(255, 255, 255), offsetX = 10, offsetY = 5},
		},
		{
		ap = cc.p(0.5, 0.5),
		pos = cc.p(self.want_lookImg:getContentSize().width / 2, 20),
		parent = self.want_lookImg,
		})

	self.want_lookImg:setVisible(false)
	--胜利特效
	
	-- 邀请好友
	self.inviteBtn = zc.createButton({
		normal = "res/Image/game/invite_btn_n.png",
		pressed = "res/Image/game/invite_btn_p.png",
		pos = cc.p(zc.cx, zc.cy - 30),
		parent = self,
		listener = function ( sender, eventType )
			if eventType == ccui.TouchEventType.ended then
				zc.doSdkShare( {
					contentType = "link",
					wxScene = 0, -- 0聊天 1朋友圈
					contentURL = "http://192.168.200.118/",
					contentTitle = "【鲸鱼德州】房间号：" .. self.room_id,
					contentDescription = zc.lang("mine_ContentDescription", "快来体验德州扑克，一起high翻天，IOS&Android首发"),
					} )
            end
		end
		})
	self.inviteBtn:setVisible(false)

	--初始化下注筹码
	self:initChipLayout()

	-- 初始化控制器
	self:initControlBar()

end

-- 播放魔法表情
function PlayGround:runMagicAction(  )
	-- 关闭上层界面
	-- zc.closePop("src/app/pop/dzRoleInfoPop")
	-- zc.closePop("src/app/pop/emotionPop")

	local fromPlayer = self:getPlayerById( self.dataPool.magic.from_id )
	local toPlayer = self:getPlayerById( self.dataPool.magic.to_id )
	if tolua.isnull(fromPlayer) or tolua.isnull(toPlayer) then return end

	local fromPos = fromPlayer.pos
	local toPos = toPlayer.pos

	if zc.isSelf(self.dataPool.magic.from_id) then
		fromPos = cc.p(fromPlayer.pos.x - 300,fromPlayer.pos.y - 60)
	end

	if zc.isSelf(self.dataPool.magic.to_id) then
		toPos = cc.p(toPlayer.pos.x - 300,toPlayer.pos.y - 60)
	end

	-- zc.playEffect("chat_face_" .. self.dataPool.magic.magic_id)

	if self.dataPool.magic.to_id == self.dataPool.magic.from_id then
		local emotionEffect, emotionAnimation = display.getAnimation("emotion_" .. (self.dataPool.magic.magic_id - 1), 0)
	   	emotionEffect:setPosition(fromPos)
	   	emotionEffect:setScale(2.1)
	    emotionEffect:setAnchorPoint(cc.p(0.5, 0.5))
	   	self:addChild(emotionEffect)
	   	emotionEffect:setLocalZOrder(Z_ORDER_MAGIC_ANIMATION)

	   	if EMOTION_CONFIG[self.dataPool.magic.magic_id] < 7 then
	   		display.playAnimationForever( emotionEffect, emotionAnimation )
	   		emotionEffect:runAction(cc.Sequence:create(cc.DelayTime:create(2), cc.RemoveSelf:create()))
	   	else
	   		display.playAnimationOnceAndCleanup( emotionEffect, self, emotionAnimation)	
	   	end

		
	else


		zc.getPop("src/app/pop/dzRoleInfoPop").createToastAction({
			startPos = fromPos,
			endPos = toPos,
			zOrder = Z_ORDER_MAGIC_ANIMATION,
			parent = self,
			emotionId = self.dataPool.magic.magic_id,
			})

		
	end

end

--初始化自己面前的下注筹码layout
function PlayGround:initChipLayout( )

	for i=1,SEAT_NUM_LIMIT do

		local round_bet_bg = zc.createLayout({
			ap = cc.p(0.5,0.5),
			pos = cc.p(SEAT_POS[i]["IN"].x + CHIP_POS_OFFSET[i].x,SEAT_POS[i]["IN"].y + CHIP_POS_OFFSET[i].y ),
			zOrder = Z_ORDER_CHIP - 1,
			scale = CHIP_AP_OFFSET[i],
			size = cc.size(120,50)
			})
		self:addChild(round_bet_bg)
		round_bet_bg:setVisible(false)
		--下注筹码bg
		local chipLabel = zc.createLabel({
			text = "0",
			fontSize = 22,
			color = cc.c3b(255, 255, 255),
			pos = cc.p(75,round_bet_bg:getContentSize().height/2),
			parent = round_bet_bg,
			tag = 33,
			})
		chipLabel:setScale(CHIP_AP_OFFSET[i])

		local default_bet_bg = zc.createImageView({
			src = "res/Image/game/chip/chip_0.png",
			pos = cc.p(27,round_bet_bg:getContentSize().height/2),
			parent = round_bet_bg,
		})
		table.insert(self.RoundLayput,round_bet_bg)
	end
end

--根据金币得到生成的筹码 table
function PlayGround:getChipsNum(money,limitNum)
	local chiptab = {}
	local money = tostring(money)
	local strLen = string.len(money)
	for i=1,strLen do
		local chip_num = string.sub(money,i,i)
		if tonumber(chip_num) > 0 then
			for j=1,chip_num do
				table.insert(chiptab,math.pow(10,strLen - i))
			end
		end
	end
	local finalChipTab = {}
	local limitNum = limitNum or 5
	for i=1,limitNum do
		if chiptab[i] then
			table.insert(finalChipTab,chiptab[i])
		end
	end

	table.sort( finalChipTab, function ( a,b )
		return a < b
	end )

	return finalChipTab
end

--paytype 1下注 2退回
function PlayGround:createChipAction( seatIndex, money, paytype ,seat)
	if paytype == 1 then
		zc.playEffect("chips_to_table")
	end

	local chiptab = self:getChipsNum(money)

	local startPos = cc.p(SEAT_POS[seatIndex]["IN"].x, SEAT_POS[seatIndex]["IN"].y )
	local endPos = cc.p(SEAT_POS[seatIndex]["IN"].x + CHIP_POS_OFFSET[seatIndex].x + CHIP_FLY_POS_OFFSET[seatIndex], SEAT_POS[seatIndex]["IN"].y + CHIP_POS_OFFSET[seatIndex].y )
	if paytype == 2 then
		local tempPos = startPos
		startPos = endPos
		endPos = tempPos
	end

	for i,v in ipairs(chiptab) do
		local commonChip = zc.createImageView({
			src = string.format("res/Image/game/chip/chip_%d.png",v),
			pos = startPos,
			parent = self,
			zOrder = Z_ORDER_CHIP
		})
		if paytype == 1 then
			local action = {}
			action[#action+1] = cc.DelayTime:create(i*0.05)
			action[#action+1] = cc.EaseExponentialOut:create(cc.MoveTo:create(0.3,endPos))
			commonChip:runAction(cc.Sequence:create({cc.Sequence:create(action)}))
			commonChip.num = v

			if self.CurrentchipsList[seat] == nil then 
				self.CurrentchipsList[seat] = {} 
			end
			table.insert(self.CurrentchipsList[seat],commonChip)
		end
		if paytype == 2 then
			local action = {}
			action[#action+1] = cc.DelayTime:create(i*0.05)
			action[#action+1] = cc.EaseExponentialOut:create(cc.MoveTo:create(0.3,endPos))
			action[#action+1] = cc.CallFunc:create(function ()
				commonChip:removeFromParent(true)
			end)
			commonChip:runAction(cc.Sequence:create({cc.Sequence:create(action)}))
		end
	end

end

function PlayGround:updateSelfDeskBet(_seatIndex,_seat)

	for i,v in ipairs(self.CurrentchipsList[_seat] or {}) do
		local endPos = cc.p(SEAT_POS[_seatIndex]["IN"].x + CHIP_POS_OFFSET[_seatIndex].x + CHIP_FLY_POS_OFFSET[_seatIndex], SEAT_POS[_seatIndex]["IN"].y + CHIP_POS_OFFSET[_seatIndex].y )
		if not tolua.isnull(v) then
			v:stopAllActions()
			v:setPosition(endPos)
		end
	end

 end

--结算退钱
function PlayGround:createWinNumAction(seatIndex,laseMoney,money)
	local discountLabel = zc.createLabel({
		text = "+" ..zc.getWan(money - laseMoney,4),
		ap = cc.p(0.5,0.5),
		parent = self,
		fontSize = 26,
		-- outline = true,
		color = cc.c3b(253,217,86),
		pos = SEAT_POS[seatIndex]["IN"],
		zOrder = Z_ORDER_PLAYER + 1,
		})
	discountLabel:setPositionY(discountLabel:getPositionY()+50)

	local act1 = cc.MoveBy:create(0.4, cc.p(0, 50))
	local act2 = cc.DelayTime:create(0.4)
	local act3 = cc.MoveBy:create(0.5, cc.p(0,  40))
	local act4 = cc.FadeOut:create(0.5)
	local act5 = cc.Spawn:create({act3,act4})
	local act6 = cc.CallFunc:create(function ( )
			discountLabel:removeFromParent(true)
		end)
	discountLabel:runAction(cc.Sequence:create({act1, act2, act5, act6}))
end

--押注完一轮更新玩家下注池
function PlayGround:updateDeskAllChip(  )
	--刷新池
	local updateChipPool
	updateChipPool = function ( _sideIndex,betNum )
		if betNum > 0 then
			self.chipPoolImg[_sideIndex]:setVisible(true)
			self.chipPoolLabel[_sideIndex]:setVisible(true)
			self.chipPoolLabel[_sideIndex]:setString(zc.getWan(betNum,4))
		else
			self.chipPoolImg[_sideIndex]:setVisible(false)
			self.chipPoolLabel[_sideIndex]:setVisible(false)
		end
	end

	for k,v in pairs(self.bet_pools) do
		local betNum = 0
		for kk,vv in pairs(v.bet_info or {}) do
			betNum = betNum + vv.bet
		end
		updateChipPool(v.poolid,betNum)
	end

end

--刷新自己面前的筹码，飞到主池和边池
function PlayGround:updateRoundMoney(  )

	for k,v in pairs(self.CurrentchipsList) do
		if k then
			zc.playEffect("dz_updatechips")
		end
	end

	for k,v in pairs(self.CurrentchipsList) do

		local index = 1
		for i=1,#v do
			v[i]:stopAllActions()
			local action = {}
			local delayact = cc.DelayTime:create(0.01*i)
			action[#action+1] = cc.EaseExponentialOut:create(cc.MoveTo:create(0.5,cc.p(zc.cx,zc.cy+230)))
		  	action[#action+1] = cc.FadeOut:create(0.3)
			local movecallback = CCCallFuncN:create(function (sender)
				sender:removeFromParent()
				sender = nil
				if index == #v then
					self.CurrentchipsList[k] = nil
				end
				index = index + 1
			end)
			v[i]:runAction(cc.Sequence:create({delayact,cc.Spawn:create(action),movecallback}))
		end

	end

	--隐藏自己面前下注的筹码img Bg
	for i,v in ipairs(self.RoundLayput) do
		v:setVisible(false)
		v:getChildByTag(33):setString(0)
	end
end


--筹码结算整理
function PlayGround:settlementChip(end_info)
	zc.playEffect("dz_updatechips")
	--隐藏底池
	self.potImg:setVisible(false)

	for i,v in ipairs(end_info) do
		if v and v.pool_profit then
			self.chipPoolImg[v.poolid]:setVisible(false)
			self.chipPoolLabel[v.poolid]:setVisible(false)

			--生成新筹码飞到对应的玩家
			for ii,vv in ipairs(v.pool_profit) do

				local seatIndex = self:_getSeatIndex( vv.roleid )
				local chiptab = self:getChipsNum(vv.profit)

				local endPos = SIDE_POT_POS[v.poolid-1]
				if v.poolid == 1 then
					endPos = cc.p(zc.cx, zc.cy+195)
				end

				for iii,vvv in ipairs(chiptab) do
					local commonChip = zc.createImageView({
						src = string.format("res/Image/game/chip/chip_%d.png",vvv),
						parent = self,
						zOrder = Z_ORDER_CHIP,
						pos = cc.p(endPos.x-50,endPos.y),
					})

					local action = {}
					action[#action+1] = cc.DelayTime:create(0.02*iii)
					action[#action+1] = cc.EaseExponentialOut:create(cc.MoveTo:create(0.5,SEAT_POS[seatIndex]["IN"]))
					action[#action+1] = cc.CallFunc:create(function ()
						commonChip:removeFromParent(true)
					end)
					commonChip:runAction(cc.Sequence:create(action))
				end
			end
		end
	end


end

--刷新牌桌的五张牌
function PlayGround:updateDeskPoker( _card )
	zc.playEffect("dz_trunpoker")
	local poker = PokerClass.new({})
	poker:setScale(0.75)
	poker:setPosition(DESK_POKER_POS[#self.deskPokers+1])
	self:addChild(poker, Z_ORDER_POKER)
	poker.card = _card

	table.insert(self.deskPokers, poker) 

	poker:update({type = math.floor(_card / 100), id = _card % 100})
	poker:showWithAction()

	if self.dataPool.state == GAME_STATE_END then
		poker:setOpacity(150)
	end

	--显示成牌
	local myData = self:getPlayerInfoById(PlayerData.base.roleid)
	if not myData then return end
	local myCards = myData.cards

	local myAllCards = {}
	for i,v in ipairs(myCards) do
		myAllCards[#myAllCards+1] = v
	end

	for i,v in ipairs(self.deskPokers) do
		myAllCards[#myAllCards+1] = v.card
	end

	local PokerType,fivePoker
	local lightPoker = {}
	if #myAllCards >= 5 then
		PokerType,fivePoker = zc.selectPoker(myAllCards)
		if PokerType == 8 or (PokerType >= 2 and  PokerType <= 4) then
			lightPoker = zc.getLightPoker(fivePoker)
		elseif PokerType >= 5 and PokerType ~= 8 then
			for i,v in ipairs(fivePoker) do
				lightPoker[#lightPoker+1] = v
			end
		end
	else
		lightPoker = zc.getLightPoker(myAllCards)
	end
	 
	local myInfo = self:getPlayerById(PlayerData.base.roleid)
	if not myInfo or #myInfo.pokerList == 0 then return end
	if myInfo.state == 4 then return end 	--弃牌不计算高亮

	local myPokers = myInfo.pokerList
	--  先全部不置灰 不高亮  
	for ii,vv in ipairs(myPokers) do
		if myPokers[ii] then
			myPokers[ii].pokerImg:getChildByTag(12):setVisible(false)
			myPokers[ii]:removePokerMask()
		end
	end

	for ii,vv in ipairs(self.deskPokers) do
		if not tolua.isnull(vv) then
			vv.pokerImg:getChildByTag(12):setVisible(false)	
			vv:removePokerMask()	
		end
	end

	--  再成牌高亮
	for i,v in ipairs(lightPoker) do
		for ii,vv in ipairs(myCards) do
			if v == vv  then
				myPokers[ii].pokerImg:getChildByTag(12):setVisible(true)
			end
		end

		for ii,vv in ipairs(self.deskPokers) do
			if v == vv.card then
				vv.pokerImg:getChildByTag(12):setVisible(true)
			end
		end

	end

	--5 6 7 张牌时候，用不到的牌置灰
	if #self.deskPokers >= 4 and myCards and #myCards > 0 then

		local grayPoker = {}
		for ii,vv in ipairs(myAllCards) do
			local index = 1
			for iii,vvv in ipairs(fivePoker) do
				if tonumber(vv) == tonumber(vvv) then
					index = 2
				end
			end
			if index == 1 then
				grayPoker[#grayPoker+1] = vv
			end
		end
		
		for i,v in ipairs(grayPoker) do
			for ii,vv in ipairs(myCards) do

				if v == vv then
					myPokers[ii]:addPokerMask()
				end
			end

			for ii,vv in ipairs(self.deskPokers) do
				if v == vv.card then
					vv:addPokerMask()
				end
			end
		end
	end


end

function PlayGround:getSitBtn()
	return self.sitBtn
end

-- 显示控制器
function PlayGround:initControlBar(  )

    local beginPosY = 0
    local offsetY = 0

	self.controlBar = zc.createLayout({ap = cc.p(0.5,0.5),pos = cc.p(zc.cx,zc.cy),zOrder = Z_ORDER_CONTROL_LAYOUT,size = cc.size(zc.width,zc.height)})
	self:addChild(self.controlBar)

	local layoutBg = zc.createLayout({pos = SEAT_POS[SEAT_OWN_INDEX]["IN"],zOrder = Z_ORDER_CONTROL_LAYOUT})
	self.controlBar:addChild(layoutBg)
	layoutBg:setPositionY(layoutBg:getPositionY() + 80)

	local cancleBtn = zc.createImageView({
		name = "addChipBtn",
		src = "res/Image/game/addSlider_cancle_img.png",
		pos = cc.p(0, 70),
		ap = cc.p(0.5,0),
		parent = layoutBg,
		})
	self.controlBar.cancleBtn = cancleBtn
	local addLine = zc.createImageView({
		name = "addChipBtn",
		ap = cc.p(0.5,0),
		src = "res/Image/game/addSlider_line_img.png",
		pos = cc.p(cancleBtn:getContentSize().width / 2, cancleBtn:getContentSize().height + 4),
		parent = cancleBtn,
		})

	local allInImg = zc.createImageView({
		name = "addChipBtn",
		ap = cc.p(0.5,0),
		src = "res/Image/game/addSlider_allin_img.png",
		pos = cc.p(0,  addLine:getContentSize().height + 4),
		parent = addLine,
		})

	local allInLabel = zc.createLabel({
		text = "0",
		fontSize = 28,
		ap = cc.p(0.5,0.5),
		color = cc.c3b(32, 94, 150),
		pos = cc.p(allInImg:getContentSize().width/2, allInImg:getContentSize().height/2+11),
		parent = allInImg,
		-- outline = true,
		})
	cancleBtn:setVisible(false)

    local maxOffset = addLine:getContentSize().height + allInImg:getContentSize().height + 5

	-- 按钮列表
	self.controlBar.btnList = {}
	--加注
	local addChipBtn = zc.createImageView({
		name = "addChipBtn",
		src = "res/Image/game/add_btn.png",
		pos = cc.p(0, 30),
		ap = cc.p(0.5,0),
		zOrder = 1,
		parent = layoutBg,
		listener = function ( sender, eventType )
		 	if eventType == ccui.TouchEventType.began then
			 		sender:loadTexture("res/Image/game/addSlider_addBtn_img.png")
					cancleBtn:setVisible(true)
					local curMoney = self:getPlayerById(PlayerData.base.roleid).money
					allInLabel:setString(zc.getWan(curMoney,4))
					sender:getChildByTag(32):setVisible(true)
					sender:getChildByTag(33):setVisible(true)
					sender:getChildByTag(33):setString(zc.getWan(sender.addNum,4))
		 		if sender.addType == 1 then
                	beginPosY = sender:getTouchBeganPosition().y
					sender:getChildByTag(33):setString(zc.getWan(sender.addNum,4))
				elseif sender.addType == 2 then
					--此时应该加动作 按钮上的字上飞 继续滑动
					sender:getChildByTag(33):stopAllActions()
					local moveact = cc.MoveTo:create(0.1,cc.p(sender:getContentSize().width/2,140))
					sender:getChildByTag(33):runAction(moveact)
				end
			elseif eventType == ccui.TouchEventType.moved then
                local movePosY = sender:getTouchMovePosition().y 
                --音效
                if movePosY >= beginPosY and movePosY < beginPosY + maxOffset then
                	sender.playType = 0
					if math.floor(movePosY - beginPosY) % 10 == 0 then
						zc.playEffect("slider")
					end
				end
				if movePosY >= beginPosY + maxOffset and sender.playType == 0 then
					zc.playEffect("slider_top")
					sender.playType = 1
                end

                if movePosY <= beginPosY then
                	movePosY = beginPosY
                end
                if movePosY >= beginPosY + maxOffset then
                	movePosY = beginPosY + maxOffset
                end

                offsetY = movePosY - beginPosY

				local min,max = self:getLimitAddNum()

                local addNum = math.floor(min + (max - min)* offsetY / maxOffset)
                if offsetY <= 0 then
                	addNum = 0
                end

                if math.abs(offsetY - sender.lastOffsetY) > 10 then
                	sender.addType = 1
           		end

                sender.addNum = addNum
 				sender:setPositionY(offsetY + 30)
                sender:getChildByTag(33):setString(zc.getWan(addNum,4))
                if offsetY >= maxOffset then
                	sender:getChildByTag(33):setString("ALLIN")
                end

			elseif eventType == ccui.TouchEventType.ended or eventType == ccui.TouchEventType.canceled  then
				sender.lastOffsetY = offsetY
				if sender.addNum == 0 then
					sender:getChildByTag(32):setVisible(false)
					sender:getChildByTag(33):setVisible(false)
					cancleBtn:setVisible(false)

					if self.cur_money == 0 then
						sender:loadTexture("res/Image/game/bet_btn.png")
					else
						sender:loadTexture("res/Image/game/add_btn.png")
					end

				elseif sender.addNum > 0 and sender.addType == 1 then
					sender:getChildByTag(32):setVisible(false)
					sender:getChildByTag(33):stopAllActions()
					local moveact = cc.MoveTo:create(0.1,cc.p(sender:getContentSize().width/2,sender:getContentSize().height/2))
					sender:getChildByTag(33):runAction(moveact)
					sender.addType = 2
				elseif sender.addNum > 0 and sender.addType == 2 then
					sender:getChildByTag(32):setVisible(false)
					sender:getChildByTag(33):setVisible(false)

					self.controlBar.cancleBtn:setVisible(false)
					zc.socket.sendTable({m = "texas_holdem_add", num = sender.addNum})
				end

			end
		end,
		})
	addChipBtn.addNum = 0
	addChipBtn.addType = 1
	addChipBtn.lastOffsetY = 0
	addChipBtn.playType = 0
	table.insert(self.controlBar.btnList, addChipBtn)

	--加注数值底 
	local addlabelBg = zc.createImageView({
		src = "res/Image/game/addSlider_label_img.png",
		ap = cc.p(0.5,0.5),
		pos = cc.p(addChipBtn:getContentSize().width/2-6, 140),
		parent = addChipBtn,
		tag = 32,
		})
	addlabelBg:setVisible(false)
	local addLabel = zc.createLabel({
		text = 0,
		fontSize = 24,
		color = cc.c3b(255, 255, 255),
		ap = cc.p(0.5,0.5),
		pos = cc.p(addChipBtn:getContentSize().width/2-6,140),
		parent = addChipBtn,
		tag = 33,
		})
	addLabel:setVisible(false)

	for i=1,3 do
		local ensureAddBtn = zc.createButton({
		name = "ensureAddBtn" .. i,
		normal = "res/Image/game/ensure_add_bg.png",
		pressed = "res/Image/game/ensure_add_bg.png",
		pos = cc.p(-130 + (i-1)* 130, 235),
		parent = layoutBg,

		})
		table.insert(self.controlBar.btnList, ensureAddBtn)
		zc.createLabel({
			text = 0,
			fontSize = 20,
			color = cc.c3b(32, 94, 150),
			pos = cc.p(ensureAddBtn:getContentSize().width/2, ensureAddBtn:getContentSize().height/2+11),
			parent = ensureAddBtn,
			tag = 31,
			})
		zc.createLabel({
			text = "",
			fontSize = 20,
			color = cc.c3b(32, 94, 150),
			pos = cc.p(ensureAddBtn:getContentSize().width/2, ensureAddBtn:getContentSize().height/2-11),
			parent = ensureAddBtn,
			tag = 32,
			})
		zc.createLabel({
			text = 0,
			fontSize = 20,
			color = cc.c3b(52, 144, 82),
			pos = cc.p(ensureAddBtn:getContentSize().width/2, -10),
			parent = ensureAddBtn,
			tag = 33,
			})
	end

	--跟注
	local followChipBtn = zc.createImageView({
		name = "followChipBtn",
		src = "res/Image/game/followchip_btn.png",
		pos = cc.p(180, -20),
		parent = layoutBg,
		listener = function ( sender, eventType )
			if eventType == ccui.TouchEventType.ended then
				zc.socket.sendTable({m = "texas_holdem_follow"})
			end
		end,
		})
	table.insert(self.controlBar.btnList, followChipBtn)

	zc.createLabel({
		text = "0",
		ap = cc.p(0.5,0.5),
		fontSize = 24,
		color = cc.c3b(17, 52, 85),
		pos = cc.p(followChipBtn:getContentSize().width/2, followChipBtn:getContentSize().height/2+5),
		tag = 33,
		parent = followChipBtn
		})

	--过牌=看牌
	local passChipBtn = zc.createImageView({
		name = "passChipBtn",
		src = "res/Image/game/passchip_btn.png",
		pos = cc.p(180, -20),
		parent = layoutBg,
		listener = function ( sender, eventType )
			if eventType == ccui.TouchEventType.ended then
				zc.socket.sendTable({m = "texas_holdem_skip"})
			end
		end,
		})

	--过牌倒计时
	local passTimer = zc.createProgressTimer({
		src = "res/Image/game/controller_progress_img.png",
		type = cc.PROGRESS_TIMER_TYPE_RADIAL,
		percent = 0,
		pos = cc.p(passChipBtn:getContentSize().width/2,passChipBtn:getContentSize().height/2),
		parent = passChipBtn,
		tag = 11,
		})
	passTimer:setScaleX(-1)
	table.insert(self.controlBar.btnList, passChipBtn)

	--弃牌
	local pickChipBtn = zc.createImageView({
		name = "pickChipBtn",
		src = "res/Image/game/pickchip_btn.png",
		pos = cc.p(-180, -20),
		parent = layoutBg,
		listener = function ( sender, eventType )
			if eventType == ccui.TouchEventType.ended then
				zc.socket.sendTable({m = "texas_holdem_drop"})
			end
		end,
		})

	--弃牌倒计时
	local pickTimer = zc.createProgressTimer({
		src = "res/Image/game/controller_progress_img.png",
		type = cc.PROGRESS_TIMER_TYPE_RADIAL,
		percent = 0,
		pos = cc.p(pickChipBtn:getContentSize().width/2,pickChipBtn:getContentSize().height/2),
		parent = pickChipBtn,
		tag = 11,
		})
	pickTimer:setScaleX(-1)
	table.insert(self.controlBar.btnList, pickChipBtn)

	local addTimeCost = DB_STATICTABLE["proto_server_value"]["list"][tostring(302)]["value"]
	local addTimeNum = DB_STATICTABLE["proto_server_value"]["list"][tostring(301)]["value"]
	--加时
	local addTimeBtn = zc.createButton({
		name = "addTimeBtn",
		normal = "res/Image/game/add_time_label_bg.png",
		pressed = "res/Image/game/add_time_label_bg.png",
		pos = cc.p(-320, -45),
		parent = layoutBg,
		listener = function ( sender, eventType )
			if eventType == ccui.TouchEventType.ended then
				zc.socket.sendTable({m = "texas_holdem_overtime"})
			end
		end,
		})
	table.insert(self.controlBar.btnList, addTimeBtn)
	--加注花费
	zc.createAttrLabel({
		{image = true, src = "res/Image/main/mine_chip_icon.png",scale = 0.7},
		{text = addTimeCost, fontSize = 22, color = cc.c3b(147, 164, 184), offsetX = 4, offsetY = 0},
		},
		{
		ap = cc.p(0.5, 0.5),
		pos =cc.p(addTimeBtn:getContentSize().width/2+20, 82),
		parent = addTimeBtn,
		})
	--加注时长
	zc.createLabel({
		text = "+" .. addTimeNum,
		ap = cc.p(0.5,0.5),
		fontSize = 22,
		color = cc.c3b(147, 164, 184),
		pos = cc.p(55, 15),
		parent = addTimeBtn
		})

	--精确加注
	local exactAddBettn = zc.createButton({
		text = zc.lang("PlayGround_PreciseFilling", "精确加注"),
		fontSize = 30,
		scale = 0.9,
		fontColor = cc.c3b(60,211,218),
		name = "exactAddBettn",
		normal = "res/Image/game/exact_add_bg.png",
		pressed = "res/Image/game/exact_add_bg.png",
		pos = cc.p(0, 308),
		parent = layoutBg,
		listener = function ( sender, eventType )
			if eventType == ccui.TouchEventType.ended then
				local min,max = self:getLimitAddNum()
				self.controlBar.addChipBtn:setVisible(false)
				zc.createPop("src/app/pop/exactAddBetPop",{min = min,max = max, ground = self})
			end
		end,
		})
	table.insert(self.controlBar.btnList, exactAddBettn)

	-- 点语法
	for i,v in ipairs(self.controlBar.btnList) do
		v:setVisible(false)
		self.controlBar[v:getName()] = v
	end

	self.controlBar.autoBtnList = {}
	--预选框逻辑
	--自动弃牌
	local auto_pickBtn, auto_checkBtn, auto_CallBtn
	auto_pickBtn = zc.createButton({
		name = "auto_pickBtn",
		normal = "res/Image/game/auto_pick_n.png",
		pressed = "res/Image/game/auto_pick_p.png",
		pos = cc.p(-180, -20),
		parent = layoutBg,
		listener = function ( sender, eventType )
            if eventType == ccui.TouchEventType.ended then
	            if sender.selected == 0 then
	            	sender.selected = 1
	            	sender:setBrightStyle(1)    
	            	auto_checkBtn.selected = 0
	            	auto_CallBtn.selected = 0
	            	auto_checkBtn:setBrightStyle(0)
	            	auto_CallBtn:setBrightStyle(0)
	        	else
	        		sender.selected = 0
	        		sender:setBrightStyle(0)
	        	end
            end
		end,
		})
	auto_pickBtn.selected = 0
	table.insert(self.controlBar.autoBtnList, auto_pickBtn)
	--自动过牌
	auto_checkBtn = zc.createButton({
		name = "auto_checkBtn",
		normal = "res/Image/game/auto_check_n.png",
		pressed = "res/Image/game/auto_check_p.png",
		pos = cc.p(180, -20),
		parent = layoutBg,
		listener = function ( sender, eventType )
			if eventType == ccui.TouchEventType.ended then
            	if  sender.selected == 0 then
            	    sender.selected = 1
 	            	sender:setBrightStyle(1)           	    
	            	auto_pickBtn.selected = 0
	            	auto_CallBtn.selected = 0
	            	auto_pickBtn:setBrightStyle(0)
	            	auto_CallBtn:setBrightStyle(0)
	            else
	            	sender.selected = 0
	        		sender:setBrightStyle(0)
	            end
	        end
		end,
		})
	auto_checkBtn.selected = 0
	table.insert(self.controlBar.autoBtnList, auto_checkBtn)
	--自动跟注
	auto_CallBtn = zc.createButton({
		name = "auto_CallBtn",
		normal = "res/Image/game/auto_call_n.png",
		pressed = "res/Image/game/auto_call_p.png",
		pos = cc.p(180, -20),
		parent = layoutBg,
		listener = function ( sender, eventType )
			if eventType == ccui.TouchEventType.ended then
			    if  sender.selected == 0 then
	            	sender.selected = 1
	            	sender:setBrightStyle(1)
	            	auto_pickBtn.selected = 0
	            	auto_checkBtn.selected = 0
	            	auto_checkBtn:setBrightStyle(0)
	                auto_pickBtn:setBrightStyle(0)
	            else
					sender.selected = 0
					sender:setBrightStyle(0)
				end
            end
		end,
		})
	auto_CallBtn.selected = 0
	table.insert(self.controlBar.autoBtnList, auto_CallBtn)

	zc.createLabel({
		text = "0",
		fontSize = 26,
		ap = cc.p(0.5,0.5),
		color = cc.c3b(255, 255, 255),
		pos = cc.p(auto_CallBtn:getContentSize().width/2, auto_CallBtn:getContentSize().height/2+5),
		parent = auto_CallBtn,
		tag = 33,
		})

	for i,v in ipairs(self.controlBar.autoBtnList) do
		v:setVisible(false)
		self.controlBar[v:getName()] = v
	end

end

-- 获取玩家UI
function PlayGround:getPlayerById( roleId )
	local _player
	for i,v in ipairs(self.playerList) do
		if v.roleid and v.roleid == roleId then
			_player = v
			break
		end
	end
	return _player
end

-- 通过roleId获取玩家信息
function PlayGround:getPlayerInfoById( roleId )
	local _playerInfo
	for i,v in ipairs(self.dataPool.players or {}) do
		if roleId == v.base_info.roleid then
			_playerInfo = v
			break
		end
	end
	return _playerInfo
end


-- 获取UI座位位置
function PlayGround:_getSeatIndex( roleId )

	local _seatIndex = 1
	if zc.isSelf(roleId) then
		return _seatIndex
	end

	if self.ownSeatIndex then -- 自己的服务器位置
		-- 座位顺差
		local seatOffset = 0
		if self.ownSeatIndex - SEAT_OWN_INDEX >= 0 then
			seatOffset = self.ownSeatIndex - SEAT_OWN_INDEX
		else
			seatOffset = SEAT_NUM_LIMIT - (SEAT_OWN_INDEX - self.ownSeatIndex)
		end
		for i,v in ipairs(self.dataPool.players) do
			if roleId == v.base_info.roleid and self.ownSeatIndex then

				if v.seat - seatOffset <= 0 then
					_seatIndex = SEAT_NUM_LIMIT - math.abs(v.seat - seatOffset)
				else
					_seatIndex = v.seat - seatOffset
				end
				break
			end
		end
	end
	return _seatIndex
end

function PlayGround:getLimitAddNum(  )
	local min = self.cur_money *2 - self.last_money
	if self.cur_money == 0 and self.last_money == 0 then
		local self_pay_money = self:getPlayerById(PlayerData.base.roleid).pay_money
		if self_pay_money == 0 then
			min = self.bb
		elseif self_pay_money > 0 then
			min = self.bb + self_pay_money
		end
	end
	local curMoney = self:getPlayerById(PlayerData.base.roleid).money
	local cur_round_money = self:getPlayerById(PlayerData.base.roleid).round_money
	min = curMoney + cur_round_money > min and min or curMoney + cur_round_money
  	local max = curMoney + cur_round_money > min and curMoney + cur_round_money or min

  	return min,max
end

-- 创建角色UI
function PlayGround:addPlayer( changeInfo )
	-- UI上显示的座位（默认自己为1，逆时针转）
	local seatIndex = self:_getSeatIndex( changeInfo.base_info.roleid )

  	if zc.isSelf(changeInfo.base_info.roleid) and GotyeApi then
  		GotyeApi:enterRoom(zc.GOTYEROOMID)

  	end

	-- 创建玩家状态Layout
	local playerStatusLayout = zc.createLayout({
		parent = self,
		zOrder = Z_ORDER_STATUS_LAYOUT,
		})

	local winImg = ccs.Armature:create("WIN")
    self:addChild(winImg ,Z_ORDER_WIN_LABEL)
	winImg:setVisible(false)

	local player = PlayerClass.new({
		ground = self,
		pos = SEAT_POS[seatIndex]["IN"],
		statusLayout = playerStatusLayout,
		winImg = winImg,
		seatIndex = seatIndex,
		cards = changeInfo.cards,
		money = changeInfo.base_info.money,
		gold = changeInfo.base_info.gold,
		image = changeInfo.base_info.image, 
		nickname = changeInfo.base_info.nickname,
		region = changeInfo.base_info.region,
		roleid = changeInfo.base_info.roleid,
		vip = changeInfo.base_info.vip,
		self_money =  changeInfo.base_info.self_money,

		round_money = changeInfo.round_money,
		single_money = changeInfo.single_money,
		is_banker = changeInfo.is_banker,
		seat = changeInfo.seat,
		state = changeInfo.state,
		state_time = changeInfo.state_time,
		state_tips = changeInfo.state_tips,
		all_in = changeInfo,all_in,

		win_rate = changeInfo.win_rate,
		total_match = changeInfo.total_match,
		total_win = changeInfo.total_win,
		insurance_state = changeInfo.insurance_state,
		insurance_state_time = changeInfo.insurance_state_time,
		})

  	self:addChild(player,Z_ORDER_PLAYER)
	table.insert(self.playerList, player)

	if zc.isSelf(changeInfo.base_info.roleid) then --如果是自己的话，要转位置，自己保持在第一位置
		self:updateVoidDesk()
		zc.playEffect("chairSitSound")
		player:setPosition(SEAT_POS[seatIndex]["IN"])
	else
		player:setPosition(SEAT_POS[seatIndex]["OUT"])
		local moveTime = cc.pGetDistance(SEAT_POS[seatIndex]["OUT"], SEAT_POS[seatIndex]["IN"]) / 800
		local moveAct = cc.EaseExponentialOut:create(cc.MoveTo:create(moveTime, SEAT_POS[seatIndex]["IN"]))
		player:runAction(moveAct)
	end

	-- 直接发牌
	if changeInfo.state == PLAYER_STATE_WAIT_2 or changeInfo.state == PLAYER_STATE_OPT_3 or changeInfo.state == PLAYER_STATE_ALLIN_10 then
		for i = 1, CARD_NUM_LIMIT do
			local _poker = PokerClass.new({})
			self:addChild(_poker, Z_ORDER_POKER)
			player:dealOnePokerFast(_poker)
			table.insert(player.pokerList, _poker) 
		end
	 end

end


-- 更新角色UI
function PlayGround:updatePlayer( changeInfo )

	for i,v in ipairs(self.playerList) do
		if v.roleid == changeInfo.base_info.roleid then
			v:update(changeInfo)
			break
		end
	end

end

-- 删除角色UI
function PlayGround:deletePlayer( roleId )

	for i,v in ipairs(self.playerList) do
		if v.roleid and v.roleid == roleId then
			v:resetAll()
			if zc.isSelf(roleId) then
				self:updateVoidDesk(true)
				v:removeFromParent()
				v = nil
			else
				local moveTime = cc.pGetDistance(SEAT_POS[v.seatIndex]["OUT"], SEAT_POS[v.seatIndex]["IN"]) / 1000
				local seq = {
					cc.MoveTo:create(moveTime, SEAT_POS[v.seatIndex]["OUT"]),
					cc.CallFunc:create(function (  )
						v:removeFromParent()
						v = nil
					end)
				}
				v:runAction(cc.Sequence:create(seq))
			end
 			table.remove(self.playerList, i)

			break
		end
	end
end

-- 更新角色牌型
function PlayGround:updatePlayerPoker( roleid )

	--	if #self.deskPokers < 3 then return end

	for i,v in ipairs(self.playerList) do
		if v.roleid == roleid then
			local pokerTab = {}
			for ii,vv in ipairs(self.deskPokers) do
				table.insert(pokerTab, vv.card)
			end
			for ii,vv in ipairs(v.cards) do
				table.insert(pokerTab, vv)
			end

			-- 5张牌可以用getFivePokerType 大于5张得不到牌型 用
			if #pokerTab <= 5 then
				self.maxPokerType = zc.getFivePokerType(pokerTab)
			else
				self.maxPokerType = zc.selectPoker(pokerTab)
			end

			v:updatePokerType(zc.getPokerNameByType(self.maxPokerType))
		elseif v.roleid ~= roleid and not zc.isSelf(v.roleid) and self.room_type > 6 and v.state ~= PLAYER_STATE_DROP_4 then
			--如果是保险模式 要更新其他玩家牌型
			local pokerTab = {}
			for ii,vv in ipairs(self.deskPokers) do
				table.insert(pokerTab, vv.card)
			end

			for ii,vv in ipairs(v.pokerList) do
				if not tolua.isnull(vv) and v.cards[ii] then
					table.insert(pokerTab, v.cards[ii])
				end
			end
			local otherType = 0
			if #pokerTab <= 5 then
				otherType = zc.getFivePokerType(pokerTab)
			else
				otherType = zc.selectPoker(pokerTab)
			end
			v:updatePokerType(zc.getPokerNameByType(otherType))
		end 
	end

end

--更新空座椅子等位置
function PlayGround:updateVoidDesk( isstand )
	local seatOffset = 0
	if self.ownSeatIndex - SEAT_OWN_INDEX >= 0 then
		seatOffset = self.ownSeatIndex - SEAT_OWN_INDEX
	else
		seatOffset = SEAT_NUM_LIMIT - (SEAT_OWN_INDEX - self.ownSeatIndex)
	end

	for i=1,SEAT_NUM_LIMIT do
		local _seatIndex = 0
		if i - seatOffset <= 0 then
			_seatIndex = SEAT_NUM_LIMIT - math.abs(i - seatOffset)
		else
			_seatIndex = i - seatOffset
		end

		self:getChildByTag(tonumber("10" .. i)):setPosition(SEAT_POS[_seatIndex]["IN"])

		if _seatIndex == 1 then
			if self.ownSeatIndex ~= 0 then
				self:getChildByTag(tonumber("10" .. i)):setVisible(false)
			end
			if isstand then
				self:getChildByTag(tonumber("10" .. i)):setVisible(true)
			end
		end
	end

end

-- 更新玩家位置
function PlayGround:updatePlayerSeat( _seat )

	for i,v in ipairs(self.playerList) do
		if not tolua.isnull(v) then
			v:stopAllActions()
			local seatIndex = self:_getSeatIndex( v.roleid )
			v:setPosition(SEAT_POS[seatIndex]["IN"])
			v:update({seatIndex = seatIndex, pos = SEAT_POS[seatIndex]["IN"]})
		end
	end
end


-- 根据UI位置排序UI数据
function PlayGround:_sortPlayersBySeat(  )
	local sortfunction = function ( a, b )
		return a.seatIndex < b.seatIndex
	end
	table.sort(self.playerList, sortfunction)
end

-- 发牌
function PlayGround:dealAllPokers(  )
	-- 根据座位先更新在table的顺序
	self:_sortPlayersBySeat()
	-- 获取哪些可以发牌
	local players = {}
	local bankerIndex = 1
	for i,v in ipairs(self.playerList) do
		if v.state == 2 then
			table.insert(players, v)
		end

		if v.is_first then
			bankerIndex = i
		end
	end

	local playerNum = #players
	local dealPlayers = {}
	local loopIndex = bankerIndex
	while #dealPlayers < playerNum do
		if loopIndex > playerNum then
			loopIndex = 1
		end
		if players[loopIndex] then
			table.insert(dealPlayers, players[loopIndex])
			players[loopIndex] = nil
		end

		loopIndex = loopIndex + 1
	end

	local pokers = {}
	-- 根据人数创建牌
	for i = 1, (#dealPlayers * CARD_NUM_LIMIT) do
		local poker = PokerClass.new({})
		poker:setScale(0.5)
		poker:setPosition(zc.cx, zc.cy)
		self:addChild(poker, Z_ORDER_POKER)
		table.insert(pokers, poker) 
	end

	for i, _poker in ipairs(pokers) do
		local playerIdx = (i-1) % (#dealPlayers) + 1
		table.insert(dealPlayers[playerIdx].pokerList, _poker) 
		local seq = {
			cc.DelayTime:create(i * 0.05),
			cc.CallFunc:create(function (  )
				if not tolua.isnull(dealPlayers[playerIdx]) then 
					dealPlayers[playerIdx]:dealOnePoker(_poker, math.floor((i-1) / #dealPlayers) + 1 )
				end
				
			end)
		}
		self:runAction(cc.Sequence:create(seq))
	end

end

--重置牌桌
function PlayGround:resetGround(  )
	-- 重置玩家状态
	for i,v in ipairs(self.playerList) do
		v:resetAll()
	end

	--清除桌面5张牌
	for k,v in pairs(self.deskPokers) do
		v:removeFromParent(true)
		v = nil
	end
	self.deskPokers = {}
	--隐藏底池
	self.potImg:setVisible(false)

	--隐藏主池和边池
	for i,v in ipairs(self.chipPoolImg ) do
		v:setVisible(false)
	end
	for i,v in ipairs(self.chipPoolLabel ) do
		v:setVisible(false)
	end
	--隐藏面前的筹码imgBg
	for i,v in ipairs(self.RoundLayput) do
		v:setVisible(false)
	end

	--清除筹码
	for i,v in ipairs(self.chipPoolList) do
		for ii,vv in ipairs(v) do
			vv:removeFromParent(true)
			vv = nil
		end
	end
	self.chipPoolList = {}
	self.bet_pools = {}

	for k,v in pairs(self.CurrentchipsList) do
		for ii,vv in ipairs(v) do
			vv:removeFromParent(true)
			vv = nil
		end
	end
	self.CurrentchipsList = {}
	self.want_lookImg:setVisible(false)

	-- 清空语音聊天
	self:removeVoiceLayout( )

	if self.insuranceLayout then
		self.insuranceLayout:removeFromParent()
		self.insuranceLayout = nil
	end
	--重置已选择outs牌
	self.insuranceSelected = {}

	zc.closePop("src/app/pop/dzRoleInfoPop")
	zc.closePop("src/app/pop/emotionPop")

end

--更新自动跟注
function PlayGround:updateAutoAdd()

	if self.dataPool.state ~= GAME_STATE_END then
		local myInfo = self:getPlayerById(PlayerData.base.roleid)
		if not myInfo then return end
		if myInfo.state ~= 2 then return end
		local cur_round_money = self:getPlayerById(PlayerData.base.roleid).round_money

		if self.equal_money - cur_round_money == 0 then
			self.controlBar.auto_checkBtn:setVisible(true)
			self.controlBar.auto_CallBtn:setVisible(false)
		elseif self.equal_money - cur_round_money > 0 then

			self.controlBar.auto_CallBtn:setVisible(true)
			self.controlBar.auto_CallBtn:getChildByTag(33):setString(zc.getWan(self.equal_money - cur_round_money,4))

			self.controlBar.auto_checkBtn:setVisible(false)
			self.controlBar.auto_checkBtn:setBrightStyle(0)
			self.controlBar.auto_checkBtn.selected = 0
		end
	end

end


--更新游戏控制器逻辑
function PlayGround:updateController( updateInfo )

	local myInfo = self:getPlayerInfoById( updateInfo.base_info.roleid )

	if  updateInfo.state == 3 then
		--隐藏预选框
		for i,v in ipairs(self.controlBar.autoBtnList) do
			v:setVisible(false)
		end

		--发送预选逻辑
		if self.controlBar.auto_pickBtn.selected == 1 then
			zc.socket.sendTable({m = "texas_holdem_drop"})
			return
		elseif self.controlBar.auto_checkBtn.selected == 1 then
			zc.socket.sendTable({m = "texas_holdem_skip"})
			return
		elseif self.controlBar.auto_CallBtn.selected == 1 then
			zc.socket.sendTable({m = "texas_holdem_follow"})
			return
		end
		--加时按钮
		self.controlBar.addTimeBtn:setVisible(true)
		--初始化加注条
		self.controlBar.addChipBtn:loadTexture("res/Image/game/add_btn.png")
		self.controlBar.addChipBtn:setPositionY(30)
		self.controlBar.addChipBtn:getChildByTag(32):setVisible(false)
		self.controlBar.addChipBtn:getChildByTag(33):setVisible(false)
		self.controlBar.addChipBtn:getChildByTag(32):setPositionY(140)
		self.controlBar.addChipBtn:getChildByTag(33):setPositionY(140)
		self.controlBar.addChipBtn.addNum = 0
		self.controlBar.addChipBtn.addType = 1
		self.controlBar.addChipBtn.lastOffsetY = 0
		self.controlBar.exactAddBettn:setVisible(true)

		--池底加注
		self.controlBar.ensureAddBtn1:setVisible(true)
		self.controlBar.ensureAddBtn2:setVisible(true)
		self.controlBar.ensureAddBtn3:setVisible(true)

		self.controlBar.addChipBtn:setVisible(true)
		self.controlBar.pickChipBtn:setVisible(true)

		if updateInfo.pay_money == 0 then
			self.controlBar.followChipBtn:setVisible(false)
			self.controlBar.passChipBtn:setVisible(true)


			if updateInfo.state_time and updateInfo.state_time - 1 > 0 then
				self:stopProgress(  )
				self.progressImg = self.controlBar.passChipBtn:getChildByTag(11)
				self:startProgress(updateInfo.state_time - 1)
			end

		elseif  updateInfo.pay_money > 0 then
			self.controlBar.followChipBtn:setVisible(true)
			self.controlBar.passChipBtn:setVisible(false)

			if updateInfo.state_time and updateInfo.state_time - 1 > 0 then
				self:stopProgress(  )
				self.progressImg = self.controlBar.pickChipBtn:getChildByTag(11)
				self:startProgress(updateInfo.state_time - 1)
			end

			local followMoney = myInfo.base_info.money < updateInfo.pay_money and myInfo.base_info.money or updateInfo.pay_money
			--更新跟注按钮的筹码
			self.controlBar.followChipBtn:getChildByTag(33):setString(zc.getWan(followMoney,4))
			if followMoney >= myInfo.base_info.money then
				self.controlBar.followChipBtn:loadTexture("res/Image/game/controllBar_Btn.png")
			else
				self.controlBar.followChipBtn:loadTexture("res/Image/game/followchip_btn.png")
			end
		end

		--更新控制器按钮显示逻辑
		local curMoney = myInfo.base_info.money
		if updateInfo.pay_money >= curMoney or self.add_roleid == updateInfo.base_info.roleid then
			self.controlBar.addChipBtn:setVisible(false)
			self.controlBar.exactAddBettn:setVisible(false)
			--池底加注
			self.controlBar.ensureAddBtn1:setVisible(false)
			self.controlBar.ensureAddBtn2:setVisible(false)
			self.controlBar.ensureAddBtn3:setVisible(false)
		end

		local minAdd = self.cur_money *2 - self.last_money
		if self.cur_money == 0 and self.last_money == 0 then
			if updateInfo.pay_money == 0 then
				minAdd = self.bb
			elseif updateInfo.pay_money > 0 then
				minAdd = self.bb + updateInfo.pay_money
			end
		end
		local curMoney = self:getPlayerById(PlayerData.base.roleid).money
		local ensureAddnum = {math.ceil(self.pot/2),math.ceil(self.pot*2/3),self.pot}

		--更新三个加注按钮 X倍盲注 X倍底池
		if self.bb + self.sb == self.pot and self.dataPool.state == GAME_STATE_ON then
			ensureAddnum = {self.bb*2,self.bb*3,self.bb*4}
			local percentNum = {"2X","3X","4X"}
			for i=1,3 do
				self.controlBar["ensureAddBtn" .. i]:getChildByTag(31):setString(percentNum[i])
				self.controlBar["ensureAddBtn" .. i]:getChildByTag(32):setString(zc.lang("PlayGround_BlindNote", "盲注"))
			end
		else
			local percentNum = {"1/2","2/3","1"}
			for i=1,3 do
				self.controlBar["ensureAddBtn" .. i]:getChildByTag(31):setString(percentNum[i])
				self.controlBar["ensureAddBtn" .. i]:getChildByTag(32):setString(zc.lang("PlayGround_Pool", "底池"))
			end
		end

		--池底加注
		for i=1,3 do
			self.controlBar["ensureAddBtn" .. i]:getChildByTag(33):setString(zc.getWan(ensureAddnum[i],4))
			if ensureAddnum[i] < minAdd or ensureAddnum[i] > curMoney then
				self.controlBar["ensureAddBtn" .. i]:setTouchEnabled(false)
				self.controlBar["ensureAddBtn" .. i]:setBright(false)
			else
				self.controlBar["ensureAddBtn" .. i]:setBright(true)
				self.controlBar["ensureAddBtn" .. i]:setTouchEnabled(true)

				self.controlBar["ensureAddBtn" .. i]:addTouchEventListener(function ( sender, eventType )
					if eventType == ccui.TouchEventType.ended then
						zc.socket.sendTable({m = "texas_holdem_add", num = ensureAddnum[i]})
					end
				end)

			end
		end

		if self.cur_money == 0 then
			self.controlBar.addChipBtn:loadTexture("res/Image/game/bet_btn.png")
		else
			self.controlBar.addChipBtn:loadTexture("res/Image/game/add_btn.png")
		end

	elseif updateInfo.state and updateInfo.state == 2 then
		for i,v in ipairs(self.controlBar.autoBtnList) do
			v:setVisible(false)
			v:setBrightStyle(0)
			v.selected = 0
		end

		if zc.popIsActive( "src/app/pop/exactAddBetPop" ) then
			zc.closePop("src/app/pop/exactAddBetPop")
		end

		--显示预选框 隐藏加注列表
		for i,v in ipairs(self.controlBar.btnList) do
			v:setVisible(false)
		end
		self.controlBar.cancleBtn:setVisible(false)

		if self.dataPool.state ~= GAME_STATE_END then
			self.controlBar.auto_pickBtn:setVisible(true)
			if self.equal_money == 0 then
				self.controlBar.auto_checkBtn:setVisible(true)
			elseif self.equal_money > 0 then
				self.controlBar.auto_CallBtn:setVisible(true)
				self.controlBar.auto_CallBtn:getChildByTag(33):setString(zc.getWan(self.equal_money,4))
			end
		end

	elseif updateInfo.state and updateInfo.state ~= 3 and updateInfo.state ~= 2 then
		if zc.popIsActive( "src/app/pop/exactAddBetPop" ) then
			zc.closePop("src/app/pop/exactAddBetPop")
		end
		for i,v in ipairs(self.controlBar.btnList) do
			v:setVisible(false)
		end
		self.controlBar.cancleBtn:setVisible(false)

		for i,v in ipairs(self.controlBar.autoBtnList) do
			v:setVisible(false)
			v:setBrightStyle(0)
			v.selected = 0
		end
	end

	--加时倒计时
	if updateInfo.overtime and updateInfo.overtime - 1 > 0 then
		self:startProgress(updateInfo.overtime - 1)
	end

	if myInfo.state == PLAYER_STATE_WATCH_9 and  updateInfo.seat and updateInfo.seat ~= 0 then -- 重新坐下
		self.ownSeatIndex = updateInfo.seat

		self:addPlayer( myInfo )
		self:updatePlayerSeat()
	end

	if updateInfo.state then

		-- 准备状态
		if updateInfo.state == PLAYER_STATE_NEXT_8 then -- 等待下一局
			self.waitingLabel:setString(zc.lang("PlayGround_NextField", "等待下一局开始..."))
			self.waitingLabel:setVisible(true)

			if self.type == zc.GAME_TYPE_TEXAS_FRIEND then -- 好友房
				self.inviteBtn:setVisible(true)
			else
				self.inviteBtn:setVisible(false)
			end

		else
			self.waitingLabel:setVisible(false)

			-- 好友房
			self.inviteBtn:setVisible(false)
			
		end

		if updateInfo.state == PLAYER_STATE_WATCH_9 then -- 观战（起身）
			zc.playEffect("chairStandSound")
			self:deletePlayer(myInfo.base_info.roleid, false)
		end
	end
end

-- 自动弃牌和自动看牌倒计时
function PlayGround:startProgress( time )
	-- 先停止
	self:stopProgress(  )

	local percent = 0
	self.progressTimer = scheduler.scheduleUpdateGlobal(function ( delta )
		if nil == self.progressImg then
			return
		end
		if percent >= time then
			self:stopProgress()
		else
			percent = percent + delta
			self.progressImg:setPercentage(100 - percent * 100/time )
		end
	end)
end

-- 停止自动弃牌和自动看牌倒计时
function PlayGround:stopProgress(  )
	if self.progressTimer then
		scheduler.unscheduleGlobal(self.progressTimer)
	end
	if self.progressImg then
		self.progressImg:setPercentage(0)
	end
end

--更新本轮下注筹码，更新筹码背景显示
function PlayGround:updateChipImgBg(seatIndex,roundMoney)
	--不传参数就是更新所有
	if not seatIndex then
		--先移除所有在刷新
		for i=1,SEAT_NUM_LIMIT do
			self.RoundLayput[i]:setVisible(false)
			self.RoundLayput[i]:getChildByTag(33):setString(0)
		end

		for i,v in ipairs(self.playerList) do

			if not tolua.isnull(v) and v.round_money > 0 then
				self.RoundLayput[v.seatIndex]:setVisible(true)
				self.RoundLayput[v.seatIndex]:getChildByTag(33):setString(v.round_money)
			end
		end
		return
	end

	if roundMoney > 0 then
		self.RoundLayput[seatIndex]:setVisible(true)
		self.RoundLayput[seatIndex]:getChildByTag(33):setString(roundMoney)
	end

end

-- 更新庄家
function PlayGround:updateBanker( seatIndex )
	self.dealerImg:setVisible(true)
	local destPos = cc.p(cc.p(SEAT_POS[seatIndex]["IN"].x + BANKER_POS_OFFSET[seatIndex].x, SEAT_POS[seatIndex]["IN"].y + BANKER_POS_OFFSET[seatIndex].y))
	local moveAct = cc.EaseExponentialOut:create(cc.MoveTo:create(0.5, destPos))
	self.dealerImg:runAction(moveAct)
end

-- 更新底池总数
function PlayGround:updatePotLabel( _pot )
	self.potImg:setString(zc.lang("PlayGround_PoolInfo", "底池: ") .. zc.getWan(_pot,4))
	if _pot > 0 then
		self.potImg:setVisible(true)
	else
		self.potImg:setVisible(false)
	end
end

------------------------------保险模式------------------------------
--初始化保险
function PlayGround:initinsuranceLayout()
	self.insuranceSelected = {}

	if self.insuranceLayout then
		self.insuranceLayout:removeFromParent()
		self.insuranceLayout = nil
	end

	if zc.popIsActive( "src/app/pop/buyInsurancePop" ) then
		zc.closePop("src/app/pop/buyInsurancePop")
	end

	for i,v in ipairs(self.dataPool.insurance) do
		if v.roleid == PlayerData.base.roleid then
			local outs = self:getOutsPokers(v.poolid)
			if #outs > 0 then
				self.insuranceSelected[#self.insuranceSelected + 1] = {["poolid"] = v.poolid,["outs"] = outs,["amount"] = 0}
			end
		end
	end

	for i,v in ipairs(self.insuranceSelected) do
		v.choose_outs = {}
		for ii,vv in ipairs(v.outs) do
			v.choose_outs[ii] = vv
		end
	end

	local insuranceLayout = zc.createLayout({
		ap = cc.p(0.5,0.5),
		pos = cc.p(zc.cx,zc.cy),
		zOrder = Z_ORDER_INSURANCE_LAYOUT,
		size = cc.size(zc.width,zc.height),
		parent = self,
		})
	self.insuranceLayout = insuranceLayout

	zc.createImageView({
		src = "res/Image/game/insurance/safe_img.png",
		pos = cc.p(zc.width / 2,800),
		parent = self.insuranceLayout,
		scale9 = true,
		size = cc.size(zc.width,200),
		})

	--选择操作
	local operatorBg = zc.createImageView({
		src = "res/Image/game/insurance/select_bg.png",
		pos = cc.p(zc.width / 2,220),
		parent = self.insuranceLayout,
		})

	-- 放弃
	zc.createImageView({
		src = "res/Image/game/insurance/capitalgive_up.png",
		pos = cc.p(operatorBg:getContentSize().width/2 - 180,operatorBg:getContentSize().height/2),
		parent = operatorBg,
		listener = function ( sender, eventType )
			if eventType == ccui.TouchEventType.ended then
				for i,v in ipairs(self.insuranceSelected) do
					v.choose_outs = {}
					for ii,vv in ipairs(v.outs) do
						v.choose_outs[ii] = vv
					end
					v.amount = 0
				end

				local sendJson = json.encode(self.insuranceSelected)
				zc.socket.sendTable({m = "texas_holdem_buy_insurance",insurance = sendJson})
			end
		end
		})

	--计算所有池子的保本和
	local selfAdd = 0
	for i,v in ipairs(self.insuranceSelected) do
		local selfhaveAdd,selfpercent = self:getInsuranceBets(v.poolid)
		selfAdd = selfAdd + math.ceil(selfhaveAdd/selfpercent)
	end
	--保本/全部保本
	local capitalBtn = zc.createButton({
		text = zc.getWan(selfAdd,4),
		fontSize = 26,
		fontColor = cc.c3b(9,40,65),
		normal = "res/Image/game/insurance/capital_img.png",
		pressed = "res/Image/game/insurance/capital_img.png",
		pos = cc.p(zc.width/2 + 180 ,220),
		parent = self.insuranceLayout,
		tag = 33,
		listener = function ( sender, eventType )
			if eventType == ccui.TouchEventType.ended then
			local self_money = self:getPlayerById(PlayerData.base.roleid).self_money
				if selfAdd > self_money then
					zc.tipInfo("金币不足")
					return
				end
				for i,v in ipairs(self.insuranceSelected) do
					v.choose_outs = {}
					for ii,vv in ipairs(v.outs) do
						v.choose_outs[ii] = vv
					end
					local selfhaveAdd,selfpercent = self:getInsuranceBets(v.poolid)
					v.amount = math.ceil(selfhaveAdd/selfpercent)
				end
				local sendJson = json.encode(self.insuranceSelected)
				zc.socket.sendTable({m = "texas_holdem_buy_insurance",insurance = sendJson})
			end
		end
		})
	--更新保险池UI
	for i=1,#self.insuranceSelected do
		self:updateInsuranceLayout(i,self.insuranceSelected[i])
	end

end

--得到单个池子自己的下注量和保本赔率
function PlayGround:getInsuranceBets(poolid)
 	local poolData = nil
	for i,v in ipairs(self.insuranceSelected) do
		if v.poolid == poolid then
			poolData = v
		end
	end

	--下注量
	local selfBetNum = 0
	for k,v in pairs(self.bet_pools) do
		if v.poolid == poolid then
			for kk,vv in pairs(v.bet_info or {}) do
				if vv.roleid == PlayerData.base.roleid then
					selfBetNum = vv.bet
					break
				end
			end
			break
		end
	end
	--赔率
	local theOdds = 0

	for i,v in ipairs(self.insurance_mutiple_json) do
		if tonumber(v.outs) == #poolData.choose_outs then
			theOdds = math.floor(tonumber(v.mutiple))/100 - 1
			break
		end
	end
	return selfBetNum,theOdds

end

function PlayGround:getBetPoolNum( poolid )
	--得到当前底池数
	local betNum = 0
	for k,v in pairs(self.bet_pools) do
		if v.poolid == poolid then
			for kk,vv in pairs(v.bet_info or {}) do
				betNum = betNum + vv.bet
			end
			break
		end
	end
	return betNum
end

--更新保险池UI
function PlayGround:updateInsuranceLayout(index,poolData)
	if self.insuranceLayout == nil then return end

	self.insuranceSelected[index] = poolData

	local sliderPos = {cc.p(zc.cx,310),cc.p(zc.cx - 170,310),cc.p(zc.cx + 170,310)}

	--得到当前底池数
	local betNum = self:getBetPoolNum(poolData.poolid)
	if self.insuranceLayout:getChildByTag(poolData.poolid) then
		self.insuranceLayout:removeChildByTag(poolData.poolid)
	end
	if poolData.amount > 0 then
		local haveCapitalBtn = zc.createButton({
		normal = "res/Image/game/insurance/have_select.png",
		pressed = "res/Image/game/insurance/have_select.png",
		pos = sliderPos[index],
		ap = cc.p(0.5,0),
		tag = poolData.poolid,
		parent = self.insuranceLayout,
		listener = function ( sender, eventType )
			if eventType == ccui.TouchEventType.ended then
				zc.createPop("src/app/pop/buyInsurancePop",{poolIndex = index,poolData = poolData,ground = self})
			end
		end
		})
		--底池
		zc.createLabel({
			text = zc.getWan(betNum,4),
			fontSize = 24,
			color = cc.c3b(255, 255, 255),
			pos = cc.p(haveCapitalBtn:getContentSize().width/2+20,145),
			parent = haveCapitalBtn
		})
		--赔率
		zc.createLabel({
			text = "赔付",
			fontSize = 20,
			color = cc.c3b(255, 255, 255),
			pos = cc.p(haveCapitalBtn:getContentSize().width/2, 110),
			parent = haveCapitalBtn
		})

		local selfhaveAdd,selfpercent = self:getInsuranceBets(poolData.poolid)
		zc.createLabel({
			text = zc.getWan(math.ceil(poolData.amount)*selfpercent,4),
			fontSize = 30,
			color = cc.c3b(238, 116, 0),
			pos = cc.p(haveCapitalBtn:getContentSize().width/2, 70),
			tag = 1,
			parent = haveCapitalBtn
		})
		--outs
		zc.createLabel({
			text = "支付",
			ap = cc.p(0,0.5),
			fontSize = 20,
			color = cc.c3b(255, 255, 255),
			pos = cc.p(haveCapitalBtn:getContentSize().width/2-60, 35),
			parent = haveCapitalBtn
		})
		zc.createLabel({
			text = zc.getWan(math.ceil(poolData.amount),4),
			fontSize = 22,
			color = cc.c3b(255, 255, 255),
			pos = cc.p(haveCapitalBtn:getContentSize().width/2+30, 35),
			parent = haveCapitalBtn
		})
	else
		local capitalBtn = zc.createButton({
		normal = "res/Image/game/insurance/select_default_n.png",
		pressed = "res/Image/game/insurance/select_default_p.png",
		pos = sliderPos[index],
		ap = cc.p(0.5,0),
		tag = poolData.poolid,
		parent = self.insuranceLayout,
		listener = function ( sender, eventType )
			if eventType == ccui.TouchEventType.ended then
				zc.createPop("src/app/pop/buyInsurancePop",{poolIndex = index, poolData = poolData,ground = self})
			end
		end
		})

		--底池
		zc.createLabel({
			text = zc.getWan(betNum,4),
			fontSize = 24,
			color = cc.c3b(255, 255, 255),
			pos = cc.p(capitalBtn:getContentSize().width/2, 120),
			parent = capitalBtn
		})
		--赔率
		zc.createLabel({
			text = "赔率",
			fontSize = 20,
			ap = cc.p(0,0.5),
			color = cc.c3b(255, 255, 255),
			pos = cc.p(capitalBtn:getContentSize().width/2-50, 75),
			parent = capitalBtn
		})

		local selfhaveAdd,selfpercent = self:getInsuranceBets(poolData.poolid)

		zc.createLabel({
			text = selfpercent,
			fontSize = 30,
			color = cc.c3b(238, 116, 0),
			pos = cc.p(capitalBtn:getContentSize().width/2+30, 75),
			tag = 1,
			parent = capitalBtn
		})

		--outs
		zc.createLabel({
			text = "Outs",
			ap = cc.p(0,0.5),
			fontSize = 20,
			color = cc.c3b(255, 255, 255),
			pos = cc.p(capitalBtn:getContentSize().width/2-50, 35),
			parent = capitalBtn
		})
		zc.createLabel({
			text = #poolData.choose_outs,
			fontSize = 22,
			color = cc.c3b(255, 255, 255),
			pos = cc.p(capitalBtn:getContentSize().width/2+25, 35),
			parent = capitalBtn
		})

	end

	--更新下面保险按钮
	--得到自己总共投保的数值
	local selfAdd = 0
	for i,v in ipairs(self.insuranceSelected) do
		selfAdd = selfAdd + v.amount
	end
	if not tolua.isnull(self.insuranceLayout:getChildByTag(11)) then
		self.insuranceLayout:removeChildByTag(11,true)
	end

	if selfAdd > 0 then
		-- 确认购买
		local selectSlidePool = zc.createImageView({
			src = "res/Image/game/insurance/confirm_img.png",
			pos = cc.p(zc.width/2 ,220),
			parent = self.insuranceLayout,
			tag = 11,
			listener = function ( sender, eventType )
				if eventType == ccui.TouchEventType.ended then
					local sendJson = json.encode(self.insuranceSelected)
					zc.socket.sendTable({m = "texas_holdem_buy_insurance",insurance = sendJson})
				end
			end
			})
		zc.createLabel({
			text = zc.getWan(math.ceil(selfAdd),4),
			fontSize = 26,
			color = cc.c3b(9, 40, 65),
			pos = cc.p(selectSlidePool:getContentSize().width/2,selectSlidePool:getContentSize().height/2),
			parent = selectSlidePool,
			})
	else
		-- 选择分池
		local selectSlidePool = zc.createImageView({
			src = "res/Image/game/insurance/select_pool_img.png",
			pos = cc.p(zc.width/2 ,220),
			parent = self.insuranceLayout,
			tag = 11,
			})
		zc.createLabel({
			text = "0",
			fontSize = 26,
			color = cc.c3b(9, 40, 65),
			pos = cc.p(selectSlidePool:getContentSize().width/2,selectSlidePool:getContentSize().height/2),
			parent = selectSlidePool,
			})
	end

end


--得到所有outs牌
function PlayGround:getOutsPokers( poolid )

	local poolHeros = {}
	for i,v in ipairs(self.bet_pools) do
		if v.poolid == poolid then
			for ii,vv in ipairs(v.bet_info) do
				poolHeros[#poolHeros+1] = vv.roleid
			end
			break
		end
	end

	local outsPokers = {}

	local allPokers = zc.getAllPokers()
	local myCards = self:getPlayerInfoById(PlayerData.base.roleid).cards
	local otherCards = {}

	for i,v in ipairs(self.playerList) do
		zc.tableDel(allPokers, v.cards)
		if v.roleid ~= PlayerData.base.roleid and v.state ~= 4 and table.inArray(v.roleid,poolHeros) then
			table.insert(otherCards, v.cards)
		end
	end

	for i,v in ipairs(self.deskPokers) do
		if not tolua.isnull(v) then
			table.removebyvalue(allPokers, v.card)
		end
	end

	if #self.deskPokers <=2 then --此时拿出一张和手牌组合比较大小

		for i,v in ipairs(allPokers) do
			local selfPoker = {}
			table.insert(selfPoker,v)
			for ii,vv in ipairs(myCards) do
				table.insert(selfPoker,vv)
			end
			for ii,vv in ipairs(self.deskPokers) do
				table.insert(selfPoker,vv.card)
			end
			--得到自己牌的牌型 权重
			local selfType,selfWeight = zc.getFivePokerType(selfPoker)
			for ii,vv in ipairs(otherCards) do
				local otherPokers = {}
				table.insert(otherPokers,v)
				for iii,vvv in ipairs(vv) do
					table.insert(otherPokers,vvv)
				end
				for ii,vv in ipairs(self.deskPokers) do
					table.insert(otherPokers,vv.card)
				end
				--得到其他人的牌型权重
				local otherType,otherWeight = zc.getFivePokerType(otherPokers)
				if otherWeight > selfWeight then
					if not table.inArray(v, outsPokers) then
						outsPokers[#outsPokers+1] = v
					end
				end
			end
		end

	else --此时拿出一张和手牌组合,再N选5 比大小
		for i,v in ipairs(allPokers) do
			local selfPoker = {}
			table.insert(selfPoker,v)
			for ii,vv in ipairs(myCards) do
				table.insert(selfPoker,vv)
			end
			for ii,vv in ipairs(self.deskPokers) do
				table.insert(selfPoker,vv.card)
			end
			--得到自己牌的牌型 权重
			local selfType,selfFinalPoker = zc.selectPoker(selfPoker)
			local selfType,selfWeight = zc.getFivePokerType(selfFinalPoker)
			for ii,vv in ipairs(otherCards) do
				local otherPokers = {}
				table.insert(otherPokers,v)
				for iii,vvv in ipairs(vv) do
					table.insert(otherPokers,vvv)
				end
				for ii,vv in ipairs(self.deskPokers) do
					table.insert(otherPokers,vv.card)
				end
				--得到其他人的牌型权重
				local otherType,otherFinalPoker = zc.selectPoker(otherPokers)
				local otherType,otherWeight = zc.getFivePokerType(otherFinalPoker)
				if otherWeight > selfWeight then
					if not table.inArray(v, outsPokers) then
						outsPokers[#outsPokers+1] = v
					end
				end
			end
		end
	end

	return outsPokers
end


-----------------------------保险模式 end-----------------------------


function PlayGround:onEventToBackground(time)
	self.stop = true
	zc.showMask(0, 0)
end

-- 断线重连 重新刷数据
function PlayGround:onTCPReconnected(time)
	self:clean()
	self:removeAllChildren(true)
	self:ctor()

end


-- 打开语音界面
function PlayGround:createVoiceLayout( )

    self.voiceLayout = zc.createLayout({
        parent = self,
        zOrder = Z_ORDER_VOICE_LAYOUT
        })

    zc.createImageView({
        src = "res/Image/game/voice_bg.png",
        ap = cc.p(0.5, 0),
        pos = cc.p(zc.width - 80, 0),
        parent = self.voiceLayout,
        })

    local timeBg = zc.createImageView({
        src = "res/Image/game/void_time_bg.png",
        pos = cc.p(zc.cx, 50),
        parent = self.voiceLayout,
        })

    local timeLabel = zc.createLabel({
        text = VOICE_LIMIT_TIME,
        pos = cc.p(timeBg:getContentSize().width/2, timeBg:getContentSize().height/2),
        zOrder = 1,
        parent = timeBg
        })

    -- 倒计时
    local passTimer = zc.createProgressTimer({
        src = "res/Image/game/voice_time_progress.png",
        midPoint = cc.p(0, 0.5),
        barChangeRate = cc.p(1, 0),
        type = cc.PROGRESS_TIMER_TYPE_BAR,
        pos = cc.p(timeBg:getContentSize().width/2, timeBg:getContentSize().height/2),
        percent = 0,
        parent = timeBg
        })

    self.voiceLayout.passTime = VOICE_LIMIT_TIME

    local passAction = cc.ProgressTo:create(self.voiceLayout.passTime, 100)
    passTimer:runAction(passAction)

    local timeSeq = cc.Sequence:create({cc.DelayTime:create(1.0), cc.CallFunc:create(function ( sender )
    	if self.voiceLayout.passTime <= 0 then
    		-- zc.tipInfo("开始发送语音")
            if GotyeApi then
                GotyeApi:stopTalk()
            end
    		self:removeVoiceLayout()
    		return
    	end

        self.voiceLayout.passTime = self.voiceLayout.passTime - 1
        timeLabel:setString(self.voiceLayout.passTime)
    end)})

    passTimer:runAction(cc.RepeatForever:create(timeSeq))

    -- 取消发送
    local cancelImg = zc.createImageView({
        src = "res/Image/game/voice_cancel_n.png",
        ap = cc.p(1, 0),
        pos = cc.p(zc.width, 190),
        parent = self.voiceLayout
        })

    self.voiceLayout.cancelImg = cancelImg

    local cancelLabel = zc.createLabel({
        text = "上滑取消发送",
        fontSize = 24,
        pos = cc.p(cancelImg:getContentSize().width / 2 + 5, cancelImg:getContentSize().height / 2),
        parent = cancelImg
        })

end

-- 关闭语音界面
function PlayGround:removeVoiceLayout( )
    if self.voiceLayout then
        self.voiceLayout:removeFromParent()
        self.voiceLayout = nil
    end
end

function PlayGround:clean(  )

	for i,v in ipairs(self.playerList) do
		v:removeFromParent()
		v = nil
	end

	for i=1,24 do
		display.unloadAnim("res/Image/main/emotion/effect/", "emotion_" .. (i-1))
	end

	display.loadAnim("res/Image/effect/", "effect_all_in", 1, 12, 1/12)
	display.unloadAnim("res/Image/effect/", "effect_operate")
	display.unloadAnim("res/Image/game/roleinfo/effect/", "toast_emotion_1")
	display.unloadAnim("res/Image/game/roleinfo/effect/", "toast_emotion_2")
	display.unloadAnim("res/Image/game/roleinfo/effect/", "toast_emotion_3")
	display.unloadAnim("res/Image/game/roleinfo/effect/", "toast_emotion_4")
	display.unloadAnim("res/Image/game/roleinfo/effect/", "toast_emotion_5")
	display.unloadAnim("res/Image/game/roleinfo/effect/", "toast_emotion_6")
	display.unloadAnim("res/Image/game/roleinfo/effect/", "toast_emotion_7")
	display.unloadAnim("res/Image/game/roleinfo/effect/", "toast_emotion_8")
	display.unloadAnim("res/Image/game/roleinfo/effect/", "toast_emotion_8_pre")
	display.unloadAnim("res/Image/game/roleinfo/effect/", "toast_emotion_9")
	display.removeArmatureFileInfo("res/Image/effect/wineffect/WIN.csb")
end


return PlayGround