
--德州扑克椭圆旋转
local CircleView = class("CircleView", ccui.Layout)

function CircleView:ctor( params )
    self.ovalW   = params.width and params.width/2 or 660/2         --椭圆宽
    self.ovalH   = params.height and params.height/2 or 1000/2      --椭圆高
    self.ground = params.ground
    self:init()
end

function CircleView:init()

    self.nodeList = {}
    self.angle          = 0                 --当前角度
    self.aimAngle       = 0                 --目标角度
    self.bUpdate        = false             --是否更新
    self.rotateSpeed    = 300               --旋转速度
    self.rotateDir  = -1                    --旋转方向
    
    local function update(delta)
        self:update(delta)
    end
    self:scheduleUpdateWithPriorityLua(update, 0)
end

function CircleView:getPointByAngle(angle)
    local _angle = 3.141592 / 180 * angle
    local x      = self.ovalW * math.cos(_angle) + zc.cx
    local y      = self.ovalH * math.sin(_angle) + zc.cy
    return x,y
end

--桌子上限人数
function CircleView:setMaxDesk(_num)
    self.maxDesk = _num
end

--是否旋转
function CircleView:Isrotate(isRotate)
    self.bUpdate = isRotate
end

--目标角度
function CircleView:setEndAngle(angle)
    self.aimAngle = angle
end

--当前角度
function CircleView:setStartAngle(angle)
    self.angle = angle
end

function CircleView:addItem(_player,_key)
    if self.nodeList[_key] then
        self.nodeList[_key] = _player
    else
        table.insert(self.nodeList,_key,_player)
    end
    self:addChild(_player)
end

function CircleView:deleteItem(_player,_key)

end

function CircleView:updateAllItems(dAngle)
    if dAngle then 
        self.angle  = self.angle + dAngle
    end 
    local dAngle    = 360 / self.maxDesk
    local _node     = nil
    local x,y       = 0,0
    for i = 1,#self.nodeList do
        _node       = self.nodeList[i]
        x,y         = self:getPointByAngle(self.angle - (i - 1) * dAngle)
        _node:setPosition(x,y)
    end
end

function CircleView:isCanRotate()
    if self.aimAngle == self.angle then
        self.ground:updateCirclerState(3)
        return false
    end
    return true 
end

function CircleView:update(delta)
    if not self:isCanRotate() or not self.bUpdate   then 
        return
    end 
    local angle = self.rotateSpeed * self.rotateDir * delta
    if math.abs(angle) > math.abs(self.aimAngle - self.angle) then
        angle  = self.aimAngle - self.angle 
        self.bUpdate = false
    end 
    self:updateAllItems(angle)
end 

return CircleView

--[[
                            _ooOoo_
                           o8888888o
                           88" . "88
                           (| -_- |)
                            O\ = /O
                        ____/`---'\____
                      .   ' \\| |// `.
                       / \\||| : |||// \
                     / _||||| -:- |||||- \
                       | | \\\ - /// | |
                     | \_| ''\---/'' | |
                      \ .-\__ `-` ___/-. /
                   ___`. .' /--.--\ `. . __
                ."" '< `.___\_<|>_/___.' >'"".
               | | : `- \`.;`\ _ /`;.`/ - ` : | |
                 \ \ `-. \_ __\ /__ _/ .-` / /
         ======`-.____`-.___\_____/___.-`____.-'======
                            `=---='

         .............................................
                  佛祖镇楼                  BUG辟易
          佛曰:
                  写字楼里写字间，写字间里程序员；
                  程序人员写程序，又拿程序换酒钱。
                  酒醒只在网上坐，酒醉还来网下眠；
                  酒醉酒醒日复日，网上网下年复年。
                  但愿老死电脑间，不愿鞠躬老板前；
                  奔驰宝马贵者趣，公交自行程序员。
                  别人笑我忒疯癫，我笑自己命太贱；
                  不见满街漂亮妹，哪个归得程序员？

]]