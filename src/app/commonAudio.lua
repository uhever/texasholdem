local audioList = {}

audioList.common = {}

audioList.bgMusic = {
    mainbgm         = "res/Audio/common/bgmusic/game_main_bg_1.mp3",
    mainbgm2        = "res/Audio/common/bgmusic/game_main_bg_2.mp3",
    mainbgm3        = "res/Audio/common/bgmusic/game_main_bg_3.mp3",
    mainbgm4        = "res/Audio/common/bgmusic/game_main_bg_4.mp3",
    --dezhou
    dz_bgm          = "res/Audio/texasholdem/bgmusic/dezhou_bg.mp3",

}

audioList.effect = {
    --common
    click           = "res/Audio/common/effect/common_click.mp3",       --按钮点击
    get_freegold    = "res/Audio/common/effect/get_freegold.mp3",    -- 在线奖励金币
    get_money1      = "res/Audio/common/effect/sound_jinbi_1.mp3",    -- 获得金币1
    get_money2      = "res/Audio/common/effect/sound_jinbi_2.mp3",    -- 获得金币2


    --dezhou
    dz_add = "res/Audio/texasholdem/effect/dz_add_1.mp3",
    dz_allin_1 = "res/Audio/texasholdem/effect/dz_allin_1.mp3",
    dz_bet = "res/Audio/texasholdem/effect/dz_bet.mp3",
    dz_deal = "res/Audio/texasholdem/effect/dz_deal.mp3",
    dz_fold_1 = "res/Audio/texasholdem/effect/dz_fold_1.mp3",
    dz_follow_1 = "res/Audio/texasholdem/effect/dz_follow_1.mp3",
    dz_reminder = "res/Audio/texasholdem/effect/dz_reminder.mp3",
    dz_updatechips = "res/Audio/texasholdem/effect/dz_updatechips.mp3",
    dz_win = "res/Audio/texasholdem/effect/dz_win.mp3",
    dz_trunpoker = "res/Audio/texasholdem/effect/dz_trunpoker.mp3",
    
    chairStandSound = "res/Audio/texasholdem/effect/chairStandSound.mp3", --站起
    chairSitSound = "res/Audio/texasholdem/effect/chairSitSound.mp3", --坐下
    cheackSound = "res/Audio/texasholdem/effect/cheackSound.mp3", --过牌
    chips_to_table = "res/Audio/texasholdem/effect/chips_to_table.mp3", --下注
    slider_top = "res/Audio/texasholdem/effect/slider_top.mp3", --下注滑倒顶
    slider = "res/Audio/texasholdem/effect/slider.mp3", --下注滑动条
}



return audioList