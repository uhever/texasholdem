--[[
    文件下载器
]]

local FileLoader = {}

-- 是否加密
local isCrypt = false

-- 是否静默下载
local isSilent = false

-- 下载是否停止（网络连接失败、账号失去登录状态）
local isStop = false

-- 下载完成的回调
local loadDoneCallback = nil
-- 下载失败、并且点了取消的时候的回调
local loadFailCallback = nil
-- 404时候的回调
-- local load404Callback = nil

-- 是否正在下载
local downloading = false

local errorRepeatNums = 2
-- 下载列表（更新后记得clean）
local updateTitle = ""
local allUpdate = {} -- 本次需要更新的资源详情
local allSize = "" -- 本次更新的文件大小
local updateInfo = ""

-- 等待下载列表
local waitingList = {}
-- 已下载列表
local finishedList = {}

local requestThreads = 3 -- 启用多少线程下载
local requestQueue = {} -- 下载队列

-- 加载进度pop
local loaderPopInstance = nil

--[[
    添加需要下载的文件
    title: 更新时界面的title
    list: 下载文件数组 [{ path: "res/Image/main/xx.jpg", url: "http://xxxx" }, ... ]
    overwrite: true/false 是否覆盖现有文件
    size: 文件大小(字符串)
    updateInfo: 更新说明
]]
function FileLoader.addFiles(title, list, overwrite, size, info)
    if(allUpdate == nil) then
        allUpdate = {}
    end

    if(waitingList == nil) then
        waitingList = {}
    end

    if(finishedList == nil) then
        finishedList = {}
    end

    if(requestQueue == nil) then
        requestQueue = {}
    end


    if(title) then
        updateTitle = title
    end
    if(size) then
        allSize = size
    end
    if(info) then
        updateInfo = info
    end

    for i = 1, #list do
        repeat

            -- Image/Main/shop/aaa.png
            local _file = list[i].path

            if(overwrite ~= nil and overwrite == false) then
                local nowCheckFile = CCFileUtils:sharedFileUtils():fullPathForFilename(_file)
                if os.exists(nowCheckFile) then
                    print("FileLoader: " .. _file .. " is exist.")
                    break
                end
            end

            table.insert(allUpdate, list[i])

            table.insert(waitingList, list[i])

        until true
    end

end

--[[
    加载主方法
    name: 模块名，根据模块名索引到文件列表
    callback: ...
]]
function FileLoader.load(callback, failCallback, objUiType, doCrypt, callback404)

    loadDoneCallback = callback
    loadFailCallback = failCallback
    -- load404Callback = callback404

    local uiType = 1 -- 0不显示 1全显示 2只显示进度条(热更新时需要)
    if(objUiType) then
        uiType = objUiType
        if uiType == 0 then
            noUI = true
        end
    end

    if(#allUpdate == 0) then
        FileLoader.launch()
    else

        -- 初始化界面
        if(noUI and noUI == true) then
            print("FileLoader: 静默下载")
        else

            -- 打开界面
            zc.createPop("src/app/pop/loaderPop", uiType)

            loaderPopInstance = zc.getPop("src/app/pop/loaderPop")
            if(updateTitle and updateTitle ~= "") then
                loaderPopInstance.setTitle(updateTitle)
            end
            if(updateInfo and updateInfo ~= "") then
                loaderPopInstance.setUpdateInfo(updateInfo)
            end
        end

        if(doCrypt and doCrypt == true) then
            isCrypt = true
        end

        FileLoader.downloadAll()
    end

end


function FileLoader.launch()

    zc.hideLoading()

    -- 清理
    FileLoader.clean(true)

    print("FileLoader: 文件下载完成")

    if(loadDoneCallback) then
        loadDoneCallback()
        loadDoneCallback = nil
    end

end

-- function FileLoader.launch404(  )
--     zc.hideLoading()

--     -- 清理
--     FileLoader.clean(true)

--     print("FileLoader: 下载错误404")

--     if(load404Callback) then
--         load404Callback()
--         load404Callback = nil
--     end
-- end

-- 清理加载器
function FileLoader.clean(isDone)

    isCrypt = false
    isSilent = false
    isStop = false

    downloading = false
    -- 暂时不清理callback。
    if(isDone == nil) then
       loadDoneCallback = nil
       -- load404Callback = nil
    end
    loadFailCallback = nil

    updateTitle = ""
    allUpdate = nil
    allSize = ""
    updateInfo = ""
    -- 等待下载列表
    waitingList = nil
    -- 已下载列表
    finishedList = nil

    requestQueue = nil -- 下载队列


    if(loaderPopInstance) then
        loaderPopInstance.clean()
    end

    loaderPopInstance = nil
end


function FileLoader.downloadAll( retryInfo )
    if(isStop == true) then
        return
    end

    if #allUpdate == #finishedList then
        print("FileLoader: 恭喜，全部下载完成")

        FileLoader.setViewPercent(100)
        FileLoader.launch()

    else

        if retryInfo then -- 重试信息

            if(downloading == false) then
                downloading = true
            end

            FileLoader.downloadResource(retryInfo, function()
                FileLoader.downloadAll()
            end)
            
        else
            for i = 1, requestThreads do
                if requestQueue[i] == nil and #waitingList > 0 then

                    if(downloading == false) then
                        downloading = true
                    end

                    local updateWord = zc.lang("loader_loading", zc.lang("fileLoader_Loading", "正在加载")) .. #finishedList .. "/" .. #allUpdate
                    if(allSize and allSize ~= "") then
                        updateWord = updateWord .. zc.lang("loader_total", "（" .. zc.lang("fileLoader_Total", "总共")) .. allSize .. "）"
                    end

                    local per = math.floor(#finishedList / #allUpdate * 100)
                    FileLoader.setViewPercent(per, updateWord)

                    requestQueue[i] = {}
                    requestQueue[i].path = waitingList[1].path
                    requestQueue[i].url = waitingList[1].url
                    requestQueue[i].index = i
                    requestQueue[i].errorTimes = 0

                    -- 等待下载列表
                    table.remove(waitingList, 1)

                    FileLoader.downloadResource(requestQueue[i], function()
                        FileLoader.downloadAll()
                    end)
                end
            end
        end

        

    end
end


function FileLoader.setViewPercent(per, word)
    print(per)
    if(isSilent == false and loaderPopInstance) then
        loaderPopInstance.setPercent(per, word)
    end
end

function FileLoader.downloadResource(requestInfo, callback)

    local request = cc.XMLHttpRequest:new()

    request.timeout = 20

    -- request.responseType = cc.XMLHTTPREQUEST_RESPONSE_ARRAY_BUFFER
    request.responseType = cc.XMLHTTPREQUEST_RESPONSE_STRING
    -- request.responseType = cc.XMLHTTPREQUEST_RESPONSE_JSON
    -- request.responseType = cc.XMLHTTPREQUEST_RESPONSE_BLOB
    -- request.responseType = cc.XMLHTTPREQUEST_RESPONSE_DOCUMENT
    

    request:open("GET", requestInfo.url)

    print("发送开始", requestInfo.url)


    local function onReadyStateChanged(  )
        if request.readyState == 4 and (request.status >= 200 and request.status < 207) then

            --保存到可写目录下的download目录
            local saveURI =  "download" .. VERSION_CACHE_ID .. "/"
            --[[
            if(self:getFileExt(filename) == "lua") then
                saveURI = saveURI .. "scripts/" .. filename
            else
                saveURI = saveURI .. "res/" .. filename
            end
            ]]
            saveURI = saveURI .. requestInfo.path
            os.prepareDir(saveURI)

            -- 新版本
            local response = request.response
            if(isCrypt) then
                response = encryptXXTEA(response, APP_XXTEA_KEY)
            end
            io.writefile(device.writablePath .. saveURI, response)


            -- local md5begin = CCCrypto:MD5(response, false)
            -- print("写入文件前：" .. md5begin)

            -- local md5str = CCCrypto:MD5File(device.writablePath .. saveURI)
            -- print("写入文件后：" .. md5str)

            -- 更新下载列表和下载队列
            table.insert(finishedList, {path = requestInfo.path, url = requestInfo.url})
            requestQueue[requestInfo["index"]] = nil
            
            -- 旧版本（在新版iOS未越狱设备上写入文件失败）
            -- request:saveResponseData(device.writablePath .. saveURI)
            if callback then callback(url) end

        else
            if(FileLoader.isDownloading() == false) then
                return
            end
            FileLoader.downloadError(requestInfo)
        end

        request:unregisterScriptHandler()
    end
    -- dump(params, "params", 5)
    -- local postString = ""
    -- for k , v in pairs(params) do
    --     if(APP_ENV == "dev") then
    --         print(k, v)
    --     end
    --     postString = postString .. k .. "=" .. v .. "&"
    -- end
    -- print("postString", postString)

    request:registerScriptHandler(onReadyStateChanged)
    request:send()

end

function FileLoader.downloadError(requestInfo)


    if(requestInfo.errorTimes < errorRepeatNums) then
        -- 错误次数+1，并重新尝试下载当前文件
        requestInfo.errorTimes = requestInfo.errorTimes + 1
        print(requestInfo.url .. "下载错误，重试（当前重试次数" .. requestInfo.errorTimes .. ")")
        FileLoader.downloadAll(requestInfo)
    else
        FileLoader.downloadErrorResult()
    end

end

-- 登录失败弹窗，让玩家选择重试或取消
function FileLoader.downloadErrorResult()
    downloading = false

    local okCallback = function()
        FileLoader.retryUpdate()
        FileLoader.downloadAll()
    end
    local cancelCallback = function()

        -- 执行取消的回调
        if(loadFailCallback) then
            loadFailCallback()
            loadFailCallback = nil
        end

        -- clean
        FileLoader.clean()

        -- 关闭遮罩
        zc.hideLoading()

        -- "TODO: 请关闭所有界面和遮罩等，返回主城"
        zc.hideMask()

        if(zc.nowScene == "src/app/scene/mainScene") then
            zc.closeAllPops()
        else
            zc.switchScene("src/app/scene/mainScene")
        end



    end

    -- 提示语
    local errorMsg = zc.lang("fileLoader_ErrorMsg", "很抱歉，由于网络不好，资源加载失败。")


    -- 在登录界面出错（热更新），只能让玩家不断重试
    if(zc.nowScene == "src/app/scene/loginScene") then
        
        zc.confirm({
            btnNum = 1,
            text = errorMsg,
            okCallback = okCallback,
            okText = zc.lang("fileLoader_Retry", "重试"),
            touchClose = false,
            })
    else
        zc.confirm({
            btnNum = 2,
            text = errorMsg,
            okCallback = okCallback,
            cancelCallback = cancelCallback,
            okText = zc.lang("fileLoader_Retry", "重试"),
            cancelText = zc.lang("fileLoader_Off", "关闭"),
            touchClose = false,
            })
    end

end



function FileLoader.isDownloading()
    return downloading
end

function FileLoader.retryUpdate()

    -- 遍历下载队列，将下载内容丢入等待列表
    for i = 1, requestThreads do
        if requestQueue[i] and waitingList then
            table.insert(waitingList, {path = requestQueue[i].path, url = requestQueue[i].url})
            requestQueue[i] = nil
        end
    end

    FileLoader.downloadAll()
end

function FileLoader.cancel()

    if(FileLoader.isDownloading() == true and isStop == false) then
        isStop = true
        FileLoader.clean()
    end
end

return FileLoader
