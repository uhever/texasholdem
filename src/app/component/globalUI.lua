--[[
	全局UI类
]]


local function onReturnCallback( sender, eventType )
	if eventType == ccui.TouchEventType.ended then
		local topPop = zc.activePops[#zc.activePops]
		if topPop then
			zc.closePop(topPop)
		end
	end
end

local function onShopCallback( sender, eventType )
	if eventType == ccui.TouchEventType.ended then
		zc.createPop("src/app/pop/shopPop")
	end
end

local function onGameInfoCallback( sender, eventType )
	if eventType == ccui.TouchEventType.ended then

        local function successCallback(data)
            if data.status == 1 then
                local curData = {}
                --重组数据
                table.sort( data.game_info, function ( a,b )
                    return a.time > b.time
                end )

                if data.game_info[1] then
                    table.insert(curData,{newday = zc.timeToDate(data.game_info[1].time, 2) })
                end

                for i,v in ipairs(data.game_info) do
                    table.insert(curData,v)
                    if data.game_info[i] and data.game_info[i+1] then
                        if  zc.timeToDate(data.game_info[i].time, 2) ~= zc.timeToDate(data.game_info[i+1].time, 2) then
                            table.insert(curData,{newday = zc.timeToDate(data.game_info[i+1].time, 2) })
                        end
                    end
                end

				local lastPop = zc.getPop("src/app/pop/mineGameInfoPop")
				if zc.popIsActive( "src/app/pop/mineGameInfoPop" ) then
					lastPop:updateUI({data = curData})
				end

            else
                zc.getErrorMsg(data.status)
            end
        end
          
        zc.loadUrl("m=texas_holdem_save_game_info", {
          }, successCallback)


	end
end

local titleInstance = nil

local titleParamsTb = {}


-- 获取title单例
function zc.getTitleInstance(  )
	if nil == titleInstance then

		titleParamsTb = {}

		local touchLayer = zc.getTouchLayer()

		local offsetX = 0
		local beginPosX = 0
		local movePosX = 0

		titleInstance = zc.createLayout({
			size = cc.size(zc.width, zc.height),
			parent = touchLayer,
			zOrder = zc.Z_ORDER_TITLE,
			})

		-- titleInstance:setSwallowTouches(false)

		titleInstance:setVisible(false)
		titleInstance:retain()


		zc.createLayout({
			size = cc.size(20, zc.height),
			parent = titleInstance,
			-- mask = true,
			listener = function ( sender, eventType )


				if eventType == ccui.TouchEventType.began then
					beginPosX = sender:getTouchBeganPosition().x

				elseif eventType == ccui.TouchEventType.moved then
					local movePosX = sender:getTouchMovePosition().x

					offsetX =  movePosX - beginPosX

					-- print("offsetX", offsetX)

				elseif eventType == ccui.TouchEventType.ended or eventType == ccui.TouchEventType.canceled then

					if offsetX > 200 then
						local topPop = zc.activePops[#zc.activePops]
						if topPop 
							and #titleParamsTb > 0 
							and titleParamsTb[#titleParamsTb].popName == topPop 
							and titleParamsTb[#titleParamsTb].params 
							and titleParamsTb[#titleParamsTb].params.left 
							then
							zc.closePop(topPop)	
						end
					end

					offsetX = 0

				end

			end
			})

		local titleBg = zc.createImageView({
			src = "res/Image/main/title_bar.jpg",
			ap = cc.p(0.5, 1),
			pos = cc.p(zc.cx, zc.height),
			parent = titleInstance,
			})
		titleInstance.titleBg = titleBg
		

		titleInstance.centerLabel = zc.createLabel({
	        text = zc.lang("globalUI_MainInterface", "主界面"), 
	        ap = cc.p(0.5, 0.5), 
	        pos = cc.p(titleBg:getContentSize().width / 2, 40), 
	        parent = titleBg,
	        color = cc.c3b(225, 205, 173),
	        fontSize = 26,
	        })

		--[[
			左边
		]]

		titleInstance.leftLayout = zc.createLayout({
			-- mask = true,
			size = cc.size(150, 80),
			pos = cc.p(0, 0),
			parent = titleBg,
			})

		titleInstance.leftImg = zc.createImageView({
			src = "res/Image/main/common_arrow_right_person.png",
			pos = cc.p(25, 40),
			parent = titleInstance.leftLayout,
			rotate = 180,
			})

		titleInstance.leftLabel = zc.createLabel({
			name = "leftLabel",
	        text = zc.lang("texasHoldemScene_Return", "返回"), 
	        ap = cc.p(0, 0.5), 
	        pos = cc.p(40, 40), 
	        parent = titleInstance.leftLayout,
	        color = cc.c3b(225, 205, 173),
	        fontSize = 26,
	        })

		--[[
			右边
		]]

		titleInstance.rightLayout = zc.createLayout({
			-- mask = true,
			size = cc.size(150, 80),
			pos = cc.p(titleBg:getContentSize().width - 150, 0),
			parent = titleBg,
			})

		titleInstance.rightImg = zc.createImageView({
			src = "res/Image/main/common_arrow_right.png",
			pos = cc.p(75, 40),
			parent = titleInstance.rightLayout,
			})


		titleInstance.rightLabel = zc.createLabel({
	        text = zc.lang("globalUI_Next", "下一步"),
	        link = true,
	        ap = cc.p(1, 0.5), 
	        pos = cc.p(130, 40), 
	        parent = titleInstance.rightLayout,
	        color = cc.c3b(225, 205, 173),
	        fontSize = 26,
	        })
	end

	if titleInstance:getReferenceCount() == 1 then
        local touchLayer = zc.getTouchLayer()
        titleInstance:setVisible(false)
        touchLayer:addChild(titleInstance)
    end

    return titleInstance

end


function zc.updateTitle( popName, params, isDelete )

	-- 获取单例
	local title = zc.getTitleInstance()

	if isDelete then
		local isNeedUpdate = false

		for i,v in ipairs(titleParamsTb) do
            if v.popName == popName then
                table.remove(titleParamsTb, i)
                isNeedUpdate = true
                break
            end
        end

        -- 不需要更新
        if not isNeedUpdate or #titleParamsTb == 0 then
        	return
        end

		params = titleParamsTb[#titleParamsTb].params
	else

		table.insert(titleParamsTb, {popName = popName, params = params})

	end

	title:setVisible(true)

	local leftCfg = params.left
	local centerCfg = params.center
	local rightCfg = params.right

	if leftCfg then

		title.leftLayout:setVisible(true)
		title.leftLayout:setTouchEnabled(true)

		if leftCfg.type then
			if leftCfg.type == "return" then
				title.leftLabel:setString(zc.lang("texasHoldemScene_Return", "返回"))
				title.leftLayout:addTouchEventListener(onReturnCallback)
			end
		elseif leftCfg.text then
			title.leftLabel:setString(leftCfg.text)
		else
			title.leftLayout:setVisible(false)
		end

	else
		title.leftLayout:setVisible(false)
	end



	if centerCfg then
		title.centerLabel:setString(centerCfg.text)
		title.centerLabel:setVisible(true)
	else
		title.centerLabel:setVisible(false)
	end



	if rightCfg then

		title.rightLayout:setVisible(true)
		title.rightLayout:setTouchEnabled(true)

		if rightCfg.type then
			if rightCfg.type == "shop" then
				title.rightImg:loadTexture("res/Image/shop/shop_main_right_icon.png")
				title.rightImg:setPosition(40, 40)
				title.rightLabel:setString(zc.lang("mine_Shop", "商店"))
				title.rightLayout:addTouchEventListener(onShopCallback)	
			elseif rightCfg.type == "gameinfo" then
				title.rightImg:loadTexture("res/Image/main/mine_profile_icon.png")
				title.rightImg:setPosition(-10, 40)
				title.rightLabel:setString(zc.lang("mine_Profile", "牌谱收藏"))
				title.rightLayout.data = rightCfg.data
				title.rightLayout:addTouchEventListener(onGameInfoCallback)	
			end
		elseif rightCfg.text then
			title.rightLabel:setString(rightCfg.text)
		else
			title.rightLayout:setVisible(false)
		end

	else
		title.rightLayout:setVisible(false)
	end

end


--[[
	BOTTOM
]]

local bottomInstance = nil

local bottomCfg = {
	{name = "record", src = "bottom_btn_record.png", pressed = "bottom_btn_record_pressed.png", posY = 8},-- posY为纵坐标偏移值
	{name = "play", src = "bottom_btn_play.png", pressed = "bottom_btn_play_pressed.png", posY = 2},
	{name = "mine", src = "bottom_btn_mine.png", pressed = "bottom_btn_mine_pressed.png", posY = 8},
}

local function onChangeTab( newIndex )
	local nowIndex = bottomInstance.selectedIndex

	bottomInstance:getChildByTag(nowIndex):getChildByName("iconImg"):loadTexture("res/Image/main/" .. bottomCfg[nowIndex].src)
	bottomInstance:getChildByTag(newIndex):getChildByName("iconImg"):loadTexture("res/Image/main/" .. bottomCfg[newIndex].pressed)

	bottomInstance.selectedIndex = newIndex

end


local function onBottomCallback( sender, eventType )
	if eventType == ccui.TouchEventType.ended then

		local name = sender:getName()
		local tag = sender:getTag()
		local nowIndex = bottomInstance.selectedIndex

		-- 点的是同一个
		if nowIndex == tag then
			return
		end

		if name == "record" then

			local successCallback = function(data)
                if(tonumber(data.status) == 1) then
                	zc.closeAllPops()
	            	zc.createPop("src/app/pop/recordPop", data) 
	            	onChangeTab(tag)
  
                else
                    zc.getErrorMsg(data.status)
                end
            end
            zc.loadUrl("m=texas_holdem_achieve", {
                }, successCallback)

			
		elseif name == "play" then
			zc.closeAllPops()
			zc.createPop("src/app/pop/enterMainPop")
			onChangeTab(tag)
			

		elseif name == "mine" then
			zc.closeAllPops()
			zc.createPop("src/app/pop/minePop")
			onChangeTab(tag)

		end

	end
end


function zc.getBottomInstance()

	if nil == bottomInstance then
		bottomInstance = zc.createImageView({
			src = "res/Image/main/bottom_bar.png",
			ap = cc.p(0.5, 0),
			pos = cc.p(zc.cx, 0),
			parent = zc.getTouchLayer(),
			zOrder = zc.Z_ORDER_TITLE,
			})
		-- !!
		bottomInstance:setVisible(false)
		bottomInstance:retain()

		local selectedIndex = bottomInstance.selectedIndex or 2
		bottomInstance.selectedIndex = selectedIndex
		

		for i,v in ipairs(bottomCfg) do

			local touchLayout = zc.createLayout({
				tag = i,
				name = v.name,
				size = cc.size(170, 120),
				ap = cc.p(0.5, 0.5),
				pos = cc.p(bottomInstance:getContentSize().width / 2 + 200 * (i-2), bottomInstance:getContentSize().height / 2 - v.posY),
				parent = bottomInstance,
				listener = onBottomCallback
				})

			local iconImg = zc.createImageView({
				name = "iconImg",
				src = "res/Image/main/" .. (i == selectedIndex and v.pressed or v.src),
				pos = cc.p(touchLayout:getContentSize().width / 2, touchLayout:getContentSize().height / 2),
				parent = touchLayout,
				})

		end
	end


	if bottomInstance:getReferenceCount() == 1 then
        local touchLayer = zc.getTouchLayer()
        bottomInstance:setVisible(false)
        touchLayer:addChild(bottomInstance)
    end

	return bottomInstance
end


local bottomParamsTb = {}

local excludePops = {
	"src/app/pop/smallLoadingPop",
	"src/app/pop/confirmPop",
	"src/app/pop/tipPop"
}

function zc.updateBottom( popName, params, isDelete )

	if table.inArray(popName, excludePops) then
		return
	end

	-- 获取单例
	local bottomBar = zc.getBottomInstance()

	if isDelete then
		local isNeedUpdate = false

		for i,v in ipairs(bottomParamsTb) do
            if v.popName == popName then
                table.remove(bottomParamsTb, i)
                isNeedUpdate = true
                break
            end
        end

        -- 不需要更新
        if not isNeedUpdate or #bottomParamsTb == 0 then
        	return
        end

		params = bottomParamsTb[#bottomParamsTb].params
	else

		table.insert(bottomParamsTb, {popName = popName, params = params})

	end

	if params then
		bottomBar:setVisible(true)
		onChangeTab( params.index )
	else
		bottomBar:setVisible(false)
	end
	
end


-- 清理
function zc.cleanGlobalUI(  )

	--[[
		Title
	]]
    if titleInstance and titleInstance:getReferenceCount() > 1 then
        titleInstance:removeFromParent()
    end

    titleParamsTb = {}

    --[[
		Bottom
    ]]
	if bottomInstance and bottomInstance:getReferenceCount() > 1 then
        bottomInstance:removeFromParent()
    end
end

