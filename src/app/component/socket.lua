--[[
    Socket
]]

-- 监听从后台恢复的处理
local lastOStime

-- 重连（从后台进入或断线重连）
local function reconnectCallFunc(  )

    if GameManager then
        GameManager:onTCPReconnected()
    end

end

local function tcpReceive( ... )
    zc.socket._receive( ... )
end

local function tcpClose( ... )
    print("tcpClose")
end

local function tcpClosed( ... )
    print("tcpClosed")
end

local function tcpConnected( ... )
    print("tcpConnected")

    -- 刷新ip地址
    zc._getIPAddress()
end

local function error( ... )
    print("socket timeOut")
end


local function toBackgroundCallback(  )
    print("toBackgroundCallback", "切换后台")

    lastOStime = os.time()

    if GameManager then
        GameManager:onEventToBackground()
    end
end

local function toForegroundCallback(  )
    print("toForegroundCallback", "切回前台")

    if(lastOStime == nil) then -- 避免安卓刚进入就触发，导致报错
        return
    end

    local nowOStime = os.time()
    local interval = nowOStime - lastOStime -- 时间间隔

    if GameManager then
        GameManager:onEventToForeground(interval)
    end

    if PlayerData and PlayerData.base then

        zc.showLoading({name = "socket"})

        if interval > 15 * 60 then
            -- 后台时间超过30分钟 走重新登录流程
            zc.logout()
        else

            zc.bPushSocketEvent = true
            
            zc.socket.connectBySchduler()

        end
    end

end



zc.socket = SocketTCP:new()

local eventDispatcher = cc.Director:getInstance():getEventDispatcher()
local _customListener1 = cc.EventListenerCustom:create("event_come_to_background", toBackgroundCallback)
eventDispatcher:addEventListenerWithFixedPriority(_customListener1, -1)

local _customListener2 = cc.EventListenerCustom:create("event_come_to_foreground", toForegroundCallback)
eventDispatcher:addEventListenerWithFixedPriority(_customListener2, -1)

local listener1 = cc.EventListenerCustom:create(SocketTCP.EVENT_DATA, tcpReceive)
local listener2 = cc.EventListenerCustom:create(SocketTCP.EVENT_CLOSE, tcpClose)
local listener3 = cc.EventListenerCustom:create(SocketTCP.EVENT_CLOSED, tcpClosed)
local listener4 = cc.EventListenerCustom:create(SocketTCP.EVENT_CONNECTED, tcpConnected)
local listener5 = cc.EventListenerCustom:create(SocketTCP.EVENT_CONNECT_FAILURE, error)

local eventDispatcher = cc.Director:getInstance():getEventDispatcher()
eventDispatcher:addEventListenerWithFixedPriority(listener1, 1)
eventDispatcher:addEventListenerWithFixedPriority(listener2, 1)
eventDispatcher:addEventListenerWithFixedPriority(listener3, 1)
eventDispatcher:addEventListenerWithFixedPriority(listener4, 1)
eventDispatcher:addEventListenerWithFixedPriority(listener5, 1)

-- 加入Event队列
table.insert(zc.EventListenerQueue, _customListener1)
table.insert(zc.EventListenerQueue, _customListener2)
table.insert(zc.EventListenerQueue, listener1)
table.insert(zc.EventListenerQueue, listener2)
table.insert(zc.EventListenerQueue, listener3)
table.insert(zc.EventListenerQueue, listener4)
table.insert(zc.EventListenerQueue, listener5)



-- 服务端推送 错误次数
zc.socket._serverPushErrorNum = 0
-- 服务端推送 请求间隔
zc.socket._serverPushInterval = 1
-- 服务端推送 定时器
zc.socket._serverPushSchduler = nil
zc.socket._serverTime = 0

-- 通知socket连接成功的消息
zc.bPushSocketEvent = false



-- 解压数据
function zc.socket.unzip(data)
    if data == nil then
        print("收到空数据")
        return {}
    end

    local zipBuf = ByteArray.new(ByteArray.ENDIAN_BIG)
    zipBuf:writeBuf(data)
    zipBuf:setPos(1)

    local resultTable = {}
    -- 注意有可能出现连包情况
    while zipBuf:getAvailable() <= zipBuf:getLen() do

        local socketLen = zipBuf:readShort()

        -- 数据可能不全
        if zipBuf:getLen() < socketLen or socketLen <= 0 or zipBuf:getAvailable() < socketLen then
            print("数据不全 or 数据长度不对")
            return resultTable
        end

        local bytes = zipBuf:readBuf(socketLen)


        if bytes == nil then
            print("数据读取失败")
            return resultTable
        end

        local uncompress = zlib.inflate()

        local inflated, eof, bytes_in, bytes_out = nil
        xpcall(function()
            inflated, eof, bytes_in, bytes_out = uncompress(bytes)
        end, function()
            print("无法预知的错误3")
        end)

        if inflated then
            resultTable[#resultTable + 1] = inflated
        end

        if zipBuf:getAvailable() == 0 then
            break
        end
    end

    return resultTable
end


-- 发送table
function zc.socket.sendTable(sendData)
    if sendData.m ~= "heart_beat" then
        dump(sendData, "sendData")
    end
    

    sendData.token = GAMETOKEN
    sendData.uid = GAMEUID

    local sendBuf = ByteArray.new(ByteArray.ENDIAN_BIG)
    sendBuf:writeStringUShort(json.encode(sendData))

    if zc.socket and zc.socket.isConnected then
        zc.socket:send(sendBuf:getPack())
    end

end

-- 提示从连
function zc.socket.confirmNetError(resultStatus)

    local _, forceLogout = zc.getErrorMsg(resultStatus)
    if forceLogout then
        -- 停止push
        zc.socket.stopServerPush()
    end
  
end

function zc.socket._receive( event )
    local netPacks = zc.socket.unzip(event.data)

    for i,v in ipairs(netPacks) do

        local continue = false
        local resultTable = json.decode(v)

        if resultTable.m == "heart_beat" then
            PlayerData.time = resultTable.stime or os.time()
        end

        if resultTable.m ~= "heart_beat" then
            dump(resultTable, "resultTable")
        end
        -- 消息可能重复
        if resultTable.sign and zc.socket.socketCatch then
            if table.inArray(resultTable.sign, zc.socket.socketCatch) then
                continue = true
                -- print("跳过了重复数据")
                zc.socket.sendTable({m = resultTable.m, sign = resultTable.sign})
            else
                zc.socket.socketCatch[#zc.socket.socketCatch + 1] = resultTable.sign
            end

        end


        if not continue then
            local resultStatus = tonumber(resultTable.status)
            -- dump(resultTable, "resultTable")
            if resultStatus == 1 then
                -- 第一次
                if resultTable.m == "tcp_connect" then
                    -- 开始连接
                    zc.socket.sendTable({m = "tcp_connect"})

                elseif resultTable.m == "tcp_connect_complete" then
                    zc.hideLoading({name = "socket"})

                    -- 重连判断
                    reconnectCallFunc()


                elseif resultTable.m == "heart_beat" then
                    -- 服务端心跳
                else

                    if resultTable.m == "server_push_msg" then 
                        -- print("给服务器回包")
                        zc.socket.sendTable({m = resultTable.m, sign = resultTable.sign})
                    end

                    zc.updateUserData(resultTable)
                end

            else
                
                zc.socket.confirmNetError(resultStatus)


            end
        end
    end

end


-- 开始连接
function zc.socket.startServerPush()

    if not zc.socket.isConnected then

        zc.showLoading({name = "socket"})

        zc.socket:connect(SERVERADDR, tonumber(SERVERTCPPORTS[math.random(1, #SERVERTCPPORTS)]))

        print("SOCKET_HOST:" .. zc.socket.host)
        print("SOCKET_PORT:" .. zc.socket.port)
    end
end


-- 循环连接（包含心跳）
function zc.socket.scheduleConnect()

    zc.socket.startServerPush()

    -- 心跳
    if not zc.socket._serverPushSchduler then

        zc.socket._serverPushSchduler = scheduler.scheduleGlobal(function ()
            -- 定时器可能和清空playerData不同步
            if zc.socket._serverPushSchduler == nil then
                return
            end

            -- 测试SOSCKET
            if IS_TEST_SOCKET == true then
                local touchLayer = zc.getTouchLayer()
                local socketTestLabel = touchLayer.socketTestLabel
                if not socketTestLabel then
                    socketTestLabel = zc.createLabel({
                        -- color = display.COLOR_RED, 
                        fontSize = 30, 
                        zOrder = zc.Z_ORDER_SMALL_LOADING, 
                        ap = cc.p(0, 1), 
                        alignH = cc.TEXT_ALIGNMENT_LEFT,
                        alignV = cc.VERTICAL_TEXT_ALIGNMENT_TOP,
                        pos = cc.p(50, zc.height - 100),
                        parent = touchLayer,
                        })
                    touchLayer.socketTestLabel = socketTestLabel -- reg
                end

                socketTestLabel:setString("SOCKET:\nSTATE:"..(zc.socket.isConnected and "TRUE" or "FALSE").."\nHOST:"..zc.socket.host.."\nPORT:"..zc.socket.port)

            end

            
            -- 增加逻辑 心跳速度和动画显示速度无关
            zc.socket._serverTime = zc.socket._serverTime + 1/cc.Director:getInstance():getScheduler():getTimeScale()


            if zc.socket.isConnected then

                zc.hideLoading({name = "socket"})

                if zc.socket._serverTime >= 5 then
                    zc.socket._serverTime = 0
                    zc.socket.sendTable({m = "heart_beat", time = os.time()})

                    zc.socket._serverPushErrorNum = 0
                    zc.socket._serverPushInterval = 1
                end

                -- 重连判断
                if zc.bPushSocketEvent then
                    zc.bPushSocketEvent = false

                    reconnectCallFunc()
                end

                
            else

                zc.bPushSocketEvent = false
                
                zc.socket._serverPushErrorNum = zc.socket._serverPushErrorNum + 1
                if zc.socket._serverPushErrorNum > zc.socket._serverPushInterval then
                    zc.socket._serverPushInterval = zc.socket._serverPushInterval * 2
                    print("开始重连...", zc.socket._serverPushErrorNum, zc.socket._serverPushInterval)

                    if GameManager then
                        GameManager:onTCPDisconnect(zc.socket._serverPushErrorNum)
                    end
                    
                    if zc.socket._serverPushInterval > 30 then
                        
                        zc.socket.confirmNetError(302)
                    else

                        zc.socket.startServerPush()
                    end
                end

            end

        end, 1)
    end
end



function zc.socket.connectBySchduler()

    zc.socket._serverTime = 5
    zc.socket._serverPushInterval = 1
    zc.socket._serverPushErrorNum = 0
    
end


function zc.socket.stopServerPush()

    if zc.socket.isConnected then
        zc.socket:close()
    end
    zc.socket:disconnect()

    zc.socket._serverPushErrorNum = 0
    zc.socket._serverPushInterval = 1

    if(zc.socket._serverPushSchduler) then
        scheduler.unscheduleGlobal(zc.socket._serverPushSchduler)
        zc.socket._serverPushSchduler = nil
        zc.socket._serverTime = 0
    end

    zc.hideLoading({name = "socket"})

    zc.bPushSocketEvent = false

end

