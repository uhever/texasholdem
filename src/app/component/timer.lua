--[[
    核心事件对象
]]

local Timer = class("Timer")

-- interval 时间间隔，默认单位为秒
function Timer:ctor(interval)
    self.interval = interval or 1
    self.thread = nil
    self.isPause = false
    self.lostTime = 0

    self.queue = {}
end

function Timer:start()
    self.thread = cc.Director:getInstance():getScheduler():scheduleScriptFunc(handler(self, self._run), 0, false)
    print("Timer.star()")
end


function Timer:stop()
    if(self.thread ~= nil) then
        cc.Director:getInstance():getScheduler():unscheduleScriptEntry(self.thread)
        self.thread = nil
    end
    print("Timer.stop()")
end


function Timer:pause()
    self.isPause = true
    self:stop()
end

function Timer:resume()
    self.isPause = false
    self:start()
end

function Timer:_run(ft)
    self.lostTime = self.lostTime + ft
    if self.lostTime < self.interval then return end

    self.lostTime = self.lostTime - self.interval
    for i = 1, #self.queue do
        local obj = self.queue[i]
        if(obj == nil) then
            -- dump(self.queue)
        else
            obj:tick()                
        end
    end
end

function Timer:add(child)
    if(child ~= nil and child.id ~= nil and child.tick ~= nil) then
        table.insert(self.queue, child)
        print("Timer.add: " .. child.id)
    else
        print("Timer: child is error")
    end

end

function Timer:remove(child)
    for i = #self.queue, 1, -1 do -- 需从后往前删除
        print(i, self.queue[i].id, child.id)
        if(self.queue[i].id == child.id) then
            table.remove(self.queue, i)
            print("Timer.remove: " .. child.id)
        end
    end
end

return Timer