--[[
	亲加云语音聊天
]]
if not GotyeTool then
	return
end

GotyeApi = GotyeTool:getInstance()

-- 初始化SDK
GotyeApi:init()

-- 注册回调(单例回调，同时存在一个)
GotyeApi:registerEventHandler(function ( event )
    dump(event, "event")

    if event.name == "onPlayStart" then
    	print("开始播放")
    end

    if event.name == "onPlaying" then
    	print("正在播放")
    	print("event.position", event.position)
    end

    if event.name == "onPlayStop" then
    	print("结束播放")
    end

end)

-- 取消回调
-- GotyeApi:unregisterEventHandler()

