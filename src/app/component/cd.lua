
-- 游戏主定时器，每秒走一下
zc.mainTimer = zc.require("src/app/component/timer").new(1)
zc.mainTimer:start()

-- CD类的集合，每个定义的类都需要保存到这里
local cdList = {}

-- CD核心对象，所有的倒计时实例都保存在这里
local cdQueue = {}

-- 简单cd类型
local cdQueueSimple = {}
-- 监听从后台恢复的处理
local lastOStime
local function toBackgroundCallback()
    print("event_come_to_background")
    lastOStime = os.time()
    -- 声音处理

    if zc.bgMusicOpen == true  then
        zc.pauseMusic()
    end
    zc.pauseAllEffects()
end
local function toForegroundCallback()
    print("toForegroundCallback")
    if(lastOStime == nil) then -- 避免安卓刚进入就触发，导致报错
        return
    end

    local nowOStime = os.time()
    local interval = nowOStime - lastOStime -- 时间间隔
    if(interval > 0) then
        print("toForegroundCallback: interval =", interval)
        zc.reduceCDAll(interval)
    end

    -- 声音处理
    if zc.bgMusicOpen == true then
        zc.resumeMusic()
    end
    zc.resumeAllEffects()

end


local eventDispatcher = cc.Director:getInstance():getEventDispatcher()
local _customListener1 = cc.EventListenerCustom:create("event_come_to_background", toBackgroundCallback)
eventDispatcher:addEventListenerWithFixedPriority(_customListener1, -1)

local _customListener2 = cc.EventListenerCustom:create("event_come_to_foreground", toForegroundCallback)
eventDispatcher:addEventListenerWithFixedPriority(_customListener2, -1)

-- 加入Event队列
table.insert(zc.EventListenerQueue, _customListener1)
table.insert(zc.EventListenerQueue, _customListener2)

local tick = function ( self )
    self.endTime = self.endTime - 1
    if self.callback then
        self.callback(self.endTime)
    end
    if self.endTime <= 0 then
        zc.cleanCDSimple(self)
    end
end

-- 设置倒计时的通用方法 id:定时器唯一id(全局唯一) , secends倒计总时间秒数, callback:每秒回调方法
function zc.updateCDSimple(id, secends, callback)
    if(cdQueueSimple[id] ~= nil) then
        cdQueueSimple[id].endTime = secends
        cdQueueSimple[id].callback = callback or cdQueueSimple[id].callback
    else
        cdQueueSimple[id] = {id = id, endTime = secends, tick = tick, callback = callback}
        zc.mainTimer:add(cdQueueSimple[id])
    end
end
function zc.cleanCDSimple(id)
    if(cdQueueSimple[id] ~= nil) then
        zc.mainTimer:remove(cdQueueSimple[id])
        cdQueueSimple[id] = nil
    end
end


-- 设置倒计时的通用方法
function zc.updateCD(id, secends)
    if(cdQueue[id] ~= nil) then
        cdQueue[id]:update(secends)
    else
        if(cdList[id] ~= nil) then
            cdQueue[id] = cdList[id].new(secends)
        end
    end
end
function zc.cleanCD(id)
    if(cdQueue[id] ~= nil) then
        cdQueue[id]:clean()
        cdQueue[id] = nil
    end
end

function zc.cleanCDAll()
    for i,v in pairs(cdQueue) do
        if(v.on == true) then
            v:clean()
            cdQueue[i] = nil
        end
    end
    for i,v in pairs(cdQueueSimple) do
        if(cdQueueSimple[id] ~= nil) then
            zc.mainTimer:remove(cdQueueSimple[id])
            cdQueueSimple[id] = nil
        end
    end
end

-- 检查CD是否运行中
function zc.isCDalive(id)
    if(cdQueue[id] ~= nil and cdQueue[id].on == true) then
        return true
    else
        return false
    end
end



local kTimerIncrease = 1 -- 定时器为增加的
local kTimerReduce   = 2 -- 定时器为减少的
function zc.reduceCDAll(seconds)
    for i,v in pairs(cdQueue) do
        if(v.on == true) then
            if(v.id == "CommonCD" or v.id == "TimeCD") then
                v:update(seconds, kTimerIncrease)
            else
                v:update(seconds, kTimerReduce)
            end
        end
    end
    for i,v in pairs(cdQueueSimple) do
        v.endTime = v.endTime - seconds
    end
end

--[[
    公共CD，通用型的倒计时处理
]]
local CommonCD = class("CommonCD")
function CommonCD:ctor(secends)
    self.id = "CommonCD"
    self.on = false

    self:init(secends)
end
function CommonCD:init(secends)
    self.lasts = secends
    self.s = secends

    if(self.on == false) then
        zc.mainTimer:add(self)
        self.on = true
    end
end
function CommonCD:update(secends, timerType)
    if(self.on == false) then
        self:init(secends)
    else
        local newSeconds = secends
        if(timerType ~= nil and timerType == kTimerIncrease) then
            newSeconds = self.s + newSeconds
        end
        self.s = newSeconds
    end
end
function CommonCD:tick()
    --[[
        dis 两次倒计时的时间差值（秒）
    ]]
    self.s = self.s + 1
    local dis = self.s - self.lasts

    -- 重要，保存上次值
    self.lasts = self.s
end
function CommonCD:clean()
    self.s = 0
    self.lasts = 0
    self.on = false
    zc.mainTimer:remove(self)
    print(self.id .. " is clean")
end
-- *****保存到类集合
cdList["CommonCD"] = CommonCD


--[[
    心跳检查
]]
local HeartbeatCD = class("HeartbeatCD")
function HeartbeatCD:ctor(secends)
    self.id = "HeartbeatCD"
    self.on = false
    self.connecting = false
    self.thread = nil

    self:init(secends)
end
function HeartbeatCD:init(secends)
    self.s = secends

    -- if(self.s > 0) then
        zc.mainTimer:add(self)
        self.on = true
    -- end
end
function HeartbeatCD:update(secends, timerType)
    if(self.on == false) then
        self:init(secends)
    else
        local newSeconds = secends
        if(timerType ~= nil and timerType == kTimerReduce) then
            newSeconds = self.s - newSeconds
        end
        self.s = newSeconds
    end
end
function HeartbeatCD:tick()
    self.s = self.s - 1
    if(self.s <= 0 and self.connecting == false) then
        if(PlayerData ~= nil) then
            self.connecting = true
            self.thread = PlayerData:heartbeat(function()
                self.s = HEARTBEAT_INTERVAL
                self.connecting = false
                self.thread = nil
                print("==resume HeartbeatCD==")
            end)
        end
    end
end
function HeartbeatCD:clean()
    if(self.thread ~= nil) then
        -- TODO 取消网络请求
    end
    self.s = 0
    self.e = nil
    self.on = false
    self.thread = nil
    zc.mainTimer:remove(self)
    print(self.id .. " is clean")
end
-- *****保存到类集合
cdList["HeartbeatCD"] = HeartbeatCD



--  在线奖励CD
local onlineAwardCD = class("onlineAwardCD")
function onlineAwardCD:ctor(seconds)
    self.id = "onlineAwardCD"
    self.on = false
    self.s = nil
    self:init(seconds)
end
function onlineAwardCD:init(seconds)
    zc.mainTimer:add(self)
    self.on = true
    self.s = seconds
    self:tick()
end

function onlineAwardCD:update(seconds, timerType)
    if(self.on == false) then
        self:init(seconds)
    else
        local newSeconds = seconds
        if(timerType ~= nil and timerType == kTimerReduce) then
            print(self.s, newSeconds)
            newSeconds = self.s - newSeconds
        end
        self.s = newSeconds
    end
end

function onlineAwardCD:tick()
    if self.s <= 0 then
        self:clean()
    else
        self.s = self.s - 1
        if PlayerData then
            PlayerData.online_reward.left_time = self.s
        end
        
        zc.iconManager.updateCDLabel(self.s)
    end

end

function onlineAwardCD:clean()
    self.on = false
    self.s = nil
    zc.mainTimer:remove(self)
end
cdList["onlineAwardCD"] = onlineAwardCD




--  换桌CD
local changeDeskCD = class("changeDeskCD")
function changeDeskCD:ctor(seconds)
    self.id = "changeDeskCD"
    self.on = false
    self.s = nil
    self:init(seconds)
end
function changeDeskCD:init(seconds)
    zc.mainTimer:add(self)
    self.on = true
    self.s = seconds
    self:tick()
end

function changeDeskCD:update(seconds, timerType)
    if(self.on == false) then
        self:init(seconds)
    else
        local newSeconds = seconds
        if(timerType ~= nil and timerType == kTimerReduce) then
            print(self.s, newSeconds)
            newSeconds = self.s - newSeconds
        end
        self.s = newSeconds
    end
end

function changeDeskCD:tick()
    if self.s <= 0 then
        zc.confirm({
            btnNum         = 2,
            text           = zc.lang("cd_Info", "暂时没有玩家加入，需要加入其它牌桌吗？"),
            okCallback = function()
                
                local successCallback = function(data)
                    if(tonumber(data.status) == 1) then
                        GameManager:startGame()
                    else
                        zc.getErrorMsg(data.status)
                    end
                end
                zc.loadUrl("m=change_room", {
                    }, successCallback)
            end
        })

        self:clean()
    else
        self.s = self.s - 1

    end

end

function changeDeskCD:clean()
    self.on = false
    self.s = nil
    zc.mainTimer:remove(self)
end
cdList["changeDeskCD"] = changeDeskCD
