print("commonHelper")


local flashNodeCenter = {}
function zc.registFlashNode( params )

    local mType = tonumber(params.type) or 0
    local mId = tonumber(params.id) or 0
    local updateNow = params.updateNow -- 立即更新

    -- 注册的节点名（每个Type(或Id)下的名字必须唯一）
    local registName = params.name or (mType .. "_" .. mId .. "_" .. math.random(10000, 100000))


    if flashNodeCenter[mType] == nil then
        flashNodeCenter[mType] = {}
    end

    -- 引用
    local referenceTb = nil

    if mId == 0 then

        referenceTb = flashNodeCenter[mType]

    else
        if flashNodeCenter[mType][mId] == nil then
            flashNodeCenter[mType][mId] = {}
        end

        referenceTb = flashNodeCenter[mType][mId]
    end

    referenceTb[registName] = params

    if updateNow then
        if params.callback then -- 如果有回调调用回调的
            params.callback()
        end
    end

end

--截取中英混合的UTF8字符串
function zc.subName(name)
    local finalLen = 0
    local subTab = string.utf8cut( name )
    local strLen = string.utf8len(name)
    local str = ""
    for i=1,strLen do
        local addLen = string.len(subTab[i]) == 3 and 2 or 1
        finalLen = finalLen + addLen
        if finalLen > 8 then
            return str .. "..."
        end
        str = str .. subTab[i]
    end
    return str
end



function zc.updateFlashNode( mType, mId )

    local mType = tonumber(mType)
    local mId = tonumber(mId) or 0

    local referenceTb = nil


    if mId == 0 then
        referenceTb = flashNodeCenter[mType]
    else
        if flashNodeCenter[mType] then

            referenceTb = flashNodeCenter[mType][mId]

        end
    end


    if referenceTb ~= nil and type(referenceTb) == "table" then

        for k,v in pairs(referenceTb) do
            if not tolua.isnull(v.node) then

                if v.callback then -- 如果有回调调用回调的
                    v.callback()
                end

                -- 以下是自定义刷新类型
                if v.flashType then
                    if v.flashType == 1 then -- updateIcon的attrLabel
                        zc._updateIconAttrLabel( v.node, v.need, v.now )
                    end
                end

            else
                referenceTb[k] = nil


            end
        end

    end

end



----------------其他----------------------

--- 过万转换数字
-- @param value 转换值
-- @param ntype 转换类型,1:金币 2:钻石 3:伤害
-- @return 返回转换后的数值
-- @see zc.reverseWan

function zc.getWan( value, ntype )
    local ntype = ntype or  1

    value = math.floor(tonumber( value ))

    if type(value) ~= "number" then
        return ""
    end


    if 1 == ntype then -- 金币
        return ( value > 99999999 and math.floor( value / 10000) .. zc.lang("commonHelper_Million", "万") or value )
    elseif 2 == ntype then -- 钻石
        return ( value > 999999 and math.floor( value / 10000) .. zc.lang("commonHelper_Million", "万") or value )
    elseif 3 == ntype then -- 伤害
        return ( value > 9999 and math.floor( value / 10000) .. zc.lang("commonHelper_Million", "万") or value )
    elseif 4 == ntype then
       -- return ( value > 9999  and string.format("%.2f",( value / 10000)) .. "万" or value )
        if value > 9999 and value <= 99999999 then
            local integetNum,decimalNum = math.modf(value / 10000)
            if decimalNum == 0 then
                return value / 10000 .. zc.lang("commonHelper_Million", "万")
            else
                local num1 = math.floor(value / 100)
                local integetNum,decimalNum = math.modf(value / 1000)
                if decimalNum == 0 then
                    return string.format("%.1f",(( value / 10000))) .. zc.lang("commonHelper_Million", "万")
                else
                    return string.format("%.2f",(( num1 / 100))) .. zc.lang("commonHelper_Million", "万")
                end
            end
        elseif value > 99999999 then
            local integetNum,decimalNum = math.modf(value / 100000000)
            if decimalNum == 0 then
                return value / 100000000 .. zc.lang("commonHelper_Billion", "亿")
            else
                local num1 = math.floor(value / 100)
                local integetNum,decimalNum = math.modf(value / 10000000)
                if decimalNum == 0 then
                    return string.format("%.1f",( value / 100000000)) .. zc.lang("commonHelper_Billion", "亿")
                else
                    return string.format("%.2f",( num1 / 1000000)) .. zc.lang("commonHelper_Billion", "亿")
                end
            end
        end
        return value
    end

    
end

--- 数字转换过万
-- @param value 待转换值
-- @return 返回转换后的数值
-- @see zc.getWan

function zc.reverseWan( value )
    if type(value) == "string" then

        if value == "" then
            return ""
        end

        if tonumber(value) then
            return tonumber(value)
        end

        local wan = 0
        local z1, z2 = string.gsub(value, "[%d]+", function ( a )
            wan = a
        end)

        return wan * 10000

        
    end
    return value
end


--缩短名字
function zc.cutRoleId( roleId )
    return tostring(roleId%10000000000)
end



function zc.sysMsgTemplatesToRich( templateId, params )
 
    params = params or {}
    local data = PlayerData:getSysMsgTemplates()[templateId]

    if data == nil then
        zc.tipInfo(zc.lang("commonHelper_NotFindID", "找不到后端文本模版ID:")..templateId)
        return params
    end
    if #data.orderList ~= #params then
        zc.tipInfo(zc.lang("commonHelper_ParameterError", "参数错误模版ID:")..templateId)
        return params
    end
    local template = table.copy(data.template)
    if template == nil then return end
    local count = 1
    for i = 1, #template do
        if template[i] == 0 then
            template[i] = params[data.orderList[count]]
            count = count + 1
        end
    end

    for i,v in ipairs(template) do

        

    end
    count = 1
    while count <= #template  do
        if template[count] == nil or template[count] == 0 then
            print("count",count)
            table.remove(template, count)
        else
            count = count + 1
        end
    end
    return template, data.title, data.name
end


--多颜色格式
function zc.getFormateByData(data)
    local richData = {}

    for i=1, #data.text do
        local line = data.text[i]
        local showtext = ""

        local red = 255
        local green = 255
        local blue = 255

        if line.c then

            red = line.c:match("(.-)-")

            if line.c:match("-(.*)") then
                green = line.c:match("-(.*)"):match("(.*)-") or 200
            end

            if line.c:match("-(.*)") then
                blue = line.c:match("-(.*)"):match("-(.*)") or 200
            end
        end

        local color = cc.c3b(tonumber(red), tonumber(green), tonumber(blue))


        local myTable = {}
        if tonumber(line.t) then
            if tonumber(line.t) == 4 then
                local pathSrc = zc.getAttrResPath(tonumber(line.res_type))
                if pathSrc ~= nil and pathSrc ~= "" then
                    myTable = {
                        {tag = 3, plist = 1, scale = 0.8, img = pathSrc, vcenter = true}
                    }
                    if tonumber(line.num) and tonumber(line.num) > 0 then
                        table.insert(myTable, {tag = 1, text = tostring(line.num), color = cc.c3b(tonumber(red), tonumber(green), tonumber(blue)) })
                    end
                else
                    local myData = {type = tonumber(line.res_type), id = tonumber(line.id), num = tonumber(line.num)}
                    local info = zc._getIconInfo( {data = myData} )
                    color = zc.nameColor(info.star)

                    if (line.res_type == zc.dataTypeHero) then
                        showtext = info.name
                        color = zc.nameColor(info.quality)
                    else
                        if line.num then
                            if info.name == nil or info.name == "" then
                                showtext = ""
                            else
                                showtext = info.name .. "X" .. info.num
                            end
                        else
                            showtext = info.name
                        end
                    end

               end
            else
                showtext = line.s
            end
        else
            showtext = line.s
        end

        local tempTable = nil

        if table.nums(myTable) ~= 0 then
            tempTable = zc.getTextDataByStr(myTable,showtext, color)
        else
            tempTable = zc.getTextDataByStr(nil, showtext, color)
        end

        for i = 1, #tempTable do
            table.insert(richData, tempTable[i])
        end
    end

    return richData
end

function zc.getTextDataByStr(beforeTabel,text, mycolor)
    if beforeTabel ~= nil then
        return beforeTabel
    end
    local keyTable = {}
    local valueTable = {}
    local findText = "%#..%#"
    local isEnd = string.find(text, findText)
    while isEnd ~= nil do
        local beginIndex, endIndex = string.find(text, findText)
        local subText = string.sub(text, beginIndex, endIndex)
        --存文字非空文字
        local tempText = string.sub(text, 1, beginIndex - 1)
        if tempText ~= "" then
            table.insert(keyTable, 1)
            table.insert(valueTable, tempText)
        end

        --存表情
        table.insert(keyTable, 3)
        table.insert(valueTable, string.sub(text, beginIndex, endIndex))

        --余下字段
        text = string.sub(text, endIndex + 1, string.len(text))
        isEnd = string.find(text, findText)
    end
    if text ~= "" then
        table.insert(keyTable, 1)
        table.insert(valueTable, text)
    end

    local tempTable = {}
    local keyNum = table.nums(keyTable)
    for i = 1, keyNum do
        local myTable = {}
        myTable["tag"] = tonumber(keyTable[i])
        if myTable["tag"] == 3 then
            for k,v in pairs(zc.expressionTabel) do
                if tostring(valueTable[i]) == tostring(v) then
                    myTable["img"] = zc.getPicPathByName(tostring(v))
                    myTable.scale = 0.8
                    break
                else
                    local path = "Main/chat/expression/expression_default.png"
                    myTable.img = path
                    myTable.scale = 0.8
                end
            end
        else
            if string.find(valueTable[i], [[\n]]) then
                myTable["tag"] = 5
            else
                myTable.text = tostring(valueTable[i])
                myTable.color = mycolor
            end
        end

        tempTable[i] = myTable
    end

    if keyNum == 0 then
        -- print("text = "..text)
        if string.find(text, [[\n]]) then
            -- print("text == \n")
            tempTable = {{tag = 5}}
        else
            local myTable = {
                {tag = 1, text = text , color = mycolor },
            }
            tempTable = myTable
        end
    end

    --拆分\n
    local index = 1
    local tmp, color
    while tempTable[index] do
        if tempTable[index].tag == 1 and tempTable[index].text then
            tmp = string.split(tempTable[index].text, "\n")
            -- dump(tmp)
            color = tempTable[index].color
            tempTable[index].text = tmp[1]
            index = index + 1
            for i = 2, #tmp do
                table.insert(tempTable, index, {tag = 5})
                index = index + 1
                table.insert(tempTable, index, {tag = 1, text = tmp[i], color = color})
                index = index + 1
            end
        else
            index = index + 1
        end
    end

    return tempTable
end



function zc.zeroPad(timeseconds, nums)
    local len = string.len(tostring(timeseconds))
    local str = tostring(timeseconds)
    for i = len, nums - 1 do
        str = "0" .. str
    end
    return str
end


--- 秒数转成 x小时x分钟x秒
-- @param timeseconds 时间戳
-- @param sp 是否少于一分钟
-- @return 返回x小时x分钟x秒
-- @see zc.timeToClock
-- @usage
function zc.timeToString(timeseconds, sp)

    local timeseconds = tonumber(timeseconds)
    local sp = sp or false

    local str_s = zc.lang("commonHelper_Second", "秒")
    local str_m = zc.lang("commonHelper_Min", "分")
    local str_h = zc.lang("commonHelper_Hour", "时")
    local str_d = zc.lang("commonHelper_Day", "天")

    -- 少于一分钟
    if (sp) then
        if (timeseconds < 60) then
            return tostring(timeseconds) .. str_s
        end
    end

    if (timeseconds < 60) then
        return zc.zeroPad(timeseconds, 2) .. str_s
    end

    local secs = timeseconds % 60 -- 秒
    local mins = (timeseconds - secs) / 60 -- 分

    if (sp) then
        if (mins < 60) then
            return tostring(mins) .. str_m
        end
    end

    if (mins < 60) then -- 少于一小时
        return tostring(mins) ..str_m .. zc.zeroPad(secs, 2) .. str_s
    end

    -- 大于一小时
    local mins_new = mins % 60 -- 分
    local hous = (mins - mins_new) / 60 -- 小时

    -- 只返回最接近的最大时间
    if (sp) then
        if (hous > 24) then
            return tostring(math.floor(hous / 24)) .. str_d
        else
            return tostring(hous) .. str_h
        end
    end

    if (hous <= 24) then
        return tostring(hous) .. str_h .. tostring(mins_new) .. str_m .. tostring(secs) .. str_s
    else
        return tostring(math.floor(hous / 24)) .. str_d .. ' ' .. tostring(hous % 24) .. str_h .. tostring(mins_new) .. str_m .. tostring(secs) .. str_s
    end
end


--- 秒数转成 00:00:00
-- @param timeseconds 描述
-- @param day 是否显示天
-- @return 00:00:00
-- @see zc.timeToString
-- @usage zc.timeToClock(time, true)
function zc.timeToClock(timeseconds, day)
    local day = day or false

    local timeseconds = tonumber(timeseconds)
    local str_d = zc.lang("commonHelper_Day", "天")

    if not day then
        -- 小于0
        if(timeseconds < 0) then
            return '00:00:00'
        end
        -- 少于一分钟
        if (timeseconds < 60) then
            return '00:00:' .. zc.zeroPad(timeseconds, 2)
        end

        local secs = timeseconds % 60 -- 秒
        local mins = (timeseconds - secs) / 60 -- 分
        if (mins < 60) then -- 少于一小时
            return '00:' .. zc.zeroPad(mins, 2) .. ':' .. zc.zeroPad(secs, 2)
            -- return mins + '分' + secs + '秒';
        end

        -- 大于一小时
        local mins_new = mins % 60 -- 分

        local hous = (mins - mins_new) / 60 -- 小时
        -- return hous + '小时' + mins_new + '分' + secs + '秒';

        -- if (hous <= 24) then
            return zc.zeroPad(hous, 2) .. ':' .. zc.zeroPad(mins_new, 2) .. ':' .. zc.zeroPad(secs, 2)
        -- else
        --     return math.floor(hous / 24) .. str_d
        --     -- return math.floor(hous / 24) .. str_d .. ' ' .. zc.zeroPad(hous % 24, 2) .. ':' .. zc.zeroPad(mins_new, 2) .. ':' .. zc.zeroPad(secs, 2)
        -- end
    else
        -- 小于0
        if(timeseconds < 0) then
            return '00:00:00'
        end
        -- 少于一分钟
        if (timeseconds < 60) then
            return '00:00:' .. zc.zeroPad(timeseconds, 2)
        end

        local secs = timeseconds % 60 -- 秒
        local mins = (timeseconds - secs) / 60 -- 分
        if (mins < 60) then -- 少于一小时
            return '00:' .. zc.zeroPad(mins, 2) .. ':' .. zc.zeroPad(secs, 2)
        end

        -- 大于一小时
        local mins_new = mins % 60 -- 分

        local hous = (mins - mins_new) / 60 -- 小时

        if hous<24 then
            return zc.zeroPad(hous, 2) .. ':' .. zc.zeroPad(mins_new, 2) .. ':' .. zc.zeroPad(secs, 2)
        end
        local days = math.floor(hous/24)

        local daysLabel = zc.lang("commonHelper_Day", "天")
        if days > 1 then
            daysLabel = zc.lang("commonHelper_Day", "天")
        end
        return '' .. days .. daysLabel .. " "..zc.zeroPad(math.fmod(hous,24), 2) .. ':' .. zc.zeroPad(mins_new, 2) .. ':' .. zc.zeroPad(secs, 2)
    end
end

--- 时间戳转成日期
-- @param time 时间戳
-- @return 返回日期

function zc.timeToDate(time, mType)
    local mType = tonumber(mType) or 1 -- 默认是1

    local timeStr = ""

    if mType == 1 then -- 年月日时分秒
        -- timeStr = os.date("%Y/%m/%d %X", time)
        timeStr = zc.getPrintDate( time, "!%Y/%m/%d %X" )

    elseif mType == 2 then -- 年月日
        -- timeStr = os.date("%Y/%m/%d", time)
        timeStr = zc.getPrintDate( time, "!%Y/%m/%d" )

    end

   return timeStr
end


--[[
    通过服务器时间戳获得当前要显示的时间戳，用于游戏内显示
]]
function zc.getPrintDate( serverTimestamp, regex )

    local regex = regex or "!*t"
    
    local serverDiffSec = 60 * 60 * 8
    local dateObj = os.date(regex, serverTimestamp + serverDiffSec)

    return dateObj     

end


--[[
    针对UTF8编码计算字数
    str = "ad英雄@!! 4"
    return 9
]]

function zc.utf8strlen(str)
    local len = #str
    local left = len
    local cnt = 0;
    local arr={0,0xc0,0xe0,0xf0,0xf8,0xfc}
    while left ~= 0 do
        local tmp=string.byte(str,-left)
        local i=#arr
        while arr[i] do
            if tmp>=arr[i] then
                left=left-i
                break
            end
            i=i-1
        end
        cnt=cnt+1
    end
    return cnt
end

--[[
    截取UTF8编码的前n个字符，若长度不够则返回全部长度
]]

function zc.getUtf8FrontN( str, n )
    if str == nil or n == nil then
        print("getUtf8FrontN fatal error ")
        return
    end
    if zc.utf8strlen(str) <= n then
        return str
    end

    local length = n
    local index = 1
    local res = ""

    while length ~= 0 do
        local temp = string.byte(str,index)

        if temp < 0xc0 then
            -- 1位
            res = res .. string.sub(str, index, index)
            index = index + 1
        elseif temp >= 0xc0 and temp <0xe0 then
            -- 2位
            res = res .. string.sub(str, index, index + 1)
            index = index + 2
        elseif temp >= 0xe0 and temp <0xf0 then
            -- 3位
            res = res .. string.sub(str, index, index + 2)
            index = index + 3
        elseif temp >= 0xf0 and temp <0xf8 then
            -- 4位
            res = res .. string.sub(str, index, index + 3)
            index = index + 4
        elseif temp >= 0xf8 and temp <0xfc then
            -- 5位
            res = res .. string.sub(str, index, index + 4)
            index = index + 5
        else
            -- 6位
            res = res .. string.sub(str, index, index + 5)
            index = index + 6
        end
        length = length - 1
    end

    return res

end

-- 获取IP地址
function zc.getIPAddress()

    local ipAddress = "0.0.0.0"

    if IP_ADDRESS == "0.0.0.0" then
        zc._getIPAddress()
    else
        ipAddress = IP_ADDRESS
    end

    return ipAddress
end


-- 在PlayerData初始化、socket连接成功后会刷新
local ipRetryTimes = 0
function zc._getIPAddress()
    zc.http("http://update.mhdl.zhanchenggame.com/index.php?c=errorlog&m=get_ip", {

    }, 
    function ( data )
        if(data and data.ip) then
            IP_ADDRESS = data.ip
            ipRetryTimes = 0

            -- 同步
            if PlayerData and PlayerData.base then
                zc.loadUrl("m=client_ip", 
                    {ip_str = IP_ADDRESS}, 
                    function() end,
                    nil,
                    true)
            end

        end
        
    end, 
    function (  )
        if ipRetryTimes <= 3 then
            ipRetryTimes = ipRetryTimes + 1
            zc._getIPAddress()
        else
            ipRetryTimes = 0
        end
        
    end,
    true)
end

-- 同步地理位置信息
function zc._syncLocationInfo()
    -- 同步
    if PlayerData and PlayerData.base then

        zc.loadUrl("m=client_location", 
            {location = json.encode(zc.getLocationInfo())}, 
            function() end,
            nil,
            true)

    end
end

-- 拼接地理位置字符
function zc._combineLocationText( locationInfo )
    local locationText = zc.lang("commonHelper_UnknownLocation", "未知地点")

    if locationInfo.province ~= "unknown" and locationInfo.city ~= "unknown" then
        locationText = locationInfo.province .. " " .. locationInfo.city
    end

    return locationText
end

-- 获取自己的地理位置
function zc.getOwnLocationText()
    local locationInfo = zc.getLocationInfo()
    return zc._combineLocationText( locationInfo )
end


-- 通过地理位置信息数组获得
function zc.getLocationText( locationInfo )
    return zc._combineLocationText( locationInfo )
end

-- 获取地理位置信息
function zc.getLocationInfo()
    local locationInfo  = {
        longitude = "0", -- 经度
        latitude = "0", -- 纬度
        country = "unknown", -- 国家
        province = "unknown", -- 省
        city = "unknown", -- 市
        district = "unknown", -- 区
        address = "unknown", -- 位置描述
    }

    if device.platform ~= "android" then
        if LOCATE_INFO and LOCATE_INFO.State and LOCATE_INFO.SubLocality then
            locationInfo = {
                longitude = LOCATE_INFO.longitude, -- 经度
                latitude = LOCATE_INFO.latitude, -- 纬度
                country = LOCATE_INFO.Country, -- 国家
                province = LOCATE_INFO.State, -- 省
                city = LOCATE_INFO.City, -- 市
                district = LOCATE_INFO.SubLocality, -- 区
                address = LOCATE_INFO.formattedAddress, -- 位置描述
            }
        else
            zc.sdkGetLocation()
        end
    else

        if LOCATE_INFO and LOCATE_INFO.city and LOCATE_INFO.district then
            locationInfo = {
                longitude = LOCATE_INFO.longitude, -- 经度
                latitude = LOCATE_INFO.latitude, -- 纬度
                country = LOCATE_INFO.country, -- 国家
                province = LOCATE_INFO.province, -- 省
                city = LOCATE_INFO.city, -- 市
                district = LOCATE_INFO.district, -- 区
                address = LOCATE_INFO.addressStr, -- 位置描述
            }
        else
            zc.sdkGetLocation()
        end

    end

    return locationInfo
end

--long1:第1个经度 lat1:第1个纬度 long2:第2个经度 lat2:第2个纬度  返回两点之间距离
function zc._getLocationDistance(long1, lat1, long2, lat2)
    local a1 = math.sin(math.rad(90-lat1)) * math.cos(math.rad(long1)) - math.sin(math.rad(90-lat2)) * math.cos(math.rad(long2))
    local a2 = math.sin(math.rad(90-lat1)) * math.sin(math.rad(long1)) - math.sin(math.rad(90-lat2)) * math.sin(math.rad(long2))
    local a3 = math.cos(math.rad(90-lat1)) - math.cos(math.rad(90-lat2))
    local b1 = a1*a1 + a2*a2 + a3*a3
    local d = 6371004 * math.acos(1-b1/2)
    return d
end

-- 检测是否是合理的距离范围
function zc.checkValidDistance( long1, lat1, long2, lat2 )

    local isValid = false

    -- 合理距离
    local validDistance = 200

    -- 计算距离
    local distance = zc._getLocationDistance(tonumber(long1), tonumber(lat1), tonumber(long2), tonumber(lat2))

    if distance < validDistance then
        isValid = true
    end

    return isValid, validDistance

end

-- 是否开启GPS
function zc.isGPSEnabled()

    local GPSEnabled = false

    local _locationInfo = zc.getLocationInfo()

    if _locationInfo.longitude ~= "0" and _locationInfo.latitude ~= "0" then
        GPSEnabled = true
    end

    return GPSEnabled
end

-- Director::getInstance()->getTextureCache()->removeTextureForKey(_filename);

-- 截屏
function zc.captureScreen( callback, fileName )
    local subDir = "tmp/screen/"

    fileName = fileName or ("screen" .. math.random(100000, 999999) .. ".jpg")

    --保存到可写目录下的download目录
    -- local fullDir = "download" .. VERSION_CACHE_ID .. "/" .. subDir

    local saveURI = subDir .. fileName

    -- 删除目录
    cc.FileUtils:getInstance():removeDirectory(device.writablePath .. subDir)

    os.prepareDir(subDir)

    local fullFileName = device.writablePath .. saveURI

    -- zc.showLoading({name = "captureScreen"})
    cc.utils:captureScreen(function ( succeed, outputFile )

        callback(succeed, outputFile)

        -- zc.hideLoading({name = "captureScreen"})

    end, fullFileName)

end

-- -- 系统截屏
-- function zc.captureScreen(callback, fileName)
--     local subDir = "tmp/screen/"

--     fileName = fileName or ("screen" .. math.random(100000, 999999) .. ".jpg")

--     --保存到可写目录下的download目录
--     local fullDir = "download" .. VERSION_CACHE_ID .. "/" .. subDir

--     local saveURI = fullDir .. fileName

--     -- 删除目录
--     cc.FileUtils:getInstance():removeDirectory(device.writablePath .. fullDir)

--     os.prepareDir(fullDir)

--     local fullFileName = device.writablePath .. saveURI

--     zc.showLoading({name = "captureScreen"})

--     local rend = cc.RenderTexture:create(zc.width, zc.height)

--     rend:begin()
--     cc.Director:getInstance():getRunningScene():visit()
--     rend:endToLua()

--     rend:saveToFile(saveURI, cc.IMAGE_FORMAT_JPEG)

    

--     scheduler.performWithDelayGlobal(function()
--         if io.exists(fullFileName) then
--             callback(true, fullFileName)
--         else
--             callback(false, fullFileName)
--         end
        
--         zc.hideLoading({name = "captureScreen"})
--     end, 0.01)

-- end


--[[
    SDK业务层逻辑
]]




-- 检测防沉迷
function zc.checkAntiAddiction( data )
    if data.m and data.m == "addicted_msg" and data.hour then

        local errorMsg = ""
        local callback = nil

        if tonumber(data.hour) < 3 then
            errorMsg = zc.lang("commonHelper_ErrorMsg1", "防沉迷提示：您累计在线时间已满") .. data.hour .. zc.lang("commonHelper_ErrorMsg2", "个小时,祝您游戏愉快！")
            callback = function (  ) end
        else
            errorMsg = zc.lang("commonHelper_ErrorMsg1", "防沉迷提示：您累计在线时间已满") .. data.hour .. zc.lang("commonHelper_ErrorMsg3", "个小时,将对您的账号进行强制下线操作。")
            callback = function()
                zc.logout()
            end
        end

        zc.confirm({
            btnNum         = 1,
            text           = errorMsg,
            okCallback     = callback,
            okText         = zc.lang("dzBuyChip_Sure","确定"),
            godMode = true
        })
    end
end

--------------------------------德州通用接口----------------------------
--选牌算法
function zc.selectPoker( _pokertbale )

    local returnType ,weight = 0, 0
    local returnTab = {}
    --7选5 方法一
    for i=1,#_pokertbale do
        for j=i+1,#_pokertbale do
            for k=j+1,#_pokertbale do
                for l=k+1,#_pokertbale do
                    for m=l+1,#_pokertbale do
                        local finalPoker = {}
                        finalPoker[#finalPoker+1] =  _pokertbale[i]
                        finalPoker[#finalPoker+1] =  _pokertbale[j]
                        finalPoker[#finalPoker+1] =  _pokertbale[k]
                        finalPoker[#finalPoker+1] =  _pokertbale[l]
                        finalPoker[#finalPoker+1] =  _pokertbale[m]
                        _type ,_weight = zc.getFivePokerType(finalPoker)
                        returnType = returnType > _type and returnType or _type 
                        if _weight > weight then
                            weight = _weight
                            returnTab = finalPoker
                        end
                    end
                end
            end
        end
    end
    return returnType,returnTab
    -- --7选5 方法二
    -- local finalPoker = {}
    -- local getCombination
    -- getCombination = function (_pokertbale, selectNum, beginindex, finalPoker, curindex)
    --  if selectNum == 0 then
    --      self:getFivePokerType(finalPoker)
    --      return
    --  end
    --  for i=beginindex,#_pokertbale do
    --      finalPoker[curindex] = _pokertbale[i]
    --      getCombination(_pokertbale,selectNum-1,i+1,finalPoker,curindex+1)
    --  end
    -- end
    -- --执行 
    -- getCombination(_pokertbale, 5, 1, finalPoker,1)

end


local pokerWeight = {0,10000000000,20000000000,30000000000,40000000000,50000000000,60000000000,70000000000,80000000000,90000000000,100000000000}
--得到牌的牌型 权重
function zc.getFivePokerType( finalPoker )
    local returnType = 0
    local _type = 0
    --其他牌型
    for i=1,#finalPoker do
        for j=i+1,#finalPoker do
            if finalPoker[i] % 100 == finalPoker[j] % 100 then
                _type = _type + 1
            end
        end
    end

    if _type  == 0 then
        _type = 1
    elseif _type  == 1 then
        _type = 2
    elseif _type  == 2 then
        _type = 3
    elseif _type  == 3 then
        _type = 4
    elseif _type  == 4 then
        _type = 7
    elseif _type  == 6 then
        _type = 8
    end

    returnType = returnType >= _type and returnType or _type

    local returnWeight = 0
    finalPokerTab,returnWeight = zc.sortPokerWeight(finalPoker)

    local finalIdTab = {}
    local finalTypeTab = {}

    for i=1,#finalPokerTab do
        finalIdTab[i] = math.floor(finalPokerTab[i] % 100)
        finalTypeTab[i] = math.floor(finalPokerTab[i] / 100)
    end


    if #finalPoker < 5 then
        return returnType,returnWeight
    elseif #finalPoker == 5 then
        local istonghua = true
        --计算同花
        for i=1,#finalTypeTab -1 do
            if finalTypeTab[i] ~= finalTypeTab[i+1] then
                istonghua = false
            end
        end
        local isshunzi = true
        local isMaxshunzi = false
        --计算顺子
        for i=1,#finalIdTab -1 do
            if  finalIdTab[i] - finalIdTab[i+1] ~= 1 then
                isshunzi = false
            end
        end
        if finalIdTab[1] == 14 and finalIdTab[2] == 5 and finalIdTab[3] == 4 and finalIdTab[4] == 3 and finalIdTab[5] == 2 then
            isshunzi = true
        end
        --皇家同花顺
        if isshunzi and istonghua and finalIdTab[1] == 14 and finalIdTab[2] == 13 then
            isMaxshunzi = true
        end

        if isshunzi then _type = 5 end
        if istonghua then _type = 6 end
        if isshunzi and istonghua then _type = 9 end
        if isMaxshunzi then _type = 10 end

        returnType = returnType >= _type and returnType or _type

        if returnType == 5 or returnType == 9 or returnType == 10 then
             table.sort( finalIdTab, function(a,b)
                return math.floor(a % 100) >  math.floor(b % 100)
            end )

             if finalIdTab[1] == 14 and finalIdTab[2] == 5 then
                table.remove(finalIdTab,1) 
                table.insert(finalIdTab,14)
             end
        end
        local baseWeight = finalIdTab[1]*100000000 + finalIdTab[2]*1000000 + finalIdTab[3] * 10000 + finalIdTab[4]*100 + finalIdTab[5]
        returnWeight = baseWeight + pokerWeight[returnType]
        return returnType,returnWeight
    end

end

--高亮牌算法 <=5的最时候，最大牌型和对应的牌
function zc.getLightPoker(finalPoker)
    local equalPoker = {}
    local _type = 0
    --其他牌型
    for i=1,#finalPoker do
        for j=i+1,#finalPoker do
            if finalPoker[i] % 100 == finalPoker[j] % 100 then
                equalPoker[#equalPoker+1] = finalPoker[i]
                equalPoker[#equalPoker+1] = finalPoker[j]
                _type = _type + 1
            end
        end
    end

    if _type  == 0 then
        _type = 1
    elseif _type  == 1 then
        _type = 2
    elseif _type  == 2 then
        _type = 3
    elseif _type  == 3 then
        _type = 4
    elseif _type  == 4 then
        _type = 7
    elseif _type  == 6 then
        _type = 8
    end

    local finalEqualPoker = {}
    for i,v in ipairs(equalPoker) do
        if not table.inArray(v, finalEqualPoker) then
            finalEqualPoker[#finalEqualPoker+1] = v
        end
    end
    return finalEqualPoker,_type
end

--排序
function zc.sortPokerWeight(finalPoker)

    local sortPoker

    table.sort( finalPoker, function(a,b)
        return math.floor(a % 100) >  math.floor(b % 100)
    end )

    local equalPoker,pokerType = zc.getLightPoker(finalPoker)

    table.sort( equalPoker, function(a,b)
        return math.floor(a % 100) >  math.floor(b % 100)
    end )

    if pokerType == 1 then
        sortPoker = finalPoker
    elseif pokerType == 2 or pokerType == 3 or pokerType == 4  then
        sortPoker = {}
        for i,v in ipairs(equalPoker) do
            table.insert(sortPoker, v)
        end
        for i,v in ipairs(finalPoker) do
            if not table.inArray(v, sortPoker) then
                table.insert(sortPoker, v)
            end
        end
    elseif pokerType == 7 then
        sortPoker = {}
        --葫芦 三张牌放前面 2张后边
        if equalPoker[3] % 100 ~= equalPoker[2] % 100 then
            table.insert(sortPoker, equalPoker[3])
            table.insert(sortPoker, equalPoker[4])
            table.insert(sortPoker, equalPoker[5])
            table.insert(sortPoker, equalPoker[1])
            table.insert(sortPoker, equalPoker[2])
        else
            sortPoker = equalPoker
        end
    elseif pokerType == 8 then
        sortPoker = equalPoker
        for i,v in ipairs(finalPoker) do
            if not table.inArray(v, sortPoker) then
                table.insert(sortPoker, v)
            end
        end
    end

    local pokerId = {}
    for i=1,#sortPoker do
        pokerId[#pokerId+1] = string.sub(tostring(sortPoker[i]),2)
    end


    local weight1 = pokerId[1] and pokerId[1]*100000000 or 0
    local weight2 = pokerId[2] and pokerId[2]*1000000 or 0
    local weight3 = pokerId[3] and pokerId[3]*10000 or 0
    local weight4 = pokerId[4] and pokerId[4]*100 or 0
    local weight5 = pokerId[5] and pokerId[5] or 0

    local baseWeight = weight1 + weight2 + weight3 + weight4 + weight5
    local returnWeight = baseWeight + pokerWeight[pokerType]
   return sortPoker,returnWeight
end

function zc.getPokerNameByType(type)
    if type == 1 then
        return zc.lang("commonHelper_PokerTypeName1", "高牌")
    elseif type == 2 then
        return zc.lang("commonHelper_PokerTypeName2", "对子")
    elseif type == 3 then
        return zc.lang("commonHelper_PokerTypeName3", "两对")
    elseif type == 4 then
        return zc.lang("commonHelper_PokerTypeName4", "三条")
    elseif type == 5 then
        return zc.lang("commonHelper_PokerTypeName5", "顺子")
    elseif type == 6 then
        return zc.lang("commonHelper_PokerTypeName6", "同花")
    elseif type == 7 then
        return zc.lang("commonHelper_PokerTypeName7", "葫芦")
    elseif type == 8 then
        return zc.lang("commonHelper_PokerTypeName8", "四条")
    elseif type == 9 then
        return zc.lang("commonHelper_PokerTypeName9", "同花顺")
    elseif type == 10 then
        return zc.lang("commonHelper_PokerTypeName10", "皇家同花顺")  
    end
    return ""
end


function zc.getRoomByType(type)
    if type == 1 then
        return zc.lang("commonHelper_RoomTypeName1", "平民场")
    elseif type == 2 then
        return zc.lang("commonHelper_RoomTypeName2", "小资场")
    elseif type == 3 then
        return zc.lang("commonHelper_RoomTypeName3", "老板场")
    elseif type == 4 then
        return zc.lang("commonHelper_RoomTypeName4", "土豪场")
    elseif type == 5 then
        return zc.lang("commonHelper_RoomTypeName5", "贵族场")
    elseif type == 6 then
        return zc.lang("commonHelper_RoomTypeName6", "皇家场")
    end
    return ""
end

function zc.getPlayType(type)
    if type == 1 then
        return zc.lang("commonHelper_PlayTypeName1", "6人桌")
    end
    return ""
end

-- 判断是否自己
function zc.isSelf( _roleid )
    if _roleid == PlayerData.base.roleid then
        return true
    end
    return false
end

--得到所有牌
function zc.getAllPokers(  )
    local allPokers = {}
    for i=1,4 do
        for j=2,14 do
            table.insert(allPokers,tonumber(i  .. string.format("%02d",j)))
        end
    end
    return allPokers
end

--table删除一个tab
function zc.tableDel(tb, del)
    local  i, max =  1, #tb
    while i <= max do
        for k, v in ipairs(del) do
            if tb[i] == v then
                table.remove(tb, i)
                i = i - 1
                max = max - 1
                break
            end
        end
        i = i + 1
    end
end

