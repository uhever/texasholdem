local POP = {}

-- view
local popName = "src/app/pop/smallLoadingPop"

local smallLoadingWidget = nil

-- loading队列
local loadingQueue = {}

local loadingBase = "loadingBase"

-- 是否在显示中
local isShow = false

-- 延时
local delayTime = 0.5

local loadingTimes = 0

local netWorkBadTimes = 0

function POP.resetState(  )
    delayTime = 0.5
    loadingTimes = 0
    netWorkBadTimes = 0
end

function POP.create(params)

    params = params or {}

    -- zc.addToActivePops(popName)

    -- 当前调用showLoading的名称
    local loadingName = params.name or loadingBase

    local isExistNowLoading = false

    for i,v in ipairs(loadingQueue) do
        if v == loadingName then
            isExistNowLoading = true
            break
        end
    end

    if not isExistNowLoading then
        table.insert(loadingQueue, loadingName)
    end

    -- dump(loadingQueue, "showLoading")
    
    -- 已经存在不创建
    if isShow == true then
        return
    end


    --查创建widget
    smallLoadingWidget = zc.createMyPop(popName, {
        mask = true,
        opacity = 0,
        zOrder = zc.Z_ORDER_SMALL_LOADING,
        })


    POP.init()

    -- 设置为已创建
    isShow = true

end


function POP.init( )
    loadingTimes = loadingTimes + 1

    if loadingTimes > 5 then

        if netWorkBadTimes > 3 then
            delayTime = 0

        else
            delayTime = 0.5
        end

        netWorkBadTimes = 0
        loadingTimes = 0

    end

    POP.createUI()
end


function POP.createUI(  )

    local loadingBg = zc.createImageView({
        src = "res/Image/loading/loading_bg.png", 
        pos = cc.p(zc.cx, zc.cy),
        parent = smallLoadingWidget,
        }) 

    local circleImg = zc.createImageView({
        src = "res/Image/loading/loading_progress_gray.png", 
        pos = cc.p(loadingBg:getContentSize().width / 2, loadingBg:getContentSize().height / 2 + 30),
        parent = loadingBg,
        })

    -- 倒计时
    local heartImg = zc.createProgressTimer({
        src = "res/Image/loading/loading_progress_yellow.png",
        midPoint = cc.p(0.5, 0),
        barChangeRate = cc.p(0, 1),
        type = cc.PROGRESS_TIMER_TYPE_BAR,
        pos = cc.p(circleImg:getContentSize().width/2, circleImg:getContentSize().height/2),
        percent = 0,
        parent = circleImg
        })

    local seq = {
        cc.ProgressTo:create(2, 100),
        cc.ProgressTo:create(0, 0),
    }

    local repeatAct = cc.RepeatForever:create(cc.Sequence:create(seq))
    heartImg:runAction(repeatAct)

    local labelImg = zc.createImageView({
        src = "res/Image/loading/loading_label.png", 
        pos = cc.p(loadingBg:getContentSize().width / 2, loadingBg:getContentSize().height / 2 - 50),
        parent = loadingBg,
        })


    loadingBg:setVisible(false)

    if delayTime == 0.5 then
        -- 延时旋转
        local actionArray = {}
        local delay = cc.DelayTime:create(delayTime)
        
        table.insert(actionArray, delay)


        local callFunc = cc.CallFunc:create(function ( sender )

            netWorkBadTimes = netWorkBadTimes + 1
            
            loadingBg:setVisible(true)

        end)
        table.insert(actionArray, callFunc)

        loadingBg:runAction(cc.Sequence:create(actionArray))
    else
        loadingTimes = 0
        
        loadingBg:setVisible(true)
    end

end


-- 关闭一个Loading
function POP.close( params )

    params = params or {}

    -- 当前关闭Loading的名称
    local loadingName = params.name or loadingBase

    for i,v in ipairs(loadingQueue) do
        if v == loadingName then
            table.remove(loadingQueue, i)
            break
        end
    end

    if #loadingQueue == 0 then
        POP.clean()
    end

    -- dump(loadingQueue, "hideLoading")


end


function POP.clean()
    if not tolua.isnull(smallLoadingWidget) then
        smallLoadingWidget:removeFromParent()
        smallLoadingWidget = nil
    end

    isShow = false

    loadingQueue = {}

    -- zc.removeFromActivePops(popName)
end

return POP