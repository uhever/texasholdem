--[[
	tipInfoPop
]]

local POP = {}

local popName = "src/app/pop/tipPop"

local tipWidget = nil

function POP.create(params)

    --查创建widget
    tipWidget = zc.createMyPop(popName, {
        mask = false,
        opacity = 0,
        zOrder = zc.Z_ORDER_TIP,
        })

    POP.createUI( params)

end


function POP.createUI( params )

    local tipLabel = zc.createLabel({
        text = params.text,
        pos = cc.p(250, 35),
        outline = true,
        outlineSize = 2,
        outlineColor = cc.c4b(112,3,118, 255),
        fontSize = 30,
        })

    -- 背景图片
    local tipBg = zc.createImageView({
        src = "res/Image/common/tip_bg.png", 
        pos = cc.p(zc.cx, zc.cy), 
        scale9 = true,
        capInsets = cc.rect(76, 17, 10, 10), 
        size = cc.size(500, 70), 
        opacity = 0,
        parent = tipWidget
        })

    tipBg:addChild(tipLabel)

    -- 淡入淡出动画
    local spawnIn = cc.Spawn:create({cc.FadeTo:create(0.2, 255), cc.MoveBy:create(0.2, cc.p(0, 100))})
    local delay = CCDelayTime:create(1.5)
    local spawnOut = cc.Spawn:create({CCFadeTo:create(0.2, 0), cc.MoveBy:create(0.2, cc.p(0, 100))})
    local removeFunc = CCCallFuncN:create(function ( node )
        -- zc.closePop(popName)
        node:getParent():removeFromParent()
    end)

    local seq = {}
    table.insert(seq, spawnIn)
    table.insert(seq, delay)
    table.insert(seq, spawnOut)
    table.insert(seq, removeFunc)

    tipBg:runAction(cc.Sequence:create(seq))
end

-- POP的清理方法，需要手动处理
function POP.clean()

    if tipWidget then
        tipWidget:removeFromParent()
        tipWidget = nil
    end

end


return POP
