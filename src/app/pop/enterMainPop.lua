--[[
	个人设置界面
]]

local POP = {}

local popName = "src/app/pop/enterMainPop"

local enterMainWidget = nil

local menuCfg = {
	{name = "quickjoin", text = zc.lang("enterMain_JoinGame", "快速加入牌局"), descText = zc.lang("enterMain_JoinGameText", "加入随机牌局，快速开始游戏！"), color1 = cc.c3b(184, 158, 123), color2 = cc.c3b(133, 122, 110)},
	{name = "season", text = zc.lang("enterMain_Season", "选择场次"), descText = zc.lang("enterMain_SeasonText", "选择进入初级场，高级场等。"), color1 = cc.c3b(130, 145, 177), color2 = cc.c3b(110, 113, 133)},
	-- {name = "champion", text = zc.lang("enterMain_Champion", "比赛场"), descText = zc.lang("enterMain_ChampionText", "加入 SNG MTT 赛事！"), color1 = cc.c3b(166, 121, 104), color2 = cc.c3b(131, 106, 96)},
	
}

local menuSubCfg = {}

function POP.create( params )
	zc.addToActivePops(popName)

	enterMainWidget = zc.createMyPop(popName, {
		opacity = 0,
		bgType = 0,
		bgSrc = "res/Image/main/main_bg.jpg",
		title = {
			center = {text = zc.lang("enterMain_CenterName", "德州扑克")},
		},
		bottom = {
			index = 2,
		}
		})

	POP.init(params)

	POP.createUI()
end

function POP.init( params )

	
end

function POP.createUI(  )

	local menuListView = zc.createListView({
        inertiaScroll = true,
        -- bounce = true,
        size = cc.size(zc.width, zc.height - 250),
        pos = cc.p(0, 130),
        parent = enterMainWidget,
        direction = ccui.ScrollViewDir.none,
        })

	menuListView:setScrollBarEnabled(false)

    for i, v in pairs(menuCfg) do

        local cellLayout = zc.createLayout({
            size = cc.size(zc.width, 190),
            })

        local cellBg = zc.createButton({
        	name = v.name,
			normal = "res/Image/common/common_scale9_btn_n.png",
        	pressed = "res/Image/common/common_scale9_btn_p.png",
			ap = cc.p(0, 0),
			pos = cc.p(5, 0),
			scale9 = true,
			size = cc.size(740, 180),
			parent = cellLayout,
			-- shaderObj = 3,
			listener = POP.menuClickCallback,
			})

        -- icon
        local cellIcon = zc.createImageView({
			src = "res/Image/main/enter_" .. v.name .. "_icon.png",
			pos = cc.p(100, cellBg:getContentSize().height / 2), 
			parent = cellBg,
			})

        -- 描述Label
        local mainLabel = zc.createLabel({
            text = v.text, 
            pos = cc.p(510, 110), 
            parent = cellBg,
            color = v.color1,
            fontSize = 36,
            })

        local descLabel = zc.createLabel({
            text = v.descText, 
            pos = cc.p(510, 60), 
            parent = cellBg,
            color = v.color2,
            fontSize = 24,
            })
   
        menuListView:pushBackCustomItem(cellLayout)

    end

   -- menuListView:pushBackCustomItem(POP.createProcessLayout())

end

-- 创建进行中的牌局
function POP.createProcessLayout(  )
	local cellLayout = zc.createLayout({
            size = cc.size(zc.width, 230),
            })

    local cellBg = zc.createImageView({
    	name = "process",
		src = "res/Image/main/enter_process_bg.png",
		ap = cc.p(0, 0),
		pos = cc.p(5, 0),
		parent = cellLayout,
		-- shaderObj = 3,
		listener = POP.menuClickCallback,
		})

    zc.createLabel({
        text = zc.lang("enterMain_OngoingGame", "正在进行中的牌局..."), 
        pos = cc.p(cellBg:getContentSize().width / 2, cellBg:getContentSize().height - 30), 
        parent = cellBg,
        color = cc.c3b(156, 156, 156),
        fontSize = 30,
        })

    zc.createLabel({
        text = zc.lang("enterMain_Champion", "比赛场"), 
        pos = cc.p(cellBg:getContentSize().width / 2, cellBg:getContentSize().height / 2), 
        parent = cellBg,
        color = cc.c3b(255, 233, 204),
        fontSize = 36,
        })

    zc.createAttrLabel({
		{image = true, src = "res/Image/main/enter_rolenum_img.png"},
		{text = 888, fontSize = 26, color = cc.c3b(255, 233, 204), offsetX = 5, offsetY = 0},
		},
		{
		ap = cc.p(0.5, 0.5),
		pos = cc.p(160, 30),
		parent = cellBg,
		})

    zc.createAttrLabel({
		{image = true, src = "res/Image/main/enter_time_img.png"},
		{text = "00:03:32", fontSize = 26, color = cc.c3b(255, 233, 204), offsetX = 5, offsetY = 0},
		},
		{
		ap = cc.p(0.5, 0.5),
		pos = cc.p(360, 30),
		parent = cellBg,
		})

    zc.createAttrLabel({
		{image = true, src = "res/Image/main/mine_chip_icon.png"},
		{text = PlayerData.base.money, fontSize = 26, color = cc.c3b(255, 233, 204), offsetX = 5, offsetY = 0},
		},
		{
		ap = cc.p(0.5, 0.5),
		pos = cc.p(560, 30),
		parent = cellBg,
		})

    return cellLayout
end

function POP.menuClickCallback( sender, eventType )

	if eventType == ccui.TouchEventType.ended then
		local name = sender:getName()

		if name == "edit" then
			
		elseif name == "season" then
			zc.createPop("src/app/pop/texasHoldemPop")
		elseif name == "quickjoin" then
			local successCallback = function(data)
		        if(tonumber(data.status) == 1) then
		            zc.showLoading()
		        else
		            zc.getErrorMsg(data.status)
		        end
		    end
		    local canenterType = POP.getGotoRoomType(  )
		    zc.loadUrl("m=th_join_room", {
		    		type = zc.GAME_TYPE_TEXAS_NORMAL,
		    		room_type = canenterType,
		            id = 0,
		        }, successCallback)

		end

	end
end

function POP.getGotoRoomType(  )
	local canenterType = 0
	local self_money = PlayerData.base.money
	local limitTab = DB_STATICTABLE["proto_texas_holdem"]["list"]
	local enterTab = {}
	for k,v in pairs(limitTab) do
		if tonumber(k) > 6 then
			enterTab[#enterTab + 1] = v
		end
	end

	table.sort( enterTab, function ( a,b )
		return tonumber(a.default) < tonumber(b.default)
	end )
	for i,v in ipairs(enterTab) do
		if self_money > tonumber(v.default)  then
			canenterType = v.id
		end
	end
	return canenterType
end

function POP.closeCallback( sender, eventType )
	if eventType == ccui.TouchEventType.ended then
		zc.closePop(popName)
	end
end

function POP.clean(  )
	if enterMainWidget then
		enterMainWidget:removeFromParent()
		enterMainWidget = nil
	end
	zc.removeFromActivePops(popName)
end

return POP