--[[
	扎金花游戏内玩家信息
]]

local POP = {}

local popName = "src/app/pop/dzRoleInfoPop"

local dzRoleInfoWidget = nil

local roleData = {}

function POP.create( params )
	zc.addToActivePops(popName)

	dzRoleInfoWidget = zc.createMyPop(popName, {
		touchClose = true,
		opacity = 0,
		})


	POP.init(params)
end

function POP.init( params )
	
	POP.createUI(params)

end

function POP.createUI( params )


	for i,v in ipairs(params.statistics) do
		if params.roleid == v.roleid then
			roleData = v
			break
		end
	end

	roleData.money = params.money


	-- -- 测试数据
	-- roleData.money = 8888
	-- roleData.image = "https://cdnt-poker.zhanchenggame.com/cdn/poker/icon/645a4ce97269c235e499037b3a771e13481.jpg"
	-- roleData.roleid = 220000000006
	-- roleData.nickname = "文档额爱的"
	-- roleData.buy_in = 200
	-- roleData.profit = 1000


	-- if roleData.roleid == PlayerData.base.roleid then
		
	-- 	POP.createMineSubLayout()
	-- else
		POP.createOtherSubLayout()
	-- end

end

-- 创建他人界面
function POP.createOtherSubLayout(  )

	local contentBg = zc.createImageView({
		src = "res/Image/game/roleinfo/roleinfo_bg.png",
		pos = cc.p(zc.width/2, 900),
		parent = dzRoleInfoWidget,
		touchEnabled = true,
		})

	local infoLayout = zc.createLayout({
		pos = cc.p(0, 185),
		parent = contentBg,
		})


    -- 头像
	local headBg = zc.createImageView({
		scale = 0.5,
		src = "res/Image/main/mine_head_bg_2.png",
		pos = cc.p(75, 100),
		parent = infoLayout,
		})

	zc.createHeadIcon( {
		image = roleData.image,
		shapeType = 1,
		scale = 1.7,
		pos = cc.p(headBg:getContentSize().width / 2, headBg:getContentSize().height / 2),
		parent = headBg,
		} )

	local genderIcon = zc.createImageView({
		src = "res/Image/main/mine_gender_2.png",
		pos = cc.p(150, 30),
		parent = headBg,
		scale = 1.2,
		})

	local SpeakIcon = zc.createImageView({
		src = "res/Image/main/common_speak.png",
		pos = cc.p(20, 30),
		parent = headBg,
		scale = 1.4,
		})

	local nameLabel = zc.createLabel({
		text = zc.subName(roleData.nickname),
		color = cc.c3b(135, 138, 147),
		pos = cc.p(145, 110),
		ap = cc.p(0, 0.5),
		fontSize = 24,
		parent = infoLayout,
		})

	local uidLabel = zc.createLabel({
		text = "ID:" .. roleData.roleid,
		color = cc.c3b(135, 138, 147),
		pos = cc.p(145, 75),
		ap = cc.p(0, 0.5),
		fontSize = 22,
		parent = infoLayout,
		})

	

	zc.createLabel({
		text = zc.lang("dzBuyChip_AlreadyCarry", "已带入: ") .. roleData.buy_in,
		color = cc.c3b(135, 138, 147),
		pos = cc.p(20, 22),
		ap = cc.p(0, 0.5),
		fontSize = 20,
		parent = infoLayout,
		})

	zc.createLabel({
		text = zc.lang("dzRoleinfo_ScoreBoard", "当前记分牌: ") .. roleData.money,
		color = cc.c3b(135, 138, 147),
		pos = cc.p(310, 22),
		fontSize = 20,
		parent = infoLayout,
		})

	zc.createLabel({
		text = zc.lang("dzRoleinfo_Profit", "桌内盈利: ") .. roleData.profit,
		color = cc.c3b(135, 138, 147),
		pos = cc.p(620, 22),
		ap = cc.p(1, 0.5),
		fontSize = 20,
		parent = infoLayout,
		})

	local effectLayout = zc.createLayout({
		pos = cc.p(0, 0),
		parent = contentBg,
		})

	

	local emotionList = zc.createListView({
		-- mask = true,
		direction = ccui.ScrollViewDir.horizontal,
		inertiaScroll = true,
		bounce = true,
		size = cc.size(contentBg:getContentSize().width - 40, 180),
		ap = cc.p(0,0),
		pos = cc.p(20, 0),
		parent = effectLayout,
		})

	for i=1,9 do
		local cellLayout = zc.createLayout({
			tag = i,
			size = cc.size(130, 180),
			listener = POP.emotionClickCallback,
			})
		-- local emotionEffect, emotionAnimation = display.getAnimation("toast_emotion_" .. i)
	 --   	emotionEffect:setPosition(cellLayout:getContentSize().width / 2, cellLayout:getContentSize().height / 2 + 20)
	 --   	emotionEffect:setScale(1)
	 --   	cellLayout:addChild(emotionEffect)
		-- display.playAnimationForever(emotionEffect, emotionAnimation, 0.5)

		zc.createImageView({
			src = "res/Image/game/roleinfo/toast_emotion_static_" .. i .. ".png",
			pos = cc.p(cellLayout:getContentSize().width / 2, cellLayout:getContentSize().height / 2 + 20),
			parent = cellLayout,
			})


		local moneyAttrLabel = zc.createAttrLabel({
			{image = true, src = "res/Image/main/mine_chip_icon.png", scale = 0.8},
			{text = 10, fontSize = 22, color = cc.c3b(239, 202, 140), offsetX = 10},
			},
			{
			ap = cc.p(0.5, 0.5),
			pos = cc.p(cellLayout:getContentSize().width / 2, 35),
			parent = cellLayout,
			})

		emotionList:pushBackCustomItem(cellLayout)
	end


end


function POP.createMineSubLayout(  )


	
end


function POP.createToastAction( params )

    local startPos = params.startPos
    local endPos = params.endPos
    local parent = params.parent
    local zOrder = params.zOrder
    local emotionId = params.emotionId


    

    

    

    local goldEffect = nil
    if emotionId == 8 then
    	local effect, animation = display.getAnimation("toast_emotion_8_pre", 1.5)
        effect:setLocalZOrder(zOrder)
        effect:setPosition(cc.p(startPos.x, startPos.y + 20))
        parent:addChild(effect)

        display.playAnimationOnce( effect, animation)


        -- 顺序
	    local aniArray = {}


	    
	    table.insert(aniArray, cc.DelayTime:create(1.5))

	    local spawn1Array = {}
	    local moveTime = cc.pGetDistance(startPos, endPos) / 1600
	    table.insert(spawn1Array, cc.EaseSineIn:create(cc.MoveTo:create(moveTime, endPos)))
	    table.insert(spawn1Array, cc.RotateBy:create(moveTime, 180))

	    table.insert(aniArray, cc.Spawn:create(spawn1Array))


	    table.insert(aniArray, cc.CallFunc:create(function ( sender )
	        local effect, animation = display.getAnimation("toast_emotion_" .. emotionId, 1.5)
	        effect:setLocalZOrder(zOrder)
	        effect:setPosition(cc.p(endPos.x, endPos.y + 40))
	        parent:addChild(effect)

	        display.playAnimationOnceAndCleanup( effect, parent, animation)
	    end))

	    
	    table.insert(aniArray, cc.FadeOut:create(0.1))
	    table.insert(aniArray, cc.RemoveSelf:create())

	    effect:runAction(cc.Sequence:create(aniArray))


    else
    	local goldEffect = zc.createImageView({
	        src = "res/Image/game/roleinfo/toast_emotion_img_" .. emotionId .. ".png",
	        pos = startPos,
	        opacity = 0,
	        scale = 0.1,
	        parent = parent,
	        zOrder = zOrder,
	        })


    	-- 顺序
	    local aniArray = {}

	    local spawnArray = {}
	    table.insert(spawnArray, cc.FadeIn:create(0.2))
	    table.insert(spawnArray, cc.ScaleTo:create(0.2, 1.0))
	    table.insert(spawnArray, cc.MoveBy:create(0.2, cc.p(0, 50)))
	    table.insert(aniArray, cc.Spawn:create(spawnArray))

	    table.insert(aniArray, cc.EaseBackOut:create(cc.MoveBy:create(0.3, cc.p(0, -50))))

	    local spawn1Array = {}
	    local moveTime = cc.pGetDistance(startPos, endPos) / 1600

	    table.insert(spawn1Array, cc.EaseSineIn:create(cc.MoveTo:create(moveTime, endPos)))
	    table.insert(spawn1Array, cc.RotateBy:create(moveTime, 360))

	    table.insert(aniArray, cc.Spawn:create(spawn1Array))


	    table.insert(aniArray, cc.CallFunc:create(function ( sender )
	        local effect, animation = display.getAnimation("toast_emotion_" .. emotionId, 1.5)
	        effect:setLocalZOrder(zOrder)
	        effect:setPosition(endPos)
	        parent:addChild(effect)

	        if emotionId == 1 then -- 鞭炮
	        	effect:setPosition(cc.p(endPos.x + 20, endPos.y))
	        end

	        display.playAnimationOnceAndCleanup( effect, parent, animation)
	    end))

	    
	    table.insert(aniArray, cc.FadeOut:create(0.1))
	    table.insert(aniArray, cc.RemoveSelf:create())

	    goldEffect:runAction(cc.Sequence:create(aniArray))

    end

    

end


function POP.addFriendcallback( sender, eventType )
	if eventType == ccui.TouchEventType.ended then
		
	end
end


function POP.emotionClickCallback( sender, eventType )
	if eventType == ccui.TouchEventType.ended then
		if zc.isSelf(roleData.roleid) then
			zc.tipInfo(zc.lang("dzRoleinfo_Hello", "您不能对自己打招呼"))
			return
		end
		local magicId = sender:getTag()
		zc.socket.sendTable({m = "send_magic", to_id = roleData.roleid, magic_id = magicId})
	end
end


function POP.closeCallback( sender, eventType )

	if eventType == ccui.TouchEventType.ended then
		zc.closePop(popName)
	end
end


function POP.clean(  )
	if dzRoleInfoWidget then
		dzRoleInfoWidget:removeFromParent()
		dzRoleInfoWidget = nil
	end

	roleData = nil

	zc.removeFromActivePops(popName)
end

return POP