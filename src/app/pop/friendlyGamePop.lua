--[[
	好友对局界面
]]

local POP = {}

local popName = "src/app/pop/friendlyGamePop"

local friendlyGameWidget = nil

local editLabelTb = nil

function POP.create( params )
	zc.addToActivePops(popName)
	friendlyGameWidget = zc.createMyPop(popName, {
		opacity = 0,
		bgType = 0,
		bgSrc = "res/Image/main/main_bg.jpg",
		title = {
			center = {text = "好友对局"},
			left = {type = "return"},
			right = {type = "shop"},
		}
		})

	POP.init(params)

	POP.createjoinUI()
	POP.createfriendUI()
end

function POP.init( params )

	editLabelTb = {}
	
end

function POP.createjoinUI(  )

	local moneyBg = zc.createImageView({
		src = "res/Image/shop/shop_money_bg.png",
		pos = cc.p(zc.width / 2, zc.height - 150),
		parent = friendlyGameWidget,
		})


	zc.createAttrLabel({
		{image = true, src = "res/Image/main/mine_chip_icon.png"},
		{text = zc.getWan(PlayerData.base.money, 4), fontSize = 26, color = cc.c3b(239, 202, 140), offsetX = 10, offsetY = 5},
		},
		{
		ap = cc.p(0.5, 0.5),
		pos = cc.p(zc.width / 2, zc.height - 150),
		parent = friendlyGameWidget,
		})

	local joinFriendBg = zc.createImageView({
		src = "res/Image/game/friendgame/friendgame_joinfriend_bg.png",
		pos = cc.p(zc.width / 2, zc.height - 390),
		parent = friendlyGameWidget,
		}) 

	zc.createImageView({
		src = "res/Image/game/friendgame/friendgame_title_decoration.png",
		ap = cc.p(0.5, 1),
		pos = cc.p(joinFriendBg:getContentSize().width / 2 - 157 , joinFriendBg:getContentSize().height - 50),
		rotate = 180,
		parent = joinFriendBg,
		})

	zc.createAttrLabel({
		-- {image = true, src = "res/Image/game/friendgame/friendgame_title_decoration.png", },
		{text = zc.lang("friendlyGame_JoinFriend", "进入好友牌局"), fontSize = 26, color = cc.c3b(239, 202, 140)},
		{image = true, src = "res/Image/game/friendgame/friendgame_title_decoration.png", offsetX = 5, offsetY = 8},
		},
		{
		ap = cc.p(0, 0.5),
		pos = cc.p(joinFriendBg:getContentSize().width / 2 - 77, joinFriendBg:getContentSize().height - 42),
		parent = joinFriendBg,
		})

	zc.createImageView({
		src = "res/Image/game/friendgame/friendgame_joinfriend_img.png",
		pos = cc.p(joinFriendBg:getContentSize().width / 2, joinFriendBg:getContentSize().height / 2 + 65),
		parent = joinFriendBg,
		})

	zc.createLabel({
		text = zc.lang("friendlyGame_Input", "输入邀请码 , 和好友一起切磋"),
		color = cc.c3b(96, 98, 104),
		pos = cc.p(joinFriendBg:getContentSize().width / 2, joinFriendBg:getContentSize().height / 2 - 30),
		fontSize = 26,
		parent = joinFriendBg,
		})

	for i=1,6 do
		local editImg = zc.createImageView({
		    src = "res/Image/game/friendgame/friendgame_joinfriend_password.png",
		    pos = cc.p(195 + (i - 1) * 75, 78),
		    parent = joinFriendBg,
		    listener = function ( sender, eventType )
		    	if eventType == ccui.TouchEventType.ended then
		    		POP.createKeyboard( )
		    	end
		    end
		    })

		local editLabel = zc.createLabel({
			text = "",
			-- color = cc.c3b(96, 98, 104),
			pos = cc.p(editImg:getContentSize().width / 2, editImg:getContentSize().height / 2),
			fontSize = 36,
			parent = editImg,
			})

		table.insert(editLabelTb, editLabel)
	end


	
end
function POP.createfriendUI(  )
	local createFriendBg = zc.createImageView({
		src = "res/Image/game/friendgame/friendgame_create_bg.png",
		pos = cc.p(0, 135),
		ap = cc.p(0, 0),
		parent = friendlyGameWidget,
		}) 

	zc.createImageView({
		src = "res/Image/game/friendgame/friendgame_title_decoration.png",
		ap = cc.p(0.5, 1),
		pos = cc.p(createFriendBg:getContentSize().width / 2 - 157 , createFriendBg:getContentSize().height - 50),
		rotate = 180,
		parent = createFriendBg,
		})

	zc.createAttrLabel({
		{text = zc.lang("friendlyGame_CreateFriend", "创建好友牌局"), fontSize = 26, color = cc.c3b(239, 202, 140)},
		{image = true, src = "res/Image/game/friendgame/friendgame_title_decoration.png", offsetX = 5, offsetY = 8},
		},
		{
		ap = cc.p(0, 0.5),
		pos = cc.p(createFriendBg:getContentSize().width / 2 - 77, createFriendBg:getContentSize().height - 42),
		parent = createFriendBg,
		})

	zc.createLabel({
		text = zc.lang("friendlyGame_Setting", "设置携带上限"),
		color = cc.c3b(96, 98, 104),
		pos = cc.p(createFriendBg:getContentSize().width / 2 + 15, createFriendBg:getContentSize().height - 140),
		fontSize = 26,
		parent = createFriendBg,
		})

	local lineBg = zc.createImageView({
		src = "res/Image/game/friendgame/friendgame_create_line.png",
		pos = cc.p(createFriendBg:getContentSize().width / 2, createFriendBg:getContentSize().height / 2),
		parent = createFriendBg,
		})
	for i=1, 6 do
		zc.createLabel({
			tag = i,
			text = i * 10 .. zc.lang("commonHelper_Million", "万"),
			color = cc.c3b(171, 172, 182),
			pos = cc.p(lineBg:getContentSize().width / 5 * (i - 1), lineBg:getContentSize().height / 2 + 55),
			fontSize = 24,
			parent = lineBg,
			})

		zc.createImageView({
			src = "res/Image/game/friendgame/friendgame_create_spot.png",
			pos = cc.p(lineBg:getContentSize().width / 5 * (i - 1), lineBg:getContentSize().height / 2),
			parent = lineBg,
			})
	end
	lineBg:getChildByTag(1):setColor(cc.c3b(239, 202, 140))
	-- 每个关键点间隔
	local marginX = 125
	-- 滑动阈值
	local thresholdPosX = 20

	-- 保存关键点
	local keyPoints = {}
	for i=1,6 do
		keyPoints[i] = (i-1) * marginX
	end

	-- 按钮点击时的位置
	local beginPosX = 0
	-- 按钮点击时的真实位置(相对父节点)
	local realStartPosX = 0
	zc.createImageView({
		tag = 1,
		src = "res/Image/game/friendgame/friendgame_create_chip.png",
		parent = lineBg,
		listener = function ( sender, eventType )
			if eventType == ccui.TouchEventType.began then
				beginPosX = sender:getTouchBeganPosition().x
				realStartPosX = (sender:getTag() - 1) * marginX				
			elseif eventType == ccui.TouchEventType.moved then
				local movePosX = sender:getTouchMovePosition().x
				-- 移动时的真实位置(相对父节点)
				local realNowPosX = realStartPosX + movePosX - beginPosX
				-- 遍历关键点，在阈值范围内时操作
				for i,v in ipairs(keyPoints) do
					if realNowPosX > (v - thresholdPosX) and realNowPosX < (v + thresholdPosX) then
						sender:setPositionX(v)
						for i=1, 6 do
							lineBg:getChildByTag(i):setColor(cc.c3b(171, 172, 182))
						end
						lineBg:getChildByTag(i):setColor(cc.c3b(239, 202, 140))
						sender:setTag(i)
						break
					end
				end

			elseif eventType == ccui.TouchEventType.ended or eventType == ccui.TouchEventType.canceled then		
			end
		end
		})

	zc.createLabel({
		text = zc.lang("texasHoldemScene_Blind", "盲注："),
		color = cc.c3b(171, 172, 182),
		pos = cc.p(createFriendBg:getContentSize().width / 2 - 20, createFriendBg:getContentSize().height / 2 - 65),
		fontSize = 24,
		parent = createFriendBg,
		})

	zc.createLabel({
		text = "54/78",
		color = cc.c3b(171, 172, 182),
		ap = cc.p(0, 0.5),
		pos = cc.p(createFriendBg:getContentSize().width / 2 + 10, createFriendBg:getContentSize().height / 2 - 65),
		fontSize = 24,
		parent = createFriendBg,
		})


	local btnText = zc.lang("friendlyGame_Begin", "开 始 牌 局")
	local roomId = nil
	local gameType = nil
	if PlayerData.friend_room_id and PlayerData.friend_room_id.room_id ~= 0 then
		btnText = "回 到 房 间"
		roomId = PlayerData.friend_room_id.room_id
		gameType = PlayerData.friend_room_id.game_type
	end

	zc.createButton({
		text = btnText, 
		normal = "res/Image/game/session/session_btn_scale9_n.png",
		pressed = "res/Image/game/session/session_btn_scale9_p.png",
		pos = cc.p(createFriendBg:getContentSize().width / 2, 100),
		scale9 = true,
		size = cc.size(300, 65),
		parent = createFriendBg,
		listener = function ( sender, eventType )
			if eventType == ccui.TouchEventType.ended then
			
				local successCallback = function(data)
			        if(tonumber(data.status) == 1) then
			            zc.showLoading()
			        else
			            zc.getErrorMsg(data.status)
			        end
			    end
			    
			    if roomId and gameType then 
			    	zc.loadUrl("m=friend_room_join", {
			    			room_id = roomId,
			    			pwd = "",
				        }, successCallback)
			    else
			    	zc.loadUrl("m=friend_room_create", {
			    		room_limit = json.encode({
			    			pwd = "",
			    			f_r_l_g_type = zc.GAME_TYPE_TEXAS_FRIEND,
			    			f_r_l_r_type = 6,
			    			}),
				        }, successCallback)
			    end

			    
			end
		end 
		})



end


function POP.createKeyboard( )


	local keyboardBg = nil
	local keyboardLayout = zc.createLayout({
		-- mask = true,
		size = cc.size(zc.width, zc.height * 2),
		parent = friendlyGameWidget,
		listener = function ( sender, eventType )

			if eventType == ccui.TouchEventType.ended then
				local moveAct = cc.EaseExponentialIn:create(cc.MoveTo:create(0.2, cc.p(zc.cx, -650)))
				local callfunc = cc.CallFunc:create(function (  )
					sender:removeFromParent()
				end)
				keyboardBg:runAction(cc.Sequence:create({moveAct, callfunc}))

			end

			
		end
		})


	keyboardBg = zc.createImageView({
		src = "res/Image/game/friendgame/friendgame_keyboard_bg.png",
		ap = cc.p(0.5, 0),
		pos = cc.p(zc.cx, -650),
		parent = keyboardLayout,
		touchEnabled = true,
		})

	local moveAct = cc.EaseExponentialOut:create(cc.MoveTo:create(0.3, cc.p(zc.cx, 0)))
	keyboardBg:runAction(moveAct)


	for i=1,4 do
		for j=1,3 do
			local idx = (i-1) * 3 + j

			local keyBg = zc.createImageView({
				tag = idx,
				src = "res/Image/game/friendgame/friendgame_key_bg.png",
				pos = cc.p(150 + (j-1) * 225, 550 - (i-1) * 150),
				parent = keyboardBg,
				listener = POP.onKeyCallback,
				})

			local keyText = idx
			if idx == 10 then
				keyText = "重输"
			elseif idx == 11 then
				keyText = 0
				keyBg:setTag(0)
			elseif idx == 12 then
				keyText = "删除"
			end

			zc.createLabel({
				text = keyText,
				fontSize = 40,
				pos = cc.p(keyBg:getContentSize().width / 2, keyBg:getContentSize().height / 2),
				parent = keyBg,
				})

		end
	end


end


function POP.onKeyCallback( sender, eventType )
	if eventType == ccui.TouchEventType.ended then

		local tag = sender:getTag()

		if tag == 10 then
			for i,v in ipairs(editLabelTb) do
				v:setString("")
			end
		elseif tag == 12 then
			for i=#editLabelTb,1,-1 do
				if editLabelTb[i]:getString() ~= "" then
					editLabelTb[i]:setString("")
					break
				end
			end
		else

			local idx = 0

			for i,v in ipairs(editLabelTb) do

				if v:getString() == "" then
					editLabelTb[i]:setString(tag)
					idx = i
					break
				end
			end

			-- 加入房间
			if idx == 6 then
				local successCallback = function(data)
			        if(tonumber(data.status) == 1) then
			            zc.showLoading()
			        else
			        	for i,v in ipairs(editLabelTb) do
					    	v:setString("")
						end
			            zc.getErrorMsg(data.status)
			        end
			    end

			    local catString = ""
			    for i,v in ipairs(editLabelTb) do
			    	catString = catString .. v:getString()
				end
			    
			    zc.loadUrl("m=friend_room_join", {
		    			room_id = catString,
		    			pwd = "",
			        }, successCallback)
			end

		end

		
	end
end


function POP.closeCallback( sender, eventType )
	if eventType == ccui.TouchEventType.ended then
		zc.closePop(popName)
	end
end

function POP.clean(  )
	if friendlyGameWidget then
		friendlyGameWidget:removeFromParent()
		friendlyGameWidget = nil
	end

	editLabelTb = nil

	zc.removeFromActivePops(popName)
end

return POP


