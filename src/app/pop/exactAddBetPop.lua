--[[
	精确加注
]]

local POP = {}

local popName = "src/app/pop/exactAddBetPop"

local exactAddBetWidget = nil

local bgImg = nil
local addNum = nil
local minAdd = nil
local maxAdd = nil
local ground = nil
function POP.create( params )
	
	zc.addToActivePops(popName)

	exactAddBetPop = zc.createMyPop(popName, {
		opacity = 0,
		touchClose = true,
		})

	POP.init(params)
end

function POP.init( params )
	minAdd = params.min
	maxAdd = params.max
	ground = params.ground
	POP.createUI()
end

function POP.createUI(  )
	bgImg = zc.createImageView({
		src = "res/Image/game/addbet_input_bg.png",
		pos = cc.p(zc.cx, 469),
		touchEnabled = true,
		parent = exactAddBetPop,
		})

	for i = 1, 2 do
		for j = 1, 5 do
			local idx = (i-1)*5 + j - 1
			local numBtn = zc.createButton({
				text = idx,
				fontSize = 35,
				fontColor = cc.c3b(255,255,255),
				normal = "res/Image/game/addbet_input_n.png",
				pressed = "res/Image/game/addbet_input_p.png",
				tag = idx,
				pos = cc.p(55 + (j-1) * 100, 200 - (i-1)*100),		
				parent = bgImg,
				listener = POP.NumberCallback,
				})
			numBtn.num = idx
		end
	end
	
	local addnumLabel = zc.createLabel({
		text = "0",
		color = cc.c3b(255,255,255),
		pos = cc.p(70,30),
		fontSize = 35,
		parent = bgImg,
		})
	exactAddBetPop.addnumLabel = addnumLabel
	zc.createButton({
		normal = "res/Image/game/addbet_delete_n.png",
		pressed = "res/Image/game/addbet_delete_p.png",
		pos = cc.p(bgImg:getContentSize().width - 70, 30),
		touchEnabled = true,
		parent = bgImg,
		listener = POP.deleteNumCallback,
		})



	zc.createImageView({
		src = "res/Image/game/add_btn.png",
		pos = cc.p(bgImg:getContentSize().width/2, -20),
		parent = bgImg,
		listener = function ( sender, eventType )
		 	if eventType == ccui.TouchEventType.ended then
		 		if addNum and tonumber(addNum) > 0 then
		 			if tonumber(addNum) < minAdd or tonumber(addNum) > maxAdd then
		 				zc.tipInfo(zc.lang("exactAddBet_FillingBeyondLimit", "加注数量超出限制"))
		 				return
		 			end
		 			zc.socket.sendTable({m = "texas_holdem_add", num = tonumber(addNum)})
		 		end
		 	end
		 end,
		})
end

function POP.NumberCallback( sender, eventType )
	if eventType == ccui.TouchEventType.ended then
		if not addNum or tonumber(addNum) == 0 then
			addNum = sender.num
		else
			addNum = addNum .. sender.num
		end
		exactAddBetPop.addnumLabel:setString(zc.getWan(tonumber(addNum),4))
	end
end

function POP.deleteNumCallback( sender, eventType )
	if eventType == ccui.TouchEventType.ended then
		if not addNum  then return end
		if string.len(addNum) == 1 then
			addNum = nil
			exactAddBetPop.addnumLabel:setString("0")
			return
		end
		addNum = string.sub(addNum, 1, string.len(addNum)-1)
		exactAddBetPop.addnumLabel:setString(zc.getWan(tonumber(addNum),4))
	end
end



function POP.closeCallback( sender, eventType )
	if eventType == ccui.TouchEventType.ended then
		ground.controlBar.addChipBtn:setVisible(true)
		zc.closePop(popName)
	end
end


function POP.clean(  )
	if exactAddBetPop then
		exactAddBetPop:removeFromParent()
		exactAddBetPop = nil
	end
	addNum = nil
	minAdd = nil
	maxAdd = nil
	ground = nil
	zc.removeFromActivePops(popName)
end

return POP