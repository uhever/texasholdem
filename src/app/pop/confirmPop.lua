local  POP = {}

-- view
local popName = "src/app/pop/confirmPop"

local confirmWidget = nil

local okCallback = nil
local cancelCallback = nil


function POP.create(params)
    

    zc.addToActivePops(popName)

    local isTouchClose = true
    
    if type(params.touchClose) == "boolean" and params.touchClose == false then -- 此时点击空白不关闭弹窗
        isTouchClose = false
    end

    local zOrder = nil
    if params.godMode then
        zOrder = zc.Z_ORDER_CONFIRM_SYSTEM
    end

    confirmWidget = zc.createMyPop(popName, {
        mask = true,
        touchClose = isTouchClose,
        zOrder = zOrder,
        })

    -- 1.普通文本
    local text = params.text or ""

    -- 按钮数目
    local btnNum = params.btnNum or 1

    okCallback = params.okCallback or nil
    cancelCallback = params.cancelCallback or nil
    local okText = params.okText or zc.lang("confirm_Sure", "确定")
    local cancelText = params.cancelText or zc.lang("confirm_Cancel", "取消")
    

    local confirmMain = zc.createImageView( {
        src = "res/Image/confirm/confirm_scale9_bg.png",
        pos = cc.p(zc.cx, zc.cy),
        scale9 = true,
        size = cc.size(564, 310),
        parent = confirmWidget
        } )

    zc.elasticInAction(confirmMain)

    -- zc.createLabel({
    --     text = zc.lang("confirm_Tip", "提 示"), 
    --     color = cc.c3b(239, 202, 140),
    --     pos = cc.p(confirmMain:getContentSize().width / 2, confirmMain:getContentSize().height - 40), 
    --     fontSize = 26,
    --     parent = confirmMain
    --     })


    local width = display.calcStrSize(text, display.FONT1, 32)
    local size = nil
    local pos = cc.p(confirmMain:getContentSize().width / 2, confirmMain:getContentSize().height / 2 + 80)
    if width > 460 then
        size = cc.size(460, 160)
        pos = cc.p(confirmMain:getContentSize().width / 2 + 11, confirmMain:getContentSize().height / 2 + 25)
    end
    local confirmText = zc.createLabel({
        text = text or "", 
        size = size,
        color = cc.c3b(255, 248, 235),
        pos = pos, 
        size = size,
        fontSize = 32,
        parent = confirmMain
        })
  

    -- 确认按钮
    local confirmButton = zc.createButton({ 
        text = "", 
        -- fontColor = cc.c3b(155, 159, 167),
        pos = cc.p(confirmMain:getContentSize().width / 2, 80),
        listener = POP.confirmCallabck,
        parent = confirmMain,
        -- normal = "res/Image/record/record_detail_btn_n.png",
        -- pressed = "res/Image/record/record_detail_btn_p.png",
        normal = "res/Image/confirm/confirm_btn_sure_scale9_n.png",
        pressed = "res/Image/confirm/confirm_btn_sure_scale9_p.png",
        size = cc.size(136, 60),
        })

    if(btnNum == 0) then
        confirmButton:setVisible(false)
        confirmButton:setEnabled(false)
    else
        confirmButton:setTitleText(tostring(okText))
    end
    confirmButton:setTitleFontSize(24)
    confirmButton:setTitleColor(cc.c3b(72, 73, 94))


    -- 需要显示取消按钮
    if(btnNum >= 2) then
        confirmButton:setPositionX(confirmMain:getContentSize().width / 2 - 90)

        local cancelButton = zc.createButton({
            text = "", 
            -- fontColor = cc.c3b(155, 159, 167),
            pos = cc.p(confirmMain:getContentSize().width / 2 + 90, 80),
            parent = confirmMain, 
            listener = POP.confirmCancelCallback,
            -- normal = "res/Image/record/record_detail_btn_n.png",
            -- pressed = "res/Image/record/record_detail_btn_p.png",
            normal = "res/Image/confirm/confirm_btn_cancel_scale9_n.png",
            pressed = "res/Image/confirm/confirm_btn_cancel_scale9_p.png",
            size = cc.size(136, 60),
            })

        cancelButton:setTouchEnabled(true)
        cancelButton:setVisible(true)
        cancelButton:setTitleText(tostring(cancelText))

        cancelButton:setTitleFontSize(24)
        cancelButton:setTitleColor(cc.c3b(77, 70, 55))
    end



end


function POP.confirmCallabck(pSender, eventType)

    if eventType == TOUCH_EVENT_ENDED then

        if(okCallback ~= nil) then
            okCallback()
        end

        zc.closePop(popName)

    end
end

function POP.confirmCancelCallback( pSender, eventType )
    if eventType == TOUCH_EVENT_ENDED then

        if(cancelCallback ~= nil) then
            cancelCallback()
        end

        zc.closePop(popName)

    end
end
function POP.closeCallback( sender, eventType )
    if eventType == ccui.TouchEventType.ended then

        zc.closePop(popName)
    end
end


-- POP的清理方法，需要手动处理
function POP.clean()
    if not tolua.isnull(confirmWidget) then
        confirmWidget:removeFromParent(true)
        confirmWidget = nil
    end

    okCallback = nil
    cancelCallback = nil

    zc.removeFromActivePops(popName)

end

return POP


