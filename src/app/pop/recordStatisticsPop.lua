--[[
	基础统计
]]

local POP = {}

local popName = "src/app/pop/recordStatisticsPop"

local datatestWidget = nil

local switchStatu = nil

local imageviewswitchBg = nil

local imageviewBg = nil

local attrLabelLeft = nil

local attrLabelRight = nil

local upData = nil

function POP.create( params )
	zc.addToActivePops(popName)
	datatestWidget = zc.createMyPop(popName, {
		opacity = 0,
		bgType = 0,
		bgSrc = "res/Image/main/main_bg.jpg",
		title = {
			center = {text = zc.lang("recordStatistics_Statistics", "基础统计")},
			left = {type = "return"},
		}
		})
	POP.init(params)
end

function POP.init( params )
	upData = {{"0", "3072", "-8941", "-941", "+8978", "+1978", 19, 59, 41, 50}, 
			  {"10", "243", "-941", "+159", "+878", "-978", 45, 40, 60, 20}, 
			  {"20", "8072", "-10941", "-9941", "+50978", "+10978", 39, 29, 78, 90}}
	switchStatu = 1
	POP.createUI()
end

function POP.createUI(  )
	imageviewswitchBg = zc.createImageView({
		src = "res/Image/record/dataAnalysis/record_Statistics_top_bg.png",
		ap = cc.p(0, 1),
		pos = cc.p(0, zc.height - 120),
		parent = datatestWidget,
		size = cc.size(zc.width, 70),
		scale9 = true,
		gray = true,
		})

	-- 顶部节点参数{rotate，gray}
	local dataTopCfg = {{name = zc.lang("recordStatistics_SwitchAll", "全部"), ispressed = "p"}, 
						{name = zc.lang("recordStatistics_SwitchSevenDays", "近七日"), ispressed = "n"}, 
						{name = zc.lang("recordStatistics_SwitchOneMonth", "近一月"), ispressed = "n"},}
	for i, v in ipairs(dataTopCfg) do
		local imageviewswitch = zc.createImageView({
			tag = i,
			src = "res/Image/record/dataAnalysis/record_statistics_btn_" .. v.ispressed .. ".png",
			pos = cc.p(imageviewswitchBg:getContentSize().width / 6 * (2 * i - 1), imageviewswitchBg:getContentSize().height / 2 + 5),
			parent = imageviewswitchBg,
			size = cc.size(zc.width / 3, 80),
			scale9 = true,
			listener = POP.switchClickCallback,
			})
		zc.createLabel({
			text = v.name,
			pos = cc.p(imageviewswitchBg:getContentSize().width / 6 * (2 * i - 1), imageviewswitchBg:getContentSize().height / 2 + 5),
			parent = imageviewswitchBg,
			color = cc.c3b(77, 72, 68),
       		fontSize = 24,
			})
	end

	imageviewBg = zc.createImageView({
		src = "res/Image/record/dataAnalysis/record_statistics_bg.png",
		ap = cc.p(0 , 1),
		pos = cc.p(0, zc.height - 115 - imageviewswitchBg:getContentSize().height),
		parent = datatestWidget,
		size = cc.size(zc.width, 795),
		scale9 = true,
		})

	attrLabelLeft = zc.createAttrLabel({
		{text = zc.lang("recordStatistics_PlayMun", "牌局数:"), fontSize = 26, color = cc.c3b(199, 199, 199)},
		{text = 0, fontSize = 26, color = cc.c3b(211, 171, 108)},
		},
		{
		ap = cc.p(0, 0.5),
		pos = cc.p(40, 745),
		parent = imageviewBg,
		})

	attrLabelRight = zc.createAttrLabel({
		{text = zc.lang("recordStatistics_HandNum", "手牌数:"), fontSize = 26, color = cc.c3b(199, 199, 199)},
		{text = 0, fontSize = 26, color = cc.c3b(211, 171, 108)},
		},
		{
		ap = cc.p(1, 0.5),
		pos = cc.p(710, 745),
		parent = imageviewBg,
		})

	-- 中间节点参数{“”， pos(195, 657)，color(191, 239, 255)}
	local dataMidCfg = {{zc.lang("recordStatistics_Earnings", "盈利"), 195, 657, 191, 239, 255},
						{zc.lang("recordStatistics_HundredEarnings", "平均百手盈利"), 541, 657 , 191, 239, 255},
						{zc.lang("recordStatistics_HundredBlindness", "平均白百手赢得大盲"), 200, 527, 211, 171, 108},
						{zc.lang("recordStatistics_AverageAllIn", "平均 All-in"), 546, 527, 211, 171, 108}}
	for i, v in ipairs(dataMidCfg) do
		zc.createLabel({
			text = v[1],
			pos = cc.p(v[2], v[3]),
			parent = imageviewBg,
			color = cc.c3b(199, 199, 199),
        	fontSize = 26,
			})
		zc.createLabel({
			tag = i,
			text = 0,
			pos = cc.p(v[2], v[3] - 40),
			parent = imageviewBg,
			color = cc.c3b(v[4], v[5], v[6]),
        	fontSize = 26,
			})
	end

	zc.createLabel({
		text = zc.lang("recordStatistics_Winrate", "胜率统计"),
		pos = cc.p(zc.width / 2, 365),
		parent = imageviewBg,
		color = cc.c3b(211, 171, 108),
        fontSize = 30,
		})

	-- 底部节点参数{}
	local dataLowCfg = {zc.lang("recordStatistics_TotalWinrate", "总胜率"), zc.lang("recordStatistics_ShowdownWinrate", "摊牌胜率"),
						zc.lang("recordStatistics_SeeWinrate", "看翻牌胜率"), zc.lang("recordStatistics_WithNodeWinrate", "河牌圈跟注胜率")}
	for i, v in ipairs(dataLowCfg) do
		zc.createLabel({
			text = v,
			ap = cc.p(0, 0.5),
			pos = cc.p(52, 330 - (i - 1) * 87),
			parent = imageviewBg,
			color = cc.c3b(199, 199, 199),
        	fontSize = 24,
			})
		zc.createLabel({
			tag = 4 + i,
			text = 0 .. "%",
			ap = cc.p(1, 0.5),
			pos = cc.p(700, 330 - (i - 1) * 87),
			parent = imageviewBg,
			color = cc.c3b(199, 199, 199),
        	fontSize = 24,
			})

		local progressbg = zc.createImageView({
			tag = 8 + i,
			src = "res/Image/record/dataAnalysis/record_statistics_progress_bg.png",
			pos = cc.p(zc.width / 2, 300  - (i - 1) * 87),
			size = cc.size(zc.width - 90, 18),
			scale9 = true,
			parent = imageviewBg,
			})
		zc.createImageView({
			tag = i,
			src = "res/Image/record/dataAnalysis/record_statistics_progress.png",
			ap = cc.p(0, 0),
			pos = cc.p(0, 0),
			size = cc.size((zc.width - 90) / 100 * 0, 18),
			scale9 = true,
			parent = progressbg,
			})
	end
	updatashow(upData[1])
end

function updatashow( updata )
	zc.updateAttrLabel(attrLabelLeft, {
		{},
		{text = "+" .. updata[1]}
		})
	zc.updateAttrLabel(attrLabelRight, {
		{},
		{text = "+" .. updata[2]}
		})
	for i=1, 4 do
		local itemimage = imageviewBg:getChildByTag(i)
		itemimage:setString(updata[2 + i])
	end

	for i=1, 4 do
		local itemimage = imageviewBg:getChildByTag(4 + i)
		itemimage:setString(updata[6 + i] .. "%")
		local itemprogressbg = imageviewBg:getChildByTag(8 + i)
		local itemprogress = itemprogressbg:getChildByTag(i)
		itemprogress:setContentSize(cc.size((zc.width - 90) / 100 * updata[6 + i], 18))
	end

end

function POP.switchClickCallback( sender, eventType )

	local clickStatu = sender:getTag()-- 1 2 3 分别是 全部，近七日，近一月
	local switchstatu1 = nil
    if eventType == ccui.TouchEventType.ended then
        for i=1, 3 do
        	if clickStatu == switchStatu then 
        		break
        	else
        		if clickStatu == i then 
        			local itemimagetab = imageviewswitchBg:getChildByTag(i)
        			 itemimagetab:loadTexture("res/Image/record/dataAnalysis/record_statistics_btn_p.png")
        			 switchstatu1 = i
        			 updatashow(upData[i])
        		else
        			 local itemimagetab = imageviewswitchBg:getChildByTag(i)
        			 itemimagetab:loadTexture("res/Image/record/dataAnalysis/record_statistics_btn_n.png")
        		end
        	end 
        end
        switchStatu = switchstatu1
    end
end


function POP.closeCallback( sender, eventType )
	if eventType == ccui.TouchEventType.ended then
		zc.closePop(popName)
	end
end

function POP.clean(  )
	if datatestWidget then
		datatestWidget:removeFromParent()
		datatestWidget = nil
	end
	switchStatu = nil
	imageviewswitchBg = nil
	imageviewBg = nil
	attrLabelLeft = nil
	attrLabelRight = nil
	upData = nil
	zc.removeFromActivePops(popName)
end

return POP
