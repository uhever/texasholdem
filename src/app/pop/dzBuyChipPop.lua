--[[
	德州扑克购买筹码界面
]]

local POP = {}

local popName = "src/app/pop/dzBuyChipPop"

local dzBuyChipWidget = nil

local contentBg = nil

local selectedLayout = nil

local curData = nil

local playerInfo = nil

local minMoney = nil

local maxMoney = nil

local seatPos = nil

function POP.create( params )
	zc.addToActivePops(popName)

	dzBuyChipWidget = zc.createMyPop(popName, {
		touchClose = true,
		})

	
	POP.init(params)

	POP.createUI()

end

function POP.init( params )

	playerInfo = params.myInfo
	minMoney = params.minMoney
	maxMoney = params.maxMoney
	seatPos = params.pos

	curData = DB_STATICTABLE["proto_vip_charge"]["list"]
	
end

function POP.createUI()
	contentBg = zc.createImageView({
        src = "res/Image/game/shopmini/shopmini_scale9_bg.png",
        pos = cc.p(zc.cx, zc.cy),
        scale9 = true,
        size = cc.size(600,700),
        parent = dzBuyChipWidget,
        touchEnabled = true,
        })

	zc.createImageView({
        src = "res/Image/game/shopmini/shopmini_logo_img.png",
        pos = cc.p(contentBg:getContentSize().width / 2, 350),
        parent = contentBg,
        })

	local menuCfg = {
		{text = zc.lang("dzBuyChip_BuyChip", "补充筹码"), selected = true},
		{text = zc.lang("dzBuyChip_BuyGameCurrency", "购买德州币")},
	}

	local selectedTabLayout = nil

	for i,v in ipairs(menuCfg) do

		local tabLayout = zc.createLayout({
			-- mask = true,
			tag = i,
			size = cc.size(290, 80),
			pos = cc.p(10 + (i-1) * 290, 610),
			parent = contentBg,
			listener = function ( sender, eventType )
				if eventType == ccui.TouchEventType.ended then
					if sender == selectedTabLayout then return end

					local tag = sender:getTag()
					if tag == 1 then
						POP.createChipLayout()
					else
						POP.createDiamondLayout()
					end

					-- title颜色
					sender.titleLabel:setColor(cc.c3b(73, 136, 195))
					selectedTabLayout.titleLabel:setColor(cc.c3b(128, 149, 169))

					sender.segImg:loadTexture("res/Image/game/shopmini/shopmini_seg_selected.png")
					selectedTabLayout.segImg:loadTexture("res/Image/game/shopmini/shopmini_seg_normal.png")

					selectedTabLayout = sender

				end
			end
			})

		local titleLabel = zc.createLabel({
			text = v.text,
			color = v.selected and cc.c3b(73, 136, 195) or cc.c3b(128, 149, 169),
			pos = cc.p(150, 35),
			fontSize = 26,
			parent = tabLayout,
			})

		tabLayout.titleLabel = titleLabel

		

		-- 分割线
		local segImg = zc.createImageView({
			ap = cc.p(0, 0),
			pos = cc.p(0, 0),
			scale9 = true,
			size = cc.size(290, 3),
	        src = v.selected and "res/Image/game/shopmini/shopmini_seg_selected.png" or "res/Image/game/shopmini/shopmini_seg_normal.png",
	        parent = tabLayout,
	        })

		tabLayout.segImg = segImg


		if v.selected then
			selectedTabLayout = tabLayout
		end

	end

	

	-- 金币
	local moneyBg = zc.createImageView({
		src = "res/Image/shop/shop_money_bg.png",
		pos = cc.p(contentBg:getContentSize().width / 2, 550),
		parent = contentBg,
		})

	local moneyAttrLabel = zc.createAttrLabel({
		{image = true, src = "res/Image/main/mine_chip_icon.png"},
		{text = zc.getWan(PlayerData.base.money, 4), fontSize = 26, color = cc.c3b(239, 202, 140), offsetX = 10, offsetY = 5},
		},
		{
		ap = cc.p(0.5, 0.5),
		pos = cc.p(moneyBg:getContentSize().width / 2, moneyBg:getContentSize().height / 2),
		parent = moneyBg,
		})

	zc.registFlashNode( {
		node = moneyAttrLabel,
		type = zc.dataTypeMoney,
		callback = function (  )
			zc.updateAttrLabel(moneyAttrLabel, {
				{},
				{text = zc.getWan(PlayerData.base.money, 4), offsetX = 10, offsetY = 5}
				})
		end
		})


	POP.createChipLayout()

end


function POP.createDiamondLayout()
	if selectedLayout then
		selectedLayout:removeFromParent()
	end

	selectedLayout = zc.createLayout({
		parent = contentBg,
		})

	for i=1,2 do
		local data = curData[tostring(i)]

		local itemBg = zc.createImageView({
			tag = i,
			src = "res/Image/game/shop_goods_bg.png",
			pos = cc.p(150 + (i-1) * 300, 300),
			parent = selectedLayout,
			listener = POP.buyCallback,
		})
		--name
		zc.createLabel({
			text = data.money .. zc.lang("dzBuyChip_Chip", "筹码"),
			color = cc.c3b(211,171,108),
			pos = cc.p(itemBg:getContentSize().width/2 ,itemBg:getContentSize().height-35),
			fontSize = 24,
			parent = itemBg,
			})

		--icon
		zc.createImageView({
			src = "res/Image/game/shop_goods_" .. i .. ".png",
			pos = cc.p(itemBg:getContentSize().width/2 ,itemBg:getContentSize().height-130),
			parent = itemBg,
			})


		zc.createAttrLabel({
			{text = zc.lang("dzBuyChip_Give", "送"), color = cc.c3b(211,171,108), fontSize = 28},
			{image = true, src = "res/Image/main/mine_chip_icon.png", offsetX = 5},
			{text = data.give_money, color = cc.c3b(211,171,108), fontSize = 28, offsetX = 5}
			},
			{
			ap = cc.p(0.5, 0,5),
			pos = cc.p(itemBg:getContentSize().width/2, 80),
			parent = itemBg,
			})

		zc.createButton({
			normal = "res/Image/game/shop_money_btn_n.png",
			pressed = "res/Image/game/shop_money_btn_p.png",
			pos = cc.p(itemBg:getContentSize().width/2 ,40),
			parent = itemBg,
			})

		zc.createLabel({
			text = "￥" .. data.rmb,
			color = cc.c3b(0,0,0),
			pos = cc.p(itemBg:getContentSize().width/2 ,40),
			fontSize = 24,
			parent = itemBg,
			})

	end

end


function POP.createChipLayout(  )
	if selectedLayout then
		selectedLayout:removeFromParent()
	end

	selectedLayout = zc.createLayout({
		parent = contentBg,
		})

	local limitlabel = zc.createLabel({
		text = "0",
		color = cc.c3b(205, 210, 216),
		pos = cc.p(contentBg:getContentSize().width / 2, 460),
		fontSize = 98,
		parent = selectedLayout,
		})


	local limitBuy = maxMoney - playerInfo.base_info.money
    limitBuy = limitBuy > 0 and limitBuy or 0
	limitBuy = limitBuy > PlayerData.base.money and PlayerData.base.money or limitBuy

    local defaultAdd = maxMoney / 2

	--滑动条
    local slider = zc.createSlider({
		barsrc = "res/Image/game/shopmini/shopmini_progress_bg.png",
		slidballsrc = "res/Image/game/shopmini/shopmini_progress_ball.png",
		progressbarsrc = "res/Image/game/shopmini/shopmini_progress_bar.png",
		pos = cc.p(contentBg:getContentSize().width / 2,350),
		percent = 0,
     	parent = selectedLayout,
     	listener = function ( sender, eventType )
            if eventType == ccui.SliderEventType.percentChanged then
				local finalNum =  math.floor(limitBuy * sender:getPercent()/100)
            	limitlabel:setString(zc.getWan(finalNum,4))
            end
        end
     	})


    if defaultAdd >= limitBuy then
    	slider:setPercent(100)
    	limitlabel:setString(limitBuy)
    else
    	slider:setPercent(math.floor(defaultAdd/limitBuy*100))
    	limitlabel:setString(defaultAdd)
    end

    zc.createLabel({
		text = "0",
		color = cc.c3b(205, 210, 216),
		pos = cc.p(65, 390),
		fontSize = 26,
		parent = selectedLayout,
		})


    zc.createLabel({
		text = limitBuy,
		color = cc.c3b(205, 210, 216),
		pos = cc.p(contentBg:getContentSize().width-65, 390),
		fontSize = 26,
		parent = selectedLayout,
		})

    zc.createLabel({
		text = zc.lang("dzBuyChip_AddGameCurrency", "下一局开始前，将为您补充所购筹码"),
		color = cc.c3b(97, 103, 114),
		pos = cc.p(contentBg:getContentSize().width / 2, 160),
		fontSize = 26,
		parent = selectedLayout,
		})


    zc.createButton({
		text = zc.lang("confirm_Sure", "确定"),
		fontSize = 30,
		fontColor = cc.c3b(240, 242, 255),
		size = cc.size(300, 70),
		pressed = "res/Image/game/shopmini/shopmini_scale9_btn_n.png",
		normal = "res/Image/game/shopmini/shopmini_scale9_btn_p.png",
		pos = cc.p(contentBg:getContentSize().width / 2, 80),
		parent = selectedLayout,
		listener = function ( sender, eventType )
			if eventType == ccui.TouchEventType.ended then
				if seatPos and seatPos ~= 0 then
					if tonumber(limitlabel:getString()) > 0 then
						zc.socket.sendTable({m = "texas_holdem_sitdown", add_money = tonumber(limitlabel:getString()), pos = seatPos})
						zc.closePop(popName)
					end
				else
					if tonumber(limitlabel:getString()) > 0 then
						zc.socket.sendTable({m = "texas_holdem_add_money", num = limitlabel:getString()})
						zc.closePop(popName)
					end
				end
			end
		end
		})


end



function POP.closeCallback( sender, eventType )
	if eventType == ccui.TouchEventType.ended then
		zc.closePop(popName)
	end
end

function POP.buyCallback( sender, eventType )
	if eventType == ccui.TouchEventType.ended then
		print("购买金币", sender:getTag())

		local goodsData = curData[tostring(sender:getTag())]

		if LOGIN_MODE == kLoginModeNormal then
			local function successCallback(data)
				if data.status == 1 then
					
					zc.updateUserData(data)

					zc.tipInfo(zc.lang("dzBuyChip_RechargeSuccess", "成功充值") .. goodsData.money .. zc.lang("dzBuyChip_Chip", "筹码"))

				else 
					zc.getErrorMsg(data.status)
				end
			end

			 zc.loadUrl("m=user_chat", {
			 	type = 1,
			 	msginfo = ".zcmoney " .. goodsData.money
		        },
		        successCallback)
		elseif LOGIN_MODE == kLoginModeLiveSDK then

			zc.createPrepayOrderAndPay({
				type = goodsData.type,
				id  = goodsData.id,
				rmb = goodsData.rmb,
				money = goodsData.money,
				payType = 1,
				})
			
		end
	end
end


function POP.clean(  )

	if dzBuyChipWidget then
		dzBuyChipWidget:removeFromParent()
		dzBuyChipWidget = nil
	end

	contentBg = nil

	selectedLayout = nil

	curData = nil

	playerInfo = nil

	minMoney = nil

	maxMoney = nil

	seatPos = nil

	zc.removeFromActivePops(popName)
end

return POP