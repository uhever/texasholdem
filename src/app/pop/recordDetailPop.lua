--[[
    主页战绩界面
]]

local POP = {}

local popName = "src/app/pop/recordDetailPop"

local recordDetailWidget = nil

local contentBg = nil

local curScroll = nil

local profitAttrLabel = nil

local dateLabel = nil

local curData = nil

function POP.create( params )
    zc.addToActivePops(popName)

    recordDetailWidget = zc.createMyPop(popName, {
        bgType = 0,
        bgSrc = "res/Image/main/main_bg.jpg",
        title = {
            center = {text = zc.lang("recordDetail_RecordDetail", "战绩详情")},
            left = {type = "return"},
        }
        })

    POP.init(params)

    POP.createUI()
end

function POP.init( params )

    curData = params

    -- 重新构建数据
    curData = {}
    curData["day"] = {}
    curData["month"] = {}

    for k,v in pairs(params.day_profit) do
        local dayTb = {}
        dayTb["index"] = tonumber(k)
        dayTb["year"] = math.floor(tonumber(k) / 10000)
        dayTb["month"] = math.floor(tonumber(k) % 10000 / 100)
        dayTb["day"] = math.floor(tonumber(k) % 10000 % 100)
        dayTb["value"] = tonumber(v)

        table.insert(curData["day"], dayTb)
    end

    table.sort(curData["day"], function ( a, b )
        return a.index < b.index
    end)


    for k,v in pairs(params.month_profit) do
        local dayTb = {}
        dayTb["index"] = tonumber(k)
        dayTb["year"] = math.floor(tonumber(k) / 100)
        dayTb["month"] = tonumber(k) % 100
        dayTb["value"] = tonumber(v)

        table.insert(curData["month"], dayTb)
    end

    table.sort(curData["month"], function ( a, b )
        return a.index < b.index
    end)

    -- dump(curData)
    
end


-- 创建战绩
function POP.createUI(  )

    contentBg = zc.createImageView({
        src = "res/Image/record/record_detail_bg.png",
        ap = cc.p(0.5, 1),
        pos = cc.p(zc.width / 2, zc.height - 180),
        parent = recordDetailWidget,
        })

    -- local tabBg = zc.createImageView({
    --     src = "res/Image/record/record_detail_tab_bg.png",
    --     -- ap = cc.p(0.5, 0.5),
    --     pos = cc.p(contentBg:getContentSize().width / 2, contentBg:getContentSize().height + 35),
    --     parent = contentBg,
    --     })

    local tabCfg = {
        {name = "day", text = zc.lang("recordDetail_Day", "日"), selected = true},
        {name = "month", text = zc.lang("recordDetail_Month", "月")},
    }

    local tabTable = {}
    local selectedIndex = 1

    for i,v in ipairs(tabCfg) do
        local tabBar = zc.createImageView({
            tag = i,
            flipX = (i == 2),
            name = v.name,
            src = "res/Image/record/record_detail_tab_pressed.png",
            ap = i == 1 and cc.p(0, 1) or cc.p(1, 1),
            pos = cc.p((i-1) * 360, contentBg:getContentSize().height + 70),
            parent = contentBg,
            scale9 = true,
            size = cc.size(375, 80),
            listener = function ( sender, eventType )
                if eventType == ccui.TouchEventType.ended then
                    local name = sender:getName()
                    local selectedName = tabTable[selectedIndex]:getName()

                    if name == selectedName then
                        return
                    end

                    if name == "day" then
                        POP.createAnalysisScroll( curData["day"], 1 )
                    else
                        POP.createAnalysisScroll( curData["month"], 2 )
                    end

                    -- sender:setOpacity(255)
                    -- tabTable[selectedIndex]:setOpacity(0)
                    sender:loadTexture("res/Image/record/record_detail_tab_pressed.png")
                    tabTable[selectedIndex]:loadTexture("res/Image/record/record_detail_tab.png")
                    selectedIndex = sender:getTag()

                end
            end
            })

        tabBar.text = zc.createLabel({
            text = v.text, 
            pos = cc.p((i-1) * zc.width / 2 + zc.width / 4, contentBg:getContentSize().height + 30),
            parent = contentBg,
            color = cc.c3b(77, 72, 68),
            fontSize = 24,
            })

        if v.selected then
            selectedIndex = i
        else
            -- tabBar:setOpacity(0)
            tabBar:loadTexture("res/Image/record/record_detail_tab.png")
        end

        table.insert(tabTable, tabBar)
    end

    zc.createAttrLabel({
        {text = zc.lang("recordDetail_Insurance", "保险："), fontSize = 24, color = cc.c3b(202, 210, 219)},
        {text = "+" .. 0, fontSize = 24, color = cc.c3b(239, 202, 140)},
        },
        {
        ap = cc.p(0, 0.5),
        pos = cc.p(20, 430),
        parent = contentBg,
        })

    profitAttrLabel = zc.createAttrLabel({
        {text = zc.lang("recordDetail_Earnings", "盈利："), fontSize = 24, color = cc.c3b(202, 210, 219)},
        {text = "+" .. 3072, fontSize = 24, color = cc.c3b(239, 202, 140)},
        },
        {
        ap = cc.p(0.5, 0.5),
        pos = cc.p(380, 430),
        parent = contentBg,
        })

    dateLabel = zc.createLabel({
        text = zc.lang("recordDetail_Date", "3月9日"), 
        pos = cc.p(zc.width - 20, 430), 
        ap = cc.p(1, 0.5),
        parent = contentBg,
        color = cc.c3b(202, 210, 219),
        fontSize = 24,
        })


    local btnCfg = {
        {name = "data", text = zc.lang("recordDetail_DataStatistics", "数据统计")},
        {name = "card", text = zc.lang("recordDetail_Analysis", "牌局分析")},
    }

    for i,v in ipairs(btnCfg) do
        local btnBg = zc.createButton({
            name = v.name,
            normal = "res/Image/record/record_detail_btn_n.png",
            pressed = "res/Image/record/record_detail_btn_p.png",
            pos = cc.p(zc.width / 2 - 185 + (i-1) * 370, zc.height - 700),
            parent = recordDetailWidget,
            listener = POP.menuClickCallback,
            })

        zc.createImageView({
            src = "res/Image/record/record_detail_" .. v.name .. "_img.png",
            pos = cc.p(btnBg:getContentSize().width / 2 - 50, btnBg:getContentSize().height / 2),
            parent = btnBg,
            })

        zc.createLabel({
            text = v.text, 
            pos = cc.p(btnBg:getContentSize().width / 2 + 40, btnBg:getContentSize().height / 2),
            parent = btnBg,
            color = cc.c3b(202, 210, 219),
            fontSize = 26,
            })
    end


    POP.createAnalysisScroll( curData["day"], 1 )
    
end



function POP.menuClickCallback( sender, eventType )

    if eventType == ccui.TouchEventType.ended then
        local name = sender:getName()

        if name == "data" then
            zc.createPop("src/app/pop/recordStatisticsPop")
 
        elseif name == "XXXX" then
            
        end

    end
end



function POP.createAnalysisScroll( profitCfg, mode )
    -- local profitCfg = {}
    -- for i=1,30 do
    --     table.insert(profitCfg, math.random(-3000, 3000))
    -- end

    if curScroll then
        curScroll:removeFromParent()
    end

    local upMax = 0
    local downMax = 0

    for i,v in ipairs(profitCfg) do
        if v.value > upMax then
            upMax = v.value
        end

        if v.value < downMax then
            downMax = v.value
        end

    end

    local maxNum = upMax > math.abs(downMax) and upMax or math.abs(downMax)

    local maxHeight = 160 --px

    local padding = 80

    -- 指示器位置
    local indicatorPos = zc.width / 2

    local function calcCellHeight( num )
        local cellHeight = 0

        if num ~= 0 then
            cellHeight = math.abs(num * maxHeight / maxNum)
        end
        return cellHeight
    end

    
    local function updateAnalysisDesc( sender, eventType )
        local posX = sender:getInnerContainerPosition().x

        local num = math.floor((indicatorPos + -posX) / padding + 1)

        local profitTb = profitCfg[num]
        if not profitTb then
            return
        end

        zc.updateAttrLabel(profitAttrLabel, {
            {},
            {text = profitTb.value}
            })

        if mode == 1 then
            dateLabel:setString(profitTb.month .. zc.lang("recordDetail_Month", "月") .. profitTb.day .. zc.lang("recordDetail_Day", "日"))    
        else
            dateLabel:setString(profitTb.year .. zc.lang("recordDetail_Year", "年") .. profitTb.month .. zc.lang("recordDetail_Month", "月"))
        end

        -- 结束运动
        if eventType == ccui.ScrollviewEventType.autoscrollEnded then
            local calcPosX = -(num - 5) * padding
            print("posX", posX)
            print("calcPosX", calcPosX)

            sender:runAction(cc.Sequence:create(cc.DelayTime:create(0), cc.CallFunc:create(function (  )
                sender:setInnerContainerPosition(cc.p(calcPosX, 0))
            end)))

        end

    end


    local analysisScroll = zc.createScrollView({
        -- mask = true,
        inertiaScroll = true,
        bounce = true,
        size = cc.size(zc.width - 40, 400),
        innerSize = cc.size(padding * #profitCfg - 15, 400),
        pos = cc.p(15, 0),
        parent = contentBg,
        direction = ccui.ScrollViewDir.horizontal,
        })

    curScroll = analysisScroll -- REG

    analysisScroll:setScrollBarEnabled(false)

    analysisScroll:addEventListener(function ( sender, eventType )

        updateAnalysisDesc(sender, eventType)
        
    end)
    

    for i, v in pairs(profitCfg) do

        local cellLayout = zc.createLayout({
            -- mask = true,
            size = cc.size(padding, analysisScroll:getContentSize().height),
            pos = cc.p((i-1) * padding, 0)
            })

        local cellImg = zc.createImageView({
            src = "res/Image/record/record_detail_cell_up.png",
            scale9 = true,
            size = cc.size(16, 27),
            pos = cc.p(padding / 2, cellLayout:getContentSize().height / 2 + 22),
            parent = cellLayout,
            })

        cellImg:setContentSize(16, calcCellHeight(v.value))


        if v.value > 0 then
            cellImg:setAnchorPoint(cc.p(0.5, 0))
        elseif v.value < 0 then
            cellImg:loadTexture("res/Image/record/record_detail_cell_down.png")
            cellImg:setAnchorPoint(cc.p(0.5, 1))
        else
            cellImg:setVisible(false)
        end

        -- 描述Label
        local descLabel = zc.createLabel({
            text = mode == 1 and v.day or v.month, 
            ap = cc.p(0.5, 0), 
            pos = cc.p(padding / 2, 10), 
            parent = cellLayout,
            color = cc.c3b(217, 171, 108),
            fontSize = 26,
            })


        -- -- 描述Label
        -- local descLabel = zc.createLabel({
        --     text = v.value, 
        --     ap = cc.p(0.5, 0.5), 
        --     pos = cc.p(padding / 2, cellLayout:getContentSize().height / 2 + 15), 
        --     parent = cellLayout,
        --     color = cc.c3b(217, 171, 108),
        --     fontSize = 26,
        --     })

        -- cellLayout.descLabel = descLabel

   
        analysisScroll:addChild(cellLayout)

    end

    updateAnalysisDesc(analysisScroll, -1)

    analysisScroll:setInnerContainerPosition(cc.p(-(analysisScroll:getInnerContainerSize().width - 4.5 * padding - analysisScroll:getContentSize().width / 2), 0))

end



function POP.closeCallback( sender, eventType )
    if eventType == ccui.TouchEventType.ended then
        zc.closePop(popName)
    end
end

function POP.clean(  )
    if recordDetailWidget then
        recordDetailWidget:removeFromParent()
        recordDetailWidget = nil
    end

    contentBg = nil

    curScroll = nil

    profitAttrLabel = nil

    dateLabel = nil

    curData = nil

    zc.removeFromActivePops(popName)
end

return POP