--[[
	商城界面
]]

local POP = {}

local popName = "src/app/pop/shopPop"

local shopWidget = nil

local curData = nil


function POP.create( params )
	zc.addToActivePops(popName)
	shopWidget = zc.createMyPop(popName, {
		opacity = 0,
		bgType = 0,
		bgSrc = "res/Image/main/main_bg.jpg",
		title = {
			center = {text = zc.lang("mine_Shop", "商店")},
			left = {type = "return"},
			}
		})
	POP.init(params)
end

function POP.init( params )

	curData = DB_STATICTABLE["proto_vip_charge"]["list"]


	POP.createUI()
end



function POP.createUI()

	local moneyBg = zc.createImageView({
		src = "res/Image/shop/shop_money_bg.png",
		pos = cc.p(zc.width / 2, zc.height - 150),
		parent = shopWidget,
		-- scale9 = true,
		-- size = cc.size(zc.width,100),
		})

	local moneyAttrLabel = zc.createAttrLabel({
		{image = true, src = "res/Image/main/mine_chip_icon.png"},
		{text = zc.getWan(PlayerData.base.money, 4), fontSize = 26, color = cc.c3b(239, 202, 140), offsetX = 10, offsetY = 5},
		},
		{
		ap = cc.p(0.5, 0.5),
		pos = cc.p(zc.width / 2, zc.height - 150),
		parent = shopWidget,
		})

	zc.registFlashNode( {
		node = moneyAttrLabel,
		type = zc.dataTypeMoney,
		callback = function (  )
			zc.updateAttrLabel(moneyAttrLabel, {
				{},
				{text = zc.getWan(PlayerData.base.money, 4), offsetX = 10, offsetY = 5}
				})
		end
		})

	for i=1,3 do
		for j=1,2 do
			local idx = (i - 1) * 2 + j
			local posX = zc.cx - 170 + (j-1) * 340
			local posY = zc.cy + 320 - (i-1) * 350

			local data = curData[tostring(idx)]

			local itemBg = zc.createImageView({
				tag = idx,
				src = "res/Image/game/shop_goods_bg.png",
				pos = cc.p(posX,posY),
				parent = shopWidget,
				listener = POP.buyCallback,
			})
			
			--name
			zc.createLabel({
				text = data.money .. zc.lang("dzBuyChip_Chip", "筹码"),
				color = cc.c3b(202, 207, 215),
				pos = cc.p(itemBg:getContentSize().width/2 ,itemBg:getContentSize().height-35),
				fontSize = 22,
				parent = itemBg,
				})

			--icon
			zc.createImageView({
				src = "res/Image/game/shop_goods_" .. idx .. ".png",
				pos = cc.p(itemBg:getContentSize().width/2 ,itemBg:getContentSize().height-130),
				parent = itemBg,
				})

			zc.createAttrLabel({
				{text = zc.lang("dzBuyChip_Give", "送"), color = cc.c3b(211,171,108), fontSize = 28},
				{image = true, src = "res/Image/main/mine_chip_icon.png", offsetX = 5},
				{text = data.give_money, color = cc.c3b(211,171,108), fontSize = 28, offsetX = 5}
				},
				{
				ap = cc.p(0.5, 0,5),
				pos = cc.p(itemBg:getContentSize().width/2, 80),
				parent = itemBg,
				})

			zc.createButton({
				normal = "res/Image/game/shop_money_btn_n.png",
				pressed = "res/Image/game/shop_money_btn_p.png",
				pos = cc.p(itemBg:getContentSize().width/2 ,40),
				parent = itemBg,
				})
			zc.createLabel({
				text = "￥" .. data.rmb,
				color = cc.c3b(239, 243, 255),
				pos = cc.p(itemBg:getContentSize().width/2 ,40),
				fontSize = 26,
				parent = itemBg,
				})
		end
	end

end


function POP.buyCallback( sender, eventType )
	if eventType == ccui.TouchEventType.began then

	elseif eventType == ccui.TouchEventType.ended then
		print("购买金币", sender:getTag())

		local goodsData = curData[tostring(sender:getTag())]

		if LOGIN_MODE == kLoginModeNormal then
			local function successCallback(data)
				if data.status == 1 then
					
					zc.updateUserData(data)

					zc.tipInfo(zc.lang("dzBuyChip_RechargeSuccess", "成功充值") .. goodsData.money .. zc.lang("dzBuyChip_Chip", "筹码"))

				else 
					zc.getErrorMsg(data.status)
				end
			end

			 zc.loadUrl("m=user_chat", {
			 	type = 1,
			 	msginfo = ".zcmoney " .. goodsData.money
		        },
		        successCallback)
		elseif LOGIN_MODE == kLoginModeLiveSDK then

			zc.createPrepayOrderAndPay({
				type = goodsData.type,
				id  = goodsData.id,
				rmb = goodsData.rmb,
				money = goodsData.money,
				payType = 1,
				})
			
		end
		
	end
end


function POP.closeCallback( sender, eventType )
	if eventType == ccui.TouchEventType.ended then
		zc.closePop(popName)
	end
end


function POP.clean(  )
	if shopWidget then
		shopWidget:removeFromParent()
		shopWidget = nil
	end

	curData = nil

	zc.removeFromActivePops(popName)
end

return POP