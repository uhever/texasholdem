--[[
	修改资料界面
]]

local POP = {}

local popName = "src/app/pop/helpWebPop"

local helpWebWidget = nil


function POP.create( params )
	zc.addToActivePops(popName)
	helpWebWidget = zc.createMyPop(popName, {
		opacity = 0,
		bgType = 0,
		bgSrc = "res/Image/main/main_bg.jpg",
		title = {
			center = {text = zc.lang("helpWeb_Help", "帮助")},
			left = {type = "return"},
		}
		})

	POP.init(params)

	POP.createUI()
end

function POP.init( params )

	
end

function POP.createUI(  )
	local webView = ccexp.WebView:create()
    webView:setPosition(zc.cx, zc.cy - 60)
    webView:setContentSize(zc.width, zc.height - 120)
    webView:loadURL("http://api.sociapoker.com/public/help/index.html")
    -- webView:setScalesPageToFit(true)

    webView:setOnShouldStartLoading(function(sender, url)
        print("onWebViewShouldStartLoading, url is ", url)
        return true
    end)
    webView:setOnDidFinishLoading(function(sender, url)
        print("onWebViewDidFinishLoading, url is ", url)
    end)
    webView:setOnDidFailLoading(function(sender, url)
        print("onWebViewDidFinishLoading, url is ", url)
    end)

    helpWebWidget:addChild(webView)
end



function POP.closeCallback( sender, eventType )
	if eventType == ccui.TouchEventType.ended then
		zc.closePop(popName)
	end
end

function POP.clean(  )
	if helpWebWidget then
		helpWebWidget:removeFromParent()
		helpWebWidget = nil
	end
	zc.removeFromActivePops(popName)
end

return POP