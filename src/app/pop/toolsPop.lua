--[[
	toolsPop -- 选择提示框
]]

--[[
1、纯文本 + 选择按钮
2、纯文本 + 图片 + 选择按钮

]]

--[[
图片用例：
imagesTable = {{backImageUrl = "res/Image/main/platform/light.png" ,imageUrl = "res/Image/main/shop/shop_money_icon_4.png", imageNumber = 1000, fontNumber =32,} }
zc.createPop("tools", {label = "游戏已经开始，退出将会由笨笨的机器人代打~", 
                    rightRichImg = "res/Image/common/icon_gold_no_light.png", -- 默认
                    imagesTable = imagesTable })
纯文字用例：
zc.createPop("tools", {label = "游戏已经开始，退出将会由笨笨的机器人代打~"})

]]

local POP = {}

local popName = "src/app/pop/toolsPop"

local tipWidget = nil

local tipBg = nil

function POP.create(params)
    --查创建widget
    tipWidget = zc.createMyPop(popName, {
        -- mask = true,
        opacity = 0,
        zOrder = zc.Z_ORDER_TIP,
        -- touchClose = true,
        })

    POP.createUI( params )

end


function POP.createUI( params )
    local data = {}
    data.label = params.label or ""
    data.imagesTable = params.imagesTable or nil -- {{imageUrl = "", imageNumber = 10, fontNumber =, },{},{}... } 
    data.leftBtnText = params.leftBtnText or zc.lang("confirm_Cancel", "取消")
    data.rightBtnText = params.rightBtnText or zc.lang("confirm_Sure", "确定")
    data.leftCallBack = params.leftCallBack or POP.closeCallback
    data.rightCallBack = params.rightCallBack or POP.closeCallback
    data.leftRichImg = params.leftRichImg or nil
    data.rightRichImg = params.rightRichImg or nil

    -- 背景图片
    tipBg = zc.createImageView({
        src = "res/Image/main/toolsTip/toolsBg.png", 
        pos = cc.p(zc.cx, zc.cy), 
        parent = tipWidget
        })

     -- 判断说明文字的位置
    local labelPos = cc.p(327, 275)
    local fontSize = 28
    -- 文本位置调整 
    if data.imagesTable then
        labelPos = cc.p(327, 300)
        fontSize = 24
        -- 添加图片,并根据数量排位置
        POP.addImageToBg(data.imagesTable)


    end

    local tipLabel = zc.createLabel({
        text = data.label,
        pos = labelPos,
        fontSize = 28,
        size = CCSize(500, 80),
        color = cc.c3b(255, 242, 181),
        shadow = true,
        align = kCCTextAlignmentLeft
        })
    
    tipBg:addChild(tipLabel)

    -- 添加按钮
    local leftBtn = zc.createButton({
                text = data.leftBtnText,
                normal = "res/Image/common/green_ellipse_btn_n.png", 
                pressed = "res/Image/common/green_ellipse_btn_p.png", 
                pos = cc.p(217, 100), 
                fontSize = 28,
                listener = data.leftCallBack,
                scale = 1.2,
                parent = tipBg,
                shadow = true,
                })

    local rightBtn = zc.createButton({
                text = data.rightBtnText,
                normal = "res/Image/common/purple_btn_n.png", 
                pressed = "res/Image/common/purple_btn_p.png", 
                pos = cc.p(434, 100), 
                fontSize = 28,
                listener = data.rightCallBack,
                scale = 1.2,
                richImg = data.rightRichImg,
                shadow = true,
                parent = tipBg,
                })
    
end

function POP.addImageToBg(params)

    if table.nums(params) == 1 then
        -- 背景图片
        local imageData = params[1]

        if imageData.backImageUrl then
            local img = zc.createImageView({
            src = imageData.backImageUrl, 
            pos = cc.p(327, 225),
            parent = tipBg
            })

        end

        local img = zc.createImageView({
            src = imageData.imageUrl, 
            pos = cc.p(327, 225),
            parent = tipBg
            })

        if imageData.imageNumber then
            local imgLabel = zc.createLabel({
                    text = "x" .. imageData.imageNumber,
                    pos = cc.p(img:getContentSize().width / 2, 10),
                    fontSize = imageData.fontNumber,
                    color = cc.c3b(255, 255, 255),
                    shadow = true,
                    
                })

             img:addChild(imgLabel)
         end


    else -- 其他的情况多图片排列或者列表情况 

    end

end


function POP.closeCallback( sender, eventType )
    if eventType == ccui.TouchEventType.ended then

        zc.closePop(popName)
    end
end

-- POP的清理方法，需要手动处理
function POP.clean()

    if tipWidget then
        tipWidget:removeFromParent()
        tipWidget = nil
    end

end


return POP
