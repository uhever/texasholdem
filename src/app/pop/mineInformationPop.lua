--[[
	修改资料界面
]]

local POP = {}

local popName = "src/app/pop/mineInformationPop"

local mineInformationWidget = nil

local menuCfg = {
	{name = "icon", text = zc.lang("mineInformation_Avatar", "头像")},
	{name = "nickname", text = zc.lang("mineInformation_Nickname", "昵称")},
}

local menuSubCfg = {}

function POP.create( params )
	zc.addToActivePops(popName)
	mineInformationWidget = zc.createMyPop(popName, {
		opacity = 0,
		bgType = 0,
		bgSrc = "res/Image/main/main_bg.jpg",
		title = {
			center = {text = zc.lang("mineInformation_Modify", "修改资料")},
			left = {type = "return"},
		}
		})

	POP.init(params)

	POP.createUI()
end

function POP.init( params )

	
end

function POP.createUI(  )

	local menuListView = zc.createListView({
        inertiaScroll = true,
        -- bounce = true,
        size = cc.size(zc.width, zc.height - 250),
        pos = cc.p(0, 130),
        parent = mineInformationWidget,
        direction = ccui.ScrollViewDir.none,
        })

	menuListView:setScrollBarEnabled(false)

	local cellHeight = 105

    for i, v in pairs(menuCfg) do

    	local padding = v.padding or 10

        local cellLayout = zc.createLayout({
            name = v.name,
            size = cc.size(zc.width, cellHeight + padding),
            listener = POP.quickChatCallback
            })

        local cellBg = zc.createImageView({
			src = "res/Image/common/common_scale9_btn_n.png",
			ap = cc.p(0, 0),
			scale9 = true,
			size = cc.size(zc.width, cellHeight),
			parent = cellLayout,
			})

        -- 描述Label
        local descLabel = zc.createLabel({
            text = v.text, 
            ap = cc.p(0, 0.5), 
            pos = cc.p(30, cellBg:getContentSize().height / 2), 
            parent = cellBg,
            color = cc.c3b(217, 171, 108),
            fontSize = 26,

            })

        -- 自定义
        if v.name == "icon" then
        	
        	-- 头像
			local headBg = zc.createImageView({
				scale = 0.5,
				src = "res/Image/main/mine_head_bg_1.png",
				pos = cc.p(cellBg:getContentSize().width - 60, cellBg:getContentSize().height / 2),
				parent = cellBg,
				listener = function ( sender, eventType )
					if eventType == ccui.TouchEventType.ended then
						zc.sdkFetchImage({type = 1, sourceType = 0, width = 96})
					end
				end
				})

			local headIcon = zc.createHeadIcon( {
				image = PlayerData.base.image,
				shapeType = 1,
				scale = 1.7,
				pos = cc.p(headBg:getContentSize().width / 2, headBg:getContentSize().height / 2),
				parent = headBg,
				} )

			zc.registFlashNode( {
				node = headIcon,
				type = zc.dataTypeHeadImage,
				callback = function (  )
					zc.updateHeadIcon(headIcon, {image = PlayerData.base.image})
				end
				} )
			

		elseif v.name == "nickname" then
			
			local nicknameEdit = zc.createEditBox({
		        -- text = "啊实打实的",
		        bgSrc = "res/Image/main/inform_edit_bg.jpg",
		        size = cc.size(323, 47),
		        pos = cc.p(cellBg:getContentSize().width - 250, cellBg:getContentSize().height / 2),
		        placeHolder = zc.lang("mineInformation_Edit", "这里直接编辑"),
		        fontSize = 26,
		        parent = cellBg
		        })

			zc.createButton({
				normal = "res/Image/main/inform_sure_btn_n.png",
				pressed = "res/Image/main/inform_sure_btn_p.png",
				pos = cc.p(cellBg:getContentSize().width - 50, cellBg:getContentSize().height / 2),
				parent = cellBg,
				listener = function ( sender, eventType )
					if eventType == ccui.TouchEventType.ended then
						local newName = nicknameEdit:getText()

						if newName == "" then
							zc.tipInfo(zc.lang("mineInformation_Tip", "请输入合法的用户名"))
							return
						end

						local successCallback = function(data)
		                    if(tonumber(data.status) == 1) then
		                        zc.tipInfo(zc.lang("mineInformation_ModifySuccess", "用户名修改成功"))
		                        zc.updateUserData(data)
		                    else
		                        zc.getErrorMsg(data.status)
		                    end
		                end
		                zc.loadUrl("m=user_rename", {
		                	name = newName,
		                    }, successCallback)


					end
				end
				})
        end

   
        menuListView:pushBackCustomItem(cellLayout)

    end
end





function POP.closeCallback( sender, eventType )
	if eventType == ccui.TouchEventType.ended then
		zc.closePop(popName)
	end
end

function POP.clean(  )
	if mineInformationWidget then
		mineInformationWidget:removeFromParent()
		mineInformationWidget = nil
	end
	zc.removeFromActivePops(popName)
end

return POP