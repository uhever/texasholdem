--[[
    主页战绩界面
]]

local POP = {}

local popName = "src/app/pop/recordPop"

local recordWidget = nil

local curData = nil

function POP.create( params )
    zc.addToActivePops(popName)

    recordWidget = zc.createMyPop(popName, {
        opacity = 0,
        bgType = 0,
        bgSrc = "res/Image/main/main_bg.jpg",
        title = {
            center = {text = zc.lang("record_Record", "战绩")},
        },
        bottom = {
            index = 1,
        }
        })

    POP.init(params)

    POP.createUI()
end

function POP.init( params )

    curData = params
    
end

function POP.createUI(  )

    local menuListView = zc.createListView({
        inertiaScroll = true,
        -- bounce = true,
        size = cc.size(zc.width, zc.height - 240),
        pos = cc.p(0, 125),
        parent = recordWidget,
        direction = ccui.ScrollViewDir.none,
        })

    menuListView:setScrollBarEnabled(false)


    menuListView:pushBackCustomItem(POP.createNormalRecord())
    menuListView:pushBackCustomItem(POP.createProfileRecord())


end

-- 创建战绩
function POP.createNormalRecord(  )
    local cellLayout = zc.createLayout({
        size = cc.size(zc.width, 456),
        })


    local cellBg = zc.createImageView({
        name = "recordDetail",
        src = "res/Image/main/record_bg.png",
        ap = cc.p(0, 0),
        pos = cc.p(5, 0),
        parent = cellLayout,
        listener = POP.menuClickCallback,
        })

    zc.createLabel({
        text = zc.lang("record_OrdinaryRecord", "普通战绩"), 
        pos = cc.p(cellBg:getContentSize().width / 2, cellBg:getContentSize().height - 40), 
        parent = cellBg,
        -- color = cc.c3b(217, 171, 108),
        fontSize = 32,
        })

    zc.createLabel({
        text = curData.profit, 
        pos = cc.p(cellBg:getContentSize().width - 20, cellBg:getContentSize().height - 40), 
        ap = cc.p(1, 0.5),
        parent = cellBg,
        color = cc.c3b(255, 88, 88),
        fontSize = 32,
        })

    zc.createLabel({
        text = zc.lang("record_Pool", "入池率"), 
        pos = cc.p(cellBg:getContentSize().width / 2, 230), 
        parent = cellBg,
        -- color = cc.c3b(217, 171, 108),
        fontSize = 32,
        })

    zc.createLabel({
        text = curData.pot_rate .. "%", 
        pos = cc.p(cellBg:getContentSize().width / 2, 180), 
        parent = cellBg,
        color = cc.c3b(255, 252, 182),
        fontSize = 32,
        })

    zc.createLabel({
        text = zc.lang("record_Winrate", "其中胜率"), 
        pos = cc.p(cellBg:getContentSize().width / 2, 80), 
        parent = cellBg,
        -- color = cc.c3b(217, 171, 108),
        fontSize = 32,
        })

    zc.createLabel({
        text = curData.pot_win_rate .. "%", 
        pos = cc.p(cellBg:getContentSize().width / 2, 40), 
        parent = cellBg,
        color = cc.c3b(255, 185, 128),
        fontSize = 32,
        })


    zc.createLabel({
        text = zc.lang("record_PlayNum", "总手数"), 
        pos = cc.p(cellBg:getContentSize().width - 20, 65), 
        ap = cc.p(1, 0.5),
        parent = cellBg,
        -- color = cc.c3b(217, 171, 108),
        fontSize = 26,
        })

    zc.createLabel({
        text = curData.total_match, 
        pos = cc.p(cellBg:getContentSize().width - 20, 25), 
        ap = cc.p(1, 0.5),
        parent = cellBg,
        color = cc.c3b(217, 171, 108),
        fontSize = 26,
        })

    --底部
    local progressBorder = zc.createImageView({
        src = "res/Image/main/record_progress_border.png",
        pos = cc.p(cellBg:getContentSize().width / 2, 180),
        parent = cellBg,
        })


    local finalPercent = (100 - 9.05*2) * curData.pot_rate / 100 + 9.05

    -- 入池率
    zc.createProgressTimer({
        src = "res/Image/main/record_progress_pool.png",
        type = cc.PROGRESS_TIMER_TYPE_RADIAL,
        percent = finalPercent,
        pos = cc.p(progressBorder:getContentSize().width / 2, progressBorder:getContentSize().height / 2),
        parent = progressBorder,
        rotate = 180,
        })

    local finalPercent = (100 - 9.05*2) * curData.pot_win_rate / 100 + 9.05

    -- 胜率
    zc.createProgressTimer({
        src = "res/Image/main/record_progress_success.png",
        type = cc.PROGRESS_TIMER_TYPE_RADIAL,
        percent = finalPercent,
        pos = cc.p(progressBorder:getContentSize().width / 2, progressBorder:getContentSize().height / 2),
        parent = progressBorder,
        rotate = 180,
        })


    return cellLayout
    
end


-- 创建进行中的牌局
function POP.createProfileRecord(  )
    local cellLayout = zc.createLayout({
        size = cc.size(zc.width, 200),
        })

    local cellBg = zc.createButton({
        name = "profileRecord",
        normal = "res/Image/common/common_scale9_btn_n.png",
        pressed = "res/Image/common/common_scale9_btn_p.png",
        ap = cc.p(0, 0),
        pos = cc.p(5, 15),
        scale9 = true,
        size = cc.size(740, 180),
        parent = cellLayout,
        -- shaderObj = 3,
        listener = POP.menuClickCallback,
        })

    -- icon
    local cellIcon = zc.createImageView({
        src = "res/Image/main/record_profileRecord_icon.png",
        pos = cc.p(100, cellBg:getContentSize().height / 2), 
        parent = cellBg,
        })

    -- 描述Label
    local mainLabel = zc.createLabel({
        text = zc.lang("record_CordRecord", "牌谱记录"), 
        pos = cc.p(510, 110), 
        parent = cellBg,
        color = cc.c3b(112, 141, 141),
        fontSize = 36,
        })

    local descLabel = zc.createLabel({
        text = zc.lang("record_CordRecordText", "收录最近19天的手牌"), 
        pos = cc.p(510, 60), 
        parent = cellBg,
        color = cc.c3b(88, 110, 110),
        fontSize = 24,
        })

    return cellLayout
end

function POP.menuClickCallback( sender, eventType )

    if eventType == ccui.TouchEventType.ended then
        local name = sender:getName()

        if name == "profileRecord" then
            local function successCallback(data)
                if data.status == 1 then
                    local curData = {}
                    --重组数据
                    table.sort( data.game_info, function ( a,b )
                        return a.time > b.time
                    end )

                    if data.game_info[1] then
                        table.insert(curData,{newday = zc.timeToDate(data.game_info[1].time, 2) })
                    end

                    for i,v in ipairs(data.game_info) do
                        table.insert(curData,v)
                        if data.game_info[i] and data.game_info[i+1] then
                            if  zc.timeToDate(data.game_info[i].time, 2) ~= zc.timeToDate(data.game_info[i+1].time, 2) then
                                table.insert(curData,{newday = zc.timeToDate(data.game_info[i+1].time, 2) })
                            end
                        end
                    end

                    zc.createPop("src/app/pop/mineGameInfoPop",{data = curData})
                else
                    zc.getErrorMsg(data.status)
                end
            end
              
            zc.loadUrl("m=texas_holdem_game_info", {
              }, successCallback)
        elseif name == "recordDetail" then -- 战绩详情
            local function successCallback(data)
                if data.status == 1 then
                    zc.createPop("src/app/pop/recordDetailPop",data)
                else
                    zc.getErrorMsg(data.status)
                end
            end
              
            zc.loadUrl("m=texas_holdem_period_profit", {
              }, successCallback)

        end

    end
end




function POP.closeCallback( sender, eventType )
    if eventType == ccui.TouchEventType.ended then
        zc.closePop(popName)
    end
end

function POP.clean(  )
    if recordWidget then
        recordWidget:removeFromParent()
        recordWidget = nil
    end

    curData = nil

    zc.removeFromActivePops(popName)
end

return POP