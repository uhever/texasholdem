--[[
	个人设置界面
]]

local POP = {}

local popName = "src/app/pop/mineSettingPop"

local mineSettingWidget = nil

local menuCfg = {
	{name = "effect", text = zc.lang("mineSetting_Effect", "游戏音效")},
	{name = "vibrate", text = zc.lang("mineSetting_Vibrate", "游戏震动")},
	{name = "version", text = zc.lang("mineSetting_Version", "当前版本")},
	{name = "logout", text = zc.lang("mineSetting_Logout", "退出登录"), padding = 40, shader = true},
}

local menuSubCfg = {}

function POP.create( params )
	zc.addToActivePops(popName)
	mineSettingWidget = zc.createMyPop(popName, {
		opacity = 0,
		bgType = 0,
		bgSrc = "res/Image/main/main_bg.jpg",
		title = {
			center = {text = zc.lang("mine_Setting", "设置")},
			left = {type = "return"},
		}
		})

	POP.init(params)

	POP.createUI()
end

function POP.init( params )

	
end

function POP.createUI(  )

	local menuListView = zc.createListView({
        inertiaScroll = true,
        -- bounce = true,
        size = cc.size(zc.width, zc.height - 250),
        pos = cc.p(0, 130),
        parent = mineSettingWidget,
        direction = ccui.ScrollViewDir.none,
        })

	menuListView:setScrollBarEnabled(false)

	local cellHeight = 105

    for i, v in pairs(menuCfg) do

    	local padding = v.padding or 10

        local cellLayout = zc.createLayout({
            size = cc.size(zc.width, cellHeight + padding),
            })

        local cellBg = nil

        if v.name == "logout" then
        	cellBg = zc.createButton({
        		name = v.name,
        		normal = "res/Image/common/common_scale9_btn_n.png",
        		pressed = "res/Image/common/common_scale9_btn_p.png",
        		scale9 = true,
				size = cc.size(zc.width, 110),
        		ap = cc.p(0, 0),
        		parent = cellLayout,
        		listener = POP.menuClickCallback,
        		})
        else
        	cellBg = zc.createImageView({
	        	name = v.name,
				src = "res/Image/common/common_scale9_btn_n.png",
				ap = cc.p(0, 0),
				scale9 = true,
				size = cc.size(zc.width, cellHeight),
				parent = cellLayout,
				shaderObj = v.shader and 3,
				listener = POP.menuClickCallback,
				})
        end


        

        -- 描述Label
        local descLabel = zc.createLabel({
            text = v.text, 
            ap = cc.p(0, 0.5), 
            pos = cc.p(30, cellBg:getContentSize().height / 2), 
            parent = cellBg,
            color = cc.c3b(217, 171, 108),
            fontSize = 26,
            -- shadow = true,
            -- outline = true,
            })

        -- 自定义
        if v.name == "effect" or v.name == "vibrate" then
        	local switchControl = POP.createSwitchControl(v.name)
        	switchControl:setPosition(cellBg:getContentSize().width - 80, cellBg:getContentSize().height / 2)
        	cellBg:addChild(switchControl)

		elseif v.name == "version" then
			
	        local descLabel = zc.createLabel({
	            text = VERSION_CACHE_ID, 
	            ap = cc.p(1, 0.5), 
	            pos = cc.p(cellBg:getContentSize().width - 30, cellBg:getContentSize().height / 2), 
	            parent = cellBg,
	            color = cc.c3b(217, 171, 108),
	            fontSize = 28,
	            })
        elseif v.name == "logout" then
        	descLabel:setAnchorPoint(cc.p(0.5, 0.5))
        	descLabel:setPositionX(cellBg:getContentSize().width / 2)
        end

   
        menuListView:pushBackCustomItem(cellLayout)

    end
end



function POP.menuClickCallback( sender, eventType )

	if eventType == ccui.TouchEventType.ended then
		local name = sender:getName()

		if name == "edit" then
			
		elseif name == "logout" then
			local function successCallback(data)
				if data.status == 1 then
					
					zc.logout()

				else 
					zc.getErrorMsg(data.status)
				end
			end

			 zc.loadUrl("m=user_exit", {
				        },
				        successCallback)
		end

	end
end



function POP.createSwitchControl( name )
	local switchThumb = cc.Sprite:create("res/Image/main/switch_thumb_open.png")
	local openLabel = cc.Label:createWithSystemFont("开", "Arial-BoldMT", 26)
	local closeLabel = cc.Label:createWithSystemFont("关", "Arial-BoldMT", 26)
	
    local function valueChanged(pSender)

    	if pSender:isOn() then
			closeLabel:setTextColor(cc.c4b(100, 100, 100, 255))
			openLabel:setTextColor(cc.c4b(217, 171, 108, 255))
    		switchThumb:setTexture("res/Image/main/switch_thumb_off.png")

    		if name == "effect" then
	    		zc.setMusicStatus(true)
	    		zc.setAudioStatus(true)
	    	elseif name == "vibrate" then
	    		zc.setPhoneShakeStatus(true)
	    	end
    	else
    		closeLabel:setTextColor(cc.c4b(100, 100, 100, 255))
			openLabel:setTextColor(cc.c4b(217, 171, 108, 255))
    		switchThumb:setTexture("res/Image/main/switch_thumb_open.png")

    		if name == "effect" then
	    		zc.setMusicStatus(false)
	    		zc.setAudioStatus(false)
	    	elseif name == "vibrate" then
	    		zc.setPhoneShakeStatus(false)
	    	end
    	end
    	

    end
    local pSwitchControl = cc.ControlSwitch:create(

            cc.Sprite:create("res/Image/main/switch_mask.png"),
            cc.Sprite:create("res/Image/main/switch_on.png"),
            cc.Sprite:create("res/Image/main/switch_off.png"),
            switchThumb,
            openLabel,
			closeLabel
        )
    pSwitchControl:registerControlEventHandler(valueChanged, cc.CONTROL_EVENTTYPE_VALUE_CHANGED)

    -- 逻辑
	if name == "effect" then
		pSwitchControl:setOn(zc.bgMusicOpen)
	elseif name == "vibrate" then
		pSwitchControl:setOn(zc.phoneShakeOpen)
	end


    return pSwitchControl
end


	




function POP.closeCallback( sender, eventType )
	if eventType == ccui.TouchEventType.ended then
		zc.closePop(popName)
	end
end

function POP.clean(  )
	if mineSettingWidget then
		mineSettingWidget:removeFromParent()
		mineSettingWidget = nil
	end
	zc.removeFromActivePops(popName)
end

return POP