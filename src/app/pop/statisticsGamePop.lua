--[[
	实时战绩界面
]]

local POP = {}

local popName = "src/app/pop/statisticsGamePop"

local statisticsGameWidget = nil

local SEAT_NUM_LIMIT = 6
-- local giveData = nil
function POP.create( params )

	statisticsGameWidget = zc.createMyPop(popName, {
		touchClose = true,
		mask = true,
		opacity = 0,
		})
	POP.init(params)
end

function POP.init( params )
	POP.createUI(params)
end

function POP.createUI(params)

	local data = params.data

	local bgImg = zc.createImageView({
		src = "res/Image/game/lastGame_bg.jpg",
		pos = cc.p(0, zc.cy),
		ap = cc.p(0,0.5),
		parent = statisticsGameWidget,
		})

	zc.createLabel({
		text = zc.lang("statisticsGame_RealRecord", "实时战绩"),
		fontSize = 30,
		color = cc.c3b(255, 178, 103),
		pos = cc.p(110, bgImg:getContentSize().height - 70),
		parent = bgImg,
		})

	local retuenArrow = zc.createImageView({
		src = "res/Image/main/common_arrow_left.png",
		pos = cc.p(30, bgImg:getContentSize().height - 70),
		parent = bgImg,
		listener = POP.closeCallback,
		})


	local nameTab = {zc.lang("statisticsGame_Player", "玩家"), zc.lang("statisticsGame_Buy", "买入"), zc.lang("recordStatistics_Earnings", "盈利")}
	local posTab = {
		cc.p(100,bgImg:getContentSize().height -120),
		cc.p(320,bgImg:getContentSize().height -120),
		cc.p(480,bgImg:getContentSize().height -120)
	}
	for i=1,3 do
		zc.createLabel({
			text = nameTab[i],
			fontSize = 24,
			color = cc.c3b(197, 200, 208),
			pos = posTab[i],
			parent = bgImg,
			})
	end

	--玩家信息
	local playerList = zc.createListView({
		-- mask = true,
		inertiaScroll = true,
		bounce = true,
		size = cc.size(564,bgImg:getContentSize().height-150),
		ap = cc.p(0,0),
		pos = cc.p(0, 10),
		parent = bgImg,
		scrollBarDisEnabled = true,
		})

	--排序

	for i,v in ipairs(data) do
		local cellLayout = zc.createLayout({
			size = cc.size(564, 68),
			})

		--底板
		local cellbg = zc.createImageView({
			src = "res/Image/game/statics_other_bg.jpg",
			pos = cc.p(564/2, 34),
			scale9 = true,
			size = cc.size(564,65),
			parent = cellLayout,
			})

		if zc.isSelf(v.roleid) then
			cellbg:loadTexture("res/Image/game/statics_self_bg.jpg")
		end
		--头像
		zc.createHeadIcon({
			image = v.image,
			pos = cc.p(50, 34),
			border = true,
			parent = cellLayout,
			scale = 0.6,
			shapeType = 1
			})

		local nameLabel = zc.createLabel({
			text = zc.subName(v.nickname),
			color = cc.c3b(135, 138, 147),
			pos = cc.p(110, 34),
			ap = cc.p(0, 0.5),
			fontSize = 20,
			parent = cellLayout,
			})

		--输赢
		local buyIn = zc.createLabel({
			text = zc.getWan(v.buy_in,4),
			fontSize = 20,
			color = cc.c3b(135, 138, 147),
			pos = cc.p(320, 34),
			parent = cellLayout,
			})


		local profit = zc.createLabel({
			text = "0",
			fontSize = 24,
			color = cc.c3b(128, 166, 193),
			pos = cc.p(480, 34),
			parent = cellLayout,
			})

		if v.profit >= 0 then
			profit:setColor(cc.c3b(255,178,103))
			profit:setString("+" .. zc.getWan(v.profit,4))
		else
			profit:setColor(cc.c3b(128, 166, 193))
			profit:setString("-" .. zc.getWan(math.abs(v.profit),4))
		end

		--自己label变金色
		if zc.isSelf(v.roleid) then
			nameLabel:setColor(cc.c3b(255,219,143))
			buyIn:setColor(cc.c3b(255,219,143))
		end

		playerList:pushBackCustomItem(cellLayout)
	end

end

function POP.closeCallback( sender, eventType )
	if eventType == ccui.TouchEventType.ended then
		zc.closePop(popName)
	end
end

function POP.clean(  )
	if statisticsGameWidget then
		statisticsGameWidget:removeFromParent()
		statisticsGameWidget = nil
	end

end

return POP