--[[
	注册账号
]]

local POP = {}

local popName = "src/app/pop/registAccountPop"

local registAccountWidget = nil

local usernameEdit = nil

local passwordEdit = nil

function POP.create( params )
	zc.addToActivePops(popName)
	registAccountWidget = zc.createMyPop(popName, {
		touchClose = true,
		})

	POP.init(params)

	POP.createUI()
end

function POP.init( params )

	
end

function POP.createUI(  )
	local mainBg = zc.createImageView( {
        src = "res/Image/confirm/confirm_bg.png",
        pos = cc.p(zc.cx, zc.cy),
        parent = registAccountWidget
        })
	mainBg:setTouchEnabled(true)

	zc.elasticInAction(mainBg)

	zc.createLabel({
        text = zc.lang("registAccount_Register", "注册账号"), 
        color = cc.c3b(239, 202, 140),
        pos = cc.p(mainBg:getContentSize().width / 2, mainBg:getContentSize().height - 40), 
        fontSize = 26,
        parent = mainBg
        })

	usernameEdit = zc.createEditBox({
        bgSrc = "res/Image/login/login_input_img.png",
        size = cc.size(350, 70),
        pos = cc.p(mainBg:getContentSize().width / 2, 280),
        placeHolder = zc.lang("registAccount_InputUserName", "输入用户名"),
        fontSize = 32,
        parent = mainBg
        })

    passwordEdit = zc.createEditBox({
        bgSrc = "res/Image/login/login_input_img.png",
        size = cc.size(350, 70),
        pos = cc.p(mainBg:getContentSize().width / 2, 180),
        placeHolder = zc.lang("registAccount_InputPassword", "输入密码"),
        fontSize = 32,
        inputFlag = cc.EDITBOX_INPUT_FLAG_PASSWORD,
        parent = mainBg
        })


    local registBtn = zc.createButton({
        text = zc.lang("loginScene_Register", "注册"),
        -- fontColor = cc.c3b(220, 199, 159),
        normal = "res/Image/login/login_small_btn_n.png",
        pressed = "res/Image/login/login_small_btn_p.png",
        scale9 = true,
        size = cc.size(200, 83),
        pos = cc.p(mainBg:getContentSize().width / 2, 50),
        parent = mainBg,
        listener = function ( sender, eventType )
            if eventType == ccui.TouchEventType.ended then
                
	            POP.confirmRegisiter()

            end
        end
        })
end



-- 真正的注册
function POP.confirmRegisiter()

	local regUsername = usernameEdit:getText()
	local regPassword = passwordEdit:getText()

	local regMobile = ""
    
    if(regUsername == "") then
        zc.tipInfo(zc.lang("registAccount_InputAccountNum", "请输入账号！"))
        return
    end
    -- if(regPassword == "" or regPassword2 == "") then
    if(regPassword == "") then
        zc.tipInfo(zc.lang("registAccount_InputPasswordPlease", "请输入密码！"))
        return
    end
    -- if(regPassword ~= regPassword2) then
    --     zc.tipInfo("两次密码输入不一致，请重新输入！")
    --     return
    -- end

    local successCallback = function(data)
        local resultStatus = tonumber(data.status)
        if(resultStatus == 1) then

        	zc.tipInfo(zc.lang("registAccount_RegisterSuccess", "注册成功！"))

            -- serverListData = data

            cc.UserDefault:getInstance():setStringForKey("loginUsername", regUsername)
            cc.UserDefault:getInstance():setStringForKey("loginPassword", regPassword)

        elseif(resultStatus == -1) then
            zc.tipInfo(zc.lang("registAccount_RegisterResult1", "账号格式不正确！（长度限制为5-16位字母或者数字）"))

        elseif(resultStatus == -2) then
            zc.tipInfo(zc.lang("registAccount_RegisterResult2", "密码格式不正确！（长度限制为6-16位字母或数字）"))

        elseif(resultStatus == -3) then
            zc.tipInfo(zc.lang("registAccount_RegisterResult3", "账号已存在！"))

        elseif(resultStatus == -4) then
            zc.tipInfo(zc.lang("registAccount_RegisterResult4", "请您将账号密码输入完整！"))

        elseif(resultStatus == -5) then
            zc.tipInfo(zc.lang("registAccount_RegisterResult5", "手机号码格式错误，请重新输入！"))

        else
            zc.tipInfo(zc.lang("registAccount_RegisterError", "注册失败，请稍后重试！"))
        end

    end


    zc.http(UCAPI .. "c=register&m=reg", {
            u = regUsername,
            p = regPassword,
            mobile = regMobile,
            channel = CHANNELID,
            mac = zc.device.mac,
            idfa = zc.device.idfa,
            android_id = zc.device.android_id,
            v = VERSION_NAME
        },
        successCallback
    )
end



function POP.closeCallback( sender, eventType )
	if eventType == ccui.TouchEventType.ended then
		zc.closePop(popName)
	end
end

function POP.clean(  )
	if registAccountWidget then
		registAccountWidget:removeFromParent()
		registAccountWidget = nil
	end

	usernameEdit = nil

	passwordEdit = nil

	zc.removeFromActivePops(popName)
end

return POP