--[[
	个人界面
]]

local POP = {}

local popName = "src/app/pop/minePop"

local mineWidget = nil

local menuCfg = {
	{name = "shop", text = zc.lang("mine_Shop", "商店")},
	{name = "setting", text = zc.lang("mine_Setting", "设置")},
	{name = "profile", text = zc.lang("mine_Profile", "牌谱收藏"), padding = 20},
	{name = "service", text = zc.lang("mine_Service", "客服")},
	-- {name = "gift", text = "奖励", padding = 20},
	{name = "help", text = zc.lang("mine_Help", "帮助"), padding = 20},
	{name = "share", text = zc.lang("mine_Share", "分享")},
}

function POP.create( params )
	zc.addToActivePops(popName)
	mineWidget = zc.createMyPop(popName, {
		opacity = 0,
		bgType = 0,
		bgSrc = "res/Image/main/main_bg.jpg",
		title = {
			center = {text = zc.lang("mine_Individual", "个人")},
		},
		bottom = {
			index = 3,
		}
		})
	POP.init(params)

	POP.createUI()
end

function POP.init( params )

	
end

function POP.createUI(  )
	POP.topUI()

	POP.bottomUI(  )
end


function POP.topUI(  )
	local topBg = zc.createImageView({
		src = "res/Image/main/mine_top_bg.jpg",
		ap = cc.p(0.5, 1),
		pos = cc.p(zc.cx, zc.height - 110),
		parent = mineWidget,
		})

	-- Edit
	zc.createButton({
		name = "edit",
		normal = "res/Image/main/mine_edit_img.png",
		pressed = "res/Image/main/mine_edit_img_pressed.png",
		pos = cc.p(topBg:getContentSize().width - 40, 330),
		parent = topBg,
		listener = POP.menuClickCallback
		})

	-- 头像
	local headBg = zc.createImageView({
		src = "res/Image/main/mine_head_bg_1.png",
		pos = cc.p(topBg:getContentSize().width / 2, 260),
		parent = topBg,
		})


	local headIcon = zc.createHeadIcon( {
		image = PlayerData.base.image,
		shapeType = 1,
		scale = 1.7,
		pos = cc.p(headBg:getContentSize().width / 2, headBg:getContentSize().height / 2),
		parent = headBg,
		} )

	zc.registFlashNode( {
		node = headIcon,
		type = zc.dataTypeHeadImage,
		callback = function (  )
			zc.updateHeadIcon(headIcon, {image = PlayerData.base.image})
		end
		} )

	-- local genderIcon = zc.createImageView({
	-- 	src = "res/Image/main/mine_gender_2.png",
	-- 	pos = cc.p(150, 30),
	-- 	parent = headBg,
	-- 	})

	-- 名字
	local nameLabel = zc.createLabel({
            text = PlayerData.base.nickname, 
            pos = cc.p(topBg:getContentSize().width / 2, 150), 
            parent = topBg,
            -- color = cc.c3b(217, 171, 108),
            fontSize = 26,
            })

	zc.registFlashNode( {
		node = nameLabel,
		type = zc.dataTypeNickName,
		callback = function (  )
			nameLabel:setString(PlayerData.base.nickname)
		end
		} )

	zc.createLabel({
            text = "ID:" .. PlayerData.base.uid, 
            pos = cc.p(topBg:getContentSize().width / 2, 110), 
            parent = topBg,
            color = cc.c3b(202, 210, 219),
            fontSize = 20,
            })

	zc.createLabel({
            text = zc.lang("mine_TimeMoney", "时间就是金钱，我的朋友！"), 
            pos = cc.p(topBg:getContentSize().width / 2, 80), 
            parent = topBg,
            color = cc.c3b(202, 210, 219),
            fontSize = 20,
            })

	local moneyAttrLabel = zc.createAttrLabel({
		{image = true, src = "res/Image/main/mine_chip_icon.png"},
		{text = zc.getWan(PlayerData.base.money, 4), fontSize = 26, color = cc.c3b(239, 202, 140), offsetX = 10, offsetY = 5},
		},
		{
		ap = cc.p(0.5, 0.5),
		pos = cc.p(topBg:getContentSize().width / 2, 25),
		parent = topBg,
		})

	zc.registFlashNode( {
		node = moneyAttrLabel,
		type = zc.dataTypeMoney,
		callback = function (  )
			zc.updateAttrLabel(moneyAttrLabel, {
				{},
				{text = zc.getWan(PlayerData.base.money, 4), offsetX = 10, offsetY = 5}
				})
		end
		})
end

function POP.bottomUI(  )
	local menuListView = zc.createListView({
        inertiaScroll = true,
        -- bounce = true,
        size = cc.size(zc.width, 700),
        pos = cc.p(0, 140),
        parent = mineWidget,
        direction = ccui.ScrollViewDir.none,
        })

	menuListView:setScrollBarEnabled(false)

	local cellHeight = 95

    for i, v in pairs(menuCfg) do

    	local padding = v.padding or 10

        local cellLayout = zc.createLayout({
            size = cc.size(zc.width, cellHeight + padding),
            })

        local cellBg = zc.createButton({
        	name = v.name,
			normal = "res/Image/common/common_scale9_btn_n.png",
        	pressed = "res/Image/common/common_scale9_btn_p.png",
			ap = cc.p(0, 0),
			scale9 = true,
			size = cc.size(zc.width, 110),
			parent = cellLayout,
			-- shaderObj = 3,
			listener = POP.menuClickCallback
			})

        -- icon
        local cellIcon = zc.createImageView({
			src = "res/Image/main/mine_" .. v.name .. "_icon.png",
			pos = cc.p(50, cellBg:getContentSize().height / 2), 
			parent = cellBg,
			})

        -- 描述Label
        local descLabel = zc.createLabel({
            text = v.text, 
            ap = cc.p(0, 0.5), 
            pos = cc.p(115, cellBg:getContentSize().height / 2), 
            parent = cellBg,
            color = cc.c3b(162, 162, 162),
            fontSize = 26,

            -- shadow = true,
            -- outline = true,
            })

        zc.createImageView({
			src = "res/Image/main/common_arrow_right_person.png",
			pos = cc.p(zc.width - 30, cellBg:getContentSize().height / 2), 
			parent = cellBg,
			})
   
        menuListView:pushBackCustomItem(cellLayout)

    end
end

function POP.menuClickCallback( sender, eventType )

	if eventType == ccui.TouchEventType.ended then
		local name = sender:getName()

		if name == "edit" then
			zc.createPop("src/app/pop/mineInformationPop")
		elseif name == "setting" then
			zc.createPop("src/app/pop/mineSettingPop")
		elseif name == "shop" then
			zc.createPop("src/app/pop/shopPop")
		elseif name == "profile" then
            local function successCallback(data)
                if data.status == 1 then
                    local curData = {}
                    --重组数据
                    table.sort( data.game_info, function ( a,b )
                        return a.time > b.time
                    end )

                    if data.game_info[1] then
                        table.insert(curData,{newday = zc.timeToDate(data.game_info[1].time, 2) })
                    end

                    for i,v in ipairs(data.game_info) do
                        table.insert(curData,v)
                        if data.game_info[i] and data.game_info[i+1] then
                            if  zc.timeToDate(data.game_info[i].time, 2) ~= zc.timeToDate(data.game_info[i+1].time, 2) then
                                table.insert(curData,{newday = zc.timeToDate(data.game_info[i+1].time, 2) })
                            end
                        end
                    end

                    zc.createPop("src/app/pop/mineGameInfoPop",{data = curData})
                else
                    zc.getErrorMsg(data.status)
                end
            end
              
            zc.loadUrl("m=texas_holdem_save_game_info", {
              }, successCallback)

        elseif name == "service" then
        	zc.showServiceCenter(  )

        elseif name == "share" then

        	zc.doSdkShare( {
				contentType = "link",
				wxScene = 0, -- 0聊天 1朋友圈
				contentURL = "http://fir.im/9v6p",
				contentTitle = zc.lang("mine_ContentTitle", "【鲸鱼德州】最火爆的德州扑克, 快来加入吧"),
				contentDescription = zc.lang("mine_ContentDescription", "快来体验德州扑克，一起high翻天，IOS&Android首发"),
				} )

        elseif name == "help" then
        	zc.createPop("src/app/pop/helpWebPop")
		end

	end
end

function POP.closeCallback( sender, eventType )
	if eventType == ccui.TouchEventType.ended then
		zc.closePop(popName)
	end
end

function POP.clean(  )
	if mineWidget then
		mineWidget:removeFromParent()
		mineWidget = nil
	end
	zc.removeFromActivePops(popName)
end

return POP