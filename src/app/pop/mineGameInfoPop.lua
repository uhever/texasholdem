--[[
	牌谱纪录界面
]]

local POP = {}

local popName = "src/app/pop/mineGameInfoPop"

local mineGameInfoWidget = nil
local curData = nil
local PokerClass = zc.require("src/app/game/texasholdem/Poker")

function POP.create( params )
	zc.addToActivePops(popName)
	mineGameInfoWidget = zc.createMyPop(popName, {
		opacity = 0,
		bgType = 0,
		bgSrc = "res/Image/main/main_bg.jpg",
		title = {
			center = {text = zc.lang("mineGameinfo_Record", "牌谱记录")},
			left = {type = "return"},
			right = {type = "gameinfo"},
		}
		})
	POP.init(params)
end

function POP.init( params )
	POP.createUI(params)
end


function POP.createUI(params)

	curData = params.data

	zc.createImageView({
		src = "res/Image/main/main_bg.jpg",
		pos = cc.p(zc.width, zc.cy),
		parent = mineGameInfoWidget,
		})

	local tableView = zc.createTableView( {
		-- mask = true,
		bounce = true,
		inertiaScroll = true,
    	size = cc.size(zc.width, zc.height - 110),
    	pos = cc.p(0, 0),
    	scrollBarDisEnabled = true,
    	parent = mineGameInfoWidget,
    	HANDLER_NUMBER_OF_CELLS_IN_TABLEVIEW = POP.numberOfCellsInTableView,
    	HANDLER_TABLECELL_TOUCHED = POP.tableCellTouched,
    	HANDLER_TABLECELL_SIZE_FOR_INDEX = POP.cellSizeForTable,
    	HANDLER_TABLECELL_SIZE_AT_INDEX = POP.tableCellAtIndex,
    	} )
    tableView:reloadData()
	mineGameInfoWidget.tableView = tableView
end

--刷新牌谱ui
function POP:updateUI( params )
	curData = params.data
 	mineGameInfoWidget.tableView:reloadData()
end

--刷新单个cell
function POP:updatecellIndex( params )
	local index = 0
	for i,v in ipairs(curData) do
		if v.id == params.game_info.id then
			curData[i] = params.game_info
			index = i
			break
		end
	end
	mineGameInfoWidget.tableView:updateCellAtIndex(index-1)
end

function POP.tableCellTouched(table, cell)
    print("cell touched at index: " .. cell:getIdx())
end

function POP.cellSizeForTable(table, idx) 
	if curData[idx+1].newday then
		return zc.cx, 50
	end
    return zc.cx, 130
end

function POP.numberOfCellsInTableView(table)
	--cell数量
   return #curData
end

function POP.tableCellAtIndex(table, idx)
		
    local i = idx + 1
	local cellItem = table:dequeueCell()

	--每个tableViewCell显示1条数据
	if cellItem == nil then
		cellItem = ccui.TableViewCell:new()

		POP.buildCellTmpl(cellItem)
	end

	-- 根据数据修改节点内容
	local girdView = cellItem:getChildByTag(1)
	local labelView = cellItem:getChildByTag(0)
	if i <= #curData then
		girdView:setVisible(true)
		labelView:setVisible(true)
		POP.updateCellTmpl(cellItem, girdView, labelView, i)
	else
		girdView:setVisible(false)
		labelView:setVisible(false)
	end

	return cellItem

end

-- 构建tableviewCell节点的模板(无数据参与)
function POP.buildCellTmpl( cellItem )

	zc.createLabel({
		text = "--",
		fontSize = 26,
		color = cc.c3b(90, 102, 109),
		pos = cc.p(120, 25),
		parent = cellItem,
		tag = 0,
		})

	local bgimg = zc.createImageView({
		src = "res/Image/main/common_scale9_img.jpg",
		pos = cc.p(zc.cx, 130/2),
		scale9 = true,
		tag = 1,
		size = cc.size(zc.width,120),
		parent = cellItem,
		})

	local rotationTaB = {-12,12}
 	bgimg.selfPoker = {}
 	bgimg.deskPoker = {}
	--2张手牌
	for ii=1,2 do
		local poker = PokerClass.new({})
		poker:setScale(0.5)
		poker:setPosition(cc.p(50+(ii-1)*15,60))
		poker:setRotation(rotationTaB[ii])
		bgimg:addChild(poker)
		poker.pokerid = 2
		bgimg.selfPoker[#bgimg.selfPoker + 1] = poker
	end

	--牌桌5张牌
	for ii=1,5 do
		local poker = PokerClass.new({})
		poker:setScale(0.5)
		poker:setPosition(cc.p(280+(ii-1)*60,60))
		bgimg:addChild(poker)
		poker.id = 2
		bgimg.deskPoker[#bgimg.deskPoker + 1] = poker
	end
	--状态或牌行
	local stateLabel = zc.createLabel({
		text = zc.lang("lastGame_DiscardCard", "弃牌"),
		fontSize = 26,
		color = cc.c3b(117, 119, 124),
		pos = cc.p(165, 90),
		parent = bgimg,
		})
	bgimg.stateLabel = stateLabel
	--盈利
	local singleMoney = zc.createLabel({
		text = 0,
		fontSize = 26,
		color = cc.c3b(128, 166, 193),
		pos = cc.p(620, 60),
		parent = bgimg,
		})
	bgimg.singleMoney = singleMoney

	--详情
	local dialogImg = zc.createImageView({
		src = "res/Image/main/gameinfo_dialog.png",
		pos = cc.p(zc.width-40, 60),
		parent = bgimg,
		listener = POP.dialogCallback,
		})
	bgimg.dialogImg = dialogImg
	dialogImg.is_collected = 0

	--收藏
	local saveImg = zc.createImageView({
		src = "res/Image/main/gameinfo_save.png",
		pos = cc.p(165, 32),
		parent = bgimg,
		})
	bgimg.saveImg = saveImg
end


function POP.updateCellTmpl( cellItem,girdView, labelView, dataIndex)
	local data = curData[dataIndex]

	-- 数据
	if  data.newday then
		girdView:setVisible(false)
		labelView:setVisible(true)
		labelView:setString(data.newday)
	else
		girdView:setVisible(true)
		labelView:setVisible(false)

		for i=1,2 do
			if data.cards[i] then
				girdView.selfPoker[i]:update({type = math.floor(data.cards[i] / 100), id = data.cards[i] % 100})
				girdView.selfPoker[i].pokerid = data.cards[i]
				girdView.selfPoker[i]:show(  )
			end
		end
		
		for i=1,5 do
			if data.public[i] then
				girdView.deskPoker[i]:setVisible(true)
				girdView.deskPoker[i]:update({type = math.floor(data.public[i] / 100), id = data.public[i] % 100})
				girdView.deskPoker[i].pokerid = data.public[i]
				girdView.deskPoker[i]:show(  )
			else
				girdView.deskPoker[i]:setVisible(false)
			end
		end


		for i,v in ipairs(girdView.selfPoker) do
			v:removePokerMask()
		end

		for i,v in ipairs(girdView.deskPoker) do
			v:removePokerMask()
		end

		if data.state ~= 4 then
			girdView.stateLabel:setString("--")
		end

		local pokerTab = {}
		for k,v in pairs(data.cards) do
			table.insert(pokerTab, v)
		end
		for k,v in pairs(data.public) do
			table.insert(pokerTab, v)
		end

		if #data.cards + #data.public == 7 then

			local maxPokerType,fivePoker = zc.selectPoker(pokerTab)
			girdView.stateLabel:setString(zc.getPokerNameByType(maxPokerType))
			girdView.stateLabel:setColor(cc.c3b(255,178,103))
			--最大牌行高亮，其他两张置灰
		 	local playerPoker = {}
		 	for i,v in ipairs(girdView.selfPoker) do
		 		table.insert(playerPoker, v)
		 	end
		 	for i,v in ipairs(girdView.deskPoker) do
		 		table.insert(playerPoker, v)
		 	end

			local grayPoker = {}
			for ii,vv in ipairs(pokerTab) do
				local index = 1
				for iii,vvv in ipairs(fivePoker) do
					if tonumber(vv) == tonumber(vvv) then
						index = 2
					end
				end
				if index == 1 then
					grayPoker[#grayPoker+1] = vv
				end
			end

			for i,v in ipairs(grayPoker) do
				for ii,vv in ipairs(playerPoker) do
					if v == vv.pokerid then
						vv:addPokerMask()
					end
				end
			end
		end

		--  成牌高亮
		local PokerType,fivePoker
		local lightPoker = {}
		if #pokerTab >= 5 then
			PokerType,fivePoker = zc.selectPoker(pokerTab)
			if PokerType == 8 or (PokerType >= 2 and  PokerType <= 4) then
				lightPoker = zc.getLightPoker(fivePoker)
			elseif PokerType >= 5 and PokerType ~= 8 then
				for i,v in ipairs(fivePoker) do
					lightPoker[#lightPoker+1] = v
				end
			end
		else
			lightPoker = zc.getLightPoker(pokerTab)
		end

		for ii,vv in ipairs(girdView.selfPoker) do
			vv.pokerImg:getChildByTag(12):setVisible(false)
		end
		for ii,vv in ipairs(girdView.deskPoker) do
			vv.pokerImg:getChildByTag(12):setVisible(false)
		end

		for i,v in ipairs(lightPoker) do
			for ii,vv in ipairs(girdView.selfPoker) do
				if v == vv.pokerid  then
					vv.pokerImg:getChildByTag(12):setVisible(true)
				end
			end
			for ii,vv in ipairs(girdView.deskPoker) do
				if v == vv.pokerid  then
					vv.pokerImg:getChildByTag(12):setVisible(true)
				end
			end
		end


		if data.state == 4 then
			girdView.stateLabel:setString(zc.lang("lastGame_DiscardCard", "弃牌"))
			girdView.stateLabel:setColor(cc.c3b(117, 119, 124))
		end

		if data.profit and data.profit > 0 then
			girdView.singleMoney:setString("+" .. zc.getWan(math.abs(data.profit),4))
			girdView.singleMoney:setColor(cc.c3b(255,178,103))
		else
			girdView.singleMoney:setString("-" .. zc.getWan(math.abs(data.profit),4))
			girdView.singleMoney:setColor(cc.c3b(128,166,193))
		end

		if data.is_collected and data.is_collected == 1 then
			girdView.saveImg:setVisible(true)
			girdView.stateLabel:setPositionY(90)
			girdView.dialogImg.is_collected = 1
		else
			girdView.saveImg:setVisible(false)
			girdView.stateLabel:setPositionY(60)
		end
		if data.id then
			girdView.dialogImg.gameid = data.id
		end

	end

end

function POP.dialogCallback( sender, eventType )
	if eventType == ccui.TouchEventType.ended then
		local successCallback = function(data)
            if data.status == 1 then
				zc.createPop("src/app/pop/lastGamePop",{data = data.last_game_info,gameid = sender.gameid,is_collected = sender.is_collected })
            else
                zc.getErrorMsg(data.status)
            end
        end
        zc.loadUrl("m=texas_holdem_game_info_detail", {
        	id = sender.gameid,
            }, successCallback)

	end
end

function POP.closeCallback( sender, eventType )
	if eventType == ccui.TouchEventType.ended then
		zc.closePop(popName)
	end
end

function POP.clean(  )
	if mineGameInfoWidget then
		mineGameInfoWidget:removeFromParent()
		mineGameInfoWidget = nil
	end
	curData = nil
	zc.removeFromActivePops(popName)
end

return POP