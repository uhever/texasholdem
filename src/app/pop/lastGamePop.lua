--[[
	上局回顾界面
]]

local POP = {}

local popName = "src/app/pop/lastGamePop"

local lastGameWidget = nil

local PokerClass = zc.require("src/app/game/texasholdem/Poker")
local SEAT_NUM_LIMIT = 6
-- local giveData = nil
function POP.create( params )

	lastGameWidget = zc.createMyPop(popName, {
		touchClose = true,
		-- opacity = 0,
		})
	POP.init(params)
end

function POP.init( params )
	POP.createUI(params)
end

function POP.createUI(params)

	local data = params.data
	local gameid = params.gameid
	local is_collected = params.is_collected
	local bgImg = zc.createImageView({
		src = "res/Image/game/lastGame_bg.jpg",
		pos = cc.p(zc.width, zc.cy),
		ap = cc.p(1,0.5),
		parent = lastGameWidget,
		})

	local laseGameLabel = zc.createLabel({
		text = zc.lang("lastGame_LastGame", "上局回顾"),
		fontSize = 28,
		color = cc.c3b(255, 178, 103),
		pos = cc.p(bgImg:getContentSize().width - 120, bgImg:getContentSize().height - 70),
		parent = bgImg,
		})

	local retuenArrow = zc.createImageView({
		src = "res/Image/game/arrow_right_img.png",
		pos = cc.p(bgImg:getContentSize().width - 40, bgImg:getContentSize().height - 70),
		parent = bgImg,
		listener = POP.closeCallback,
		})

	 if gameid then
	 	laseGameLabel:setVisible(false)
	 	retuenArrow:setVisible(false)
	 end
	--收藏牌局
	local saveBtn = zc.createButton({
		text = zc.lang("lastGame_Collection", "收藏本手牌"),
		normal = "res/Image/record/record_detail_btn_n.png",
		pressed = "res/Image/record/record_detail_btn_p.png",
		pos = cc.p(bgImg:getContentSize().width /2, 50),
		parent = bgImg,
		fontColor = cc.c3b(155, 159, 167),
		-- size = cc.size(250,65),
		listener = POP.saveCallback,
		})

	lastGameWidget.saveBtn = saveBtn
	 if data and data[1] and data[1].time then
		saveBtn.create_time = data[1].time
	 end
	 if gameid then
		saveBtn.gameid = gameid
	 end

	if is_collected and is_collected == 1 then
		saveBtn:setTitleText(zc.lang("lastGame_CancelCollection", "取消收藏"))
	end

	--玩家信息
	local playerList = zc.createListView({
		-- mask = true,
		inertiaScroll = true,
		bounce = true,
		size = cc.size(564,bgImg:getContentSize().height-150),
		ap = cc.p(0,0),
		pos = cc.p(0, 50),
		parent = bgImg,
		scrollBarDisEnabled = true,
		})

	--排序
	table.sort( data, function(a,b)
 		return a.seat < b.seat
 	end )
	local offset = 0
	for i,v in ipairs(data) do
		if v.is_dealer == 1 then
			offset = i
		end
	end

	local newData = {}
	for i,v in ipairs(data) do
		local newIndex = i - offset > 0 and i - offset or i - offset + #data
		newData[newIndex] = v
	end

	for i,v in ipairs(newData) do
		local cellLayout = zc.createLayout({
			size = cc.size(564, 170),
			})

		--单数底板
		if i%2 == 1 then
			zc.createImageView({
				src = "res/Image/main/common_scale9_img.jpg",
				pos = cc.p(564/2, 170/2),
				scale9 = true,
				size = cc.size(564,170),
				parent = cellLayout,
				})
		end

		--头像
		zc.createHeadIcon({
			image = v.image,
			pos = cc.p(80, 75),
			border = true,
			parent = cellLayout,
			shapeType = 1
			})

		local nameLabel = zc.createLabel({
			text = zc.subName(v.nickname),
			color = cc.c3b(197, 200, 208),
			pos = cc.p(80, 140),
			ap = cc.p(0.5, 0.5),
			fontSize = 26,
			parent = cellLayout,
			})

		--dealer
		if v.is_dealer == 1 then
			zc.createImageView({
				src = "res/Image/game/first_round_img.png",
				pos = cc.p(140, 25),
				parent = cellLayout
			})
		end
		local playerPoker = {}
		local rotationTaB = {-12,12}

		--2张手牌
		for ii=1,#v.cards do
			local poker = PokerClass.new({})
			poker:setScale(0.5)
			poker:setPosition(cc.p(190+(ii-1)*20,75))
			poker:setRotation(rotationTaB[ii])
			cellLayout:addChild(poker)
			if v.cards[ii] then
				poker:update({type = math.floor(v.cards[ii] / 100), id = v.cards[ii] % 100})
				poker:show(  )
			end
			poker.id = v.cards[ii]
			playerPoker[#playerPoker + 1] = poker


			if not zc.isSelf(v.roleid) then
				if v.state == 4 then
					poker:hide(  )
				end
				if v.state ~= 4 and POP:getLastNum(newData) <= 1 then
					poker:hide(  )
				end
			end

		end

		--牌桌5张牌
		for ii=1,#v.public do
			local poker = PokerClass.new({})
			poker:setScale(0.5)
			poker:setPosition(cc.p(290+(ii-1)*59,75))
			cellLayout:addChild(poker)
			if v.public[ii] then
				poker:update({type = math.floor(v.public[ii] / 100), id = v.public[ii] % 100})
				poker:show(  )
			end
			poker.id = v.public[ii]
			playerPoker[#playerPoker + 1] = poker
		end
		--输赢
		local singleMoney = zc.createLabel({
			text = "0",
			fontSize = 26,
			color = cc.c3b(128, 166, 193),
			pos = cc.p(490, 140),
			parent = cellLayout,
			})
		if v.profit and v.profit > 0 then
			singleMoney:setColor(cc.c3b(255,178,103))
			singleMoney:setString("+" .. zc.getWan(math.abs(v.profit),4))
		else
			singleMoney:setColor(cc.c3b(128,166,193))
			singleMoney:setString("-" .. zc.getWan(math.abs(v.profit),4))
		end

		local stateLabel = zc.createLabel({
			text = " ",
			fontSize = 26,
			color = cc.c3b(197, 200, 208),
			pos = cc.p(200, 140),
			parent = cellLayout,
			})


		local pokerTab = {}
		for k,v in pairs(v.cards) do
			table.insert(pokerTab, v)
		end
		for k,v in pairs(v.public) do
			table.insert(pokerTab, v)
		end
		--状态 最大牌行高亮，其他两张置灰
		if #v.cards + #v.public == 7 then
			if zc.isSelf(v.roleid) or (not zc.isSelf(v.roleid) and v.state ~= 4 and POP:getLastNum(newData) > 1 ) then
				local maxPokerType,fivePoker = zc.selectPoker(pokerTab)
				stateLabel:setString(zc.getPokerNameByType(maxPokerType))

				local grayPoker = {}
				for ii,vv in ipairs(pokerTab) do
					local index = 1
					for iii,vvv in ipairs(fivePoker) do
						if tonumber(vv) == tonumber(vvv) then
							index = 2
						end
					end
					if index == 1 then
						grayPoker[#grayPoker+1] = vv
					end
				end

				for i,v in ipairs(grayPoker) do
					for ii,vv in ipairs(playerPoker) do
						if v == vv.id then
							vv:addPokerMask()
						end
					end
				end
			end
		end

		if zc.isSelf(v.roleid) or (not zc.isSelf(v.roleid) and v.state ~= 4 and POP:getLastNum(newData) > 1 ) then
			local PokerType,fivePoker
			local lightPoker = {}
			if #pokerTab >= 5 then
				PokerType,fivePoker = zc.selectPoker(pokerTab)
				if PokerType == 8 or (PokerType >= 2 and  PokerType <= 4) then
					lightPoker = zc.getLightPoker(fivePoker)
				elseif PokerType >= 5 and PokerType ~= 8 then
					for i,v in ipairs(fivePoker) do
						lightPoker[#lightPoker+1] = v
					end
				end
			else
				lightPoker = zc.getLightPoker(pokerTab)
			end

			--  成牌高亮
			for i,v in ipairs(lightPoker) do
				for ii,vv in ipairs(playerPoker) do
					if v == vv.id  then
						vv.pokerImg:getChildByTag(12):setVisible(true)
					end
				end
			end
		end
		if v.state == 4 then
			stateLabel:setString(zc.lang("lastGame_DiscardCard", "弃牌"))
		end


		--自己label变金色
		if zc.isSelf(v.roleid) then
			nameLabel:setColor(cc.c3b(255,219,143))
			stateLabel:setColor(cc.c3b(255,219,143))
			singleMoney:setColor(cc.c3b(255,219,143))
		end

		playerList:pushBackCustomItem(cellLayout)
	end

end


function POP:getLastNum( _tab )
	local index = 0
	for i,v in ipairs(_tab) do
		if v.state ~= 4 then
			index = index + 1
		end
	end
	return index
end



function POP.closeCallback( sender, eventType )
	if eventType == ccui.TouchEventType.ended then
		zc.closePop(popName)
	end
end

function POP.saveCallback( sender, eventType )
	if eventType == ccui.TouchEventType.ended then

		local successCallback = function(data)
			dump(data)
            if data.status == 1 then
				if sender.gameid then
					local lastPop = zc.getPop("src/app/pop/mineGameInfoPop")
					if zc.popIsActive( "src/app/pop/mineGameInfoPop" ) then
						lastPop:updatecellIndex(data)
					end
					if data.game_info.is_collected == 1 then
						zc.tipInfo(zc.lang("lastGame_CollectionSuccess", "收藏成功"))
						lastGameWidget.saveBtn:setTitleText(zc.lang("lastGame_CancelCollection", "取消收藏"))
					elseif data.game_info.is_collected == 0 then
						zc.tipInfo(zc.lang("lastGame_CancelCollection", "取消收藏"))
						lastGameWidget.saveBtn:setTitleText(zc.lang("lastGame_Collection", "收藏本手牌"))
					end

				end
            else
                zc.getErrorMsg(data.status)
            end
        end

		if sender.gameid then
	        zc.loadUrl("m=texas_holdem_game_info_keep", {
	        	id = sender.gameid,
	            }, successCallback)
	    elseif sender.create_time then
	        zc.loadUrl("m=texas_holdem_game_info_keep", {
	        	create_time = sender.create_time,
	            }, successCallback)
		end
	end
end


function POP.clean(  )
	if lastGameWidget then
		lastGameWidget:removeFromParent()
		lastGameWidget = nil
	end

end

return POP