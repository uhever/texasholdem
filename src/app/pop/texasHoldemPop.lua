

--[[
	修改资料界面
]]

local POP = {}

local popName = "src/app/pop/texasHoldemPop"

local texasHoldemWidget = nil

local btnTab = nil

local gameType = 1 --0普通玩法 1保险模式

function POP.create( params )
	zc.addToActivePops(popName)
	texasHoldemWidget = zc.createMyPop(popName, {
		opacity = 0,
		bgType = 0,
		bgSrc = "res/Image/main/main_bg.jpg",
		title = {
			center = {text = zc.lang("enterMain_Season", "选择场次")},
			left = {type = "return"},
			right = {type = "shop"},
		}
		})

	POP.init(params)

	POP.createUI()
end

function POP.init( params )

	
end

function POP.createUI(  )

	local moneyBg = zc.createImageView({
		src = "res/Image/shop/shop_money_bg.png",
		pos = cc.p(zc.width / 2, zc.height - 150),
		parent = texasHoldemWidget,
		-- scale9 = true,
		-- size = cc.size(zc.width,100),
		})


	zc.createAttrLabel({
		{image = true, src = "res/Image/main/mine_chip_icon.png"},
		{text = zc.getWan(PlayerData.base.money, 4), fontSize = 26, color = cc.c3b(239, 202, 140), offsetX = 10, offsetY = 5},
		},
		{
		ap = cc.p(0.5, 0.5),
		pos = cc.p(zc.width / 2, zc.height - 150),
		parent = texasHoldemWidget,
		})

	if not APP_REVIEW then
		zc.createButton({
			text = "好友对局", 
	        -- fontColor = cc.c3b(155, 159, 167),
			normal = "res/Image/game/session/session_btn_scale9_n.png",
			pressed = "res/Image/game/session/session_btn_scale9_p.png",
			ap = cc.p(1, 0.5),
			pos = cc.p(zc.width - 25, zc.height - 150),
			scale9 = true,
			size = cc.size(142, 48),
			parent = texasHoldemWidget,
			listener = function ( sender, eventType )
				if eventType == ccui.TouchEventType.ended then
					zc.createPop("src/app/pop/friendlyGamePop")
				end
			end 
			})
	end

	

	local menuCfg = {
			{name = "first",color = cc.c3b(174, 189, 217)},
			{name = "first",color = cc.c3b(174, 189, 217)},
			{name = "second",color = cc.c3b(178, 161, 138)},
			{name = "second",color = cc.c3b(178, 161, 138)},
			{name = "third",color = cc.c3b(255, 202, 202)},
			{name = "third",color = cc.c3b(255, 202, 202)},
			}


	btnTab = {}
	-- 6个场次
	for i=1,3 do -- 行
		for j=1,2 do -- 列
			local idx = (i - 1) * 2 + j
			if gameType == 1 then
				idx = idx + 6
			end
			local posX = zc.cx - 180 + (j-1) * 360
			local posY = zc.cy + 340 - (i-1) * 340
			local sessionBtn = zc.createButton({
				tag = idx,
				normal = "res/Image/game/session/dz_session_" .. menuCfg[(i - 1) * 2 + j].name .. ".png",
				pressed = "res/Image/game/session/dz_session_" .. menuCfg[(i - 1) * 2 + j].name ..".png",
				pos = cc.p(posX, posY),
				parent = texasHoldemWidget,
				listener = POP.selectCallback,
				})

			-- 选择场次读表
			local over_wan = false

			local limitMoney = DB_STATICTABLE["proto_texas_holdem"]["list"][tostring(idx)]["max"]
			if tonumber(limitMoney) >= 10000 then
				over_wan = true
				limitMoney = tostring(math.ceil(tonumber(limitMoney)/10000))
			end 
			local startPosX = 120-(string.len(limitMoney)-1)*20
			if over_wan then
				startPosX = 120-string.len(limitMoney)*20
			end
			for ii=1,string.len(limitMoney) do
				local fontnumber = string.sub(limitMoney, ii,ii)
				local numImg = cc.LabelBMFont:create(fontnumber, "res/Image/game/session/session_label_" .. menuCfg[(i - 1) * 2 + j].name ..".fnt")
				numImg:setPosition(cc.p(startPosX+(ii-1)*40, 200+(ii-1)*3))
				sessionBtn:addChild(numImg)

			end
			if over_wan then
				local numImg = cc.LabelBMFont:create("w", "res/Image/game/session/session_label_" .. menuCfg[(i - 1) * 2 + j].name ..".fnt")
				numImg:setPosition(cc.p(startPosX+10+string.len(limitMoney)*40, 200+string.len(limitMoney)*3))
				sessionBtn:addChild(numImg)
			end
			zc.createImageView({
				src = "res/Image/game/session/dz_max_" .. menuCfg[(i - 1) * 2 + j].name .. ".png",
				pos = cc.p(sessionBtn:getContentSize().width / 2, sessionBtn:getContentSize().height / 2 - 10),
				parent = sessionBtn,
				})
			zc.createLabel({
				text = "0",
				color = menuCfg[(i - 1) * 2 + j].color,
				pos = cc.p(120, 80),
				ap = cc.p(0, 0.5),
				fontSize = 26,
				parent = sessionBtn,
				tag = 33,
				})

			sessionBtn.endPos = cc.p(posX, posY)
			btnTab[idx] = sessionBtn

			--大下盲注
 			local sb = DB_STATICTABLE["proto_texas_holdem"]["list"][tostring(idx)]["sb"]
 			local bb = DB_STATICTABLE["proto_texas_holdem"]["list"][tostring(idx)]["bb"]

			local betBg = zc.createImageView({
				src = "res/Image/main/bet_black_Img.png",
				pos = cc.p(sessionBtn:getContentSize().width/2,-25),
				parent = sessionBtn,
				scaleX = 1.2,
				})

			zc.createLabel({
				text = zc.lang("texasHoldemScene_Blind", "盲注: ") .. zc.getWan(sb,4) .. "/" .. zc.getWan(bb,4),
				color = cc.c3b(132,111,83),
				pos = cc.p(sessionBtn:getContentSize().width/2,-25),
				fontSize = 28,
				parent = sessionBtn,
				})

		end
		
	end
	--刷新在线人数
	POP:getPlayerNum()
end

function POP.getPlayerNum( )
	local function successCallback(data)
		if data.status == 1 then
			for k,v in pairs(data.list) do
				if btnTab[v.id] ~= nil and (not tolua.isnull(btnTab[v.id]:getChildByTag(33)))  then
					btnTab[v.id]:getChildByTag(33):setString(zc.getWan(v.player_num,4))
				end
			end
		else
			zc.getErrorMsg(data.status)
		end
	end
	
	zc.loadUrl("m=texas_holdem_info", {
		}, successCallback)

end


-- 选择场次
function POP.selectCallback( sender, eventType )
	if eventType == ccui.TouchEventType.ended then
		local subType = sender:getTag()
		local limitTab = DB_STATICTABLE["proto_texas_holdem"]["list"]
		if PlayerData.base.money < tonumber(limitTab[tostring(subType)].min) then
			local needMoney = tonumber(limitTab[tostring(subType)].min) - PlayerData.base.money
			zc.confirm({
	            btnNum         = 2,
	            text           = string.format(zc.lang("texasHoldem_BuyInfo", "您还需要%s金币才能进入房间，是否购买？"),tostring(zc.getWan(needMoney,2))), 
	            okCallback = function()
	            	zc.createPop("src/app/pop/shopPop")
	        	end
	        })
			return
		end

		local successCallback = function(data)
	        if(tonumber(data.status) == 1) then
	            zc.showLoading()
	        else
	            zc.getErrorMsg(data.status)
	        end
	    end

	    zc.loadUrl("m=th_join_room", {
	    		type = zc.GAME_TYPE_TEXAS_NORMAL,
	    		room_type = subType,
	            id = 0,
	        }, successCallback)


	end
end


function POP.closeCallback( sender, eventType )
	if eventType == ccui.TouchEventType.ended then
		zc.closePop(popName)
	end
end

function POP.clean(  )
	if texasHoldemWidget then
		texasHoldemWidget:removeFromParent()
		texasHoldemWidget = nil
	end
	zc.removeFromActivePops(popName)
	btnTab = nil
end

return POP


