--[[
	购买保险界面界面
]]

local POP = {}

local popName = "src/app/pop/buyInsurancePop"

local PokerClass = zc.require("src/app/game/texasholdem/Poker")

local buyInsuranceWidget = nil

local poolData = nil
local ground = nil
local statetime = nil
local insuranceTimer = nil
local operatorTime = nil
local poolIndex = nil
function POP.create( params )
	zc.addToActivePops(popName)

	buyInsuranceWidget = zc.createMyPop(popName, {
		mask =true,
		opacity = 150,
		touchClose = true,
		})

	POP.init()
	POP.createUI(params)
end

function POP.init( params )
	
end

function POP.createUI( params )

	poolData = params.poolData
	ground = params.ground
	poolIndex = params.poolIndex
	local myInfo = ground:getPlayerById(PlayerData.base.roleid)
	statetime =  myInfo.insurance_state_time
	operatorTime = myInfo.operatorTime
	local selfBetNum, theOdds = ground:getInsuranceBets(poolData.poolid)

	local safeBg = zc.createImageView({
		src = "res/Image/game/insurance/safe_img_bg.png",
		pos = cc.p(zc.width / 2,780),
		touchEnabled = true,
		parent = buyInsuranceWidget,
		})

	-- 初始化UI
	zc.createLabel({
		text = "公共牌",
		fontSize = 22,
		color = cc.c3b(156,159,169),
		pos = cc.p(65, 460),
		parent = safeBg,
		})

	zc.createLabel({
		text = "赔率:",
		fontSize = 22,
		color = cc.c3b(156,159,169),
		pos = cc.p(370, 485),
		parent = safeBg,
		ap = cc.p(0,0.5),
		})
	local capitalCount = zc.createLabel({
		text = "投保: " .. zc.getWan(math.ceil(poolData.amount),4),
		fontSize = 24,
		color = cc.c3b(130,198,168),
		pos = cc.p(50, 170),
		ap = cc.p(0,0.5),
		parent = safeBg,
		})

	local oddsCount =  zc.createLabel({
		text = "赔付: " .. zc.getWan(math.ceil(poolData.amount)*theOdds,4),
		ap = cc.p(0,0.5),
		fontSize = 24,
		color = cc.c3b(255,178,103),
		pos = cc.p(410, 170),
		parent = safeBg,
		})

	local oddsLabel = zc.createLabel({
		text = theOdds,
		fontSize = 30,
		color = cc.c3b(238,116,0),
		pos = cc.p(440, 485),
		parent = safeBg,
		ap = cc.p(0,0.5),
		})

	local str = string.format("已选: %d张",#poolData.choose_outs)
	local selectNum = zc.createLabel({
		text = str,
		fontSize = 22,
		color = cc.c3b(156,159,169),
		pos = cc.p(370, 435),
		parent = safeBg,
		ap = cc.p(0,0.5),
		})
	zc.createLabel({
		text = "全选",
		fontSize = 22,
		color = cc.c3b(156,159,169),
		pos = cc.p(550, 435),
		parent = safeBg,
		})

 	local betNum = ground:getBetPoolNum( poolData.poolid )
	zc.createAttrLabel({
		{image = true, src = "res/Image/game/chip/chip_0.png"},
		{text = zc.getWan(betNum, 4), fontSize = 24, color = cc.c3b(255, 255, 255), offsetX = 10, offsetY = 5},
		},
		{
		ap = cc.p(0, 0.5),
		pos = cc.p(20, safeBg:getContentSize().height -35),
		parent = safeBg,
		})

	--倒计时
	local remainTime = statetime - operatorTime

	local percent = remainTime/statetime * 100

	local lastTime = zc.createLabel({
		text = "0s",
		fontSize = 24,
		color = cc.c3b(241,0,0),
		pos = cc.p(550,safeBg:getContentSize().height - 30),
		parent = safeBg,
		})
	lastTime:setVisible(false)
	local progressImg = zc.createProgressTimer({
		src = "res/Image/game/insurance/capital_progress.jpg",
		type = cc.PROGRESS_TIMER_TYPE_BAR,
		percent = percent,
		pos = cc.p(safeBg:getContentSize().width/2,safeBg:getContentSize().height - 60),
		parent = safeBg,
		})
	progressImg:setVisible(false)
	progressImg:setMidpoint(cc.p(1, 0.5))
	progressImg:setBarChangeRate(cc.p(1, 0))
	if remainTime > 0 then
		insuranceTimer = scheduler.scheduleUpdateGlobal(function ( delta )
			remainTime = remainTime + delta
			if remainTime >= statetime then
				scheduler.unscheduleGlobal(insuranceTimer)
				insuranceTimer = nil
				if not tolua.isnull(progressImg) and not tolua.isnull(lastTime) then
					progressImg:setPercentage(0)
					lastTime:setVisible(false)
				end
			else
				lastTime:setVisible(true)
				progressImg:setVisible(true)
				if not tolua.isnull(progressImg) and not tolua.isnull(lastTime) then
					progressImg:setPercentage(100 - remainTime * 100/statetime )
					lastTime:setString(math.floor(statetime - remainTime) .. "s")
				end
			end
		end)
	end

	-- 自己和其他玩家的底牌
	local playerList = zc.createListView({
		-- mask = true,
		inertiaScroll = true,
		bounce = true,
		size = cc.size(safeBg:getContentSize().width-40, 115),
		ap = cc.p(0,0),
		pos = cc.p(20, safeBg:getContentSize().height - 180),
		parent = safeBg,
		direction = ccui.ScrollViewDir.horizontal,
		})

	local playerCards = {}
	local poolHeros = {}
	for i,v in ipairs(ground.bet_pools) do
		if v.poolid == poolData.poolid then
			for ii,vv in ipairs(v.bet_info) do
				poolHeros[#poolHeros+1] = vv.roleid
			end
			break
		end
	end

	--自己在最前面
	table.insert(playerCards,{["cards"] = myInfo.cards,["name"] = myInfo.nickname})

	for i,v in ipairs(ground.playerList) do
		if  v.state ~= 4 and table.inArray(v.roleid,poolHeros) and v.roleid ~= PlayerData.base.roleid then
			playerCards[#playerCards+1] = {["cards"] = v.cards,["name"] = v.nickname}
		end
	end
	--手牌
	for i,v in ipairs(playerCards) do
		local cellLayout = zc.createLayout({
			size = cc.size(120, 110),
			})
			zc.createLabel({
			text = v.name,
			fontSize = 22,
			color = cc.c3b(156,159,169),
			pos = cc.p(60, 90),
			parent = cellLayout,
			})
		--2张手牌
		for j=1,2 do
			local poker = PokerClass.new({})
			poker:setScale(0.4)
			poker:setPosition(cc.p(37+(j-1)*46,35))
			poker:update({type = math.floor(v.cards[j] / 100), id = v.cards[j] % 100})
			poker:show(  )
			cellLayout:addChild(poker)
		end
		playerList:pushBackCustomItem(cellLayout)
	end

	--牌桌公共牌
	for i=1,#ground.deskPokers do
		local poker = PokerClass.new({})
		poker:setScale(0.4)
		poker:setPosition(cc.p(130+(i-1)*50,460))
		poker:update({type = math.floor(ground.deskPokers[i].card / 100), id = ground.deskPokers[i].card % 100})
		poker:show(  )
		safeBg:addChild(poker)
	end

	local curMoney = ground:getPlayerById(PlayerData.base.roleid).self_money
	--计算当前所有选择的池子的购买保险筹码，根据自己的钱捡去筹码 判断金币是否足够
	local haveBuyMoney = 0
	for i,v in ipairs(ground.insuranceSelected) do
		if v.poolid ~= poolData.poolid then
			haveBuyMoney = haveBuyMoney + v.amount
		end
	end
	curMoney = curMoney - haveBuyMoney
	--赔付金额不能超过当前边池的总筹码
	local limitMoney = betNum
	limitMoney = curMoney < limitMoney  and curMoney or limitMoney

	local outsPoker = {}
	--outs牌
	for i=1,2 do
		for j=1,8 do
			local idx = (i-1)*8 + j
			if poolData.outs[idx] then
				local poker = PokerClass.new({})
				poker:setScale(0.6)
				poker:setPosition(cc.p(45+(j%9-1)*70,362-(i-1)*97))
				poker:update({type = math.floor(poolData.outs[idx] / 100), id = poolData.outs[idx] % 100})
				poker:show(  )
				safeBg:addChild(poker)
				table.insert(outsPoker, poker)
				if table.inArray(poolData.outs[idx], poolData.choose_outs) then
					poker.pokerImg:getChildByTag(13):setVisible(true)
				end
				poker.pokerImg:setTouchEnabled(true)
				poker.pokerImg:addTouchEventListener(function ( sender, eventType )
			        if eventType == ccui.TouchEventType.ended then
						if table.inArray(poolData.outs[idx], poolData.choose_outs) then
							if #poolData.choose_outs <= 1 then
								zc.tipInfo("您必须至少选择一张outs牌")
							else
								sender:getChildByTag(13):setVisible(false)
								table.removebyvalue(poolData.choose_outs, poolData.outs[idx])
							end
						else
							sender:getChildByTag(13):setVisible(true)
							table.insert(poolData.choose_outs, poolData.outs[idx])
						end

		              	theOdds = POP.getInsuranceBets()
		              	if math.ceil(poolData.amount) * theOdds > limitMoney then
		              		poolData.amount = math.floor(limitMoney/theOdds)
		              	end
		              	capitalCount:setString("投保: " .. zc.getWan(poolData.amount,4))
	            		oddsCount:setString("赔付: " .. zc.getWan(poolData.amount * theOdds,4))
	            		oddsLabel:setString(theOdds)
	            		selectNum:setString(string.format("已选: %d张",#poolData.choose_outs))
			        end
			    end)
			end
		end
	end

	local allSelect = zc.createCheckBox({
			boxSrc = "res/Image/game/insurance/check_box.png",
			selectSrc = "res/Image/game/insurance/check_yellow_img.png",
			selected = false,
			pos = cc.p(500,435),
			parent = safeBg,
			listener = function(sender,eventType)
	            if eventType == ccui.CheckBoxEventType.selected then
	              	poolData.choose_outs = {}
	              	for i,v in ipairs(outsPoker) do
	              		v.pokerImg:getChildByTag(13):setVisible(true)
	              	end
	              	for i,v in ipairs(poolData.outs) do
						poolData.choose_outs[i] = v
	              	end

	              	theOdds = POP.getInsuranceBets()
	              	capitalCount:setString("投保: " .. zc.getWan(math.ceil(poolData.amount),4))
            		oddsCount:setString("赔付: " .. zc.getWan(math.ceil(poolData.amount) * theOdds,4))
            		oddsLabel:setString(theOdds)
            		selectNum:setString(string.format("已选: %d张",#poolData.choose_outs))
	            elseif eventType == ccui.CheckBoxEventType.unselected then
	            	zc.tipInfo("您必须至少选择一张outs牌")
	            end
	        end
		})
	if #poolData.choose_outs == #poolData.outs then
		allSelect:setSelected(true)
	end


	--滑动条
    local slider = zc.createSlider({
		barsrc = "res/Image/game/insurance/capital_slider.png",
		slidballsrc = "res/Image/game/chip/chip_0.png",
		progressbarsrc = "res/Image/game/insurance/capital_slider.png",
		pos = cc.p(safeBg:getContentSize().width/2,125),
		percent = math.floor(poolData.amount/limitMoney*100),
     	parent = safeBg,
     	listener = function ( sender, eventType )
            if eventType == ccui.SliderEventType.percentChanged then
				poolData.amount = math.floor(sender:getPercent() * limitMoney /100/theOdds)
            	capitalCount:setString("投保: " .. zc.getWan(poolData.amount,4))
            	oddsCount:setString("赔付: " .. zc.getWan(poolData.amount * theOdds,4))
            end
        end
     	})
	slider:setPercent(math.ceil(poolData.amount*theOdds/limitMoney*100))

	--保本
	local capitalBtn = zc.createButton({
		text = "保本",
		fontSize = 24,
		fontColor = cc.c3b(68,77,75),
		normal = "res/Image/game/insurance/captital_btn_bg.png",
		pressed = "res/Image/game/insurance/captital_btn_bg.png",
		pos = cc.p(safeBg:getContentSize().width/2 - 150 ,60),
		parent = safeBg,
		listener = function ( sender, eventType )
			if eventType == ccui.TouchEventType.ended then

				selfBetNum, theOdds = ground:getInsuranceBets(poolData.poolid)
				local needBet = selfBetNum/theOdds

				if needBet > curMoney then
					zc.tipInfo("金币不足")
					return
				end
				slider:setPercent(math.ceil(needBet*theOdds/limitMoney*100))
				poolData.amount = math.ceil(needBet)
				capitalCount:setString("投保: " .. zc.getWan(poolData.amount,4))
            	oddsCount:setString("赔付: " .. zc.getWan(poolData.amount * theOdds,4))
			end
		end
		})
	--确定
	local confirmBtn = zc.createButton({
		text = "确定",
		fontSize = 24,
		fontColor = cc.c3b(68,77,75),
		normal = "res/Image/game/insurance/confirm_btn_bg.png",
		pressed = "res/Image/game/insurance/confirm_btn_bg.png",
		pos = cc.p(safeBg:getContentSize().width/2 + 150 ,60),
		parent = safeBg,
		listener = function ( sender, eventType )
			if eventType == ccui.TouchEventType.ended then

				if poolData.amount > curMoney then
					zc.tipInfo("金币不足")
					return
				end

				ground:updateInsuranceLayout(poolIndex,poolData)
				zc.closePop(popName)
			end
		end
		})

end


--得到单个池子自己的下注量和保本赔率
function POP.getInsuranceBets()
	--赔率
	local theOdds = 0
	for i,v in ipairs(DB_STATICTABLE["proto_insurance_mutiple"]["list"]) do
		if tonumber(v.outs) == #poolData.choose_outs then
			theOdds = math.floor(tonumber(v.mutiple))/100 - 1
			break
		end
	end
	return theOdds
end


function POP.closeCallback( sender, eventType )
	if eventType == ccui.TouchEventType.ended then
		zc.closePop(popName)
	end
end

function POP.clean(  )
	if buyInsuranceWidget then
		buyInsuranceWidget:removeFromParent()
		buyInsuranceWidget = nil
	end
	zc.removeFromActivePops(popName)
	statetime = nil
	poolData = nil
	ground = nil
	poolIndex = nil
	if insuranceTimer then
		scheduler.unscheduleGlobal(insuranceTimer)
		insuranceTimer = nil
	end
	operatorTime = nil
end

return POP



