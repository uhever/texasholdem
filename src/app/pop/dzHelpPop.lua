--[[
	德州扑克帮助界面
]]

local POP = {}

local popName = "src/app/pop/dzHelpPop"

local dzHelpWidget = nil


function POP.create( params )
	zc.addToActivePops(popName)

	dzHelpWidget = zc.createMyPop(popName, {
		opacity = 0,
		touchClose = true,
		swallowTouches = false,
		})
	POP.init(params)

end

function POP.init( params )
	
	POP.createUI()
end


function POP.createUI(  )
	local helpBg = zc.createImageView({
		src = "res/Image/game/help/dz_help_bg.png",
		ap = cc.p(0, 1),
		pos = cc.p(0, zc.height),
		parent = dzHelpWidget,
		})

	zc.createImageView({
		src = "res/Image/game/help/dz_help_label.png",
		ap = cc.p(0.5, 0.5),
		pos = cc.p(helpBg:getContentSize().width/2, helpBg:getContentSize().height/2+10),
		parent = helpBg,
		})

end
function POP.closeCallback( sender, eventType )
	if eventType == ccui.TouchEventType.ended then
		zc.closePop(popName)
	end
end


function POP.clean(  )
	if dzHelpWidget then
		dzHelpWidget:removeFromParent()
		dzHelpWidget = nil
	end
	zc.removeFromActivePops(popName)
end

return POP