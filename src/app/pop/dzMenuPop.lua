--[[
	德州扑克菜单
]]

local POP = {}

local popName = "src/app/pop/dzMenuPop"

local dzMenuWidget = nil

local menuBg = nil


function POP.create( params )
	zc.addToActivePops(popName)

	dzMenuWidget = zc.createMyPop(popName, {
		opacity = 150,
		touchClose = true,
		swallowTouches = false,
		})
	dzMenuWidget.state = params.state or 0
	POP.init(params)

end

function POP.init( params )
	
	POP.createUI()
end


function POP.createUI(  )

	-- 配置
	local menuConfig = {
	-- {name = "help",text = "牌型提示", icon_n = "res/Image/game/menu/menu_1_n.png", icon_p = "res/Image/game/menu/menu_1_p.png"},
	-- {name = "buybet", text ="补充记分牌",icon_n = "res/Image/game/menu/menu_2_n.png", icon_p = "res/Image/game/menu/menu_2_p.png"},
		{name = "changeRoom", text =zc.lang("dzMenu_ChangeTable", "换桌"), icon_n = "res/Image/game/menu/menu_3_n.png", icon_p = "res/Image/game/menu/menu_3_p.png",},
		{name = "standup",  text = zc.lang("dzMenu_StandUpCrowd", "站起围观"),icon_n = "res/Image/game/menu/menu_4_n.png", icon_p = "res/Image/game/menu/menu_4_p.png"},
	-- {name = "returnMainScene", text = "返回大厅", icon_n = "res/Image/game/menu/menu_5_n.png", icon_p = "res/Image/game/menu/menu_5_p.png"},
		{name = "exitgame",text = zc.lang("dzMenu_Exit", "退出牌局"), icon_n = "res/Image/game/menu/menu_6_n.png", icon_p = "res/Image/game/menu/menu_6_p.png"},
	}


	menuBg = zc.createLayout({
		ap = cc.p(0.5,0.5),
		pos = cc.p(zc.cx,92 * #menuConfig + zc.cy - 40),
		zOrder = Z_ORDER_CONTROL_LAYOUT,
		size = cc.size(zc.width,zc.height),
		parent = dzMenuWidget
		})

	for i=1, #menuConfig do
		local menuItemLayout = zc.createImageView({
			name = menuConfig[i].name,
			src = "res/Image/game/menu/dz_menu_bg.png",
			pos = cc.p(177, zc.height - 46 - (i-1)*92),
			parent = menuBg,
			touchEnabled = true,
			tag = i,
			listener = POP.menuCallback
			})



		local iconImg = zc.createImageView({
			src = menuConfig[i].icon_p,
			pos = cc.p(85, 45),
			parent = menuItemLayout,
			tag = 1,
			})

		zc.createLabel({
			text = menuConfig[i].text,
			fontSize = 22,
			color = cc.c3b(195,166,89), -- 黄 cc.c3b(195,166,89)  灰cc.c3b(127, 128, 137),
			pos = cc.p(210, 45),
			parent = menuItemLayout,
			tag = 2,
			})

		if (dzMenuWidget.state == 2 or dzMenuWidget.state == 3) and (menuConfig[i].name == "changeRoom" or menuConfig[i].name == "standup") then
			menuItemLayout:setTouchEnabled(false)
			menuItemLayout:getChildByTag(1):loadTexture(menuConfig[i].icon_n)
			menuItemLayout:getChildByTag(2):setColor(cc.c3b(127, 128, 137))
		end

	end

	local moveAct = cc.EaseExponentialOut:create(cc.MoveBy:create(0.2, cc.p(0, -92 * #menuConfig)))
	menuBg:runAction(moveAct)



	--DEBUG调试模式
	local debug_labelTab = {zc.lang("dzMenu_OverTime", "加时"),zc.lang("dzMenu_Skip", "跳过"),
							zc.lang("dzMenu_Pause", "暂停"),zc.lang("dzMenu_Continue", "继续")}
	local sendTab = {".zc_add_time",".zc_delete_time",".zc_pause",".zc_continue"}
	for i=1,4 do
		zc.createButton({
		text = debug_labelTab[i],
		normal = "res/Image/common/yellow_btn_n.png",
		pressed = "res/Image/common/yellow_btn_n.png",
		pos = cc.p(zc.cx-200, 650-(i-1)*100 ),
		parent = dzMenuWidget,
		listener = function ( sender, eventType )
			if eventType == ccui.TouchEventType.ended then

			local successCallback = function(data)
		        if(tonumber(data.status) == 1) then
		            zc.tipInfo(zc.lang("dzMenu_SendSuccess", "发送成功"))
		        end
		    end

		    zc.loadUrl("m=user_chat", {
		    	type = 3,
		    	roleid = PlayerData.base.roleid,
		    	msginfo = sendTab[i],
		        }, successCallback)

			end
		end
		})
	end


end


function POP.menuCallback( sender, eventType )
	if eventType == ccui.TouchEventType.ended then
		local name = sender:getName()
		if name == "back" then
			local successCallback = function(data)
		        if(tonumber(data.status) == 1) then
		            zc.tipInfo(zc.lang("dzMenu_ExitRoomSuccess", "退出房间成功"))
					zc.switchScene("src/app/scene/mainScene")
					if GotyeApi then
		  				GotyeApi:leaveRoom(zc.GOTYEROOMID)
		  			end
		        end
		    end

		    zc.loadUrl("m=th_leave_room", {
		            -- type = 1,
		       }, successCallback)

		elseif name == "standup" then
			zc.socket.sendTable({m = "texas_holdem_standup"})
		elseif name == "help"  then
			print("打开牌型帮助界面")
		elseif  name == "buybet" then
			zc.createPop("src/app/pop/dzBuyChipPop")
		elseif name == "returnMainScene" then
  			local successCallback = function(data)
		        if(tonumber(data.status) == 1) then
		            zc.tipInfo(zc.lang("dzMenu_ExitRoomSuccess", "退出房间成功"))
					zc.switchScene("src/app/scene/mainScene")
					if GotyeApi then
		  				GotyeApi:leaveRoom(zc.GOTYEROOMID)
		  			end
		        end
		    end

		    zc.loadUrl("m=th_leave_room", {
		            -- type = 1,
		       }, successCallback)
		elseif  name == "exitgame" then
  			local successCallback = function(data)
		        if(tonumber(data.status) == 1) then
		            zc.tipInfo(zc.lang("dzMenu_ExitRoomSuccess", "退出房间成功"))
					zc.switchScene("src/app/scene/mainScene")
					if GotyeApi then
		  				GotyeApi:leaveRoom(zc.GOTYEROOMID)
		  			end
		        end
		    end

		    zc.loadUrl("m=th_leave_room", {
		            -- type = 1,
		       }, successCallback)
  		elseif  name == "changeRoom" then
  			local successCallback = function(data)
		        if(tonumber(data.status) == 1) then
		        	zc.showLoading()
		        	if GotyeApi then
		  				GotyeApi:leaveRoom(zc.GOTYEROOMID)
		  			end
		  		elseif (tonumber(data.status) == 11769) then
		  			zc.getErrorMsg(data.status)
		  		else
		  			if GotyeApi then
		  				GotyeApi:leaveRoom(zc.GOTYEROOMID)
		  			end
		  			zc.switchScene("src/app/scene/mainScene")
		        end
		    end

		    zc.loadUrl("m=texas_holdem_change_room", {
		       }, successCallback)
		end

		zc.closePop(popName)

	end
end


function POP.closeCallback( sender, eventType )
	if eventType == ccui.TouchEventType.ended then

		menuBg:stopAllActions()

		local moveAct = cc.EaseExponentialIn:create(cc.MoveBy:create(0.2, cc.p(0, 600)))
		local callfunc = cc.CallFunc:create(function (  )
			zc.closePop(popName)
		end)
		menuBg:runAction(cc.Sequence:create({moveAct, callfunc}))
		
	end
end

function POP.clean(  )
	if dzMenuWidget then
		dzMenuWidget:removeFromParent()
		dzMenuWidget = nil
	end

	menuBg = nil

	zc.removeFromActivePops(popName)
end

return POP