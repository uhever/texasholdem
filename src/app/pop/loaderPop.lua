--[[
	下载器的界面
]]

local POP =  {}
local loaderWidget = nil
local popName     = "src/app/pop/loaderPop"

local titleText = nil
local loadingBar = nil
local loadingBarText = nil
local processText = nil
local infoText = nil

---------------------------- 创建 -----------------------------
function POP.create(objUiType)

    local uiType = 1
    if(objUiType) then
        uiType = objUiType
    end

    print("uiType:", uiType)
    if(uiType == 1) then
        POP.createUI1()
    else
        POP.createUI2()
    end

end

function POP.createUI1()

    loaderWidget = zc.createMyPop(popName, {
        mask = true,
        opacity = 0
        })

    local loadingBarBg = zc.createImageView({
        src = "res/Image/loader/loader_bar_bg.png",
        pos = cc.p(zc.cx, zc.cy - 100 - 170),
        parent = loaderWidget,
    })

    loadingBar = zc.createLoadingBar({
        name = "loadingBar", 
        src = "res/Image/loader/loader_bar.png", 
        percent = 0,
        pos = cc.p(loadingBarBg:getContentSize().width/2, loadingBarBg:getContentSize().height/2),
        parent = loadingBarBg,
        })
    
    -- loadingBarText = zc.createLabel({
    --     text = "0%",
    --     outline = true,
    --     pos = cc.p(loadingBarBg:getContentSize().width/2, loadingBarBg:getContentSize().height/2),
    --     parent = loadingBarBg,
    --     fontSize = 30,
    -- })
    -- loadingBarText:enableOutline(cc.c4b(30,61,119,255),2)


    processText = zc.createLabel({
        text = zc.lang("loader_UpdatingResourcePack", "正在为您更新资源包..."), -- 正在加载0/10（总共2.0M）
        color = cc.c3b(223, 196, 143),
        parent = loaderWidget,
        pos = cc.p(zc.cx, zc.cy - 80 - 170),
        fontSize = 20,
    })

    -- local defaultText = ""
    -- infoText = zc.createLabel({
    --     text = defaultText, -- 更新说明：优化游戏体验
    --     ap = cc.p(0, 1),
    --     pos = cc.p(100, 400),
    --     color = cc.c3b(223, 196, 143),
    --     parent = loaderWidget,
    --     fontSize = 22,
    --     size = cc.size(550, 0)
    -- })
    
end


function POP.createUI2()

    
end


function POP.setTitle(word)
    if word and titleText and tolua.isnull(titleText) == false then
        titleText:setString(word)
    end
end

function POP.setPercent(per, word)

    if loadingBarText and tolua.isnull(loadingBarText) == false  then
        local text = tostring(per) .. "%"
        loadingBarText:setString(text)
    end

    if loadingBar and tolua.isnull(loadingBar) == false then
        loadingBar:setPercent(per)
    end

    if word and processText and tolua.isnull(processText) == false then
        processText:setString(word)
    end
end

function POP.setUpdateInfo(word)
    if word and infoText and tolua.isnull(infoText) == false then
        -- infoText:setString(word)
        -- infoText:setTextAreaSize(cc.size(480,string.len(word)/25))

    end
end


---------------------------- 清除数据 -----------------------------
function POP.clean()

    titleText = nil
    loadingBar = nil
    loadingBarText = nil
    processText = nil
    infoText = nil

    local widget = ccui.Helper:seekWidgetByName(zc.getTouchLayer(), popName)
    if widget then
        widget:removeFromParent()
    end

end

return POP
