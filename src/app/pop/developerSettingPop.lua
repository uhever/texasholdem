--[[
    开发者设置
]]

-- 开发者工具全局变量
-- cc.UserDefault里保存的Key为 ["DEVELPOER_" + 全局变量名 ]
-- 1:全局变量名, 2:数据类型(number, string, bool, eval), 3:"中文描述"
local CFG = {

    {"UPDATE_DEBUG", "number", "UPDATE_DEBUG"},
    {"IS_TEST_SOCKET", "bool", "IS_TEST_SOCKET"},
    {"LOGIN_MODE", "number", "LOGIN_MODE"},
    {"CHANNELID", "number", "CHANNELID"},

}


local POP = {}

-- view
local popName = "src/app/pop/developerSettingPop"

-- 触摸层
local developerSettingWidget = nil

local popHeight = nil

-- 背景图
local bgView = nil


local listItemTb = {}

function POP.loadUserCFG(  )
    -- dump(CFG, "加载开发者工具")
    -- print("测试CHANNELID", cc.UserDefault:getInstance():getStringForKey("DEVELOPER_" .. "CHANNELID"))

    for i = 1, #CFG do
        if CFG[i][1] then
            -- 获取本地存储的字符串
            local str = cc.UserDefault:getInstance():getStringForKey("DEVELOPER_" .. CFG[i][1])

            if str and str ~= "" then -- 如果存在字符串

                package.loaded["_G"][CFG[i][1]] = POP.formatUserValue(str, CFG[i][2])


                if CFG[i][1] == "OPEN_GUIDE" then -- 引导特殊处理
                    if package.loaded["_G"][CFG[i][1]] == true then
                        zc.guide.open()
                    else
                        zc.guide.close()
                    end
                    
                end

            end
        end

    end

end


-- userData值转代码值
function POP.formatUserValue( userValue, mType )

    local formatStr = nil

    if mType == "number" then
        formatStr = tonumber(userValue)

    elseif mType == "bool" then
        if tonumber(userValue) == 0 then
            formatStr = false
        else
            formatStr = true
        end

    elseif mType == "string" then
        formatStr = tostring(userValue)

    end

    return formatStr

end


-- 代码值转userData值
function POP.formatCodeValue( codeValue, mType )
    local formatStr = nil

    if mType == "number" then
        formatStr = codeValue

    elseif mType == "bool" then
        if codeValue == false then
            formatStr = 0
        else
            formatStr = 1
        end

    elseif mType == "string" then
        formatStr = codeValue

    end

    return tostring(formatStr)
end


-- 格式化输入文本
function POP.formatInputStr( inputValue, mType )
    local formatStr = nil

    if mType == "number" then

        if type(tonumber(inputValue)) ~= "number" then
            formatStr = ""
        else
            formatStr = tonumber(inputValue)
        end

    elseif mType == "bool" then
        if tonumber(inputValue) == 0 then
            formatStr = 0
        else
            formatStr = 1
        end

    elseif mType == "string" then
        formatStr = inputValue

    end

    return tostring(formatStr)
end


function POP.create( params )

    POP.loadUserCFG(  )

    zc.addToActivePops(popName)

    POP.init(params or {})

end

function POP.init(params)
    -- 初始化数据
    developerSettingWidget, popHeight = zc.createMyPop(popName, {
        mask = true,
        })

    bgView = zc.createImageView({
        src = "res/Image/main/shopmini/shopmini_list_bg.png",
        pos = cc.p(zc.cx, zc.cy),
        parent = developerSettingWidget,
        })

    listItemTb = {}


    POP.createUI()
end



function POP.createUI()

    zc.createButton({
        normal = "res/Image/common/closeBtn.png",
        pressed = "res/Image/common/closeBtn.png",
        pos = cc.p(bgView:getContentSize().width - 30, bgView:getContentSize().height - 50),
        parent = bgView,
        listener = POP.closeCallback,
        })

    zc.createLabel({
         text = zc.lang("developerSetting_DevelopmentSettings", "开发设置") ,
         pos = cc.p(bgView:getContentSize().width / 2, bgView:getContentSize().height - 65),
         fontSize = 36,
         shadow = true,
         outline = true,
         parent = bgView,
         })


    local TextList = zc.createListView({
        inertiaScroll = true,
        bounce = true,
        size = cc.size(bgView:getContentSize().width-60, 520),
        pos = cc.p(30, 150),
        parent = bgView,
        })

    for i, v in pairs(CFG) do
        local cellLayout = zc.createLayout({
            tag = i,
            size = cc.size(bgView:getContentSize().width-60, 90),
            listener = POP.quickChatCallback
            })
        -- 描述Label
        local descLabel = zc.createLabel({
            text = v[3] .. ":", 
            ap = ccp(0, 0), 
            pos = ccp(30, 40), 
            parent = cellLayout,
            fontSize = 28,
            shadow = true,
            outline = true,
            })

        --edit Bg
        local editBg = zc.createImageView({
            src = "res/Image/common/edit_bg.png",
            pos = cc.p(cellLayout:getContentSize().width/4*3, 46),
            parent = cellLayout,
            scale9 = true,
            size = cc.size(cellLayout:getContentSize().width/2, 80),
        })

        local editValue = ""
        if v[1] then
            editValue = POP.formatCodeValue(package.loaded["_G"][v[1]], v[2])
        else
            local str = cc.UserDefault:getInstance():getStringForKey(v[3])
            if str and str ~= "" then -- 如果存在字符串
                editValue = str
            end
        end

        -- 输入框
        local editBox = zc.createEditBox( {
            bgSrc = "res/Image/common/edit_bg.png",
            pos = cc.p(cellLayout:getContentSize().width/4*3, 46),
            size = cc.size(cellLayout:getContentSize().width/2, 80),
            text = editValue,
            handler = POP.onEditHandler,
            fontSize = 24,
            tag = i,
            parent = cellLayout
            })

        editBox.beginStr = editValue -- 初始时候的文字

        cellLayout.editBox = editBox
        cellLayout.nameLabel = descLabel 
        zc.createImageView( {
            src = "res/Image/main/chat/chat_cut_line.png",
            pos = cc.p((bgView:getContentSize().width-60)/2, 1),
            parent = cellLayout
        } )


        TextList:pushBackCustomItem(cellLayout)
        table.insert(listItemTb, cellLayout)

    end


    -- 确认按钮
    zc.createButton({ 
        text = zc.lang("developerSetting_Reduction", "还原"), 
        pos = cc.p(bgView:getContentSize().width / 2-150, 105),
        listener = POP.onRevertCallback,
        parent = bgView,
        normal = "res/Image/common/green_ellipse_btn_n.png",
        pressed = "res/Image/common/green_ellipse_btn_p.png",
    })

    -- 确认按钮
    zc.createButton({ 
        text = zc.lang("developerSetting_Preservation", "保存"), 
        pos = cc.p(bgView:getContentSize().width / 2+150, 105),
        listener = POP.onClickConfirm,
        parent = bgView,
        normal = "res/Image/common/green_ellipse_btn_n.png",
        pressed = "res/Image/common/green_ellipse_btn_p.png",
    })

end



-- 编辑框回调
function POP.onEditHandler( eventType, editBox )
    if eventType == "ended" then
        -- 获取当前的tag
        local tag = editBox:getTag()
        local nowCfg = CFG[tag] -- 当前的配置

        local listItem = listItemTb[tag] -- 当前的cell

        local beginStr = editBox.beginStr -- 最开始的时候赋值

        local inputStr = editBox:getText()

        local finalStr = "" -- 最终

        if nowCfg[1] then
            finalStr = POP.formatInputStr(inputStr, nowCfg[2])
            if inputStr ~= "" and finalStr ~= "" then -- 如果输入不为空（编辑过）
                -- 把输入的东西规范
                editBox:setText(finalStr)
            else
                if nowCfg[2] ~= "string" then
                    editBox:setText(POP.formatCodeValue(package.loaded["_G"][nowCfg[1]], nowCfg[2])) -- 输入为空保存默认值
                end
            end
        else
            finalStr = inputStr
        end
        if finalStr ~= beginStr then
            listItem.nameLabel:setColor(cc.c3b(255,0,0))
        else
            listItem.nameLabel:setColor(cc.c3b(255,255,255))
        end
    end
end


-- 保存按钮
function POP.onClickConfirm( sender, eventType )
    if eventType == ccui.TouchEventType.ended then

        local devReginChange = false
        local channelChange = false

        for i,v in ipairs(CFG) do
            local beginStr = listItemTb[i].editBox.beginStr -- 开始时候的值
            local inputStr = listItemTb[i].editBox:getText()
            -- 规范保存值
            local formatStr = POP.formatInputStr(inputStr, v[2])
            if formatStr ~= beginStr then
                if CFG[i][3] == "devRegion" then
                    devReginChange = true
                elseif CFG[i][3] == "CHANNELID" then
                    channelChange = true
                end

                if v[1] then -- 有全局变量值的
                    cc.UserDefault:getInstance():setStringForKey("DEVELOPER_" .. CFG[i][1], formatStr)
                    
                else
                    cc.UserDefault:getInstance():setStringForKey(CFG[i][3], formatStr)
                    
                end
            end
        end

        if devReginChange == true then
            cc.UserDefault:getInstance():setStringForKey("DEVELOPER_" .. "CHANNELID", "")
        end

        if channelChange == true then
            cc.UserDefault:getInstance():setStringForKey("devRegion", "")
        end

        zc.logout(true)

    end
end


-- 还原
function POP.onRevertCallback( sender, eventType )
    if eventType == ccui.TouchEventType.ended then
        local okCallback = function (  )
            for i = 1, #CFG do
                if CFG[i][1] then
                    -- 初始化
                    cc.UserDefault:getInstance():setStringForKey("DEVELOPER_" .. CFG[i][1], "")
                    
                else
                    cc.UserDefault:getInstance():setStringForKey(CFG[i][3], "")
                    
                end
            end
            zc.logout(true)
        end
        zc.confirm({
            btnNum         = 2,
            text           = zc.lang("developerSetting_InitSetting", "确定要初始化配置?"),
            okCallback     = okCallback,
        })
    end
end

function POP.closeCallback(sender, eventType)
    if eventType == TOUCH_EVENT_ENDED then
        zc.closePop(popName)
    end
end

function POP.clean(  )
    if(developerSettingWidget ~= nil) then
        developerSettingWidget:removeFromParent(true)
        developerSettingWidget = nil
    end

    listItemTb = nil

    zc.removeFromActivePops(popName)

end

return POP