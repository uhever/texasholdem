--[[
	表情聊天
]]

local POP = {}

local popName = "src/app/pop/emotionPop"

local emotionWidget = nil

local bgImg = nil

local EMOTION_CONFIG_DELAY = {0,0,0.2,0,0.1,0,0.1,0.2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}


function POP.create( params )
	
	zc.addToActivePops(popName)

	emotionWidget = zc.createMyPop(popName, {
		opacity = 0,
		touchClose = true,
		})

	POP.init(params)
end

function POP.init( params )
	
	POP.createUI()
end

function POP.createUI(  )
	bgImg = zc.createImageView({
		src = "res/Image/main/emotion/emation_bg.png",
		ap = cc.p(0.5, 0),
		pos = cc.p(zc.cx, -500),
		touchEnabled = true,
		parent = emotionWidget,
		})

	local moveAct = cc.EaseExponentialOut:create(cc.MoveTo:create(0.3, cc.p(zc.cx, 0)))
	bgImg:runAction(moveAct)


	local emotionList = zc.createListView({
		-- mask = true,
		scrollBarDisEnabled = true,
		inertiaScroll = true,
		bounce = true,
		size = cc.size(bgImg:getContentSize().width, bgImg:getContentSize().height),
		ap = cc.p(0,0),
		pos = cc.p(0, 0),
		parent = bgImg,
		})


	local emotionNum = 24
	local colMaxNum = 6

	local rows = math.ceil(emotionNum / colMaxNum)

	for i = 1, rows do
		local cellLayout = zc.createLayout({			
			size = cc.size(bgImg:getContentSize().width, 85),
			})

		local colsNum = colMaxNum
		if emotionNum - (i-1) * colMaxNum < colMaxNum then
			colsNum = emotionNum - (i-1) * colMaxNum
		end

		for j = 1, colsNum do
			local idx = (i-1)*colMaxNum + j

			local emotionLayout = zc.createImageView({
				src = "res/Image/main/emotion/emotion_item_bg.png",
				ap = cc.p(0.5, 0.5),
				pos = cc.p(40 + (j-1) * 85, 40),
				-- size = cc.size(100,100),
				parent = cellLayout,
				tag = idx,
				listener = POP.emotionClickCallback,
				})

			local emotionEffect, emotionAnimation = display.getAnimation("emotion_" .. (idx - 1))
		   	emotionEffect:setPosition(cc.p(emotionLayout:getContentSize().width/2,emotionLayout:getContentSize().height/2))
		   	emotionEffect:setScale(1.3)
		   	emotionLayout:addChild(emotionEffect)
			display.playAnimationForever( emotionEffect,emotionAnimation, EMOTION_CONFIG_DELAY[idx])
		end


		emotionList:pushBackCustomItem(cellLayout)
		

	end
	
end



function POP.emotionClickCallback( sender, eventType )
	if eventType == ccui.TouchEventType.ended then

		local magicId = sender:getTag()

		zc.socket.sendTable({m = "send_magic", magic_id = magicId})
	end
end


function POP.closeCallback( sender, eventType )
	if eventType == ccui.TouchEventType.ended then

		local moveAct = cc.EaseExponentialIn:create(cc.MoveTo:create(0.2, cc.p(zc.cx, -500)))
		local callfunc = cc.CallFunc:create(function (  )
			zc.closePop(popName)
		end)
		bgImg:runAction(cc.Sequence:create({moveAct, callfunc}))
	end
end


function POP.clean(  )
	if emotionWidget then
		emotionWidget:removeFromParent()
		emotionWidget = nil
	end

	bgImg = nil

	zc.removeFromActivePops(popName)
end

return POP