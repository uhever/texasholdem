-- 产品BundleID
PROCUDT_ID     = "com.zhancheng.sanguo"
LANGUAGE       = "zh-cn"

-- 产品状态(主要控制Log)
APP_ENV        = "dev" -- dev/dist
-- 心跳检查时间间隔
HEARTBEAT_INTERVAL = 30
-- 聊天检查时间间隔（如果没有新聊天信息，下次自动延长5s，最高30s）
CHAT_INTERVAL = 5

if CC_SHOW_FPS then
    cc.Director:getInstance():setDisplayStats(true)
end

function G_setDebug(status)
    if(status and status == true) then
        APP_ENV = "dev"

        -- 测试状态下，聊天和心跳检查的时间都加长
        CHAT_INTERVAL = 30
        HEARTBEAT_INTERVAL = 60
    else
        APP_ENV = "dist"
    end
end

-- 1正式还是0测试服
APP_SERVER     = 0
-- 网络返回值是否加密
APP_CRYPTO = false
function G_setServer(s)
    if(s == 1) then
        APP_SERVER = 1
        APP_CRYPTO = true
    else
        APP_SERVER = 0
        APP_CRYPTO = false
    end
end

-- 下面是更新系统所需要的
UPDATE_OPEN    = false
UPDATE_DEBUG   = 0

function G_setHotUpdate(open, isAlpha)
    if(open and open == true) then
        UPDATE_OPEN = true
        if(isAlpha and isAlpha == true) then
            UPDATE_DEBUG = 1
        else
            UPDATE_DEBUG = 0
        end
    else
        UPDATE_OPEN = false
        UPDATE_DEBUG = 0
    end
end

function G_setHotUpdateAlpha(status)
    UPDATE_DEBUG = status
end


-- 模块更新开关
MODULE_UPDATE  = false
function G_setModuleUpdate(open)
    if(open and open == true) then
        MODULE_UPDATE = true
    else
        MODULE_UPDATE = false
    end
end


-- 当前游戏服务器API（登录后获取）
GAMEUID        = 0
-- 当前登录玩家的TOKEN
GAMETOKEN      = ""
-- 当前服务器ID
SERVERID       = "0"
-- 当前服务器名字
SERVERNAME     = ""
-- 当前服务器地址和端口列表
SERVERADDR     = ""
SERVERPORTS    = ""
SERVERTCPPORTS    = ""


-- 接入SDK的用户信息(未登录或登出后，值为nil)
-- ZCSDK
APP_SDK_INFO   = nil

-- 产品是否在审核
APP_REVIEW     = false

-- CMDKey 用于服务器MD5校验
CMDKEY = "kdhfwoeijksldofcqpcmzmqsdf"

-- 静态表版本(默认0)
LOCAL_STABLE_VERSION = "0"
-- 静态表列表(默认空数组)
LOCAL_STABLE_LIST = "[]"

-- 静态表的全局变量
DB_STATICTABLE = {}

-- 测试socket状态
IS_TEST_SOCKET = false

-- 客户端外网IP
IP_ADDRESS = "0.0.0.0"

-- 客户端地理位置
LOCATE_INFO = {}