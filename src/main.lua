--set FPS. the default value is 1.0/60 if you don't call this
cc.Director:getInstance():setAnimationInterval(1.0 / 60)


-- 全局命名空间
zc = {}

zc.loadedFiles = {}

function zc.require(luafile)
    if(table.inArray(luafile, zc.loadedFiles) == false) then
        
        table.insert(zc.loadedFiles, luafile)
    end

    return require(luafile)
    
end

APP_XXTEA_KEY  = "zctech1215poker"


-- 加载配置
require("src/baseConfig")
require("src/gameConfig")


cc.FileUtils:getInstance():setPopupNotify(false)

function zc.initSearchPath(  )
    -- 初始化配置
    local wp = cc.FileUtils:getInstance():getWritablePath()
    print("wp = ", wp)

    local searchPaths = {}
    table.insert(searchPaths, wp .. "download" .. VERSION_CACHE_ID .. "/")
    table.insert(searchPaths, wp .. "download" .. VERSION_CACHE_ID .. "/res/")
    table.insert(searchPaths, wp .. "download" .. VERSION_CACHE_ID .. "/src/")
    table.insert(searchPaths, "res/")
    table.insert(searchPaths, "src/")
    table.insert(searchPaths, wp)

    cc.FileUtils:getInstance():setSearchPaths(searchPaths)


end

zc.initSearchPath()


require("src/config")
require("src/cocos/init")
require("src/frameworkExtend")



--[[ API配置 ]]

-- 热更新系统API
UPDATE_API = "https://img-live.jjshowtime.com/index.php?c=index&m=update_world_common"

-- 热更新资源下载地址
CDN_URL    = "https://imgcdn-live.jjshowtime.com/"

-- 静态表API
STABLE_URL = ""
if(APP_SERVER == 1) then
    STABLE_URL = "https://cdn-texas.zhanchenggame.com/"
else
    STABLE_URL = "https://cdnt-poker.zhanchenggame.com/"
end

-- 用户中心API
UCAPI = ""
if(APP_SERVER == 1) then
    UCAPI = "https://uc-texas.zhanchenggame.com/index.php?"
else
    UCAPI = "https://uct-poker.zhanchenggame.com/index.php?"
    
end

-- 支付系统API
PAYAPI = ""
if(APP_SERVER == 1) then
    PAYAPI = "http://pay.texas.zhanchenggame.com/"
else
    PAYAPI = "http://36.110.19.12/pay.poker.zhanchenggame.com/"
end

-- 展程统计数据接口
ZC_TRACK_API = "https://ad-ucapi.zhanchenggame.com/index.php?"

--[[ API配置 end ]]



-- 加载语言包(如果后面有更新，则会在updateScene覆盖加载)
-- require("applang/language")


--[[
    method: doLogin
    paramsArray: {} -- 必须为数组
    sig: (Ljava/lang/String;)V
]]
function callJava(method, paramsArray, sig)

    LuaJavaBridge:callStaticMethod("com/zhanyouall/poker/AppController", method, paramsArray, sig)
end

--[[
    method: doLogin
    params: {} -- 字典类型
]]
function callOc(method, params)
    -- only for quick2
    if(table.nums(params) == 0) then
        params = { test = "yes" }
    end
    LuaObjcBridge.callStaticMethod("MyApi", method, params)
end



-- 屏幕真实尺寸配置
function zc.screenInit()
    local rw = 0
    local rh = 0
    local sx = 0
    local sy = 0

    -- 需要针对Pad加边
    if(IS_PAD_SIDERBAR) then
        if(CONFIG_SCREEN_ORIENTATION == 1) then -- 横屏，高度固定住，iPad上下加边
            rw = display.width
            rh = 640

            sy = (display.height - rh) / 2 -- 主容器起始y坐标
            if(sy < 0) then
                sy = 0
            end
        else -- 竖屏，宽度固定住，iPad左右加边
            rw = 640
            rh = display.height

            sx = (display.width - rw) / 2 -- 主容器起始x坐标
            if(sx < 0) then
                sx = 0
            end
        end
    else
        rw = display.width
        rh = display.height
    end

    zc.size               = { width = rw, height = rh }
    zc.width              = zc.size.width
    zc.height             = zc.size.height
    zc.sx                 = sx
    zc.sy                 = sy
    zc.cx                 = zc.width / 2
    zc.cy                 = zc.height / 2
    zc.left               = 0
    zc.right              = zc.width
    zc.top                = zc.height
    zc.bottom             = 0

    print(string.format("# zc.width                     = %0.2f", zc.width))
    print(string.format("# zc.height                    = %0.2f", zc.height))
    print(string.format("# zc.sx                        = %0.2f", zc.sx))
    print(string.format("# zc.sy                        = %0.2f", zc.sy))
end
zc.screenInit()
dump(zc)



local function main()

    collectgarbage("setpause", 100)
    collectgarbage("setstepmul", 1000)

    zc.require("src/initScene")

end

zc.require("src/mainSub")



-- for CCLuaEngine traceback
function __G__TRACKBACK__(msg)

    if(__G__TRACKBACK__Process ~= nil) then
        __G__TRACKBACK__Process(msg)
    end

end

-- 此函数如果有问题，可以在后面覆盖
function __G__TRACKBACK__Process(msg)

    -- 关闭loading界面、引导界面 防止卡死
    if(zc.hideLoading ~= nil) then
        zc.hideLoading()
    end

    print("----------------------------------------")
    print("LUA ERROR: " .. tostring(msg) .. "\n")
    print(debug.traceback())
    print("----------------------------------------")
    -- 出错界面
    showErrorLayer("----------------------------------------\n" .. "LUA ERROR: " .. tostring(msg) .. "\n" ..
        debug.traceback() .. "\n----------------------------------------")

end



local status, msg = xpcall(main, __G__TRACKBACK__)
if not status then
    print(msg)
end
