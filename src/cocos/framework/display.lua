--[[

Copyright (c) 2011-2014 chukong-inc.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

]]

local display = {}

local director = cc.Director:getInstance()
local view = director:getOpenGLView()

if CC_DISPLAY_ORIENTATION == "portrait" then
    CC_DESIGN_RESOLUTION = CC_DESIGN_RESOLUTION_PORTRAIT
else
    CC_DESIGN_RESOLUTION = CC_DESIGN_RESOLUTION_LANDSCAPE
end

if not view then
    local width = 960
    local height = 640
    if CC_DESIGN_RESOLUTION then
        if CC_DESIGN_RESOLUTION.width then
            width = CC_DESIGN_RESOLUTION.width
        end
        if CC_DESIGN_RESOLUTION.height then
            height = CC_DESIGN_RESOLUTION.height
        end
    end
    view = cc.GLViewImpl:createWithRect("Cocos2d-Lua", cc.rect(0, 0, width, height))
    director:setOpenGLView(view)
end

local framesize = view:getFrameSize()
local textureCache = director:getTextureCache()
local spriteFrameCache = cc.SpriteFrameCache:getInstance()
local animationCache = cc.AnimationCache:getInstance()

local frameSizeLandscape
local frameSizePortrait
if framesize.width > framesize.height then
    frameSizeLandscape = cc.size(framesize.width, framesize.height)
    frameSizePortrait = cc.size(framesize.height, framesize.width)
else
    frameSizeLandscape = cc.size(framesize.height, framesize.width)
    frameSizePortrait = cc.size(framesize.width, framesize.height)
end

-- auto scale
local function checkResolution(r)
    r.width = checknumber(r.width)
    r.height = checknumber(r.height)
    r.autoscale = string.upper(r.autoscale)
    assert(r.width > 0 and r.height > 0,
        string.format("display - invalid design resolution size %d, %d", r.width, r.height))
end

local function setDesignResolution(r, framesize)
    view:setFrameSize(framesize.width, framesize.height)

    if r.autoscale == "FILL_ALL" then
        view:setDesignResolutionSize(framesize.width, framesize.height, cc.ResolutionPolicy.FILL_ALL)
    else
        local scaleX, scaleY = framesize.width / r.width, framesize.height / r.height
        local width, height = framesize.width, framesize.height
        if r.autoscale == "FIXED_WIDTH" then
            width = framesize.width / scaleX
            height = framesize.height / scaleX
            view:setDesignResolutionSize(width, height, cc.ResolutionPolicy.NO_BORDER)
        elseif r.autoscale == "FIXED_HEIGHT" then
            width = framesize.width / scaleY
            height = framesize.height / scaleY
            view:setDesignResolutionSize(width, height, cc.ResolutionPolicy.NO_BORDER)
        elseif r.autoscale == "EXACT_FIT" then
            view:setDesignResolutionSize(r.width, r.height, cc.ResolutionPolicy.EXACT_FIT)
        elseif r.autoscale == "NO_BORDER" then
            view:setDesignResolutionSize(r.width, r.height, cc.ResolutionPolicy.NO_BORDER)
        elseif r.autoscale == "SHOW_ALL" then
            view:setDesignResolutionSize(r.width, r.height, cc.ResolutionPolicy.SHOW_ALL)
        else
            printError(string.format("display - invalid r.autoscale \"%s\"", r.autoscale))
        end
    end
end

local function setConstants()
    local sizeInPixels = view:getFrameSize()
    display.sizeInPixels = {width = sizeInPixels.width, height = sizeInPixels.height}

    local viewsize = director:getWinSize()
    display.contentScaleFactor = director:getContentScaleFactor()
    display.size               = {width = viewsize.width, height = viewsize.height}
    display.width              = display.size.width
    display.height             = display.size.height
    display.cx                 = display.width / 2
    display.cy                 = display.height / 2
    display.c_left             = -display.width / 2
    display.c_right            = display.width / 2
    display.c_top              = display.height / 2
    display.c_bottom           = -display.height / 2
    display.left               = 0
    display.right              = display.width
    display.top                = display.height
    display.bottom             = 0
    display.center             = cc.p(display.cx, display.cy)
    display.left_top           = cc.p(display.left, display.top)
    display.left_bottom        = cc.p(display.left, display.bottom)
    display.left_center        = cc.p(display.left, display.cy)
    display.right_top          = cc.p(display.right, display.top)
    display.right_bottom       = cc.p(display.right, display.bottom)
    display.right_center       = cc.p(display.right, display.cy)
    display.top_center         = cc.p(display.cx, display.top)
    display.top_bottom         = cc.p(display.cx, display.bottom)

    printInfo(string.format("# display.sizeInPixels         = {width = %0.2f, height = %0.2f}", display.sizeInPixels.width, display.sizeInPixels.height))
    printInfo(string.format("# display.size                 = {width = %0.2f, height = %0.2f}", display.size.width, display.size.height))
    printInfo(string.format("# display.contentScaleFactor   = %0.2f", display.contentScaleFactor))
    printInfo(string.format("# display.width                = %0.2f", display.width))
    printInfo(string.format("# display.height               = %0.2f", display.height))
    printInfo(string.format("# display.cx                   = %0.2f", display.cx))
    printInfo(string.format("# display.cy                   = %0.2f", display.cy))
    printInfo(string.format("# display.left                 = %0.2f", display.left))
    printInfo(string.format("# display.right                = %0.2f", display.right))
    printInfo(string.format("# display.top                  = %0.2f", display.top))
    printInfo(string.format("# display.bottom               = %0.2f", display.bottom))
    printInfo(string.format("# display.c_left               = %0.2f", display.c_left))
    printInfo(string.format("# display.c_right              = %0.2f", display.c_right))
    printInfo(string.format("# display.c_top                = %0.2f", display.c_top))
    printInfo(string.format("# display.c_bottom             = %0.2f", display.c_bottom))
    printInfo(string.format("# display.center               = {x = %0.2f, y = %0.2f}", display.center.x, display.center.y))
    printInfo(string.format("# display.left_top             = {x = %0.2f, y = %0.2f}", display.left_top.x, display.left_top.y))
    printInfo(string.format("# display.left_bottom          = {x = %0.2f, y = %0.2f}", display.left_bottom.x, display.left_bottom.y))
    printInfo(string.format("# display.left_center          = {x = %0.2f, y = %0.2f}", display.left_center.x, display.left_center.y))
    printInfo(string.format("# display.right_top            = {x = %0.2f, y = %0.2f}", display.right_top.x, display.right_top.y))
    printInfo(string.format("# display.right_bottom         = {x = %0.2f, y = %0.2f}", display.right_bottom.x, display.right_bottom.y))
    printInfo(string.format("# display.right_center         = {x = %0.2f, y = %0.2f}", display.right_center.x, display.right_center.y))
    printInfo(string.format("# display.top_center           = {x = %0.2f, y = %0.2f}", display.top_center.x, display.top_center.y))
    printInfo(string.format("# display.top_bottom           = {x = %0.2f, y = %0.2f}", display.top_bottom.x, display.top_bottom.y))
    printInfo("#")
end

function display.setAutoScale(configs, framesize)
    if type(configs) ~= "table" then return end

    checkResolution(configs)
    if type(configs.callback) == "function" then
        local c = configs.callback(framesize)
        for k, v in pairs(c or {}) do
            configs[k] = v
        end
        checkResolution(configs)
    end

    setDesignResolution(configs, framesize)

    printInfo(string.format("# design resolution size       = {width = %0.2f, height = %0.2f}", configs.width, configs.height))
    printInfo(string.format("# design resolution autoscale  = %s", configs.autoscale))
    setConstants()
end

if type(CC_DESIGN_RESOLUTION) == "table" then
    display.setAutoScale(CC_DESIGN_RESOLUTION, framesize)
end

display.curOrentation = CC_DISPLAY_ORIENTATION
function display.setOrientation( orientation )
    if orientation == "portrait" then
        display.curOrentation = "portrait"

        if device.platform == "android" then
            callJava("portraitAction", {}, "()V")
        else
            callOc("portraitAction", {})
        end
        
        display.setAutoScale(CC_DESIGN_RESOLUTION_PORTRAIT, frameSizePortrait)
    else
        display.curOrentation = "landscape"

        if device.platform == "android" then
            callJava("landscapAction", {}, "()V")
        else
            callOc("landscapAction", {})
        end
        display.setAutoScale(CC_DESIGN_RESOLUTION_LANDSCAPE, frameSizeLandscape)
    end

    zc.screenInit()
end

function display.getOrientation()
    return display.curOrentation
end

display.COLOR_WHITE = cc.c3b(255, 255, 255)
display.COLOR_BLACK = cc.c3b(0, 0, 0)
display.COLOR_RED   = cc.c3b(255, 0, 0)
display.COLOR_GREEN = cc.c3b(0, 255, 0)
display.COLOR_BLUE  = cc.c3b(0, 0, 255)

display.COLOR_GOLD  = cc.c3b(255, 228, 0)

display.AUTO_SIZE      = 0
display.FIXED_SIZE     = 1
display.LEFT_TO_RIGHT  = 0
display.RIGHT_TO_LEFT  = 1
display.TOP_TO_BOTTOM  = 2
display.BOTTOM_TO_TOP  = 3

display.CENTER        = cc.p(0.5, 0.5)
display.LEFT_TOP      = cc.p(0, 1)
display.LEFT_BOTTOM   = cc.p(0, 0)
display.LEFT_CENTER   = cc.p(0, 0.5)
display.RIGHT_TOP     = cc.p(1, 1)
display.RIGHT_BOTTOM  = cc.p(1, 0)
display.RIGHT_CENTER  = cc.p(1, 0.5)
display.CENTER_TOP    = cc.p(0.5, 1)
display.CENTER_BOTTOM = cc.p(0.5, 0)

display.SCENE_TRANSITIONS = {
    CROSSFADE       = {cc.TransitionCrossFade},
    FADE            = {cc.TransitionFade, cc.c3b(0, 0, 0)},
    FADEBL          = {cc.TransitionFadeBL},
    FADEDOWN        = {cc.TransitionFadeDown},
    FADETR          = {cc.TransitionFadeTR},
    FADEUP          = {cc.TransitionFadeUp},
    FLIPANGULAR     = {cc.TransitionFlipAngular, cc.TRANSITION_ORIENTATION_LEFT_OVER},
    FLIPX           = {cc.TransitionFlipX, cc.TRANSITION_ORIENTATION_LEFT_OVER},
    FLIPY           = {cc.TransitionFlipY, cc.TRANSITION_ORIENTATION_UP_OVER},
    JUMPZOOM        = {cc.TransitionJumpZoom},
    MOVEINB         = {cc.TransitionMoveInB},
    MOVEINL         = {cc.TransitionMoveInL},
    MOVEINR         = {cc.TransitionMoveInR},
    MOVEINT         = {cc.TransitionMoveInT},
    PAGETURN        = {cc.TransitionPageTurn, false},
    ROTOZOOM        = {cc.TransitionRotoZoom},
    SHRINKGROW      = {cc.TransitionShrinkGrow},
    SLIDEINB        = {cc.TransitionSlideInB},
    SLIDEINL        = {cc.TransitionSlideInL},
    SLIDEINR        = {cc.TransitionSlideInR},
    SLIDEINT        = {cc.TransitionSlideInT},
    SPLITCOLS       = {cc.TransitionSplitCols},
    SPLITROWS       = {cc.TransitionSplitRows},
    TURNOFFTILES    = {cc.TransitionTurnOffTiles},
    ZOOMFLIPANGULAR = {cc.TransitionZoomFlipAngular},
    ZOOMFLIPX       = {cc.TransitionZoomFlipX, cc.TRANSITION_ORIENTATION_LEFT_OVER},
    ZOOMFLIPY       = {cc.TransitionZoomFlipY, cc.TRANSITION_ORIENTATION_UP_OVER},
}

display.TEXTURES_PIXEL_FORMAT = {}

display.DEFAULT_TTF_FONT        = "Arial"
display.DEFAULT_TTF_FONT_SIZE   = 32


local PARAMS_EMPTY = {}
local RECT_ZERO = cc.rect(0, 0, 0, 0)

local sceneIndex = 0
function display.newScene(name, params)
    params = params or PARAMS_EMPTY
    sceneIndex = sceneIndex + 1
    local scene
    if not params.physics then
        scene = cc.Scene:create()
    else
        scene = cc.Scene:createWithPhysics()
    end
    scene.name_ = string.format("%s:%d", name or "<unknown-scene>", sceneIndex)

    if params.transition then
        scene = display.wrapSceneWithTransition(scene, params.transition, params.time, params.more)
    end

    return scene
end

function display.wrapScene(scene, transition, time, more)
    local key = string.upper(tostring(transition))

    if key == "RANDOM" then
        local keys = table.keys(display.SCENE_TRANSITIONS)
        key = keys[math.random(1, #keys)]
    end

    if display.SCENE_TRANSITIONS[key] then
        local t = display.SCENE_TRANSITIONS[key]
        local cls = t[1]
        time = time or 0.2
        more = more or t[2]
        if more ~= nil then
            scene = cls:create(time, scene, more)
        else
            scene = cls:create(time, scene)
        end
    else
        error(string.format("display.wrapScene() - invalid transition %s", tostring(transition)))
    end
    return scene
end

function display.runScene(newScene, transition, time, more)
    if director:getRunningScene() then
        if transition then
            newScene = display.wrapScene(newScene, transition, time, more)
        end
        director:replaceScene(newScene)
    else
        director:runWithScene(newScene)
    end
end

function display.getRunningScene()
    return director:getRunningScene()
end

function display.newNode()
    return cc.Node:create()
end

function display.newLayer(...)
    local params = {...}
    local c = #params
    local layer
    if c == 0 then
        -- /** creates a fullscreen black layer */
        -- static Layer *create();
        layer = cc.Layer:create()
    elseif c == 1 then
        -- /** creates a Layer with color. Width and height are the window size. */
        -- static LayerColor * create(const Color4B& color);
        layer = cc.LayerColor:create(cc.convertColor(params[1], "4b"))
    elseif c == 2 then
        -- /** creates a Layer with color, width and height in Points */
        -- static LayerColor * create(const Color4B& color, const Size& size);
        --
        -- /** Creates a full-screen Layer with a gradient between start and end. */
        -- static LayerGradient* create(const Color4B& start, const Color4B& end);
        local color1 = cc.convertColor(params[1], "4b")
        local p2 = params[2]
        assert(type(p2) == "table" and (p2.width or p2.r), "display.newLayer() - invalid paramerter 2")
        if p2.r then
            layer = cc.LayerGradient:create(color1, cc.convertColor(p2, "4b"))
        else
            layer = cc.LayerColor:create(color1, p2.width, p2.height)
        end
    elseif c == 3 then
        -- /** creates a Layer with color, width and height in Points */
        -- static LayerColor * create(const Color4B& color, GLfloat width, GLfloat height);
        --
        -- /** Creates a full-screen Layer with a gradient between start and end in the direction of v. */
        -- static LayerGradient* create(const Color4B& start, const Color4B& end, const Vec2& v);
        local color1 = cc.convertColor(params[1], "4b")
        local p2 = params[2]
        local p2type = type(p2)
        if p2type == "table" then
            layer = cc.LayerGradient:create(color1, cc.convertColor(p2, "4b"), params[3])
        else
            layer = cc.LayerColor:create(color1, p2, params[3])
        end
    end
    return layer
end

function display.newSprite(source, x, y, params)
    local spriteClass = cc.Sprite
    local scale9 = false

    if type(x) == "table" and not x.x then
        -- x is params
        params = x
        x = nil
        y = nil
    end

    local params = params or PARAMS_EMPTY
    if params.scale9 or params.capInsets then
        spriteClass = ccui.Scale9Sprite
        scale9 = true
        params.capInsets = params.capInsets or RECT_ZERO
        params.rect = params.rect or RECT_ZERO
    end

    local sprite
    while true do
        -- create sprite
        if not source then
            sprite = spriteClass:create()
            break
        end

        local sourceType = type(source)
        if sourceType == "string" then
            if string.byte(source) == 35 then -- first char is #
                -- create sprite from spriteFrame
                if not scale9 then
                    sprite = spriteClass:createWithSpriteFrameName(string.sub(source, 2))
                else
                    sprite = spriteClass:createWithSpriteFrameName(string.sub(source, 2), params.capInsets)
                end
                break
            end

            -- create sprite from image file
            if display.TEXTURES_PIXEL_FORMAT[source] then
                cc.Texture2D:setDefaultAlphaPixelFormat(display.TEXTURES_PIXEL_FORMAT[source])
            end
            if not scale9 then
                sprite = spriteClass:create(source)
            else
                sprite = spriteClass:create(source, params.rect, params.capInsets)
            end
            if display.TEXTURES_PIXEL_FORMAT[source] then
                cc.Texture2D:setDefaultAlphaPixelFormat(cc.TEXTURE2_D_PIXEL_FORMAT_BGR_A8888)
            end
            break
        elseif sourceType ~= "userdata" then
            error(string.format("display.newSprite() - invalid source type \"%s\"", sourceType), 0)
        else
            sourceType = tolua.type(source)
            if sourceType == "cc.SpriteFrame" then
                if not scale9 then
                    sprite = spriteClass:createWithSpriteFrame(source)
                else
                    sprite = spriteClass:createWithSpriteFrame(source, params.capInsets)
                end
            elseif sourceType == "cc.Texture2D" then
                sprite = spriteClass:createWithTexture(source)
            else
                error(string.format("display.newSprite() - invalid source type \"%s\"", sourceType), 0)
            end
        end
        break
    end

    if sprite then
        if x and y then sprite:setPosition(x, y) end
        if params.size then sprite:setContentSize(params.size) end
    else
        error(string.format("display.newSprite() - create sprite failure, source \"%s\"", tostring(source)), 0)
    end

    return sprite
end

function display.newSpriteFrame(source, ...)
    local frame
    if type(source) == "string" then
        if string.byte(source) == 35 then -- first char is #
            source = string.sub(source, 2)
        end
        frame = spriteFrameCache:getSpriteFrame(source)
        if not frame then
            error(string.format("display.newSpriteFrame() - invalid frame name \"%s\"", tostring(source)), 0)
        end
    elseif tolua.type(source) == "cc.Texture2D" then
        frame = cc.SpriteFrame:createWithTexture(source, ...)
    else
        error("display.newSpriteFrame() - invalid parameters", 0)
    end
    return frame
end

function display.newFrames(pattern, begin, length, isReversed)
    local frames = {}
    local step = 1
    local last = begin + length - 1
    if isReversed then
        last, begin = begin, last
        step = -1
    end

    for index = begin, last, step do
        local frameName = string.format(pattern, index)
        local frame = spriteFrameCache:getSpriteFrame(frameName)
        if not frame then
            error(string.format("display.newFrames() - invalid frame name %s", tostring(frameName)), 0)
        end
        frames[#frames + 1] = frame
    end
    return frames
end

local function newAnimation(frames, time)
    local count = #frames
    assert(count > 0, "display.newAnimation() - invalid frames")
    time = time or 1.0 / count
    return cc.Animation:createWithSpriteFrames(frames, time),
           cc.Sprite:createWithSpriteFrame(frames[1])
end

function display.newAnimation(...)
    local params = {...}
    local c = #params
    if c == 2 then
        -- frames, time
        return newAnimation(params[1], params[2])
    elseif c == 4 then
        -- pattern, begin, length, time
        local frames = display.newFrames(params[1], params[2], params[3])
        return newAnimation(frames, params[4])
    elseif c == 5 then
        -- pattern, begin, length, isReversed, time
        local frames = display.newFrames(params[1], params[2], params[3], params[4])
        return newAnimation(frames, params[5])
    else
        error("display.newAnimation() - invalid parameters")
    end
end

function display.loadImage(imageFilename, callback)
    if not callback then
        return textureCache:addImage(imageFilename)
    else
        textureCache:addImageAsync(imageFilename, callback)
    end
end

local fileUtils = cc.FileUtils:getInstance()
function display.getImage(imageFilename)
    local fullpath = fileUtils:fullPathForFilename(imageFilename)
    return textureCache:getTextureForKey(fullpath)
end

function display.removeImage(imageFilename)
    textureCache:removeTextureForKey(imageFilename)
end

function display.loadSpriteFrames(dataFilename, imageFilename, callback)
    if display.TEXTURES_PIXEL_FORMAT[imageFilename] then
        cc.Texture2D:setDefaultAlphaPixelFormat(display.TEXTURES_PIXEL_FORMAT[imageFilename])
    end
    if not callback then
        spriteFrameCache:addSpriteFrames(dataFilename, imageFilename)
    else
        spriteFrameCache:addSpriteFramesAsync(dataFilename, imageFilename, callback)
    end
    if display.TEXTURES_PIXEL_FORMAT[imageFilename] then
        cc.Texture2D:setDefaultAlphaPixelFormat(cc.TEXTURE2_D_PIXEL_FORMAT_BGR_A8888)
    end
end

function display.removeSpriteFrames(dataFilename, imageFilename)
    spriteFrameCache:removeSpriteFramesFromFile(dataFilename)
    if imageFilename then
        display.removeImage(imageFilename)
    end
end

function display.removeSpriteFrame(imageFilename)
    spriteFrameCache:removeSpriteFrameByName(imageFilename)
end

function display.setTexturePixelFormat(imageFilename, format)
    display.TEXTURES_PIXEL_FORMAT[imageFilename] = format
end

function display.setAnimationCache(name, animation)
    animationCache:addAnimation(animation, name)
end

function display.getAnimationCache(name)
    return animationCache:getAnimation(name)
end

function display.removeAnimationCache(name)
    animationCache:removeAnimation(name)
end

function display.removeUnusedSpriteFrames()
    spriteFrameCache:removeUnusedSpriteFrames()
    textureCache:removeUnusedTextures()
end

function display.loadAnim(path,name,indexA,indexB,d)
    local delay = d or 0.1

    if name ~= nil and path ~= nil then
        local frames = {}
        spriteFrameCache:addSpriteFrames(path .. name .. ".plist", path .. name .. ".png")
        for j = indexA,indexB do
            if spriteFrameCache:getSpriteFrame(name.."_"..tostring(j)..".png") then
                frames[#frames + 1] = spriteFrameCache:getSpriteFrame(name.."_"..tostring(j)..".png")
            end
        end
        local newAnimation = cc.Animation:createWithSpriteFrames(frames, delay)
        animationCache:addAnimation(newAnimation, name)
    end
end

function display.getAnimation(name, scale, addType)
    local imageType = ".png"
  
    -- 判断如果有压缩版 优先使用压缩版
    local sprite = nil
    local animation = animationCache:getAnimation(name)
    if animation then
        sprite = cc.Sprite:createWithSpriteFrameName(name.."_1"..imageType)
        if addType then
            local blf = cc.blendFunc()
            blf.src = GL_ONE
            blf.dst = GL_ONE
            sprite:setBlendFunc(blf)
        end

        if scale then
            if scale ~= 0 then
                sprite:setScale(scale)
            end
        else
            sprite:setScale(1)
        end
    else
        return nil, nil
    end


    return sprite,animation
end

function display.unloadAnim(path, name, part, imgType)
    local imageType = ".png"

    if path ~= nil and name ~= nil then
        animationCache:removeAnimation(name)
        if part ~= nil then
            for i = 1,part do
                spriteFrameCache:removeSpriteFramesFromFile(path..name..i..".plist")
                cc.Director:getInstance():getTextureCache():dumpCachedTextureInfo()
                cc.Director:getInstance():getTextureCache():removeTextureForKey(path..name..i..imageType)
            end
        else
            animationCache:removeAnimation(name)
            spriteFrameCache:removeSpriteFramesFromFile(path..name..".plist")
            cc.Director:getInstance():getTextureCache():removeTextureForKey(path..name..imageType)
        end
    end
end

--[[
    持久播放序列帧动画
    target: 需要播放动画的对象
    animation: 动作对象
    delay : 播放前延迟时间
]]
function display.playAnimationForever( target, animation, delay )
    local animate = cc.Animate:create( animation )
    local action
    if type(delay) == "number" and delay > 0 then
        target:setVisible(false)
        -- local sequence = display.sequence({
        --     cc.DelayTime:create(delay),
        --     cc.Show:create(),
        --     animation })
        local array = {}
        array[#array + 1] = cc.DelayTime:create(delay)
        array[#array + 1] = cc.Show:create()
        array[#array + 1] =  animate 
        local sequence = cc.Sequence:create(array)
        action = cc.RepeatForever:create(sequence)
    else
        action = cc.RepeatForever:create(animate)
    end
    target:runAction(action)
    return action
end

function display.playAnimationOnce( target, animation, callBack, delay, isBlinkCallBack )
    local array   = {}
    local animate = cc.Animate:create( animation )
    -- 判断是否有间隔时间
    if "number" == type( delay ) and delay > 0 then
        target:setVisible( false )
        array[#array+1] = cc.DelayTime:create( delay )
        array[#array+1] =  cc.Show:create() 
    end
    -- 添加动画
    array[#array+1] = animate 
    -- 判断闪烁
    if isBlinkCallBack then
        array[#array+1] = cc.CallFunc:create(isBlinkCallBack)
    end
      -- 判断回调
    if callBack then
        array[#array+1] =  cc.CallFunc:create(callBack)
    end
    local action =  cc.Sequence:create( array )
    target:runAction(action)
end
function display.playAnimationOnceAndCleanup( target, parent, animation, callBack, delay )
    local array   = {}
    local animate = cc.Animate:create( animation )
    -- 判断是否有间隔时间
    if "number" == type( delay ) and delay > 0 then
        target:setVisible( false )
        array[#array+1] = cc.DelayTime:create( delay )
        array[#array+1] = cc.Show:create()
    end
    -- 添加动画
    array[#array+1] = animate 
      -- 判断回调
    if callBack then
        array[#array+1] = cc.CallFunc:create(callBack)
    end
    array[#array+1] = cc.CallFunc:create(function (sender)
        sender:removeFromParent()
    end)
    local action = cc.Sequence:create( array )
    target:runAction(action)
end


function display.calcStrSize(text, fontName, fontSize)

    text = string.gsub(text, "​", "")

    local sizeStr = ZCTool:getInstance():calcStrSize(text, fontName, fontSize)
    local sizeArr = string.split(sizeStr, ",")
    local tmpLabelWidth = tonumber(sizeArr[1])
    local tmpLabelHeight = tonumber(sizeArr[2])

    return tmpLabelWidth, tmpLabelHeight
end

function display.Rich(config, data)
    -- 总容器
    local RichLayout = ccui.Layout:create()
    RichLayout:setTouchEnabled(true)
    RichLayout:setContentSize(cc.size(config.width or 640, config.height or 0))
    RichLayout:setContentSize(cc.size(config.width or 640, config.height or 0))

    -- RichLayout:setBackGroundColorType(LAYOUT_COLOR_SOLID)
 --    RichLayout:setBackGroundColor(display.COLOR_BLUE)
 --    RichLayout:setBackGroundColorOpacity(100)
    -- 是否按实际内容宽度（内容只有一行时有效）
    local realWidth = false
    if config.realWidth then
        realWidth = true
    end
    -- 行间距
    local lineSpacing = 0
    if config.lineSpacing then
        lineSpacing = tonumber(config.lineSpacing)
    end
    -- 当前已经累计的宽度，如果超过则换行
    local nowWidth = 0
    -- 行数
    local lineCount = 1
    -- 行对象
    local richList = {}
    -- 每行最高高度
    local richHeightList = {}
    for i = 1, #data do
        local labelCfg = {
                -- width = config.width,
                -- height = config.height,

                tag = data[i].tag or 1,
                name = data[i].name or "",
                -- 文本相关
                fontSize = data[i].fontSize or config.fontSize or 20,
                fontName = data[i].fontName or config.fontName or display.FONT1,
                color = data[i].color or display.COLOR_WHITE,
                color2 = data[i].color2 or display.COLOR_BLACK,
                numVcenterFixY = data[i].numVcenterFixY, -- TODO
                -- 图片相关
                img = data[i].img or "",
                plist = data[i].plist or 0,
                scale = data[i].scale or 1,
                vcenter = data[i].vcenter or false,-- 让图片在本行垂直居中
                vcenterFixY = data[i].vcenterFixY , --TODO

                -- 其他节点相关
                node = data[i].node,

                -- 事件相关
                click = data[i].click or false,
                callback = data[i].callback or nil,
                actionTag = data[i].actionTag or 0
        }
        -- 文本
        if(labelCfg.tag == 1) then
            local labelText = data[i].text
            -- 递归每个节点，判断是否超过了
            local function dealText(myText)
                -- 判断当前节点是否已经超过了，否则就拆分
                local tmpLabelWidth, tmpLabelHeight = display.calcStrSize(myText, labelCfg.fontName, labelCfg.fontSize)

                if tmpLabelWidth == nil and tmpLabelHeight == nil then
                    data[i].text = " "
                    myText = " "
                    tmpLabelWidth = 1
                    tmpLabelHeight = 1
                end

                -- local tmpLabelWidth = tmpLabelSize.width
                -- local tmpLabelHeight = tmpLabelSize.height
                if(nowWidth + tmpLabelWidth > config.width) then -- 超宽
                    local labelText1, labelText2 = display.cutText(myText, labelCfg, config.width, config.width - nowWidth)
                    -- 第1行
                    if(richList[lineCount] == nil) then
                        richList[lineCount] = {}
                        richHeightList[lineCount] = 0
                    end
                    table.insert(richList[lineCount], {
                            labelCfg = table.copy(labelCfg),
                            text = labelText1
                        })
                    -- 保存本列最高高度
                    if(tmpLabelHeight > richHeightList[lineCount]) then
                        richHeightList[lineCount] = tmpLabelHeight
                    end
                    -- 第2行
                    lineCount = lineCount + 1
                    nowWidth = 0
                    -- 继续递归处理
                    dealText(labelText2)
                else --添加到当前行
                    if(richList[lineCount] == nil) then
                        richList[lineCount] = {}
                        richHeightList[lineCount] = 0
                    end
                    table.insert(richList[lineCount], {
                            labelCfg = table.copy(labelCfg),
                            text = (myText)
                        })
                    nowWidth = nowWidth + tmpLabelWidth
                    -- 保存本列最高高度
                    if(tmpLabelHeight > richHeightList[lineCount]) then
                        richHeightList[lineCount] = tmpLabelHeight
                    end
                end
                -- tmpLabel:release()
            end
            dealText(labelText)
        elseif(labelCfg.tag == 3) then -- 图片
            local tmpImg, imgWidth, imgHeight = display.newRichImg(labelCfg)
            -- print("labelCfg.plist", labelCfg.plist)
            --  tmpImg:release()
            if(nowWidth + imgWidth > config.width) then -- 超宽，转到下一行
                -- 第2行
                lineCount = lineCount + 1
                nowWidth = 0
            end
            if(richList[lineCount] == nil) then
                richList[lineCount] = {}
                richHeightList[lineCount] = 0
            end
            labelCfg.tmpImg = tmpImg
            table.insert(richList[lineCount], {
                    labelCfg = table.copy(labelCfg)
                })
            nowWidth = nowWidth + imgWidth
            -- 保存本列最高高度(如果设置了垂直居中，那么忽略本图片的高度)
            if(labelCfg.vcenter == false) then
                if(imgHeight > richHeightList[lineCount]) then
                    richHeightList[lineCount] = imgHeight
                end
            end
        elseif(labelCfg.tag == 4) then -- 适配任意node
            local node = labelCfg.node

            local imgSize = node:getSize()
            local imgWidth = imgSize.width
            local imgHeight = imgSize.height
          --  tmpImg:release()
            if(nowWidth + imgWidth > config.width) then -- 超宽，转到下一行
                -- 第2行
                lineCount = lineCount + 1
                nowWidth = 0
            end
            if(richList[lineCount] == nil) then
                richList[lineCount] = {}
                richHeightList[lineCount] = 0
            end
            table.insert(richList[lineCount], {
                    labelCfg = table.copy(labelCfg)
                })
            nowWidth = nowWidth + imgWidth
            -- 保存本列最高高度(如果设置了垂直居中，那么忽略本图片的高度)
            if(labelCfg.vcenter == false) then
                if(imgHeight > richHeightList[lineCount]) then
                    richHeightList[lineCount] = imgHeight
                end
            end
        elseif(labelCfg.tag == 5) then -- 换行
            lineCount = lineCount + 1
            nowWidth = 0
            if(richList[lineCount] == nil) then
                richList[lineCount] = {}
                richHeightList[lineCount] = 0
            end
        end
    end
    local nowX = 0
    local nowY = 0
    for lineId = 1, #richList do
        -- X轴初始化
        nowX = 0
        local lineHeight = richHeightList[lineId] -- 本行最高高度
        -- 行高
        if(lineSpacing > 0) then
            lineHeight = lineHeight + lineSpacing
        end
        nowY = nowY + lineHeight
        for i = 1, #richList[lineId] do
            if nowY <= config.height or config.height == 0 then
                local now = richList[lineId][i]
                if(now.labelCfg.tag == 1) then
                    local vcenterFixY = 0
                    if  now.labelCfg.numVcenterFixY then
                        vcenterFixY = tonumber(now.labelCfg.numVcenterFixY)
                    end
                    local node, nodeWidth, nodeHeight = display.newRichLabel(now.text, now.labelCfg)
                    node:setPosition(cc.p(nowX, config.height - nowY + vcenterFixY))
                    -- node:setPosition(cc.p(nowX, config.height - (lineId - 1) * 30))
                    RichLayout:addChild(node)
                    nowX = nowX + nodeWidth
                elseif(now.labelCfg.tag == 3) then
                    local node, nodeWidth, nodeHeight
                    if now.labelCfg.tmpImg then
                        node = now.labelCfg.tmpImg
                        nodeWidth = node:getContentSize().width*now.labelCfg.scale
                        nodeHeight = node:getContentSize().height*now.labelCfg.scale
                    else
                        node, nodeWidth, nodeHeight = display.newRichImg(now.labelCfg)
                    end
                    local vcenterFixY = 0
                    if now.labelCfg.vcenterFixY then
                        vcenterFixY = tonumber(now.labelCfg.vcenterFixY)
                    end
                    if(now.labelCfg.vcenter == true) then
                        local oy = (lineHeight - nodeHeight) / 2
                        node:setPosition(cc.p(nowX, config.height - (nowY - oy - vcenterFixY)))
                    else
                        node:setPosition(cc.p(nowX, config.height - nowY + vcenterFixY))
                    end
                    
                    RichLayout:addChild(node)
                    nowX = nowX + nodeWidth
                elseif(now.labelCfg.tag == 4) then
                    local node = now.labelCfg.node
                    local ap = node:getAnchorPoint()
                    local imgSize = node:getSize()
                    if now.labelCfg.click then
                        node:setTouchEnabled(true)
                        node:setActionTag(now.labelCfg.actionTag)
                        node:addTouchEventListener(now.labelCfg.callback)
                    end
                    local nodeWidth, nodeHeight = imgSize.width, imgSize.height
                    local vcenterFixY = 0
                    if now.labelCfg.vcenterFixY then
                        vcenterFixY = tonumber(now.labelCfg.vcenterFixY)
                    end
                    if(now.labelCfg.vcenter == true) then
                        local oy = (lineHeight - nodeHeight) / 2
                        node:setPosition(cc.p(nowX + ap.x * nodeWidth, config.height + ap.y * nodeHeight - (nowY - oy - vcenterFixY)))
                    else
                        node:setPosition(cc.p(nowX + ap.x * nodeWidth, config.height + ap.y * nodeHeight - nowY + vcenterFixY))
                    end
                    
                    RichLayout:addChild(node)
                    nowX = nowX + nodeWidth
                end
            else
                nowY = nowY - lineHeight
            end
        end
    end
    if(realWidth == true) then
        RichLayout:setContentSize(cc.size(config.width, nowY))
        RichLayout:setContentSize(cc.size(config.width, nowY))
    end
    return RichLayout, nowY, #richList > 1 and config.width or nowX
    -- if config.isScissor ~= true then
    --     return RichLayout, nowY, #richList > 1 and config.width or nowX
    -- else
    --     local glScissorNode = ZCTool:getInstance():createGLScissorNode(config.width, config.height)
    --     glScissorNode:addChild(RichLayout)
    --     return glScissorNode, nowY, #richList > 1 and config.width or nowX
    -- end
end


function display.cutText(labelText, labelCfg, richWidth, widthLimit)
    local labelStrArr = string.utf8cut(labelText)
    local resultText1 = ""
    local resultText2 = ""
    if device.platform == "android" then
        resultText2 = " "
    end
    local renderText = ""
    local sepIndex = 0

    local function isLetter(letter)
        if(type(letter) == "string") then
            if string.byte(letter) then
                if ((string.byte(letter) >= 65 and
                    string.byte(letter) <= 90) or
                    (string.byte(letter) >= 97 and
                    string.byte(letter) <= 122)) then
                    return true
                end
            end
        end
        return false
    end
    -- 获取第一行
    for i = 1, #labelStrArr do
        renderText = renderText .. labelStrArr[i]
        local tmpLabelWidth = display.calcStrSize(renderText, labelCfg.fontName, labelCfg.fontSize)
        if(tmpLabelWidth > widthLimit) then -- 超宽，则结束
            resultText1 = renderText
            sepIndex = i
            break
        end
    end
    -- 处理英文换行加"-"
    if labelStrArr[sepIndex] and labelStrArr[sepIndex+1] and tostring(labelStrArr[sepIndex]) ~= " " and tostring(labelStrArr[sepIndex+1]) ~= " " then         
        if isLetter(labelStrArr[sepIndex]) and isLetter(labelStrArr[sepIndex+1]) then
            resultText2 = "-"
        end
    end
    -- 剩下的是第2行
    for i = sepIndex + 1, #labelStrArr do
        resultText2 = resultText2 .. labelStrArr[i]
    end

    if device.platform == "android" then
        resultText2 = resultText2 .. " "
    end

    return resultText1, resultText2
end


function display.newRichLabel(text, labelCfg)
    local myNode
    local nodeWidth
    local nodeHeight

    myNode = ccui.Layout:create()
    if(labelCfg.name and labelCfg.name ~= "") then
        myNode:setName(labelCfg.name)
    end
    local node = cc.LabelTTF:create(text, labelCfg.fontName, labelCfg.fontSize, cc.size(0, 0), kCCTextAlignmentLeft)
    node:setColor(labelCfg.color)
    node:setPosition(cc.p(0, 0))
    node:setAnchorPoint(cc.p(0, 0))
    node:setTag(1)
    nodeWidth = node:getContentSize().width
    nodeHeight = node:getContentSize().height
    myNode:addChild(node)

    myNode:setContentSize(cc.size(nodeWidth, nodeHeight))
    myNode:setContentSize(cc.size(nodeWidth, nodeHeight))
    -- 添加事件
    if(labelCfg.click ~= nil and labelCfg.click == true and labelCfg.callback ~= nil) then
        -- print("-----click event registered----")
            myNode:setTouchEnabled(true)
            myNode:setActionTag(labelCfg.actionTag)
            myNode:addTouchEventListener(function ( sender, eventType )
                if eventType == TOUCH_EVENT_ENDED then
  
                    labelCfg.callback(sender, eventType)

                end
            end)
    end
    return myNode, nodeWidth, nodeHeight
end

function display.newRichImg(labelCfg)
    local myNode
    local nodeSize
    local nodeWidth
    local nodeHeight
    myNode = ccui.ImageView:create()

    if labelCfg.plist then
        myNode:loadTexture(labelCfg.img, labelCfg.plist)
    else
        myNode:loadTexture(labelCfg.img)
    end

    myNode:setScale(labelCfg.scale)
    myNode:setAnchorPoint(cc.p(0, 0))
    myNode:setPosition(cc.p(0, 0))
    myNode:setTag(1)
    nodeSize = myNode:getContentSize()
    nodeWidth = nodeSize.width *labelCfg.scale
    nodeHeight = nodeSize.height*labelCfg.scale
    -- 添加事件
    if(labelCfg.click ~= nil and labelCfg.click == true and labelCfg.callback ~= nil) then
        -- print("-----click event registered----")
        myNode:setTouchEnabled(true)
        myNode:setActionTag(labelCfg.actionTag)
        myNode:addTouchEventListener(labelCfg.callback)
    end
    return myNode, nodeWidth, nodeHeight
end


-- 加载骨骼动画
function display.addArmatureFileInfo(csbFullName)
    ccs.ArmatureDataManager:getInstance():addArmatureFileInfo(csbFullName)
end

-- 异步加载骨骼动画
function display.addArmatureFileInfoAsync(csbFullName, callBack)
    ccs.ArmatureDataManager:getInstance():addArmatureFileInfoAsync(csbFullName, callBack)
end

-- 创建Armature
function display.createArmature( name )
    -- body
    return ccs.Armature:create(name)
end

-- 删除骨骼动画
function display.removeArmatureFileInfo(csbFullName)
    ccs.ArmatureDataManager:getInstance():removeArmatureFileInfo(csbFullName)
end

return display
