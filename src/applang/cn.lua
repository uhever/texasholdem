local langConfig = {
	-- loginScene.lua
  	loginScene_VersionUpdate 					= "版本更新中",
  	loginScene_UpdataFileOverlayFailure			= "更新文件覆盖失败，请稍后重试！",
  	loginScene_UpdataSeverConFailure 			= "更新服务器 连接失败，请稍后重试！",
  	loginScene_GetSeverListFailure 				= "获取最新服务器列表失败，请稍后重试！",
  	loginScene_LoginUserCenterFailure 			= "登录用户中心失败，请稍后重试！",
  	loginScene_DataUpdata 						= "数据更新中",
  	loginScene_StaticDataSeverFailure 			= "静态数据服务器 加载失败，请稍后重试！",
  	loginScene_StaticDataSeverReturnabnormal 	= "静态数据服务器 返回异常，请稍后重试！",
  	loginScene_StaticDataSeverConFailure 		= "静态数据服务器 连接失败，请稍后重试！",
  	loginScene_NeedAgainLoadStaticData 			= "需要重新加载静态数据。",
  	loginScene_InputUserName 					= "输入用户名",
  	loginScene_InputPassword 					= "输入密码",
  	loginScene_Register 						= "注册",
  	loginScene_PhoneNum 						= "手机号码:",
  	loginScene_VerificationCode 				= "验证码:",
  	loginScene_TipInfo							= "6-12个字符",
  	loginScene_Send								= "发送",
  	loginScene_Login 							= "登 录",
  	loginScene_UserProtocol 					= "用户协议",
  	loginScene_InputPhoneNum 					= "请输入手机号",
  	loginScene_SendVerificaFailure 				= "验证码发送失败，请稍后重试",
  	loginScene_InputPhoneNumPlease 				= "请输入手机号",
  	loginScene_InputVerificationPlease 			= "请输入验证码",
  	loginScene_UpdtaResources					= "正在为您更新资源包...",
  	loginScene_EndUpdata						= "更新完成,正在为您加载资源...",
  	loginScene_UnzipResources					= "正在为您解压资源...",

  	-- loginSceneSdk.lua
  	loginSceneSdk_ShareSuccess 					= "分享成功",
  	loginSceneSdk_ShareFailure 					= "分享失败",
  	loginSceneSdk_LoginFailure 					= "登录失败，请重新登录！",
  	loginSceneSdk_GetPictureFailure 			= "获取图片失败！",
  	loginSceneSdk_UploadPictureFailure 			= "头像上传失败！",
  	loginSceneSdk_ErrorTip 						= "错误：未配置登录UC成功后的参数[buildSDKLoginUCSuccessInfo]",
  	loginSceneSdk_OrderFailure 					= "订单创建失败，请联系客服处理",

  	-- texasHoldemScene.lua
  	texasHoldemScene_GameName					= "真人德州",
  	texasHoldemScene_Return 					= "返回",
  	texasHoldemScene_Shop 						= "商店",
  	texasHoldemScene_Blind 						= "盲注: ",
    texasHoldemScene_JoinRoomErrorMsg = "您还需要%s金币才能进入房间，是否购买？",

  	-- confirmPop.lua
  	confirm_Sure 								= "确定",
  	confirm_Cancel 								= "取消",
  	confirm_Tip 								= "提 示",

  	-- developerSettingPop.lua
  	developerSetting_DevelopmentSettings 		= "开发设置",
  	developerSetting_Reduction 					= "还原",
  	developerSetting_Preservation 				= "保存",
  	developerSetting_InitSetting 				= "确定要初始化配置?",

  	-- dzBuyChipPop.lua
  	dzBuyChip_BuyChip 							= "补充筹码",
  	dzBuyChip_BuyGameCurrency 					= "购买德州币",
  	dzBuyChip_AddGameCurrency 					= "下一局开始前，将为您补充所购筹码",
  	dzBuyChip_MaxCarry 							= "最大带入: ",
  	dzBuyChip_AlreadyCarry 						= "已带入: ",
  	-- dzBuyChip_Sure								= "确定",
  	dzBuyChip_Chip 								= "筹码",
  	dzBuyChip_Give 								= "送",
  	dzBuyChip_RechargeSuccess 					= "成功充值",

  	-- dzMenuPop.lua
  	dzMenu_ChangeTable 							= "换桌",
  	dzMenu_StandUpCrowd 						= "站起围观",
  	dzMenu_Exit 								= "退出牌局",
  	dzMenu_OverTime 							= "加时",
  	dzMenu_Skip 								= "跳过",
  	dzMenu_Pause 								= "暂停",
  	dzMenu_Continue 							= "继续",
  	dzMenu_SendSuccess 							= "发送成功",
  	dzMenu_ExitRoomSuccess 						= "退出房间成功",

  	-- dzRoleinfoPop.lua
  	dzRoleinfo_ScoreBoard 						= "当前记分牌: ",
  	dzRoleinfo_Profit 							= "桌内盈利: ",
  	dzRoleinfo_Hello 							= "您不能对自己打招呼",

  	-- enterMainPop.lua
  	enterMain_JoinGame 							= "快速加入牌局",
  	enterMain_JoinGameText 						= "加入随机牌局，快速开始游戏！",
  	enterMain_Season 							= "选择场次",
  	enterMain_SeasonText 						= "选择进入初级场，高级场等。",
  	enterMain_Champion 							= "比赛场",
  	enterMain_ChampionText 						= "加入 SNG MTT 赛事！",
  	enterMain_CenterName 						= "德州扑克",
  	enterMain_OngoingGame						= "正在进行中的牌局...",

  	-- exactAddBetPop.lua
  	exactAddBet_FillingBeyondLimit 				= "加注数量超出限制",

  	-- helpWebPop.lua
  	helpWeb_Help 								= "帮助",

  	-- lastGamePop.lua
  	lastGame_LastGame 							= "上局回顾",
  	lastGame_Collection 						= "收藏本手牌",
  	lastGame_CancelCollection					= "取消收藏",
  	lastGame_DiscardCard 						= "弃牌",
  	lastGame_CollectionSuccess 					= "收藏成功",

  	-- loaderPop.lua
  	loader_UpdatingResourcePack 				= "正在为您更新资源包...",

  	-- mineGameinfoPop.lua
  	mineGameinfo_Record 						= "牌谱记录",

  	-- mineInformationPop.lua
  	mineInformation_Modify 						= "修改资料",
  	mineInformation_Avatar 						= "头像",
  	mineInformation_Nickname 					= "昵称",
  	mineInformation_Edit 						= "这里直接编辑",
  	mineInformation_Tip 						= "请输入合法的用户名",
  	mineInformation_ModifySuccess 				= "用户名修改成功",

  	-- minePop.lua
  	mine_Shop 									= "商店",
  	mine_Setting 								= "设置",
  	mine_Profile								= "牌谱收藏",
  	mine_Service 								= "客服",
  	mine_Help									= "帮助",
  	mine_Share									= "分享",
  	mine_Individual								= "个人",
  	mine_TimeMoney								= "时间就是金钱，我的朋友！",
  	mine_ContentTitle							= "【鲸鱼德州】最火爆的德州扑克, 快来加入吧",
  	mine_ContentDescription						= "快来体验德州扑克，一起high翻天，IOS&Android首发",

  	-- mineSettingPop.lua
  	mineSetting_Effect 							= "游戏音效",
  	mineSetting_Vibrate 						= "游戏震动",
  	mineSetting_Version 						= "当前版本",
  	mineSetting_Logout							= "退出登录",
  	mineSetting_Setting							= "设置",

  	-- recordDetailPop.lua
  	recordDetail_RecordDetail				 	= "战绩详情",
  	recordDetail_Day 							= "日",
  	recordDetail_Month							= "月",
  	recordDetail_Insurance						= "保险：",
  	recordDetail_Earnings 						= "盈利：",
  	recordDetail_Date 							= "3月9日",
  	recordDetail_DataStatistics 				= "数据统计",
  	recordDetail_Analysis						= "牌局分析",
  	recordDetail_Year							= "年",

  	-- recordPop.lua
  	record_Record 								= "战绩",
  	record_OrdinaryRecord						= "普通战绩",
  	record_Pool 								= "入池率",
  	record_Winrate								= "其中胜率",
  	record_PlayNum								= "总手数",
  	record_CordRecord							= "牌谱记录",
  	record_CordRecordText						= "收录最近19天的手牌",

  	-- recordStatisticsPop.lua
  	recordStatistics_Statistics					= "基础统计",
  	recordStatistics_SwitchAll 					= "全部",
  	recordStatistics_SwitchSevenDays			= "近七日",
  	recordStatistics_SwitchOneMonth				= "近一月",
  	recordStatistics_PlayMun					= "牌局数:",
  	recordStatistics_HandNum					= "手牌数:",
  	recordStatistics_Earnings					= "盈利",
  	recordStatistics_HundredEarnings			= "平均百手盈利",
  	recordStatistics_HundredBlindness			= "平均白百手赢得大盲",
  	recordStatistics_AverageAllIn				= "平均 All-in",
  	recordStatistics_Winrate					= "胜率统计",
  	recordStatistics_TotalWinrate				= "总胜率",
  	recordStatistics_ShowdownWinrate			= "摊牌胜率",
  	recordStatistics_SeeWinrate					= "看翻牌胜率",
  	recordStatistics_WithNodeWinrate			= "河牌圈跟注胜率",

  	-- registAccountPop.lua
  	registAccount_Register						= "注册账号",
  	registAccount_InputUserName					= "输入用户名",
  	registAccount_InputPassword					= "输入密码",
  	registAccount_InputAccountNum				= "请输入账号！",
  	registAccount_InputPasswordPlease			= "请输入密码！",
  	registAccount_RegisterSuccess				= "注册成功！",
  	registAccount_RegisterResult1				= "账号格式不正确！（长度限制为5-16位字母或者数字）",
  	registAccount_RegisterResult2				= "密码格式不正确！（长度限制为6-16位字母或数字）",
  	registAccount_RegisterResult3				= "账号已存在！",
  	registAccount_RegisterResult4				= "请您将账号密码输入完整！",
  	registAccount_RegisterResult5				= "手机号码格式错误，请重新输入！",
  	registAccount_RegisterError					= "注册失败，请稍后重试！",

  	-- statisticsGamePop.lua
  	statisticsGame_RealRecord 					= "实时战绩",
  	statisticsGame_Player						= "玩家",
  	statisticsGame_Buy							= "买入",

  	-- texasHoldemPop.lua
  	texasHoldem_BuyInfo							= "您还需要%s金币才能进入房间，是否购买？",

  	-- toolsPop.lua
  	-- tools_Cancel								= "取消",

  	-- Player.lua
  	Player_Tip 									= "成功兑换%d筹码,退出房间返还对应金币",
  	Player_Pass									= "过牌",
  	Player_Filling								= "加注",
  	Player_Following							= "跟注",
  	Player_AddTime								= "加时",
  	Player_LookCards							= "看牌",
  	Player_DiscardCard							= "弃牌",
  	Player_EndAndShow							= "结束后亮出",

  	-- PlayGround.lua
  	PlayGround_Info								= "号桌",
  	PlayGround_Flop								= "翻牌",
  	PlayGround_TurnCard							= "转牌",
  	PlayGround_RiverCard						= "河牌",
  	PlayGround_Player							= "玩家 ",
  	PlayGround_WantCardInfo						= " 想看翻牌圈的牌",
  	PlayGround_WantTurnCardInfo					= " 想看转牌圈的牌",
  	PlayGround_WantRiverCardInfo				= " 想看河牌圈的牌",
  	PlayGround_Field							= "场",
  	PlayGround_NextField						= "等待下一局开始...",
  	PlayGround_SeeNextCards						= "查看下一圈牌",
  	PlayGround_PreciseFilling					= "精确加注",
  	PlayGround_BlindNote						= "盲注",
  	PlayGround_Pool								= "底池",
  	PlayGround_PoolInfo							= "底池: ",

  	-- cd.lua
  	cd_Info										= "暂时没有玩家加入，需要加入其它牌桌吗？",

  	-- fileLoader.lua
  	fileLoader_Loading							= "正在加载",
  	fileLoader_Total							= "总共",
  	fileLoader_ErrorMsg							= "很抱歉，由于网络不好，资源加载失败。",
  	fileLoader_Retry							= "重试",
  	fileLoader_Off								= "关闭",

  	-- globalUI.lua
  	globalUI_MainInterface						= "主界面",
  	globalUI_Next								= "下一步",

  	-- PlayerData.lua
  	PlayerData_TipInfo							= "模版格式错误:",

  	-- common.lua
  	common_UpdataSuccess						= "头像上传成功！",
  	common_ClubUpdataSuccess					= "俱乐部头像上传成功!",
  	common_NetFailureMsg						= "网络连接失败，请稍后重试:",
  	common_Reconnect							= "重新连接",
  	common_ReturnLogin							= "返回登录",
  	common_OperationFailed						= "操作失败，请稍后重试！",
  	common_PayMsg1								= "恭喜您充值成功，共获得",
  	common_PayMsg2								= "钻石。",
  	common_PayMsg3								= "游戏币。",
  	common_PayMsg4								= "恭喜您成为VIP",

  	-- commonConfig.lua
  	commonConfig_Font							= "简体",

  	-- commonHelper.lua
  	commonHelper_Million						= "万",
  	commonHelper_Billion						= "亿",
  	commonHelper_NotFindID						= "找不到后端文本模版ID:",
  	commonHelper_ParameterError					= "参数错误模版ID:",
  	commonHelper_Day							= "天",
  	commonHelper_Second			                = "秒",
  	commonHelper_Hour                           = "时",
    commonHelper_Min                            = "分",
    commonHelper_UnknownLocation				= "未知地点",
    commonHelper_ErrorMsg1						= "防沉迷提示：您累计在线时间已满",
    commonHelper_ErrorMsg2						= "个小时,祝您游戏愉快！",
    commonHelper_ErrorMsg3						= "个小时,将对您的账号进行强制下线操作。",
    commonHelper_PokerTypeName1					= "高牌",
    commonHelper_PokerTypeName2					= "对子",
    commonHelper_PokerTypeName3					= "两对",
    commonHelper_PokerTypeName4					= "三条",
    commonHelper_PokerTypeName5					= "顺子",
    commonHelper_PokerTypeName6					= "同花",
    commonHelper_PokerTypeName7					= "葫芦",
    commonHelper_PokerTypeName8					= "四条",
    commonHelper_PokerTypeName9					= "同花顺",
    commonHelper_PokerTypeName10				= "皇家同花顺",
    commonHelper_RoomTypeName1					= "平民场",
    commonHelper_RoomTypeName2					= "小资场",
    commonHelper_RoomTypeName3					= "老板场",
    commonHelper_RoomTypeName4					= "土豪场",
    commonHelper_RoomTypeName5					= "贵族场",
    commonHelper_RoomTypeName6					= "皇家场",
    commonHelper_PlayTypeName1					= "6人桌",

    -- mainSub.lua
    mainSub_ErrorMsg							= "Error, 重启游戏即可继续游戏！",

    -- friendlyGamePop.lua
    friendlyGame_JoinFriend       = "进入好友牌局",
    friendlyGame_Input            = "输入邀请码 , 和好友一起切磋",
    friendlyGame_CreateFriend     = "创建好友牌局",
    friendlyGame_Setting          = "设置携带上限",
    friendlyGame_Begin            = "开 始 牌 局",
}

return langConfig