--[[
    更新服务
]]

-- 重新加载语言包(在更新后重启的时候)
zc.require("src/applang/language")

local SCENE = {}
-- local myTouchLayer = nil
local mainWidget = nil

-- params 传递参数
function SCENE.create(params)

    print("new init scene")

    local loadingScene = cc.Scene:create()
    loadingScene:registerScriptHandler(function(state)
        if state == "enter" then
            -- myTouchLayer = TouchGroup:create()
            -- loadingScene:addChild(myTouchLayer)
            -- 所有得东西都放到这个 widget上
            mainWidget = ccui.Widget:create()
            loadingScene:addChild(mainWidget)


            SCENE.initPage()

            
            -- self:onEnter_()
        -- elseif state == "exit" then
        --     self:onExit_()
        -- elseif state == "enterTransitionFinish" then
        --     self:onEnterTransitionFinish_()
        -- elseif state == "exitTransitionStart" then
        --     self:onExitTransitionStart_()
        -- elseif state == "cleanup" then
        --     self:onCleanup_()
        end
    end)

    return loadingScene

end

function SCENE.initPage()

    -- SDK的根据平台，看是否需要显示启动画面
    local launchImgURL = "res/Launch/demo.jpg"

    if(launchImgURL ~= "") then
        local img = ccui.ImageView:create()
        img:loadTexture(launchImgURL)
        img:setScale(display.height / 640)
        img:setAnchorPoint(cc.p(0.5, 0.5))
        img:setPosition(cc.p(display.cx, display.cy))
        mainWidget:addChild(img)


        SCENE.splashDone()


    else
        SCENE.splashDone()
    end

end

function SCENE.splashDone()

    print("splashDone.")
    -- 【不需要初始化SDK】

    
    if(device.platform == "mac") then

        SCENE.sdkFinish()

    elseif LOGIN_MODE == kLoginModeNormal then -- 普通登陆

        SCENE.sdkFinish()

    elseif LOGIN_MODE == kLoginModeZCSDK then -- ZCSDK
        -- IOS
        if(device.platform == "ios") then

            callOc("registerCallBackByName", { initFinish = SCENE.sdkFinish })
            callOc("initSDK", { })

        -- ANDROID
        elseif(device.platform == "android") then

            callJava("initSDK", { SCENE.sdkFinish }, "(I)V")

        end

    elseif LOGIN_MODE == kLoginModeInApp then -- InApp
        SCENE.sdkFinish()
    elseif LOGIN_MODE == kLoginModeLiveSDK then -- 直播SDK
        SCENE.sdkFinish()
    else
        SCENE.sdkFinish()
    end

end

function SCENE.sdkFinish()

    print("SDK初始化完成，开始获取设备信息，然后启动游戏")

    -- 设备信息
    zc.device = {
        model       = "",
        mac         = "",
        idfa        = "",
        idfaEnable  = "",
        openudid    = "",
        odin        = "",
        deviceToken = "",
        os_version  = "", -- 设备系统的版本号 如ios 7.1
        jailbreak   = "", -- 是否越狱
        android_id  = "", --安卓唯一id
        model       = "",
        ssid        = ""
    }


    -- 获取设备信息，然后启动游戏
    if(device.platform == "ios") then
        print("getDeviceInfo")
        callOc("getDeviceInfo", { callback = function(result)

                local myDeviceInfo = string.split(result.deviceInfo, "=")
                zc.device.mac = myDeviceInfo[1] or "00:00:00:00:00:00"
                zc.device.idfa = myDeviceInfo[2] or ""
                zc.device.deviceToken = myDeviceInfo[3] or ""
                zc.device.idfaEnable = myDeviceInfo[4] or "0"
                zc.device.openudid = myDeviceInfo[5] or ""
                zc.device.odin = myDeviceInfo[6] or ""
                zc.device.os_version = string.urlencode(myDeviceInfo[7]) or ""
                zc.device.jailbreak = myDeviceInfo[8] or "0"
                zc.device.ssid = string.urlencode(myDeviceInfo[9]) or ""
                zc.device.model = string.urlencode(myDeviceInfo[10]) or ""
                dump(zc.device, "zc.device")

                SCENE.playLogo()

            end})
    elseif(device.platform == "android") then
        callJava(
            "getDeviceInfo",
            {
            function(deviceStr)
                local myDeviceInfo = string.split(deviceStr, "=")
                zc.device.mac = myDeviceInfo[1] or "00:00:00:00:00:00"
                zc.device.model = string.urlencode(myDeviceInfo[2]) or ""
                zc.device.android_id = myDeviceInfo[3] or ""
                zc.device.os_version = string.urlencode(myDeviceInfo[4]) or ""
                zc.device.ssid = string.urlencode(myDeviceInfo[5]) or ""
                zc.device.advertising_id = myDeviceInfo[6] or ""
                dump(zc.device, "zc.device")

                if(zc.device.model == "GT-I9100") then
                    IS_NO_VIDEO = true
                end
                SCENE.playLogo()
            end
            },
            "(I)V"
        )
    else
        
        SCENE.playLogo()
    end

    


end

function SCENE.playLogo()

    print("playLogo.")

    -- if(APP_PLATFORM == "ios") then
    --     zcAdTrack() -- 所有渠道都需要统计
    -- end

    if(IS_NO_VIDEO) then

        SCENE.finish()

    elseif device.platform == "android" then

        local javaParams = {
           "res/Video/logo.mp4",
            function()
                SCENE.finish()
            end
        }
        callJava("playVideo", javaParams, "(Ljava/lang/String;I)V")

    elseif device.platform == "ios" then

        local playCallback = function()
            SCENE.finish()
        end
        callOc("registerCallBackByName", { videoFinished = playCallback })
        callOc("showLogoVideo", { name = "logo", needPass = "false" })

    else

        SCENE.finish()

    end

end

function SCENE.finish()
    zc.require("src/app/base")
end


function SCENE.clean()

    -- myTouchLayer = nil
    mainWidget = nil

    cc.Director:getInstance():getTextureCache():removeAllTextures()

end

-- 进入界面
display.runScene( SCENE.create() )
