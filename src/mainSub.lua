function simpleHttp(requestUrl, params, successCallback, failCallback)

    local url = requestUrl .. "&time=" .. os.time() .. "&"
    local params = params or {}
    local successFunction = successCallback or function() end
    local failFunction = failCallback or function() end

    local request = cc.XMLHttpRequest:new()
    request.timeout = 5
    request.responseType = cc.XMLHTTPREQUEST_RESPONSE_BLOB
    
    request:open("POST", url)

    print("SimpleHTTP - ", url)


    local postString = ""
    for k , v in pairs(params) do
        postString = postString .. k .. "=" .. v .. "&"
    end


    local function onReadyStateChanged(  )
        if request.readyState == 4 and (request.status >= 200 and request.status < 207) then

            -- 新版本
            local response = request.response

            local result = json.decode(response)

            pcall(function()
                local result = json.decode(response)
                successFunction(result)
            end, failFunction)

        else
            failFunction()
        end

        request:unregisterScriptHandler()
    end

    request:registerScriptHandler(onReadyStateChanged)
    request:send(postString)

end

function zcAdTrack()

    local isTracked = cc.UserDefault:getInstance():getStringForKey("isTracked")
    if(isTracked and isTracked == "true") then -- 已经激活过
        print("推广统计：产品已经完成激活")
        return
    end
    -- 此处的配置不同的语言或客户端版本会不同。
    local params = {
        appid        = "", -- 苹果市场id
        product      = ZC_TRACK_CFG.product,
        mac          = zc.device.mac,
        idfa         = zc.device.idfa,
        channel      = CHANNELID,
        device_name  = zc.device.model,
        os_name      = device.platform,
        os_version   = zc.device.os_version,
        jailbreak    = zc.device.jailbreak,
        ssid         = zc.device.ssid,
        android_id   = zc.device.android_id,
        advertising_id   = zc.device.advertising_id or "",
        sign         = ""
    }

    local appkey
    local httpUrl
    if(APP_PLATFORM == "ios" or APP_PLATFORM == "ios-zc") then
        appkey = ZC_TRACK_CFG.ios.appkey
        params.appid = ZC_TRACK_CFG.ios.appid
        httpUrl = ZC_TRACK_API .. "c=iostrack"
    else
        appkey = ZC_TRACK_CFG.android.appkey
        params.appid = ZC_TRACK_CFG.android.appid
        httpUrl = ZC_TRACK_API .. "c=android_track&m=active"
    end
    print(httpUrl)

    local signStr = params.advertising_id..params.android_id..params.appid ..params.channel..params.device_name..params.idfa..params.jailbreak..params.mac.. params.os_name .. params.os_version .. params.product..params.ssid .. appkey
    params.sign = md5.sumhexa(signStr)
    print("signStr", signStr)
    print("signResult", params.sign)

    dump(params)
    simpleHttp(httpUrl, { data = json.encode(params) }, function(data)
        dump(data)
        cc.UserDefault:getInstance():setStringForKey("isTracked", "true")
        print("推广统计：激活信息已发送")
    end)


end

function showErrorLayer( msg )

    local curScene = display.getRunningScene()
    if curScene._errorLayer ~= nil then return end

    -- 显示ErrorLayer
    if device.platform == "mac" or UPDATE_DEBUG == 1 then 
        local bgImgStr = "res/Image/main/main_bg.jpg"
        local tipLabelPos = cc.p(display.cx, 200)
        local showLogBtnPos = cc.p(display.cx - 160, 100)
        local okBtnPos = cc.p(display.cx + 160, 100)
        local logWidth = display.width
        local logHeight = display.height - 250
        local logScrollViewPosY = 230
        if display.getOrientation() ~= "portrait" then
            bgImgStr = "res/Image/main/main_bg.jpg"
            tipLabelPos = cc.p(display.cx, 170)
            showLogBtnPos = cc.p(display.cx - 160, 100)
            okBtnPos = cc.p(display.cx + 160, 100)
            logWidth = display.width
            logHeight = display.height - 220
            logScrollViewPosY = 200
        end

        local errorLayer = ccui.Widget:create()
        curScene._errorLayer = errorLayer
        errorLayer:setTouchEnabled(true)
        curScene:addChild(errorLayer, 10001)

        local bg = ccui.ImageView:create()
        bg:loadTexture(bgImgStr)
        bg:setScaleX(display.width / bg:getContentSize().width)
        bg:setScaleY(display.height / bg:getContentSize().height)
        errorLayer:addChild(bg)
        bg:setPosition(display.cx, display.cy)

        local tipLabel = ccui.Text:create()
        tipLabel:setColor(display.COLOR_RED)
        tipLabel:setFontSize(28)
        tipLabel:setFontName(display.FONT1)
        tipLabel:setString(zc.lang("mainSub_ErrorMsg", "Error, 重启游戏即可继续游戏！"))
        tipLabel:setPosition(tipLabelPos)
        errorLayer:addChild(tipLabel)

        local haveShowLog = false

        local function showLogCallback()
            if haveShowLog then return end
            haveShowLog = true

            local logLabel = ccui.Text:create()
            logLabel:setColor(display.COLOR_BLACK)
            logLabel:setFontSize(28)
            logLabel:setFontName(display.FONT1)
            logLabel:setTextAreaSize(cc.size(logWidth, 0))
            logLabel:setString(msg)

            local logSize = logLabel:getContentSize()

            local logScrollView = ccui.ScrollView:create()
            logScrollView:setBackGroundColorType(ccui.LayoutBackGroundColorType.solid)
            logScrollView:setBackGroundColor(display.COLOR_WHITE)
            logScrollView:setBackGroundColorOpacity(180)
            logScrollView:setContentSize(cc.size(logWidth, logHeight))
            logScrollView:setDirection(ccui.ScrollViewDir.vertical)
            logScrollView:setTouchEnabled(true)
            logScrollView:setBounceEnabled(true)
            logScrollView:setInertiaScrollEnabled(true)
            logScrollView:setInnerContainerSize(logSize)
            logScrollView:setAnchorPoint(cc.p(0, 0))
            errorLayer:addChild(logScrollView)
            logScrollView:setPosition(0, logScrollViewPosY)
            
            logScrollView:addChild(logLabel)
            logLabel:setPosition(logSize.width/2, logSize.height/2)
        end

        local function okCallback()
            print("ok-------")
            zc.logout()
        end

        local normalSrc = "res/Image/login/login_small_btn_n.png"
        local pressedSrc = "res/Image/login/login_small_btn_p.png"
        local disabledSrc = "res/Image/login/login_small_btn_n.png"

        local showLogBtn = ccui.Button:create()
        showLogBtn:loadTextures(normalSrc, pressedSrc, disabledSrc)
        showLogBtn:addTouchEventListener(function(sender, eventType)
            if eventType == ccui.TouchEventType.ended then
                showLogCallback()
            end
        end)
        showLogBtn:setTitleFontSize(38)
        showLogBtn:setTitleText("Show Log")
        showLogBtn:setPosition(showLogBtnPos)
        errorLayer:addChild(showLogBtn)

        local okBtn = ccui.Button:create()
        okBtn:loadTextures(normalSrc, pressedSrc, disabledSrc)
        okBtn:addTouchEventListener(function(sender, eventType)
            if eventType == ccui.TouchEventType.ended then
                okCallback()
            end
        end)
        okBtn:setTitleFontSize(38)
        okBtn:setTitleText("OK")
        okBtn:setPosition(okBtnPos)
        errorLayer:addChild(okBtn)
    end

    -- 用热更新的开启判断是否上传错误信息(排除开发时候的错误)
    if (UPDATE_OPEN and UPDATE_OPEN == true) then
        -- 出错不管
        xpcall(function() saveLogOnWeb(msg) end, function() print("错误.") end)
    end

end

function saveLogOnWeb(msg)

    local sendLogAction = function(data)

        local userIp = "0.0.0.0"
        if(data and data.ip) then
            userIp = data.ip
        end

        -- 1.功能模块、渠道、设备、IP
        local popName = ""
        if zc.activePops and #zc.activePops > 0 then
            popName = zc.activePops[#zc.activePops]
        end
        local deviceMac = ""
        if zc.device and zc.device.mac then
            deviceMac = zc.device.mac
        end
        local errorMsg = "POKER - CHANNELID:" .. CHANNELID .. ", Device:" .. device.platform .. " " .. deviceMac .. " | " .. userIp .. " | " .. "LOCAL_VERSION:" .. LOCAL_VERSION .. " | " .. popName .. " |"


        -- 2.用户信息
        if(PlayerData and PlayerData.base) then
            errorMsg = errorMsg .. "UID:" .. PlayerData.base.uid .. ", NAME:" .. PlayerData.base.nickname .. " | "
        end

        -- 3.实际log内容
        errorMsg = errorMsg .. string.trim(msg)

        -- 发送
        simpleHttp("https://img-live.jjshowtime.com/index.php?c=errorlog&m=send", { msg = errorMsg })

    end

    simpleHttp("https://update-mhdl.zhanchenggame.com/index.php?c=errorlog&m=get_ip", nil,
        function(data)
            sendLogAction(data)
        end,
        function()
            sendLogAction()
        end
    )

end