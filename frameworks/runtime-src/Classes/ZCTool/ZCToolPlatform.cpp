//
//  ZCToolPlatform.cpp
//  Poker
//
//  Created by zctech-011 on 2017/1/16.
//
//

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)

#include "ZCToolPlatform.h"

#include <jni.h>
#include "jni/JniHelper.h"
#include <android/log.h>


// JNI
extern "C"
{
    JNIEXPORT void JNICALL Java_com_zhanyouall_poker_AppController_nativeExitGame1(JNIEnv*  env, jobject thiz)
    {
        cocos2d::Director::getInstance()->end();
    }
    
    JNIEXPORT void JNICALL Java_com_zhanyouall_poker_AppController_nativeExitGame2(JNIEnv*  env, jobject thiz)
    {
        cocos2d::ScriptEngineManager::getInstance()->removeScriptEngine();
    }
}


const char* calcStrSizePlatform(const char* str, const char* font, const char* fontSize)
{
    
    
    
    JniMethodInfo methodInfo; //用于获取函数体
    bool isHave = JniHelper::getStaticMethodInfo(methodInfo, "com/zhanyouall/poker/AppController", "getAndroidLabelSize", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;");
    
    if (isHave)
    {
        const char* resultChar;
        
        jstring jmsgStr = methodInfo.env->NewStringUTF(str);
        jstring jmsgFont = methodInfo.env->NewStringUTF(font);
        jstring jmsgFontSize = methodInfo.env->NewStringUTF(fontSize);
        
        jstring jstr;
        jstr = (jstring)methodInfo.env->CallStaticObjectMethod(methodInfo.classID, methodInfo.methodID, jmsgStr, jmsgFont, jmsgFontSize);
        
        resultChar = methodInfo.env->GetStringUTFChars(jstr, NULL);
        
        str = NULL;
        font = NULL;
        fontSize = NULL;
        
        methodInfo.env->DeleteLocalRef(jmsgStr);
        methodInfo.env->DeleteLocalRef(jmsgFont);
        methodInfo.env->DeleteLocalRef(jmsgFontSize);
        
        methodInfo.env->DeleteLocalRef(methodInfo.classID);// 神坑
        
        methodInfo.env->DeleteLocalRef(jstr);
        
        jmsgStr = NULL;
        jmsgFont = NULL;
        jmsgFontSize = NULL;
        jstr = NULL;
        isHave = NULL;
        
        return  resultChar;
    }
    else
    {
        CCLOG("java层 calcStrSize 函数不存在;");
        
        return "";
    }
    
}



#endif
