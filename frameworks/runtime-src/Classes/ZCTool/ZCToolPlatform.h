//
//  ZCToolPlatform.hpp
//  Poker
//
//  Created by zctech-011 on 2017/1/16.
//
//

#ifndef ZCToolPlatform_h
#define ZCToolPlatform_h

#include "cocos2d.h"
USING_NS_CC;

const char* calcStrSizePlatform(const char* str, const char* font, const char* fontSize);

#endif /* ZCToolPlatform_hpp */
