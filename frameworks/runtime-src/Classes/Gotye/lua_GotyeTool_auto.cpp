#include "lua_GotyeTool_auto.hpp"
#include "GotyeTool.h"
#include "scripting/lua-bindings/manual/tolua_fix.h"
#include "scripting/lua-bindings/manual/LuaBasicConversions.h"

int lua_GotyeTool_GotyeTool_leaveRoom(lua_State* tolua_S)
{
    int argc = 0;
    GotyeTool* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"GotyeTool",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (GotyeTool*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_GotyeTool_GotyeTool_leaveRoom'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 1) 
    {
        int arg0;

        ok &= luaval_to_int32(tolua_S, 2,(int *)&arg0, "GotyeTool:leaveRoom");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_GotyeTool_GotyeTool_leaveRoom'", nullptr);
            return 0;
        }
        cobj->leaveRoom(arg0);
        lua_settop(tolua_S, 1);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "GotyeTool:leaveRoom",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_GotyeTool_GotyeTool_leaveRoom'.",&tolua_err);
#endif

    return 0;
}
int lua_GotyeTool_GotyeTool_unregisterEventHandler(lua_State* tolua_S)
{
    int argc = 0;
    GotyeTool* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"GotyeTool",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (GotyeTool*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_GotyeTool_GotyeTool_unregisterEventHandler'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_GotyeTool_GotyeTool_unregisterEventHandler'", nullptr);
            return 0;
        }
        cobj->unregisterEventHandler();
        lua_settop(tolua_S, 1);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "GotyeTool:unregisterEventHandler",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_GotyeTool_GotyeTool_unregisterEventHandler'.",&tolua_err);
#endif

    return 0;
}
int lua_GotyeTool_GotyeTool_stopTalk(lua_State* tolua_S)
{
    int argc = 0;
    GotyeTool* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"GotyeTool",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (GotyeTool*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_GotyeTool_GotyeTool_stopTalk'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_GotyeTool_GotyeTool_stopTalk'", nullptr);
            return 0;
        }
        cobj->stopTalk();
        lua_settop(tolua_S, 1);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "GotyeTool:stopTalk",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_GotyeTool_GotyeTool_stopTalk'.",&tolua_err);
#endif

    return 0;
}
int lua_GotyeTool_GotyeTool_getTalkingPower(lua_State* tolua_S)
{
    int argc = 0;
    GotyeTool* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"GotyeTool",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (GotyeTool*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_GotyeTool_GotyeTool_getTalkingPower'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_GotyeTool_GotyeTool_getTalkingPower'", nullptr);
            return 0;
        }
        int ret = cobj->getTalkingPower();
        tolua_pushnumber(tolua_S,(lua_Number)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "GotyeTool:getTalkingPower",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_GotyeTool_GotyeTool_getTalkingPower'.",&tolua_err);
#endif

    return 0;
}
int lua_GotyeTool_GotyeTool_talkToUser(lua_State* tolua_S)
{
    int argc = 0;
    GotyeTool* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"GotyeTool",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (GotyeTool*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_GotyeTool_GotyeTool_talkToUser'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 1) 
    {
        const char* arg0;

        std::string arg0_tmp; ok &= luaval_to_std_string(tolua_S, 2, &arg0_tmp, "GotyeTool:talkToUser"); arg0 = arg0_tmp.c_str();
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_GotyeTool_GotyeTool_talkToUser'", nullptr);
            return 0;
        }
        cobj->talkToUser(arg0);
        lua_settop(tolua_S, 1);
        return 1;
    }
    if (argc == 2) 
    {
        const char* arg0;
        unsigned int arg1;

        std::string arg0_tmp; ok &= luaval_to_std_string(tolua_S, 2, &arg0_tmp, "GotyeTool:talkToUser"); arg0 = arg0_tmp.c_str();

        ok &= luaval_to_uint32(tolua_S, 3,&arg1, "GotyeTool:talkToUser");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_GotyeTool_GotyeTool_talkToUser'", nullptr);
            return 0;
        }
        cobj->talkToUser(arg0, arg1);
        lua_settop(tolua_S, 1);
        return 1;
    }
    if (argc == 3) 
    {
        const char* arg0;
        unsigned int arg1;
        bool arg2;

        std::string arg0_tmp; ok &= luaval_to_std_string(tolua_S, 2, &arg0_tmp, "GotyeTool:talkToUser"); arg0 = arg0_tmp.c_str();

        ok &= luaval_to_uint32(tolua_S, 3,&arg1, "GotyeTool:talkToUser");

        ok &= luaval_to_boolean(tolua_S, 4,&arg2, "GotyeTool:talkToUser");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_GotyeTool_GotyeTool_talkToUser'", nullptr);
            return 0;
        }
        cobj->talkToUser(arg0, arg1, arg2);
        lua_settop(tolua_S, 1);
        return 1;
    }
    if (argc == 4) 
    {
        const char* arg0;
        unsigned int arg1;
        bool arg2;
        unsigned int arg3;

        std::string arg0_tmp; ok &= luaval_to_std_string(tolua_S, 2, &arg0_tmp, "GotyeTool:talkToUser"); arg0 = arg0_tmp.c_str();

        ok &= luaval_to_uint32(tolua_S, 3,&arg1, "GotyeTool:talkToUser");

        ok &= luaval_to_boolean(tolua_S, 4,&arg2, "GotyeTool:talkToUser");

        ok &= luaval_to_uint32(tolua_S, 5,&arg3, "GotyeTool:talkToUser");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_GotyeTool_GotyeTool_talkToUser'", nullptr);
            return 0;
        }
        cobj->talkToUser(arg0, arg1, arg2, arg3);
        lua_settop(tolua_S, 1);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "GotyeTool:talkToUser",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_GotyeTool_GotyeTool_talkToUser'.",&tolua_err);
#endif

    return 0;
}
int lua_GotyeTool_GotyeTool_talkToGroup(lua_State* tolua_S)
{
    int argc = 0;
    GotyeTool* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"GotyeTool",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (GotyeTool*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_GotyeTool_GotyeTool_talkToGroup'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 1) 
    {
        int arg0;

        ok &= luaval_to_int32(tolua_S, 2,(int *)&arg0, "GotyeTool:talkToGroup");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_GotyeTool_GotyeTool_talkToGroup'", nullptr);
            return 0;
        }
        cobj->talkToGroup(arg0);
        lua_settop(tolua_S, 1);
        return 1;
    }
    if (argc == 2) 
    {
        int arg0;
        unsigned int arg1;

        ok &= luaval_to_int32(tolua_S, 2,(int *)&arg0, "GotyeTool:talkToGroup");

        ok &= luaval_to_uint32(tolua_S, 3,&arg1, "GotyeTool:talkToGroup");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_GotyeTool_GotyeTool_talkToGroup'", nullptr);
            return 0;
        }
        cobj->talkToGroup(arg0, arg1);
        lua_settop(tolua_S, 1);
        return 1;
    }
    if (argc == 3) 
    {
        int arg0;
        unsigned int arg1;
        bool arg2;

        ok &= luaval_to_int32(tolua_S, 2,(int *)&arg0, "GotyeTool:talkToGroup");

        ok &= luaval_to_uint32(tolua_S, 3,&arg1, "GotyeTool:talkToGroup");

        ok &= luaval_to_boolean(tolua_S, 4,&arg2, "GotyeTool:talkToGroup");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_GotyeTool_GotyeTool_talkToGroup'", nullptr);
            return 0;
        }
        cobj->talkToGroup(arg0, arg1, arg2);
        lua_settop(tolua_S, 1);
        return 1;
    }
    if (argc == 4) 
    {
        int arg0;
        unsigned int arg1;
        bool arg2;
        unsigned int arg3;

        ok &= luaval_to_int32(tolua_S, 2,(int *)&arg0, "GotyeTool:talkToGroup");

        ok &= luaval_to_uint32(tolua_S, 3,&arg1, "GotyeTool:talkToGroup");

        ok &= luaval_to_boolean(tolua_S, 4,&arg2, "GotyeTool:talkToGroup");

        ok &= luaval_to_uint32(tolua_S, 5,&arg3, "GotyeTool:talkToGroup");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_GotyeTool_GotyeTool_talkToGroup'", nullptr);
            return 0;
        }
        cobj->talkToGroup(arg0, arg1, arg2, arg3);
        lua_settop(tolua_S, 1);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "GotyeTool:talkToGroup",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_GotyeTool_GotyeTool_talkToGroup'.",&tolua_err);
#endif

    return 0;
}
int lua_GotyeTool_GotyeTool_joinGroup(lua_State* tolua_S)
{
    int argc = 0;
    GotyeTool* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"GotyeTool",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (GotyeTool*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_GotyeTool_GotyeTool_joinGroup'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 1) 
    {
        int arg0;

        ok &= luaval_to_int32(tolua_S, 2,(int *)&arg0, "GotyeTool:joinGroup");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_GotyeTool_GotyeTool_joinGroup'", nullptr);
            return 0;
        }
        cobj->joinGroup(arg0);
        lua_settop(tolua_S, 1);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "GotyeTool:joinGroup",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_GotyeTool_GotyeTool_joinGroup'.",&tolua_err);
#endif

    return 0;
}
int lua_GotyeTool_GotyeTool_isInRoom(lua_State* tolua_S)
{
    int argc = 0;
    GotyeTool* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"GotyeTool",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (GotyeTool*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_GotyeTool_GotyeTool_isInRoom'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 1) 
    {
        int arg0;

        ok &= luaval_to_int32(tolua_S, 2,(int *)&arg0, "GotyeTool:isInRoom");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_GotyeTool_GotyeTool_isInRoom'", nullptr);
            return 0;
        }
        bool ret = cobj->isInRoom(arg0);
        tolua_pushboolean(tolua_S,(bool)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "GotyeTool:isInRoom",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_GotyeTool_GotyeTool_isInRoom'.",&tolua_err);
#endif

    return 0;
}
int lua_GotyeTool_GotyeTool_init(lua_State* tolua_S)
{
    int argc = 0;
    GotyeTool* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"GotyeTool",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (GotyeTool*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_GotyeTool_GotyeTool_init'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_GotyeTool_GotyeTool_init'", nullptr);
            return 0;
        }
        cobj->init();
        lua_settop(tolua_S, 1);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "GotyeTool:init",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_GotyeTool_GotyeTool_init'.",&tolua_err);
#endif

    return 0;
}
int lua_GotyeTool_GotyeTool_exit(lua_State* tolua_S)
{
    int argc = 0;
    GotyeTool* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"GotyeTool",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (GotyeTool*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_GotyeTool_GotyeTool_exit'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_GotyeTool_GotyeTool_exit'", nullptr);
            return 0;
        }
        cobj->exit();
        lua_settop(tolua_S, 1);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "GotyeTool:exit",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_GotyeTool_GotyeTool_exit'.",&tolua_err);
#endif

    return 0;
}
int lua_GotyeTool_GotyeTool_isOnline(lua_State* tolua_S)
{
    int argc = 0;
    GotyeTool* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"GotyeTool",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (GotyeTool*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_GotyeTool_GotyeTool_isOnline'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_GotyeTool_GotyeTool_isOnline'", nullptr);
            return 0;
        }
        bool ret = cobj->isOnline();
        tolua_pushboolean(tolua_S,(bool)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "GotyeTool:isOnline",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_GotyeTool_GotyeTool_isOnline'.",&tolua_err);
#endif

    return 0;
}
int lua_GotyeTool_GotyeTool_enterRoom(lua_State* tolua_S)
{
    int argc = 0;
    GotyeTool* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"GotyeTool",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (GotyeTool*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_GotyeTool_GotyeTool_enterRoom'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 1) 
    {
        int arg0;

        ok &= luaval_to_int32(tolua_S, 2,(int *)&arg0, "GotyeTool:enterRoom");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_GotyeTool_GotyeTool_enterRoom'", nullptr);
            return 0;
        }
        cobj->enterRoom(arg0);
        lua_settop(tolua_S, 1);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "GotyeTool:enterRoom",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_GotyeTool_GotyeTool_enterRoom'.",&tolua_err);
#endif

    return 0;
}
int lua_GotyeTool_GotyeTool_cancelTalk(lua_State* tolua_S)
{
    int argc = 0;
    GotyeTool* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"GotyeTool",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (GotyeTool*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_GotyeTool_GotyeTool_cancelTalk'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_GotyeTool_GotyeTool_cancelTalk'", nullptr);
            return 0;
        }
        cobj->cancelTalk();
        lua_settop(tolua_S, 1);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "GotyeTool:cancelTalk",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_GotyeTool_GotyeTool_cancelTalk'.",&tolua_err);
#endif

    return 0;
}
int lua_GotyeTool_GotyeTool_update(lua_State* tolua_S)
{
    int argc = 0;
    GotyeTool* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"GotyeTool",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (GotyeTool*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_GotyeTool_GotyeTool_update'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 1) 
    {
        double arg0;

        ok &= luaval_to_number(tolua_S, 2,&arg0, "GotyeTool:update");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_GotyeTool_GotyeTool_update'", nullptr);
            return 0;
        }
        cobj->update(arg0);
        lua_settop(tolua_S, 1);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "GotyeTool:update",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_GotyeTool_GotyeTool_update'.",&tolua_err);
#endif

    return 0;
}
int lua_GotyeTool_GotyeTool_dismissGroup(lua_State* tolua_S)
{
    int argc = 0;
    GotyeTool* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"GotyeTool",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (GotyeTool*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_GotyeTool_GotyeTool_dismissGroup'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 1) 
    {
        int arg0;

        ok &= luaval_to_int32(tolua_S, 2,(int *)&arg0, "GotyeTool:dismissGroup");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_GotyeTool_GotyeTool_dismissGroup'", nullptr);
            return 0;
        }
        cobj->dismissGroup(arg0);
        lua_settop(tolua_S, 1);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "GotyeTool:dismissGroup",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_GotyeTool_GotyeTool_dismissGroup'.",&tolua_err);
#endif

    return 0;
}
int lua_GotyeTool_GotyeTool_logout(lua_State* tolua_S)
{
    int argc = 0;
    GotyeTool* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"GotyeTool",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (GotyeTool*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_GotyeTool_GotyeTool_logout'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_GotyeTool_GotyeTool_logout'", nullptr);
            return 0;
        }
        cobj->logout();
        lua_settop(tolua_S, 1);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "GotyeTool:logout",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_GotyeTool_GotyeTool_logout'.",&tolua_err);
#endif

    return 0;
}
int lua_GotyeTool_GotyeTool_talkToRoom(lua_State* tolua_S)
{
    int argc = 0;
    GotyeTool* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"GotyeTool",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (GotyeTool*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_GotyeTool_GotyeTool_talkToRoom'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 1) 
    {
        int arg0;

        ok &= luaval_to_int32(tolua_S, 2,(int *)&arg0, "GotyeTool:talkToRoom");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_GotyeTool_GotyeTool_talkToRoom'", nullptr);
            return 0;
        }
        cobj->talkToRoom(arg0);
        lua_settop(tolua_S, 1);
        return 1;
    }
    if (argc == 2) 
    {
        int arg0;
        unsigned int arg1;

        ok &= luaval_to_int32(tolua_S, 2,(int *)&arg0, "GotyeTool:talkToRoom");

        ok &= luaval_to_uint32(tolua_S, 3,&arg1, "GotyeTool:talkToRoom");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_GotyeTool_GotyeTool_talkToRoom'", nullptr);
            return 0;
        }
        cobj->talkToRoom(arg0, arg1);
        lua_settop(tolua_S, 1);
        return 1;
    }
    if (argc == 3) 
    {
        int arg0;
        unsigned int arg1;
        bool arg2;

        ok &= luaval_to_int32(tolua_S, 2,(int *)&arg0, "GotyeTool:talkToRoom");

        ok &= luaval_to_uint32(tolua_S, 3,&arg1, "GotyeTool:talkToRoom");

        ok &= luaval_to_boolean(tolua_S, 4,&arg2, "GotyeTool:talkToRoom");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_GotyeTool_GotyeTool_talkToRoom'", nullptr);
            return 0;
        }
        cobj->talkToRoom(arg0, arg1, arg2);
        lua_settop(tolua_S, 1);
        return 1;
    }
    if (argc == 4) 
    {
        int arg0;
        unsigned int arg1;
        bool arg2;
        unsigned int arg3;

        ok &= luaval_to_int32(tolua_S, 2,(int *)&arg0, "GotyeTool:talkToRoom");

        ok &= luaval_to_uint32(tolua_S, 3,&arg1, "GotyeTool:talkToRoom");

        ok &= luaval_to_boolean(tolua_S, 4,&arg2, "GotyeTool:talkToRoom");

        ok &= luaval_to_uint32(tolua_S, 5,&arg3, "GotyeTool:talkToRoom");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_GotyeTool_GotyeTool_talkToRoom'", nullptr);
            return 0;
        }
        cobj->talkToRoom(arg0, arg1, arg2, arg3);
        lua_settop(tolua_S, 1);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "GotyeTool:talkToRoom",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_GotyeTool_GotyeTool_talkToRoom'.",&tolua_err);
#endif

    return 0;
}
int lua_GotyeTool_GotyeTool_getAutoPlayAudio(lua_State* tolua_S)
{
    int argc = 0;
    GotyeTool* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"GotyeTool",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (GotyeTool*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_GotyeTool_GotyeTool_getAutoPlayAudio'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_GotyeTool_GotyeTool_getAutoPlayAudio'", nullptr);
            return 0;
        }
        bool ret = cobj->getAutoPlayAudio();
        tolua_pushboolean(tolua_S,(bool)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "GotyeTool:getAutoPlayAudio",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_GotyeTool_GotyeTool_getAutoPlayAudio'.",&tolua_err);
#endif

    return 0;
}
int lua_GotyeTool_GotyeTool_isRoomSupportRealtime(lua_State* tolua_S)
{
    int argc = 0;
    GotyeTool* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"GotyeTool",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (GotyeTool*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_GotyeTool_GotyeTool_isRoomSupportRealtime'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 1) 
    {
        int arg0;

        ok &= luaval_to_int32(tolua_S, 2,(int *)&arg0, "GotyeTool:isRoomSupportRealtime");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_GotyeTool_GotyeTool_isRoomSupportRealtime'", nullptr);
            return 0;
        }
        bool ret = cobj->isRoomSupportRealtime(arg0);
        tolua_pushboolean(tolua_S,(bool)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "GotyeTool:isRoomSupportRealtime",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_GotyeTool_GotyeTool_isRoomSupportRealtime'.",&tolua_err);
#endif

    return 0;
}
int lua_GotyeTool_GotyeTool_setAutoPlayAudio(lua_State* tolua_S)
{
    int argc = 0;
    GotyeTool* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"GotyeTool",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (GotyeTool*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_GotyeTool_GotyeTool_setAutoPlayAudio'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 1) 
    {
        bool arg0;

        ok &= luaval_to_boolean(tolua_S, 2,&arg0, "GotyeTool:setAutoPlayAudio");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_GotyeTool_GotyeTool_setAutoPlayAudio'", nullptr);
            return 0;
        }
        cobj->setAutoPlayAudio(arg0);
        lua_settop(tolua_S, 1);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "GotyeTool:setAutoPlayAudio",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_GotyeTool_GotyeTool_setAutoPlayAudio'.",&tolua_err);
#endif

    return 0;
}
int lua_GotyeTool_GotyeTool_createGroup(lua_State* tolua_S)
{
    int argc = 0;
    GotyeTool* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"GotyeTool",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (GotyeTool*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_GotyeTool_GotyeTool_createGroup'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 2) 
    {
        const char* arg0;
        const char* arg1;

        std::string arg0_tmp; ok &= luaval_to_std_string(tolua_S, 2, &arg0_tmp, "GotyeTool:createGroup"); arg0 = arg0_tmp.c_str();

        std::string arg1_tmp; ok &= luaval_to_std_string(tolua_S, 3, &arg1_tmp, "GotyeTool:createGroup"); arg1 = arg1_tmp.c_str();
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_GotyeTool_GotyeTool_createGroup'", nullptr);
            return 0;
        }
        cobj->createGroup(arg0, arg1);
        lua_settop(tolua_S, 1);
        return 1;
    }
    if (argc == 3) 
    {
        const char* arg0;
        const char* arg1;
        bool arg2;

        std::string arg0_tmp; ok &= luaval_to_std_string(tolua_S, 2, &arg0_tmp, "GotyeTool:createGroup"); arg0 = arg0_tmp.c_str();

        std::string arg1_tmp; ok &= luaval_to_std_string(tolua_S, 3, &arg1_tmp, "GotyeTool:createGroup"); arg1 = arg1_tmp.c_str();

        ok &= luaval_to_boolean(tolua_S, 4,&arg2, "GotyeTool:createGroup");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_GotyeTool_GotyeTool_createGroup'", nullptr);
            return 0;
        }
        cobj->createGroup(arg0, arg1, arg2);
        lua_settop(tolua_S, 1);
        return 1;
    }
    if (argc == 4) 
    {
        const char* arg0;
        const char* arg1;
        bool arg2;
        bool arg3;

        std::string arg0_tmp; ok &= luaval_to_std_string(tolua_S, 2, &arg0_tmp, "GotyeTool:createGroup"); arg0 = arg0_tmp.c_str();

        std::string arg1_tmp; ok &= luaval_to_std_string(tolua_S, 3, &arg1_tmp, "GotyeTool:createGroup"); arg1 = arg1_tmp.c_str();

        ok &= luaval_to_boolean(tolua_S, 4,&arg2, "GotyeTool:createGroup");

        ok &= luaval_to_boolean(tolua_S, 5,&arg3, "GotyeTool:createGroup");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_GotyeTool_GotyeTool_createGroup'", nullptr);
            return 0;
        }
        cobj->createGroup(arg0, arg1, arg2, arg3);
        lua_settop(tolua_S, 1);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "GotyeTool:createGroup",argc, 2);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_GotyeTool_GotyeTool_createGroup'.",&tolua_err);
#endif

    return 0;
}
int lua_GotyeTool_GotyeTool_login(lua_State* tolua_S)
{
    int argc = 0;
    GotyeTool* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"GotyeTool",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (GotyeTool*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_GotyeTool_GotyeTool_login'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 1) 
    {
        const char* arg0;

        std::string arg0_tmp; ok &= luaval_to_std_string(tolua_S, 2, &arg0_tmp, "GotyeTool:login"); arg0 = arg0_tmp.c_str();
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_GotyeTool_GotyeTool_login'", nullptr);
            return 0;
        }
        cobj->login(arg0);
        lua_settop(tolua_S, 1);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "GotyeTool:login",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_GotyeTool_GotyeTool_login'.",&tolua_err);
#endif

    return 0;
}
int lua_GotyeTool_GotyeTool_destroyInstance(lua_State* tolua_S)
{
    int argc = 0;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif

#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertable(tolua_S,1,"GotyeTool",0,&tolua_err)) goto tolua_lerror;
#endif

    argc = lua_gettop(tolua_S) - 1;

    if (argc == 0)
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_GotyeTool_GotyeTool_destroyInstance'", nullptr);
            return 0;
        }
        GotyeTool::destroyInstance();
        lua_settop(tolua_S, 1);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d\n ", "GotyeTool:destroyInstance",argc, 0);
    return 0;
#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_GotyeTool_GotyeTool_destroyInstance'.",&tolua_err);
#endif
    return 0;
}
int lua_GotyeTool_GotyeTool_getInstance(lua_State* tolua_S)
{
    int argc = 0;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif

#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertable(tolua_S,1,"GotyeTool",0,&tolua_err)) goto tolua_lerror;
#endif

    argc = lua_gettop(tolua_S) - 1;

    if (argc == 0)
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_GotyeTool_GotyeTool_getInstance'", nullptr);
            return 0;
        }
        GotyeTool* ret = GotyeTool::getInstance();
        object_to_luaval<GotyeTool>(tolua_S, "GotyeTool",(GotyeTool*)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d\n ", "GotyeTool:getInstance",argc, 0);
    return 0;
#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_GotyeTool_GotyeTool_getInstance'.",&tolua_err);
#endif
    return 0;
}
int lua_GotyeTool_GotyeTool_constructor(lua_State* tolua_S)
{
    int argc = 0;
    GotyeTool* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif



    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_GotyeTool_GotyeTool_constructor'", nullptr);
            return 0;
        }
        cobj = new GotyeTool();
        cobj->autorelease();
        int ID =  (int)cobj->_ID ;
        int* luaID =  &cobj->_luaID ;
        toluafix_pushusertype_ccobject(tolua_S, ID, luaID, (void*)cobj,"GotyeTool");
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "GotyeTool:GotyeTool",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_error(tolua_S,"#ferror in function 'lua_GotyeTool_GotyeTool_constructor'.",&tolua_err);
#endif

    return 0;
}

static int lua_GotyeTool_GotyeTool_finalize(lua_State* tolua_S)
{
    printf("luabindings: finalizing LUA object (GotyeTool)");
    return 0;
}

int lua_register_GotyeTool_GotyeTool(lua_State* tolua_S)
{
    tolua_usertype(tolua_S,"GotyeTool");
    tolua_cclass(tolua_S,"GotyeTool","GotyeTool","cc.Ref",nullptr);

    tolua_beginmodule(tolua_S,"GotyeTool");
        tolua_function(tolua_S,"new",lua_GotyeTool_GotyeTool_constructor);
        tolua_function(tolua_S,"leaveRoom",lua_GotyeTool_GotyeTool_leaveRoom);
        tolua_function(tolua_S,"unregisterEventHandler",lua_GotyeTool_GotyeTool_unregisterEventHandler);
        tolua_function(tolua_S,"stopTalk",lua_GotyeTool_GotyeTool_stopTalk);
        tolua_function(tolua_S,"getTalkingPower",lua_GotyeTool_GotyeTool_getTalkingPower);
        tolua_function(tolua_S,"talkToUser",lua_GotyeTool_GotyeTool_talkToUser);
        tolua_function(tolua_S,"talkToGroup",lua_GotyeTool_GotyeTool_talkToGroup);
        tolua_function(tolua_S,"joinGroup",lua_GotyeTool_GotyeTool_joinGroup);
        tolua_function(tolua_S,"isInRoom",lua_GotyeTool_GotyeTool_isInRoom);
        tolua_function(tolua_S,"init",lua_GotyeTool_GotyeTool_init);
        tolua_function(tolua_S,"exit",lua_GotyeTool_GotyeTool_exit);
        tolua_function(tolua_S,"isOnline",lua_GotyeTool_GotyeTool_isOnline);
        tolua_function(tolua_S,"enterRoom",lua_GotyeTool_GotyeTool_enterRoom);
        tolua_function(tolua_S,"cancelTalk",lua_GotyeTool_GotyeTool_cancelTalk);
        tolua_function(tolua_S,"update",lua_GotyeTool_GotyeTool_update);
        tolua_function(tolua_S,"dismissGroup",lua_GotyeTool_GotyeTool_dismissGroup);
        tolua_function(tolua_S,"logout",lua_GotyeTool_GotyeTool_logout);
        tolua_function(tolua_S,"talkToRoom",lua_GotyeTool_GotyeTool_talkToRoom);
        tolua_function(tolua_S,"getAutoPlayAudio",lua_GotyeTool_GotyeTool_getAutoPlayAudio);
        tolua_function(tolua_S,"isRoomSupportRealtime",lua_GotyeTool_GotyeTool_isRoomSupportRealtime);
        tolua_function(tolua_S,"setAutoPlayAudio",lua_GotyeTool_GotyeTool_setAutoPlayAudio);
        tolua_function(tolua_S,"createGroup",lua_GotyeTool_GotyeTool_createGroup);
        tolua_function(tolua_S,"login",lua_GotyeTool_GotyeTool_login);
        tolua_function(tolua_S,"destroyInstance", lua_GotyeTool_GotyeTool_destroyInstance);
        tolua_function(tolua_S,"getInstance", lua_GotyeTool_GotyeTool_getInstance);
    tolua_endmodule(tolua_S);
    std::string typeName = typeid(GotyeTool).name();
    g_luaType[typeName] = "GotyeTool";
    g_typeCast["GotyeTool"] = "GotyeTool";
    return 1;
}
TOLUA_API int register_all_GotyeTool(lua_State* tolua_S)
{
	tolua_open(tolua_S);
	
	tolua_module(tolua_S,nullptr,0);
	tolua_beginmodule(tolua_S,nullptr);

	lua_register_GotyeTool_GotyeTool(tolua_S);

	tolua_endmodule(tolua_S);
	return 1;
}

