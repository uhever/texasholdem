#include "GotyeTool.h"

#include "scripting/lua-bindings/manual/CCLuaEngine.h"

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    #include "platform/android/jni/JniHelper.h"
    #include <jni.h>
    #include <android/log.h>
#endif

// singleton stuff
static GotyeTool *s_SharedTool = nullptr;

GotyeTool* GotyeTool::getInstance()
{
    if (!s_SharedTool)
    {
        s_SharedTool = new (std::nothrow) GotyeTool();
        CCASSERT(s_SharedTool, "FATAL: Not enough memory");
        
        s_SharedTool->init();
    }
    return s_SharedTool;
}

void GotyeTool::destroyInstance()
{
    apiist->removeAllListeners();
    
    CC_SAFE_RELEASE_NULL(s_SharedTool);
}

GotyeTool::GotyeTool()
:_isAutoPlayAudio(true),
_isCancelTalk(false),
_eventHandler(0)
{
}

GotyeTool::~GotyeTool()
{
    #if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
        Director::getInstance()->getScheduler()->unschedule(CC_SCHEDULE_SELECTOR(GotyeTool::update), this);
    #endif
    
    CCLOGINFO("deallocing GotyeTool: %p", this);
}


template<class T>
string convertToString(const T val)
{
    string s;
    std::stringstream ss;
    ss << val;
    ss >> s;
    return s;
}

// 转换
GotyeWhineMode convertWhineMode(unsigned int whineMode)
{
    GotyeWhineMode convertWhineMode;
    switch (whineMode) {
        case 1:
            whineMode = GotyeWhineModeFatherChristmas;
            break;
        case 2:
            whineMode = GotyeWhineModeOptimus;
            break;
        case 3:
            whineMode = GotyeWhineModeDolphine;
            break;
        case 4:
            whineMode = GotyeWhineModeBaby;
            break;
        case 5:
            whineMode = GotyeWhineModeSubwoofer;
            break;
        default:
            whineMode =  GotyeWhineModeDefault;
            break;
    }
    return convertWhineMode;
}

// 初始化
void GotyeTool::init()
{
    
    apiist->addListener(*s_SharedTool);
    
    #if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
        apiist->setJVM(JniHelper::getJavaVM());///< 设置JVM
        Director::getInstance()->getScheduler()->schedule(CC_SCHEDULE_SELECTOR(GotyeTool::update), this, 0.0f, false);
    #endif
    
}


void GotyeTool::update(float dt)
{
    #if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
        apiist->mainLoop();/* api在主线程循环驱动下，将各类异步回调通知到调用层 */
    #endif
}

// 注册lua回调
void GotyeTool::registerEventHandler(int handler)
{
    unregisterEventHandler();
    
    _eventHandler = handler;
    
}

// 移除lua回调
void GotyeTool::unregisterEventHandler()
{
    if (0 != _eventHandler)
    {
        cocos2d::LuaEngine::getInstance()->removeScriptHandler(_eventHandler);
        _eventHandler = 0;
    }
}

// 设置是否自动播放语音
void GotyeTool::setAutoPlayAudio(bool isAutoPlayAudio)
{
    _isAutoPlayAudio = isAutoPlayAudio;
}
bool GotyeTool::getAutoPlayAudio() const
{
    return _isAutoPlayAudio;
}

// 取消语音
void GotyeTool::cancelTalk()
{
    _isCancelTalk = true;
    
    // 取消talk
    stopTalk();
    
}

// 退出销毁
void GotyeTool::exit()
{
    #if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
        Director::getInstance()->getScheduler()->unschedule(CC_SCHEDULE_SELECTOR(GotyeTool::update), this);
    #endif
    
    apiist->exit();
}

// 登陆
void GotyeTool::login(const char* username)
{
    apiist->login(username);
}

// 登出
void GotyeTool::logout()
{
    apiist->logout();
}

// 检测是否在线
bool GotyeTool::isOnline()
{
    int status = apiist->isOnline();
    if (status)
        return true;
    return false;
}


// 跟单人说话
void GotyeTool::talkToUser(const char* username, unsigned int whineMode, bool realtime, unsigned int maxDuration)
{
    GotyeUser user(username);
    apiist->startTalk(user, convertWhineMode(whineMode), realtime, maxDuration);
}

// 跟群组说话
void GotyeTool::talkToGroup(int groupId, unsigned int whineMode, bool realtime, unsigned int maxDuration)
{
    GotyeGroup group(groupId);
    apiist->startTalk(group, convertWhineMode(whineMode), realtime, maxDuration);
}

// 跟聊天室说话
void GotyeTool::talkToRoom(int roomId, unsigned int whineMode, bool realtime, unsigned int maxDuration)
{
    GotyeRoom room(roomId);
    apiist->startTalk(room, convertWhineMode(whineMode), realtime, maxDuration);
}

// 获取说话音量
int GotyeTool::getTalkingPower() const
{
    return apiist->getTalkingPower();
}

// 停止说话
void GotyeTool::stopTalk()
{
    apiist->stopTalk();
}

// 创建群组
void GotyeTool::createGroup(const char* name, const char* info, bool isPublic, bool needAuthentication)
{
    GotyeGroupType ownerType = GotyeGroupTypePublic;
    if (isPublic == false)
        ownerType = GotyeGroupTypePrivate;
    
    GotyeGroup group;
    group.name = name; /// <设定群名字
    group.info = info; /// <设定群扩展信息
    group.ownerType = ownerType; /// <设定群类型(公共群/私有群)
    group.needAuthentication = needAuthentication; /// <是否需要验证才能加入
    apiist->createGroup(group);/// <对应回调GotyeDelegate::onCreateGroup
}

// 加入群组
void GotyeTool::joinGroup(int groupId)
{
    GotyeGroup group(groupId);
    apiist->joinGroup(group);
}

// 解散群组
void GotyeTool::dismissGroup(int groupId)
{
    GotyeGroup group(groupId);
    apiist->dismissGroup(group);/// <对应回调GotyeDelegate::onDismissGroup
}

// 进入聊天室
void GotyeTool::enterRoom(int roomId)
{
    GotyeRoom room(roomId);
    apiist->enterRoom(room);
}

// 退出聊天室
void GotyeTool::leaveRoom(int roomId)
{
    GotyeRoom room(roomId);
    apiist->leaveRoom(room);
}

// 是否在聊天室
bool GotyeTool::isInRoom(int roomId) const
{
    GotyeRoom room(roomId);
    return apiist->isInRoom(room);
}

// 聊天室是否支持实时语音
bool GotyeTool::isRoomSupportRealtime(int roomId) const
{
    GotyeRoom room(roomId);
    return apiist->supportRealtime(room);
}



#pragma mark - Delegate


// 执行lua回调
void GotyeTool::callLuaHandler(const ValueMap& dict)
{
    if (0 != _eventHandler)
    {
        // 测试代码
        auto stack = cocos2d::LuaEngine::getInstance()->getLuaStack();
        //    stack->pushObject(this, "GotyeTool");
        //    stack->pushInt(555);
        //    stack->pushString("hahahahah");
        //    stack->executeFunctionByHandler(_eventHandler, 2);
        
        // debug
        cout << "GotyeTool-Event: name = " << dict.at("name").asString() << ", status = " << dict.at("status").asString() << endl;
        
        // 测试代码
        cocos2d::LuaValueDict eventData;
        
        for (auto &a : dict)
        {
            eventData[a.first] = cocos2d::LuaValue::stringValue(a.second.asString());
        }
        
        stack->pushLuaValueDict(eventData);
        
        stack->executeFunctionByHandler(_eventHandler, 1);

    }
}


// 登陆成功回调
void GotyeTool::onLogin(GotyeStatusCode code, const GotyeLoginUser& user)
{
    ValueMap map;
    map["name"] = "onLogin";
    
    if(code == GotyeStatusCodeOK || code == GotyeStatusCodeReloginOK || code == GotyeStatusCodeOfflineLoginOK){
        map["status"] = "1";
    }else{
        map["status"] = "0";
    }
    
    callLuaHandler(map);
}

// 退出登陆回调
void GotyeTool::onLogout(GotyeStatusCode code)
{
    ValueMap map;
    map["name"] = "onLogout";

    if(code == GotyeStatusCodeOK){
        map["status"] = "1";
    }else{
        map["status"] = "0";
    }
    callLuaHandler(map);
}

// 发送消息成功回调
void GotyeTool::onSendMessage(GotyeStatusCode code, const GotyeMessage& message)
{
    ValueMap map;
    map["name"] = "onSendMessage";
    
    if(code == GotyeStatusCodeOK){
        map["status"] = "1";
    }else{
        map["status"] = "0";
    }
    callLuaHandler(map);
}

// 接受消息成功回调
void GotyeTool::onReceiveMessage(const GotyeMessage& message, bool* downloadMediaIfNeed)
{
    *downloadMediaIfNeed = true; // true - 如有媒体文件，主动下载
    
    ValueMap map;
    map["name"] = "onReceiveMessage";
    map["status"] = "1";

    callLuaHandler(map);
}

// 媒体文件下载完毕回调
void GotyeTool::onDownloadMediaInMessage(GotyeStatusCode code, const GotyeMessage& message)
{
    ValueMap map;
    map["name"] = "onDownloadMediaInMessage";
    
    if(code == GotyeStatusCodeOK){
        map["status"] = "1";
    }else{
        map["status"] = "0";
    }
    
    // 自动播放媒体
    if (code == GotyeStatusCodeOK && message.type == GotyeMessageTypeAudio && _isAutoPlayAudio == true){
        apiist->playMessage(message); /// <对应回调GotyeDelegate::onPlayStart，onPlaying和onPlayStop
    }
    
    callLuaHandler(map);
}

// 播放开始回调
void GotyeTool::onPlayStart(GotyeStatusCode code, const GotyeMessage &message)
{
    ValueMap map;
    map["name"] = "onPlayStart";
    
    if(code == GotyeStatusCodeOK){
        map["status"] = "1";
        map["username"] = message.sender.name;
    }else{
        map["status"] = "0";
    }
    callLuaHandler(map);
}

// 实施模式播放回调
void GotyeTool::onRealPlayStart(GotyeStatusCode code, const GotyeUser& speaker, const GotyeRoom& room)
{
    ValueMap map;
    map["name"] = "onRealPlayStart";
    
    if(code == GotyeStatusCodeOK){
        map["status"] = "1";
        map["username"] = speaker.name;
    }else{
        map["status"] = "0";
    }
    callLuaHandler(map);
}

// 正在播放回调
void GotyeTool::onPlaying(long position)
{
    ValueMap map;
    map["name"] = "onPlaying";
    map["status"] = "1";
    map["position"] = convertToString(position);

    callLuaHandler(map);
}

// 播放完毕回调
void GotyeTool::onPlayStop()
{
    ValueMap map;
    map["name"] = "onPlayStop";
    map["status"] = "1";
    
    callLuaHandler(map);
}

// 录音开始回调
void GotyeTool::onStartTalk(GotyeStatusCode code, GotyeChatTarget target, bool realtime)
{
    ValueMap map;
    map["name"] = "onStartTalk";
    
    if(code == GotyeStatusCodeOK){
        _isCancelTalk = false;
        
        map["status"] = "1";
        map["realtime"] = convertToString(realtime);
    }else{
        map["status"] = "0";
    }
    callLuaHandler(map);
}

// 录音结束回调
void GotyeTool::onStopTalk(GotyeStatusCode code, bool realtime, GotyeMessage& message, bool *cancelSending)
{
    ValueMap map;
    map["name"] = "onStopTalk";
    
    if(code == GotyeStatusCodeOK){
        if (_isCancelTalk){
            *cancelSending = true;
            _isCancelTalk = false;
        }
        
        map["status"] = "1";
        map["realtime"] = convertToString(realtime);
        
    }else{
        map["status"] = "0";
    }
    callLuaHandler(map);
    
}

// 创建群组成功回调
void GotyeTool::onCreateGroup(GotyeStatusCode code, const GotyeGroup& group)
{
    ValueMap map;
    map["name"] = "onCreateGroup";
    
    if(code == GotyeStatusCodeOK){
        map["status"] = "1";
        map["groupId"] = convertToString(group.id);
    }else{
        map["status"] = "0";
    }
    callLuaHandler(map);
    
}

// 加入群回调
void GotyeTool::onJoinGroup(GotyeStatusCode code, const GotyeGroup& group)
{
    ValueMap map;
    map["name"] = "onJoinGroup";
    
    if(code == GotyeStatusCodeOK){
        map["status"] = "1";
        map["groupId"] = convertToString(group.id);
    }else{
        map["status"] = "0";
    }
    callLuaHandler(map);
}

// 离开群回调
void GotyeTool::onLeaveGroup(GotyeStatusCode code, const GotyeGroup& group)
{
    ValueMap map;
    map["name"] = "onLeaveGroup";
    
    if(code == GotyeStatusCodeOK){
        map["status"] = "1";
        map["groupId"] = convertToString(group.id);
    }else{
        map["status"] = "0";
    }
    callLuaHandler(map);
}

// 解散群回调
void GotyeTool::onDismissGroup(GotyeStatusCode code, const GotyeGroup& group)
{
    ValueMap map;
    map["name"] = "onDismissGroup";
    
    if(code == GotyeStatusCodeOK){
        map["status"] = "1";
        map["groupId"] = convertToString(group.id);
    }else{
        map["status"] = "0";
    }
    callLuaHandler(map);
}

// 进入聊天室
void GotyeTool::onEnterRoom(GotyeStatusCode code, GotyeRoom& room)
{
    ValueMap map;
    map["name"] = "onEnterRoom";
    
    if(code == GotyeStatusCodeOK){
        map["status"] = "1";
        map["roomId"] = convertToString(room.id);
    }else{
        map["status"] = "0";
    }
    callLuaHandler(map);
}


// 离开聊天室
void GotyeTool::onLeaveRoom(GotyeStatusCode code, GotyeRoom& room)
{
    ValueMap map;
    map["name"] = "onLeaveRoom";
    
    if(code == GotyeStatusCodeOK){
        map["status"] = "1";
        map["roomId"] = convertToString(room.id);
    }else{
        map["status"] = "0";
    }
    callLuaHandler(map);
}

