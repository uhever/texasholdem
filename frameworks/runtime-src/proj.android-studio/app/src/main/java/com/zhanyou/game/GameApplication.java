package com.zhanyou.game;

import android.app.Application;
import android.widget.Toast;

import com.zhanshow.library.*;
import com.zhanshow.library.utils.PackageUtils;
import com.zhanyou.bugreport.BugReportManager;
import com.zhanyou.statistics.StatisticsManager;

/**
 * Created by kay on 16/12/15.
 */

public class GameApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        //Bugly初始化
        BugReportManager.getInstance().initBugReport(getApplicationContext(),"8ce3ca0dcd",Config.getUmengChannel(this));
        //友盟初始化
        XLog.d("umeng_appkey: " + BuildConfig.UMENG_APPKEY);
        StatisticsManager.getInstance().init(getApplicationContext(), BuildConfig.UMENG_APPKEY, Config.getUmengChannel(this));
    }
}
