package com.zhanyou.poker.laiqudoudizhu.wxapi;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.tencent.mm.sdk.constants.ConstantsAPI;
import com.tencent.mm.sdk.modelbase.BaseReq;
import com.tencent.mm.sdk.modelbase.BaseResp;
import com.tencent.mm.sdk.openapi.IWXAPI;
import com.tencent.mm.sdk.openapi.IWXAPIEventHandler;
import com.tencent.mm.sdk.openapi.WXAPIFactory;
import com.zhanshow.library.Config;
import com.zhanshow.library.RxBus.BusProvider;
import com.zhanshow.library.RxBus.BusTag;

/**
 * Created by kay on 17/1/16.
 */

public class WXEntryActivity extends Activity implements IWXAPIEventHandler {
    private IWXAPI api;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        api = WXAPIFactory.createWXAPI(this, Config.getWechatAppId(this), true);
        api.handleIntent(this.getIntent(), this);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        api.handleIntent(intent, this);
    }
    @Override
    public void onReq(BaseReq baseReq) {
        Log.d("WXEntryActivity","type: " + baseReq.getType());
    }

    @Override
    public void onResp(BaseResp baseResp) {
        Log.d("WXEntryActivity","type: " + baseResp.getType() + ";errorCode: " + baseResp.errCode + ";errorMsg: " + baseResp.errStr);
        if(baseResp.getType()== ConstantsAPI.COMMAND_SENDAUTH){
            switch (baseResp.errCode) {
                case BaseResp.ErrCode.ERR_OK:
                    String code = getIntent().getExtras().getString("_wxapi_sendauth_resp_token");
                    BusProvider.getInstance().post(BusTag.WECHAT_LOGIN_AUTH_SUCCESS,code);
                    break;
                case BaseResp.ErrCode.ERR_USER_CANCEL:
                default:
                    BusProvider.getInstance().post(BusTag.WECHAT_LOGIN_AUTH_ERROR,String.valueOf(baseResp.errCode));
                    break;
            }

        }else if(baseResp.getType()==ConstantsAPI.COMMAND_SENDMESSAGE_TO_WX){
            BusProvider.getInstance().post(BusTag.WECHAT_SHARE_CALLBACK,String.valueOf(baseResp.errCode));
        }
        finish();
    }
}
