package com.zhanyou.kay.youchat.bean.game;

import java.util.List;

/**
 * Created by kay on 16/7/13.
 */
public class LoginNetEaseData {
    private int status;
    private String token;
    private String uid;
    private String nimtoken;
    private String new_member;

    public String getNew_member() {
        return new_member;
    }

    public void setNew_member(String new_member) {
        this.new_member = new_member;
    }

    private List<NetEaseUserData> reg_give;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getNimtoken() {
        return nimtoken;
    }

    public void setNimtoken(String nimtoken) {
        this.nimtoken = nimtoken;
    }

    public List<NetEaseUserData> getReg_give() {
        return reg_give;
    }

    public void setReg_give(List<NetEaseUserData> reg_give) {
        this.reg_give = reg_give;
    }

    public static  class NetEaseUserData {

    }
}
