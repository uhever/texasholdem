package com.zhanshow.library.utils;

import com.zhanshow.library.LibraryConfig;

/**
 * @author weilinhu
 */

public class ZLUrl {


    /**
     *
     * 返回带参数的url,只适合不带参数的url
     *
     * @param url 普通url
     * @param uid uid
     * @param app_skin 皮肤
     * @param token token
     * @return url和参数
     */
    public static String getFullUrl(String url,String uid,String app_skin,String token){

        return new StringBuilder().append(url)
                .append("?app_channel_id=2&app_token_uid=")
                .append(uid)
                .append("&app_version=")
                .append(String.valueOf(PackageUtils.getVersionCode(LibraryConfig.getInstance().getContext())))
                .append("&app_skin=")
                .append(app_skin)
                .append("&token=")
                .append(token)
                .append("&uid=")
                .append(uid).toString();

    }
}
