package com.zhanshow.library;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Sha1加密文本
 * @author yukai 2015-07-31
 */
public class Sha1 {  
	public static String encode(String text){
		try {
			return hexString(eccryptSHA1(text));
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			return null;
		}
	}
	private static byte[] eccryptSHA1(String info) throws NoSuchAlgorithmException {   
		return eccrypt(info, "SHA1");    
	}  
	//byte字节转换成16进制的字符串MD5Utils.hexString      
	private static byte[] eccrypt(String info, String shaType) throws NoSuchAlgorithmException {
		MessageDigest sha = MessageDigest.getInstance(shaType);       
		byte[] srcBytes = info.getBytes();        
		// 使用srcBytes更新摘要        
		sha.update(srcBytes);        
		// 完成哈希计算，得到result       
		byte[] resultBytes = sha.digest();        
		return resultBytes;    
	}    
	
	private static String hexString(byte[] bytes){    
		StringBuffer hexValue = new StringBuffer();            
		for (int i = 0; i < bytes.length; i++) {              
			int val = ((int) bytes[i]) & 0xff;             
			if (val < 16)                  
				hexValue.append("0");              
			hexValue.append(Integer.toHexString(val));          
		}          
		return hexValue.toString();     
	}
	
//	public byte[] eccryptSHA256(String info) throws NoSuchAlgorithmException {   
//		return eccrypt(info, "SHA-256");   
//		}    
//	public byte[] eccryptSHA384(String info) throws NoSuchAlgorithmException {    
//		return eccrypt(info, "SHA-384");    }  
//	public byte[] eccryptSHA512(String info) throws NoSuchAlgorithmException {    
//		return eccrypt(info, "SHA-512");    }   
//	public static void main(String[] args) throws NoSuchAlgorithmException {   
//		String msg = "zhancheng";        
//		Sha1 sha = new Sha1();        
//		String sha1=sha.hexString(sha.eccryptSHA1(msg));        
//		}  
	
}
