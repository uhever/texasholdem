package com.zhanshow.library.RxBus;

/**
 * @author weilinhu
 */
public class BusTag {

    //未读消息更新
    public static final String UN_READ_UPDATE = "un_read_update";
    //关注的主播正在直播
    public static final String YOUR_FRIEND_WAS_LIVING = "your_friend_was_living";
    public static final String GIFT_BY_USER_ACTION = "gift_by_user_action";
    public static final String SHARE_SUCCESS_CALLBACK = "share_success_callback";
    public static final String SHARE_FAILED_CALLBACK = "share_failed_callback";
    public static final String SHARE_CANCEL_CALLBACK = "share_cancel_callback";
    public static final String SHOW_TIP_TEXT_RETURN = "show_tip_text_return";
    //系统管理员消息 1是禁播,2是通知观看者退出房间，3是禁言,4是给主播警告,6是解除禁言
    public static final String ADMINI_NOTITE = "admini_notice";
    //圈子消息通知
    public static final String MONMENT_NOTICE = "monment_notice";
    //私信送礼物
    public static final String GIFT_PRIVATE_SEND = "gift_private_send";
    //展示私信页面礼物对话框
    public static final String GIFT_PRIVATE_SHOW = "gift_private_show";
    //守护界面通知好观看界面 发送购买守护的自定义消息
    public static final String HANDLE_GUARD_MSG = "handle_guard_msg";
    //跳转到我发布的圈子附近
    public static final String GOTO_MY_PUBLISH = "goto_my_publish";
    //删除一条圈子
    public static final String DELETE_CIRCLE_ITEM = "delete_circle_item";
    //更新一条圈子
    public static final String UPDATE_CIRCLE_ITEM = "update_circle_item";
    //当前云信登录状态为登录状态
    public static final String NETEASE_STATUS_LOGINED = "netease_status_logined";
    //主播关闭直播刷新最新和关注列表
    public static final String ANCHOR_CLOSE_LIVE_REFRESH_HALL = "anchor_close_live_refresh_hall";
    //私信发言等级限制
    public static final String PRIVATE_MSG_LEVEL = "private_msg_level";
    //私信次数限制
    public static final String PRIVATE_MSG_NUM = "private_msg_num";
    //云信被踢下或者后端错误码是12，13，14；或者展游平台错误码2004，2005
    public static final String NETEASE_KICKOFF = "neteasekickoff";
    //云信被踢下或者后端错误码是12，13，14；或者展游平台错误码2004，2005且正在直播
    public static final String NETEASE_KICKOFF_LIVING = "neteasekickoff_living";

    public static final String MSG_TOO_FASE = "msg_too_fase";

    //微信登录失败
    public static final String WECHAT_LOGIN_AUTH_ERROR = "wx_login_auth_error";
    //微信登录回调
    public static final String WECHAT_LOGIN_AUTH_SUCCESS = "wx_login_auth_success";
    //微信登录回调
    public static final String WECHAT_SHARE_CALLBACK = "wx_share_callback";
    //定位成功回调
    public static final String LOCATION_SUCCESS_CALLBACK = "location_success_callback";
    ////定位失败回调
    public static final String LOCATION_FAILED_CALLBACK = "location_failed_callback";

    //发送圈子通知消息的数目
    public static final String MONMENT_MESSAGE_NUMBER = "monment_message_number";
    //管理员私信，显示对话框
    public static final String ADMIN_PRIVATE_CHAT = "admin_private_chat";
}
