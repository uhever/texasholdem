package com.zhanshow.library.game;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;

import com.iapppay.interfaces.callback.IPayResultCallback;
import com.iapppay.sdk.main.IAppPay;
import com.zhanshow.library.Config;
import com.zhanshow.library.RequestHelper;
import com.zhanshow.library.XLog;
import com.zhanyou.kay.youchat.bean.game.GameOrderBean;
import com.zhanyou.kay.youchat.bean.game.GameRequestCode;
import com.zhanyou.kay.youchat.bean.game.GameUCMobileLoginData;
import com.zhanyou.kay.youchat.bean.game.GameUCWechatLoginData;
import com.zhanyou.kay.youchat.bean.game.LoginData;
import com.zhanyou.kay.youchat.bean.game.LoginNetEaseData;
import com.zhanyou.kay.youchat.bean.game.UCAiBeiBean;
import com.zhanyou.kay.youchat.bean.game.GameUCResponse;
import com.zhanshow.library.okhttp.GameHttpBaseParamsLoggingInterceptor;
import com.zhanshow.library.okhttp.GameHttpLoggingInterceptor;
import com.zhanshow.library.sp.SharedPreferencesUtils;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;


import okhttp3.OkHttpClient;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;

/**
 * Created by kay on 16/12/13.
 */

public class GameRequestManager {
    private static String sdkSession;
    private static String sdkAuthkey;

    private static GameRequestManager instance;
    private static Object INSTANCE_LOCK = new Object();
    private Subscription subscription;

    private static GameRequestApi gameRequestApi;
    private GameRequestManager(){

    }

    public static GameRequestManager getInstance(Context context){
        if(instance==null){
            synchronized (INSTANCE_LOCK){
                instance = new GameRequestManager();
                initGameRequestApi(context);
            }
        }
        return instance;
    }

    private static void initGameRequestApi(Context context) {
        OkHttpClient.Builder builder =
                new OkHttpClient.Builder().connectTimeout(20 * 1000, TimeUnit.MILLISECONDS)
                        .readTimeout(20 * 1000, TimeUnit.MILLISECONDS);
        //添加日志拦截器
        GameHttpLoggingInterceptor logging = new GameHttpLoggingInterceptor();
        logging.setLevel(GameHttpLoggingInterceptor.Level.BODY);
        builder.addInterceptor(logging);
        //添加请求url后面加三个参数的拦截器
        GameHttpBaseParamsLoggingInterceptor httpBaseParamsLoggingInterceptor = null;
        try {
            httpBaseParamsLoggingInterceptor = new GameHttpBaseParamsLoggingInterceptor(
                    context,
                    context.getPackageManager().getPackageInfo(context.getPackageName(),0).versionCode);
            builder.addInterceptor(httpBaseParamsLoggingInterceptor);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        gameRequestApi = new GameRequestApi(new RequestHelper(context),builder.build());
    }

    /**
     * 创建支付订单
     * @param context
     * @param token
     * @param type
     * @param productId
     * @param appid
     * @param server_poker
     * @param gameResponse
     */
    public void createGameOrder(Context context,String token,String type,String productId,String appid,String server_poker,final GameResponse gameResponse){
        subscription = gameRequestApi.createPokerOrder(token,productId,type,appid,server_poker)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<GameOrderBean>() {
                    @Override
                    public void call(GameOrderBean gameOrderBean) {
                        if(gameOrderBean.getStatus()==1){
                            gameResponse.onResponse(GameRequestCode.CREATE_PAY_ORDER_SUCCESS,"success",gameOrderBean);
                        }else{
                            gameResponse.onResponse(GameRequestCode.CREATE_PAY_ORDER_FAILED,"failed","");
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        XLog.e("error； " + throwable.getMessage());
                        gameResponse.onResponse(GameRequestCode.CREATE_PAY_ORDER_SUCCESS,"exception: "+throwable.getMessage(),"");
                    }
                });

    }

    /**
     * 爱贝支付
     * @param activity
     * @param appGoodsId
     * @param appOrderId
     * @param appGoodsPrice
     * @param appGoodsName
     * @param appCallback
     * @param channel
     * @param payMethod
     * @param gameResponse
     */
    public void aibeiPay(final Activity activity, String appGoodsId, final String appOrderId, String appGoodsPrice, String appGoodsName, String appCallback, String channel, int payMethod, final GameResponse<String> gameResponse){
        int money = Integer.parseInt(appGoodsPrice) * 100;
        subscription = gameRequestApi.aibeiPay(activity, sdkSession, sdkAuthkey, appGoodsId, appGoodsName, String.valueOf(money), appCallback, appOrderId, channel)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<GameUCResponse<UCAiBeiBean>>() {
                    @Override
                    public void call(GameUCResponse<UCAiBeiBean> aiBeiBeanUCResponse) {
                        if (aiBeiBeanUCResponse != null && aiBeiBeanUCResponse.getCode() == 0) {
                            String params = "transid=" + aiBeiBeanUCResponse.getData().getRequestParam().getTransid() + "&appid=" + Config.AIBEI_APPID;
                            XLog.e("aibei params: " + params);
                            //爱贝支付
                            IAppPay.startPay(activity, params, new IPayResultCallback() {
                                @Override
                                public void onPayResult(int resultCode, String signvalue, String resultInfo) {
                                    if (resultCode == 0) {
                                        //支付成功
                                        XLog.e("aibei pay success");
                                        gameResponse.onResponse(GameRequestCode.PAY_SUCCESS,"success",appOrderId);
                                    } else {
                                        XLog.e("aibei pay failed");
                                        gameResponse.onResponse(GameRequestCode.PAY_FAILED,"failed: " + "errorCode=" + resultCode + "; resultInfo=" + resultInfo,"");
                                        XLog.e("errorCode: " + resultCode + ";resultInfo: " + resultInfo);
                                    }
                                }
                            });
                        } else if (aiBeiBeanUCResponse != null) {
                            XLog.e("code: " + aiBeiBeanUCResponse.getCode() + " ;msg: " + aiBeiBeanUCResponse.getMsg());
                            gameResponse.onResponse(GameRequestCode.PAY_FAILED,"failed: " + "code=" + aiBeiBeanUCResponse.getCode() + " ;msg=" + aiBeiBeanUCResponse.getMsg(),"");
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        XLog.e("error: " + throwable.getMessage());
                        gameResponse.onResponse(GameRequestCode.PAY_FAILED,"exception: " + throwable.getMessage(),"");
                    }
                });
    }

    /**
     * 发送验证码
     * @param activity
     * @param mobileNumber
     * @param gameResponse
     */
    public void sendMobileCode(Activity activity,String mobileNumber,final GameResponse gameResponse){
        subscription = gameRequestApi.sendMsgCode(activity,mobileNumber).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<GameUCResponse>() {
                    @Override
                    public void call(GameUCResponse gameUCResponse) {
                        if(gameUCResponse.getCode()==0){
                            //验证码发送成功
                            gameResponse.onResponse(GameRequestCode.SEND_MOBILE_CODE_SYCCESS,"success","");
                        }else{
                            XLog.e("code: " + gameUCResponse.getCode() + "; msg: " + gameUCResponse.getMsg());
                            gameResponse.onResponse(GameRequestCode.SEND_MOBILE_CODE_FAILED,"failed: " + "code=" + gameUCResponse.getCode() +  ";msg=" + gameUCResponse.getMsg(),"");
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        XLog.e("error； " + throwable.getMessage());
                        gameResponse.onResponse(GameRequestCode.SEND_MOBILE_CODE_FAILED,"exception: "+ throwable.getMessage(),"");
                    }
                });
    }

    /**
     * 手机号登录
     * @param activity
     * @param mobileNumber
     * @param code
     * @param gameResponse
     */
    public void mobileLogin(final Activity activity, String mobileNumber, String code, final GameResponse<Map<String,String>> gameResponse){
        subscription = gameRequestApi.mobileLogin(activity,mobileNumber,code).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<GameUCResponse<GameUCMobileLoginData>>() {
                    @Override
                    public void call(GameUCResponse<GameUCMobileLoginData> gameUCMobileLoginDataGameUCResponse) {
                        if(gameUCMobileLoginDataGameUCResponse.getCode()==0){
                            sdkSession = gameUCMobileLoginDataGameUCResponse.getData().getSession();
                            sdkAuthkey = gameUCMobileLoginDataGameUCResponse.getData().getAuthkey();
                           loginUC(activity,gameUCMobileLoginDataGameUCResponse.getData(),gameResponse,false);
                        }else{
                            XLog.e("code: " + gameUCMobileLoginDataGameUCResponse.getCode() + "; msg: " + gameUCMobileLoginDataGameUCResponse.getMsg());
                            gameResponse.onResponse(GameRequestCode.LOGIN_FAILED,"failed: " + "code=" + gameUCMobileLoginDataGameUCResponse.getCode() + ";msg=" + gameUCMobileLoginDataGameUCResponse.getMsg(),null);
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        XLog.e("error； " + throwable.getMessage());
                        gameResponse.onResponse(GameRequestCode.LOGIN_FAILED,"exception: " + throwable.getMessage(),null);
                    }
                });
    }

    /**
     * 游客登录
     * @param activity
     * @param gameResponse
     */
    public void visitorLogin(final Activity activity, final GameResponse<Map<String,String>> gameResponse){
        subscription = gameRequestApi.visitorLogin(activity).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<GameUCResponse<GameUCMobileLoginData>>() {
                    @Override
                    public void call(GameUCResponse<GameUCMobileLoginData> gameUCResponse) {
                        if(gameUCResponse.getCode()==0){
                            sdkSession = gameUCResponse.getData().getSession();
                            sdkAuthkey = gameUCResponse.getData().getAuthkey();
                            loginUC(activity,gameUCResponse.getData(),gameResponse,false);

                        }else{
                            XLog.e("code: " + gameUCResponse.getCode() + "; msg: " + gameUCResponse.getMsg());
                            gameResponse.onResponse(GameRequestCode.LOGIN_FAILED,"failed: " + "code=" + gameUCResponse.getCode() + ";msg=" + gameUCResponse.getMsg(),null);
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        XLog.e("error: " + throwable.getMessage());
                        gameResponse.onResponse(GameRequestCode.LOGIN_FAILED,"exception: " + throwable.getMessage(),null);
                    }
                });
    }

    /**
     * 微信登录
     * @param activity
     * @param code
     * @param wechatAppId
     * @param isUserInfo
     * @param gameResponse
     */
    public void wechatLogin(final Activity activity,String code,String wechatAppId,String isUserInfo,final GameResponse<Map<String,String>> gameResponse){
        subscription = gameRequestApi.wechatLogin(activity,code,wechatAppId,isUserInfo).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<GameUCResponse<GameUCWechatLoginData>>() {
                    @Override
                    public void call(GameUCResponse<GameUCWechatLoginData> gameUCWechatLoginDataGameUCResponse) {
                        if(gameUCWechatLoginDataGameUCResponse.getCode()==0){
                            sdkSession = gameUCWechatLoginDataGameUCResponse.getData().getSession();
                            sdkAuthkey = gameUCWechatLoginDataGameUCResponse.getData().getAuthkey();
                            loginUC(activity,gameUCWechatLoginDataGameUCResponse.getData(),gameResponse,true);
                        }else{
                            XLog.e("code: " + gameUCWechatLoginDataGameUCResponse.getCode() + "; msg: " + gameUCWechatLoginDataGameUCResponse.getMsg());
                            gameResponse.onResponse(GameRequestCode.LOGIN_FAILED,"failed: " + "code=" + gameUCWechatLoginDataGameUCResponse.getCode() + ";msg=" + gameUCWechatLoginDataGameUCResponse.getMsg(),null);
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        XLog.e("error: " + throwable.getMessage());
                        gameResponse.onResponse(GameRequestCode.LOGIN_FAILED,"exception: " + throwable.getMessage(),null);
                    }
                });
    }
    public void loginUC(final Activity activity, final GameUCMobileLoginData gameUCMobileLoginData, final GameResponse<Map<String,String>> gameResponse, final boolean isWechatLogin){
        subscription = gameRequestApi.login(activity,gameUCMobileLoginData.getUId(),gameUCMobileLoginData.getSession(),gameUCMobileLoginData.getAuthkey()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<LoginData>() {
                    @Override
                    public void call(LoginData loginData) {
                        if(loginData.getStatus()==1){
                            HashMap<String,String> map = new HashMap<String, String>();
                            map.put("uid",loginData.getUid());
                            map.put("token",loginData.getToken());
                            if(isWechatLogin){
                                map.put("nickname",((GameUCWechatLoginData) gameUCMobileLoginData).getName());
                                map.put("gender",String.valueOf(((GameUCWechatLoginData)gameUCMobileLoginData).getSex()));
                                map.put("iconurl",((GameUCWechatLoginData) gameUCMobileLoginData).getHdImage());
                            }
                            gameResponse.onResponse(GameRequestCode.LOGIN_SUCCESS,"success",map);
                        }else{
                            XLog.e("code: " + loginData.getStatus() );
                            gameResponse.onResponse(GameRequestCode.LOGIN_FAILED,"failed: " + "code=" + loginData.getStatus(),null);
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        XLog.e("error: " + throwable.getMessage());
                        gameResponse.onResponse(GameRequestCode.LOGIN_FAILED,"exception: " + throwable.getMessage(),null);
                    }
                });
    }

    public void clean(){
        if(subscription!=null){
            subscription.unsubscribe();
        }
    }
}
