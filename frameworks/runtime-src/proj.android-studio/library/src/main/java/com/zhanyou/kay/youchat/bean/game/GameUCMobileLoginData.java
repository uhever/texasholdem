package com.zhanyou.kay.youchat.bean.game;

/**
 * Created by kay on 16/12/16.
 */

public class GameUCMobileLoginData {
    private String authkey;
    private String session;
    private String uId;

    public String getAuthkey() {
        return authkey;
    }

    public void setAuthkey(String authkey) {
        this.authkey = authkey;
    }

    public String getSession() {
        return session;
    }

    public void setSession(String session) {
        this.session = session;
    }

    public String getUId() {
        return uId;
    }

    public void setUId(String uId) {
        this.uId = uId;
    }
}
