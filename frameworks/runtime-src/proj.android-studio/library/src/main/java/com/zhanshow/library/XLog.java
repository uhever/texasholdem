package com.zhanshow.library;

import android.util.Log;


/**
 * 日志管理类
 * @author yukai 2015-07-31
 */
public class XLog {
	public static final int ERROR_LEVEL = 5;
	public static final int WARN_LEVEL = 4;
	public static final int INFO_LEVEL = 3;
	public static final int DEBUG_LEVEL = 2;
	public static final int VERB_LEVEL = 1;
	public static void e(String tag , String log) {
		if (Config.getLogLevel() <= ERROR_LEVEL) {
			Log.e(tag, log);
		}
	}
	public static void e(String log) {
		e(getTag(),log);
	}
	public static void e(Exception e) {
		if (Config.getLogLevel() <=ERROR_LEVEL) {
			e.printStackTrace();
		}
	}
	public static void w(String tag ,String log) {
		if (Config.getLogLevel() <= WARN_LEVEL) {
			Log.w(tag, log);
		}
	}
	public static void w(String log) {
		w(getTag(),log);
	}
	public static void i(String tag ,String log) {
		if (Config.getLogLevel() <= INFO_LEVEL) {
			Log.i(tag, log);
		}
	}
	public static void i(String log) {
		i(getTag(),log);
	}
	public static void d(String tag ,String log) {
		if (Config.getLogLevel() <= DEBUG_LEVEL) {
			if(log==null){
				log=""+log;
			}
			Log.d(tag, log);
		}
	}
	public static void d(String log) {
		d(getTag(),log);
	}
	public static void v(String tag ,String log) {
		if (Config.getLogLevel() <= VERB_LEVEL) {
			Log.v(tag, log);
		}
	}
	public static void v(String log) {
		v(getTag(),log);
	}
	private static String getTag(){
		StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
		int index = 4;
		String className = stackTrace[index].getFileName();
		String methodName = stackTrace[index].getMethodName();
		int lineNumber = stackTrace[index].getLineNumber();
		return "[(" + className + ":" + lineNumber + ")#" + methodName + "]";
	}
//	private static String getCurrentClassname(){
//		StackTraceElement[] stacks = Thread.currentThread().getStackTrace();
//		String className = stacks[5].getClassName();
//		return className;
//	}
//	private static String getCurrentMethod(){
//		StackTraceElement[] stacks = Thread.currentThread().getStackTrace();
//		String methodName = stacks[5].getMethodName();
//		return methodName;
//	}
//	private static int getCurrentLineNumber(){
//		StackTraceElement[] stacks = Thread.currentThread().getStackTrace();
//		int lineNumber= stacks[5].getLineNumber();
//		return lineNumber;
//	}
}
