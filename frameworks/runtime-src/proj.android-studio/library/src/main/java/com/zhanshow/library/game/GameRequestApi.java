package com.zhanshow.library.game;

import android.app.Activity;
import android.content.Context;

import com.google.gson.GsonBuilder;
import com.zhanshow.library.Config;
import com.zhanshow.library.RequestHelper;
import com.zhanshow.library.XLog;
import com.zhanshow.library.utils.PackageUtils;
import com.zhanyou.kay.youchat.bean.game.GameOrderBean;
import com.zhanyou.kay.youchat.bean.game.GameUCMobileLoginData;
import com.zhanyou.kay.youchat.bean.game.GameUCWechatLoginData;
import com.zhanyou.kay.youchat.bean.game.LoginData;
import com.zhanyou.kay.youchat.bean.game.LoginNetEaseData;
import com.zhanyou.kay.youchat.bean.game.UCAiBeiBean;
import com.zhanyou.kay.youchat.bean.game.GameUCResponse;

import java.util.HashMap;
import java.util.Map;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.Observable;
import rx.schedulers.Schedulers;

/**
 * Created by kay on 16/12/13.
 */

public class GameRequestApi {
    public static final int SUCCESS = 1 ;
    /**
     * 多端登录错误码
     */
    public static final int TOKEN_EXPIRED = 12;//被踢或者token过期
    public static final int GET_USERINFO_ERROR = 13;//获取用户信息失败
    public static final int CLOSURE_ACCOUNT = 14;//封号

    public static final int SESSION_TIMEOUT = 20004;
    public static final int SESSION_ERROR = 20005;

    public static final String KEY_STATUS = "status";
    public static final String KEY_CODE = "code";
    static final String BASE_URL = "http://42.62.78.20/uc.live.com/";
    private GameRequestService mRequestService;
    private RequestHelper mRequestHelper;
    public GameRequestApi(RequestHelper requestHelper,OkHttpClient mOkHttpClient){
        this.mRequestHelper = requestHelper;
        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create( new GsonBuilder().create()))
                .client(mOkHttpClient)
                .baseUrl(BASE_URL)
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build();
        mRequestService = retrofit.create(GameRequestService.class);
    }

    /**
     * 组织扑克创建订单的参数
     * @param token
     * @param id
     * @param type
     * @param appid
     * @param server_poker
     * @return
     */
    public Observable<GameOrderBean> createPokerOrder(String token,String id,String type,String appid,String server_poker){
        Map<String,String> params = new HashMap<String,String>();
        params.put("token",token);
        params.put("id",id);
        params.put("type",type);
        params.put("appid",appid);
        params.put("server_poker",server_poker);
        return mRequestService.createPokerOrder(params).subscribeOn(Schedulers.io());
    }

    /**
     * 组织爱贝支付的参数
     * @param context
     * @param session
     * @param authkey
     * @param appGoodsId
     * @param appGoodsName
     * @param appGoodsPrice
     * @param appCallback
     * @param appOrderId
     * @param channel
     * @return
     */
    public Observable<GameUCResponse<UCAiBeiBean>> aibeiPay(Context context, String session, String authkey, String appGoodsId, String appGoodsName, String appGoodsPrice, String appCallback, String appOrderId, String channel){
        String sys = mRequestHelper.appendSys();
        Map<String,String> map = new HashMap<String,String>();
        map.put("session", session);
        map.put("appGoodsId", appGoodsId);
        map.put("appGoodsName", appGoodsName);
        map.put("appGoodsPrice", appGoodsPrice);
        map.put("appOrderId", appOrderId);
        map.put("appCallback", appCallback);
        map.put("iappPayAppId", Config.AIBEI_APPID);//爱贝支付AppId
        map.put("sys", sys);
        String result = mRequestHelper.getResult(map, authkey);
        XLog.d("爱贝支付参数:" + result);
        return mRequestService.aibeiPay(mRequestHelper.encode(result)).subscribeOn(Schedulers.io());
    }

    /**
     * 组织手机号验证码登录的参数
     * @param context
     * @param mobileNumber
     * @param seccode
     * @return
     */
    public Observable<GameUCResponse<GameUCMobileLoginData>> mobileLogin(Context context,String mobileNumber, String seccode){
        String sys = mRequestHelper.appendSys();
        Map<String,String> map = new HashMap<String,String>();
        map.put("mobileNumber", mobileNumber);
        map.put("seccode", seccode);
        map.put("devInfo", mRequestHelper.getDeviceInfo(context));
        map.put("sys", sys);
        String result = mRequestHelper.getResult(map, "");
        XLog.d("手机号登录参数:" + result);
        return  mRequestService.mobileLogin(mRequestHelper.encode(result)).subscribeOn(Schedulers.io());
    }

    /**
     * 组织发送验证码的参数
     * @return
     */
    public Observable<GameUCResponse> sendMsgCode(Activity context, String mobileNumber){
        String sys = mRequestHelper.appendSys();
        Map<String,String> map = new HashMap<String,String>();
        map.put("mobileNumber", mobileNumber);
        map.put("sys", sys);
        String result = mRequestHelper.getResult(map, "");
        XLog.d("发送短信验证码参数:" + result);
        return mRequestService.sendMsgCode(mRequestHelper.encode(result)).subscribeOn(Schedulers.io());
    }

    /**
     * 组织游客登录的参数
     * @param context
     * @return
     */
    public Observable<GameUCResponse<GameUCMobileLoginData>> visitorLogin(Context context){
        String sys = mRequestHelper.appendSys();
        Map<String,String> map = new HashMap<String,String>();
        map.put("devInfo", mRequestHelper.getDeviceInfo(context));
        map.put("uuid", mRequestHelper.getUniqueId(context));
        map.put("sys", sys);
        String result = mRequestHelper.getResult(map, "");
        XLog.d("试玩登录参数:" + result);
        return mRequestService.visitorLogin(mRequestHelper.encode(result)).subscribeOn(Schedulers.io());
    }
    /**
     * 微信登录
     * @param code
     * @param isUserInfo
     * @return
     */
    public Observable<GameUCResponse<GameUCWechatLoginData>> wechatLogin(Context context, String code, String wcAppId, String isUserInfo){
        String sys = mRequestHelper.appendSys();
        Map<String,String> map = new HashMap<String,String>();
        map.put("wcAppId", wcAppId);
        map.put("code", code);
        map.put("devInfo", mRequestHelper.getDeviceInfo(context));
        map.put("isUserInfo", isUserInfo);
        map.put("sys", sys);
        String result = mRequestHelper.getResult(map, "");
        XLog.d("微信登录参数:" + result);
        return mRequestService.wechatLogin(mRequestHelper.encode(result)).subscribeOn(Schedulers.io());
    }

    /**
     * 组织登录UC服务器的参数
     * @param uId
     * @param token
     * @param authkey
     * @return
     */
    public Observable<LoginData> login(Context context, String uId, String token, String authkey){
        HashMap<String,String> params = new HashMap<String,String>();
        params.put("uId",uId);
        params.put("session",token);
        params.put("authkey",authkey);
        params.put("channel", PackageUtils.getApplicationMetadata(context,Config.UMENG_CHANNEL));
        params.put("mac",mRequestHelper.getMacID());
        params.put("android_id",mRequestHelper.getAndroidId());
        params.put("appid", Config.getAppId(context));
        params.put("idfa", "");
//        params.put("isbind","");
        return mRequestService.login(params).subscribeOn(Schedulers.io());
    }

    /**
     * 组织登录网易服务器的参数
     * @param uctoken
     * @return
     */
    public Observable<LoginNetEaseData> loginNetEase(Context context, String uctoken, String nickname, String sex, String icon){
        HashMap<String,String> params = new HashMap<String,String>();
        params.put("uctoken",uctoken);
        params.put("mac",mRequestHelper.getMacID());
        params.put("imei",mRequestHelper.getImei());
        params.put("nickname",nickname);
        params.put("sex",sex);
        params.put("icon",icon);
        return mRequestService.loginNetEase(params).subscribeOn(Schedulers.io());
    }

}
