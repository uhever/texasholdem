package com.zhanshow.library.sp;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.preference.PreferenceManager;
import android.text.TextUtils;

public class XSharedPreferences implements SharedPreferences{
	private String keyWord;
	private String sharedPrefFilename;
	private SharedPreferences sharedPreferences;
	private static final String encryptCharset = "ISO-8859-1";
	private static final String charset = "UTF-8";
	public XSharedPreferences(Context context,String fileName){
		this(context,"",fileName);
	}
	public XSharedPreferences(Context context,String password,String fileName){
		if(sharedPreferences==null){
			sharedPreferences = getSharedPreferenceFile(context, fileName);
		}

	}
    private SharedPreferences getSharedPreferenceFile(Context context, String prefFilename) {
	     this.sharedPrefFilename = prefFilename;
	     if(TextUtils.isEmpty(prefFilename)) {
	         return PreferenceManager.getDefaultSharedPreferences(context);
	     }
	     else{
	        return context.getSharedPreferences(prefFilename, Context.MODE_PRIVATE);
	     }
	}

    private String encrypt(String text){
		return text;
    }
    private String encryptKey(String text) {
		return text;
	}
    private String decrypt(String text){
		return text;
	}
	@Override
	public Map<String, String> getAll(){
		final Map<String, ?> encryptedMap = sharedPreferences.getAll();
		if(encryptedMap==null){
			return null;
		}
		final Map<String,String> decryptedMap = new HashMap<String,String>(encryptedMap.size());
		for (Entry<String,?> entry : encryptedMap.entrySet()) {
			Object cipherText = entry.getValue();
			if(cipherText!=null){
				decryptedMap.put(decrypt(entry.getKey()), decrypt(cipherText.toString()));
			}
		}
		return decryptedMap;
	}

	@Override
	public String getString(String key, String defValue) {
		String encryptedText = sharedPreferences.getString(encryptKey(key), null);
		return encryptedText==null ? defValue : decrypt(encryptedText);
	}
	public String getUnEncryptedString(String key,String defValue){
		String encryptedText = sharedPreferences.getString(encryptKey(key), null);
		return encryptedText==null ? defValue : encryptedText;
	}
	@Override
	public Set<String> getStringSet(String key, Set<String> defValues) {
		Set<String> encryptedSet = sharedPreferences.getStringSet(encryptKey(key), null);
		if(encryptedSet==null){
			return defValues;
		}
		final Set<String> decryptedSet = new HashSet<String>(encryptedSet.size());
		for (String encryptedValue : encryptedSet) {
			decryptedSet.add(decrypt(encryptedValue));
		}
		return decryptedSet;
	}

	@Override
	public int getInt(String key, int defValue) {
		String encryptedText = sharedPreferences.getString(encryptKey(key), null);
		if(encryptedText==null){
			return defValue;
		}
		return Integer.parseInt(decrypt(encryptedText));
	}

	@Override
	public long getLong(String key, long defValue) {
		String encryptedText = sharedPreferences.getString(encryptKey(key), null);
		if(encryptedText==null){
			return defValue;
		}
		return Long.parseLong(decrypt(encryptedText));
	}

	@Override
	public float getFloat(String key, float defValue) {
		String encryptedText = sharedPreferences.getString(encryptKey(key), null);
		if(encryptedText==null){
			return defValue;
		}
		return Float.parseFloat(decrypt(encryptedText));
	}

	@Override
	public boolean getBoolean(String key, boolean defValue) {
		String encryptedText = sharedPreferences.getString(encryptKey(key), null);
		if(encryptedText==null){
			return defValue;
		}
		return Boolean.parseBoolean(decrypt(encryptedText));
	}

	@Override
	public boolean contains(String key) {
		return sharedPreferences.contains(encryptKey(key));
	}
	public class XEditor implements Editor {
		private Editor mEditor;
		/**
		 * Constructor.
		 */
		private XEditor() {
			mEditor = sharedPreferences.edit();
		}
		@Override
		public Editor putString(String key, String value) {
			mEditor.putString(encryptKey(key), encrypt(value));
			return this;
		}
		public Editor putUnEncryptedString(String key,String value){
			mEditor.putString(encryptKey(key), value);
			return this;
		}
		@Override
		public Editor putStringSet(String key, Set<String> values) {
			final Set<String> encryptedValues = new HashSet<String>(values.size());
			for (String value : values) {
				encryptedValues.add(encrypt(value));
			}
			mEditor.putStringSet(encryptKey(key),encryptedValues);
			return this;
		}

		@Override
		public Editor putInt(String key, int value) {
			mEditor.putString(encryptKey(key), encrypt(Integer.toString(value)));
			return this;
		}

		@Override
		public Editor putLong(String key, long value) {
			mEditor.putString(encryptKey(key), encrypt(Long.toString(value)));
			return this;
		}

		@Override
		public Editor putFloat(String key, float value) {
			mEditor.putString(encryptKey(key), encrypt(Float.toString(value)));
			return this;
		}

		@Override
		public Editor putBoolean(String key, boolean value) {
			mEditor.putString(encryptKey(key), encrypt(Boolean.toString(value)));
			return this;
		}

		@Override
		public Editor remove(String key) {
			mEditor.remove(encryptKey(key));
			return this;
		}

		@Override
		public Editor clear() {
			mEditor.clear();
			return this;
		}

		@Override
		public boolean commit() {
			return mEditor.commit();
		}

		@Override
		public void apply() {
			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD) {
				mEditor.apply();
			} else {
				commit();
			}
		}
		
	}
	@Override
	public XEditor edit() {
		return new XEditor();
	}

	@Override
	public void registerOnSharedPreferenceChangeListener(OnSharedPreferenceChangeListener listener) {
		sharedPreferences.registerOnSharedPreferenceChangeListener(listener);
	}

	@Override
	public void unregisterOnSharedPreferenceChangeListener(OnSharedPreferenceChangeListener listener) {
		sharedPreferences.unregisterOnSharedPreferenceChangeListener(listener);
	}

}
