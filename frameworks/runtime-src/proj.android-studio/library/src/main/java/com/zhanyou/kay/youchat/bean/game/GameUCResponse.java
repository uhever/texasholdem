package com.zhanyou.kay.youchat.bean.game;

/**
 * Created by kay on 16/8/1.
 */
public class GameUCResponse<T> {

    /**
     * code : 0
     * msg :
     * data : {""}
     */

    private int code;
    private String msg;
    private T data;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

}
