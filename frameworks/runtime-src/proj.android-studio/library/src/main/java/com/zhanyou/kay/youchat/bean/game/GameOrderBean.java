package com.zhanyou.kay.youchat.bean.game;

/**
 * Created by kay on 16/12/13.
 */

public class GameOrderBean {
    private int status;
    /**
     * amount : 6
     * product_name : 60 Ingots
     * product_id : 1
     * app_order_id : 2661160806143643362306
     */

    private String amount;
    private String product_name;
    private int product_id;
    private String app_order_id;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }


    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public int getProduct_id() {
        return product_id;
    }

    public void setProduct_id(int product_id) {
        this.product_id = product_id;
    }

    public String getApp_order_id() {
        return app_order_id;
    }

    public void setApp_order_id(String app_order_id) {
        this.app_order_id = app_order_id;
    }
}
