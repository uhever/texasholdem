package com.zhanshow.library.game;

/**
 * Created by kay on 16/12/13.
 */

public abstract class GameResponse<T> {
    public abstract void onResponse(int code ,String msg ,T extraData);
}
