package com.zhanyou.kay.youchat.bean.game;

/**
 * Created by kay on 16/7/19.
 */
public class ServerInfo {

    /**
     * id : 1
     * name : t1 荆襄九郡
     * addr : 42.62.78.20/t1.game.kapai.com
     * servergroup : 1
     * newserver : 0
     * status : 2
     * message :
     * type : 1
     * nickname : Marcy
     * gender : 1
     * selected : 0
     */

    private String id;
    private String name;
    private String addr;
    private String servergroup;
    private String newserver;
    private int status;
    private String message;
    private int type;
    private String nickname;
    private String gender;
    private int selected;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddr() {
        return addr;
    }

    public void setAddr(String addr) {
        this.addr = addr;
    }

    public String getServergroup() {
        return servergroup;
    }

    public void setServergroup(String servergroup) {
        this.servergroup = servergroup;
    }

    public String getNewserver() {
        return newserver;
    }

    public void setNewserver(String newserver) {
        this.newserver = newserver;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public int getSelected() {
        return selected;
    }

    public void setSelected(int selected) {
        this.selected = selected;
    }
}
