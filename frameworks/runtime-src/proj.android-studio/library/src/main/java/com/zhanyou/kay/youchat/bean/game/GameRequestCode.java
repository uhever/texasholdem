package com.zhanyou.kay.youchat.bean.game;

/**
 * Created by kay on 16/12/13.
 */

public class GameRequestCode {
    public static final int CREATE_PAY_ORDER_SUCCESS = 0;
    public static final int CREATE_PAY_ORDER_FAILED = 1;


    public static final int PAY_SUCCESS = 0;
    public static final int PAY_FAILED = 1;


    public static final int SEND_MOBILE_CODE_SYCCESS = 0;
    public static final int SEND_MOBILE_CODE_FAILED = 1;


    public static final int LOGIN_SUCCESS = 0;
    public static final int LOGIN_FAILED = 1;


}
