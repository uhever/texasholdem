package com.zhanshow.library;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.TextUtils;

import com.zhanshow.library.Config;
import com.zhanshow.library.Sha1;
import com.zhanshow.library.XLog;
import com.zhanshow.library.sp.SharedPreferencesUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by kay on 16/7/13.
 */
public class RequestHelper {
    private final char[] legalChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"
            .toCharArray();
    private long time;
    private Context mContext;
    private String sys;
    public RequestHelper(Context context){
        this.mContext = context;
    }

    public String getAndroidId() {
        return mContext == null?"":Settings.Secure.getString(mContext.getContentResolver(), Settings.Secure.ANDROID_ID);
    }

    public String getDeviceId() {
        String deviceId;
        TelephonyManager tm = (TelephonyManager) mContext.getSystemService(Context.TELEPHONY_SERVICE);
        if (tm.getDeviceId() == null) {
            deviceId = getAndroidId();
        } else {
            deviceId = tm.getDeviceId();
        }
        return deviceId;
    }
    public String getDeviceInfo(Context context){
        String uuid = "";
        String idfa = "";
        String androidid = "";

             androidid = Settings.Secure.getString(context.getContentResolver(),
                    Settings.Secure.ANDROID_ID);

        if(TextUtils.isEmpty(androidid)){
            androidid = "";
        }
        TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        //需要权限：READ_PHONE_STATE
        String ieme = tm.getDeviceId();
        if(TextUtils.isEmpty(ieme)){
            ieme = "";
        }
        String mac = getMacID();
        String brand = Build.MANUFACTURER;
        String model = Build.MODEL;
        String ver = Build.VERSION.RELEASE;
        JSONObject jsonObj = new JSONObject();
        try {
            jsonObj.put("uuid", uuid);
            jsonObj.put("idfa", idfa);
            jsonObj.put("androidid", androidid);
            jsonObj.put("ieme", ieme);
            jsonObj.put("mac", mac);
            jsonObj.put("brand", brand);
            jsonObj.put("model", model);
            jsonObj.put("ver", ver);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObj.toString();
    }
    public String getImei(){
        String deviceId = "000000000000000";
        TelephonyManager tm = (TelephonyManager) mContext.getSystemService(Context.TELEPHONY_SERVICE);
        String imei = tm.getDeviceId();
        if(!TextUtils.isEmpty(imei)){
            deviceId =imei;
        }
        return deviceId;
    }
    public String getMacID() {
        String Mac = "00:00:00:00:00:00";
        WifiManager wifi = (WifiManager) mContext.getSystemService(Context.WIFI_SERVICE);
        WifiInfo info = wifi.getConnectionInfo();
        Mac = info.getMacAddress();

        if (Mac == null || Mac.length() == 0) {
            XLog.i("GetMobileInfo", "获取android mac地址失败");
            try {
                String path = "sys/class/net/wlan0/address";
                if ((new File(path)).exists()) {
                    FileInputStream fis = new FileInputStream(path);
                    byte[] buffer = new byte[8192];
                    int byteCount = fis.read(buffer);
                    if (byteCount > 0) {
                        Mac = new String(buffer, 0, byteCount, "utf-8");
                    }
                    fis.close();
                }
                if (Mac == null || Mac.length() == 0) {
                    path = "sys/class/net/eth0/address";
                    FileInputStream fis_name = new FileInputStream(path);
                    byte[] buffer_name = new byte[8192];
                    int byteCount_name = fis_name.read(buffer_name);
                    if (byteCount_name > 0) {
                        Mac = new String(buffer_name, 0, byteCount_name,
                                "utf-8");
                    }
                    fis_name.close();
                }
                XLog.d("mac***eth0**mac11**为" + Mac);
                if (Mac.length() == 0 || Mac == null) {
                    return "00:00:00:00:00:00";
                }
            } catch (Exception e) {
                XLog.e("mac exception :" + e.toString());
                return "00:00:00:00:00:00";
            }
        }
        return Mac;
    }
    public int getNetType(Context context){
        int type = 0;
        ConnectivityManager connectManager = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo info = connectManager.getActiveNetworkInfo();
        if(info!=null && info.isConnected()){
            if(info.getType() == ConnectivityManager.TYPE_WIFI){
                type = 4;
            }
            else if(info.getType() == ConnectivityManager.TYPE_MOBILE){
                switch (info.getSubtype()) {
                    case TelephonyManager.NETWORK_TYPE_GPRS:
                    case TelephonyManager.NETWORK_TYPE_EDGE:
                    case TelephonyManager.NETWORK_TYPE_CDMA:
                    case TelephonyManager.NETWORK_TYPE_1xRTT:
                    case TelephonyManager.NETWORK_TYPE_IDEN:
                        type = 1;
                        break;

                    case TelephonyManager.NETWORK_TYPE_UMTS:
                    case TelephonyManager.NETWORK_TYPE_EVDO_0:
                    case TelephonyManager.NETWORK_TYPE_EVDO_A:
                    case TelephonyManager.NETWORK_TYPE_HSDPA:
                    case TelephonyManager.NETWORK_TYPE_HSUPA:
                    case TelephonyManager.NETWORK_TYPE_HSPA:
                    case TelephonyManager.NETWORK_TYPE_EVDO_B:
                    case TelephonyManager.NETWORK_TYPE_EHRPD:
                    case TelephonyManager.NETWORK_TYPE_HSPAP:
                        type = 2;
                        break;

                    case TelephonyManager.NETWORK_TYPE_LTE:
                        type = 3;
                        break;
                }
            }
        }
        return type;
    }
    /**
     * 拼接公共参数
     */
    public String appendSys() {
        time = System.currentTimeMillis()/1000;// 单位为秒
        StringBuilder sb = new StringBuilder();
        sb.append(time);
        sb.append("|");//设备uid
        sb.append(getUniqueId(mContext));
        sb.append("|");
        sb.append(Config.serverVersion);
        sb.append("|");
        sb.append(getNetType(mContext));
        sb.append("|");
        sb.append(Config.plantform);
        sb.append("|");//appid
        sb.append(Config.getAppId(mContext));//appid
        sb.append("|");
        sb.append(Config.getSDKVersion(mContext));
        sys = sb.toString();
        return sys;
    }
    public  String getResult(Map<String, String> map, String authkey) {
        Object[] keys =  map.keySet().toArray();
        Arrays.sort(keys);
        String str = "";
        StringBuffer stringBuffer = new StringBuffer();
        for (Object key : keys) {
            stringBuffer.append(map.get(key));
        }
        str = stringBuffer.toString();
        String result = "sig=" + appendSig(str, authkey);
        Iterator iterator = map.entrySet().iterator();
        StringBuffer sBuffer = new StringBuffer();
        while(iterator.hasNext()){
            Map.Entry entry =  (Map.Entry) iterator.next();
            Object key = entry.getKey();
            Object value = entry.getValue();
            sBuffer.append("&").append(key).append("=").append(value);
        }
        return result+sBuffer.toString();
    }
    private String appendSig(String str, String authkey) {
        String firstTempStr = str + time + Sha1.encode(Config.getAppKey(mContext))+ authkey;
        String secondTempStr = Sha1.encode(firstTempStr) + time;
        String endStr = Sha1.encode(secondTempStr);
        return endStr;
    }
    /**
     * Base64编码
     * @param text 需要Base64编码的字符串
     * @return 经过Base编码后的32位字符串
     */
    public  String encode(String text) {
        byte[] data = text.getBytes();
        int start = 0;
        int len = data.length;
        StringBuffer buf = new StringBuffer(data.length * 3 / 2);

        int end = len - 3;
        int i = start;
        int n = 0;

        while (i <= end) {
            int d = ((((int) data[i]) & 0x0ff) << 16)
                    | ((((int) data[i + 1]) & 0x0ff) << 8)
                    | (((int) data[i + 2]) & 0x0ff);

            buf.append(legalChars[(d >> 18) & 63]);
            buf.append(legalChars[(d >> 12) & 63]);
            buf.append(legalChars[(d >> 6) & 63]);
            buf.append(legalChars[d & 63]);

            i += 3;

            if (n++ >= 14) {
                n = 0;
                buf.append(" ");
            }
        }

        if (i == start + len - 2) {
            int d = ((((int) data[i]) & 0x0ff) << 16)
                    | ((((int) data[i + 1]) & 255) << 8);

            buf.append(legalChars[(d >> 18) & 63]);
            buf.append(legalChars[(d >> 12) & 63]);
            buf.append(legalChars[(d >> 6) & 63]);
            buf.append("=");
        } else if (i == start + len - 1) {
            int d = (((int) data[i]) & 0x0ff) << 16;

            buf.append(legalChars[(d >> 18) & 63]);
            buf.append(legalChars[(d >> 12) & 63]);
            buf.append("==");
        }

        return buf.toString();
    }
    public  String encode(byte[] data) {
        int start = 0;
        int len = data.length;
        StringBuffer buf = new StringBuffer(data.length * 3 / 2);

        int end = len - 3;
        int i = start;
        int n = 0;

        while (i <= end) {
            int d = ((((int) data[i]) & 0x0ff) << 16)
                    | ((((int) data[i + 1]) & 0x0ff) << 8)
                    | (((int) data[i + 2]) & 0x0ff);

            buf.append(legalChars[(d >> 18) & 63]);
            buf.append(legalChars[(d >> 12) & 63]);
            buf.append(legalChars[(d >> 6) & 63]);
            buf.append(legalChars[d & 63]);

            i += 3;

            if (n++ >= 14) {
                n = 0;
                buf.append(" ");
            }
        }

        if (i == start + len - 2) {
            int d = ((((int) data[i]) & 0x0ff) << 16)
                    | ((((int) data[i + 1]) & 255) << 8);

            buf.append(legalChars[(d >> 18) & 63]);
            buf.append(legalChars[(d >> 12) & 63]);
            buf.append(legalChars[(d >> 6) & 63]);
            buf.append("=");
        } else if (i == start + len - 1) {
            int d = (((int) data[i]) & 0x0ff) << 16;

            buf.append(legalChars[(d >> 18) & 63]);
            buf.append(legalChars[(d >> 12) & 63]);
            buf.append("==");
        }

        return buf.toString();
    }
    public String getUniqueId(final Context context){
        String uid = SharedPreferencesUtils.getStringData(context, "UNIQUEID");
        if (TextUtils.isEmpty(uid) && context!= null) {
            uid= Settings.Secure.getString(context.getContentResolver(),
                    Settings.Secure.ANDROID_ID);
            if (TextUtils.isEmpty(uid)){
                final TelephonyManager tm = (TelephonyManager) context
                        .getSystemService(Context.TELEPHONY_SERVICE);
                //需要权限：READ_PHONE_STATE
                uid = tm.getDeviceId();
            }
            if(TextUtils.isEmpty(uid)){
                uid = "";
            }
            SharedPreferencesUtils.saveStringData(context, "UNIQUEID", uid);
        }
        return uid;
    }
}
