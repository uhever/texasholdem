package com.zhanshow.library.okhttp;


import android.content.Context;

import com.zhanshow.library.Config;
import com.zhanshow.library.utils.PackageUtils;
import com.zhanshow.library.XLog;
import com.zhanshow.library.sp.SharedPreferencesUtils;

import java.io.IOException;

import okhttp3.FormBody;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okio.Buffer;

/**
 * Created by kay on 16/9/23.
 */

public class GameHttpBaseParamsLoggingInterceptor implements Interceptor {
    private String uid;
    private int version;
    private Context context;
    public GameHttpBaseParamsLoggingInterceptor(Context context, int version){
        this.context = context;
        this.version = version;
    }
    @Override
    public Response intercept(Chain chain) throws IOException {
        Request request = chain.request();
        Request.Builder requestBuilder = request.newBuilder();
        RequestBody formBody = null;
        String  channelId = PackageUtils.getApplicationMetadata(context,Config.UMENG_CHANNEL);
        formBody = new FormBody.Builder()
                    .add("app_channel_id", channelId)
                    .add("app_token_uid", SharedPreferencesUtils.getStringData(context,"uid",""))
                    .add("app_version", String.valueOf(version))
                    .add("app_product_id", Config.getProduceId(context))
                    .add("app_skin", "0")
                    .build();

        String url = request.url().toString();
//        int isOnline = SharedPreferencesUtils.getIntData(context,"isOnline");
//
//        if(BuildConfig.DEBUG){
//            XLog.d("isOnline: " + isOnline);
//            if(isOnline==1){
//                //测试环境
//                if(url.contains(Config.ZHIBO_REQUEST_PREFIX)){
//                    url = url.replace(Config.ZHIBO_REQUEST_PREFIX,Config.OFFLINE_ZHIBO_REQUEST_PREFIX);
//                    XLog.d("url replace: " +url);
//                }else if(url.contains(Config.UC_REQUEST_PREFIX)){
//                    url = url.replace(Config.UC_REQUEST_PREFIX,Config.OFFLINE_UC_REQUEST_PREFIX);
//                }else if(url.contains(Config.FIRST_ZHIBO_REQUEST_PREFIX)){
//                    url = url.replace(Config.FIRST_ZHIBO_REQUEST_PREFIX,Config.OFFLINE_FIRST_ZHIBO_REQUEST_PREFIX);
//                }else if(url.contains(Config.PAY_ZHIBO_REQUEST_PREFIX)){
//                    url = url.replace(Config.PAY_ZHIBO_REQUEST_PREFIX,Config.OFFLINE_PAY_ZHIBO_REQUEST_PREFIX);
//                }else if(url.contains(Config.UPLOAD_IMAGE_PREFIX)){
//                    url = url.replace(Config.UPLOAD_IMAGE_PREFIX,Config.OFFLINE_UPLOAD_IMAGE_PREFIX);
//                }
//            }
//        }

//        if(ReviewHelper.isReviewServer(context)&&url.contains(Config.ZHIBO_REQUEST_PREFIX)&&!url.equals((Config.ZHIBO_REQUEST_PREFIX)+"?c=liveinit&m=check_version")){
//            String app_url = SharedPreferencesUtils.getStringData(context,"app_url");
//            if(!TextUtils.isEmpty(app_url)){
//                url = url.replace(Config.ZHIBO_REQUEST_PREFIX,app_url);
//            }
//        }

        XLog.d("url prefix: " + url);
        if(url.contains(Config.ZHIBO_REQUEST_PREFIX)||url.contains(Config.FIRST_ZHIBO_REQUEST_PREFIX)){
            //判断是钥匙秋亭这边的接口都在url地址后面加上三个参数
            url += ((url.contains("?")) ? "&" : "?") + bodyToString(formBody);//如果url带有?,就以&符开始拼接三个参数,如果没有问号那么第一个以?开头,后面用&连接参数
        }
        XLog.d("url postfix: " + url);
        request = requestBuilder.url(url).build();
        return chain.proceed(request);
    }


    private String bodyToString(final RequestBody body){

        try {
            final Buffer buffer = new Buffer();
            body.writeTo(buffer);
            return buffer.readUtf8();
        } catch (final IOException e) {
            return "did not work";
        }
    }
}
