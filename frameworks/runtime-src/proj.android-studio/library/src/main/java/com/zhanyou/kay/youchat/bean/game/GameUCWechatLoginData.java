package com.zhanyou.kay.youchat.bean.game;

/**
 * Created by kay on 17/1/17.
 */

public class GameUCWechatLoginData extends GameUCMobileLoginData{
    private String name;
    private String hdImage;
    private int sex;
    private String site;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHdImage() {
        return hdImage;
    }

    public void setHdImage(String hdImage) {
        this.hdImage = hdImage;
    }

    public int getSex() {
        return sex;
    }

    public void setSex(int sex) {
        this.sex = sex;
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }
}
