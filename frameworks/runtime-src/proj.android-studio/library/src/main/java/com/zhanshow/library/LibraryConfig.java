package com.zhanshow.library;

import android.app.Application;
import android.content.Context;
import com.zhanshow.library.utils.DateUtil;
import java.util.HashSet;
import java.util.Set;

/**
 * @author weilinhu
 */
public class LibraryConfig {
    private static final String TAG = "LibraryConfig";
    private Application application;

    private LibraryConfig() {
    }



    private static final class Singleton {
        private static final LibraryConfig INSTANCE = new LibraryConfig();
    }


    public static LibraryConfig getInstance() {
        return Singleton.INSTANCE;
    }


    public void initApplication(Application application) {
        this.application =application;
    }

    public Context getContext() {
        return application;
    }

    public boolean getMaskWordAvalibe(){
        try {
            String string = application.getSharedPreferences("live_config", Context.MODE_PRIVATE).getString("mask_word", "2");
            //   "mask_wrod" : 1是开启屏蔽字,0关闭
            if ("1".equals(string)){
                return true;
            }else {
                return false;
            }
        }catch (Exception e){
            e.printStackTrace();
            return true;
        }


    }

    public boolean getIsManager(){
        try {

            return  "true".equals(application.getSharedPreferences("live_config", Context.MODE_PRIVATE).getString("is_manager","false"));

        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }



    private int getLevelPrivateMsg(){
        try {
            return  Integer.parseInt(application.getSharedPreferences("live_config", Context.MODE_PRIVATE).getString("private_msg_level","0"));

        }catch (Exception e){
            e.printStackTrace();
            return 0;
        }
    }

    public int getLevelCommentCircle(){
        try {
            return  Integer.parseInt(application.getSharedPreferences("live_config", Context.MODE_PRIVATE).getString("circle_comment_level","0"));

        }catch (Exception e){
            e.printStackTrace();
            return 0;
        }
    }

    public boolean isCanCommentCircle(){
        return 0 == getLevelCommentCircle() || (getCurrentUserLevel() >= getLevelCommentCircle()) || getIsManager();
    }

    private int getCurrentUserLevel(){
        try {
            return  Integer.parseInt(application.getSharedPreferences("live_config", Context.MODE_PRIVATE).getString("user_level","0"));

        }catch (Exception e){
            e.printStackTrace();
            return 0;
        }
    }

    public boolean isCanSendPrivateMsgLevelLimit(){
        return 0 == getLevelPrivateMsg() || !(getCurrentUserLevel() < getLevelPrivateMsg()) ||getIsManager();
    }


    private int getLevelPrivateNum(){
        try {
            return  Integer.parseInt(application.getSharedPreferences("live_config", Context.MODE_PRIVATE).getString("private_msg_num","0"));

        }catch (Exception e){
            e.printStackTrace();
            return 0;
        }
    }


    public int getMinTime() {
        return Integer.parseInt(application.getSharedPreferences("live_config", Context.MODE_PRIVATE).getString("chat_send_sec","0")) * 1000;
    }

    public String getCircle_verify() {
        return application.getSharedPreferences("live_config", Context.MODE_PRIVATE).getString("circle_verify","1");
    }

}
