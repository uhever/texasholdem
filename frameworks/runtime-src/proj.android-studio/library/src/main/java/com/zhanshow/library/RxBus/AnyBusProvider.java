package com.zhanshow.library.RxBus;

import com.hwangjr.rxbus.Bus;
import com.hwangjr.rxbus.thread.ThreadEnforcer;

/**
 * Maintains a singleton instance for obtaining the bus. Ideally this would be replaced with a more efficient means
 * such as through injection directly into interested classes.
 */
public final class AnyBusProvider {
    /**
     * Check out the bus, like identifier or thread enforcer etc.
     */
    private static Bus BUS;

    private AnyBusProvider() {
    }

    public static synchronized Bus getInstance() {
        if (BUS == null) {
            BUS = new Bus(ThreadEnforcer.ANY);
        }
        return BUS;
    }

}
