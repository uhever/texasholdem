package com.zhanshow.library.sp;


import android.content.Context;

import com.zhanshow.library.Config;

public class SharedPreferencesUtils {
	public static void saveStringData(Context context,String key,String value){
		saveStringData(context, Config.fileName, key, value);
	}
	public static void saveStringData(Context context,String filename,String key,String value){
		XSharedPreferences sp = getSharedPreferences(context, filename);
		XSharedPreferences.XEditor editor = sp.edit();
		editor.putString(key, value);
		editor.commit();
	}
	public static String getStringData(Context context,String key){
		return getStringData(context, Config.fileName, key,"");
	}
	public static String getStringData(Context context,String key,String defaultValue){
		XSharedPreferences sp = getSharedPreferences(context, Config.fileName);
		return sp.getString(key, defaultValue);
	}
	public static String getStringData(Context context,String filename,String key,String defaultValue){
		XSharedPreferences sp = getSharedPreferences(context, filename);
		return sp.getString(key, defaultValue);
	}
	
	
	public static void saveIntData(Context context,String key,int value){
		saveIntData(context, Config.fileName, key, value);
	}
	public static void saveIntData(Context context,String filename,String key,int value){
		XSharedPreferences sp = getSharedPreferences(context, filename);
		XSharedPreferences.XEditor editor = sp.edit();
		editor.putInt(key, value);
		editor.commit();
	}
	public static int getIntData(Context context,String key){
		return getIntData(context, Config.fileName, key);
	}
	public static int getIntData(Context context,String filename,String key){
		XSharedPreferences sp = getSharedPreferences(context, filename);
		return sp.getInt(key, 0);
	}
	
	
	
	public static void saveLongData(Context context,String key,long value){
		saveLongData(context, Config.fileName, key, value);
	}
	public static void saveLongData(Context context,String filename,String key,long value){
		XSharedPreferences sp = getSharedPreferences(context, filename);
		XSharedPreferences.XEditor editor = sp.edit();
		editor.putLong(key, value);
		editor.commit();
	}
	public static long getLongData(Context context,String key){
		return getLongData(context, Config.fileName, key);
	}
	public static long getLongData(Context context,String filename,String key){
		XSharedPreferences sp = getSharedPreferences(context, filename);
		return sp.getLong(key, 0);
	}
	
	
	
	
	public static void saveFloatData(Context context,String key,float value){
		saveFloatData(context, Config.fileName, key, value);
	}
	public static void saveFloatData(Context context,String filename,String key,float value){
		XSharedPreferences sp = getSharedPreferences(context, filename);
		XSharedPreferences.XEditor editor = sp.edit();
		editor.putFloat(key, value);
		editor.commit();
	}
	public static float getFloatData(Context context,String key){
		return getFloatData(context, Config.fileName, key);
	}
	public static float getFloatData(Context context,String filename,String key){
		XSharedPreferences sp = getSharedPreferences(context, filename);
		return sp.getFloat(key, 0.0f);
	}

	
	
	
	public static void saveBooleanData(Context context,String key,boolean value){
		saveBooleanData(context, Config.fileName, key, value);
	}
	public static void saveBooleanData(Context context,String filename,String key,boolean value){
		XSharedPreferences sp = getSharedPreferences(context, filename);
		XSharedPreferences.XEditor editor = sp.edit();
		editor.putBoolean(key, value);
		editor.commit();
	}
	public static boolean getBooleanData(Context context,String key){
		return getBooleanData(context, Config.fileName, key, false);
	}
	public static boolean getBooleanData(Context context,String key,boolean defaultBoolean){
		return getBooleanData(context, Config.fileName, key, defaultBoolean);
	}
	public static boolean getBooleanData(Context context,String filename,String key,boolean defaultBoolean){
		XSharedPreferences sp = getSharedPreferences(context, filename);
		return sp.getBoolean(key, false);
	}
	
	private static XSharedPreferences getSharedPreferences(Context context,String fileName){
		return new XSharedPreferences(context, "", fileName);
	}

	public static XSharedPreferences getDefaultSharedPreferences(Context context){
		return getSharedPreferences(context, Config.fileName);
	}
}
