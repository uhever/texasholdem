package com.zhanshow.library;


import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.text.TextUtils;
import android.util.Log;
import android.webkit.URLUtil;
import android.widget.Toast;

import com.meituan.android.walle.WalleChannelReader;
import com.zhanshow.library.sp.SharedPreferencesUtils;
import com.zhanshow.library.utils.PackageUtils;
import com.zhanshow.library.utils.ZLUrl;

public class Config {

	public static final String UMENG_KEY = "57e571c3e0f55ae5160053ec";
	public static final String UMENG_CHANNEL = "UMENG_CHANNEL";
	/**
	 * 控制日志打印，如果是debug包打印所有log,如果是正式relase包则只打印warn和error
	 * @return
	 */
	public static int getLogLevel(){
		if(BuildConfig.DEBUG){
			return 0;
		}
		return 4;//release包隐藏warn级别一下的日志Late
	}
//	public static final String UC_REQUEST_PREFIX = "http://192.168.21.34:12356/";
//	public static final String ZHIBO_REQUEST_PREFIX = "http://106.75.51.187:9501/";
//	public static final String FIRST_ZHIBO_REQUEST_PREFIX = "http://106.75.51.187/uc.live.com/";
//	public static final String PAY_ZHIBO_REQUEST_PREFIX = "http://106.75.51.187/pay.live.com/";
//	public static final String UPLOAD_IMAGE_PREFIX = "http://update.bdwsw.zhanchenggame.com/";


	public static final String UC_REQUEST_PREFIX = "http://uc-platform.jjshowtime.com/";
	public static final String ZHIBO_REQUEST_PREFIX = "http://app.live.jjshowtime.com/";
	public static final String FIRST_ZHIBO_REQUEST_PREFIX = "http://uc.live.jjshowtime.com/";
	public static final String PAY_ZHIBO_REQUEST_PREFIX = "http://pay.live.jjshowtime.com/";
	public static final String UPLOAD_IMAGE_PREFIX = "http://img.live.jjshowtime.com/";


	public static final String UPLOAD_IMAGE_MAP_PREFIX = "http://img.live.jjshowtime.com/";
	public static final String POKER_REQUEST_PREFIX = "http://pay.poker.yeyeyouxi.net/";

	private static final boolean mIsDebug = false; //true为测试环境，false为正式环境
	public  static final boolean other_btm_on_off = true; //他人页面圈子的开关  true代表打开 false代表关闭

	public static boolean isDebugMode(Context context){
//		if(BuildConfig.DEBUG){
//			return SharedPreferencesUtils.getIntData(context,"isOnline")==1;//0是正式环境，1是测试环境
//		}
		return mIsDebug;
	}

	public static int payMethod = 1;//1是ping++ 支付宝支付,2是ping++ 微信支付，3是ping++ 支付宝+微信支付 ，4是爱贝支付
	public  static int getPayMethod(Context context){
		return payMethod;
	}
	public static final String AIBEI_APPID = "3008169122";


	public  static final int loginType = 1;//登录类型：1普通版，2简洁版(只有微信登录和手机登录),3最简版(只有手机登录)
	public static int getLoginType(Context context){
		return loginType;
	}

	public static final String fileName = "live_config";
	public static final String sdkFloatVersion = "1.3.5";
	public static final int plantform = 2; //1代表ios,2代表安卓，3代表wp
	public static final int serverVersion = 2;
	private static final String mRegionCode = "zhCN";
	private static final String LANGUAGE_POSTFIX = "zh";
	public static String getAppId(Context context){

		if(isDebugMode(context)){
			//测试
			return "28";
		}
		return "1";
	}
	public static String getAppKey(Context context){
		if(isDebugMode(context)){
			return "7aafc6822374c185eee3eda934bef0b3";
		}
//		return "c19f53df0a842d4a599b2335a1579b0d";
		return "4ea514d2b9d66db4f780a39ae96efb26";
	}
	public static String getSDKVersion(Context context){
		return sdkFloatVersion + "_" +  mRegionCode + "_" + getScreenOriention(context)+"_" + LANGUAGE_POSTFIX;
	}
	private static int getScreenOriention(Context context){
		if(context.getResources().getConfiguration().orientation==1){
			//竖屏
			return 1;
		}
		else{
			//横屏
			return 2;
		}
	}


	public static  String getPaySuccessUrl(Context context) {
		if(isDebugMode(context)){
			return "http://42.62.78.20/pay.live.com/payzc.php";//线下
		}
		return "http://pay.live.jjshowtime.com/payzc.php";//线上
	}
	public static final String WEIBO_APP_ID = "247079852";
//	public static final String WEIBO_APP_ID = "3242651170";//闪电红包
	public static String getWeiboAppId(Context context){
		return "WEIBO_APP_ID";
	}


	public static final String REDIRECT_URL = "https://api.weibo.com/oauth2/default.html";// 应用的回调页
//	public static final String REDIRECT_URL = "http://app.flyme.cn/apps/public/detail?package_name=com.paozhuanyinyu.justice.done";// 闪电红包的回调页
	public static final String SCOPE = 							   // 应用申请的高级权限
			"email,direct_messages_read,direct_messages_write,"
					+ "friendships_groups_read,friendships_groups_write,statuses_to_me_read,"
					+ "follow_app_official_microblog," + "invitation_write";


	public static final String WECHAT_APP_ID = "1215";//来去麻将
	public static String getWechatAppId(Activity context){
		String wechat_appid = WECHAT_APP_ID;
		try {
			wechat_appid = context.getPackageManager().getApplicationInfo(context.getPackageName(), PackageManager.GET_META_DATA).metaData.getString("WECHAT_APPID");
			XLog.d("wechat_appid: " + wechat_appid);
		} catch (PackageManager.NameNotFoundException e) {
			e.printStackTrace();
		}
		return wechat_appid;
	}


	public static final String QQ_APP_ID = "1105577663";//1105678494;1105577663
	public static String getQqAppId(Context context){
		return QQ_APP_ID;
	}
	public static String getProduceId(Context context) {
		return "0";
	}

	public static final int USER_PROTOCAL = 97;
	public static final int WAP_PAY = 98;
	public static final String PROTOCAL_PREFIX = "http://zcpub/";
	public static final String BANNER_PREFIX = "http://zcpub/banner/";

	//常见问题url
	public static final String NORMAL_QUESTION = "http://uc.live.jjshowtime.com/static/question_jj.html";
    //JJ直播服务和隐私条款
    public static final String PRIVACY_JJ = "http://uc.live.jjshowtime.com/static/privacy_jj.html";


    public static  String getGameCenterUrl(Context context,String uid,String app_skin,String token){


        return ZLUrl.getFullUrl(SharedPreferencesUtils.getStringData(context,"game_center_url","http://www.baidu.com"),uid,app_skin,token);

    }


    public static  String getWeChatUrl(Context context,String uid,String app_skin,String token){
        StringBuilder builder = new StringBuilder();
        builder.append(SharedPreferencesUtils.getStringData(context,"wechat_url","http://www.baidu.com"))
                .append("&app_channel_id=2&app_token_uid=")
                .append(uid)
                .append("&app_version=")
                .append(PackageUtils.getVersionCode(context))
                .append("&app_skin=")
                .append(app_skin)
                .append("&token=")
                .append(token)
                .append("&uid=")
                .append(uid);
        return builder.toString();
    }


    public static  String getBankUrl(Context context,String uid,String app_skin,String token){

		return ZLUrl.getFullUrl(SharedPreferencesUtils.getStringData(context,"bank_url","http://www.baidu.com"),uid,app_skin,token);
    }
	public static String getCertification(Context context,String uid,String app_skin,String token){
		StringBuilder builder=new StringBuilder();
		builder.append(SharedPreferencesUtils.getStringData(context,"auth_url","http://42.62.78.20:9501/?c=member&m=certification"))
				.append(uid)
				.append("&app_channel_id=2&app_token_uid=")
				.append("&app_version=")
				.append(PackageUtils.getVersionCode(context))
				.append("&app_skin=")
                .append(app_skin)
                .append("&token=")
                .append(token)
                .append("&uid=")
                .append(uid);
		return builder.toString();

	}

    public static final int  REFRESH_DELAY_NUMBER = 300;
	//聊天室最大重连次数
    public static final int  CHAT_MAX_CONNECT = 3;

    public static final String QIYU_ICON = "http://imgcdn.live.3-6-5.com.cn/cdn/liveimg/icon/kefu.jpg";

	public static String getUmengChannel(Context context){
		String channel = WalleChannelReader.getChannel(context.getApplicationContext());
		XLog.d("channel:" + channel);
		if(!TextUtils.isEmpty(channel)){
			return channel;
		}
		return "com";
	}

}
