package com.zhanyou.kay.youchat.bean.game;

import java.util.List;

/**
 * Created by kay on 16/7/13.
 */
public class LoginData {
    private int status;
    private String zcid;
    private String uid;
    private String message;
    private String token;
    private List<ServerInfo> serverlist;
    public List<ServerInfo> getServerlist() {
        return serverlist;
    }

    public void setServerlist(List<ServerInfo> serverlist) {
        this.serverlist = serverlist;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getZcid() {
        return zcid;
    }

    public void setZcid(String zcid) {
        this.zcid = zcid;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getMessage() {
        return message;
    }

    public void setMessgage(String messgage) {
        this.message = messgage;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

}
