package com.zhanyou.kay.youchat.bean.game;

/**
 * Created by kay on 16/11/3.
 */

public class UCAiBeiBean {

    /**
     * transid : 32421611032014307893
     */

    private RequestParamBean requestParam;

    public RequestParamBean getRequestParam() {
        return requestParam;
    }

    public void setRequestParam(RequestParamBean requestParam) {
        this.requestParam = requestParam;
    }

    public static class RequestParamBean {
        private String transid;

        public String getTransid() {
            return transid;
        }

        public void setTransid(String transid) {
            this.transid = transid;
        }
    }
}
