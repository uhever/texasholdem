package com.zhanshow.library.game;

import com.zhanshow.library.Config;
import com.zhanyou.kay.youchat.bean.game.GameOrderBean;
import com.zhanyou.kay.youchat.bean.game.GameUCMobileLoginData;
import com.zhanyou.kay.youchat.bean.game.GameUCWechatLoginData;
import com.zhanyou.kay.youchat.bean.game.LoginData;
import com.zhanyou.kay.youchat.bean.game.LoginNetEaseData;
import com.zhanyou.kay.youchat.bean.game.UCAiBeiBean;
import com.zhanyou.kay.youchat.bean.game.GameUCResponse;

import java.util.Map;

import retrofit2.http.Body;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;
import rx.Observable;

/**
 * Created by kay on 16/12/13.
 */

public interface GameRequestService {
    /**
     * 创建支付订单
     * @param map
     * @return
     */
    @FormUrlEncoded @POST(Config.PAY_ZHIBO_REQUEST_PREFIX + "index.php?c=payzc&m=create_prepay_order")
    Observable<GameOrderBean> createPokerOrder(@FieldMap Map<String,String> map);

    /**
     * 爱贝支付
     * @param request
     * @return
     */
    @POST(Config.UC_REQUEST_PREFIX + "pay/iapppayinitiate")
    Observable<GameUCResponse<UCAiBeiBean>> aibeiPay(@Body String request);


    /**
     * 手机验证码登录
     * @param request
     * @return
     */
    @POST(Config.UC_REQUEST_PREFIX + "member/mobilelogin")
    Observable<GameUCResponse<GameUCMobileLoginData>> mobileLogin(@Body String request);


    /**
     * 发送短信验证码
     * @param request
     * @return
     */
    @POST(Config.UC_REQUEST_PREFIX + "member/mobileseccodesend")
    Observable<GameUCResponse> sendMsgCode(@Body String request);

    /**
     * 游客登录
     * @param request
     * @return
     */
    @POST(Config.UC_REQUEST_PREFIX+"member/trial")
    Observable<GameUCResponse<GameUCMobileLoginData>> visitorLogin(@Body String request);


    /**
     * 登录UC
     * @param params
     * @return
     */
    @FormUrlEncoded @POST(Config.FIRST_ZHIBO_REQUEST_PREFIX+"index.php?c=userzc&m=login")
    Observable<LoginData> login(@FieldMap Map<String, String> params);

    /**
     * 用户中心微信登录
     * @param request
     * @return
     */
    @POST(Config.UC_REQUEST_PREFIX + "member/wclogin")
    Observable<GameUCResponse<GameUCWechatLoginData>> wechatLogin(@Body String request);

    /**
     * 登录网易服务器
     * @param params
     * @return
     */
    @FormUrlEncoded @POST(Config.ZHIBO_REQUEST_PREFIX+"?c=login&m=user_token")
    Observable<LoginNetEaseData> loginNetEase(@FieldMap Map<String, String> params);
}
