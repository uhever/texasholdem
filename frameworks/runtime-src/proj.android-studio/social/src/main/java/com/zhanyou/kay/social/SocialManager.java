package com.zhanyou.kay.social;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.util.Log;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.tencent.mm.sdk.modelmsg.SendAuth;
import com.tencent.mm.sdk.modelmsg.SendMessageToWX;
import com.tencent.mm.sdk.modelmsg.WXImageObject;
import com.tencent.mm.sdk.modelmsg.WXMediaMessage;
import com.tencent.mm.sdk.modelmsg.WXTextObject;
import com.tencent.mm.sdk.modelmsg.WXWebpageObject;
import com.tencent.mm.sdk.openapi.IWXAPI;
import com.tencent.mm.sdk.openapi.WXAPIFactory;
import com.zhanshow.library.XLog;
import java.io.ByteArrayOutputStream;

/**
 * Created by kay on 17/1/16.
 */

public class SocialManager {
    private boolean isGlideLoad;
    private IWXAPI iwxapi;
    private static SocialManager instance;
    private static Object INSTANCE_LOCK = new Object();
    private SocialManager(){

    }
    public static SocialManager getInstance(){
        if(instance==null){
            synchronized (INSTANCE_LOCK){
                instance = new SocialManager();
            }
        }
        return instance;
    }

    public void initWechat(Context context,String wechatAppId){
        iwxapi = WXAPIFactory.createWXAPI(context,wechatAppId,true);
        iwxapi.registerApp(wechatAppId);
    }

    public boolean isWechatInstalled(){
        boolean isWechatInstalled = iwxapi.isWXAppInstalled();
        Log.d("zhanle","isWechatInstalled: "+isWechatInstalled);
        return iwxapi.isWXAppInstalled();
    }

    public void lanuchWechatLogin(){
        SendAuth.Req req = new SendAuth.Req();
        req.scope = "snsapi_userinfo";
        req.state = "zhanyou";
        iwxapi.sendReq(req);
    }
    public void share(Context context,String param,int iconId){
        XLog.d("share param: " + param);
        String[] params = param.split("&");
        String shareContentType = params[0];
        int scene = Integer.parseInt(params[1]);
        String source = params[2];
        if("text".equals(shareContentType)){
            shareTextToWechat(source,scene);
        }else if("image".equals(shareContentType)){
            shareImageToWechat(context,source,scene,iconId);
        }else if("link".equals(shareContentType)){
            String title = "";
            if(params.length>=4){
                title = params[3];
            }
            String description = "";
            if(params.length>=5){
                description = params[4];
            }
            sharePageToWechat(context,source,iconId,title,description,scene);
        }

    }
    public void shareTextToWechat(String text,int scene) {
        WXTextObject wxTextObject = new WXTextObject();
        wxTextObject.text = text;

        WXMediaMessage msg = new WXMediaMessage();
        msg.mediaObject = wxTextObject;
        msg.description = text;

        SendMessageToWX.Req req = new SendMessageToWX.Req();
        req.transaction = buildTransaction("text");
        req.message = msg;
        req.scene = scene;
        iwxapi.sendReq(req);
    }

    public void shareImageToWechat(final Context context, String imagePath,final int scene,final int iconId) {
        Log.d("SocialManager","path: " + imagePath);
        isGlideLoad = true;
        final SimpleTarget<Bitmap> simpleTarget = new SimpleTarget<Bitmap>() {

            @Override
            public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                WXImageObject imageObject = new WXImageObject(resource);
                WXMediaMessage msg = new WXMediaMessage();
                msg.mediaObject = imageObject;
                SendMessageToWX.Req req = new SendMessageToWX.Req();
                req.transaction = buildTransaction("img");
                req.message = msg;
                req.scene = scene;
                iwxapi.sendReq(req);
            }

            @Override
            public void onLoadFailed(Exception e, Drawable errorDrawable) {
                super.onLoadFailed(e, errorDrawable);
                if(isGlideLoad){
                    Bitmap thumb = BitmapFactory.decodeResource(context.getResources(), iconId);
                    WXImageObject imageObject = new WXImageObject(thumb);
                    WXMediaMessage msg = new WXMediaMessage();
                    msg.mediaObject = imageObject;
                    SendMessageToWX.Req req = new SendMessageToWX.Req();
                    req.transaction = buildTransaction("img");
                    req.message = msg;
                    req.scene = scene;
                    iwxapi.sendReq(req);
                    isGlideLoad = false;
                }
            }
        };
        Glide.with(context)
                .load(imagePath)
                .asBitmap()
                .placeholder(iconId)
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .error(iconId)
                .into(simpleTarget);

    }
    public void sharePageToWechat(Context context,String pageUrl,int iconId,String tile,String description,int scene){
        WXWebpageObject webpage = new WXWebpageObject();
        webpage.webpageUrl = pageUrl;
        WXMediaMessage msg = new WXMediaMessage(webpage);
        msg.title = tile;
        msg.description = description;
        Bitmap thumb = BitmapFactory.decodeResource(context.getResources(), iconId);
        msg.thumbData = bmpToByteArray(thumb, true);

        SendMessageToWX.Req req = new SendMessageToWX.Req();
        req.transaction = buildTransaction("webpage");
        req.message = msg;
        req.scene = scene;
        iwxapi.sendReq(req);
    }

    private Bitmap drawBg4Bitmap(int color, Bitmap orginBitmap) {
        Paint paint = new Paint();
        paint.setColor(color);
        Bitmap bitmap = Bitmap.createBitmap(orginBitmap.getWidth(),
                orginBitmap.getHeight(), orginBitmap.getConfig());
        Canvas canvas = new Canvas(bitmap);
        canvas.drawRect(0, 0, orginBitmap.getWidth(), orginBitmap.getHeight(), paint);
        canvas.drawBitmap(orginBitmap, 0, 0, paint);
        return bitmap;
    }
    private   byte[] bmpToByteArray(final Bitmap bmp, final boolean needRecycle) {
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, output);
        if (needRecycle) {
            bmp.recycle();
        }

        byte[] result = output.toByteArray();
        try {
            output.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }
    private  String buildTransaction(final String type) {
        return (type == null) ? String.valueOf(System.currentTimeMillis()) : type + System.currentTimeMillis();
    }
}
