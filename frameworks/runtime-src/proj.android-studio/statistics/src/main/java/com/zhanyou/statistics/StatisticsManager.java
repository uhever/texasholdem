package com.zhanyou.statistics;

import android.content.Context;

import com.umeng.analytics.MobclickAgent;

import java.util.Map;

/**
 * Created by kay on 16/12/16.
 */

public class StatisticsManager {
    private static StatisticsManager instance;
    private static Object INSTANCE_LOCK = new Object();
    private StatisticsManager(){

    }
    public static StatisticsManager getInstance(){
        if(instance==null){
            synchronized (INSTANCE_LOCK){
                instance = new StatisticsManager();
            }
        }
        return instance;
    }

    /**
     * //TODO 设置统计场景类型，暂时只有游戏
     * @param context
     */
    public void setSceneType(Context context){
        MobclickAgent.setScenarioType(context, MobclickAgent.EScenarioType.E_UM_GAME);
    }

    /**
     * //TODO 初始化友盟，设置appkey,channelId,场景类型
     * @param context
     * @param appkey
     * @param channelId
     */
    public void init(Context context,String appkey,String channelId){
        MobclickAgent.UMAnalyticsConfig  umAnalyticsConfig = new MobclickAgent.UMAnalyticsConfig(context,appkey,channelId,MobclickAgent.EScenarioType.E_UM_GAME);
        MobclickAgent.startWithConfigure(umAnalyticsConfig);
    }

    /**
     * 界面可见
     * @param context
     */
    public void onResume(Context context){
        MobclickAgent.onResume(context);
    }

    /**
     * 界面不可见
     * @param context
     */
    public void onPause(Context context){
        MobclickAgent.onPause(context);
    }

    /**
     * 是否加密友盟产生的日志
     * @param isEnable
     */
    public void enableEncrypt(boolean isEnable){
        MobclickAgent.enableEncrypt(isEnable);
    }

    /**
     * 计数事件
     * @param context
     * @param eventId
     */
    public void eventCount(Context context,String eventId){
        MobclickAgent.onEvent(context,eventId);
    }

    /**
     * 行为事件
     * @param context
     * @param eventId
     * @param map
     */
    public void eventAction(Context context,String eventId,Map map){
        MobclickAgent.onEvent(context,eventId,map);
    }
}
