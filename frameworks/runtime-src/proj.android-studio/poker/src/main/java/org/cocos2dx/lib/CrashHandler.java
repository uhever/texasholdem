package org.cocos2dx.lib;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.lang.Thread.UncaughtExceptionHandler;
import java.lang.reflect.Field;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import com.zhanyouall.poker.AppController;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.Looper;
import android.util.Base64;
import android.util.Log;
import android.view.WindowManager;
import android.widget.Toast;

/**
 * UncaughtException处理类,当程序发生Uncaught异常的时候,由该类来接管程序,并记录发送错误报告.
 * 
 * @author way
 * 
 */
public class CrashHandler implements UncaughtExceptionHandler {
	private UncaughtExceptionHandler mDefaultHandler;// 系统默认的UncaughtException处理类
	private static CrashHandler INSTANCE;// CrashHandler实例
	private Context mContext;// 程序的Context对象
	private boolean isReport = false;
	
    private static final String TAG = "uploadFile";  
    private static final int TIME_OUT = 10 * 10000000; // 超时时间  
    private static final String CHARSET = "utf-8"; // 设置编码  
    public static final String SUCCESS = "1";  
    public static final String FAILURE = "0";  
 
	/** 保证只有一个CrashHandler实例 */
	private CrashHandler() {

	}

	/** 获取CrashHandler实例 ,单例模式 */
	public static CrashHandler getInstance() {
		if (INSTANCE == null)
			INSTANCE = new CrashHandler();
		return INSTANCE;
	}

	/**
	 * 初始化
	 * 
	 * @param context
	 */
	public void init(Context context) {
		mContext = context;

		mDefaultHandler = Thread.getDefaultUncaughtExceptionHandler();// 获取系统默认的UncaughtException处理器
		Thread.setDefaultUncaughtExceptionHandler(this);// 设置该CrashHandler为程序的默认处理器
	}

	/**
	 * 当UncaughtException发生时会转入该重写的方法来处理
	 */
	public void uncaughtException(Thread thread, Throwable ex) {
		if (!handleException(ex) && mDefaultHandler != null) {
			// 如果自定义的没有处理则让系统默认的异常处理器来处理
			mDefaultHandler.uncaughtException(thread, ex);
		}else {  
            try {  
                Thread.sleep(3000);// 如果处理了，让程序继续运行3秒再退出，保证文件保存并上传到服务器  
            } catch (InterruptedException e) {  
                e.printStackTrace();  
            }  
            
            // 退出程序  
            Intent intent = new Intent(mContext.getApplicationContext(), AppController.class);  
            PendingIntent restartIntent = PendingIntent.getActivity(    
            		mContext.getApplicationContext(), 0, intent,    	
                    Intent.FLAG_ACTIVITY_NEW_TASK);                                                 
            AlarmManager mgr = (AlarmManager)mContext.getSystemService(Context.ALARM_SERVICE);    
            mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 1000,    
                    restartIntent); // 1秒钟后重启应用   
            ((Activity)mContext).finish();  
            
            android.os.Process.killProcess(android.os.Process.myPid());  
            System.exit(1);  
        }  
	}

	/**
	 * 自定义错误处理,收集错误信息 发送错误报告等操作均在此完成.
	 * 
	 * @param ex
	 *            异常信息
	 * @return true 如果处理了该异常信息;否则返回false.
	 */
	public boolean handleException(Throwable ex) {
		if (ex == null || mContext == null)
			return false;
		new Thread() {
			public void run() {
				Looper.prepare();
				Toast.makeText(mContext, "检测到即时网络不稳定,为了让您减少损失，即将为您重启游戏", 0).show();  
				if (!isReport) {
					
//					if (Sanguo.isSendLogCat) {
//						httpUrlConnection();
//
//					}
					
			//		uploadFile(LogcatHelper.getInstance(mContext).file,mContext);
			//		sendAppCrashReport(mContext, crashReport, file);
	
				}
				Looper.loop();
			}

		}.start();
		return true;
	}

	private File saveFile(String crashReport) {
		// TODO Auto-generated method stub
		String fileName = "crash-" + System.currentTimeMillis() + ".txt";
		if (Environment.getExternalStorageState().equals(
				Environment.MEDIA_MOUNTED)) {
			try {
				File dir = new File(Environment.getExternalStorageDirectory()
						.getAbsolutePath() + File.separator + "crash");
				if (!dir.exists())
					dir.mkdir();
				File file = new File(dir, fileName);
				FileOutputStream fos = new FileOutputStream(file);
				fos.write(crashReport.toString().getBytes());
				fos.close();
				return file;
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	private void sendAppCrashReport(final Context context,
			final String crashReport, final File file) {
		// TODO Auto-generated method stub
		isReport = true;
		AlertDialog mDialog = null;
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setIcon(android.R.drawable.ic_dialog_info);
		builder.setTitle("程序出错啦");
		builder.setMessage("请把错误报告以邮件的形式提交给我们，谢谢！");
		builder.setPositiveButton(android.R.string.ok,
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {

						// 发送异常报告
						try {
							//注释部分是已文字内容形式发送错误信息
							// Intent intent = new Intent(Intent.ACTION_SENDTO);
							// intent.setType("text/plain");
							// intent.putExtra(Intent.EXTRA_SUBJECT,
							// "推聊Android客户端 - 错误报告");
							// intent.putExtra(Intent.EXTRA_TEXT, crashReport);
							// intent.setData(Uri
							// .parse("mailto:way.ping.li@gmail.com"));
							// intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
							// context.startActivity(intent);
							
							//下面是以附件形式发送邮件
							Intent intent = new Intent(Intent.ACTION_SEND);
							intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
							String[] tos = { "492217311@qq.com" };
							intent.putExtra(Intent.EXTRA_EMAIL, tos);

							intent.putExtra(Intent.EXTRA_SUBJECT,
									"暴打魏蜀吴 - 错误报告");
							if (file != null) {
								intent.putExtra(Intent.EXTRA_STREAM,
										Uri.fromFile(file));
								intent.putExtra(Intent.EXTRA_TEXT,
										"请将此错误报告发送给我，以便我尽快修复此问题，谢谢合作！\n");
							} else {
								intent.putExtra(Intent.EXTRA_TEXT,
										"请将此错误报告发送给我，以便我尽快修复此问题，谢谢合作！\n"
												+ crashReport);
							}
							intent.setType("text/plain");
							intent.setType("message/rfc882");
							Intent.createChooser(intent, "Choose Email Client");
							context.startActivity(intent);
						} catch (Exception e) {
							Toast.makeText(context,
									"There are no email clients installed.",
									Toast.LENGTH_SHORT).show();
						} finally {
							dialog.dismiss();
							// 退出
							android.os.Process.killProcess(android.os.Process
									.myPid());
							System.exit(1);
						}
					}
				});
		builder.setNegativeButton(android.R.string.cancel,
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
						// 退出
						android.os.Process.killProcess(android.os.Process
								.myPid());
						System.exit(1);
					}
				});
		mDialog = builder.create();
		mDialog.getWindow().setType(
				WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
		mDialog.show();
	}

	/**
	 * 获取APP崩溃异常报告
	 * 
	 * @param ex
	 * @return
	 */
	private String getCrashReport(Context context) {
		PackageInfo pinfo = getPackageInfo(context);
		StringBuffer exceptionStr = new StringBuffer();
		exceptionStr.append("Version: " + pinfo.versionName + "("
				+ pinfo.versionCode + ")\n");
		exceptionStr.append("Android: " + Build.VERSION.RELEASE
				+ "(" + Build.MODEL + ")\n");
		exceptionStr.append("DeviceID: " 
				+ "(" + GameConfig.getDeviceID(context) + ")\n");
		exceptionStr.append("MacID: " 
				+ "(" + AppController.getMacID() + ")\n");
		exceptionStr.append("PackageName: " 
				+ "(" + pinfo.packageName + ")\n");
		
		
//		exceptionStr.append("Exception: " + ex.getMessage() + "\n");
//		StackTraceElement[] elements = ex.getStackTrace();
//		for (int i = 0; i < elements.length; i++) {
//			exceptionStr.append(elements[i].toString() + "\n");
//		}
		return exceptionStr.toString();
	}

	/**
	 * 获取App安装包信息
	 * 
	 * @return
	 */
	private PackageInfo getPackageInfo(Context context) {
		PackageInfo info = null;
		try {
			info = context.getPackageManager().getPackageInfo(
					context.getPackageName(), 0);
		} catch (NameNotFoundException e) {
			// e.printStackTrace(System.err);
			// L.i("getPackageInfo err = " + e.getMessage());
		}
		if (info == null)
			info = new PackageInfo();
		return info;
	}
	public static String getString(File file) {
		InputStream inputStream =null;
		try {
			inputStream = new FileInputStream(file);
		} catch (FileNotFoundException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		} 
	    InputStreamReader inputStreamReader = null;  
	    try {  
	        inputStreamReader = new InputStreamReader(inputStream, "utf-8");  
	    } catch (UnsupportedEncodingException e1) {  
	        e1.printStackTrace();  
	    }  
	    BufferedReader reader = new BufferedReader(inputStreamReader);  
	    StringBuffer sb = new StringBuffer("");  
	    String line;  
	    try {  
	        while ((line = reader.readLine()) != null) {  
	            sb.append(line);  
	            sb.append("\n");  
	        }  
	    } catch (IOException e) {  
	        e.printStackTrace();  
	    }  
	    Log.e("leo_k","neirong = = = ="+sb.toString());
	    return sb.toString();  
	}  
    public static String uploadFile(File file,Context context) {  
        String BOUNDARY = UUID.randomUUID().toString(); // 边界标识 随机生成  
        String PREFIX = "--", LINE_END = "\r\n";  
        String CONTENT_TYPE = "multipart/form-data"; // 内容类型  
        String RequestURL = "http://42.62.78.20/www.kapai.file.com/index.php?c=file&m=save&type=changshaosh";  
        
        //c=file&m=save&content=%27changshaoshuainihaoasdfsadf%27&type=changshaosh
        try {  
            URL url = new URL(RequestURL);  
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();  
            conn.setReadTimeout(TIME_OUT);  
            conn.setConnectTimeout(TIME_OUT);  
            conn.setUseCaches(false); // 不允许使用缓存  
            conn.setRequestMethod("POST"); // 请求方式  
            conn.setRequestProperty("Charset", CHARSET); // 设置编码  
            conn.setRequestProperty("content", getString(file));  
                /** 
                 * 获取响应码 200=成功 当响应成功，获取响应的流 
                 */  

                int res = conn.getResponseCode();  
                if (res == 200) {  
                	
                	
                    return SUCCESS;  
                }  
//            }  
        } catch (MalformedURLException e) {  
            e.printStackTrace();  
        } catch (IOException e) {  
            e.printStackTrace();  
        }  
        return FAILURE;  
    }  
    public void httpUrlConnection() { 
    	try { 
    		isReport = true;
	    	String pathUrl = "http://42.62.78.20/www.kapai.file.com/index.php?c=file&m=save&type="+Cocos2dxHelper.getDeviceModel()+"&macid="+AppController.getMacID()+"&deviceid="+GameConfig.getDeviceID(mContext);
	    	// 建立连接 
	    	URL url = new URL(pathUrl); 
	    	HttpURLConnection httpConn = (HttpURLConnection) url.openConnection(); 
	    	 
	    	// //设置连接属性 
	    	httpConn.setDoOutput(true);// 使用 URL 连接进行输出 
	    	httpConn.setDoInput(true);// 使用 URL 连接进行输入 
	    	httpConn.setUseCaches(false);// 忽略缓存 
	    	httpConn.setRequestMethod("POST");// 设置URL请求方法 
	    	// 设置请求属性 
	    	// 获得数据字节数据，请求数据流的编码，必须和下面服务器端处理请求流的编码一致 
	    	String contentString = getString(LogcatHelper.getInstance(mContext).file);
	    	httpConn.getURL().toString();
	    	// 建立输出流，并写入数据 
	    	contentString = "content="+getCrashReport(mContext)+"\n"+contentString;
	    	OutputStream outputStream = httpConn.getOutputStream(); 
	    	outputStream.write(contentString.getBytes("UTF-8")); 

	    	outputStream.close(); 
	    	// 获得响应状态 
	    	int responseCode = httpConn.getResponseCode(); 
	    	 
	    	if (HttpURLConnection.HTTP_OK == responseCode) {// 连接成功 
	    	// 当正确响应时处理数据 
		    	StringBuffer sb = new StringBuffer(); 
		    	String readLine; 
		    	BufferedReader responseReader; 
		    	// 处理响应流，必须与服务器响应流输出的编码一致 
		    	responseReader = new BufferedReader(new InputStreamReader(httpConn.getInputStream(), "utf-8")); 
		    	while ((readLine = responseReader.readLine()) != null) { 
		    		sb.append(readLine).append("\n"); 
		    	} 
		    	Log.e("leo_k", "sb  ==  == "+sb.toString());
	
		    	responseReader.close(); 
		    	LogcatHelper.getInstance(mContext).file = new File(LogcatHelper.PATH_LOGCAT, "GPS-"  
                        + MyDate.getFileName() + ".txt");
		    	LogcatHelper.getInstance(mContext).out = new FileOutputStream(LogcatHelper.getInstance(mContext).file);
		    	isReport = false;
		    	
	    	} 
    	} catch (Exception ex) { 
    	ex.printStackTrace(); 
    	} 

    	 
    	} 

	
}