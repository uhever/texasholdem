package org.cocos2dx.lib;

import java.util.HashMap;
import java.util.Map;

import com.zhanyouall.poker.AppController;

import android.content.Context;
import android.util.Log;

public class MyApi {

	private Map<String, String> FunctionMap = null;

	private AppController mycontext = null;

	public MyApi(Context context) {

		FunctionMap = new HashMap<String, String>();
		mycontext = (AppController) context;

	}

	public void registerCallBackByName(final String name, final int functionID) {
		if (mycontext == null) {
			return;
		}
//		mycontext.runOnUiThread(new Runnable() {
//
//			@Override
//			public void run() {
//				LogUtils.d("java_1111111");
//				FunctionMap.put(name, String.valueOf(functionID));
//			}
//
//		});
		//没必要放进UI线程中执行，map添加元素不涉及刷新UI(蛮荒GooglePlay1.0版本，遇到第一次安装黑屏无法进入的问题
		//，原因是map添加元素发生在了doCallbackByName方法里 CheckIsExist(name)方法之后,但是第二次进入就正常了
		//根本原因还是用了runOnUiThread增加了两个方法执行顺序的不确定性)
		FunctionMap.put(name, String.valueOf(functionID));
//		LogUtils.d("java_222222222");
		Log.e("try", "name" + name + "-------id =" + functionID);

	}

	public void removeCallBackByName(final String name) {
		if (mycontext == null) {

			return;
		}
		mycontext.runOnUiThread(new Runnable() {

			@Override
			public void run() {
				if (FunctionMap.isEmpty()) {

				} else {
					if (CheckIsExist(name)) {
						int functionID = Integer.parseInt(FunctionMap.get(name));
						FunctionMap.remove(FunctionMap.get(name));

						Cocos2dxLuaJavaBridge.releaseLuaFunction(functionID);
					}
				}
			}
		});
	}

	public boolean CheckIsExist(String name) {

		String a = FunctionMap.get(name);
		if (a == null) {

			return false;

		} else if (a.length() == 0) {

			return false;
		} else {
			return true;

		}

	}

	public void doCallbackByName(final String name, final String result) {
		if (mycontext == null) {

			return;
		}
		mycontext.runOnGLThread(new Runnable() {

			@Override
			public void run() {
				if (CheckIsExist(name)) {
					Cocos2dxLuaJavaBridge.callLuaFunctionWithString(
							Integer.parseInt(FunctionMap.get(name)), result);
				}
			}

		});

	}
}
