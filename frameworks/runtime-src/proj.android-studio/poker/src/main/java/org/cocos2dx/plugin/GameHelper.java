package org.cocos2dx.plugin;

import java.io.File;
import java.io.FileInputStream;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.util.Log;

public class GameHelper {

	public static String ADK_APPID = "";

	public static boolean isNetworkAvailable(final Context context) {
		ConnectivityManager cm = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo info = cm.getActiveNetworkInfo();
		if (info != null && info.getState() == NetworkInfo.State.CONNECTED) {
			Log.i("java", "网络正常");
			return true;
		} 
		
//		else {
//
//			Log.i("java", "没有网络");
//
//			AlertDialog.Builder ab = new AlertDialog.Builder(context);
//			ab.setMessage("网络未连接，请设置网络");
//			ab.setPositiveButton("设置网络", new DialogInterface.OnClickListener() {
//				@Override
//				public void onClick(DialogInterface dialog, int which) {
//					// dialog.dismiss();
//					Intent wifiSettingsIntent = new Intent(
//							"android.settings.WIFI_SETTINGS");
//					context.startActivity(wifiSettingsIntent);
//					
//
//				}
//			});
//			ab.setNegativeButton("退出", new DialogInterface.OnClickListener() {
//				@Override
//				public void onClick(DialogInterface dialog, int which) {
//					System.exit(0);
//				}
//			});
//			ab.setOnCancelListener(new DialogInterface.OnCancelListener() {
//
//				@Override
//				public void onCancel(DialogInterface dialog) {
//					// TODO Auto-generated method stub
//
//					System.exit(0);
//				}
//			});
//			ab.show();
//
//		}

		return false;
	}
	public static boolean fileIsExists(String path){
        try{
                File f=new File(path);
                if(!f.exists()){
                        return false;
                }
                
        }catch (Exception e) {
                // TODO: handle exception
                return false;
        }
        return true;
	}
	
	/**
	 * 获取版本号
	 * @return 当前应用的版本号
	 */
	public static int getVersionCode(Context mContext) {
	    try {
	        PackageManager manager = mContext.getPackageManager();
	        PackageInfo info = manager.getPackageInfo(mContext.getPackageName(), 0);
	        int version = info.versionCode;
	        return version;
	    } catch (Exception e) {
	        e.printStackTrace();
	        return 0;
	    }
	}




}
