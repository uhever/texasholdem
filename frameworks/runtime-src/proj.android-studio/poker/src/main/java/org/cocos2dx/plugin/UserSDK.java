package org.cocos2dx.plugin;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import com.zhanshow.library.Config;
import com.zhanshow.library.XLog;
import com.zhanyou.kay.social.SocialManager;
import com.zhanyou.kay.youchat.bean.game.GameOrderBean;
import com.zhanyou.kay.youchat.bean.game.GameRequestCode;
import com.zhanshow.library.game.GameRequestManager;
import com.zhanshow.library.game.GameResponse;
import com.zhanshow.library.sp.SharedPreferencesUtils;
import com.zhanyouall.poker.AppController;
import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.util.Log;
import android.widget.Toast;


public class UserSDK implements SDKHelper {
	private Activity context;
	private static boolean isInit = false;
	public static boolean isLogin = false;
	private static final int RQF_PAY = 1;

	private static final int RQF_LOGIN = 2;
	private static final String TAG = "alipay";

	public UserSDK(Activity context) {
		this.context = context;
	}

	@Override
	public void initSDK() {
		Log.d("zhancheng","initSDK 进来啦");
		isLogin = false;
		isInit = true;
		final AppController SanguoContext = (AppController) context;
	}

	public static boolean isLandscape(Activity context) {
		Configuration config = context.getResources().getConfiguration();
		int orientation = config.orientation;

		if (orientation != Configuration.ORIENTATION_LANDSCAPE
				&& orientation != Configuration.ORIENTATION_PORTRAIT) {
			orientation = Configuration.ORIENTATION_PORTRAIT;
		}

		return (orientation == Configuration.ORIENTATION_LANDSCAPE);
	}


	@Override
	public void loginSDK() {
//		AppController SanguoContext = (AppController) context;
//		String uid = SharedPreferencesUtils.getStringData(context,"uid");
//		String token = SharedPreferencesUtils.getStringData(context,"liveToken");
//		Log.d("zhancheng","1" + "," + uid + "," + token + Config.getProduceId(context));
//		SanguoContext.doCallbackByName("loginResultInApp","1" + "," + uid + "," + token + ","+ Config.getProduceId(context));
	}
	public void loginSDKLive(final String params){
		final AppController SanguoContext = (AppController) context;
		SanguoContext.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				String[] param = params.split("&");
				String loginMethod = param[0];
				if("1".equals(loginMethod)){
					visitorLogin(SanguoContext);
				}else if("2".equals(loginMethod)){
					String phoneNumber =  param[1];
					String authCode = param[2];
					mobileLogin(SanguoContext,phoneNumber,authCode);
				}else if("3".equals(loginMethod)){
					if(SocialManager.getInstance().isWechatInstalled()){
						SocialManager.getInstance().lanuchWechatLogin();
					}else{
						Toast.makeText(SanguoContext,"未安装微信",Toast.LENGTH_SHORT).show();
						SanguoContext.doCallbackByName("loginResultLive","0");
					}
				}
			}
		});

 	}
	public void sendAuthCode(String params){
		AppController SanguoContext = (AppController) context;
		String[] param = params.split("&");
		String phoneNumber = param[0];
		sendMobileCode(SanguoContext,phoneNumber);
	}

	private void visitorLogin(final AppController activity){
		GameRequestManager.getInstance(context).visitorLogin(activity, new GameResponse<Map<String,String>>() {
			@Override
			public void onResponse(int code, String msg, Map<String,String> extraData) {
				if(code==GameRequestCode.LOGIN_SUCCESS){
					String callback = "1," + extraData.get("uid") + "," + extraData.get("token") + "," + Config.getProduceId(activity) + ",,,";
					XLog.d("callback: " + callback);
					//游客登录成功
					activity.doCallbackByName("loginResultLive",callback);
				}else if(code==GameRequestCode.LOGIN_FAILED){
					//游客登录失败
					XLog.e(msg);
					activity.doCallbackByName("loginResultLive","0");
				}
			}
		});
	}
	private void sendMobileCode(final AppController activity,String mobileNumber){
		GameRequestManager.getInstance(activity).sendMobileCode(activity, mobileNumber, new GameResponse() {
			@Override
			public void onResponse(int code, String msg, Object extraData) {
				if(code==GameRequestCode.SEND_MOBILE_CODE_SYCCESS){
					//发送短信验证码成功
					activity.doCallbackByName("authcodeResult","1");
				}else if(code==GameRequestCode.SEND_MOBILE_CODE_FAILED){
					//发送验证码失败
					activity.doCallbackByName("authcodeResult","0");
					XLog.e(msg);
				}
			}
		});
	}

	private void mobileLogin(final AppController activity,String mobileNumber,String code){
		GameRequestManager.getInstance(activity).mobileLogin(activity, mobileNumber, code, new GameResponse<Map<String,String>>() {
			@Override
			public void onResponse(int code, String msg, Map<String,String> extraData) {
				if(code==GameRequestCode.LOGIN_SUCCESS){
					String callback = "1," + extraData.get("uid") + "," + extraData.get("token") + "," + Config.getProduceId(activity) + ",,,";
					XLog.d("callback: " + callback);
					activity.doCallbackByName("loginResultLive",callback);
					//手机登录成功
				}else if(code==GameRequestCode.LOGIN_FAILED){
					//手机登录失败
					XLog.e(msg);
					activity.doCallbackByName("loginResultLive","0");
				}
			}
		});
	}
	public void wechatLogin(final AppController activity,String code){
		GameRequestManager.getInstance(activity).wechatLogin(activity, code, Config.getWechatAppId(activity), String.valueOf(1), new GameResponse<Map<String, String>>() {
			@Override
			public void onResponse(int code, String msg, Map<String, String> extraData) {
				if(code==GameRequestCode.LOGIN_SUCCESS){
					String callback = "1," + extraData.get("uid") + "," + extraData.get("token") + "," + Config.getProduceId(activity)
							+ "," + extraData.get("nickname") + "," + extraData.get("iconurl") + "," + extraData.get("gender");
					XLog.d("callback: " + callback);
					activity.doCallbackByName("loginResultLive",callback);
					//微信登录成功
				}else if(code==GameRequestCode.LOGIN_FAILED){
					//微信登录失败
					XLog.e(msg);
					activity.doCallbackByName("loginResultLive","0");
				}
			}
		});
	}

	@Override
	public void logout() {
		isLogin = false;
		AppController SanguoContext = (AppController) context;

	}


	@Override
	public void doSdkPay(final String params) {
		Log.d("zhancheng", params);
		final AppController SanguoContext = (AppController) context;
		SanguoContext.runOnUiThread(new Runnable() {

			@Override
			public void run() {

				// 将Lua传递过来的支付信息，按&截取成多个字段
				String[] param = params.split("&");
				String type = param[0];
				final String productId = param[1];
				String token = param[2];
				GameRequestManager.getInstance(SanguoContext).createGameOrder(SanguoContext, token, type, productId, "1", "1001", new GameResponse() {
					@Override
					public void onResponse(int code, String msg, Object extraData) {
						if(code== GameRequestCode.CREATE_PAY_ORDER_SUCCESS){
							GameRequestManager.getInstance(SanguoContext).aibeiPay(SanguoContext,String.valueOf(((GameOrderBean)extraData).getProduct_id()),((GameOrderBean)extraData).getApp_order_id(),((GameOrderBean)extraData).getAmount(),((GameOrderBean)extraData).getProduct_name(),Config.getPaySuccessUrl(SanguoContext),"aibei",Config.getPayMethod(context.getBaseContext()), new GameResponse<String>() {
								@Override
								public void onResponse(int code, String msg, String extraData) {
									if(code==GameRequestCode.PAY_SUCCESS){
										SanguoContext.doCallbackByName("payLiveResult","1,"+ extraData);
									}else{
										SanguoContext.doCallbackByName("payLiveResult","0");
									}
								}
							});
						}else{
							SanguoContext.doCallbackByName("payLiveResult","0");
						}
					}
				});

			}
		});


	}
	
	public void switchLanguage(final String params) {
		final AppController SanguoContext = (AppController) context;
	}
	
	public void switchRegion(final String params) {
		Log.d("zhancheng","switchRegion params: " + params);
		final AppController SanguoContext = (AppController) context;

	}
	
	//uid,name,level,gender,serverid,servername,gold,vip,country
	public void submitExtInfo(String params) {
		Log.d("zhancheng", "参数：" + params);
		String[] param = params.split(",");
		String roleName="";
		String serverName="";
		try {
			roleName = URLDecoder.decode(param[1], "utf-8");
			serverName = URLDecoder.decode(param[5], "utf-8");
		} catch (UnsupportedEncodingException e1) {
			e1.printStackTrace();
		}
		Log.d("zhancheng", "roleName:" + roleName);
		final JSONObject jsonExData = new JSONObject();
		try {
			jsonExData.put("appUId", param[0]);
			jsonExData.put("roleId", param[3]);
			jsonExData.put("roleName", roleName);
			jsonExData.put("roleLevel", param[2]);
			jsonExData.put("roleVip", param[7]);
			jsonExData.put("serverId", param[4]);
			jsonExData.put("serverName", serverName);
			final AppController SanguoContext = (AppController) context;
			
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		
	}

	@Override
	public void exitSDK() {
		final AppController SanguoContext = (AppController) context;
	}
	
	// 客服中心
	public void showCustomerServiceCenter() {
		final AppController SanguoContext = (AppController) context;
		
	}

	@Override
	public void switchAccount() {

	}


}
