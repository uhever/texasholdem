package com.zhanyouall.poker.component.view;

/*
 * Copyright (C) 2006 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import android.R.integer;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.content.res.Resources;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnErrorListener;
import android.net.Uri;
import android.os.PowerManager;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.MediaController;
import android.widget.MediaController.MediaPlayerControl;

import java.io.IOException;

import org.cocos2dx.lib.Cocos2dxLuaJavaBridge;

import com.zhanyouall.poker.AppController;
 
/**
 * Displays a video file.  The VideoView class
 * can load images from various sources (such as resources or content
 * providers), takes care of computing its measurement from the video so that
 * it can be used in any layout manager, and provides various display options
 * such as scaling and tinting.
 */
public class VideoView extends SurfaceView implements MediaPlayerControl {
    private String TAG = "zhancheng";
    
    private Context mContext;
    
    // settable by the client
    private String         mUri;
    private int         mDuration;
    public boolean mIsPlaying;
    // All the stuff we need for playing and showing a video
    private SurfaceHolder mSurfaceHolder = null;
    public MediaPlayer mMediaPlayer = null;
    private boolean     mIsPrepared;
    private int         mVideoWidth;
    private int         mVideoHeight;
    private int         mSurfaceWidth;
    private int         mSurfaceHeight;
    private MediaController mMediaController;
    private OnCompletionListener mOnCompletionListener;
    private MediaPlayer.OnPreparedListener mOnPreparedListener;
    private int         mCurrentBufferPercentage;
    private OnErrorListener mOnErrorListener;
    private boolean     mStartWhenPrepared;
    private int         mSeekWhenPrepared;
    
    private static boolean hasAlreadyCallBack = false;//防止多次回调

    private MySizeChangeLinstener mMyChangeLinstener;
    
    public int getVideoWidth(){
    	return mVideoWidth;
    }
    
    public int getVideoHeight(){
    	return mVideoHeight;
    }
    
    public void setVideoScale(int width , int height){
    	LayoutParams lp = getLayoutParams();
    	lp.height = height;
		lp.width = width;
		setLayoutParams(lp);
    }
    
    public interface MySizeChangeLinstener{
    	public void doMyThings();
    }
    
    public void setMySizeChangeLinstener(MySizeChangeLinstener l){
    	mMyChangeLinstener = l;
    }
    
    public VideoView(Context context) {
        super(context);
        mContext = context;
        initVideoView();
    }
    public VideoView(Context context,String path) {
        super(context);
        mContext = context;
        Log.e("zhancheng","new VideoView ");
        setVideoPath(path);
        initVideoView();
    }

    public VideoView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
        mContext = context;
        initVideoView();
    }

    public VideoView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        mContext = context;
        initVideoView();
    }


    private void initVideoView() {
        mVideoWidth = 0;
        mVideoHeight = 0;
        getHolder().addCallback(mSHCallback);
      //  getHolder().setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
        setFocusable(true);
 //       setFocusableInTouchMode(true);
        requestFocus();
        
        Log.e("zhancheng","initVideoView  finish");
    }

    public void setVideoPath(String path) {
         
        Log.e("zhancheng","setVideoPath  whats up!");

        setVideoURI(path);
    }

    public void setVideoURI(String uri) {
    	mUri = uri;
        mStartWhenPrepared = false;
        mSeekWhenPrepared = 0;
      //  openVideo();
        requestLayout();
        invalidate();
    }


    AssetFileDescriptor fileDescriptor;
    private void openVideo() {
    	Log.v(TAG, " openVideo");
        if (mUri == null || mSurfaceHolder == null) {
            // not ready for playback just yet, will try again later
            return;
        }
        // Tell the music playback service to pause
        // TODO: these constants need to be published somewhere in the framework.
        
        Intent i = new Intent("com.android.music.musicservicecommand");
        i.putExtra("command", "pause");
        mContext.sendBroadcast(i);


        try {
        	
        	hasAlreadyCallBack = false;
        	
            mMediaPlayer = new MediaPlayer();
            mMediaPlayer.setOnPreparedListener(mPreparedListener);
            mMediaPlayer.setOnVideoSizeChangedListener(mSizeChangedListener);
            mIsPrepared = false;
            Log.v(TAG, "reset duration to -1 in openVideo");
            mDuration = -1;
            mMediaPlayer.setOnCompletionListener(mCompletionListener);
            mMediaPlayer.setOnErrorListener(mErrorListener);
            mMediaPlayer.setOnBufferingUpdateListener(mBufferingUpdateListener);
            mCurrentBufferPercentage = 0;
           
             fileDescriptor = mContext.getAssets().openFd(
            		mUri);
//             try {
//                 String command = "chmod 777"+ mContext.getFilesDir()+mUri;
//                 Log.e("zhancheng","command = "+command);
//             	Runtime runtime = Runtime.getRuntime();
//             	java.lang.Process proc = runtime.exec(command);
//             } catch (IOException e) {
//             	e.printStackTrace();
//             }
            mMediaPlayer.setDataSource(fileDescriptor.getFileDescriptor(),
            		
            fileDescriptor.getStartOffset(),
                            
            fileDescriptor.getLength());
            mMediaPlayer.setDisplay(mSurfaceHolder);
            mMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            mMediaPlayer.setScreenOnWhilePlaying(true);
            mStartWhenPrepared = true;
            if(mVideoHeight!=0){
                getHolder().setFixedSize(mVideoWidth, mVideoHeight);

            }
 
            //mMediaPalyer.prepareAync 是异步立即播放
            //prepare 是缓冲完之后播放
            mIsPlaying = true;
            mMediaPlayer.prepare();
      //      attachMediaController();
            Log.e("zhancheng","openvideo  finish"+fileDescriptor.getFileDescriptor().toString());
        } catch (IOException ex) {
            Log.w(TAG, "Unable to open content: " + mUri, ex);
            return;
        } catch (IllegalArgumentException ex) {
            Log.w(TAG, "Unable to open content: " + mUri, ex);
            return;  
        }
    }

    public void setMediaController(MediaController controller) {
        if (mMediaController != null) {
            mMediaController.hide();
        }
//        mMediaController = controller;
//        attachMediaController();
    }

    private void attachMediaController() {
        if (mMediaPlayer != null && mMediaController != null) {
            mMediaController.setMediaPlayer(this);
            View anchorView = this.getParent() instanceof View ?
                    (View)this.getParent() : this;
            mMediaController.setAnchorView(anchorView);
            
            Log.e("zhancheng","attachMediaController  finish");
            mMediaController.setEnabled(mIsPrepared);
        }
    }

    MediaPlayer.OnVideoSizeChangedListener mSizeChangedListener =
        new MediaPlayer.OnVideoSizeChangedListener() {
            public void onVideoSizeChanged(MediaPlayer mp, int width, int height) {
            	
                Log.i("@@@@", "video size: " + mVideoWidth +"/"+ mVideoHeight);
                Log.i("@@@@", "surface size: " + mSurfaceWidth +"/"+ mSurfaceHeight);

                mVideoWidth = mp.getVideoWidth();
                mVideoHeight = mp.getVideoHeight();
                
                if(mMyChangeLinstener!=null){
                	mMyChangeLinstener.doMyThings();
                }
                
                if (mVideoWidth != 0 && mVideoHeight != 0) {
                    getHolder().setFixedSize(mVideoWidth, mVideoHeight);
                }
            }
    };

    MediaPlayer.OnPreparedListener mPreparedListener = new MediaPlayer.OnPreparedListener() {
        public void onPrepared(MediaPlayer mp) {
            // briefly show the mediacontroller
            mIsPrepared = true;

            if (mOnPreparedListener != null) {
                mOnPreparedListener.onPrepared(mMediaPlayer);
            }

            if (mMediaController != null) {
                mMediaController.setEnabled(true);
            }
            mVideoWidth = mp.getVideoWidth();
            mVideoHeight = mp.getVideoHeight();
            Log.i("@@@@", "video size: " + mVideoWidth +"/"+ mVideoHeight);
            Log.i("@@@@", "surface size: " + mSurfaceWidth +"/"+ mSurfaceHeight);

            if (mVideoWidth != 0 && mVideoHeight != 0) {     
            	
                Log.i("@@@@", "video size: " + mVideoWidth +"/"+ mVideoHeight);
                Log.i("@@@@", "surface size: " + mSurfaceWidth +"/"+ mSurfaceHeight);

     
                    // We didn't actually change the size (it was already at the size
                    // we need), so we won't get a "surface changed" callback, so
                    // start the video here instead of in the callback.
                    if (mSeekWhenPrepared != 0) {
                        mMediaPlayer.seekTo(mSeekWhenPrepared);
                        mSeekWhenPrepared = 0;
                    }
                    if (mStartWhenPrepared) {
                		if (((AppController)mContext).cur_posttion>0) {
                			mSeekWhenPrepared = ((AppController)mContext).cur_posttion;
                		}
                		mMediaPlayer.seekTo(mSeekWhenPrepared);

                        mMediaPlayer.start();
                        Log.e("zhancheng","start");
                        mStartWhenPrepared = false;
                        if (mMediaController != null) {
                        	 Log.e("zhancheng","show");
                            mMediaController.show();
                        }
                    } else if (!isPlaying() &&
                            (mSeekWhenPrepared != 0 || getCurrentPosition() > 0)) {
                       if (mMediaController != null) {
                    	   Log.e("zhancheng","show 0 ");
                           // Show the media controls when we're paused into a video and make 'em stick.
                           mMediaController.show(0);
                       }
                   }
             } else {
            	 
//            	 mMediaPlayer.setOnCompletionListener(null); //不让回调成功
            	 
            	 if(hasAlreadyCallBack)
            		 return;
            	 
             	if (mMediaController != null) {
                     mMediaController.hide();
                 }
                 if (mOnCompletionListener != null) {
                     mOnCompletionListener.onCompletion(mMediaPlayer);
                 }
         		((AppController)mContext).cur_posttion = 0;
         		
         		
//         		stopPlayback();

                if (mMediaPlayer != null) {
//                    mMediaPlayer.stop();                //没有开始，不需要关闭
//                    mMediaPlayer.release();			//
                    mMediaPlayer = null;
                }
            
         		
         		
                callBackLua();
                
            	//Log.e("zhancheng"," We don't know the video size yet, but should start anyway.");
                // We don't know the video size yet, but should start anyway.
                // The video size might be reported to us later.
//                if (mSeekWhenPrepared != 0) {
//                 	Log.e("zhancheng"," We don't know the video size yet, but should start anyway.  seekTo");
//                    mMediaPlayer.seekTo(mSeekWhenPrepared);
//                    mSeekWhenPrepared = 0;
//                }
//                if (mStartWhenPrepared) {
//                	Log.e("zhancheng"," We don't know the video size yet, but should start anyway.  start");
//            		if (((Sanguo)mContext).cur_posttion>0) {
//            			mSeekWhenPrepared = ((Sanguo)mContext).cur_posttion;
//            		}
//            		mMediaPlayer.seekTo(mSeekWhenPrepared);
//
//                    mMediaPlayer.start();
//                    mStartWhenPrepared = false;
//                }
                
                
                
            }
        }
    };
    
    
    public void stopPlayback() {
        if (mMediaPlayer != null) {
            mMediaPlayer.stop();
            mMediaPlayer.release();
            mMediaPlayer = null;
        }
    }

    private OnCompletionListener mCompletionListener =
        new OnCompletionListener() {
        public void onCompletion(MediaPlayer mp) {  
        	
        	if(hasAlreadyCallBack)//防止重复回调
        		return;
        	

        	if (mMediaController != null) {
                mMediaController.hide();
            }
            if (mOnCompletionListener != null) {
                mOnCompletionListener.onCompletion(mMediaPlayer);
            }
    		((AppController)mContext).cur_posttion = 0;
    		stopPlayback();
    		
    		callBackLua();
    		

        }
    };
    
    
    private void callBackLua(){
    	
    	Log.e("zhancheng","stop!!   luaOnFinishCallback = "+luaOnFinishCallback);
		
		((AppController)mContext).runOnUiThread(new Runnable() {
			@Override
			public void run() {
				ViewGroup group = (ViewGroup) ((AppController)mContext).getWindow()
						.getDecorView();
				group.removeView(VideoView.this);
			}
		});
		if (luaOnFinishCallback != 0) {
			Log.e("zhancheng","luaOnFinishCallback lua!!");
			((AppController)mContext).runOnGLThread(new Runnable() {
				@Override
				public void run() {
		   			Log.e("zhancheng","run lua!!");

					doLuaFinishCallback();
				}
			});
		}
    	mIsPlaying = false;
    	hasAlreadyCallBack = true;
    }
    


    private OnErrorListener mErrorListener =
        new OnErrorListener() {
        public boolean onError(MediaPlayer mp, int framework_err, int impl_err) {
        	
            Log.d(TAG, "Error: " + framework_err + "," + impl_err);
            if (mMediaController != null) {
                mMediaController.hide();
            }

            /* If an error handler has been supplied, use it and finish. */
            if (mOnErrorListener != null) {
                if (mOnErrorListener.onError(mMediaPlayer, framework_err, impl_err)) {
                    return true;
                }
            }

            /* Otherwise, pop up an error dialog so the user knows that
             * something bad has happened. Only try and pop up the dialog
             * if we're attached to a window. When we're going away and no
             * longer have a window, don't bother showing the user an error.
             */
            if (getWindowToken() != null) {
                Resources r = mContext.getResources();
                int messageId;

/*                if (framework_err == MediaPlayer.MEDIA_ERROR_NOT_VALID_FOR_PROGRESSIVE_PLAYBACK) {
                    messageId = com.android.internal.R.string.VideoView_error_text_invalid_progressive_playback;
                } else {
                    messageId = com.android.internal.R.string.VideoView_error_text_unknown;
                }

                new AlertDialog.Builder(mContext)
                        .setTitle(com.android.internal.R.string.VideoView_error_title)
                        .setMessage(messageId)
                        .setPositiveButton(com.android.internal.R.string.VideoView_error_button,
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int whichButton) {
                                         If we get here, there is no onError listener, so
                                         * at least inform them that the video is over.
                                         
                                        if (mOnCompletionListener != null) {
                                            mOnCompletionListener.onCompletion(mMediaPlayer);
                                        }
                                    }
                                })
                        .setCancelable(false)
                        .show();*/
            }
            
            if(hasAlreadyCallBack)
            	return true;
            
            
         	if (mMediaController != null) {
                 mMediaController.hide();
             }
             if (mOnCompletionListener != null) {
                 mOnCompletionListener.onCompletion(mMediaPlayer);
             }
     		((AppController)mContext).cur_posttion = 0;
     		
     		
            if (mMediaPlayer != null) {
                mMediaPlayer.stop();                
                mMediaPlayer.release();			
                mMediaPlayer = null;
            }
        
            callBackLua();

         	
            return true;
        }
    };

    private MediaPlayer.OnBufferingUpdateListener mBufferingUpdateListener =
        new MediaPlayer.OnBufferingUpdateListener() {
        public void onBufferingUpdate(MediaPlayer mp, int percent) {
            mCurrentBufferPercentage = percent;
        }
    };

    /**
     * Register a callback to be invoked when the media file
     * is loaded and ready to go.
     *
     * @param l The callback that will be run
     */
    public void setOnPreparedListener(MediaPlayer.OnPreparedListener l)
    {
        mOnPreparedListener = l;
    }

    /**
     * Register a callback to be invoked when the end of a media file
     * has been reached during playback.
     *
     * @param l The callback that will be run
     */
    public void setOnCompletionListener(OnCompletionListener l)
    {
        mOnCompletionListener = l;
    }

    /**
     * Register a callback to be invoked when an error occurs
     * during playback or setup.  If no listener is specified,
     * or if the listener returned false, VideoView will inform
     * the user of any errors.
     *
     * @param l The callback that will be run
     */
    public void setOnErrorListener(OnErrorListener l)
    {
        mOnErrorListener = l;
    }
    public boolean surfaceCreated;
    SurfaceHolder.Callback mSHCallback = new SurfaceHolder.Callback()
    {
       

		public void surfaceChanged(SurfaceHolder holder, int format,
                                    int w, int h)
        {
            mSurfaceWidth = w;
            mSurfaceHeight = h;

//            Log.e("zhancheng","issurfaceChange~~~~"+mSurfaceWidth);
//            if (mMediaPlayer != null && mIsPrepared && mVideoWidth == w && mVideoHeight == h) {
//         		if (((Sanguo)mContext).cur_posttion>0) {
//        			mSeekWhenPrepared = ((Sanguo)mContext).cur_posttion;
//        		}
//                if (mSeekWhenPrepared != 0) {
//                    Log.e("zhancheng","mMediaPlayer.seekTo();~~~~");
//                               
//                    mMediaPlayer.seekTo(mSeekWhenPrepared);
//                    mSeekWhenPrepared = 0;
//                }
//                Log.e("zhancheng","mMediaPlayer.start();~~~~");
//                mMediaPlayer.start();
//                Log.e("zhancheng","mMediaPlayer.start() 22 2;~~~~");
//
//                if (mMediaController != null) {
//                    Log.e("zhancheng","mMediaController != null;~~~~");
//
//                    mMediaController.show();
//                    Log.e("zhancheng","mMediaController.show();~~~~");
//
//                }
//            }
        }

        public void surfaceCreated(SurfaceHolder holder)
        {

        	surfaceCreated = true;

            mSurfaceHolder = holder;
            Log.e("zhancheng","surfaceCreated");
            openVideo();
            
         }

        public void surfaceDestroyed(SurfaceHolder holder)
        {
            // after we return from this we can't use the surface any more
        	surfaceCreated = false;
        	mIsPrepared = false;
   //         mSurfaceHolder = null;
  //          if (mMediaController != null) mMediaController.hide();
            if (mMediaPlayer != null) {
        			((AppController)mContext).cur_posttion = mMediaPlayer.getCurrentPosition();
        			Log.e("zhancheng", "cur_posttion = "+((AppController)mContext).cur_posttion);
        		
        			stopPlayback();
            }
    		if (fileDescriptor != null) {
    			try {
    				fileDescriptor.close();
    			} catch (IOException e) {
    				e.printStackTrace();
    			}
    			fileDescriptor = null;
    		}

        }
    };

	private int luaOnFinishCallback;

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        if (mIsPrepared && mMediaPlayer != null && mMediaController != null) {
            toggleMediaControlsVisiblity();
        }
        return false;
    }

    @Override
    public boolean onTrackballEvent(MotionEvent ev) {
        if (mIsPrepared && mMediaPlayer != null && mMediaController != null) {
            toggleMediaControlsVisiblity();
        }
        return false;
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        if (mIsPrepared &&
                keyCode != KeyEvent.KEYCODE_BACK &&
                keyCode != KeyEvent.KEYCODE_VOLUME_UP &&
                keyCode != KeyEvent.KEYCODE_VOLUME_DOWN &&
                keyCode != KeyEvent.KEYCODE_MENU &&
                keyCode != KeyEvent.KEYCODE_CALL &&
                keyCode != KeyEvent.KEYCODE_ENDCALL &&
                mMediaPlayer != null &&
                mMediaController != null) {
            if (keyCode == KeyEvent.KEYCODE_HEADSETHOOK ||
                    keyCode == KeyEvent.KEYCODE_MEDIA_PLAY_PAUSE) {
                if (mMediaPlayer.isPlaying()) {
                    pause();
                    mMediaController.show();
                } else {
                    start();
                    mMediaController.hide();
                }
                return true;
            } else if (keyCode == KeyEvent.KEYCODE_MEDIA_STOP
                    && mMediaPlayer.isPlaying()) {
                pause();
                mMediaController.show();
            } else {
                toggleMediaControlsVisiblity();
            }
        }

        return false;
    }

    private void toggleMediaControlsVisiblity() {
        if (mMediaController.isShowing()) {
            mMediaController.hide();
        } else {
            mMediaController.show();
        }
    }

    public void start() {
        if (mMediaPlayer != null && mIsPrepared) {
                mMediaPlayer.start();
                mStartWhenPrepared = false;
        } else {
            mStartWhenPrepared = true;
        }
    }
    
    public void playVideo(){
//    	  VideoView view =new VideoView(this);  
//          //设置显示视频View  
//          setContentView(view);  
//          //注册监听视频  
//          view.setOnCompletionListener(this);  
//          //设置视频文件路径  
//          view.setVideoURI("avc_ac.mp4");
//          //播放视频  
//          view.start();  
//    	
    }

    public void pause() {
        if (mMediaPlayer != null && mIsPrepared) {
            if (mMediaPlayer.isPlaying()) {
                mMediaPlayer.pause();
            }
        }
        mStartWhenPrepared = false;
    }

    public int getDuration() {
        if (mMediaPlayer != null && mIsPrepared) {
            if (mDuration > 0) {
                return mDuration;
            }
            mDuration = mMediaPlayer.getDuration();
            return mDuration;
        }
        mDuration = -1;
        return mDuration;
    }

    public int getCurrentPosition() {
        if (mMediaPlayer != null && mIsPrepared) {
            return mMediaPlayer.getCurrentPosition();
        }
        return 0;
    }

    public void seekTo(int msec) {
        if (mMediaPlayer != null && mIsPrepared) {
            mMediaPlayer.seekTo(msec);
        } else {
            mSeekWhenPrepared = msec;
        }
    }

    public boolean isPlaying() {
        if (mMediaPlayer != null && mIsPrepared) {
            return mMediaPlayer.isPlaying();
        }
        return false;
    }

    public int getBufferPercentage() {
        if (mMediaPlayer != null) {
            return mCurrentBufferPercentage;
        }
        return 0;
    }

	@Override
	public boolean canPause() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean canSeekBackward() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean canSeekForward() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int getAudioSessionId() {
		// TODO Auto-generated method stub
		return 0;
	}
	public void doLuaFinishCallback() {

		Cocos2dxLuaJavaBridge.callLuaFunctionWithString(luaOnFinishCallback,
				"");
		Cocos2dxLuaJavaBridge.releaseLuaFunction(luaOnFinishCallback);
		// TODO Auto-generated method stub

	}
	public void setLuaOnFinishCallback(int luaOnFinishCallback) {
		this.luaOnFinishCallback = luaOnFinishCallback;
	}

}
