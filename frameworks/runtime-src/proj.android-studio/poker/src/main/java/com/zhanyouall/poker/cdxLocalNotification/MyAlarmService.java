package com.zhanyouall.poker.cdxLocalNotification;

import java.util.Calendar;
import java.util.Date;

import com.zhanyouall.poker.AppController;
import com.zhanyouall.poker.R;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.widget.Toast;

public class MyAlarmService extends Service {
	public static final String TITLE = "ALARM_TITLE";
	public static final String SUBTITLE = "ALARM_SUBTITLE";
	public static final String TICKER_TEXT = "ALARM_TICKER";
	public static final String NOTIFICATION_ID = "NOTIFICATION_ID";
	public static final String TIMESTAMP = "TIMESTAMP";
	/* Contains time in 24hour format 'HH:mm' e.g. '04:30' or '18:23' */
	public static final String HOUR_OF_DAY = "HOUR_OF_DAY";
	public static final String MINUTE = "MINUTES";
	public static String intentTest = "just";

	@Override
	public void onCreate() {
		// // TODO Auto-generated method stub
		// Toast.makeText(this, "MyAlarmService.onCreate()", Toast.LENGTH_LONG)
		// .show();
	}

	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		// Toast.makeText(this, "MyAlarmService.onBind()", Toast.LENGTH_LONG)
		// .show();
		return null;
	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub

	}

	public void showNotificaiton(Intent intent) {

		final Bundle bundle = intent.getExtras();

		// Retrieve notification details from the intent
		final String tickerText = bundle.getString(TICKER_TEXT);
		final String notificationTitle = bundle.getString(TITLE);
		final String notificationSubText = bundle.getString(SUBTITLE);
		final String notificationId = bundle.getString(NOTIFICATION_ID);
		final Long timestamp = bundle.getLong(TIMESTAMP);
		Calendar currentCal = Calendar.getInstance();
		System.out.println("当前事件 " + currentCal.getTimeInMillis());
		System.out.println("闹钟事件 " + timestamp.toString());
		// if (currentCal.getTimeInMillis()>timestamp){
		// return;
		// }
		// if (currentHour>alarmHour) {
		/*
		 * If you set a repeating alarm at 11:00 in the morning and it should
		 * trigger every morning at 08:00 o'clock, it will immediately fire.
		 * E.g. Android tries to make up for the 'forgotten' reminder for that
		 * day. Therefore we ignore the event if Android tries to 'catch up'.
		 */
		// Log.d("LocalNotification AlarmReceiver",
		// "AlarmReceiver, ignoring alarm since it is due");
		// return;
		// }
		final NotificationManager mNotificationManager = (NotificationManager) getApplication()
				.getSystemService(Context.NOTIFICATION_SERVICE);
		final Intent notifyIntent = new Intent(getApplication(), AppController.class);
		notifyIntent.addFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT
				| Intent.FLAG_ACTIVITY_SINGLE_TOP);
		final int id = Integer.parseInt(notificationId);
		PendingIntent contentIntent = PendingIntent.getActivity(
				getApplication(), id, notifyIntent,
				PendingIntent.FLAG_ONE_SHOT);
		int icon = R.drawable.icon;

		CharSequence tickerText1 = tickerText;

		long when = System.currentTimeMillis();
		Date as = new Date(when);

		// 通过Notification.Builder来创建通知，注意API Level
		// API11之后才支持
		Notification notify = new NotificationCompat.Builder(getApplication())
				.setSmallIcon(R.drawable.icon) // 设置状态栏中的小图片，尺寸一般建议在24×24，这个图片同样也是在下拉状态栏中所显示，如果在那里需要更换更大的图片，可以使用setLargeIcon(Bitmap
				// icon)
				.setTicker(tickerText)// 设置在status
				// bar上显示的提示文字
				.setContentTitle(notificationTitle)// 设置在下拉status
				// bar后Activity，本例子中的NotififyMessage的TextView中显示的标题
				.setContentText(notificationSubText)// TextView中显示的详细内容

				.setContentIntent(contentIntent) // 关联PendingIntent
//                .setNumber(1) // 在TextView的右方显示的数字，可放大图片看，在最右侧。这个number同时也起到一个序列号的左右，如果多个触发多个通知（同一ID），可以指定显示哪一个。
				.setSound(Uri.parse("android.resource://"
						+ getApplication().getPackageName() + "/"
						+ R.raw.notificationaudio))
				.setWhen(when)
				.build(); // 需要注意build()是在API level
		// 16及之后增加的，在API11中可以使用getNotificatin()来代替

		notify.flags |= Notification.FLAG_AUTO_CANCEL;
		mNotificationManager.notify(id, notify);
	}

	@Override
	public void onStart(Intent intent, int startId) {
		// TODO Auto-generated method stub

		super.onStart(intent, startId);

	}

	@Override
	public boolean onUnbind(Intent intent) {
		// TODO Auto-generated method stub
		Toast.makeText(this, "MyAlarmService.onUnbind()", Toast.LENGTH_LONG)
				.show();
		return super.onUnbind(intent);
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		// TODO Auto-generated method stub
		boolean mStartCompatibility = getApplicationInfo().targetSdkVersion < Build.VERSION_CODES.ECLAIR;
		try {
			if (intent != null) {
				showNotificaiton(intent);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return mStartCompatibility ? START_STICKY_COMPATIBILITY : START_STICKY;

	}

}