package com.zhanyouall.poker.com.wxapi;

import java.io.ByteArrayOutputStream;
import java.io.File;

import com.tencent.mm.sdk.modelmsg.SendMessageToWX;
import com.tencent.mm.sdk.modelmsg.WXImageObject;
import com.tencent.mm.sdk.modelmsg.WXMediaMessage;
import com.tencent.mm.sdk.modelmsg.WXTextObject;
import com.tencent.mm.sdk.openapi.IWXAPI;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.Log;
import android.widget.Toast;

public class WechatShareManager {
	public static WechatShareManager instance;
	private static Object INSTANCE_LOCK = new Object();
	private WechatShareManager(){
		
	}
	public static WechatShareManager getInstance(){
		if (instance == null) {
			synchronized (INSTANCE_LOCK) {
				if (instance == null) {
					instance = new WechatShareManager();
				}
			}
		}
		return instance;
	}
	public void shareTextToWechat(final IWXAPI api, final String text) {
		
		
		WXTextObject wxTextObject = new WXTextObject();
		wxTextObject.text = text;
		
		WXMediaMessage msg = new WXMediaMessage();
		msg.mediaObject = wxTextObject;
		
		SendMessageToWX.Req req = new SendMessageToWX.Req();
		req.transaction = buildTransaction("text");
		req.message = msg;
		req.scene = SendMessageToWX.Req.WXSceneTimeline;
		api.sendReq(req);
	}
	
	public void shareImageToWechat(final Activity context,final IWXAPI api, final String imagePath) {
		Log.d("zhancheng","path: " + imagePath);
		File file = new File(imagePath);
		if(!file.exists()){
			Toast.makeText(context, "图片不存在", Toast.LENGTH_SHORT).show();
			return;
		}
		Bitmap bmp = drawBg4Bitmap(0xffffffff, BitmapFactory.decodeFile(imagePath));
		WXImageObject imageObject = new WXImageObject(bmp);
		
		WXMediaMessage msg = new WXMediaMessage();
		msg.mediaObject = imageObject;
		
//		Bitmap thumbBmp = Bitmap.createScaledBitmap(bmp, 150, 150, true);
//		bmp.recycle();
//		msg.thumbData = bmpToByteArray(thumbBmp, true);
		
		SendMessageToWX.Req req = new SendMessageToWX.Req();
		req.transaction = buildTransaction("img");
		req.message = msg;
		req.scene = SendMessageToWX.Req.WXSceneTimeline;
		api.sendReq(req);
		
	}
	private  Bitmap drawBg4Bitmap(int color, Bitmap orginBitmap) {  
        Paint paint = new Paint();  
        paint.setColor(color);  
        Bitmap bitmap = Bitmap.createBitmap(orginBitmap.getWidth(),  
                orginBitmap.getHeight(), orginBitmap.getConfig());  
        Canvas canvas = new Canvas(bitmap);  
        canvas.drawRect(0, 0, orginBitmap.getWidth(), orginBitmap.getHeight(), paint);  
        canvas.drawBitmap(orginBitmap, 0, 0, paint);  
        return bitmap;  
    }  
	private   byte[] bmpToByteArray(final Bitmap bmp, final boolean needRecycle) {
		ByteArrayOutputStream output = new ByteArrayOutputStream();
		bmp.compress(CompressFormat.JPEG, 100, output);
		if (needRecycle) {
			bmp.recycle();
		}
		
		byte[] result = output.toByteArray();
		try {
			output.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return result;
	}
	private  String buildTransaction(final String type) {
		return (type == null) ? String.valueOf(System.currentTimeMillis()) : type + System.currentTimeMillis();
	}
}
