package com.zhanyouall.poker.com.wxapi;

import com.tencent.mm.sdk.modelbase.BaseReq;
import com.tencent.mm.sdk.modelbase.BaseResp;
import com.tencent.mm.sdk.openapi.IWXAPI;
import com.tencent.mm.sdk.openapi.IWXAPIEventHandler;
import com.tencent.mm.sdk.openapi.WXAPIFactory;
import com.zhanyouall.poker.AppController;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

public class WXEntryActivity extends Activity implements IWXAPIEventHandler{
	private IWXAPI api;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		api = WXAPIFactory.createWXAPI(this, "wxc0e602b3b8ee3dec", false);
		api.handleIntent(getIntent(), this);
	}
	@Override
	protected void onNewIntent(Intent intent) {
		super.onNewIntent(intent);
		setIntent(intent);
		api.handleIntent(intent, this);
	}
	@Override
	public void onReq(BaseReq req) {
		
	}

	@Override
	public void onResp(BaseResp resp) {
		switch (resp.errCode) {
			case BaseResp.ErrCode.ERR_OK:
				Toast.makeText(this, "分享成功", Toast.LENGTH_SHORT).show();
				AppController.sActivity.doCallbackByName("weixinSuccess", "1" + "," + AppController.sActivity.shareId);
				break;
	
			case BaseResp.ErrCode.ERR_USER_CANCEL:
				Toast.makeText(this, "分享取消", Toast.LENGTH_SHORT).show();
				AppController.sActivity.doCallbackByName("weixinSuccess", "0"+ "," + AppController.sActivity.shareId);
				break;
			case BaseResp.ErrCode.ERR_UNSUPPORT:
				Toast.makeText(this, "分享失败", Toast.LENGTH_SHORT).show();
				Log.e("zhancheng","error msg: " + resp.errStr);
				AppController.sActivity.doCallbackByName("weixinSuccess", "0"+ "," + AppController.sActivity.shareId);
				break;
			case BaseResp.ErrCode.ERR_SENT_FAILED:	
				Toast.makeText(this, "分享取失败", Toast.LENGTH_SHORT).show();
				Log.e("zhancheng","error msg: " + resp.errStr);
				AppController.sActivity.doCallbackByName("weixinSuccess", "0"+ "," + AppController.sActivity.shareId);
				break;
		}
		finish();
	}

}
