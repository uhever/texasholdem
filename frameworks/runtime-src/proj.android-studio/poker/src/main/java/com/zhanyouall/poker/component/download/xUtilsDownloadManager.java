package com.zhanyouall.poker.component.download;

import java.io.File;


import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.HttpHandler;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Environment;
import android.os.StatFs;
import android.preference.PreferenceManager;
import android.util.Log;



/**
 * 单例，提供下载东西的功能
 * @author zctech
 *
 */
public class xUtilsDownloadManager {
	
	public static final String DOWNLOAD_FOLDER ="/bdwsw/bdwsw_"; 
	public static final int DOWNLOAD_TYPE_APK = 1001;
	public static final int DOWNLOAD_TYPE_SCRIPTS = 1002;
	
	public static final int DOWNLOAD_STATE_LOADING = 1;
	public static final int DOWNLOAD_STATE_STOP = 2;
	public static final int DOWNLOAD_STATE_SUCCESS = 3;
//	public static final int DOWNLOAD_STATE_FAIL = 4;
	
	private HttpUtils http;
	private HttpHandler handler;
	
	private SharedPreferences sp;
	
	private IDownloadManagerFeedback iDownloadManagerFeedback;
	
	private String downloadUrl;
	private String downloadVersion;
	private String downloadFileName;
	private String savePath;
	
	private int state;
	
	private boolean isInDownloadScriptsTask = false;
	
	
	public boolean isInDownloadScriptsTask() {
		return isInDownloadScriptsTask;
	}

	public void setInDownloadScriptsTask(boolean isInDownloadScriptsTask) {
		this.isInDownloadScriptsTask = isInDownloadScriptsTask;
	}
	
	private boolean isCheckSpaceOk = false;
	
	private String externalPath;
	
	private static int downloadType = -1;
	
	private static xUtilsDownloadManager manager;
	
	private xUtilsDownloadResponseDTO feedbackDto;
	
	public static xUtilsDownloadManager getInstance(Context contex){
		if(manager == null)
			manager = new xUtilsDownloadManager(contex);
		return manager;
	}
	
	public xUtilsDownloadManager(Context contex) {
		http = new HttpUtils();
		sp = PreferenceManager.getDefaultSharedPreferences(contex);
		externalPath = Environment.getExternalStorageDirectory().getAbsolutePath();
		feedbackDto = new xUtilsDownloadResponseDTO();
	}
	
	public interface IDownloadManagerFeedback{
		public void feedback(xUtilsDownloadResponseDTO dto);
	}
	
	/**
	 * 下载游戏资源
	 * 当下载后的存放位置（解压目标路径fileName）或下载链接和之前存的不一样，则会重新下载
	 * @param url 下载链接
	 * @param fileName 解压目标路径（注意不是下载存放路径）
	 * @param callBack 下载响应回调
	 */
	public void downloadScripts(String url, String fileName,IDownloadManagerFeedback callBack){
		Log.e("zhancheng", "downloadScripts:"+url);
		downloadType = DOWNLOAD_TYPE_SCRIPTS;
		downloadUrl = url;
		downloadFileName = fileName;
		iDownloadManagerFeedback = callBack;
		savePath = externalPath + DOWNLOAD_FOLDER + "scripts_"+ fileName + ".zip";

		String saveFileName = sp.getString("downloadScriptsInfo_fileName", "error");
		String saveUrl = sp.getString("downloadScriptsInfo_url", "error");
		if(!saveFileName.equals(downloadFileName)||!saveUrl.equals(url)){
			//没有下载过或重新安装了，重新下载
			startupDownload();
		}else{
			state = sp.getInt("downloadScriptsInfo_state", -1);
			switch(state){
			case DOWNLOAD_STATE_LOADING:
				//继续下载
				continueDownload();
				break;
			case DOWNLOAD_STATE_STOP:
				//暂停
				stopDownload();
				break;
			case DOWNLOAD_STATE_SUCCESS:
				File checkFile = new File(savePath);
				if(!checkFile.exists()){
					sp.edit().putString("downloadScriptsInfo_fileName", "error").commit();
					sp.edit().putString("downloadScriptsInfo_url", "error").commit();
					downloadScripts(downloadUrl, downloadFileName, iDownloadManagerFeedback);
				}
				
				//成功，直接回调
				if(iDownloadManagerFeedback!=null){
					feedbackDto.setFlag(xUtilsDownloadResponseDTO.FLAG_SUCCESS);
					feedbackDto.setExtra(savePath);
					iDownloadManagerFeedback.feedback(feedbackDto);
				}
				break;
			default:
				Log.e("zhancheng", "downloadScripts state error:"+state);
				break;
			}
		}
	}
	
	/**
	 * 下载新apk，当下载版本号与之前保存的不同时则删除旧版本，下载新的
	 * @param url 下载链接
	 * @param version 需求的新版本
	 * @param callBack 下载响应回调
	 */
	public void downloadNewApk(String url, String version, IDownloadManagerFeedback callBack){
		Log.e("zhancheng", "downloadNewApk:"+url+" "+version);
		downloadType = DOWNLOAD_TYPE_APK;
		downloadUrl = url;
		downloadVersion = version;
		iDownloadManagerFeedback = callBack;
		savePath = externalPath + DOWNLOAD_FOLDER + "bdwsw_"+version+".apk";
		
		String saveVersion = sp.getString("downloadAPKInfo_version", "error");
		Log.e("zhancheng", "saveVersion:"+saveVersion);
		if(!saveVersion.equals(version)){
			//版本不同，重新下载
			startupDownload();
		}else{
			//版本相同
			state = sp.getInt("downloadAPKInfo_state", -1);
			switch(state){
			case DOWNLOAD_STATE_LOADING:
				//继续下载
				continueDownload();
				break;
			case DOWNLOAD_STATE_STOP:
				//暂停
				stopDownload();
				break;
			case DOWNLOAD_STATE_SUCCESS:
				File checkFile = new File(savePath);
				if(!checkFile.exists()){
					sp.edit().putString("downloadAPKInfo_version", "error").commit();
					downloadNewApk(downloadUrl, downloadFileName, iDownloadManagerFeedback);
				}
				
				
				//成功，直接回调
				if(iDownloadManagerFeedback!=null){
					feedbackDto.setFlag(xUtilsDownloadResponseDTO.FLAG_SUCCESS);
					feedbackDto.setExtra(savePath);
					iDownloadManagerFeedback.feedback(feedbackDto);
				}
				break;
			default:
				Log.e("zhancheng", "downloadNewApk state error:"+state);
				break;
			}
			
		}
			
		
	}
	
	private RequestCallBack<File> requestCallBack = new RequestCallBack<File>() {
		
		@Override
		public void onStart() {
			Log.e("zhancheng", "requestCallBack onStart");
			if(iDownloadManagerFeedback!=null){
				feedbackDto.setFlag(xUtilsDownloadResponseDTO.FLAG_START);
				iDownloadManagerFeedback.feedback(feedbackDto);
			}
				
		}

		@Override
		public void onLoading(long total, long current, boolean isUploading) {
//			Log.e("zhancheng", "requestCallBack onLoading total:"+ total+"current:"+current+" "+isUploading);
			if(!isCheckSpaceOk){
				if(getSDAvailableSize()<total){
					Log.e("zhancheng", "doDownload 空间不足");
					handler.cancel();
					if(iDownloadManagerFeedback!=null){
						feedbackDto.setFlag(xUtilsDownloadResponseDTO.FLAG_FAIL);
						feedbackDto.setMsg("空间不足！");
						iDownloadManagerFeedback.feedback(feedbackDto);
					}
					return;
				}else{
					isCheckSpaceOk = true;
				}
			}
			
			
			double percent = (double)current/(double)total;
			percent = percent*100;
			if(iDownloadManagerFeedback!=null){
				feedbackDto.setFlag(xUtilsDownloadResponseDTO.FLAG_LOADING);
				feedbackDto.setPercent(percent);
				feedbackDto.setExtra(total+"_"+current+"_"+isUploading);
				iDownloadManagerFeedback.feedback(feedbackDto);
			}
			
			

		}

		@Override
		public void onSuccess(ResponseInfo<File> responseInfo) {
			Log.e("zhancheng", "requestCallBack onSuccess");
										
			switch(downloadType){
			case DOWNLOAD_TYPE_APK:
				sp.edit().putInt("downloadAPKInfo_state", DOWNLOAD_STATE_SUCCESS).commit();
				break;
			case DOWNLOAD_TYPE_SCRIPTS:
				sp.edit().putInt("downloadScriptsInfo_state", DOWNLOAD_STATE_SUCCESS).commit();
				break;
			default:
				Log.e("zhancheng", "RequestCallBack onSuccess downloadType error");
				break;
				
			}
			state = DOWNLOAD_STATE_SUCCESS;
			if(iDownloadManagerFeedback!=null){
				feedbackDto.setFlag(xUtilsDownloadResponseDTO.FLAG_SUCCESS);
				feedbackDto.setExtra(savePath);
				iDownloadManagerFeedback.feedback(feedbackDto);
			}
			
		}
		
		@Override
		public void onFailure(HttpException error, String msg) {
			Log.e("zhancheng", "requestCallBack onFailure msg:"+msg);
			if(iDownloadManagerFeedback!=null){
				feedbackDto.setFlag(xUtilsDownloadResponseDTO.FLAG_FAIL);
				feedbackDto.setMsg(msg);
				iDownloadManagerFeedback.feedback(feedbackDto);
			}
//			state = DOWNLOAD_STATE_FAIL;
			if(handler!=null)
				handler.cancel();
		}
		
		@Override
		public void onCancelled() {
			Log.e("zhancheng", "requestCallBack onCancelled");
			if(iDownloadManagerFeedback!=null){
				feedbackDto.setFlag(xUtilsDownloadResponseDTO.FLAG_CANCEL);
				iDownloadManagerFeedback.feedback(feedbackDto);
			}
		}
	};
	
	
	
	public void doDownload(){
		
		Log.e("zhancheng", "doDownload !!!!!!!");
		
		switch(state){
		case DOWNLOAD_STATE_LOADING:

			handler = http.download(downloadUrl,
					savePath,
				    true, // 如果目标文件存在，接着未完成的部分继续下载。服务器不支持RANGE时将从新下载。
				    false, // 如果从请求返回信息中获取到文件名，下载完成后自动重命名。
				    requestCallBack);
			
			break;
		case DOWNLOAD_STATE_STOP:
			if(iDownloadManagerFeedback!=null){
				feedbackDto.setFlag(xUtilsDownloadResponseDTO.FLAG_STOP);
				iDownloadManagerFeedback.feedback(feedbackDto);
			}
			break;
		case DOWNLOAD_STATE_SUCCESS:
			if(iDownloadManagerFeedback!=null){
				feedbackDto.setFlag(xUtilsDownloadResponseDTO.FLAG_STOP);
				iDownloadManagerFeedback.feedback(feedbackDto);
			}
			break;
		default:
			break;
		}
		
		
	}
	
	public void continueDownload(){
		Log.e("zhancheng", "continueDownload");
		switch(downloadType){
		case DOWNLOAD_TYPE_APK:
			sp.edit().putInt("downloadAPKInfo_state", DOWNLOAD_STATE_LOADING).commit();
			break;
		case DOWNLOAD_TYPE_SCRIPTS:
			sp.edit().putInt("downloadScriptsInfo_state", DOWNLOAD_STATE_LOADING).commit();
			break;
		default:
			Log.e("zhancheng", "continueDownload error");
			break;
			
		}
		
		state = DOWNLOAD_STATE_LOADING;
		doDownload();
		
		
	}
	
//	public void retryDownload(){
//		if(state==DOWNLOAD_STATE_FAIL)
//			continueDownload();
//	}

	
	public void startupDownload(){
		Log.e("zhancheng", "reTryDownload");
		if(handler!=null)
			handler.cancel();
		switch(downloadType){
		case DOWNLOAD_TYPE_APK:
			String saveVersion = sp.getString("downloadAPKInfo_version", "error");
			File f = new File(externalPath + DOWNLOAD_FOLDER + saveVersion+".apk");
			if(f.exists())
				f.delete();
			File f2 = new File(savePath);
			if(f2.exists())
				f2.delete();
			sp.edit().putString("downloadAPKInfo_version", downloadVersion).commit();
			sp.edit().putString("downloadAPKInfo_url", downloadUrl).commit();
			sp.edit().putInt("downloadAPKInfo_state", DOWNLOAD_STATE_LOADING).commit();			
			break;
		case DOWNLOAD_TYPE_SCRIPTS:
			String saveName = sp.getString("downloadScriptsInfo_fileName", "error");
			File f3 = new File(externalPath + DOWNLOAD_FOLDER + saveName);
			if(f3.exists())
				f3.delete();
			File f4 = new File(savePath);
			if(f4.exists())
				f4.delete();
			sp.edit().putString("downloadScriptsInfo_fileName", downloadFileName).commit();
			sp.edit().putString("downloadScriptsInfo_url", downloadUrl).commit();
			sp.edit().putInt("downloadScriptsInfo_state", DOWNLOAD_STATE_LOADING).commit();			
					
			break;
		default:
			Log.e("zhancheng", "reTryDownload error");
			break;
			
		}
		state = DOWNLOAD_STATE_LOADING;
		doDownload();
		
	}
	
	public void stopDownload(){
		Log.e("zhancheng", "stopDownload");
		switch(downloadType){
		case DOWNLOAD_TYPE_APK:
			sp.edit().putInt("downloadAPKInfo_state", DOWNLOAD_STATE_STOP).commit();			
			break;
		case DOWNLOAD_TYPE_SCRIPTS:
			sp.edit().putInt("downloadScriptsInfo_state", DOWNLOAD_STATE_STOP).commit();
			break;
		default:
			Log.e("zhancheng", "stopDownload error");
			break;
			
		}
		
		state = DOWNLOAD_STATE_STOP;
		if(iDownloadManagerFeedback!=null){
			feedbackDto.setFlag(xUtilsDownloadResponseDTO.FLAG_STOP);
			iDownloadManagerFeedback.feedback(feedbackDto);
		}
		if(handler!=null)
			handler.cancel();
	}
	
	
	public void quitManager(){
		if(handler!=null)
			handler.cancel();
	}
	
	/** 
     * 计算SD卡的剩余空间 
     * @return 剩余空间 
     */  
    public static long getSDAvailableSize()  
    {  
        if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED))  
        {  
            return getAvailableSize(Environment.getExternalStorageDirectory().toString());  
        }  
          
        return 0;  
    } 
    
    private static long getAvailableSize(String path)  
    {  
        StatFs fileStats = new StatFs(path);  
        fileStats.restat(path);  
        return (long) fileStats.getAvailableBlocks() * fileStats.getBlockSize(); // 注意与fileStats.getFreeBlocks()的区别  
    } 
	

}
