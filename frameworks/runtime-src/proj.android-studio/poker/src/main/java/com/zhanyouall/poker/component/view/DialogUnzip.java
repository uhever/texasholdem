package com.zhanyouall.poker.component.view;




import java.util.Timer;
import java.util.TimerTask;

import com.zhanyouall.poker.AppController;
import com.zhanyouall.poker.R;

import android.app.Dialog;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

/**
 * 转圈等待的Dialog
 * @author zctech
 *
 */
public class DialogUnzip extends Dialog {
	
    private int id; 
    
    private boolean isStop;
    
    private TextView tv; 
    
    private Button btCtrl, btReset;
       
    private DialogUnzip dialogUnzip;

    private AppController sActivity;

	public DialogUnzip(Context context, int id) {
		super(context, R.style.zc_dialog_normal);// 创建自定义样式dialog  
		LayoutInflater inflater = LayoutInflater.from(context);  
        View v = inflater.inflate(R.layout.zc_dialog_unzip_contentview, null);// 得到加载view  
        RelativeLayout layout = (RelativeLayout) v.findViewById(R.id.zc_dialog_unzip_view);// 加载布局 
        
        tv = (TextView)v.findViewById(R.id.zc_dialog_unzip_tv);     
        btCtrl = (Button)v.findViewById(R.id.zc_dialog_unzip_btctrl);
        btReset = (Button)v.findViewById(R.id.zc_dialog_unzip_btrestart);
                    	  
        isStop = false;
        this.setCancelable(false);// 不可以用“返回键”取消  
        this.setContentView(layout, new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.FILL_PARENT, RelativeLayout.LayoutParams.FILL_PARENT));// 设置布局  
        dialogUnzip = this;
        this.id = id;
        sActivity = (AppController)context;
        Log.e("zhancheng", "onCreateDialog DialogUnzip");
        
	}
	
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Boolean getIsStop() {
		return isStop;
	}

	public void setIsStop(Boolean isStop) {
		this.isStop = isStop;
	}

	public Button getBtCtrl() {
		return btCtrl;
	}

	public void setBtCtrl(Button btCtrl) {
		this.btCtrl = btCtrl;
	}

	public Button getBtReset() {
		return btReset;
	}

	public void setBtReset(Button btReset) {
		this.btReset = btReset;
	}

	public TextView getTv() {
		return tv;
	}

	public void setTv(TextView tv) {
		this.tv = tv;
	}


}
