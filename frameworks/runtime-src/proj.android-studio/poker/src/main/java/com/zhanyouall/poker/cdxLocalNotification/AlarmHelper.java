package com.zhanyouall.poker.cdxLocalNotification;

import java.util.Calendar;
import java.util.Date;
import java.util.Map;

import java.util.Set;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import android.os.Bundle;
import android.widget.Toast;

public class AlarmHelper {

	private Context ctx;

	public AlarmHelper(Context context) {
		this.ctx = context;
	}

	public boolean addAlarm(int ClockType, String alarmTitle,
			String alarmSubTitle, String alarmTicker, String notificationId,
			Calendar cal) {

		final long triggerTime = cal.getTimeInMillis();

		final Intent intent = new Intent(this.ctx, MyAlarmService.class);

		intent.setAction(notificationId);
		Bundle bundle = new Bundle();
		bundle.putString(MyAlarmService.TITLE, alarmTitle);
		bundle.putString(MyAlarmService.SUBTITLE, alarmSubTitle);
		bundle.putString(MyAlarmService.TICKER_TEXT, alarmTicker);
		bundle.putString(MyAlarmService.NOTIFICATION_ID, notificationId);
		bundle.putLong(MyAlarmService.TIMESTAMP, cal.getTimeInMillis());
		// bundle.putInt(MyAlarmService.HOUR_OF_DAY,cal.HOUR_OF_DAY);
		// bundle.putInt(MyAlarmService.MINUTE,cal.MINUTE);
		intent.putExtras(bundle);
		int id = Integer.parseInt(notificationId);
		final PendingIntent sender = PendingIntent.getService(this.ctx, id,
				intent, PendingIntent.FLAG_UPDATE_CURRENT);

		final AlarmManager am = getAlarmManager();

		if (ClockType == 1) {

			am.setRepeating(AlarmManager.RTC_WAKEUP, triggerTime,
					AlarmManager.INTERVAL_DAY, sender);
		} else if (ClockType == 2) {

			am.setRepeating(AlarmManager.RTC_WAKEUP, triggerTime,
					AlarmManager.INTERVAL_DAY * 7, sender);
		} else {

			am.set(AlarmManager.RTC_WAKEUP, triggerTime, sender);
		}

		return true;
	}

	/**
	 * @see LocalNotification#cancelNotification(int)
	 */
	public boolean cancelAlarm(String id) {
		return cancelAlarmReal(id);
	}

	public boolean cancelAlarmReal(String Id) {

		final Intent intent = new Intent(this.ctx, MyAlarmService.class);
		intent.setAction(Id);

		final PendingIntent pi = PendingIntent
				.getService(this.ctx, Integer.parseInt(Id), intent,
						PendingIntent.FLAG_UPDATE_CURRENT);
		final AlarmManager am = getAlarmManager();

		try {
			am.cancel(pi);
		} catch (Exception e) {
			return false;
		}

		return true;
	}

	/**
	 * @see LocalNotification#cancelAllNotifications()
	 */
	public boolean cancelAll(SharedPreferences alarmSettings) {
		final Map<String, ?> allAlarms = alarmSettings.getAll();
		final Set<String> alarmIds = allAlarms.keySet();

		for (String alarmId : alarmIds) {

			cancelAlarm(alarmId);
		}

		return true;
	}

	private AlarmManager getAlarmManager() {
		final AlarmManager am = (AlarmManager) this.ctx
				.getSystemService(Context.ALARM_SERVICE);

		return am;
	}
}
