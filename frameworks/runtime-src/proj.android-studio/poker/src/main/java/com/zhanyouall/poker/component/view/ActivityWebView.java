package com.zhanyouall.poker.component.view;


import com.zhanyouall.poker.R;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

@SuppressLint("JavascriptInterface")
public class ActivityWebView extends Activity{
	
	private static ActivityWebView sActivityWebView;
	
	private WebView webview;

	private static Dialog progressDialog;
	
	private static final int MY_DIALOG = 1000; 
	
	private ImageView spaceshipImage;
	
	private Animation hyperspaceJumpAnimation;
	
	private static final int DIALOG_SHOW = 1001;
	
	private static final int DIALOG_DISMISS = 1002;
	
	private static final int ACTIVITY_QUIT = 2000;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		sActivityWebView = this;
		
		Intent in = getIntent();
		String url = in.getStringExtra("url");
		if(url==null){
			Log.e("zhancheng", "url is null");
			setResult(0, null);
			handler.sendEmptyMessage(ACTIVITY_QUIT);
		}
		
		setContentView(R.layout.zc_activity_webview);
		((ImageView)findViewById(R.id.tv)).setOnClickListener(new OnClickListener() {			
			@Override
			public void onClick(View arg0) {
				handler.sendEmptyMessage(ACTIVITY_QUIT);			
			}
		});
		
		webview = (WebView) findViewById(R.id.mywebview);
		
//		缩放
		webview.getSettings().setSupportZoom(true);
		webview.getSettings().setBuiltInZoomControls(true); 
		
		//清理Cookie
		//CookieSyncManager.createInstance(this); 
		//CookieSyncManager.getInstance().startSync(); 
		//CookieManager.getInstance().removeSessionCookie(); 
		
		//清理缓存和历史记录
		//webview.clearCache(true); 
		//webview.clearHistory();
		
		
		progressDialog = onCreateDialog(MY_DIALOG);
		
		 
		webview.setWebViewClient(new WebViewClient(){  
			
            public boolean shouldOverrideUrlLoading(WebView view, String url) {  
            	
            	if(url.contains("native://")){
            		String action = url.substring(9);
            		if("showLoading".equals(action)){
            			handler.sendEmptyMessage(DIALOG_SHOW);
            		}else if("hideLoading".equals(action)){
            			handler.sendEmptyMessage(DIALOG_DISMISS);
            		}else if("closeWebView".equals(action)){
            			handler.sendEmptyMessage(ACTIVITY_QUIT);
            		}
            		           		
            		Log.e("zhancheng", "预留的跳转接口");
            		
            		return true;
            	}
            	
            	
            	
                //点击页面中的跳转按钮直接在本页面跳转
                view.loadUrl(url);       
                return true;       
            }
            
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
            	// TODO Auto-generated method stub
            	
            	handler.sendEmptyMessage(DIALOG_SHOW);
            	 
            	//预留的特殊跳转接口，通过检查跳转url决定
//            	if(url.contains("tieba")){
//            		Log.e("zhancheng", "预留的跳转接口");
//            	}
            }
            @Override
            public void onReceivedError(WebView view, int errorCode,
            		String description, String failingUrl) {
            	// TODO Auto-generated method stub
            	handler.sendEmptyMessage(DIALOG_DISMISS);
            	super.onReceivedError(view, errorCode, description, failingUrl);
            }
            
            @Override
            public void onPageFinished(WebView view, String url) {
                 
            	handler.sendEmptyMessage(DIALOG_DISMISS);
            		 
            	super.onPageFinished(view, url);
            }
            
            
		});
		
		webview.getSettings().setJavaScriptEnabled(true);
		
		webview.addJavascriptInterface(new Object(){
			public void showLoading() {
		        handler.sendEmptyMessage(DIALOG_SHOW);
		    }
		    public void hideLoading() {
		        handler.sendEmptyMessage(DIALOG_DISMISS);
		    }
		    public void closeWebView() {
		    	handler.sendEmptyMessage(ACTIVITY_QUIT);
		    }
		},"native");
		
		
		

		webview.loadUrl(url);
		
		setResult(1, null);
	}
	
	private static Handler handler = new Handler(){
		public void handleMessage(android.os.Message msg) {
			
			Log.e("zhancheng", "webview handleMessage what："+ msg.what);
			
			switch(msg.what){
			case DIALOG_SHOW:
				if(progressDialog!=null&&!progressDialog.isShowing())
					sActivityWebView.showDialog(MY_DIALOG);
				break;
				
			case DIALOG_DISMISS:
				if(progressDialog!=null&&progressDialog.isShowing())
					sActivityWebView.dismissDialog(MY_DIALOG);
				break;
				
			case ACTIVITY_QUIT:
				sActivityWebView.finish();
				break;
				
			default:
				Log.e("zhancheng", "webview handleMessage error");
				break;	
			}
			
			
			
		};
	};
	
	
	
	public boolean onKeyDown(int keyCode, KeyEvent event) {       
        if ((keyCode == KeyEvent.KEYCODE_BACK) && webview.canGoBack()) {       
        	webview.goBack();       
                   return true;       
        }       
        return super.onKeyDown(keyCode, event);       
    } 		
	
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}
	
	
	@Override
	@Deprecated
	protected void onPrepareDialog(int id, Dialog dialog) {
		// TODO Auto-generated method stub
		Log.e("zhancheng", "onPrepareDialog");
		if(id == MY_DIALOG){
			if(hyperspaceJumpAnimation!=null&&spaceshipImage!=null){
				spaceshipImage.startAnimation(hyperspaceJumpAnimation); 
			}
		}
	}
	
	@Override
	@Deprecated
	protected Dialog onCreateDialog(int id) {
		// TODO Auto-generated method stub
		try {
			
			LayoutInflater inflater = LayoutInflater.from(this);  
	        View v = inflater.inflate(R.layout.zc_dialog_waiting_webview_contentview, null);// 得到加载view  
	        RelativeLayout layout = (RelativeLayout) v.findViewById(R.id.zc_dialog_webview_view);// 加载布局  
	        // main.xml中的ImageView  
	        spaceshipImage = (ImageView) v.findViewById(R.id.zc_dialog_webview_img);  
//	        TextView tipTextView = (TextView) v.findViewById(R.id.progress_msg);// 提示文字
//	        tipTextView.setText(msg);// 设置加载信息
	        
	        // 加载动画  
	        hyperspaceJumpAnimation = AnimationUtils.loadAnimation(  
	                this, R.anim.zc_dialog_waiting_animation);  
	        // 使用ImageView显示动画  
//	        spaceshipImage.startAnimation(hyperspaceJumpAnimation); 
	        
	        ImageView exitTv = (ImageView) v.findViewById(R.id.zc_dialog_webview_tv);
	        exitTv.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View arg0) {
					handler.sendEmptyMessage(ACTIVITY_QUIT);
					
				}
			});
	          
	  
	        progressDialog = new Dialog(this, R.style.zc_dialog_waiting);// 创建自定义样式dialog  
	        
	        progressDialog.setCancelable(false);// 不可以用“返回键”取消  
	        progressDialog.setContentView(layout, new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.FILL_PARENT, RelativeLayout.LayoutParams.FILL_PARENT));// 设置布局  
			
		} catch (Exception e) {
			e.printStackTrace();
			Log.e("zhancheng", "生成dialog发生异常！");
			handler.sendEmptyMessage(ACTIVITY_QUIT);
		}
		
		return progressDialog;
		
	}
	
	
	
	
	
}