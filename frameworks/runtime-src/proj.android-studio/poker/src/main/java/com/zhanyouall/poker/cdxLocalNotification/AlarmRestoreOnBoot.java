package com.zhanyouall.poker.cdxLocalNotification;

import java.util.Calendar;
import java.util.Map;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.widget.Toast;

/**
 * This class is triggered upon reboot of the device. It needs to re-register
 * the alarms with the AlarmManager since these alarms are lost in case of
 * reboot.
 * 
 * @author dvtoever
 */
public class AlarmRestoreOnBoot extends BroadcastReceiver {

	private AlarmHelper alarm = null;

	@Override
	public void onReceive(Context context, Intent intent) {
		alarm = new AlarmHelper(context);

		// Obtain alarm details form Shared Preferences
		final SharedPreferences alarmSettings = context.getSharedPreferences(
				LocalNotification.PLUGIN_NAME, Context.MODE_PRIVATE);
		final Map<String, ?> allAlarms = alarmSettings.getAll();
		final Set<String> alarmIds = allAlarms.keySet();

		/*
		 * For each alarm, parse its alarm options and register is again with
		 * the Alarm Manager
		 */

		for (String alarmId : alarmIds) {
			try {
				this.processAlarm(new JSONArray(alarmSettings.getString(
						alarmId, "")));

			} catch (JSONException e) {

			}
		}

	}

	public boolean processAlarm(JSONArray args) throws JSONException {
		return this.add(args.getString(0), args.getString(1),
				args.getString(2), args.getString(3), args.getJSONArray(4),
				args.getInt(5));
	}

	public boolean add(String id, String title, String subtitle, String ticker,
			JSONArray date, int ClockType) {
		Calendar calendar = Calendar.getInstance();

		if (date.length() != 0) {
			try {
				calendar.set(date.getInt(0), date.getInt(1), date.getInt(2),
						date.getInt(3), date.getInt(4), date.getInt(5));
			} catch (JSONException e) {

			}
		}

		Calendar currentCal = Calendar.getInstance();
		// System.out.println("当前事件 "+currentCal.getTimeInMillis());
		// System.out.println("闹钟事件 "+calendar.toString());
		boolean result = false;
		if (currentCal.getTimeInMillis() > calendar.getTimeInMillis()) {

		} else {
			result = alarm.addAlarm(ClockType, title, subtitle, ticker, id,
					calendar);
		}
		return result;
	}
}
