package com.zhanyou.bugreport;

import android.content.Context;
import android.text.TextUtils;

import com.tencent.bugly.crashreport.CrashReport;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Map;

/**
 * Created by kay on 16/12/15.
 */

public class BugReportManager {
    private static BugReportManager instance;
    private BugReportManager(){}
    private static Object INSTANCE_LOCK = new Object();
    public static BugReportManager getInstance(){
        if(instance==null){
            synchronized (INSTANCE_LOCK){
                instance = new BugReportManager();
            }
        }
        return instance;
    }

    /**
     * 在Application onCreate方法内调用
     * @param context
     * @param appid
     */
    public void initBugReport(Context context,String appid,String channel){
        String packageName = context.getPackageName();
        // 获取当前进程名
        String processName = getProcessName(android.os.Process.myPid());
        // 设置是否为上报进程
        CrashReport.UserStrategy strategy = new CrashReport.UserStrategy(context);
        strategy.setUploadProcess(processName == null || processName.equals(packageName));
        if(!TextUtils.isEmpty(channel)){
            strategy.setAppChannel(channel);
        }
        CrashReport.initCrashReport(context.getApplicationContext(), appid, BuildConfig.DEBUG,strategy);
    }

    /**
     * 设置用户id
     * @param context
     * @param uid
     */
    public void setUserId(Context context,String uid){
        CrashReport.setUserId(context,uid);
    }

    /**
     * 设置标签
     * @param context
     * @param tag
     */
    public void setTag(Context context,int tag){
        CrashReport.setUserSceneTag(context,tag);
    }

    /**
     * 设置扩展信息
     * @param context
     * @param map
     */
    public void setExtraData(Context context,Map<String,String> map){
        for(Map.Entry<String, String> entry:map.entrySet()){
            CrashReport.putUserData(context,entry.getKey(),entry.getValue());
        }

    }

    /**
     * 主动上传异常
     * @param throwable
     */
    public void uploadThrowable(Throwable throwable){
        CrashReport.postCatchedException(throwable);
    }
    private String getProcessName(int pid) {
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new FileReader("/proc/" + pid + "/cmdline"));
            String processName = reader.readLine();
            if (!TextUtils.isEmpty(processName)) {
                processName = processName.trim();
            }
            return processName;
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        } finally {
            try {
                if (reader != null) {
                    reader.close();
                }
            } catch (IOException exception) {
                exception.printStackTrace();
            }
        }
        return null;
    }
}
