//
//  ZCThirdDelegate.m
//  GameDemo
//
//  Created by Happy on 1/16/17.
//  Copyright © 2017 Happy. All rights reserved.
//

#import "ZCThirdDelegate.h"

@implementation ZCThirdDelegate
+ (ZCThirdDelegate *)sharedInstance {
    static ZCThirdDelegate *share = nil;
    static dispatch_once_t predicate;
    dispatch_once(&predicate, ^{
        share = [[ZCThirdDelegate alloc] init];
    });
    return share;
}
@end


#if ZBShareWeiXinIsOpen | ZBLoginWeiXinIsOpen
@implementation ZCThirdDelegate (WeiXinShare)
- (void)onReq:(BaseReq *)req {
    
}
-(void)onResp:(BaseResp*)resp {
    if([resp isKindOfClass:[SendAuthResp class]]) {
        // 微信登陆回调
        NSString *code = ((SendAuthResp *)resp).code;
        if (code) {
            self.loginCallback(nil, @{@"code":((SendAuthResp *)resp).code}, ZBLoginTypeWeiXin);
            
        } else {
            NSError *error = [NSError errorWithDomain:@"com.zbsdk.login" code:resp.errCode userInfo:@{NSLocalizedDescriptionKey:resp.errStr ? resp.errStr:@"微信登录失败"}];
            self.loginCallback(error, @{}, ZBLoginTypeWeiXin);
            
        }
    } else if([resp isKindOfClass:[SendMessageToWXResp class]]) {
        if (resp.errCode == 0) {
            self.shareCallback(nil, @{}, ZCBShareTypeWeiXin);

        } else {
            NSError *error = [NSError errorWithDomain:@"com.zbsdk.share" code:resp.errCode userInfo:@{NSLocalizedDescriptionKey:resp.errStr ? resp.errStr:@"微信分享失败"}];
            self.shareCallback(error, @{}, ZCBShareTypeWeiXin);

        }
    }
}
@end
#endif


#if ZBShareFaceBookIsOpen
@implementation ZCThirdDelegate (FacebookShare)
- (void)sharer:(id<FBSDKSharing>)sharer didCompleteWithResults:(NSDictionary *)results {
    self.shareCallback(nil, @{}, ZCBShareTypeFacebook);
}

- (void)sharer:(id<FBSDKSharing>)sharer didFailWithError:(NSError *)error {
    self.shareCallback(error, @{}, ZCBShareTypeFacebook);
}

- (void)sharerDidCancel:(id<FBSDKSharing>)sharer {
    NSError *error = [NSError errorWithDomain:@"com.zcbsdk.share" code:-2 userInfo:@{NSLocalizedDescriptionKey:@"用户取消"}];
    self.shareCallback(error, @{}, ZCBShareTypeFacebook);
}
@end
#endif
