//
//  ZBManager.m
//  GameDemo
//
//  Created by Happy on 12/16/16.
//  Copyright © 2016 Happy. All rights reserved.
//

#import "ZBManager.h"

@implementation ZBManager

static ZBManager *manager = nil;
+ (instancetype)sharedInstance {
    if (!manager) {
        manager = [[ZBManager alloc] init];
        
        [ZBSDKConfigure sharedInstance].isOnline = YES; // 默认线上环境
    }
    
    return manager;
}

- (void)setAppId:(NSString *)appid appkey:(NSString *)appkey {
    [ZBSDKConfigure sharedInstance].appid = appid;
    [ZBSDKConfigure sharedInstance].appkey = appkey;
}
- (void)setChannelId:(NSString *)channelId productId:(NSString *)productId skinColor:(int)skinColor {
    [ZBSDKConfigure sharedInstance].projectChannelId = channelId;
    [ZBSDKConfigure sharedInstance].appProductId = productId;
    [ZBSDKConfigure sharedInstance].skinColor = skinColor;
}

- (void)configureLoginProcessType:(ZBSDKProcessType)processType {
    [ZBSDKConfigure sharedInstance].serverProcess = processType;
}

- (void)configureServerEnvironment:(BOOL)isOnline {
    [ZBSDKConfigure sharedInstance].isOnline = isOnline;
}

- (void)configureWeiXinAppId:(NSString *)appId {
    [WXApi registerApp:appId];
    
    [ZCShare shareInstance].weiXinAppId = appId;
    [ZCLoginManager sharedInstance].weiXinAppId = appId;
}

- (BOOL)application:(UIApplication *)application
didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    return YES;
}
- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url {
    BOOL wxValue = NO;
#if ZBLoginWeiXinIsOpen | ZBShareWeiXinIsOpen
    wxValue = [WXApi handleOpenURL:url delegate:[ZCThirdDelegate sharedInstance]];
#endif
    
    BOOL qqValue = NO;
#if ZBLoginQQIsOpen | ZBShareQQIsOpen
    qqValue = [TencentOAuth HandleOpenURL:url];
#endif
    
    BOOL wbValue = NO;
#if ZBLoginWeiBoIsOpen | ZBShareWeiBoIsOpen
//    wbValue = [WeiboSDK handleOpenURL:url delegate:[ZCThirdDelegate sharedInstance]];
#endif
    
#if ZCPayAiBeiIsOpen
    [[IapppayKit sharedInstance] handleOpenUrl:url];
#endif
    
#if ZCPayPingppIsOpen
    [[ZCPaymentManager shareInstance] handlePingppPayResult:url.absoluteString];
#endif
    
    return wxValue | qqValue | wbValue;
}
- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    
    return [self application:application handleOpenURL:url];
}
- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<NSString*, id> *)options {
    
    return [self application:app handleOpenURL:url];
}
@end


@implementation ZBManager (Login)

- (void)loginSendAuthCode:(NSString *)phoneNum callback:(void(^)(NSError *error, NSDictionary *result))callback {
    if (phoneNum.length == 0) { // 手机号为空
        NSError *newError = [NSError errorWithDomain:ZBLoginErrorDomain code:ZBLoginErrorBlankPhoneNum userInfo:@{NSLocalizedDescriptionKey:@"手机号不能为空"}];
        callback(newError, nil);
        
        return;
    }
    
    [ZCPlatformRequestManager requestForSendAuthCode:phoneNum callback:callback];
}

- (void)loginInPhone:(NSString *)phoneNum authCode:(NSString *)authCode callback:(void(^)(NSError *error, NSDictionary *result))callback {
    if (phoneNum.length == 0) { // 手机号为空
        NSError *newError = [NSError errorWithDomain:ZBLoginErrorDomain code:ZBLoginErrorBlankPhoneNum userInfo:@{NSLocalizedDescriptionKey:@"手机号不能为空"}];
        callback ? callback(newError, nil):nil;
        
        return;
    }
    
    if (authCode.length == 0) { // 验证码为空
        NSError *newError = [NSError errorWithDomain:ZBLoginErrorDomain code:ZBLoginErrorBlankAuthCode userInfo:@{NSLocalizedDescriptionKey:@"验证码不能为空"}];
        callback(newError, nil);
        
        return;
    }
    
    [ZCPlatformRequestManager requestForPhoneLogin:phoneNum authCode:authCode callback:^(NSError *error, NSDictionary *dic) {
        if (error) {
            callback(error, nil);
            
        } else {
            if ([ZBSDKConfigure sharedInstance].serverProcess == ZBSDKProcessTypePlatform) {
                callback(nil, @{ZBLoginResultKeyForLogin:dic});
                
            } else if ([ZBSDKConfigure sharedInstance].serverProcess == ZBSDKProcessTypeLiveUC) {
                [ZCLiveRequestManager requestForLoginUCServer:dic callback:callback];
                
            } else {
                [ZCLiveRequestManager requestForLoginLiveServer:@{ZBLoginResultKeyForLogin:dic} callback:callback];
                
            }
        }
        
    }];
    
}

- (void)loginInVisitor:(void(^)(NSError *error, NSDictionary *result))callback {
    [ZCPlatformRequestManager requestForVisitor:^(NSError *error, NSDictionary *dic) {
        if (error) {
            callback(error, nil);
            
        } else {
            if ([ZBSDKConfigure sharedInstance].serverProcess == ZBSDKProcessTypePlatform) {
                callback(nil, @{ZBLoginResultKeyForLogin:dic});
                
            } else if ([ZBSDKConfigure sharedInstance].serverProcess == ZBSDKProcessTypeLiveUC) {
                [ZCLiveRequestManager requestForLoginUCServer:dic callback:callback];
                
            } else {
                [ZCLiveRequestManager requestForLoginLiveServer:dic callback:callback];
                
            }
        }
    }];
    
}

- (void)loginInWeiXin:(void(^)(NSError *error, NSDictionary *result))callback {
    [[ZCLoginManager sharedInstance] loginInWeiXin:^(NSError *error, NSDictionary *result) {
        if (error) {
            callback(error, nil);
            
        } else {
            [ZCPlatformRequestManager requestForWeiXinLogin:result[@"code"] wxAppid:result[@"appid"] callback:^(NSError *error, NSDictionary *result) {
                if (error) {
                    callback(error, nil);
                    
                } else {
                    // 登录平台服务器成功后
                    
                    
                    if ([ZBSDKConfigure sharedInstance].serverProcess == ZBSDKProcessTypePlatform) {
                        callback(nil, result);
                        
                    } else if ([ZBSDKConfigure sharedInstance].serverProcess == ZBSDKProcessTypeLiveUC) {
                        [ZCLiveRequestManager requestForLoginUCServer:result[ZBLoginResultKeyForLogin] callback:^(NSError *error, NSDictionary *dic) {
                            
                            if (error) {
                                callback(error, nil);
                                
                            } else {
                                callback(nil, @{ZBLoginResultKeyForLogin:dic, ZBLoginResultKeyForProfile:result[ZBLoginResultKeyForProfile]});
                            }
                        }];

                    } else {
                        [ZCLiveRequestManager requestForLoginLiveServer:result callback:callback];
                        
                    }

                }

            }];
        }
    }];
}

@end


@implementation ZBManager (Payment)
- (void)configurePayCallbackAddress:(NSString *)address {
    [ZCPaymentManager shareInstance].payCallback = address;
}

- (void)configureProductIdentifierArr:(NSArray *)productIdentifierArr {
    [ZCApplePay shareInstance].productIdentifierArr = productIdentifierArr;
}

- (void)setAipppayAppId:(NSString *)aipppayAppId platformKey:(NSString *)platformKey {
#if ZCPayAiBeiIsOpen
    [ZBSDKConfigure sharedInstance].aipppayAppId = aipppayAppId;
    [ZBSDKConfigure sharedInstance].aipppayPlatformKey = platformKey;
    
    NSString *appAlipayScheme = [NSString stringWithFormat:@"Aipppay.alipay.%@", [ZCLocalData preferencePlistObject:@"CFBundleIdentifier"]];
    [[IapppayKit sharedInstance] setAppURLScheme:appAlipayScheme];
    [[IapppayKit sharedInstance] setAppId:aipppayAppId mACID:nil];
    [[IapppayKit sharedInstance] setIapppayPayWindowOrientationMask:UIInterfaceOrientationMaskPortrait];
#else
    NSLog(@"ZCSDK 爱贝支付暂未开放");
    
#endif
}

- (void)pay:(ZCPayItem *)payItem completion:(ZCPayCompletion)completion {
    [[ZCPaymentManager shareInstance] pay:payItem completion:completion];
}
@end

@implementation ZBManager (Share)
- (void)share:(ZCBShareContent *)shareContent shareType:(ZCBShareType)shareType callback:(void(^)(NSError *error, NSDictionary *result))callback {
    [[ZCShare shareInstance] share:shareContent shareType:shareType callback:callback];
}
@end
