//
//  ZBSDKConfigure.h
//  GameDemo
//
//  Created by Happy on 12/16/16.
//  Copyright © 2016 Happy. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, ZBSDKProcessType) {
    ZBSDKProcessTypePlatform = 0, //只走平台服务器
    ZBSDKProcessTypeLiveUC = 1, //走到直播UC服务器
    ZBSDKProcessTypeLiveApp = 2 //走到直播App服务器,支付时走查询订单逻辑
};

@interface ZBSDKConfigure : NSObject
@property (nonatomic, copy) NSString *appid;
@property (nonatomic, copy) NSString *appkey;

@property (nonatomic, copy) NSString *aipppayAppId;
@property (nonatomic, copy) NSString *aipppayPlatformKey;

@property (nonatomic, copy) NSString *facebookAppId;

@property (nonatomic, assign) int isReview; //是否审核中 0:审核中；1:审核通过
@property (nonatomic, assign) BOOL isOnline; //是否线上
@property (nonatomic, copy) NSString *testBaseUrl; //审核服Base url
@property (nonatomic, assign) ZBSDKProcessType serverProcess; //走服务器流程

@property (nonatomic, copy) NSString *token; //游戏单服token
@property (nonatomic, copy) NSString *uid;
@property (nonatomic, copy) NSString *projectChannelId;
@property (nonatomic, copy) NSString *appProductId;
@property (nonatomic, assign) int skinColor; //皮肤颜色

@property (nonatomic, strong) NSDictionary *ipInfoDic;

+ (instancetype)sharedInstance;
@end
