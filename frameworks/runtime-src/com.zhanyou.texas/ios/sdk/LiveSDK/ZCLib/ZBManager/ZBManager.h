//
//  ZBManager.h
//  GameDemo
//
//  Created by Happy on 12/16/16.
//  Copyright © 2016 Happy. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "ZCShare.h"
#import "ZCLoginManager.h"
#import "ZCPaymentManager.h"
#import "ZCThirdDelegate.h"

#import "ZCPlatformRequestManager.h"
#import "ZCLiveRequestManager.h"

#import "LGActionSheet.h"
#import "ZCUIExtension.h"

#import "ZBSDKConfigure.h"
#import "ZCUtils.h"

@interface ZBManager : NSObject

+ (instancetype)sharedInstance;

/**
 配置sdk的appid和appke
 */
- (void)setAppId:(NSString *)appid appkey:(NSString *)appkey;

/**
 配置channelId和productId

 @param channelId 默认值 appstore_game
 @param productId 默认值 0
 @param skinColor 皮肤颜色
 */
- (void)setChannelId:(NSString *)channelId productId:(NSString *)productId skinColor:(int)skinColor;


/**
 选择服务器环境，默认YES 线上

 @param isOnline YES:线上；NO:线下
 */
- (void)configureServerEnvironment:(BOOL)isOnline;


/**
 配置微信AppId，微信登录分享需要

 @param appId 微信appid
 */
- (void)configureWeiXinAppId:(NSString *)appId;

//
- (void)configureLoginProcessType:(ZBSDKProcessType)processType;


- (BOOL)application:(UIApplication *)application
didFinishLaunchingWithOptions:(NSDictionary *)launchOptions;
- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url;
- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation;
- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<NSString*, id> *)options;

@end


@interface ZBManager (Login)
/**
 游客登录
 
 @param callback 回调
 */
- (void)loginInVisitor:(void(^)(NSError *error, NSDictionary *result))callback;

/**
 手机登录获取验证码
 
 @param phoneNum 手机号
 @param callback 回调
 */
- (void)loginSendAuthCode:(NSString *)phoneNum callback:(void(^)(NSError *error, NSDictionary *result))callback;
/**
 手机登录
 
 @param phoneNum 手机号
 @param authCode 验证码
 @param callback 回调
 */
- (void)loginInPhone:(NSString *)phoneNum authCode:(NSString *)authCode callback:(void(^)(NSError *error, NSDictionary *result))callback;

/**
 微信登录

 @param callback 回调
 */
- (void)loginInWeiXin:(void(^)(NSError *error, NSDictionary *result))callback;

@end


@interface ZBManager (Payment)
/**
 配置支付回调地址

 @param address 回调地址
 */
- (void)configurePayCallbackAddress:(NSString *)address;


/**
 In-App Purchase, 添加产品Id，比如：com.zhanle.shoutime.appms.diamond1
 
 @param productIdentifierArr 产品Id数组
 */
- (void)configureProductIdentifierArr:(NSArray *)productIdentifierArr;


/**
 配置爱贝支付的appid和platformKey
 
 @param aipppayAppId 爱贝appid，如果不需要爱贝支付，可以不配置
 */
- (void)setAipppayAppId:(NSString *)aipppayAppId platformKey:(NSString *)platformKey;


/**
 支付sdk

 @param payItem 支付参数
 @param completion 回调
 */
- (void)pay:(ZCPayItem *)payItem completion:(ZCPayCompletion)completion;
@end


@interface ZBManager (Share)
- (void)share:(ZCBShareContent *)shareContent shareType:(ZCBShareType)shareType callback:(void(^)(NSError *error, NSDictionary *result))callback;
/*
 使用教程如下：
 1.分享链接到微信好友(分享到微信朋友圈只需要将wxScene赋值为1)
 ZCBShareContent *shareContent = [[ZCBShareContent alloc] init];
 shareContent.contentType = ZCBShareContentTypeLink;
 shareContent.wxScene = 0;
 shareContent.contentTitle = _title;
 shareContent.contentURL = _httpURL;
 shareContent.contentDescription = des;
 shareContent.thumbImageURL = _imageUrlStr;
 [[ZBManager sharedInstance] share:shareContent shareType:ZCBShareTypeWeiXin callback:callback];
 */
@end
