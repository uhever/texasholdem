//
//  ZCPlatformRequest.h
//  ZCLibProjectDEV
/*
    最终的BodyData使用UTF8编码，然后Base64
 */
//  Created by Happy on 8/27/16.
//  Copyright © 2016 Happy. All rights reserved.
//

#import "ZCRequest.h"
#import "ZCPlatformApi.h"

#define ZCAPIV 4 //接口版本
#define ZCPLATFORM 1 //手机平台（1:ios, 2:and, 3:wp）
#define ZCSDK_SDK_Ver @"1.3.5_zhCN_1_cn" //sdk版本号_地区标识(大陆:zhCN，港台:zhTW，东南亚中文:zhCNSA，东南亚英文:enSA，欧美:US)_横竖屏标识(竖屏1，横屏2)_语言标识

#define ZCPlatformAuthkeyKey @"authkey" //登录成功返回的authkey
#define ZCPlatformUidKey @"uId"
#define ZCPlatformSessionKey @"session"


@interface ZCPlatformRequest : ZCRequest
+ (void)request:(NSString *)url parameter:(NSDictionary *)parameter success:(RequestSuccess)success failure:(RequestFailure)failure;
@end
