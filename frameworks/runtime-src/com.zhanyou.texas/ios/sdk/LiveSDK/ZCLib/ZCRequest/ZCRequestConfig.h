//
//  ZCRequestConfig.h
//  ZCLibProjectDEV
//
//  Created by Happy on 8/27/16.
//  Copyright © 2016 Happy. All rights reserved.
//

#ifndef ZCRequestConfig_h
#define ZCRequestConfig_h

#pragma mark - 可更改配置参数
#define ZCRLogLevel 1 //0:无任何错误提示；1:只打印控制台log；2:打印控制台log，并且允许弹窗提示


#pragma mark - 不可更改配置参数
#if DEBUG
// 开发产品时可控请求的日志
#if ZCRLogLevel > 0
#define ZCRequestLog(...) NSLog(__VA_ARGS__)
#else
#define ZCRequestLog(...)
#endif

#else
// 提交产品时关闭请求的日志
#define ZCRequestLog(...)
#endif

#endif /* ZCRequestConfig_h */
