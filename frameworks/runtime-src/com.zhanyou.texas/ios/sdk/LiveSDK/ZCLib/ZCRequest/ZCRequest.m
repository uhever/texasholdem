//
//  ZCRequest.m
//  ZCLibProjectDEV
//
//  Created by Happy on 8/27/16.
//  Copyright © 2016 Happy. All rights reserved.
//

#import "ZCRequest.h"

NSString *const ZCRequestEncryptKey = @"encryptType";
NSString *const ZCRequestErrorDomain = @"com.zcrequest.errordomain";

@implementation ZCRequest
- (id)initWithBaseURL:(NSString *)baseURL {
    if (self = [super init]) {
        _httpOperationManager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:baseURL]];
        _httpOperationManager.requestSerializer = [AFJSONRequestSerializer serializer];
        _httpOperationManager.responseSerializer = [AFHTTPResponseSerializer serializer];
        _httpOperationManager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/html", @"text/plain", @"application/json",nil];
        _httpOperationManager.requestSerializer.timeoutInterval = 30;
    }
    return self;
}

- (void)post:(NSString *)URLString parameter:(NSDictionary *)parameter success:(RequestSuccess)success failure:(RequestFailure)failure {
    // 打印请求发送参数
    NSMutableDictionary *newParameter = parameter.mutableCopy;
    [newParameter removeObjectForKey:ZCRequestEncryptKey];
    ZCRequestLog(@"RequestUrl->%@", [NSString stringWithFormat:@"%@%@",_httpOperationManager.baseURL.absoluteString, URLString]);
    ZCRequestLog(@"RequestParameter->%@", [ZCEncrypt getKeyValuedString:newParameter]);
    
    [_httpOperationManager POST:URLString parameters:parameter progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
	 	// 打印请求返回结果
        ZCRequestLog(@"ResponseUrl->%@", [NSString stringWithFormat:@"%@",task.currentRequest.URL.absoluteString]);
        ZCRequestLog(@"ResponseJsonString->%@", [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding]);
        
        NSError *error = nil;
        NSDictionary* responseDic = [NSJSONSerialization JSONObjectWithData:(NSData *)responseObject options:NSJSONReadingAllowFragments error:&error];
        if (error) {
            NSLog(@"%@",error.userInfo);
        }else{
            success(responseDic);
        }

    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        failure(error);
        // 打印请求返回结果
        ZCRequestLog(@"ResponseUrl->%@", [NSString stringWithFormat:@"%@",task.currentRequest.URL.absoluteString]);
        ZCRequestLog(@"ResponseError->%@", error);

    }];
}

- (void)get:(NSString *)URLString parameter:(NSDictionary *)parameter success:(RequestSuccess)success failure:(RequestFailure)failure {
    [_httpOperationManager GET:URLString parameters:parameter progress:^(NSProgress * _Nonnull downloadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary* responseDic = [NSJSONSerialization JSONObjectWithData:(NSData *)responseObject options:NSJSONReadingAllowFragments error:nil];
        success(responseDic);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        failure(error);

    }];
}

+ (NSString *)deviceInfo {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    
    [dic setObject:[ZCDeviceInfo deviceUUIDString] forKey:@"uuid"];
    [dic setObject:[ZCDeviceInfo advertisingIdentifier] forKey:@"idfa"];
    [dic setObject:@"" forKey:@"mac"];
    [dic setObject:[ZCDeviceInfo deviceBrand] forKey:@"brand"];
    [dic setObject:[ZCDeviceInfo deviceModel] forKey:@"model"];
    [dic setObject:[ZCDeviceInfo deviceSystemVersion] forKey:@"ver"];
    
    return [ZCEncrypt getJsonString:dic];
}

+ (NSString *)languageParameter {
    LGLanguageKind kind = [[LGLocalization sharedInstance] currentLanguageInt];
    
    if (kind == LGLanguageKindZhs) {
        return @"cn";
    } else if (kind == LGLanguageKindZht) {
        return @"tw";
    }
    
    return @"cn";
}

+ (NSData *)requestBodyData:(NSDictionary *)parameter {
    NSInteger encryptType = [parameter[ZCRequestEncryptKey] integerValue];

    NSMutableDictionary *newParameter = parameter.mutableCopy;
    [newParameter removeObjectForKey:ZCRequestEncryptKey];
    NSString *paraStr = [ZCEncrypt getKeyValuedString:newParameter];
    
    //选择加密方式
    if (encryptType == ZCRequestEncryptTypeUTF8AndBase64) {
        return [[paraStr dataUsingEncoding:NSUTF8StringEncoding] base64EncodedDataWithOptions:NSDataBase64Encoding64CharacterLineLength];
    } else if (encryptType == ZCRequestEncryptTypeUTF8) {
        return [paraStr dataUsingEncoding:NSUTF8StringEncoding];
    }
    
    return [NSData data];
}
@end
