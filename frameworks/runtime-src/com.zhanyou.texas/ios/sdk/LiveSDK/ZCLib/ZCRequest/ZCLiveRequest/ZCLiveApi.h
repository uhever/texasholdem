//
//  ZCLiveApi.h
//  ZCLibProjectDEV
//
//  Created by Happy on 8/27/16.
//  Copyright © 2016 Happy. All rights reserved.
//

#ifndef ZCLiveApi_h
#define ZCLiveApi_h



#pragma mark - uc服务器 url
#define ZCBaseURLForUCServer_Dev @"http://106.75.51.187/uc.live.com/"
#define ZCBaseURLForUCServer_Dist @"https://uc-live.jjshowtime.com/"
#define ZCURL_Login_UCServer @"index.php?c=userzc&m=login"


#pragma mark - 直播服务器 url
#define ZCBaseURLForAppServer_Dev @"http://106.75.51.187:9501"   // 服务器2
#define ZCBaseURLForAppServer_Dist @"https://app-live.jjshowtime.com"
#define ZCURL_Login_LiveServer @"/?c=login&m=user_token"
#define ZCURL_Get_Info        @"/?c=member&m=get_member_info"
#define ZCURL_Lookup_Order_State @"/?c=recharge&m=get_order_status"


#pragma mark - 支付服务器 url
#define ZCBaseURLForPayServer_Dev @"http://106.75.51.187/pay.live.com/"
#define ZCBaseURLForPayServer_Dist @"https://pay-live.jjshowtime.com/"
#define ZCURL_Create_Prepay_Order @"index.php?c=payzc&m=create_prepay_order"
#define ZCURL_Pay_CallBack @"payzc.php"
#define ZCURL_Get_Product_List @"index.php?c=ios&m=get_product_list" //获取支付的订单列表
#endif /* ZCLiveApi_h */
