//
//  ZCLiveRequest.m
//  ZCLibProjectDEV
//
//  Created by Happy on 8/27/16.
//  Copyright © 2016 Happy. All rights reserved.
//

#import "ZCLiveRequest.h"

NSString *const ZCLiveRequestFailureKey = @"ZCLiveRequestFailureKey";
NSString *const ZCLiveRequestLiveAppServerTokenKey = @"ZCLiveRequestLiveAppServerTokenKey";
NSString *const ZCLiveRequestLiveAppServerUIDKey = @"ZCLiveRequestLiveAppServerUIDKey";

@implementation ZCLiveRequest
#pragma mark - OverWrite
- (id)initWithBaseURL:(NSString *)baseURL {
    if (self = [super initWithBaseURL:baseURL]) {
        [self.httpOperationManager.requestSerializer setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];

    }
    return self;
}
- (void)post:(NSString *)URLString parameter:(NSDictionary *)parameter success:(RequestSuccess)success failure:(RequestFailure)failure {
    
    NSMutableDictionary *newPara = [NSMutableDictionary dictionaryWithDictionary:parameter];
    [newPara setObject:@(ZCRequestEncryptTypeUTF8) forKey:ZCRequestEncryptKey];
    if ([newPara objectForKey:@"devInfo"]) {
        [newPara setObject:[ZCRequest deviceInfo] forKey:@"devInfo"];
    }
    if ([newPara objectForKey:@"idfa"]) {
        [newPara setObject:[ZCDeviceInfo advertisingIdentifier] forKey:@"idfa"];
    }
    if (newPara[@"token"] && [newPara[@"token"] length] == 0) {
        [newPara setObject:[ZCLiveRequest tokenForLiveAppServer] forKey:@"token"];
    }
    
    [super post:URLString parameter:newPara success:^(NSDictionary *result) {
        if ([[result objectForKey:@"status"] intValue] == 1) {
            success(result);
            
        } else {
            NSString *errorMsg = LGLocalizedString(@"badNetworkAlert", @"服务端异常，请稍后重试。");
            NSError *error = [NSError errorWithDomain:ZCRequestErrorDomain code:[[result objectForKey:@"status"] intValue] userInfo:@{NSLocalizedDescriptionKey:errorMsg}];
            failure(error);
            
        }
    } failure:^(NSError *error) {
        failure(error);
        
    }];
}
#pragma mark


+ (void)requestUCServer:(NSString *)url parameter:(NSDictionary *)parameter success:(RequestSuccess)success failure:(RequestFailure)failure {
    [ZCLiveRequest request:[self p_baseURLForUCServer] relativeURL:url parameter:parameter success:success failure:failure];
}
+ (void)requestAppServer:(NSString *)url parameter:(NSDictionary *)parameter success:(RequestSuccess)success failure:(RequestFailure)failure {
    [ZCLiveRequest request:[self p_baseURLForAppServer] relativeURL:url parameter:parameter success:success failure:failure];
}
+ (void)requestPayServer:(NSString *)url parameter:(NSDictionary *)parameter success:(RequestSuccess)success failure:(RequestFailure)failure {
    [ZCLiveRequest request:[self p_baseURLForPayServer] relativeURL:url parameter:parameter success:success failure:failure];
}


- (void)downloadFile:(NSString *)url filePath:(NSString *)filePath progress:(void(^)(NSProgress *))progress success:(void(^)(NSData *fileData))success failure:(RequestFailure)failure {
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
    NSURL *URL = [NSURL URLWithString:url];
    NSURLRequest *request = [NSURLRequest requestWithURL:URL];
    
    NSURLSessionDownloadTask *downloadTask = [manager downloadTaskWithRequest:request progress:^(NSProgress * _Nonnull downloadProgress) {
        !progress ? : progress(downloadProgress);
        
    } destination:^NSURL * _Nonnull(NSURL * _Nonnull targetPath, NSURLResponse * _Nonnull response) {
        return [NSURL fileURLWithPath:filePath];
        
    } completionHandler:^(NSURLResponse * _Nonnull response, NSURL * _Nullable filePath, NSError * _Nullable error) {
        if (!error) {
            NSLog(@"下载成功:%@", filePath.absoluteString);
            !success ? :success(nil);
            
        } else {
            NSLog(@"下载失败: %@", error.localizedDescription);
        }
    }];
    
    [downloadTask resume];
}
+ (void)uploadPicture:(NSString *)url parameters:(NSDictionary *)para imagePara:(NSDictionary *)imagePara progress:(void(^)(NSProgress *))progress success:(RequestSuccess)success failure:(RequestFailure)failure {
    NSMutableDictionary *mutPara = para.mutableCopy;
    if (para[@"token"] && [para[@"token"] length] == 0) {
        [mutPara setObject:[ZCLiveRequest tokenForLiveAppServer] forKey:@"token"];
    }
    UIImage *image = [imagePara objectForKey:@"image"];
    NSData *imageData = UIImageJPEGRepresentation(image, 1.0);//将UIImage转为NSData，1.0表示不压缩图片质量。
    
    AFHTTPSessionManager *httpManager = [[AFHTTPSessionManager alloc] initWithBaseURL:nil];
    httpManager.requestSerializer = [AFJSONRequestSerializer serializer];
    [httpManager.requestSerializer setValue:@"multipart/form-data" forHTTPHeaderField:@"Content-Type"];
    [httpManager.requestSerializer setValue:[NSString stringWithFormat:@"%ld", (unsigned long)imageData.length] forHTTPHeaderField:@"Content-Length"];
    httpManager.responseSerializer = [AFHTTPResponseSerializer serializer];
    httpManager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/html", @"text/plain", nil];
    httpManager.requestSerializer.timeoutInterval = 30;

    if (para[@"server_url"]) {
        url = [url stringByAppendingString:[NSString stringWithFormat:@"&server_url=%@", [ZCEncrypt base64EncodedString:para[@"server_url"]]]];
    }
    
    [httpManager POST:url parameters:mutPara constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        if ([imagePara objectForKey:@"image"]) {
            [formData appendPartWithFileData:imageData name:[imagePara objectForKey:@"uploadKey"] fileName:[imagePara objectForKey:@"fileName"] mimeType:@"image/jpeg"];
        }
    } progress:progress success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSString *resString = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        
        NSDictionary* responseDic = [NSJSONSerialization JSONObjectWithData:(NSData *)responseObject options:NSJSONReadingAllowFragments error:nil];
        
        success(responseDic);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        failure(error);
    }];
}

+ (NSString *)tokenForLiveAppServer {
    NSString *token = [ZCLocalData userDefaultObject:ZCLiveRequestLiveAppServerTokenKey];
    if (token.length) {
        return token;
    }
    return @"";
}
+ (NSString *)uidForLiveAppServer {
    NSString *uid = [ZCLocalData userDefaultObject:ZCLiveRequestLiveAppServerUIDKey];
    if (uid.length) {
        return uid;
    }
    return @"";
}

#pragma mark - Private method
+ (void)request:(NSString *)baseURL relativeURL:(NSString *)relativeURL parameter:(NSDictionary *)parameter success:(RequestSuccess)success failure:(RequestFailure)failure {
    
    //以get参数方式，在每个接口后边拼接app_version、app_token_uid、app_channel_id、app_product_id、app_skin
    NSString *newRelativeURL = [relativeURL stringByAppendingString:[self relativeURLAppendingString]];

    ZCLiveRequest *request = [[ZCLiveRequest alloc] initWithBaseURL:baseURL];
    [request post:newRelativeURL parameter:parameter success:^(NSDictionary *dic) {
        success(dic);
        
    } failure:^(NSError *error) {
        failure(error);
        if (error.code == -1009) { //网络断了
            [ZCLiveRequest alertForBadNetwork:relativeURL];
        }
        
        ZCRequestLog(@"RUL:%@\%@ \nErrorCode:%d, ErrorMsg:%@", baseURL,relativeURL, (int)error.code, error.localizedDescription);
    }];
}

+ (NSString *)relativeURLAppendingString {
    //    return @""; //如果不需要拼接参数，则返回空字符串
    
    return [NSString stringWithFormat:@"&app_channel_id=%@&app_version=%@&app_token_uid=%@&app_product_id=%@&app_skin=%@&lang_type=%@", [ZBSDKConfigure sharedInstance].projectChannelId, [ZCLocalData preferencePlistObject:@"CFBundleVersion"], [self uidForLiveAppServer], [ZBSDKConfigure sharedInstance].appProductId, [NSString stringWithFormat:@"%d", [ZBSDKConfigure sharedInstance].skinColor], [ZCRequest languageParameter]];
}


#pragma mark - 获取Base URL
+ (NSString *)p_baseURLForUCServer {
    NSString *baseUrlForUCServer = ZCBaseURLForUCServer_Dev;
    
    if ([ZBSDKConfigure sharedInstance].isOnline) {
        baseUrlForUCServer = ZCBaseURLForUCServer_Dist;
    }
    
    return baseUrlForUCServer;
}
+ (NSString *)p_baseURLForAppServer {
    NSString *baseUrlForAppServer = ZCBaseURLForAppServer_Dev;
    
    if ([ZBSDKConfigure sharedInstance].isOnline) {
        baseUrlForAppServer = ZCBaseURLForAppServer_Dist;
        
        if ([ZBSDKConfigure sharedInstance].isReview == 0) {
            if ([ZBSDKConfigure sharedInstance].testBaseUrl.length) {
                baseUrlForAppServer = [ZBSDKConfigure sharedInstance].testBaseUrl;
            }
        }
    }
    
    return baseUrlForAppServer;
}
+ (NSString *)p_baseURLForPayServer {
    NSString *baseUrlForPayServer = ZCBaseURLForPayServer_Dev;
    
    if ([ZBSDKConfigure sharedInstance].isOnline) {
        baseUrlForPayServer = ZCBaseURLForPayServer_Dist;
    }
    
    return baseUrlForPayServer;
}

+ (void)alertForBadNetwork:(NSString *)urlStr {
    if ([urlStr isEqualToString:ZCURL_Get_Info] ||
        [urlStr isEqualToString:ZCURL_Login_LiveServer] ||
        [urlStr isEqualToString:ZCURL_Login_UCServer]) {
//        [HRAlertView showMessage:LGLocalizedString(@"noNetworkAlert", nil) alertViewType:HRAlertViewTypeFromTop];
    }
}
@end
