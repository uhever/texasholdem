//
//  SignalStrengthManager.h
//  cocos
//
//  Created by user on 2017/2/17.
//  Copyright © 2017年 refrainC. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void(^SignalStrengthChange)(float SignalStrengthLevel);


@interface SignalStrengthManager : NSObject

@property (nonatomic, copy)SignalStrengthChange SignalStrengthChange;



+ (instancetype)manager;

- (void)startObserveSignalStrength;

- (void)stopObserveSignalStrength;

- (float)getCurrentSignalStrength;




@end
