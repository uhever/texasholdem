//
//  RecorderManager.h
//  cocos
//
//  Created by user on 2017/2/17.
//  Copyright © 2017年 refrainC. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void(^recorderComplete)(BOOL isSuccess);
typedef void(^interruptionBlock)();

@interface RecorderManager : NSObject


@property (nonatomic, copy)recorderComplete recorderComplete;

@property (nonatomic, copy)interruptionBlock interruptionBlock;

+ (instancetype)manager;


- (void)startRecorderWithSuccess:(void(^)(BOOL isSuccess))success;


- (void)stopRecorder;

@end
