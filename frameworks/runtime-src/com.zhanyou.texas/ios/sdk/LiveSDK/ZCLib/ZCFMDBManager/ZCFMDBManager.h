//
//  ZCFMDBManager.h
//  HuaRong
//
//  Created by Happy on 8/29/16.
//  Copyright © 2016 Happy. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ZCFMDBManager : NSObject
+ (instancetype)shareInstance;

/**
 *  处理苹果交易订单
 */
- (BOOL)appleOrderSave:(NSDictionary *)orderDic;
- (NSDictionary *)appleOrderByOrderId:(NSString *)orderId;
- (NSArray *)appleOrderByUid:(NSString *)uid;
- (BOOL)appleOrderDeleteByOrderId:(NSString *)orderId;
- (BOOL)appleOrderDeleteByUid:(NSString *)uid;

/**
 *  保存苹果交易号
 *
 *  @param transactionIdentifier 苹果交易号
 *
 *  @return YES:保存成功；NO:保存失败(可能原因是已经存在)
 */
- (BOOL)appleTransactionIdentifierSave:(NSString *)transactionIdentifier;
- (void)appleTransactionIdentifierTraverse;
- (void)table:(NSString *)tableName topLine:(NSInteger)topLine;
@end
