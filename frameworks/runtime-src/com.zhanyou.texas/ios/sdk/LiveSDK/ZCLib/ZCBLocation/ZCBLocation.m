//
//  ZCBLocation.m
//  HuaRong
//
//  Created by user on 16/8/6.
//  Copyright © 2016年 Happy. All rights reserved.
//
#import <CoreLocation/CoreLocation.h>
#import "ZCBLocation.h"

typedef void(^ZCBLocationCallback)(NSError *error, NSDictionary *result);

@interface ZCBLocation () <CLLocationManagerDelegate>
@property (nonatomic,retain) CLLocationManager *locationManager;
@property (nonatomic, copy) ZCBLocationCallback locationCallback;
@end

@implementation ZCBLocation
+ (instancetype)sharedInstance {
    static ZCBLocation *manager = nil;
    static dispatch_once_t predicate;
    dispatch_once(&predicate, ^{
        manager = [[ZCBLocation alloc] init];
    });
    
    return manager;
}

- (void)locate:(void(^)(NSError *error, NSDictionary *result))callback {
    _locationCallback = callback;
    
    if([CLLocationManager locationServicesEnabled]) {
        
        self.locationManager = [[CLLocationManager alloc] init] ;
        self.locationManager.delegate = self;
        
        if([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
            [self.locationManager requestWhenInUseAuthorization]; //使用中授权
        }
        
        [self.locationManager startUpdatingLocation];
    }
}

- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error{
    NSString *errorString;
    [manager stopUpdatingLocation];
    NSLog(@"Error: %@",[error localizedDescription]);
    switch([error code]) {
        case kCLErrorDenied:
            //Access denied by user
            errorString = @"Access to Location Services denied by user";
            //Do something...
            break;
        case kCLErrorLocationUnknown:
            //Probably temporary...
            errorString = @"Location data unavailable";
            //Do something else...
            break;
        default:
            errorString = @"An unknown error has occurred";
            break;
    }
    
    _locationCallback ? _locationCallback(error, nil):nil;
    _locationCallback = nil;

}
-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations{
    //此处locations存储了持续更新的位置坐标值，取最后一个值为最新位置，如果不想让其持续更新位置，则在此方法中获取到一个值之后让locationManager stopUpdatingLocation
    CLLocation *currentLocation = [locations lastObject];
    if (currentLocation) {
        [manager stopUpdatingLocation];
    }
    
    //位置坐标
    CLLocationCoordinate2D coordinate = currentLocation.coordinate;
    NSString *longitude = [NSString stringWithFormat:@"%f", coordinate.longitude]; //经度
    NSString *latitude = [NSString stringWithFormat:@"%f", coordinate.latitude]; //纬度
    NSMutableDictionary *locationResult = @{@"longitude":longitude,
                                            @"latitude":latitude}.mutableCopy;
    
    //根据经纬度反向地理编译出地址信息
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    [geocoder reverseGeocodeLocation:currentLocation completionHandler:^(NSArray *array, NSError *error){
        if (array.count > 0) {
            
            CLPlacemark *placemark = [array objectAtIndex:0];
            
            //获取城市
            [locationResult addEntriesFromDictionary:placemark.addressDictionary];
            
            NSString *city = placemark.addressDictionary[@"City"];
            if (!city){
                //四大直辖市的城市信息无法通过locality获得，只能通过获取省份的方法来获得（如果city为空，则可知为直辖市）
                
                city = placemark.administrativeArea;
            }
            
        } else if (error == nil && [array count] == 0) {
            
            NSLog(@"No results were returned.");
            
        } else if (error != nil) {
            
            NSLog(@"An error occurred = %@", error);
            
        }
        
//        NSLog(@"ZCBLocation result:%@", locationResult);
        _locationCallback ? _locationCallback(error, locationResult):nil;
        _locationCallback = nil;
        
    }];
}
@end
