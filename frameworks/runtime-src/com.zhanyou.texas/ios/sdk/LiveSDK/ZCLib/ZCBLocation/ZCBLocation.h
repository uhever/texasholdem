//
//  ZCBLocation
//  HuaRong
//
//  Created by user on 16/8/6.
//  Copyright © 2016年 Happy. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, ZCBLocationError) {
    ZCBLocationErrorLocationUnknown = 0, //地理位置信息不可用
    ZCBLocationErrorUserDenied = 1, //用户拒绝
    ZCBLocationErrorNoNetwork = 2, //没有网络
};

@interface ZCBLocation : NSObject
+ (instancetype)sharedInstance;

- (void)locate:(void(^)(NSError *error, NSDictionary *result))callback;
@end

/* 使用方法如下
 result中的字段信息如下
 City = "北京市";
 Country = "中国";
 CountryCode = CN;
 FormattedAddressLines =     (
 "中国北京市海淀区花园路街道学院路51号"
 );
 Name = "首享科技大厦";
 State = "北京市";
 Street = "学院路51号";
 SubLocality = "海淀区";
 Thoroughfare = "学院路51号";
 latitude = "39.976520";
 longitude = "116.346112";
 
 1.调用方法
 [[ZCBLocation sharedInstance] locate:^(NSError *error, NSDictionary *result) {
    if (error) {
        NSLog(@"获取地理位置错误：%@", error);
        
    } else {
        NSLog(@"地理位置 result:%@, city:%@", result, result[@"City"]);
        
    }
 }];
 
 2.请注意：需要再Info.plist文件中添加两个key
    <key>NSLocationUsageDescription</key>
	<string>我们需要通过您的地理位置信息获取您周边的相关数据</string>
	<key>NSLocationWhenInUseUsageDescription</key>
	<string>我们需要通过您的地理位置信息获取您周边的相关数据</string>
*/
