//
//  ZCPayItem.h
//  ZCLibProjectDEV
//
//  Created by Happy on 8/28/16.
//  Copyright © 2016 Happy. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString *const ZCPayErrorDomain;

typedef NS_ENUM(NSInteger, ZCPayErrorCode) {
    ZCPayErrorCodeFailure = 100, //购买失败
    ZCPayErrorCodeTimeout //购买操作超时
};

//支付方式
typedef NS_ENUM(NSInteger, ZCPayType) {
    ZCPayTypeApple, //苹果支付
    ZCPayTypeAliPay, //Ping++支付宝支付
    ZCPayTypeWeiXin, //Ping++微信支付, 使用微信支付必须要求用户安装微信客户端
    ZCPayTypeAipppay = 10, //爱贝支付
    ZCPayTypeOther
};

@interface ZCPayItem : NSObject
// 必须参数
@property (nonatomic, assign) ZCPayType payType; //支付方式
@property (nonatomic, copy) NSString *goodIndex; //从1开始
@property (nonatomic, copy) NSString *goodType; //商品类型（默认为0）
@property (nonatomic, copy) NSString *payCallback; //支付回调地址

// 需要创建预订单参数
@property (nonatomic, copy) NSString *token;

// 不需要创建预订单参数
@property (nonatomic, copy) NSString *orderId; //订单OrderId
@property (nonatomic, copy) NSString *goodName; //商品名称
@property (nonatomic, copy) NSString *goodPrice; //商品价格，单位分

//服务端端口号，不需要传空即可
@property (nonatomic, copy) NSString *serverPoker;
@property (nonatomic, assign) BOOL isLookupOrderStatus; //是否查询订单状态
@end
