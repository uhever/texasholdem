//
//  LGActionSheet.h
//  TestForProject
//
//  Created by Happy on 9/9/16.
//  Copyright © 2016 ZhanCheng. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol LGActionSheetDelegate <NSObject>

- (void)actionSheetDidSelected:(NSInteger)index;

@end

@interface LGActionSheet : UIControl
@property (nonatomic, copy) NSString *title;
@property (nonatomic, weak) id<LGActionSheetDelegate>delegate;

- (void)addButtonTitles:(NSArray *)btnTitles btnIcons:(NSArray *)btnIcons;
- (void)addButtonTitles:(NSArray *)btnTitles btnIcons:(NSArray *)btnIcons btnTags:(NSArray *)btnTags;
- (void)showInView:(UIView *)superView;
@end
