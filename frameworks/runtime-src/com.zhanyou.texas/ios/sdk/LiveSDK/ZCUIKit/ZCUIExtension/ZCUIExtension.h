//
//  ZCUIExtension.h
//  HuaRong
//
//  Created by Happy on 9/1/16.
//  Copyright © 2016 Happy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface ZCUIExtension : NSObject

@end


@interface UIButton (Extension)
/* 设置好Button的宽高，设置title和image后，调用如下两个方法布局
 UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
 [btn setFrame:CGRectMake(0, TopEdgeInset + (btnHeight + ySpace)*index, functionViewWidth, btnHeight)];
 
 [btn setTitle:self.normalTitleArr[index] forState:UIControlStateNormal];
 
 [btn setImage:[UIImage imageNamed:self.normalImageArr[index]] forState:UIControlStateNormal];
 [btn setImage:[UIImage imageNamed:self.selectImageArr[index]] forState:UIControlStateSelected];
 */
- (void)adjustHorizontalAlignment:(CGFloat)leftSpace space:(CGFloat)space;

- (void)adjustVerticalAlignment:(CGFloat)topSpace space:(CGFloat)space;
@end
