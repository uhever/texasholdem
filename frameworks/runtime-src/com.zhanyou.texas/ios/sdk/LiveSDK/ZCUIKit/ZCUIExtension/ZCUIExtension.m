//
//  ZCUIExtension.m
//  HuaRong
//
//  Created by Happy on 9/1/16.
//  Copyright © 2016 Happy. All rights reserved.
//

#import "ZCUIExtension.h"

@implementation ZCUIExtension

@end


@implementation UIButton (Extension)
- (void)adjustHorizontalAlignment:(CGFloat)leftSpace space:(CGFloat)space {
    [self setImageEdgeInsets:UIEdgeInsetsMake(0, -self.frame.size.width + self.imageView.frame.size.width + self.titleLabel.frame.size.width + leftSpace, 0, 0)];
    [self setTitleEdgeInsets:UIEdgeInsetsMake(0, self.imageEdgeInsets.left + space, 0, 0)];
}

- (void)adjustVerticalAlignment:(CGFloat)topSpace space:(CGFloat)space {
    [self setImageEdgeInsets:UIEdgeInsetsMake(-self.frame.size.height + self.imageView.frame.size.height + self.titleLabel.frame.size.height + topSpace, 0, 0, 0)];
    [self setTitleEdgeInsets:UIEdgeInsetsMake(self.imageEdgeInsets.top + space, 0, 0, 0)];
}
@end