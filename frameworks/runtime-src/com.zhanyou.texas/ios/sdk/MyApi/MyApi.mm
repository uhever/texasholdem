//
//  MyPPApi.m
//  Sanguo
//
//  Created by hubin on 14-8-30.
//
//

#import "MyApi.h"
#import "MyConstant.h"

#import "AppController.h"

#import "scripting/lua-bindings/manual/platform/ios/CCLuaObjcBridge.h"

#import "scripting/lua-bindings/manual/CCLuaEngine.h"

#import "scripting/lua-bindings/manual/CCLuaBridge.h"
#import "cocos2d.h"

@implementation MyApi

+ (void)getDeviceInfo:(NSDictionary *) dic {
    NSMutableDictionary* allInfo = [DeviceInfo getAllInfo];
    
    NSString *mac = [allInfo objectForKey:@"mac"];
    NSString *idfa = [allInfo objectForKey:@"idfa"];
    NSString *deviceToken = [allInfo objectForKey:@"deviceToken"];
    NSString *idfaEnable = [allInfo objectForKey:@"idfaEnable"];
    NSString *openudid = [allInfo objectForKey:@"openudid"];
    NSString *odin = [allInfo objectForKey:@"odin"];
    NSString *os_version = [allInfo objectForKey:@"os_version"];
    NSString *jailbreaked = @"0";
    NSString *ssid = [allInfo objectForKey:@"ssid"];
    NSString *model = [allInfo objectForKey:@"model"];
    
    
    int handlerID = [[dic objectForKey:@"callback"] intValue];
    
    if (handlerID) {
        cocos2d::LuaObjcBridge::pushLuaFunctionById(handlerID);
        // 2. 将需要传递给 Lua function 的参数放入 Lua stack
        cocos2d::LuaValueDict item;
        item["deviceInfo"] = cocos2d::LuaValue::stringValue([[NSString stringWithFormat:@"%@=%@=%@=%@=%@=%@=%@=%@=%@=%@", mac, idfa, deviceToken, idfaEnable, openudid, odin, os_version, jailbreaked, ssid, model] UTF8String]);
        cocos2d::LuaObjcBridge::getStack()->pushLuaValueDict(item);
        
        // 3. 执行 Lua function
        cocos2d::LuaObjcBridge::getStack()->executeFunction(1);
    }
    
}

+ (void)registerCallBackByName:(NSDictionary * ) functions{
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSDictionary * dict =  [userDefaults dictionaryForKey:@"myFunctions"];
    NSMutableDictionary * dic;
    if (dict) {
        
        dic = [NSMutableDictionary dictionaryWithDictionary:dict];
        
    }else{
        
        dic = [NSMutableDictionary dictionaryWithCapacity:0];
        
    }
    
    for (int i = 0; i < functions.allKeys.count; i ++ ) {
        
        [dic setObject:[functions objectForKey:functions.allKeys[i] ] forKey:functions.allKeys[i] ];
        
    }
    
    //存储时，除NSNumber类型使用对应的类型意外，其他的都是使用setObject:forKey:
    [userDefaults setObject:dic forKey:@"myFunctions"];
    [userDefaults synchronize];
}

+ (void) removeCallBackByName:(NSDictionary * ) functionName{
    
    NSUserDefaults *userDefaultes = [NSUserDefaults standardUserDefaults];
    NSDictionary * dic =  [userDefaultes dictionaryForKey:@"myFunctions"];
    
    if (dic) {
        
        if ( [dic objectForKey:[functionName objectForKey:@"remove"]]) {
            int handlerID = [[dic objectForKey:[functionName objectForKey:@"remove"]]intValue];
            cocos2d::LuaObjcBridge::releaseLuaFunctionById(handlerID);
        }
        
        
    }
    
}

+ (void)callLua: (NSString *) callbackName  data:(NSDictionary *)params {
    
    NSUserDefaults *userDefaultes = [NSUserDefaults standardUserDefaults];
    NSDictionary * dic =  [userDefaultes dictionaryForKey:@"myFunctions"];
    int handlerID = [[dic objectForKey: callbackName] intValue];
    
    
    if (handlerID) {
        cocos2d::LuaObjcBridge::pushLuaFunctionById(handlerID);
        // 2. 将需要传递给 Lua function 的参数放入 Lua stack
        cocos2d::LuaValueDict item;
        
        for(NSString *k in params) {    // 正确的字典遍历方式
            NSString *v = [params objectForKey:k];
            item[[k UTF8String]] = cocos2d::LuaValue::stringValue([v UTF8String]);
        }
        
        cocos2d::LuaObjcBridge::getStack()->pushLuaValueDict(item);
        
        // 3. 执行 Lua function
        cocos2d::LuaObjcBridge::getStack()->executeFunction(1);
    }
}


// OC主动回调
+ (void)registInitiativeEvent {
    
    //网络监听管理类
    NetStatusManager *mgr = [NetStatusManager manager];
    mgr.NetStatusChange = ^(NetworkStatus status){
        
        NSDictionary *dict = @{@"status" : @"1", @"networkStatus" : @(status).stringValue};
        [MyApi callLua:@"networkStatusChange" data:dict];
        
        
    };
    
    //    [self checkoutNetworkStatus:mgr.status];
    
    //电池电量回调管理类
    DeviceBatteryManager *batteryMgr = [DeviceBatteryManager batteryManager];
    batteryMgr.batteryChange = ^(float level){
        
        NSDictionary *dict = @{@"status" : @"1", @"batteryLevel" : @(level).stringValue};
        [MyApi callLua:@"batteryChange" data:dict];
        
    };
    
    
    //蜂窝数据信号量  （私有API,上线AppStore可能会被拒）
    SignalStrengthManager *signalStrengthMgr = [SignalStrengthManager manager];
    [signalStrengthMgr startObserveSignalStrength];
    signalStrengthMgr.SignalStrengthChange = ^(float signalStrengthLevel){
        NSDictionary *dict = @{@"status" : @"1", @"signalStrengthLevel" : @(signalStrengthLevel).stringValue};
        [MyApi callLua:@"signalStrengthChange" data:dict];
        
        
    };
    
    
    //录音被中断回调
    RecorderManager *recorderMgr = [RecorderManager manager];
    recorderMgr.interruptionBlock = ^(){
        
        NSDictionary *dict = @{@"status" : @"-1"};
        [MyApi callLua:@"recorderResult" data:dict];
    };
}

// 数据统计 events
+ (void)sendDataEvent:(NSDictionary * ) params {
    
    [(AppController *)[UIApplication sharedApplication].delegate sendDataEvent:params];
}

#pragma mark Live Start

+ (void)loginSDKLive:(NSDictionary *)params {
    NSLog(@"lua doSDKLogin->%@", params);
    
    int loginType = [params[@"method"] intValue];
    if (loginType == 1) {
        // 游客登录
        [[ZBManager sharedInstance] loginInVisitor:^(NSError *error, NSDictionary *result) {
            [self noticeLuaLoginResult:error result:result];
            
        }];
        
    } else if (loginType == 2) {
        // 手机登录
        NSString *phoneNum = params[@"phoneNumber"];
        NSString *authCode = params[@"authcode"];
        
        [[ZBManager sharedInstance] loginInPhone:phoneNum authCode:authCode callback:^(NSError *error, NSDictionary *result) {
            [self noticeLuaLoginResult:error result:result];
            
        }];
        
    } else if (loginType == 3) {
        // 微信登录
        [[ZBManager sharedInstance] loginInWeiXin:^(NSError *error, NSDictionary *result) {
            [self noticeLuaLoginResult:error result:result];
        }];
    }
}
+ (void)noticeLuaLoginResult:(NSError *)error result:(NSDictionary *)result {
    NSLog(@"ZBSDK 登录返回参数: error->%@, result->%@", error, result);
    
    if (error) {
        [MyApi callLua:@"loginResult" data:@{@"status":@"0"}];
        
    } else {
        
        NSDictionary *zcDict = [result objectForKey:ZBLoginResultKeyForLogin];
 
        NSString *sdkUid = zcDict[@"uId"] ? zcDict[@"uId"] : @"";
        NSString *sdkAuthkey = zcDict[@"authkey"] ? zcDict[@"authkey"] : @"";
        NSString *sdkSid = zcDict[@"session"] ? zcDict[@"session"] : @"";
        NSString *appid = [ZBSDKConfigure sharedInstance].appid;

        
        NSDictionary *extraDict = [result objectForKey:ZBLoginResultKeyForProfile];
        
        NSString *name = extraDict[@"nickname"] ? extraDict[@"nickname"] : @""; //微信name
        NSString *icon = extraDict[@"icon"] ? extraDict[@"icon"] : @""; //微信icon
        NSString *sex = extraDict[@"sex"] ? extraDict[@"sex"] : @""; //微信sex
        
        
        [MyApi callLua:@"loginResult" data:@{@"status":@"1",
                                                 @"sdkUid":sdkUid,
                                                 @"sdkAuthkey":sdkAuthkey,
                                                 @"sdkSid":sdkSid,
                                                 @"appid":appid,
                                                 @"name":name,
                                                 @"icon":icon,
                                                 @"sex":sex}];
    }
}

+ (void)doSdkPay:(NSDictionary *)params {
    NSLog(@"lua doSdkPay->%@", params);
    
    ZCPayItem *payItem = [[ZCPayItem alloc] init];
    payItem.payType = ZCPayTypeApple; //支付方式 "payType"
    payItem.goodIndex = params[@"productId"]; // @"商品索引，从1开始"
    payItem.goodName = params[@"productName"];// @"商品名称";
    payItem.goodPrice = [NSString stringWithFormat:@"%@", @([params[@"amount"] intValue] * 100)]; // @"商品价格"; //单位分
    payItem.orderId = params[@"orderId"]; // @"订单orderId";
    payItem.payCallback = params[@"callbackUrl"]; // 回调地址
    
    [[ZBManager sharedInstance] pay:payItem completion:^(NSError *error, NSDictionary *payResult) {
        if (error) {
            // 支付失败
            NSString *errorMsg = error.localizedDescription; //失败原因
            NSLog(@"OC doSdkPay->%@", errorMsg);
            
            NSDictionary *dict = @{@"status":@"0"};
            [MyApi callLua:@"payResult" data:dict];
            
        } else {
            // 支付成功
            NSString *orderId = payResult[ZCPayOrderIdKey]; //订单Id
            
            NSDictionary *dict = @{@"status":@"1", @"orderId":orderId};
            [MyApi callLua:@"payResult" data:dict];
            
        }
    }];
    

}


+ (void)sendAuthCode:(NSDictionary *)params {
    
    [[ZBManager sharedInstance] loginSendAuthCode:params[@"phoneNumber"] callback:^(NSError *error, NSDictionary *result) {
        
        if (error) {
            [MyApi callLua:@"authCodeResult" data:@{@"status":@"0"}];
            
        } else {
            [MyApi callLua:@"authCodeResult" data:@{@"status":@"1"}];
            
        }
    }];
}


+ (void)share:(NSDictionary * ) params {
    
    ZCBShareContent *content = [[ZCBShareContent alloc] init];
    
    NSString *contentType = [params objectForKey:@"contentType"];
    int wxScene = [[params objectForKey:@"wxScene"] intValue]; //0聊天 1朋友圈 2收藏
    
    content.wxScene = wxScene;
    
    if ([contentType isEqualToString:@"text"]) { //分享文本
        content.contentType = ZCBShareContentTypeText;
        content.contentText = [params objectForKey:@"contentText"];
        
        
    } else if ([contentType isEqualToString:@"image"]) { //分享image
        content.contentType = ZCBShareContentTypeImage;
        content.imageURL = [params objectForKey:@"imageURL"];
        
        
    } else if ([contentType isEqualToString:@"link"]) { //分享link
        content.contentType = ZCBShareContentTypeLink;
        content.contentURL = [params objectForKey:@"contentURL"]; // fb weixin
        content.contentTitle = [params objectForKey:@"contentTitle"]; // fb weixin
        content.contentDescription = [params objectForKey:@"contentDescription"]; // fb

    }
    
    [[ZBManager sharedInstance] share:content shareType:ZCBShareTypeWeiXin callback:^(NSError *error, NSDictionary *result) {
        NSLog(@"error:%@, result:%@", error, result);
        
        if (error) {
            // 分享失败

            NSDictionary *dict = @{@"status":@"0"};
            [MyApi callLua:@"shareResult" data:dict];
            
        } else {
            // 分享成功
            
            NSDictionary *dict = @{@"status":@"1"};
            [MyApi callLua:@"shareResult" data:dict];
            
        }

        
    }];
}


+ (void)sdkGetLocation:(NSDictionary * ) params {
    /* result中的字段信息如下
     City = "北京市";
     Country = "中国";
     CountryCode = CN;
     FormattedAddressLines =     (
     "中国北京市海淀区花园路街道学院路51号"
     );
     Name = "首享科技大厦";
     State = "北京市";
     Street = "学院路51号";
     SubLocality = "海淀区";
     Thoroughfare = "学院路51号";
     latitude = "39.976520";
     longitude = "116.346112";
     */
    [[ZCBLocation sharedInstance] locate:^(NSError *error, NSDictionary *result) {
        if (error) {
            NSLog(@"获取地理位置错误：%@", error);
            NSString *errorCode = @"0";
            if (error.code == ZCBLocationErrorUserDenied) {
                // 用户拒绝
                errorCode = @"-1";
            } else if (error.code == ZCBLocationErrorNoNetwork) {
                // 无网络
                errorCode = @"-2";
            } else if (error.code == ZCBLocationErrorLocationUnknown) {
                // 地理信息不可用
                errorCode = @"-3";
            } else {
                // 其它错误信息
                errorCode = @"0";
            }
            NSDictionary *dict = @{@"status":errorCode};
            [MyApi callLua:@"locationResult" data:dict];
            
        } else {
            NSLog(@"地理位置 result:%@, city:%@", result, result[@"City"]);
            
            NSArray *formattedAddressLinesArr = result[@"FormattedAddressLines"];
            NSString *formattedAddress = formattedAddressLinesArr.firstObject ? formattedAddressLinesArr.firstObject : @"";
            
            NSString *City = result[@"City"] ? result[@"City"] : @"";
            NSString *Country = result[@"Country"] ? result[@"Country"] : @"";
            NSString *CountryCode = result[@"CountryCode"] ? result[@"CountryCode"] : @"";
            NSString *Name = result[@"Name"] ? result[@"Name"] : @"";
            NSString *State = result[@"State"] ? result[@"State"] : @"";
            NSString *Street = result[@"Street"] ? result[@"Street"] : @"";
            NSString *SubLocality = result[@"SubLocality"] ? result[@"SubLocality"] : @"";
            NSString *Thoroughfare = result[@"Thoroughfare"] ? result[@"Thoroughfare"] : @"";
            NSString *latitude = result[@"latitude"] ? result[@"latitude"] : @"";
            NSString *longitude = result[@"longitude"] ? result[@"longitude"] : @"";
            
            
            NSDictionary *dict = @{@"status":@"1", @"City":City, @"Country":Country, @"CountryCode":CountryCode, @"Name":Name, @"State":State, @"Street":Street, @"SubLocality":SubLocality, @"Thoroughfare":Thoroughfare, @"latitude":latitude, @"longitude":longitude, @"formattedAddress":formattedAddress};
            
            [MyApi callLua:@"locationResult" data:dict];
        }
    }];
}

// 主动获取网络状态
+ (void)sdkGetNetworkStatus:(NSDictionary * ) params {
    NetStatusManager *mgr = [NetStatusManager manager];
    
    NSDictionary *dict = @{@"status" : @"1", @"networkStatus" : @(mgr.status).stringValue};
    [MyApi callLua:@"networkStatusChange" data:dict];

}

// 主动获取电量状态
+ (void)sdkGetBatteryLevel:(NSDictionary * ) params {
     DeviceBatteryManager *batteryMgr = [DeviceBatteryManager batteryManager];
    
    NSDictionary *dict = @{@"status" : @"1", @"batteryLevel" : @(batteryMgr.currentBatteryLevel).stringValue};
    [MyApi callLua:@"batteryChange" data:dict];
    
}

// 主动获取蜂窝网络信号量
+ (void)sdkGetSignalStrengthLevel:(NSDictionary * ) params {
    SignalStrengthManager *signalStrengthMgr = [SignalStrengthManager manager];

    NSDictionary *dict = @{@"status" : @"1", @"signalStrengthLevel" : @(signalStrengthMgr.getCurrentSignalStrength).stringValue};
    [MyApi callLua:@"signalStrengthChange" data:dict];
    
}

// 获取通讯录列表
+ (void)sdkGetContact:(NSDictionary * ) params {
    
    PhoneNumManager *mgr = [[PhoneNumManager alloc]init];
    [mgr loadAllPhoneNumWithComplete:^(NSArray<ContactModel *> *contactModels,
                                       NSArray<NSString *> *phoneNumbers,
                                       BOOL isSuccess) {
        if (isSuccess) {
            
            
            NSMutableArray *contactArray = [[NSMutableArray alloc] init];
            
            for (ContactModel *model in contactModels) {

                NSArray *contact = [[NSArray alloc] initWithObjects:model.name, model.phone, model.firstCharater, nil];
                
                [contactArray addObject:contact];

            }
            
            
            NSError *error = nil;
            NSData *jsonData = [NSJSONSerialization dataWithJSONObject:contactArray options:NSJSONWritingPrettyPrinted error:&error];
            
            NSString *jsonString;
            if ([jsonData length] > 0 && error == nil){
                jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
            }else{
                jsonString = @"[]";
            }
            
            NSDictionary *dict = @{@"status" : @"1", @"contact" : jsonString};
            [MyApi callLua:@"contactResult" data:dict];
            
        }else{
            NSDictionary *dict = @{@"status" : @"0"};
            [MyApi callLua:@"contactResult" data:dict];
        }
    }];
    
}

// 开始录音
+ (void)sdkStartRecorder:(NSDictionary * ) params {
    RecorderManager *recorderMgr = [RecorderManager manager];
    
    [recorderMgr startRecorderWithSuccess:^(BOOL isSuccess) {
        
        if (isSuccess) {
            NSDictionary *dict = @{@"status" : @"1", @"event" : @"start"};
            [MyApi callLua:@"recorderResult" data:dict];
            
        }else{
            NSDictionary *dict = @{@"status" : @"0", @"event" : @"start"};
            [MyApi callLua:@"recorderResult" data:dict];
        }
        
    }];
    
}

// 结束录音
+ (void)sdkStopRecorder:(NSDictionary * ) params {
    RecorderManager *recorderMgr = [RecorderManager manager];
    
    [recorderMgr stopRecorder];
    
    recorderMgr.recorderComplete = ^(BOOL isSuccess){
        
        if (isSuccess) {
            NSDictionary *dict = @{@"status" : @"1", @"event" : @"stop"};
            [MyApi callLua:@"recorderResult" data:dict];
        }else{
            NSDictionary *dict = @{@"status" : @"0", @"event" : @"stop"};
            [MyApi callLua:@"recorderResult" data:dict];
        }
    };
    
}


// 手机震动
+ (void)sdkPhoneShake:(NSDictionary * ) params {
    [ZCDeviceInfo audioServicesPlaySystemSoundWithCompletion:^{
//        NSDictionary *dict = @{@"status" : @"1"};
//        [MyApi callLua:@"phoneShakeResult" data:dict];
    }];
}

// 系统相册或者相机读取图片
+ (void)sdkFetchImage:(NSDictionary * ) params {
    
//    NSString *contentType
    int type = [[params objectForKey:@"type"] intValue]; //1.玩家头像 2.俱乐部头像
    int sourceType = [[params objectForKey:@"sourceType"] intValue]; //0从相册，1从相机读取
    int width = [[params objectForKey:@"width"] intValue]; // 宽度
    
    
    // 系统相册或者相机读取图片
    [[ZCDeviceInfo sharedInstance] fetchImageFromSystem:sourceType viewController:nil callback:^(UIImage *image) {
        
        //        NSData *originalImageData = UIImagePNGRepresentation(image); //原始图片的二进制数据
        
        //压缩图片
        NSData *imageData = [ZCUtils imageCompress:image targetWidth:width compression:0.8];
        
        NSDate* timeStmp = [NSDate dateWithTimeIntervalSinceNow:0];
        NSTimeInterval timeInterval=[timeStmp timeIntervalSince1970]*1000;
        NSString *timeString = [NSString stringWithFormat:@"%f", timeInterval];
        
        //保存头像到沙盒路径
        NSString *documentPath = [ZCLocalData pathForDocuments];
        NSString *iconPath = [NSString stringWithFormat:@"%@/zcsdk_Icon_%@.jpg", documentPath, timeString]; //头像路径
        [imageData writeToFile:iconPath atomically:YES];
        
        NSDictionary *dict = @{@"status" : @"1", @"iconPath" : iconPath, @"type" : @(type).stringValue};
        [MyApi callLua:@"fetchImageResult" data:dict];
        
    }];

}


// 上传图片
+ (void)sdkUploadImage:(NSDictionary * ) params {
    
    
    NSString *uploadUrl = [params objectForKey:@"uploadUrl"]; // 服务器地址
    
    NSString *server_url = [params objectForKey:@"server_url"]; // POST消息
    NSString *token = [params objectForKey:@"token"]; // POST消息
    NSString *server_id = [params objectForKey:@"server_id"]; // POST消息
    NSString *type = [params objectForKey:@"type"]; // POST消息(上传类型1.头像 2.俱乐部头像)
    
    NSString *userfile = [params objectForKey:@"userfile"]; // form表单字段
    
    UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfFile:userfile]]; // 图片Data
    
    
    [ZCLiveRequest uploadPicture:uploadUrl parameters:@{@"token":token, @"userfile":userfile, @"server_url":server_url, @"server_id":server_id, @"type":type} imagePara:@{@"uploadKey":@"userfile", @"image":image, @"fileName":userfile} progress:^(NSProgress *progress) {
        
//        int value = (1.0 * progress.completedUnitCount / progress.totalUnitCount) * 100;
//        
//        NSDictionary *dict = @{@"status" : @"2", @"progress" : @(value)};
//        [MyApi callLua:@"uploadImageResult" data:dict];
        
    } success:^(NSDictionary *dic) {
        NSLog(@"上传图片成功:%@", dic);

        NSString *iconSmall = @"";
//        NSString *type = @"";
        if (dic){
            iconSmall = dic[@"icon_small"] ? dic[@"icon_small"] : @"";
//            type = dic[@"type"] ? dic[@"type"] : @"";
        }
        
        NSDictionary *dict = @{@"status" : @"1", @"icon_small" : iconSmall, @"type" : type};
        [MyApi callLua:@"uploadImageResult" data:dict];

    } failure:^(NSError *error) {
        NSDictionary *dict = @{@"status" : @"0"};
        [MyApi callLua:@"uploadImageResult" data:dict];
        
    }];
}


// 显示客服中心
+ (void)showServiceCenter:(NSDictionary * ) params {
    
    [(AppController *)[UIApplication sharedApplication].delegate showServiceCenter:params];

}


#pragma mark Live End


+ (NSString*)calculateStringSize:(NSString *)str fontName:(NSString *)fontName fontNumber:(NSString *)fontNumber {
    NSArray *listItems = [str componentsSeparatedByString: @"\n"];
    CGSize dim = CGSizeZero;
    CGSize textRect = CGSizeZero;
    textRect.width = 0x7fffffff;
    textRect.height = 0x7fffffff;
    
    
    UIFont *font = [UIFont fontWithName:fontName size:[fontNumber floatValue]];
    
    for (NSString *s in listItems)
    {
        CGSize tmp = [s sizeWithFont:font constrainedToSize:textRect];
        
        if (tmp.width > dim.width)
        {
            dim.width = tmp.width;
        }
        
        dim.height += tmp.height;
    }
    
    dim.width = ceilf(dim.width);
    dim.height = ceilf(dim.height);
    
    dim.width = (int)(dim.width / 2) * 2 + 2;
    dim.height = (int)(dim.height / 2) * 2 + 2;
    
    return [NSString stringWithFormat:@"%.0f,%.0f", dim.width, dim.height];

}

// 横屏
+ (void)landscapAction:(NSDictionary * ) params {
    [self interfaceOrientation:UIDeviceOrientationLandscapeLeft];
}

// 竖屏
+ (void)portraitAction:(NSDictionary * ) params {
    [self interfaceOrientation:UIDeviceOrientationPortrait];
}

+ (void)interfaceOrientation:(UIDeviceOrientation)orientation
{
    
    [MyConstant setClickRotate:YES];
    
    /*
        非arc下
     */
//    [[UIDevice currentDevice] performSelector:@selector(setOrientation:) withObject:@(orientation)];
    
    
    /*
     arc下
     */
    if ([[UIDevice currentDevice] respondsToSelector:@selector(setOrientation:)]) {
        SEL selector = NSSelectorFromString(@"setOrientation:");
        NSInvocation *invocation = [NSInvocation invocationWithMethodSignature:[UIDevice instanceMethodSignatureForSelector:selector]];
        [invocation setSelector:selector];
        [invocation setTarget:[UIDevice currentDevice]];
        int val = orientation;
        [invocation setArgument:&val atIndex:2];
        [invocation invoke];
    }
    
    [MyConstant setClickRotate:NO];
    
}


@end
