//
//  MyPPApi.h
//  Sanguo
//
//  Created by hubin on 14-8-30.
//
//

#import <Foundation/Foundation.h>

#import <Cocoa/Cocoa.h>
@interface MyApi : NSObject
+ (void)callLua: (NSString *) callbackName  data:(NSDictionary *)params;
@end
