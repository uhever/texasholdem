varying vec4 v_fragmentColor;
varying vec2 v_texCoord;
uniform float high;
uniform int type;
void main()
{
    if(type == 0)
        gl_FragColor = texture2D(CC_Texture0, v_texCoord) * high * v_fragmentColor;
    else
    {
        vec4 color =texture2D(CC_Texture0, v_texCoord) * v_fragmentColor;
        float gray = dot(color.rgb, vec3(0.299, 0.587, 0.114));
        gl_FragColor = vec4(gray, gray, gray, color.a);
    }
}

