--[[
    基本配置
]]
-- 是否开启log
G_setDebug(false)


-- 1正式服 0测试服
G_setServer(1)
G_setHotUpdate(true)
-- G_setModuleUpdate(false)
-- G_setHotUpdateAlpha(0)

-------------------以下配置将由程序根据渠道ID自动生成-------------------
-- 发布平台
--[[
    ios          ios-appstore
    ios-com      ios-官网版本
    android-360  安卓-360
]]
APP_PLATFORM   = "ios-zc"
-- 平台名称（打包时自动添加）
APP_PLATFORM_NAME = "应用市场"

-- 渠道ID
CHANNELID      = 101

-- 在热更新系统里注册的APPID
UPDATE_ID      = 4
-- 保持不变(后台初始)
CLIENT_VERSION = "20170325001"


-- 大版本号
VERSION_NAME   = "1.0.1" -- "1.0"

-- 提包时后台更新版本
BASE_LOCAL_VERSION = "20170325001"

-- 随更新后改变
LOCAL_VERSION  =  BASE_LOCAL_VERSION .. ""
SERVER_VERSION = "0"

-- 版本缓存、更新目录后缀(更新客户端必须更换) V2以后启用
--[[
    /download1.0.2/
    local_version1.0.2
]]
VERSION_CACHE_ID = VERSION_NAME .. ""



--[[
    大量的控制条件
]]

-- 登陆模式
kLoginModeNormal  = 0 -- 普通单服
kLoginModeZCSDK   = 1 -- 展程SDK
kLoginModeInApp   = 2 -- 直播内跳转
kLoginModeLiveSDK = 3 -- 直播SDK

LOGIN_MODE = kLoginModeLiveSDK -- 默认登陆

-- 广电审核模式 1.斗地主 2.麻将
PRC_REVIEW = false -- 广电审核模式(boolean/number)

-- 是否关闭视频
IS_NO_VIDEO = true
-- 是否开启安卓下载器
IS_ANDROID_DOWNLOADER = true
-- 启动游戏时是否需要加载额外资源
IS_EXTRARESOURCES = false

-- 展程数据统计配置
ZC_TRACK_CFG = {
    product = "dzpk",
    ios = {
        appid = "1216162143",
        appkey = "193a485424e377303b7f9314aeabe638",
    },
    android = {
        appid = "dzpk.android.iosjb",
        appkey = "f354f1ec60200814099f7ac31a94bdbb",
    },
}


-- 调试登录（指定用某用户的身份登录到某服务器）
-- GM_LOGIN_UID = ""
