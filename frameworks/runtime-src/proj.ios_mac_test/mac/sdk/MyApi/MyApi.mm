//
//  MyPPApi.m
//  Sanguo
//
//  Created by hubin on 14-8-30.
//
//

#import "MyApi.h"

#import "scripting/lua-bindings/manual/platform/ios/CCLuaObjcBridge.h"

#import "scripting/lua-bindings/manual/CCLuaEngine.h"

#import "scripting/lua-bindings/manual/CCLuaBridge.h"
#import "cocos2d.h"


#import <AppKit/AppKit.h>



@implementation MyApi

+ (void)getDeviceInfo:(NSDictionary *) dic {
//    NSMutableDictionary* allInfo = [DeviceInfo getAllInfo];
//    
//    NSString *mac = [allInfo objectForKey:@"mac"];
//    NSString *idfa = [allInfo objectForKey:@"idfa"];
//    NSString *deviceToken = [allInfo objectForKey:@"deviceToken"];
//    NSString *idfaEnable = [allInfo objectForKey:@"idfaEnable"];
//    NSString *openudid = [allInfo objectForKey:@"openudid"];
//    NSString *odin = [allInfo objectForKey:@"odin"];
//    NSString *os_version = [allInfo objectForKey:@"os_version"];
//    NSString *jailbreaked = @"0";
//    NSString *ssid = [allInfo objectForKey:@"ssid"];
//    NSString *model = [allInfo objectForKey:@"model"];
//    
//    
//    int handlerID = [[dic objectForKey:@"callback"] intValue];
//    
//    if (handlerID) {
//        cocos2d::LuaObjcBridge::pushLuaFunctionById(handlerID);
//        // 2. 将需要传递给 Lua function 的参数放入 Lua stack
//        cocos2d::LuaValueDict item;
//        item["deviceInfo"] = cocos2d::LuaValue::stringValue([[NSString stringWithFormat:@"%@=%@=%@=%@=%@=%@=%@=%@=%@=%@", mac, idfa, deviceToken, idfaEnable, openudid, odin, os_version, jailbreaked, ssid, model] UTF8String]);
//        cocos2d::LuaObjcBridge::getStack()->pushLuaValueDict(item);
//        
//        // 3. 执行 Lua function
//        cocos2d::LuaObjcBridge::getStack()->executeFunction(1);
//    }
    
}


+ (void)registerCallBackByName:(NSDictionary * ) functions{
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSDictionary * dict =  [userDefaults dictionaryForKey:@"myFunctions"];
    NSMutableDictionary * dic;
    if (dict) {
        
        dic = [NSMutableDictionary dictionaryWithDictionary:dict];
        
    }else{
        
        dic = [NSMutableDictionary dictionaryWithCapacity:0];
        
    }
    
    for (int i = 0; i < functions.allKeys.count; i ++ ) {
        
        [dic setObject:[functions objectForKey:functions.allKeys[i] ] forKey:functions.allKeys[i] ];
        
    }
    
    //存储时，除NSNumber类型使用对应的类型意外，其他的都是使用setObject:forKey:
    [userDefaults setObject:dic forKey:@"myFunctions"];
    [userDefaults synchronize];
}

+ (void) removeCallBackByName:(NSDictionary * ) functionName{
    
    NSUserDefaults *userDefaultes = [NSUserDefaults standardUserDefaults];
    NSDictionary * dic =  [userDefaultes dictionaryForKey:@"myFunctions"];
    
    if (dic) {
        
        if ( [dic objectForKey:[functionName objectForKey:@"remove"]]) {
            int handlerID = [[dic objectForKey:[functionName objectForKey:@"remove"]]intValue];
            cocos2d::LuaObjcBridge::releaseLuaFunctionById(handlerID);
        }
        
        
    }
    
}

+ (void)callLua: (NSString *) callbackName  data:(NSDictionary *)params {
    
    NSUserDefaults *userDefaultes = [NSUserDefaults standardUserDefaults];
    NSDictionary * dic =  [userDefaultes dictionaryForKey:@"myFunctions"];
    int handlerID = [[dic objectForKey: callbackName] intValue];
    
    
    if (handlerID) {
        cocos2d::LuaObjcBridge::pushLuaFunctionById(handlerID);
        // 2. 将需要传递给 Lua function 的参数放入 Lua stack
        cocos2d::LuaValueDict item;
        
        for(NSString *k in params) {    // 正确的字典遍历方式
            NSString *v = [params objectForKey:k];
            item[[k UTF8String]] = cocos2d::LuaValue::stringValue([v UTF8String]);
        }
        
        cocos2d::LuaObjcBridge::getStack()->pushLuaValueDict(item);
        
        // 3. 执行 Lua function
        cocos2d::LuaObjcBridge::getStack()->executeFunction(1);
    }
}



+ (NSString*)calculateStringSize:(NSString *)str fontName:(NSString *)fontName fontNumber:(NSString *)fontNumber {
    NSArray *listItems = [str componentsSeparatedByString: @"\n"];
    CGSize dim = CGSizeZero;
    CGSize textRect = CGSizeZero;
    textRect.width = 0x7fffffff;
    textRect.height = 0x7fffffff;
    
    
    NSFont *font = [NSFont fontWithName:fontName size:[fontNumber floatValue]];
    
    for (NSString *s in listItems)
        
    {
//        CGSize tmp = [s sizeWithFont:font constrainedToSize:textRect];
        
        CGSize tmp = [s boundingRectWithSize:textRect options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:font}].size;
//        [s boundingRectWithSize:<#(NSSize)#> options:<#(NSStringDrawingOptions)#> attributes:<#(nullable NSDictionary<NSString *,id> *)#> context:<#(nullable NSStringDrawingContext *)#>]
        
        if (tmp.width > dim.width)
        {
            dim.width = tmp.width;
        }
        
        dim.height += tmp.height;
    }
    
    dim.width = ceilf(dim.width);
    dim.height = ceilf(dim.height);
    
    dim.width = (int)(dim.width / 2) * 2 + 2;
    dim.height = (int)(dim.height / 2) * 2 + 2;
    
    return [NSString stringWithFormat:@"%.0f,%.0f", dim.width, dim.height];
    
    //    return dim;
}

// 横屏
+ (void)landscapAction:(NSDictionary * ) params {
    
    [[NSApplication sharedApplication].delegate changeFrameOrientationToLandscape];
}

// 竖屏
+ (void)portraitAction:(NSDictionary * ) params {
    [[NSApplication sharedApplication].delegate changeFrameOrientationToPortait];
}



@end
