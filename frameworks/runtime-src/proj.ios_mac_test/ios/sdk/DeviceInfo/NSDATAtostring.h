//
//  NSDATAtostring.h
//  pgdemo
//
//  Created by zctech on 14-2-24.
//
//

#import <Foundation/Foundation.h>

@interface NSData(ToNSString)
-(NSString *)ConvertToNSString;

@end
