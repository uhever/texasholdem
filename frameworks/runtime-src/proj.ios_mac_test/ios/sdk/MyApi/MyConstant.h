//
//  MyConstant.h
//  Poker
//
//  Created by zctech-011 on 2016/12/5.
//
//

#import <Foundation/Foundation.h>

@interface MyConstant : NSObject

+ (void)setClickRotate:(bool)clickRotate;
+ (bool)getClickRotate;

+ (void)setRotating:(bool)rotating;
+ (bool)getRotating;

+ (void)setFirstRotate:(bool)firstRotate;
+ (bool)getFitstRotate;

+ (void)setOrentation:(NSString *)orentation;
+ (NSString *)getOrentation;




@end
