//
//  MyPPApi.h
//  Sanguo
//
//  Created by hubin on 14-8-30.
//
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#import <Foundation/NSError.h>
#import <Foundation/NSData.h>
#import <Foundation/NSJSONSerialization.h>

// 设备信息
#import "DeviceInfo.h"

#import "ZCBShareContent.h"
#import "ZCShare.h"
#import "ZCBLocation.h"

#import "PhoneNumManager.h"
#import "NetStatusManager.h"
#import "RecorderManager.h"
#import "DeviceBatteryManager.h"
#import "SignalStrengthManager.h"
#import "ContactModel.h"


@interface MyApi : NSObject
+ (void)callLua: (NSString *) callbackName  data:(NSDictionary *)params;
+ (void)registInitiativeEvent;
+ (NSString*)calculateStringSize:(NSString *)str fontName:(NSString *)fontName fontNumber:(NSString *)fontNumber;
@end
