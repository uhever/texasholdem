//
//  LGActionSheet.m
//  TestForProject
//
//  Created by Happy on 9/9/16.
//  Copyright © 2016 ZhanCheng. All rights reserved.
//

#import "LGActionSheet.h"
#import "ZCUtils.h"

#define ScreenWidth [UIScreen mainScreen].bounds.size.width
#define ScreenHeight [UIScreen mainScreen].bounds.size.height

#define LeftAndRightSpace 10 //左右间距
#define CornerRadius 20 //圆角弧度

#define ShowTime 0.25
#define DismissTime 0.15

#define TitleColor [UIColor grayColor]
#define BtnTitleColor [ZCUtils colorWithHexString:@"434343"]
#define TitleLHeight 45
#define BtnHeight 55

#define BottomSpace1 8
#define BottomSpace2 10

#define Alpha 1

@interface LGActionSheet ()
@property (nonatomic, strong) UIView *contentView;
@property (nonatomic, strong) UILabel *titleL;
@end

@implementation LGActionSheet
- (instancetype)init
{
    self = [super init];
    if (self) {
        self.frame = [UIScreen mainScreen].bounds;
        self.backgroundColor = [UIColor colorWithWhite:0.6 alpha:0.4];
        
        [self addTarget:self action:@selector(dismiss) forControlEvents:UIControlEventTouchUpInside];
    }
    return self;
}

- (void)addButtonTitles:(NSArray *)btnTitles btnIcons:(NSArray *)btnIcons {
    [self createUIButtonTitles:btnTitles btnIcons:btnIcons btnTags:nil];
}
- (void)addButtonTitles:(NSArray *)btnTitles btnIcons:(NSArray *)btnIcons btnTags:(NSArray *)btnTags {
    [self createUIButtonTitles:btnTitles btnIcons:btnIcons btnTags:btnTags];
}

- (void)showInView:(UIView *)superView {
    [superView addSubview:self];
    
    CGRect frame = self.contentView.frame;
    frame.origin.y = ScreenHeight;
    
    __weak typeof(self)weakSelf = self;
    [UIView animateWithDuration:ShowTime animations:^{
        CGRect frame = weakSelf.contentView.frame;
        frame.origin.y = ScreenHeight - self.contentView.frame.size.height;
        self.contentView.frame = frame;
    } completion:^(BOOL finished) {

    }];
}
#pragma mark - Private method
- (void)dismiss {
    __weak typeof(self)weakSelf = self;
    [UIView animateWithDuration:DismissTime animations:^{
        CGRect frame = weakSelf.contentView.frame;
        frame.origin.y = ScreenHeight;
        self.contentView.frame = frame;
    } completion:^(BOOL finished) {
        [weakSelf removeFromSuperview];
    }];
}

- (void)createUIButtonTitles:(NSArray *)btnTitles btnIcons:(NSArray *)btnIcons btnTags:(NSArray *)btnTags {
    UIView *contentView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, 0)];
    contentView.backgroundColor = [UIColor clearColor];
    [self addSubview:contentView];
    self.contentView = contentView;
    
    UIView *topView = [[UIView alloc] initWithFrame:CGRectMake(LeftAndRightSpace, 0, ScreenWidth - LeftAndRightSpace*2, 0)];
    topView.backgroundColor = [UIColor colorWithWhite:1 alpha:Alpha];
    topView.layer.masksToBounds = YES;
    topView.layer.cornerRadius = CornerRadius;
    CGRect frame = topView.frame;
    frame.size.height = TitleLHeight + BtnHeight*btnTitles.count + 3;
    topView.frame = frame;
    [contentView addSubview:topView];
    
    self.titleL = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(topView.frame), TitleLHeight)];
    self.titleL.text = self.title;
    self.titleL.textColor = TitleColor;
    self.titleL.textAlignment = NSTextAlignmentCenter;
    [topView addSubview:self.titleL];
    
    CGFloat space = 10;
    CGFloat firstItemWidth = 0;
    for (int index = 0; index < btnTitles.count; index++) {
        UIImageView *verLine = [[UIImageView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.titleL.frame) + index*(BtnHeight + 1), CGRectGetWidth(topView.frame), 1)];
        verLine.backgroundColor = [ZCUtils colorWithHexString:@"dcdcdc"];
        [topView addSubview:verLine];
        
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        [btn setFrame:CGRectMake(0, CGRectGetMaxY(self.titleL.frame) + index*BtnHeight + 1*(index + 1), CGRectGetWidth(topView.frame), BtnHeight)];

        [btn setTitleColor:BtnTitleColor forState:UIControlStateNormal];
        [btn setImage:[UIImage imageNamed:btnIcons[index]] forState:UIControlStateNormal];
        [btn setTitle:btnTitles[index] forState:UIControlStateNormal];
        [btn layoutIfNeeded];
        
        CGFloat titleLabelWidth = btn.titleLabel.frame.size.width;
        CGFloat imageViewWidth = btn.imageView.frame.size.width;
        if (index == 0) {
            firstItemWidth = titleLabelWidth + imageViewWidth;
            
        } else {
            CGFloat itemWidth = titleLabelWidth + imageViewWidth;
            [btn setImageEdgeInsets:UIEdgeInsetsMake(0, itemWidth - firstItemWidth, 0, 0)];

        }
        [btn setTitleEdgeInsets:UIEdgeInsetsMake(0, btn.imageEdgeInsets.left + space, 0, 0)];
        
        if (btnTags && btnTags.count == btnTitles.count) {
            btn.tag = [btnTags[index] integerValue];
        } else {
            btn.tag = index + 1;
        }
        [btn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
        [topView addSubview:btn];
    }
    
    
    UIButton *cancelBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [cancelBtn setFrame:CGRectMake(LeftAndRightSpace, CGRectGetMaxY(topView.frame) + BottomSpace1, CGRectGetWidth(topView.frame), BtnHeight)];
    cancelBtn.layer.masksToBounds = YES;
    cancelBtn.layer.cornerRadius = CornerRadius;
    cancelBtn.backgroundColor = [UIColor colorWithWhite:1 alpha:Alpha];
    [cancelBtn setTitle:@"取消" forState:UIControlStateNormal];
    [cancelBtn setTitleColor:BtnTitleColor forState:UIControlStateNormal];
    [cancelBtn.titleLabel setFont:[UIFont boldSystemFontOfSize:17]];
    cancelBtn.tag = 0;
    [cancelBtn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
    [contentView addSubview:cancelBtn];
    
    frame = contentView.frame;
    frame.size.height = CGRectGetMaxY(cancelBtn.frame) + BottomSpace2;
    frame.origin.y = ScreenHeight;
    contentView.frame = frame;
}


- (void)setTitle:(NSString *)title {
    _title = title;
    self.titleL.text = title;
}
- (void)btnClick:(UIButton *)btn {
    [self dismiss];
    
    if ([self.delegate respondsToSelector:@selector(actionSheetDidSelected:)]) {
        [self.delegate actionSheetDidSelected:btn.tag];
    }
}

- (NSAttributedString *)imageAttributedString:(NSString *)imageName {
    
    NSTextAttachment *attachMent = [[NSTextAttachment alloc] init];
    // 设置图片
    attachMent.image = [UIImage imageNamed:imageName];
    // 设置大小
    attachMent.bounds = CGRectMake(0, -16, 40, 40);
    // 添加
    // 2.使用附件创建属性字符串
    NSAttributedString *attrString = [NSAttributedString attributedStringWithAttachment:attachMent];
    
    return attrString;
}
- (NSAttributedString *)textAttributedString:(NSString *)text color:(UIColor *)color {
    NSShadow *shadow = [[NSShadow alloc] init];
    shadow.shadowOffset = CGSizeMake(0.5, 0.5);
    shadow.shadowColor = [UIColor colorWithWhite:0.5 alpha:1];
    
    NSDictionary *attribs = @{NSForegroundColorAttributeName:color,
                              NSShadowAttributeName:shadow,
                              NSFontAttributeName:[UIFont systemFontOfSize:18]};
    
    NSMutableAttributedString *strM = [[NSMutableAttributedString alloc] initWithString:text attributes:attribs];
    
    return strM;
}

@end
