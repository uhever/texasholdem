//
//  ZBManager.m
//  GameDemo
//
//  Created by Happy on 12/16/16.
//  Copyright © 2016 Happy. All rights reserved.
//

#import "ZBManager.h"
#import "ZCPlatformRequestManager.h"
#import "ZCLiveRequestManager.h"
#import "ZCLoginManager.h"
#import "ZCThirdDelegate.h"

@implementation ZBManager

static ZBManager *manager = nil;
+ (instancetype)sharedInstance {
    if (!manager) {
        manager = [[ZBManager alloc] init];
        
        [ZBSDKConfigure sharedInstance].isOnline = YES; // 默认线上环境
    }
    
    return manager;
}

- (void)setAppId:(NSString *)appid appkey:(NSString *)appkey {
    [ZBSDKConfigure sharedInstance].appid = appid;
    [ZBSDKConfigure sharedInstance].appkey = appkey;
}
- (void)setChannelId:(NSString *)channelId productId:(NSString *)productId {
    [ZBSDKConfigure sharedInstance].projectChannelId = channelId;
    [ZBSDKConfigure sharedInstance].appProductId = productId;
}

- (void)configureServerEnvironment:(BOOL)isOnline {
    [ZBSDKConfigure sharedInstance].isOnline = isOnline;
}

- (void)configureWeiXinAppId:(NSString *)appId {
    [WXApi registerApp:appId];

    [ZBSDKConfigure sharedInstance].weiXinAppId = appId;
}

- (BOOL)application:(UIApplication *)application
didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    return YES;
}
- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url {
    
    BOOL wxValue = YES;
#if ZBLoginWeiXinIsOpen
    wxValue = [WXApi handleOpenURL:url delegate:[ZCThirdDelegate sharedInstance]];
#endif
    
    return YES | wxValue;
}
- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {

    BOOL wxValue = YES;
#if ZBLoginWeiXinIsOpen
    wxValue = [WXApi handleOpenURL:url delegate:[ZCThirdDelegate sharedInstance]];
#endif
    
    return YES | wxValue;
}
- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<NSString*, id> *)options {

    BOOL wxValue = YES;
#if ZBLoginWeiXinIsOpen
    wxValue = [WXApi handleOpenURL:url delegate:[ZCThirdDelegate sharedInstance]];
#endif
    
    return YES | wxValue;
}
@end


@implementation ZBManager (Login)

- (void)loginSendAuthCode:(NSString *)phoneNum callback:(void(^)(NSError *error, NSDictionary *result))callback {
    if (phoneNum.length == 0) { // 手机号为空
        NSError *newError = [NSError errorWithDomain:ZBLoginErrorDomain code:ZBLoginErrorBlankPhoneNum userInfo:@{NSLocalizedDescriptionKey:@"手机号不能为空"}];
        callback(newError, nil);
        
        return;
    }
    
    [ZCPlatformRequestManager requestForSendAuthCode:phoneNum callback:callback];
}

- (void)loginInPhone:(NSString *)phoneNum authCode:(NSString *)authCode callback:(void(^)(NSError *error, NSDictionary *result))callback {
    if (phoneNum.length == 0) { // 手机号为空
        NSError *newError = [NSError errorWithDomain:ZBLoginErrorDomain code:ZBLoginErrorBlankPhoneNum userInfo:@{NSLocalizedDescriptionKey:@"手机号不能为空"}];
        callback ? callback(newError, nil):nil;
        
        return;
    }
    
    if (authCode.length == 0) { // 验证码为空
        NSError *newError = [NSError errorWithDomain:ZBLoginErrorDomain code:ZBLoginErrorBlankAuthCode userInfo:@{NSLocalizedDescriptionKey:@"验证码不能为空"}];
        callback(newError, nil);
        
        return;
    }
    
    [ZCPlatformRequestManager requestForPhoneLogin:phoneNum authCode:authCode callback:^(NSError *error, NSDictionary *dic) {
        if (error) {
            callback ? callback(error, dic):nil;
            
        } else {
            [ZCLiveRequestManager requestForLogin:dic callback:callback];
            
        }
        
    }];
    
}

- (void)loginInVisitor:(void(^)(NSError *error, NSDictionary *result))callback {
    [ZCPlatformRequestManager requestForVisitor:^(NSError *error, NSDictionary *dic) {
        if (error) {
            callback ? callback(error, dic):nil;
            
        } else {
            [ZCLiveRequestManager requestForLogin:dic callback:callback];
            
        }
    }];
    
}

- (void)loginInWeiXin:(void(^)(NSError *error, NSDictionary *result))callback {
    [[ZCLoginManager sharedInstance] loginInWeiXin:^(NSError *error, NSDictionary *result) {
        if (error) {
            callback ? callback(error, result):nil;
            
        } else {
            [ZCLiveRequestManager requestForLogin:result callback:^(NSError *error, NSDictionary *dic) {
                if (error) {
                    callback(error, nil);
                    
                } else {
                    NSMutableDictionary *newDic = dic.mutableCopy;
                    [newDic setObject:result[@"name"] ? result[@"name"]:@"" forKey:@"name"];
                    [newDic setObject:result[@"sex"] ? [NSString stringWithFormat:@"%@", result[@"sex"]]:@"" forKey:@"sex"];
                    [newDic setObject:result[@"hdImage"] ? result[@"hdImage"]:@"" forKey:@"icon"];
                    
                    callback(nil, newDic);
                    
                }
            }];
            
        }
    }];
}

@end


@implementation ZBManager (Payment)



- (void)configureProductIdentifierArr:(NSArray *)productIdentifierArr {
    [ZCApplePay shareInstance].productIdentifierArr = productIdentifierArr;
}

- (void)payForGame:(NSDictionary *)parameter payType:(ZCPayType)payType completion:(ZCPayCompletion)completion {
    [[ZCPaymentManager shareInstance] payForGame:parameter payType:payType completion:completion];
}
@end

@implementation ZBManager (Share)
- (void)share:(ZCBShareContent *)shareContent shareType:(ZCBShareType)shareType callback:(void(^)(NSError *error, NSDictionary *result))callback {
    [[ZCShare shareInstance] share:shareContent shareType:shareType callback:callback];
}
@end
