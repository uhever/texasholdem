//
//  ZBSDKConfigure.h
//  GameDemo
//
//  Created by Happy on 12/16/16.
//  Copyright © 2016 Happy. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ZBSDKConfigure : NSObject
@property (nonatomic, copy) NSString *appid;
@property (nonatomic, copy) NSString *appkey;

@property (nonatomic, copy) NSString *aipppayAppId;
@property (nonatomic, copy) NSString *aipppayPlatformKey;

@property (nonatomic, assign) BOOL isReview; //是否审核中
@property (nonatomic, assign) BOOL isOnline; //是否线上
@property (nonatomic, copy) NSString *testBaseUrl; //审核服Base url

@property (nonatomic, copy) NSString *token; //游戏单服token
@property (nonatomic, copy) NSString *uid;
@property (nonatomic, copy) NSString *projectChannelId;
@property (nonatomic, copy) NSString *appProductId;
@property (nonatomic, assign) int skinColor; //皮肤颜色

@property (nonatomic, strong) NSDictionary *ipInfoDic;

@property (nonatomic, copy) NSString *weiXinAppId; //微信appid
+ (instancetype)sharedInstance;
@end
