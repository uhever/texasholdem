//
//  ZBSDKConfigure.m
//  GameDemo
//
//  Created by Happy on 12/16/16.
//  Copyright © 2016 Happy. All rights reserved.
//

#import "ZBSDKConfigure.h"

@interface ZBSDKConfigure ()
@end


@implementation ZBSDKConfigure

static ZBSDKConfigure *manager = nil;
+ (instancetype)sharedInstance {
    if (!manager) {
        manager = [[ZBSDKConfigure alloc] init];
    }
    
    return manager;
}

- (NSString *)projectChannelId {
    if (_projectChannelId.length == 0) {
        return @"appstore_game";
    }
    
    return _projectChannelId;
}

- (NSString *)appProductId {
    if (_appProductId.length == 0) {
        return @"0";
    }
    
    return _appProductId;
}

- (NSString *)token {
    if (_token.length == 0) {
        return @"";
    }
    
    return _token;
}
@end
