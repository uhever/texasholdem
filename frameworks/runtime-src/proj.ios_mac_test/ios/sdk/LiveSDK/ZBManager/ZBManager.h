//
//  ZBManager.h
//  GameDemo
//
//  Created by Happy on 12/16/16.
//  Copyright © 2016 Happy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ZCPaymentManager.h"
#import "ZCUtils.h"
#import "ZBSDKConfigure.h"
#import "ZCShare.h"

@interface ZBManager : NSObject

+ (instancetype)sharedInstance;

/**
 配置sdk的appid和appke
 */
- (void)setAppId:(NSString *)appid appkey:(NSString *)appkey;

/**
 配置channelId和productId

 @param channelId 默认值 appstore_game
 @param productId 默认值 0
 */
- (void)setChannelId:(NSString *)channelId productId:(NSString *)productId;


/**
 选择服务器环境，默认YES 线上

 @param isOnline YES:线上；NO:线下
 */
- (void)configureServerEnvironment:(BOOL)isOnline;

- (void)configureWeiXinAppId:(NSString *)appId;


- (BOOL)application:(UIApplication *)application
didFinishLaunchingWithOptions:(NSDictionary *)launchOptions;
- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url;
- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation;
- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<NSString*, id> *)options;

@end


@interface ZBManager (Login)

/**
 游客登录
 
 @param callback 回调
 */
- (void)loginInVisitor:(void(^)(NSError *error, NSDictionary *result))callback;

/**
 获取验证码
 
 @param phoneNum 手机号
 @param callback 回调
 */
- (void)loginSendAuthCode:(NSString *)phoneNum callback:(void(^)(NSError *error, NSDictionary *result))callback;
/**
 手机登录
 
 @param phoneNum 手机号
 @param authCode 验证码
 @param callback 回调
 */
- (void)loginInPhone:(NSString *)phoneNum authCode:(NSString *)authCode callback:(void(^)(NSError *error, NSDictionary *result))callback;

/**
 微信登录

 @param callback 回调
 */
- (void)loginInWeiXin:(void(^)(NSError *error, NSDictionary *result))callback;

@end


@interface ZBManager (Payment)


/**
 In-App Purchase, 添加产品Id，比如：com.zhanle.shoutime.appms.diamond1

 @param productIdentifierArr 产品Id数组
 */
- (void)configureProductIdentifierArr:(NSArray *)productIdentifierArr;

/**
 游戏支付接口
 
 @param parameter 支付参数
 @param payType 支付类型
 @param completion 支付回调
 */
- (void)payForGame:(NSDictionary *)parameter payType:(ZCPayType)payType completion:(ZCPayCompletion)completion;

@end

@interface ZBManager (Share)
- (void)share:(ZCBShareContent *)shareContent shareType:(ZCBShareType)shareType callback:(void(^)(NSError *error, NSDictionary *result))callback;
@end
