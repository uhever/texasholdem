//
//  ZCBShareContent.h
//  GameDemo
//
//  Created by Happy on 1/16/17.
//  Copyright © 2017 Happy. All rights reserved.
//

#import <Foundation/Foundation.h>

// 分享内容类型
typedef NS_ENUM(NSInteger, ZCBShareContentType) {
    ZCBShareContentTypeText = 1, //文本
    ZCBShareContentTypeImage = 2, //图片
    ZCBShareContentTypeLink = 3, //链接
    ZCBShareContentTypeVideo = 4 //视频
};

@interface ZCBShareContent:NSObject
@property (nonatomic, assign) ZCBShareContentType contentType; //分享内容类型

@property (nonatomic, copy) NSString *contentText; //分享文字
@property (nonatomic, assign) int wxScene; //0:分享到微信聊天界面；1:分享到微信朋友圈

@property (nonatomic, copy) NSString *imageURL; //分享图片地址 (可以是本地的，也可以是网络的)

@property (nonatomic, copy) NSString *contentURL; //分享链接地址

@property (nonatomic, copy) NSString *videoURL; //分享视频地址

@property (nonatomic, copy) NSString *thumbImageURL; //微信分享图片、链接可以添加缩略图，图片大小不要超过32k；facebook也分享链接可以添加缩略图
@property (nonatomic, copy) NSString *contentTitle; //分享图片、链接到微信可以添加标题；分享链接到facebook也可以添加标题
@property (nonatomic, copy) NSString *contentDescription; //分享图片、链接到微信可以添加描述；分享链接到facebook也可以添加描述
@end
