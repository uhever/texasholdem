//
//  ZCShare.m
//  ZCPlatformSDK
//
//  Created by Happy on 6/22/16.
//  Copyright © 2016 ZhanCheng. All rights reserved.
//
#import "ZCShare.h"
#import "ZCThirdDelegate.h"

typedef void(^ZCBShareCallBackType)(NSError *error, NSDictionary *result);

@interface ZCShare () {
    ZCBShareCallBackType _shareCallback;
}
@end


@implementation ZCShare
+ (ZCShare *)shareInstance {
    static ZCShare *share = nil;
    static dispatch_once_t predicate;
    dispatch_once(&predicate, ^{
        share = [[ZCShare alloc] init];
    });
    return share;
}

#pragma mark - Share Method
- (void)share:(ZCBShareContent *)shareContent shareType:(ZCBShareType)shareType callback:(void(^)(NSError *error, NSDictionary *result))callback {
    _shareCallback = callback;
    
    if (shareType == ZCBShareTypeWeiXin) {
        [self shareInWeiXin:shareContent];
        
    } else if (shareType == ZCBShareTypeFacebook) {
        [self shareInFacebook:shareContent];
        
    }
}


- (void)shareInWeiXin:(ZCBShareContent *)shareContent {
#if ZBShareWeiXinIsOpen
    switch (shareContent.contentType) {
        case ZCBShareContentTypeText:
            [self weixinShareText:shareContent];
            break;
            
        case ZCBShareContentTypeImage:
            [self weixinShareImage:shareContent];
            break;
            
        case ZCBShareContentTypeLink:
            [self weixinShareLink:shareContent];
            break;
            
        case ZCBShareContentTypeVideo:
            [self weixinShareVideo:shareContent];
            break;
    }
    
#else
    NSLog(@"ZCBSDK share error:微信分享暂未实现");

#endif
}
- (void)weixinShareText:(ZCBShareContent *)shareContent {
#if ZBShareWeiXinIsOpen
    SendMessageToWXReq *req = [[SendMessageToWXReq alloc] init];
    req.text = shareContent.contentText;
    req.bText = YES;
    req.scene = shareContent.wxScene;
    [WXApi sendReq:req];
#endif
}
- (void)weixinShareImage:(ZCBShareContent *)shareContent {
#if ZBShareWeiXinIsOpen
    NSURL *imageURL = nil;
    
    if ([shareContent.imageURL hasPrefix:@"http"]) {
        imageURL = [NSURL URLWithString:shareContent.imageURL];
    } else {
        imageURL = [NSURL fileURLWithPath:shareContent.imageURL];
    }
    
    /*
     Photos must be less than 12MB in size
     People need the native Facebook for iOS app installed, version 7.0 or higher
     */
    UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfURL:imageURL]];
    if (!image) {
        NSError *error = [NSError errorWithDomain:@"com.zcbsdk.share" code:-3 userInfo:@{NSLocalizedDescriptionKey:@"读取图片失败"}];
        _shareCallback(error, nil);
        return;
    }
    
    WXMediaMessage *message = [WXMediaMessage message];
    
    NSURL *thumbImageUrl = nil;
    if (shareContent.thumbImageURL) { //设置缩略图
        if ([shareContent.thumbImageURL hasPrefix:@"http"]) {
            thumbImageUrl = [NSURL URLWithString:shareContent.thumbImageURL];
        } else {
            thumbImageUrl = [NSURL fileURLWithPath:shareContent.thumbImageURL];
        }
        
    } else {
        thumbImageUrl = imageURL;
        
    }
    UIImage *thumbImage = [UIImage imageWithData:[NSData dataWithContentsOfURL:thumbImageUrl]];
    NSData *thumbDataCompressed = [ZCUtils imageCompress:thumbImage targetWidth:160 compression:1.0];
    [message setThumbImage:[UIImage imageWithData:thumbDataCompressed]];
    
    
    if (shareContent.contentTitle) {
        message.title = shareContent.contentTitle;
    }
    if (shareContent.contentDescription) {
        message.description = shareContent.contentTitle;
    }
    
    WXImageObject *imageObject = [WXImageObject object];
    imageObject.imageData = UIImagePNGRepresentation(image);
    message.mediaObject = imageObject;
    
    SendMessageToWXReq *req = [[SendMessageToWXReq alloc] init];
    req.bText = NO;
    req.message = message;
    req.scene = shareContent.wxScene;
    [WXApi sendReq:req];
#endif
}
// 微信可以显示Title和缩略图
- (void)weixinShareLink:(ZCBShareContent *)shareContent {
#if ZBShareWeiXinIsOpen
    WXMediaMessage *message = [WXMediaMessage message];
    if (shareContent.contentTitle) {
        message.title = shareContent.contentTitle;
    }
    if (shareContent.contentDescription) {
        message.description = shareContent.contentDescription;
    }
    
    if (shareContent.thumbImageURL) {
        NSURL *thumbImageURL = nil;
        if ([shareContent.thumbImageURL hasPrefix:@"http"]) {
            thumbImageURL = [NSURL URLWithString:shareContent.thumbImageURL];
            [message setThumbImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:thumbImageURL]]];
            
        } else {
            //                thumbImageURL = [NSURL fileURLWithPath:shareContent.thumbImageURL];
        }
    } else {
        UIImage *image = [UIImage imageNamed:@"Icon.png"];
        [message setThumbImage:image];
    }
    WXWebpageObject *webpageObject = [WXWebpageObject object];
    webpageObject.webpageUrl = shareContent.contentURL;
    message.mediaObject = webpageObject;
    
    SendMessageToWXReq *req = [[SendMessageToWXReq alloc] init];
    req.bText = NO;
    req.message = message;
    req.scene = shareContent.wxScene;
    
    [WXApi sendReq:req];
#endif
}
- (void)weixinShareVideo:(ZCBShareContent *)shareContent {
    NSLog(@"ZCBSDK error:微信分享视频暂未实现");
}


- (void)shareInFacebook:(ZCBShareContent *)shareContent {
#if ZBShareFaceBookIsOpen
    switch (shareContent.contentType) {
        case ZCBShareContentTypeText:
            [self facebookShareText:shareContent];
            break;
        case ZCBShareContentTypeImage:
            [self facebookShareImage:shareContent];
            break;
        case ZCBShareContentTypeLink:
            [self facebookShareLink:shareContent];
            break;
        case ZCBShareContentTypeVideo:
            [self facebookShareVideo:shareContent];
            break;
    }
#else
    NSLog(@"ZCBSDK share error:Facebook分享暂未实现");
    
#endif
}
- (void)facebookShareText:(ZCBShareContent *)shareContent {
    NSLog(@"ZCBSDK Error: facebook不能分享纯文本");
}
- (void)facebookShareImage:(ZCBShareContent *)shareContent {
#if ZBShareFaceBookIsOpen
    NSURL *imageURL = nil;
    
    if ([shareContent.imageURL hasPrefix:@"http"]) {
        imageURL = [NSURL URLWithString:shareContent.imageURL];
    } else {
        imageURL = [NSURL fileURLWithPath:shareContent.imageURL];
    }
    
    /*
     Photos must be less than 12MB in size
     People need the native Facebook for iOS app installed, version 7.0 or higher
     */
    UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfURL:imageURL]];
    if (!image) {
        NSError *error = [NSError errorWithDomain:@"com.zcbsdk.share" code:-3 userInfo:@{NSLocalizedDescriptionKey:@"读取图片失败"}];
        _shareCallback(error, nil);
        return;
    }
    
    FBSDKSharePhoto *photo = [[FBSDKSharePhoto alloc] init];
    photo.image = image;
    photo.userGenerated = YES;
    FBSDKSharePhotoContent *content = [[FBSDKSharePhotoContent alloc] init];
    content.photos = @[photo];
    
    [FBSDKShareDialog showFromViewController:[self currentViewController] withContent:content delegate:[ZCThirdDelegate sharedInstance]];
#endif

}
- (void)facebookShareVideo:(ZCBShareContent *)shareContent {
#if ZBShareFaceBookIsOpen
    FBSDKShareVideo *video = [[FBSDKShareVideo alloc] init];
    video.videoURL = [NSURL URLWithString:shareContent.videoURL];
    FBSDKShareVideoContent *content = [[FBSDKShareVideoContent alloc] init];
    content.video = video;
    
    [FBSDKShareDialog showFromViewController:nil withContent:content delegate:[ZCThirdDelegate sharedInstance]];
#endif
}
//Facebook可以显示缩略图、Title和描述
- (void)facebookShareLink:(ZCBShareContent *)shareContent {
#if ZBShareFaceBookIsOpen
    FBSDKShareLinkContent *content = [[FBSDKShareLinkContent alloc] init];
    content.contentURL = [NSURL URLWithString:shareContent.contentURL];
    
    if (shareContent.thumbImageURL) { //Facebook分享缩略图只能是网络地址
        content.imageURL = [NSURL URLWithString:shareContent.thumbImageURL];
    }
    if (shareContent.contentTitle) {
        content.contentTitle = shareContent.contentTitle;
    }
    if (shareContent.contentDescription) {
        content.contentDescription = shareContent.contentDescription;
    }
    
    //这种方式无法显示缩略图，只能使用下面方法
    //      [FBSDKShareDialog showFromViewController:[self currentViewController] withContent:content delegate:self];
    
    FBSDKShareDialog *dialog = [[FBSDKShareDialog alloc] init];
    dialog.fromViewController = nil;
    dialog.mode = FBSDKShareDialogModeFeedWeb;
    dialog.shareContent = content;
    dialog.delegate = [ZCThirdDelegate sharedInstance];
    [dialog show];
#endif
}


- (UIViewController *)currentViewController {
    UIViewController *currentVC = [UIApplication sharedApplication].keyWindow.rootViewController;
    
    while (currentVC.presentedViewController) {
        currentVC = currentVC.presentedViewController;
    }
    
    return currentVC;
}


- (void)shareCallback:(NSError *)error result:(NSDictionary *)result loginType:(ZCBShareType)shareType {
    if (error) {
        NSLog(@"ZCShare Error: %@", error);
        _shareCallback(error, result);
        
    } else {
        _shareCallback(nil, result);
        
    }
}
@end
