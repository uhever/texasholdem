//
//  ZCLiveRequestManager.h
//  GameDemo
//
//  Created by Happy on 12/16/16.
//  Copyright © 2016 Happy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ZCLiveRequest.h"

typedef void(^ZCLiveRequestCallback)(NSError *error, NSDictionary *dic);

@interface ZCLiveRequestManager : ZCLiveRequest
+ (void)requestForLogin:(NSDictionary *)dic callback:(ZCLiveRequestCallback)callback;
@end
