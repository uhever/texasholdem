//
//  ZCPlatformRequestManager.m
//  GameDemo
//
//  Created by Happy on 12/16/16.
//  Copyright © 2016 Happy. All rights reserved.
//

#import "ZCPlatformRequestManager.h"

NSString *const ZBLoginErrorDomain = @"com.zblogin.result";

@implementation ZCPlatformRequestManager
+ (void)requestForVisitor:(ZCPlatformRequestCallback)callback {
    NSDictionary *params = @{@"uuid": [ZCDeviceInfo deviceUUIDString],
                             @"devInfo":@""};
    
    [ZCPlatformRequest request:ZCURL_Trial_Login parameter:params success:^(NSDictionary *dic) {
        callback(nil, dic);
        
    } failure:^(NSError *error) {
        callback(error, nil);
        
    }];
}

+ (void)requestForSendAuthCode:(NSString *)phoneNum callback:(ZCPlatformRequestCallback)callback {
    NSDictionary *params = @{@"mobileNumber": phoneNum};
    [ZCPlatformRequest request:ZCURL_Send_AuthCode parameter:params success:^(NSDictionary *dic) {
        callback(nil, dic);
        
    } failure:^(NSError *error) {
        
        NSError *newError = error;
        if (error.code == 60010) { //60秒内不能重复发送
            newError = [NSError errorWithDomain:ZBLoginErrorDomain code:ZBLoginErrorSendAuthCodeFrequent userInfo:@{NSLocalizedDescriptionKey:@"60秒内不能重复发送"}];
            
        }
        
        callback(newError, nil);
    }];

}
+ (void)requestForPhoneLogin:(NSString *)phoneNum authCode:(NSString *)authCode callback:(ZCPlatformRequestCallback)callback {
    NSDictionary *params = @{@"mobileNumber": phoneNum,
                             @"seccode" : authCode,
                             @"devInfo" : @""};
    
    [ZCPlatformRequest request:ZCURL_Mobile_Login parameter:params success:^(NSDictionary *dic) {
        callback(nil, dic);
        
    } failure:^(NSError *error) {
        callback(error, nil);

    }];
}

+ (void)requestForWeiXin:(NSDictionary *)dic callback:(ZCPlatformRequestCallback)callback {
    if ([ZBSDKConfigure sharedInstance].weiXinAppId.length == 0) {
        NSError *weiXinError = [NSError errorWithDomain:@"com.zbsdk.login" code:-1 userInfo:@{NSLocalizedDescriptionKey:@"缺少参数微信appid"}];
        callback(weiXinError, nil);
        return;
    }
    
    NSDictionary *params = @{@"code":dic[@"code"],
                             @"devInfo":@"",
                             @"wcAppId" : [ZBSDKConfigure sharedInstance].weiXinAppId,
                             @"isUserInfo":@"1"};
    
    [ZCPlatformRequest request:ZCURL_Weixin_Login parameter:params success:^(NSDictionary *dic) {
        callback(nil, dic);
        
    } failure:^(NSError *error) {
        callback(error, nil);
        
    }];
}
@end

