//
//  ZCRequest.m
//  ZCLibProjectDEV
//
//  Created by Happy on 8/27/16.
//  Copyright © 2016 Happy. All rights reserved.
//

#import "ZCRequest.h"
#import "ZCEncrypt.h"
#import "ZCDeviceInfo.h"

NSString *const ZCRequestEncryptKey = @"encryptType";
NSString *const ZCRequestErrorDomain = @"com.zcrequest.errordomain";

@implementation ZCRequest
- (id)initWithBaseURL:(NSString *)baseURL {
    if (self = [super init]) {
        _httpOperationManager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:baseURL]];
        _httpOperationManager.requestSerializer = [AFJSONRequestSerializer serializer];
        _httpOperationManager.responseSerializer = [AFHTTPResponseSerializer serializer];
        _httpOperationManager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/html", @"text/plain", @"application/json",nil];
        _httpOperationManager.requestSerializer.timeoutInterval = 30;
    }
    return self;
}

- (void)post:(NSString *)URLString parameter:(NSDictionary *)parameter success:(RequestSuccess)success failure:(RequestFailure)failure {
    
    [_httpOperationManager POST:URLString parameters:parameter progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary* responseDic = [NSJSONSerialization JSONObjectWithData:(NSData *)responseObject options:NSJSONReadingAllowFragments error:nil];
        success(responseDic);

    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        failure(error);

    }];
}

- (void)get:(NSString *)URLString parameter:(NSDictionary *)parameter success:(RequestSuccess)success failure:(RequestFailure)failure {
    [_httpOperationManager GET:URLString parameters:parameter progress:^(NSProgress * _Nonnull downloadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary* responseDic = [NSJSONSerialization JSONObjectWithData:(NSData *)responseObject options:NSJSONReadingAllowFragments error:nil];
        success(responseDic);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        failure(error);

    }];
}

+ (NSString *)deviceInfo {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    
    [dic setObject:[ZCDeviceInfo deviceUUIDString] forKey:@"uuid"];
    [dic setObject:[ZCDeviceInfo advertisingIdentifier] forKey:@"idfa"];
    [dic setObject:@"" forKey:@"mac"];
    [dic setObject:[ZCDeviceInfo deviceBrand] forKey:@"brand"];
    [dic setObject:[ZCDeviceInfo deviceModel] forKey:@"model"];
    [dic setObject:[ZCDeviceInfo deviceSystemVersion] forKey:@"ver"];
    
    return [ZCEncrypt getJsonString:dic];
}

+ (NSString *)getStanderdString:(NSDictionary *)dic {
    return [ZCEncrypt getStanderdString:dic];
}
@end
