//
//  ZCLiveRequest.h
//  ZCLibProjectDEV
//
//  Created by Happy on 8/27/16.
//  Copyright © 2016 Happy. All rights reserved.
//

#import "ZCRequest.h"
#import "ZCLiveApi.h"

@interface ZCLiveRequest : ZCRequest
/**
 *  访问直播UC服务器
 */
+ (void)requestUCServer:(NSString *)url parameter:(NSDictionary *)parameter success:(RequestSuccess)success failure:(RequestFailure)failure;

/**
 *  访问直播App服务器
 */
+ (void)requestAppServer:(NSString *)url parameter:(NSDictionary *)parameter success:(RequestSuccess)success failure:(RequestFailure)failure;


/**
 *  访问支付服务器
 */
+ (void)requestPayServer:(NSString *)url parameter:(NSDictionary *)parameter success:(RequestSuccess)success failure:(RequestFailure)failure;
/**
 *  上传图片
 *
 *  @param url       上传地址
 *  @param para      接口参数
 *  @param imagePara 三个值，image:图片对象(UIImage格式)、uploadKey(服务器规定)、fileName:保存在服务器的图片名字
 *  @param progress  上传进度
 */
+ (void)uploadPicture:(NSString *)url parameters:(NSDictionary *)para imagePara:(NSDictionary *)imagePara progress:(void(^)(NSProgress *))progress success:(RequestSuccess)success failure:(RequestFailure)failure;

- (void)downloadFile:(NSString *)url filePath:(NSString *)filePath progress:(void(^)(NSProgress *))progress success:(void(^)(NSData *fileData))success failure:(RequestFailure)failure;
@end


#define TokenKey_For_ZCLive @"TokenKey_For_ZCLive"
