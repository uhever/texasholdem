//
//  ZCPlatformApi.h
//  ZCLibProjectDEV
//
//  Created by Happy on 8/27/16.
//  Copyright © 2016 Happy. All rights reserved.
//

#ifndef ZCPlatformApi_h
#define ZCPlatformApi_h


#define ZCBaseURLDEV  @"http://192.168.21.34:12356"
#define ZCBaseURLDist @"https://uc-platform.jjshowtime.com"

#define ZCURL_ApplePay_Init @"/pay/iosinitiate" //发起Apple支付
#define ZCURL_ApplePay_CheckOrder @"/pay/ioscheck" //支付核对

#define ZCURL_PingPay_Init @"/pay/pinginitiate" //Ping支付
#define ZCURL_AippPay_Init @"/pay/iapppayinitiate" //爱贝支付

#define ZCURL_Trial_Login     @"/member/trial" //游客登录
#define ZCURL_Send_AuthCode @"/member/mobileseccodesend" //发送验证码
#define ZCURL_Mobile_Login @"/member/mobilelogin" //手机登录
#define ZCURL_Weixin_Login @"/member/wclogin" //微信登录

#endif /* ZCPlatformApi_h */
