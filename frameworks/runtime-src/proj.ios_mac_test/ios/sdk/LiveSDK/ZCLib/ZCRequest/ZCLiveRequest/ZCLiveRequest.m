//
//  ZCLiveRequest.m
//  ZCLibProjectDEV
//
//  Created by Happy on 8/27/16.
//  Copyright © 2016 Happy. All rights reserved.
//

#import "ZCLiveRequest.h"
#import "ZCPlatformRequest.h"

@implementation ZCLiveRequest
#pragma mark - OverWrite
- (id)initWithBaseURL:(NSString *)baseURL {
    if (self = [super initWithBaseURL:baseURL]) {
        [self.httpOperationManager.requestSerializer setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];

    }
    return self;
}
- (void)post:(NSString *)URLString parameter:(NSDictionary *)parameter success:(RequestSuccess)success failure:(RequestFailure)failure {
    
    NSMutableDictionary *newPara = [NSMutableDictionary dictionaryWithDictionary:parameter];
    [newPara setObject:@(ZCRequestEncryptTypeLive) forKey:ZCRequestEncryptKey];
    if ([newPara objectForKey:@"devInfo"]) {
        [newPara setObject:[ZCPlatformRequest deviceInfo] forKey:@"devInfo"];
    }
    if ([newPara objectForKey:@"idfa"]) {
        [newPara setObject:[ZCDeviceInfo advertisingIdentifier] forKey:@"idfa"];
    }
    
    [super post:URLString parameter:newPara success:^(NSDictionary *result) {
        if ([[result objectForKey:@"status"] intValue] == 1) {
            success(result);
            
        } else {
            NSString *errorMsg = LGLocalizedString(@"badNetworkAlert", @"服务端异常，请稍后重试。");
            NSError *error = [NSError errorWithDomain:ZCRequestErrorDomain code:[[result objectForKey:@"status"] intValue] userInfo:@{NSLocalizedDescriptionKey:errorMsg}];
            failure(error);
            
        }
    } failure:^(NSError *error) {
        failure(error);
        
    }];
}
#pragma mark


+ (void)requestUCServer:(NSString *)url parameter:(NSDictionary *)parameter success:(RequestSuccess)success failure:(RequestFailure)failure {
    NSString *baseUrlForUCServer = ZCBaseURLForUCServer_Dev;
    if ([ZBSDKConfigure sharedInstance].isOnline) {
        baseUrlForUCServer = ZCBaseURLForUCServer_Dist;
    }
    
    [ZCLiveRequest request:baseUrlForUCServer relativeURL:url parameter:parameter success:success failure:failure];
}
+ (void)requestAppServer:(NSString *)url parameter:(NSDictionary *)parameter success:(RequestSuccess)success failure:(RequestFailure)failure {
    NSString *baseUrlForAppServer = ZCBaseURLForAppServer_Dev;
    if ([ZBSDKConfigure sharedInstance].isOnline) {
        baseUrlForAppServer = ZCBaseURLForAppServer_Dist;
        
        if ([ZBSDKConfigure sharedInstance].isReview) {
            // 审核中，应该使用审核服baseUrl
            if ([ZBSDKConfigure sharedInstance].testBaseUrl.length) {
                baseUrlForAppServer = [ZBSDKConfigure sharedInstance].testBaseUrl;
            }
        }
    }
    
    [ZCLiveRequest request:baseUrlForAppServer relativeURL:url parameter:parameter success:success failure:failure];
}
+ (void)requestPayServer:(NSString *)url parameter:(NSDictionary *)parameter success:(RequestSuccess)success failure:(RequestFailure)failure {
    NSString *baseUrlForPayServer = ZCBaseURLForPayServer_Dev;
    if ([ZBSDKConfigure sharedInstance].isOnline) {
        baseUrlForPayServer = ZCBaseURLForPayServer_Dist;
        
    }
    
    [ZCLiveRequest request:baseUrlForPayServer relativeURL:url parameter:parameter success:success failure:failure];
}


- (void)downloadFile:(NSString *)url filePath:(NSString *)filePath progress:(void(^)(NSProgress *))progress success:(void(^)(NSData *fileData))success failure:(RequestFailure)failure {
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
    NSURL *URL = [NSURL URLWithString:url];
    NSURLRequest *request = [NSURLRequest requestWithURL:URL];
    
    NSURLSessionDownloadTask *downloadTask = [manager downloadTaskWithRequest:request progress:^(NSProgress * _Nonnull downloadProgress) {
        !progress ? : progress(downloadProgress);
        
    } destination:^NSURL * _Nonnull(NSURL * _Nonnull targetPath, NSURLResponse * _Nonnull response) {
        return [NSURL fileURLWithPath:filePath];
        
    } completionHandler:^(NSURLResponse * _Nonnull response, NSURL * _Nullable filePath, NSError * _Nullable error) {
        if (!error) {
            NSLog(@"下载成功:%@", filePath.absoluteString);
            !success ? :success(nil);
            
        } else {
            NSLog(@"下载失败: %@", error.localizedDescription);
        }
    }];
    
    [downloadTask resume];
}
+ (void)uploadPicture:(NSString *)url parameters:(NSDictionary *)para imagePara:(NSDictionary *)imagePara progress:(void(^)(NSProgress *))progress success:(RequestSuccess)success failure:(RequestFailure)failure {
    UIImage *image = [imagePara objectForKey:@"image"];
    NSData *imageData = UIImageJPEGRepresentation(image, 1.0);//将UIImage转为NSData，1.0表示不压缩图片质量。
    
    AFHTTPSessionManager *httpManager = [[AFHTTPSessionManager alloc] initWithBaseURL:nil];
    httpManager.requestSerializer = [AFJSONRequestSerializer serializer];
    [httpManager.requestSerializer setValue:@"multipart/form-data" forHTTPHeaderField:@"Content-Type"];
    [httpManager.requestSerializer setValue:[NSString stringWithFormat:@"%ld", (unsigned long)imageData.length] forHTTPHeaderField:@"Content-Length"];
    httpManager.responseSerializer = [AFHTTPResponseSerializer serializer];
    httpManager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/html", @"text/plain", nil];
    httpManager.requestSerializer.timeoutInterval = 30;

    
    if ([ZBSDKConfigure sharedInstance].isReview == 0) {
        if ([ZBSDKConfigure sharedInstance].isOnline) {
            if ([ZBSDKConfigure sharedInstance].testBaseUrl.length) {
                url = [url stringByAppendingString:[NSString stringWithFormat:@"&server_url=%@", [ZCUtils base64EncodedString:[ZBSDKConfigure sharedInstance].testBaseUrl]]];
                
            } else {
                url = [url stringByAppendingString:[NSString stringWithFormat:@"&server_url=%@", [ZCUtils base64EncodedString:ZCBaseURLForAppServer_Dist]]];
                
            }
        } else {
            url = [url stringByAppendingString:[NSString stringWithFormat:@"&server_url=%@", [ZCUtils base64EncodedString:ZCBaseURLForAppServer_Dev]]];

        }
        
    } else {
        if ([ZBSDKConfigure sharedInstance].isOnline) {
            url = [url stringByAppendingString:[NSString stringWithFormat:@"&server_url=%@", [ZCUtils base64EncodedString:ZCBaseURLForAppServer_Dist]]];

        } else {
            url = [url stringByAppendingString:[NSString stringWithFormat:@"&server_url=%@", [ZCUtils base64EncodedString:ZCBaseURLForAppServer_Dev]]];

        }
        
    }
    
    [httpManager POST:url parameters:para constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        if ([imagePara objectForKey:@"image"]) {
            [formData appendPartWithFileData:imageData name:[imagePara objectForKey:@"uploadKey"] fileName:[imagePara objectForKey:@"fileName"] mimeType:@"image/jpeg"];
        }
    } progress:progress success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary* responseDic = [NSJSONSerialization JSONObjectWithData:(NSData *)responseObject options:NSJSONReadingAllowFragments error:nil];
        
        success(responseDic);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        failure(error);
    }];
}


#pragma mark - Private method
+ (void)request:(NSString *)baseURL relativeURL:(NSString *)relativeURL parameter:(NSDictionary *)parameter success:(RequestSuccess)success failure:(RequestFailure)failure {
    NSMutableDictionary *newPara = [NSMutableDictionary dictionaryWithDictionary:parameter];
    [newPara setObject:@(ZCRequestEncryptTypeLive) forKey:ZCRequestEncryptKey];
    
    
    //以get参数方式，在每个接口后边拼接app_version、app_token_uid、app_channel_id、app_product_id、app_skin
    NSString *appTokenUid = @"";
    if ([ZCLocalData userDefaultObject:TokenKey_For_ZCLive] && [ZBSDKConfigure sharedInstance].uid) {
        appTokenUid = [ZBSDKConfigure sharedInstance].uid;
    }
    
    NSString *newRelativeURL = [relativeURL stringByAppendingString:[NSString stringWithFormat:@"&app_channel_id=%@&app_version=%@&app_token_uid=%@&app_product_id=%@&app_skin=%@", [ZBSDKConfigure sharedInstance].projectChannelId, [ZCLocalData preferencePlistObject:@"CFBundleVersion"], appTokenUid, [ZBSDKConfigure sharedInstance].appProductId, [NSString stringWithFormat:@"%d", [ZBSDKConfigure sharedInstance].skinColor]]];
    
    ZCLiveRequest *request = [[ZCLiveRequest alloc] initWithBaseURL:baseURL];
    [request post:newRelativeURL parameter:newPara success:^(NSDictionary *dic) {
        success(dic);
        
    } failure:^(NSError *error) {
        failure(error);
        if (error.code == -1009) { //网络断了
            [ZCLiveRequest alertForBadNetwork:relativeURL];
        }
        
        ZCRequestLog(@"RUL:%@\%@ \nErrorCode:%d, ErrorMsg:%@", baseURL,relativeURL, (int)error.code, error.localizedDescription);
    }];
}

+ (void)alertForBadNetwork:(NSString *)urlStr {
    if ([urlStr isEqualToString:ZCURL_Get_Info] ||
        [urlStr isEqualToString:ZCURL_Login_LiveServer] ||
        [urlStr isEqualToString:ZCURL_Login_UCServer]) {
//        [HRAlertView showMessage:LGLocalizedString(@"noNetworkAlert", nil) alertViewType:HRAlertViewTypeFromTop];
    }
}
@end
