//
//  ZCPlatformRequest.m
//  ZCLibProjectDEV
//
//  Created by Happy on 8/27/16.
//  Copyright © 2016 Happy. All rights reserved.
//

#import "ZCPlatformRequest.h"
#import "ZCLocalData.h"
#import "ZCEncrypt.h"
#import "ZBSDKConfigure.h"

@implementation ZCPlatformRequest
- (id)init {
    NSString *baseURL = nil;
    if ([ZBSDKConfigure sharedInstance].isOnline) {
        baseURL = ZCBaseURLDist;
    } else {
        baseURL = ZCBaseURLDEV;
    }
    
    if (self = [super initWithBaseURL:baseURL]) {
        
    }
    return self;
}

- (void)post:(NSString *)URLString parameter:(NSDictionary *)parameter success:(RequestSuccess)success failure:(RequestFailure)failure {
    NSMutableDictionary *newPara = [NSMutableDictionary dictionaryWithDictionary:parameter];
    if ([newPara objectForKey:@"devInfo"]) {
        [newPara setObject:[ZCRequest deviceInfo] forKey:@"devInfo"];
    }
    if ([newPara objectForKey:ZCPlatformSessionKey]) {
        if([ZCLocalData userDefaultObject:ZCPlatformSessionKey]) {
            [newPara setObject:[ZCLocalData userDefaultObject:ZCPlatformSessionKey] forKey:ZCPlatformSessionKey];
        } else {
            NSLog(@"Fatal Error:\n本地没有session供请求:%@使用", URLString);
        }
    }
    newPara = [NSMutableDictionary dictionaryWithDictionary:[self resetParameter:newPara]];
    
    [newPara setObject:@(ZCRequestEncryptTypePlatform) forKey:ZCRequestEncryptKey];
    [super post:URLString parameter:newPara success:^(NSDictionary *result){
        if ([[result objectForKey:@"code"] intValue] == 0) {
            NSDictionary *dataDic = [result objectForKey:@"data"];
            
            //保存登陆平台服务器信息session、authkey和uid
            if ([dataDic objectForKey:ZCPlatformSessionKey]) {
                [ZCPlatformRequest saveLoginInfo:dataDic];
            }
            success(dataDic);
        } else {
            NSError *error = [NSError errorWithDomain:ZCRequestErrorDomain code:[[result objectForKey:@"code"] intValue] userInfo:@{NSLocalizedDescriptionKey:[result objectForKey:@"msg"]}];
            failure(error);
        }
    } failure:failure];
}

+ (void)request:(NSString *)url parameter:(NSDictionary *)parameter success:(RequestSuccess)success failure:(RequestFailure)failure {
    ZCPlatformRequest *request = [[ZCPlatformRequest alloc] init];
    [request post:url parameter:parameter success:^(NSDictionary *dic) {
        success(dic);
    } failure:^(NSError *error) {
        ZCRequestLog(@"RUL:%@\nErrorCode:%d, ErrorMsg:%@", url, (int)error.code, error.localizedDescription);

        failure(error);
    }];
}

#pragma mark - Private method
+ (void)saveLoginInfo:(NSDictionary *)dic {
    [ZCLocalData userDefaultSave:[dic objectForKey:ZCPlatformAuthkeyKey] key:ZCPlatformAuthkeyKey];
    [ZCLocalData userDefaultSave:[dic objectForKey:ZCPlatformUidKey] key:ZCPlatformUidKey];
    [ZCLocalData userDefaultSave:[dic objectForKey:ZCPlatformSessionKey] key:ZCPlatformSessionKey];
}

#pragma mark - 签名
#define ZCSDK_SDK_Ver @"1.3.5_zhCN_1_cn"
- (NSDictionary *)resetParameter:(NSDictionary *)parameter {
    NSMutableDictionary *newParameter = [NSMutableDictionary dictionaryWithDictionary:parameter];
    
    //构建sys参数
    NSString *uuid = [ZCDeviceInfo deviceUUIDString];
    NSString *apiv = [NSString stringWithFormat:@"%d", ZCAPIV];
    NSString *timeStamp = [ZCDeviceInfo timeStamp];
    NSString *net = [NSString stringWithFormat:@"%d", (int)[ZCDeviceInfo networkKind]];
    NSString *platform = [NSString stringWithFormat:@"%d", ZCPLATFORM];
    NSString *appId = [ZBSDKConfigure sharedInstance].appid;
    NSString *sdkVer = ZCSDK_SDK_Ver;
    NSString *sys = [NSString stringWithFormat:@"%@|%@|%@|%@|%@|%@|%@", timeStamp, uuid, apiv, net, platform, appId, sdkVer];
    [newParameter setObject:sys forKey:ZCPara_Key_Sys];
    
    //获取签名
    NSString *signStr = [ZCPlatformRequest getSign:newParameter timeStamp:timeStamp];
    [newParameter setObject:signStr forKey:ZCPara_Key_Sig];
    
    return newParameter;
}
+ (NSString *)getSign:(NSDictionary *)dic timeStamp:(NSString *)timeStamp {
    //获取签名中的str
    NSMutableArray *allKeyArr = [NSMutableArray arrayWithArray:dic.allKeys];
    for (int i = 0; i < allKeyArr.count; i++) {
        BOOL isOK = YES;
        for (int j = 0; j < allKeyArr.count - i - 1; j++) {
            if ([allKeyArr[j] compare:allKeyArr[j+1] options:NSLiteralSearch] == NSOrderedDescending) {
                isOK = NO;
                [allKeyArr exchangeObjectAtIndex:j withObjectAtIndex:j+1];
            }
        }
        if (isOK) {
            break;
        }
    }
    
    //根据正序排好的Key，组织str
    NSMutableString *str = [NSMutableString stringWithFormat:@""];
    for (NSInteger index = 0; index < allKeyArr.count; index++) {
        NSString *tmpStr = [dic objectForKey:allKeyArr[index]];
        [str appendString:tmpStr];
    }
    
    //构建sig
    NSString *appKey = [ZBSDKConfigure sharedInstance].appkey;
    BOOL isNeedAuthkey = NO;
    if ([dic objectForKey:ZCPlatformSessionKey]) {
        isNeedAuthkey = YES;
    }
    NSString *authKey = isNeedAuthkey?[ZCLocalData userDefaultObject:ZCPlatformAuthkeyKey]:@"";
    ZCRequestLog(@"签名用authKey:%@", authKey);
    NSString *sig1 = [NSString stringWithFormat:@"%@%@%@%@", str, timeStamp, [ZCEncrypt encryptWithsha1:appKey], authKey];
    NSString *sig2 = [NSString stringWithFormat:@"%@%@", [ZCEncrypt encryptWithsha1:sig1], timeStamp];
    NSString *sig = [ZCEncrypt encryptWithsha1:sig2];
    
    return sig;
}
@end
