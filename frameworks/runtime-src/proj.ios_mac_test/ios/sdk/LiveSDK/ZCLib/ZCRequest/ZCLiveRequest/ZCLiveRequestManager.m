//
//  ZCLiveRequestManager.m
//  GameDemo
//
//  Created by Happy on 12/16/16.
//  Copyright © 2016 Happy. All rights reserved.
//

#import "ZCLiveRequestManager.h"
#import "ZCUtils.h"

@implementation ZCLiveRequestManager
+ (void)requestForLogin:(NSDictionary *)dic callback:(ZCLiveRequestCallback)callback {
    [self requestForLoginUCServer:dic callback:^(NSError *error, NSDictionary *dic) {
        if (error) {
            callback(error, nil);
            
        } else {
            callback(nil, dic);

            // 对于单独的游戏App，需要走App服务器登录流程
//            [self requestForLoginAppServer:dic callback:^(NSError *error, NSDictionary *dic) {
//                if (error) {
//                    callback(error, nil);
//                    
//                } else {
//                    callback(nil, dic);
//                    
//                }
//            }];
        }
    }];
}
+ (void)requestForLoginUCServer:(NSDictionary *)dic callback:(ZCLiveRequestCallback)callback {
//    NSDictionary *params = @{@"authkey" : dic[@"authkey"],
//                                 @"session" : dic[@"session"],
//                                 @"uId" : dic[@"uId"]};
    
    NSMutableDictionary *newParams = [NSMutableDictionary dictionaryWithDictionary:dic];
    [newParams addEntriesFromDictionary:@{@"channel":[ZBSDKConfigure sharedInstance].appProductId,
                                          @"idfa":@"",
                                          @"appid":[ZBSDKConfigure sharedInstance].appid,
                                          @"mac":@"",
                                          @"android_id":@""}];
    [ZCLiveRequest requestUCServer:ZCURL_Login_UCServer parameter:newParams success:^(NSDictionary *dic) {
        callback(nil, dic);
        
    } failure:^(NSError *error) {
        callback(error, nil);
        
    }];
}

+ (void)requestForLoginAppServer:(NSDictionary *)dic callback:(ZCLiveRequestCallback)callback {
    NSDictionary *params = @{@"uctoken":[dic objectForKey:@"token"],
                             @"idfa":@"",
                             };
    [ZCLiveRequest requestAppServer:ZCURL_Login_LiveServer parameter:params success:^(NSDictionary *dic) {
        callback(nil, dic);
        
    } failure:^(NSError *error) {
        callback(error, nil);
        
    }];
}
@end
