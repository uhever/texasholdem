//
//  ZCPlatformRequestManager.h
//  GameDemo
//
//  Created by Happy on 12/16/16.
//  Copyright © 2016 Happy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ZCPlatformRequest.h"

extern NSString *const ZBLoginErrorDomain;

// 登录错误
typedef NS_ENUM(NSInteger, ZBLoginError) {
    ZBLoginErrorBlankPhoneNum = -1, //手机号不能为空
    ZBLoginErrorBlankAuthCode = -2, //验证码不能为空
    ZBLoginErrorSendAuthCodeFrequent = -3, //60秒内不能重复发送
    ZBLoginErrorOther
};


typedef void(^ZCPlatformRequestCallback)(NSError *error, NSDictionary *dic);

@interface ZCPlatformRequestManager : NSObject
+ (void)requestForVisitor:(ZCPlatformRequestCallback)callback;

+ (void)requestForSendAuthCode:(NSString *)phoneNum callback:(ZCPlatformRequestCallback)callback;
+ (void)requestForPhoneLogin:(NSString *)phoneNum authCode:(NSString *)authCode callback:(ZCPlatformRequestCallback)callback;

+ (void)requestForWeiXin:(NSDictionary *)dic callback:(ZCPlatformRequestCallback)callback;
@end
