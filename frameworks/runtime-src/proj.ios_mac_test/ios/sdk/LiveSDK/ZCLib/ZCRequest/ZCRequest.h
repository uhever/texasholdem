//
//  ZCRequest.h
//  ZCLibProjectDEV
//
//  Created by Happy on 8/27/16.
//  Copyright © 2016 Happy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ZCRequestConfig.h"
#import "AFNetworking.h"
#import "ZCDeviceInfo.h"

extern NSString *const ZCRequestEncryptKey;
extern NSString *const ZCRequestErrorDomain;

typedef void(^RequestSuccess)(NSDictionary *dic);
typedef void(^RequestFailure)(NSError *error);
typedef NS_ENUM(NSInteger, ZCRequestEncryptType) {
    ZCRequestEncryptTypeNone, //无加密
    ZCRequestEncryptTypePlatform, //平台服务器加密
    ZCRequestEncryptTypeLive, //直播服务器加密
};

// 请求错误码解说
typedef NS_ENUM(NSInteger, ZCRequestErrorCode) {
    ZCRequestErrorCodeNoNetwork = -1004, //无网络
};


@interface ZCRequest : NSObject
@property (nonatomic, strong) AFHTTPSessionManager *httpOperationManager;


- (id)initWithBaseURL:(NSString *)baseURL;
- (void)post:(NSString *)URLString parameter:(NSDictionary *)paramter success:(RequestSuccess)success failure:(RequestFailure)failure;
- (void)get:(NSString *)URLString parameter:(NSDictionary *)parameter success:(RequestSuccess)success failure:(RequestFailure)failure;

/**
 *  获取特定格式字符串，"key1=value1&key2=value2&key3=value3"
 */
+ (NSString *)getStanderdString:(NSDictionary *)dic;
+ (NSString *)deviceInfo;
@end

/*
 base url后边记着添加‘\’，relative url前边不要添加'\'
 */
