//
//  ZCPlatformRequest.h
//  ZCLibProjectDEV
//
//  Created by Happy on 8/27/16.
//  Copyright © 2016 Happy. All rights reserved.
//

#import "ZCRequest.h"
#import "ZCPlatformApi.h"

#define ZCAPIV 2 //接口版本
#define ZCPLATFORM 1 //手机平台（1:ios, 2:and, 3:wp）

#define ZCPlatformAuthkeyKey @"authkey" //登录成功返回的authkey
#define ZCPlatformUidKey @"uId"
#define ZCPlatformSessionKey @"session"

#define ZCPara_Key_Sig  @"sig"
#define ZCPara_Key_Sys  @"sys"

@interface ZCPlatformRequest : ZCRequest
+ (void)request:(NSString *)url parameter:(NSDictionary *)parameter success:(RequestSuccess)success failure:(RequestFailure)failure;
@end
