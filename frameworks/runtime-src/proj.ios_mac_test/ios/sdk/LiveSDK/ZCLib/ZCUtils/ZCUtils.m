//
//  ZCUtils.m
//  HuaRong
//
//  Created by Happy on 8/31/16.
//  Copyright © 2016 Happy. All rights reserved.
//

#import "ZCUtils.h"

@implementation ZCUtils

#pragma mark get Hexadecimal color
+ (UIColor *)colorWithHexString:(NSString *)hexString alpha:(CGFloat)alpha {
    //删除字符串中的空格
    NSString *cString = [[hexString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];
    // String should be 6 or 8 characters
    if ([cString length] < 6)
    {
        return [UIColor clearColor];
    }
    // strip 0X if it appears
    //如果是0x开头的，那么截取字符串，字符串从索引为2的位置开始，一直到末尾
    if ([cString hasPrefix:@"0X"])
    {
        cString = [cString substringFromIndex:2];
    }
    //如果是#开头的，那么截取字符串，字符串从索引为1的位置开始，一直到末尾
    if ([cString hasPrefix:@"#"])
    {
        cString = [cString substringFromIndex:1];
    }
    if ([cString length] != 6)
    {
        return [UIColor clearColor];
    }
    
    // Separate into r, g, b substrings
    NSRange range;
    range.location = 0;
    range.length = 2;
    //r
    NSString *rString = [cString substringWithRange:range];
    //g
    range.location = 2;
    NSString *gString = [cString substringWithRange:range];
    //b
    range.location = 4;
    NSString *bString = [cString substringWithRange:range];
    
    // Scan values
    unsigned int r, g, b;
    [[NSScanner scannerWithString:rString] scanHexInt:&r];
    [[NSScanner scannerWithString:gString] scanHexInt:&g];
    [[NSScanner scannerWithString:bString] scanHexInt:&b];
    return [UIColor colorWithRed:((float)r / 255.0f) green:((float)g / 255.0f) blue:((float)b / 255.0f) alpha:alpha];
}
+ (UIColor *)colorWithHexString:(NSString *)hexString {
    return [self colorWithHexString:hexString alpha:1.0f];
}


#pragma mark - 处理富文本
+ (NSAttributedString *)textAttributedString:(NSString *)text color:(UIColor *)color {
    NSDictionary *attribs = @{NSForegroundColorAttributeName:color};
    
    NSMutableAttributedString *strM = [[NSMutableAttributedString alloc] initWithString:text attributes:attribs];
    
    return strM;
}
+ (NSAttributedString *)imageAttributedString:(NSString *)imageName bounds:(CGRect)bounds {
    NSTextAttachment *attachMent = [[NSTextAttachment alloc] init];
    // 设置图片
    attachMent.image = [UIImage imageNamed:imageName];
    // 设置大小
    attachMent.bounds = bounds;
    // 添加
    // 2.使用附件创建属性字符串
    NSAttributedString *attrString = [NSAttributedString attributedStringWithAttachment:attachMent];
    
    return attrString;
}
+ (NSAttributedString *)attributedStr:(NSArray *)textArr color:(NSArray *)colorArr {
    NSMutableAttributedString *strM = [[NSMutableAttributedString alloc] initWithString:@""];
    
    for (int index = 0; index < textArr.count; index++) {
        UIColor *color = [UIColor whiteColor];
        if (colorArr) {
            color = colorArr[index];
        }
        NSDictionary *attribs = @{NSForegroundColorAttributeName:color};
        
        NSString *text = textArr[index];
        NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:text attributes:attribs];
        
        [strM appendAttributedString:str];
    }
    
    return strM;
}


#pragma mark - 处理图片
+ (NSData *)imageCompress:(UIImage *)sourceImage targetWidth:(CGFloat)targetWidth compression:(CGFloat)compression {
    CGSize imageSize = sourceImage.size;
    
    CGFloat width = imageSize.width;
    CGFloat height = imageSize.height;
    
    CGFloat targetHeight = (targetWidth / width) * height;
    
    UIGraphicsBeginImageContext(CGSizeMake(targetWidth, targetHeight));
    [sourceImage drawInRect:CGRectMake(0, 0, targetWidth, targetHeight)];
    
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return UIImageJPEGRepresentation(newImage, compression);
}

- (UIImage *)imageFromColor:(UIColor *)color rect:(CGRect)rect {
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    UIImage *theImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return theImage;
}

#pragma mark - URL处理
+ (NSString *)URLEncodeString:(NSString *)URLString {
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 9.0) {
        return [URLString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!*'();:,.@&=+$/?%#[]_-{}|\""]];
    } else {
        return [URLString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    }
}
+ (NSString *)URLDecodeString:(NSString *)URLString {
    NSString *result = [(NSString *)URLString stringByReplacingOccurrencesOfString:@"+" withString:@" "];
    
    result = [result stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    return result;
}


+ (NSString *)base64EncodedString:(NSString *)string
{
    NSData *data = [string dataUsingEncoding:NSUTF8StringEncoding];
    return [data base64EncodedStringWithOptions:0];
}

+ (NSString *)base64DecodedString:(NSString *)string
{
    NSData *data = [[NSData alloc]initWithBase64EncodedString:string options:0];
    return [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
}
@end
