//
//  ZCUtils.h
//  HuaRong
//
//  Created by Happy on 8/31/16.
//  Copyright © 2016 Happy. All rights reserved.
//  一些实用工具

#import <Foundation/Foundation.h>

#import "ZCLocalData.h"
#import "LGLocalization.h"
#import "ZCPaymentManager.h"
#import "ZCEncrypt.h"

@interface ZCUtils : NSObject
/**
 *  通过十六进制获取UIColor
 *
 *  @param color 十六进制颜色
 *  @param alpha 透明度
 *
 *  @return UIColor
 */
+ (UIColor *)colorWithHexString:(NSString *)hexString alpha:(CGFloat)alpha;
+ (UIColor *)colorWithHexString:(NSString *)hexString;


/**
 *  获取特定颜色的字符串富文本
 */
+ (NSAttributedString *)textAttributedString:(NSString *)text color:(UIColor *)color;
/**
 *  获取特定Bounds的图片富文本
 */
+ (NSAttributedString *)imageAttributedString:(NSString *)imageName bounds:(CGRect)bounds;
/**
 *  根据不同颜色组合富文本
 *
 *  @param textArr 富文本数组
 *  @param colorArr 颜色数组，如果为空，则默认白色
 *
 *  @return
 */
+ (NSAttributedString *)attributedStr:(NSArray *)textArr color:(NSArray *)colorArr;


/**
 *  图片压缩处理
 *
 *  @param image       图片
 *  @param targetSize  目标尺寸
 *  @param compression 压缩比例系数(大于0，小于等于1)
 *
 *  @return
 */
+ (NSData *)imageCompress:(UIImage *)sourceImage targetWidth:(CGFloat)targetWidth compression:(CGFloat)compression;
/**
 *  根据color创建UIImage
 *
 *  @param color
 *
 *  @return
 */
- (UIImage *)imageFromColor:(UIColor *)color rect:(CGRect)rect;


/**
 *  URL encode or decode
 *
 *  @param URLString 待处理的url
 *
 *  @return
 */
+ (NSString *)URLEncodeString:(NSString *)URLString;
+ (NSString *)URLDecodeString:(NSString *)URLString;


/**
 String Base64 encode or decode

 @param string 参数
 @return 
 */
+ (NSString *)base64EncodedString:(NSString *)string;
+ (NSString *)base64DecodedString:(NSString *)string;
@end
