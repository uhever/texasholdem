//
//  ZCThirdDelegate.h
//  GameDemo
//
//  Created by Happy on 1/16/17.
//  Copyright © 2017 Happy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ZCShare.h"
#import "ZCLoginManager.h"

#import "WXApi.h"

#if ZBShareFaceBookIsOpen
#import <FBSDKShareKit/FBSDKShareKit.h>

@interface ZCThirdDelegate : NSObject <WXApiDelegate, FBSDKSharingDelegate>
+ (ZCThirdDelegate *)sharedInstance;
@end

#else

@interface ZCThirdDelegate : NSObject <WXApiDelegate>
+ (ZCThirdDelegate *)sharedInstance;
@end

#endif
