//
//  ZCThirdDelegate.m
//  GameDemo
//
//  Created by Happy on 1/16/17.
//  Copyright © 2017 Happy. All rights reserved.
//

#import "ZCThirdDelegate.h"

@implementation ZCThirdDelegate
+ (ZCThirdDelegate *)sharedInstance {
    static ZCThirdDelegate *share = nil;
    static dispatch_once_t predicate;
    dispatch_once(&predicate, ^{
        share = [[ZCThirdDelegate alloc] init];
    });
    return share;
}

#pragma mark WXApiDelegate
- (void)onReq:(BaseReq *)req {
    
}
-(void)onResp:(BaseResp*)resp {
    if([resp isKindOfClass:[SendAuthResp class]]) {
        // 微信登陆回调
        NSString *code = ((SendAuthResp *)resp).code;
        if (code) {
            [[ZCLoginManager sharedInstance] loginCallback:nil result:@{@"code":((SendAuthResp *)resp).code} loginType:ZBLoginTypeWeiXin];

        } else {
            NSError *error = [NSError errorWithDomain:@"com.zbsdk.login" code:resp.errCode userInfo:@{NSLocalizedDescriptionKey:resp.errStr ? resp.errStr:@"微信登录失败"}];
            [[ZCLoginManager sharedInstance] loginCallback:error result:@{} loginType:ZBLoginTypeWeiXin];

        }
    } else if([resp isKindOfClass:[SendMessageToWXResp class]]) {
        if (resp.errCode == 0) {
            [[ZCShare shareInstance] shareCallback:nil result:@{} loginType:ZCBShareTypeWeiXin];

        } else {
            NSError *error = [NSError errorWithDomain:@"com.zbsdk.share" code:resp.errCode userInfo:@{NSLocalizedDescriptionKey:resp.errStr ? resp.errStr:@"微信分享失败"}];
            [[ZCShare shareInstance] shareCallback:error result:@{} loginType:ZCBShareTypeWeiXin];

        }
    }
}


#pragma mark - FBSDKSharingDelegate
#if ZBShareFaceBookIsOpen
- (void)sharer:(id<FBSDKSharing>)sharer didCompleteWithResults:(NSDictionary *)results {
    [[ZCShare shareInstance] shareCallback:nil result:@{} loginType:ZCBShareTypeFacebook];
}

- (void)sharer:(id<FBSDKSharing>)sharer didFailWithError:(NSError *)error {
    [[ZCShare shareInstance] shareCallback:error result:@{} loginType:ZCBShareTypeFacebook];
}

- (void)sharerDidCancel:(id<FBSDKSharing>)sharer {
    NSError *error = [NSError errorWithDomain:@"com.zcbsdk.share" code:-2 userInfo:@{NSLocalizedDescriptionKey:@"用户取消"}];
    [[ZCShare shareInstance] shareCallback:error result:@{} loginType:ZCBShareTypeFacebook];
}
#endif
@end
