//
//  ZCEncrypt.h
//  ZCLibProjectDEV
//
//  Created by Happy on 8/27/16.
//  Copyright © 2016 Happy. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ZCEncrypt : NSObject
/**
 *  sha1加密
 */
+ (NSString *)encryptWithsha1:(NSString *)input;

/**
 *  AES加密解密
 *
 *  @param input 需要加解密的字符串
 *  @param key   key
 *
 */
+ (NSString *)encryptWithAES256:(NSString *)input key:(NSString *)key;
+ (NSString *)decryptWithAES256:(NSString *)input key:(NSString *)key;


/**
 *  获取字典对象对应的json串
 */
+ (NSString*)getJsonString:(NSDictionary*)dic;
/**
 *  获取"key1=value1&key2=value2"格式的字符串
 */
+ (NSString *)getStanderdString:(NSDictionary *)dic;
@end
