//
//  ZCApplePay.m
//  ZCLibProjectDEV
//
//  Created by Happy on 8/28/16.
//  Copyright © 2016 Happy. All rights reserved.
//

#import "ZCApplePay.h"
#import "ZCPlatformRequest.h"
#import "ZCDeviceInfo.h"
#import "ZCFMDBManager.h"
#import "ZCLocalData.h"
#import "ZCUtils.h"

@interface ZCApplePay ()
@property (nonatomic, strong) NSArray *productArr; //苹果商品列表
@property (nonatomic, strong) ZCApplePayItem *payItem;
@end

@implementation ZCApplePay
static ZCApplePay *manager = nil;
+ (ZCApplePay *)shareInstance {
    if (!manager) {
        manager = [[ZCApplePay alloc] init];
    }
    return manager;
}

- (NSArray *)productIdentifierArr {
    if (!_productIdentifierArr) {
        NSArray *productList = @[@".diamond1", @".diamond2", @".diamond3", @".diamond4", @".diamond5", @".diamond6"];
        
        NSString *bundleId = [ZCLocalData preferencePlistObject:@"CFBundleIdentifier"];
        NSMutableArray *tempMutArr = [NSMutableArray array];
        for (int index = 0; index < productList.count; index++) {
            [tempMutArr addObject:[bundleId stringByAppendingString:productList[index]]];
        }
        //        return @[@"com.zhanle.showtime.app.diamond1",
        //                 @"com.zhanle.showtime.app.diamond2",
        //                 @"com.zhanle.showtime.app.diamond3",
        //                 @"com.zhanle.showtime.app.diamond4",
        //                 @"com.zhanle.showtime.app.diamond5",
        //                 @"com.zhanle.showtime.app.diamond6"];
        return tempMutArr;
    } else {
        return _productIdentifierArr;
    }
}

- (void)registerTransactionObserver {
    [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
}
- (void)payWithItem:(ZCApplePayItem *)payItem {
    [[SKPaymentQueue defaultQueue] removeTransactionObserver:self];
    [self registerTransactionObserver];
    
    self.payItem = payItem;
    
    //    [LGTool startLoading];
    
    if (self.productArr.count == 0) {
        [self requestProducts];
    } else {
        [self getOrderFromPlatformServer];
    }
}

#pragma mark 向苹果服务器发送请求商品列表请求
- (void)requestProducts {
    NSMutableSet *productIdentifiers = [NSMutableSet set];
    if (self.productIdentifierArr.count > 0) {
        for (int i=0; i< self.productIdentifierArr.count; i++) {
            [productIdentifiers addObject:self.productIdentifierArr[i]];
        }
    }
    SKProductsRequest *request = [[SKProductsRequest alloc]initWithProductIdentifiers:productIdentifiers];
    request.delegate = self;
    [request start];
}
- (void)request:(SKRequest *)request didFailWithError:(NSError *)error {
//    [LGTool stopLoading];
    
    if ([self.payDelegate respondsToSelector:@selector(applePayPruchaseCompletion:error:)]) {
        [self.payDelegate applePayPruchaseCompletion:self.payItem.orderId error:error];
    }
}
- (void)productsRequest:(SKProductsRequest *)request didReceiveResponse:(SKProductsResponse *)response {
    self.productArr = response.products;
    
    if (self.productArr.count == 0) {
//        [LGTool stopLoading];
        if ([self.payDelegate respondsToSelector:@selector(applePayPruchaseCompletion:error:)]) {
            NSError *error = [NSError errorWithDomain:ZCPayErrorDomain code:ZCPayErrorCodeFailure userInfo:@{NSLocalizedDescriptionKey:LGLocalizedString(@"payFailureApple", @"向苹果服务器请求回来的商品列表为空")}];

            [self.payDelegate applePayPruchaseCompletion:self.payItem.orderId error:error];
        }
    } else {
        [self getOrderFromPlatformServer];
    }
}

#pragma mark 向商家服务器下订单
- (void)getOrderFromPlatformServer {
    NSDictionary *parameter = @{@"appGoodsId":self.productIdentifierArr[self.payItem.goodIndex],
                                @"appGoodsName":self.payItem.goodName,
                                @"appGoodsPrice":self.payItem.goodPrice,
                                @"appOrderId":self.payItem.orderId,
                                @"appCallback":[ZCPaymentManager shareInstance].payCallback,
                                @"session":@""};
    
    [ZCPlatformRequest request:ZCURL_ApplePay_Init parameter:parameter success:^(NSDictionary *dic) {
        
        //如果需要展示本地订单信息，则保存
        /*
         NSString *newOrderId = [dic objectForKey:@"orderId"];
        [ZCTool saveNewOrderInfo:[NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:newOrderId, payItem.payGoodPrice, payItem.payGoodName, @"-1", [LGTool getDateStr:[NSDate date] withDateStyle:@"MM-dd HH:mm"], [LGTool getObjectFromUserDefaultForKey:ZCUser_UID], nil] forKeys:[NSArray arrayWithObjects:@"orderId", @"goodPrice", @"goodName", @"orderState", @"transimateDate", @"uid", nil]]];
        
        [ZCTool saveNewOrderId:newOrderId];
         */
//        [LGTool startLoading];
        [self buyProductIdentifier:self.productArr[self.payItem.goodIndex] platformOrderId:[dic objectForKey:@"orderId"]];
        
    } failure:^(NSError *error) {
        [self.payDelegate applePayPruchaseCompletion:self.payItem.orderId error:error];

    }];
}

#pragma mark 商家服务器下单成功后，调用苹果支付接口
- (void)buyProductIdentifier:(SKProduct *)product platformOrderId:(NSString *)platformOrderId {
    SKMutablePayment *payment = [SKMutablePayment paymentWithProduct:product];
    payment.applicationUsername = platformOrderId;
    
//    _nofifTimer = [NSTimer scheduledTimerWithTimeInterval:60 target:self selector:@selector(timeCount:) userInfo:nil repeats:NO];
    [[SKPaymentQueue defaultQueue] addPayment:payment];
}
- (void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray *)transactions {
    for (SKPaymentTransaction *transaction in transactions)
    {
//        NSLog(@"updatedTransactions->transaction.transactionState: %d", (int)transaction.transactionState);
        switch (transaction.transactionState)
        {
            case SKPaymentTransactionStatePurchased:
//                if (_nofifTimer) {
//                    [self timeCount:_nofifTimer];
//                }
                [self completeTransaction:transaction];
                break;
            case SKPaymentTransactionStateFailed:
//                if (_nofifTimer) {
//                    [self timeCount:_nofifTimer];
//                }
                [self failedTransaction:transaction];
                break;
            case SKPaymentTransactionStateRestored:
                [self restoreTransaction:transaction];
                break;
            case SKPaymentTransactionStatePurchasing:
//                NSLog(@"Transaction is being added to the server queue.");
                break;
            default:
                break;
        }
    }
}
- (void)restoreTransaction:(SKPaymentTransaction *)transaction {
    [[SKPaymentQueue defaultQueue]finishTransaction:transaction];
}
- (void)completeTransaction:(SKPaymentTransaction *)transaction {
    [[SKPaymentQueue defaultQueue]finishTransaction:transaction];
    // 如果这个交易已经有过回调，则屏蔽
    if (![[ZCFMDBManager shareInstance] appleTransactionIdentifierSave:transaction.transactionIdentifier]) {
        return;
    }
    
    // 处理苹果交易凭据
    NSData *receiptData = [NSData dataWithContentsOfURL:[[NSBundle mainBundle] appStoreReceiptURL]];
    NSString * receipt = [receiptData base64EncodedStringWithOptions:NSDataBase64EncodingEndLineWithLineFeed];
    
    // 将交易结果上传服务器
    NSString *orderId = transaction.payment.applicationUsername;
    if (orderId) {
        [[ZCFMDBManager shareInstance] appleOrderSave:@{ZCApplePayUidKey:[ZCLocalData userDefaultObject:ZCPlatformUidKey], ZCApplePayOrderIdKey:orderId, ZCApplePayReceiptKey:receipt}];

        [self noticePlatformServer:receipt orderId:orderId];
    }
}
- (void)failedTransaction:(SKPaymentTransaction *)transaction {
    [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
    
    if ([self.payDelegate respondsToSelector:@selector(applePayPruchaseCompletion:error:)]) {
//        NSString *orderId = transaction.payment.applicationUsername;
        [self.payDelegate applePayPruchaseCompletion:self.payItem.orderId error:transaction.error];
    }
}

// 向苹果购买成功，通知平台服务器
- (void)noticePlatformServer:(NSString *)receipt orderId:(NSString *)orderId {
    // 将结果通知给商家服务器
    NSDictionary *parameter = @{ZCApplePayReceiptKey:receipt,
                                ZCApplePayOrderIdKey:orderId,
                                @"retry":@"0",
                                @"session":@"",
                                @"receiptType":@"2",
                                @"appVersion":[ZCLocalData preferencePlistObject:@"CFBundleShortVersionString"],
                                @"retry":@"0"};
    
    __weak typeof(self)weakSelf = self;
    __strong typeof(self)strongSelf = weakSelf;
    [ZCPlatformRequest request:ZCURL_ApplePay_CheckOrder parameter:parameter success:^(NSDictionary *dic) {
        [[ZCFMDBManager shareInstance] appleOrderDeleteByOrderId:dic[@"orderId"]];

        if ([strongSelf.payDelegate respondsToSelector:@selector(applePayPruchaseCompletion:error:)]) {
            [strongSelf.payDelegate applePayPruchaseCompletion:dic[@"appOrderId"] error:nil];
        }
    } failure:^(NSError *error) {
        if (error.code != ZCRequestErrorCodeNoNetwork) {
            [[ZCFMDBManager shareInstance] appleOrderDeleteByOrderId:orderId];
        }
        
        if ([strongSelf.payDelegate respondsToSelector:@selector(applePayPruchaseCompletion:error:)]) {
            [strongSelf.payDelegate applePayPruchaseCompletion:self.payItem.orderId error:error];
        }
    }];
}


- (void)retryNoticePlatformServerTheOrderUnSubmit {
    NSString *uid = [ZCLocalData userDefaultObject:ZCPlatformUidKey];
    NSArray *orderUnSubmitedArr = [[ZCFMDBManager shareInstance] appleOrderByUid:uid];
    
    for (NSDictionary *childDic in orderUnSubmitedArr) {
        NSString *orderId = [childDic objectForKey:ZCApplePayOrderIdKey];
        NSString *receipt = [childDic objectForKey:ZCApplePayReceiptKey];
        
        NSDictionary *parameter = @{ZCApplePayReceiptKey:receipt,
                                    ZCApplePayOrderIdKey:orderId,
                                    @"retry":@"1",
                                    @"session":@""};
        [ZCPlatformRequest request:ZCURL_ApplePay_CheckOrder parameter:parameter success:^(NSDictionary *dic) {
            
            [[ZCFMDBManager shareInstance] appleOrderDeleteByOrderId:orderId];

        } failure:^(NSError *error) {
            if (error.code != ZCRequestErrorCodeNoNetwork) {
                [[ZCFMDBManager shareInstance] appleOrderDeleteByOrderId:orderId];
            }
        }];
    }
}
@end
