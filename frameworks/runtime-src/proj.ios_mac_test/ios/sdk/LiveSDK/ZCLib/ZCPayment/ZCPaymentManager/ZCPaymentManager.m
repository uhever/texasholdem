//
//  ZCPaymentManager.m
//  ZCDemo
//
//  Created by Happy on 8/26/16.
//  Copyright © 2016 Happy. All rights reserved.
//

#import "ZCPaymentManager.h"


#import "ZBManager.h"
#import "ZCPlatformRequest.h"
#import "ZCUtils.h"
#import "ZCLiveRequest.h"
#import "NIMProgressHUD.h"

NSString *const ZCPayTypeKey = @"paymentType";
NSString *const ZCPayOrderIdKey = @"paymentOrderId";
NSString *const ZCPayOrderChangeKey = @"payOrderChangeKey";

#define TimeLimitForLookOrderState 10 //轮询订单状态时长, 单位秒
#define CountForLookOrderState 5 //轮询订单状态上限次数

@interface ZCPaymentManager () <ZCApplePayDelegate> {
    NSInteger _goodIndex;
    ZCPayType _payType;
    BOOL _isPaying;
    NSDate *_recordDate; //订单计时器
    
    BOOL _isPayForGame;
    NSString *_gameGoodType; //游戏商品类型，月卡、年卡等
    NSString *_gameToken;
}
@property (nonatomic, copy) NSString *orderId;
@property (nonatomic, copy) ZCPayCompletion payCompletion;
@property (nonatomic, strong) NSMutableDictionary *payResult;
@property (nonatomic, assign) NSInteger lookOrderStateCount; //查询订单次数
@end


@implementation ZCPaymentManager
static ZCPaymentManager *paymentManager = nil;
+ (instancetype)shareInstance {
    if (!paymentManager) {
        paymentManager = [[ZCPaymentManager alloc] init];
        paymentManager.payResult = [NSMutableDictionary dictionary];
    }
    return paymentManager;
}

// 支付回调地址
- (NSString *)payCallback {
    if (!_payCallback) {
        NSString *payCallback = [ZCBaseURLForPayServer_Dev stringByAppendingString:ZCURL_Pay_CallBack];
        if ([ZBSDKConfigure sharedInstance].isOnline) {
            payCallback = [ZCBaseURLForPayServer_Dist stringByAppendingString:ZCURL_Pay_CallBack];
        }

        return payCallback;
    } else {
        return _payCallback;
    }
}

- (void)payForApp:(NSInteger)goodIndex payType:(ZCPayType)payType completion:(ZCPayCompletion)completion {
    _isPayForGame = NO;
    
    [self doSDKPay:goodIndex payType:payType completion:completion];
}

- (void)payForGame:(NSDictionary *)parameter payType:(ZCPayType)payType completion:(ZCPayCompletion)completion {
    _isPayForGame = YES;
    _gameGoodType = parameter[@"type"];
    _gameToken = parameter[@"token"];
    
    [self doSDKPay:[parameter[@"id"] intValue] payType:payType completion:completion];
}


#pragma mark - 苹果支付
- (void)payInApple:(NSDictionary *)payDic {
    //设置苹果支付回调代理
    if (![ZCApplePay shareInstance].payDelegate) {
        [ZCApplePay shareInstance].payDelegate = self;
    }
    
    ZCApplePayItem *item = [[ZCApplePayItem alloc] init];
    item.orderId = payDic[@"app_order_id"];
    item.goodIndex = _goodIndex - 1;
    item.goodName = payDic[@"product_name"];
    
    int goodPrice = [payDic[@"amount"] intValue];
    item.goodPrice = [NSString stringWithFormat:@"%d", goodPrice*100];
    
    [[ZCApplePay shareInstance] payWithItem:item];
}

- (void)applePayPruchaseCompletion:(NSString *)orderId error:(NSError *)error {
    if (!error) {
        [self loolupOrderState:orderId];
        
    } else {
        if (self.payCompletion) {
            self.payCompletion(error, self.payResult);
        }
    }
}




#pragma mark - Private method
- (NSString *)payTypeString:(ZCPayType)payType {
    NSString *typeStr = @"ios";
    if (payType == ZCPayTypeWeiXin) {
        typeStr = @"wx";
    } else if (payType == ZCPayTypeAliPay) {
        typeStr = @"ali";
    } else if (payType == ZCPayTypeAipppay) {
        typeStr = @"aipp";
    }
    
    return typeStr;
}
- (NSString *)platformPayChannel {
    if (_payType == ZCPayTypeWeiXin) {
        return @"wx";
    } else if (_payType == ZCPayTypeAliPay) {
        return @"alipay";
    }
    
    return @"";
}

- (NSString *)weixinScheme {
    NSDictionary *plistDic = [[NSBundle mainBundle] infoDictionary];
    NSArray *schemeArr = plistDic[@"CFBundleURLTypes"];
    NSDictionary *wxSchemeDic = schemeArr.firstObject;
    NSArray *weixinSchemeArr = wxSchemeDic[@"CFBundleURLSchemes"];
    
    return weixinSchemeArr.firstObject;
}

- (void)doSDKPay:(NSInteger)goodIndex payType:(ZCPayType)payType completion:(ZCPayCompletion)completion {
    if (payType == ZCPayTypeAipppay) { //爱贝支付需要appid和platformKey
        if ([ZBSDKConfigure sharedInstance].aipppayAppId.length == 0 ||
            [ZBSDKConfigure sharedInstance].aipppayPlatformKey.length == 0) {
            NSError *error = [NSError errorWithDomain:ZCPayErrorDomain code:ZCPayResultMissParameter userInfo:@{NSLocalizedDescriptionKey:LGLocalizedString(@"", @"缺少爱贝支付必要的参数，请使用方法setAipppayAppId: platformKey:配置")}];
            
            completion(error, nil);
            return;
        }
    }
    
    
    _isPaying = YES;
    _goodIndex = goodIndex;
    _payType = payType;
    
    self.payCompletion = completion;
    [self.payResult removeAllObjects];
    [self.payResult setObject:@(payType) forKey:ZCPayTypeKey];
    
    __weak typeof(self)weakSelf = self;
    [self createOrderFromLiveServer:^(NSError *error, NSDictionary *dic) {
        [self.payResult setObject:dic[@"app_order_id"] forKey:ZCPayOrderIdKey];

        weakSelf.orderId = dic[@"app_order_id"];
        [weakSelf perfomCharge:dic];
    }];
}

// 根据支付方式参数，调用不同的支付方法
- (void)perfomCharge:(NSDictionary *)payParameter {
    if (_payType == ZCPayTypeApple) {
        [self payInApple:payParameter];
    }
}


#pragma mark 在app服务器创建订单
// 在app服务器创建订单
- (void)createOrderFromLiveServer:(void(^)(NSError *error, NSDictionary *dic))callback {

    NSMutableDictionary *parameter = [NSMutableDictionary dictionaryWithDictionary:@{@"appid":[ZBSDKConfigure sharedInstance].appid, @"pay_type":[self payTypeString:_payType], @"id":[NSString stringWithFormat:@"%d", (int)_goodIndex], @"server_review":[NSString stringWithFormat:@"%d", ![ZBSDKConfigure sharedInstance].isReview]}];
    if (_isPayForGame) {
        [parameter setObject:_gameToken forKey:@"token"];
        [parameter setObject:_gameGoodType forKey:@"type"];
        [parameter setObject:@"1001" forKey:@"server_poker"];
        
    } else {
        [parameter setObject:[ZBSDKConfigure sharedInstance].token forKey:@"token"];
        [parameter setObject:@"0" forKey:@"type"];
        
    }
    
    [ZCLiveRequest requestPayServer:ZCURL_Create_Prepay_Order parameter:parameter success:^(NSDictionary *dic) {
        callback(nil, dic);
        
    } failure:^(NSError *error) {
        NSError *newError = [NSError errorWithDomain:ZCPayErrorDomain code:error.code userInfo:@{NSLocalizedDescriptionKey:LGLocalizedString(@"payFailure", @"支付失败")}];
        self.payCompletion(newError, self.payResult);
        
    }];
}


#pragma mark 从app服务器查询订单状态
- (void)loolupOrderState:(NSString *)orderId {
    // 游戏支付时，不需要走查询订单逻辑
    if (_isPayForGame) {
        _isPayForGame = NO;
        self.payCompletion(nil, self.payResult);

        return;
    }
    
    
    [NIMProgressHUD showWithStatus:LGLocalizedString(@"lookingOrder", @"查询订单中") maskType:NIMProgressHUDMaskTypeClear];
    
    _recordDate = [NSDate date]; //开始计时
    self.lookOrderStateCount = 0;
    _orderId = orderId;
    
    __weak typeof(self)weakSelf = self;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        NSTimer *timer = [NSTimer scheduledTimerWithTimeInterval:2 target:self selector:@selector(loopForOrderState:) userInfo:nil repeats:YES];
        
        [weakSelf loopForOrderState:timer];
    });
}
- (void)loopForOrderState:(NSTimer *)timer {
    // 超时处理
    NSString *timeLength = [NSString stringWithFormat:@"%f", [[NSDate date] timeIntervalSinceDate:_recordDate]];
    
    self.lookOrderStateCount++;
    if (timeLength.intValue >= TimeLimitForLookOrderState || self.lookOrderStateCount > CountForLookOrderState) {
        NSError *error = [NSError errorWithDomain:ZCPayErrorDomain code:ZCPayResultTimeout userInfo:@{NSLocalizedDescriptionKey:LGLocalizedString(@"payTimeout", @"支付超时")}];
        self.payCompletion(error, self.payResult);
        [timer invalidate];
        timer = nil;
        _recordDate = nil;
        return;
    }
    
    [ZCLiveRequest requestAppServer:ZCURL_Lookup_Order_State parameter:@{@"order_id":_orderId, @"token":[ZBSDKConfigure sharedInstance].token} success:^(NSDictionary *dic) {
        _recordDate = nil;

        [self.payResult setObject:dic forKey:ZCPayOrderChangeKey];
                
        [timer invalidate];
        
        self.payCompletion(nil, self.payResult);
    } failure:^(NSError *error) {
        if (error.code == -3) { //订单未完成
            //次数可以给用户点温馨小提示，以缓解用户焦急的心情
            
        } else {
            NSError *error = [NSError errorWithDomain:ZCPayErrorDomain code:ZCPayResultFailure userInfo:@{NSLocalizedDescriptionKey:LGLocalizedString(@"payFailure", @"支付失败")}];
            self.payCompletion(error, self.payResult);
            [timer invalidate];
        }
    }];
}
@end
