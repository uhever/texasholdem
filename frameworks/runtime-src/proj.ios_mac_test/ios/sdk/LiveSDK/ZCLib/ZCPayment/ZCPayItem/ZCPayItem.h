//
//  ZCPayItem.h
//  ZCLibProjectDEV
//
//  Created by Happy on 8/28/16.
//  Copyright © 2016 Happy. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString *const ZCPayErrorDomain;

typedef NS_ENUM(NSInteger, ZCPayErrorCode) {
    ZCPayErrorCodeFailure = 100, //购买失败
    ZCPayErrorCodeTimeout //购买操作超时
};

//公共支付参数
@interface ZCPayItem : NSObject
@property (nonatomic, copy) NSString *orderId;
@end


//苹果支付参数
@interface ZCApplePayItem : ZCPayItem
@property (nonatomic, assign) NSInteger goodIndex; //从0开始
@property (nonatomic, copy) NSString *goodName;
@property (nonatomic, copy) NSString *goodPrice; //单位分

@end