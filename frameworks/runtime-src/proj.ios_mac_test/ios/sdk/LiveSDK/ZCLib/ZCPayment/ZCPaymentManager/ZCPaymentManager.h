//
//  ZCPaymentManager.h
//  ZCDemo
//
//  Created by Happy on 8/26/16.
//  Copyright © 2016 Happy. All rights reserved.
//  调用ZCApplePay中的苹果支付方法，收到支付成功回调后，去直播服务器轮询支付结果



#import <Foundation/Foundation.h>
#import "ZCApplePay.h"


//从支付结果中获取信息的一些Key
extern NSString *const ZCPayTypeKey; //支付方式Key
extern NSString *const ZCPayOrderIdKey; //订单Id
extern NSString *const ZCPayOrderChangeKey; //支付后，用户变动信息key


//支付结果
typedef NS_ENUM(NSInteger, ZCPayResult) {
    ZCPayResultSuccess = 100, //支付成功
    ZCPayResultFailure, //支付失败
    ZCPayResultTimeout, //支付超时
    ZCPayResultMissParameter //缺少参数
};

//支付方式
typedef NS_ENUM(NSInteger, ZCPayType) {
    ZCPayTypeApple, //苹果支付
    ZCPayTypeAliPay, //Ping++支付宝支付
    ZCPayTypeWeiXin, //Ping++微信支付, 使用微信支付必须要求用户安装微信客户端
    ZCPayTypeAipppay = 10, //爱贝支付
    ZCPayTypeOther
};

typedef void(^ZCPayCompletion)(NSError *error, NSDictionary *payResult);

@interface ZCPaymentManager : NSObject
@property (nonatomic, readonly) BOOL isPaying; //是否正在支付
@property (nonatomic, copy) NSString *payCallback; //支付回调地址

+ (instancetype)shareInstance;


/**
 应用的支付接口，含订单状态查询功能

 @param goodIndex 商品索引，从1开始
 @param payType 支付方式
 @param completion 支付回调
 */
- (void)payForApp:(NSInteger)goodIndex payType:(ZCPayType)payType completion:(ZCPayCompletion)completion;

/**
 游戏的支付接口，不含订单状态查询功能

 @param parameter {@"id":@"产品ID，从1开始", @"type":@"产品类型，月卡、年卡等", @"token":@""}
 @param payType 支付方式
 @param completion 支付回调
 */
- (void)payForGame:(NSDictionary *)parameter payType:(ZCPayType)payType completion:(ZCPayCompletion)completion;




@end
