//
//  PhoneNumManager.m
//  cocos
//
//  Created by user on 2017/2/17.
//  Copyright © 2017年 refrainC. All rights reserved.
//

#import "PhoneNumManager.h"
#import <AddressBook/AddressBook.h>
#import "ContactModel.h"
#import "NSString+Firstcharater.h"
#define emptyString(value)   value == nil?@"":value
@interface PhoneNumManager ()

@property (nonatomic, strong)NSMutableArray *contacts;


@end

@implementation PhoneNumManager


- (void)loadAllPhoneNumWithComplete:(void(^)(NSArray <ContactModel *>* contactModels,
                                             NSArray <NSString *>*phoneNumbers,
                                             BOOL isSuccess))complete{
    ABAddressBookRef addBook = ABAddressBookCreateWithOptions(NULL, NULL);
    
    //等待同意后向下执行创建信号，发出信号， 等待信号
    dispatch_semaphore_t sema = dispatch_semaphore_create(0);
    ABAddressBookRequestAccessWithCompletion(addBook, ^(bool granted, CFErrorRef error)                                                 {
        dispatch_semaphore_signal(sema);
    });
    dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
    
    if (ABAddressBookGetAuthorizationStatus() != kABAuthorizationStatusAuthorized) {
        
        !complete ? : complete(@[],@[], NO);
        return ;
    }
    
    
    
    self.contacts = [NSMutableArray array];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        //取得本地所有联系人记录
        NSMutableArray *phones = [[NSMutableArray alloc] init];
        NSMutableArray *contacts = [[NSMutableArray alloc] init];
        
        CFArrayRef results = ABAddressBookCopyArrayOfAllPeople(addBook);
        for(int i = 0; i < CFArrayGetCount(results); i++)
        {
            ABRecordRef person = CFArrayGetValueAtIndex(results, i);
            //读取firstname
            NSString *personName = (__bridge NSString*)ABRecordCopyValue(person, kABPersonFirstNameProperty);
            
            //读取lastname
            NSString *lastname = (__bridge NSString*)ABRecordCopyValue(person, kABPersonLastNameProperty);
            
            //读取middlename
            NSString *middlename = (__bridge NSString*)ABRecordCopyValue(person, kABPersonMiddleNameProperty);
            
            NSString *name = [NSString stringWithFormat:@"%@%@%@",emptyString(lastname),emptyString(personName),emptyString(middlename)];
            //读取organization公司
            NSString *organization = (__bridge NSString*)ABRecordCopyValue(person, kABPersonOrganizationProperty);
            
            if (name.length == 0) name = organization;
            if (name.length == 0) continue ;
            
            //        NSData *image = (__bridge NSData*)ABPersonCopyImageData(person);
            //        UIImage *headImage = [UIImage imageWithData:image];
            //读取电话多值
            ABMultiValueRef phone = ABRecordCopyValue(person, kABPersonPhoneProperty);
            for (int k = 0; k<ABMultiValueGetCount(phone); k++){
                
                ContactModel *contactModel = [[ContactModel alloc]init];
                
                
                contactModel.name = name;
                contactModel.firstCharater  = [name firstCharater];
                
                NSString * personPhone = (__bridge NSString*)ABMultiValueCopyValueAtIndex(phone, k);
                contactModel.phone =[personPhone castSpecialChar];
                
                [phones addObject:contactModel.phone];
                
                
                
                
                [contacts addObject:contactModel];
                
            }
        }
         CFRelease(results);
        dispatch_async(dispatch_get_main_queue(), ^{
            !complete ? : complete(contacts, phones, YES);
        });
        
        
        
       
    });
    
}
- (NSString*)castSpecialCharWithString:(NSString *)string {
    NSString *str = [string stringByReplacingOccurrencesOfString:@"+86" withString:@""];
    str = [str stringByReplacingOccurrencesOfString:@"(" withString:@""];
    str = [str stringByReplacingOccurrencesOfString:@")" withString:@""];
    str = [str stringByReplacingOccurrencesOfString:@"-" withString:@""];
    str = [str stringByReplacingOccurrencesOfString:@" " withString:@""];
    str = [str stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    NSCharacterSet *set = [NSCharacterSet characterSetWithCharactersInString:@"@／：；（）¥「」＂、[]{}#%+-*=_\\|~＜＞$€^• '@#$%^&*()_'\""];
    return [[str componentsSeparatedByCharactersInSet: set]componentsJoinedByString:@""];
}

@end
