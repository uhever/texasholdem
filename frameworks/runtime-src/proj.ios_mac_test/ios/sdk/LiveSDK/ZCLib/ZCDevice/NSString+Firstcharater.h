//
//  NSString+Firstcharater.h
//  
//
//  Created by Samuel Lee on 16/1/22.
//
//

#import <Foundation/Foundation.h>

@interface NSString (Firstcharater)

@property (nonatomic, readonly) NSString *firstCharater;
- (NSString *)castSpecialChar;
@end
