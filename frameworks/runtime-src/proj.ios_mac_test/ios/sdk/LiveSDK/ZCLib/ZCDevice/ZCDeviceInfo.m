//
//  ZCDeviceInfo.m
//  ZCLibProjectDEV
//
//  Created by Happy on 8/27/16.
//  Copyright © 2016 Happy. All rights reserved.
//

#import "ZCDeviceInfo.h"
#import "ZCLocalData.h"
#import "LGReachability.h"
#import <AdSupport/AdSupport.h> //获取广告标示符的支持框架
#import <sys/utsname.h> //获取设备类型必备

@implementation ZCDeviceInfo
+ (NSString *)deviceUUIDString {
    NSString *uuid = [ZCLocalData keyChainObject:ZCPara_Key_UUID];
    if (!uuid) {
        // 新版本修改了ZCPara_Key_UUID对应的值，此处为了兼容老值uuid
        uuid = [ZCLocalData keyChainObject:@"uuid"];
        if (!uuid) {
            uuid = [[UIDevice currentDevice] identifierForVendor].UUIDString;
            [ZCLocalData keyChainSave:uuid account:ZCPara_Key_UUID];
        }
    }
    return uuid;
}

+ (NSString *)advertisingIdentifier {
    NSString *idfa = [ZCLocalData keyChainObject:ZCPara_Key_IDFA];
    if (!idfa) {
        // 新版本修改了ZCPara_Key_IDFA对应的值，此处为了兼容老值ZCIDFA
        idfa = [ZCLocalData keyChainObject:@"ZCIDFA"];
        if (!idfa) {
            idfa = [[[ASIdentifierManager sharedManager] advertisingIdentifier] UUIDString];
            [ZCLocalData keyChainSave:idfa account:ZCPara_Key_IDFA];
        }
    }
    return idfa;
}

+ (NSString *)deviceBrand {
    NSString *deviceType = [self deviceModel];
    
    if ([deviceType hasPrefix:@"iPhone"]) {
        return @"iPhone";
    } else if ([deviceType hasPrefix:@"iPod"]) {
        return @"iPod";
    } else if ([deviceType hasPrefix:@"iPad mini"]) {
        return @"iPad mini";
    } else  if ([deviceType hasPrefix:@"iPad air"]) {
        return @"iPad air";
    } else if ([deviceType hasPrefix:@"iPad"]) {
        return @"iPad";
    } else {
        return deviceType;
    }
}
+ (NSString *)deviceModel {
    NSArray *modelArray = @[
                            @"i386",
                            @"x86_64",
                            
                            @"iPhone1,1",
                            @"iPhone1,2",
                            @"iPhone2,1",
                            @"iPhone3,1",
                            @"iPhone3,2",
                            @"iPhone3,3",
                            @"iPhone4,1",
                            @"iPhone5,1",
                            @"iPhone5,2",
                            @"iPhone5,3",
                            @"iPhone5,4",
                            @"iPhone6,1",
                            @"iPhone6,2",
                            @"iPhone7,2",
                            @"iPhone7,1",
                            @"iPhone8,1",
                            @"iPhone8,2",
                            @"iPhone8,4",
                            
                            //iPod Touch 1-6
                            @"iPod1,1",
                            @"iPod2,1",
                            @"iPod3,1",
                            @"iPod4,1",
                            @"iPod5,1",
                            @"iPad7,1",
                            
                            //iPad 1
                            @"iPad1,1",
                            //iPad 2
                            @"iPad2,1",
                            @"iPad2,2",
                            @"iPad2,3",
                            @"iPad2,4",
                            //iPad 3
                            @"iPad3,1",
                            @"iPad3,2",
                            @"iPad3,3",
                            //iPad 4
                            @"iPad3,4",
                            @"iPad3,5",
                            @"iPad3,6",
                            
                            //iPad Air 1
                            @"iPad4,1",
                            @"iPad4,2",
                            @"iPad4,3",
                            //iPad Air 2
                            @"iPad5,3",
                            @"iPad5,4",
                            
                            //iPad mini 1
                            @"iPad2,5",
                            @"iPad2,6",
                            @"iPad2,7",
                            //iPad mini 2
                            @"iPad4,4",
                            @"iPad4,5",
                            @"iPad4,6",
                            //iPad mini 3
                            @"iPad4,7",
                            @"iPad4,8",
                            @"iPad4,9",
                            //iPad mini 4
                            @"iPad5,1",
                            @"iPad5,2",
                            
                            //iPad Pro
                            @"iPad6,3",
                            @"iPad6,4",
                            @"iPad6,7",
                            @"iPad6,8",
                            ];
    
    NSArray *modelNameArray = @[
                                @"iPhone Simulator",
                                @"iPhone Simulator",
                                
                                @"iPhone 1G",
                                @"iPhone 3G",
                                @"iPhone 3GS",
                                @"iPhone 4(GSM)",
                                @"iPhone 4(GSM Rev A)",
                                @"iPhone 4(CDMA)",
                                @"iPhone 4S",
                                @"iPhone 5(GSM)",
                                @"iPhone 5(GSM+CDMA)",
                                @"iPhone 5c(GSM)",
                                @"iPhone 5c(Global)",
                                @"iphone 5s(GSM)",
                                @"iphone 5s(Global)",
                                @"iPhone 6",
                                @"iPhone 6 Plus",
                                @"iPhone 6s",
                                @"iPhone 6s Plus",
                                @"iPhone SE",
                                
                                @"iPod Touch 1G",
                                @"iPod Touch 2G",
                                @"iPod Touch 3G",
                                @"iPod Touch 4G",
                                @"iPod Touch 5G",
                                @"iPod Touch 6G",
                                
                                @"iPad 1",
                                @"iPad 2",
                                @"iPad 2",
                                @"iPad 2",
                                @"iPad 2",
                                @"iPad 3",
                                @"iPad 3",
                                @"iPad 3",
                                @"iPad 4",
                                @"iPad 4",
                                @"iPad 4",
                                
                                @"iPad Air 1",
                                @"iPad Air 1",
                                @"iPad Air 1",
                                @"iPad Air 2",
                                @"iPad Air 2",
                                
                                @"iPad mini 1",
                                @"iPad mini 1",
                                @"ipad mini 1",
                                @"iPad mini 2",
                                @"iPad mini 2",
                                @"iPad mini 2",
                                @"iPad mini 3",
                                @"iPad mini 3",
                                @"iPad mini 3",
                                @"iPad mini 4",
                                @"iPad mini 4",
                                
                                @"iPad Pro(9.7 inch)",
                                @"iPad Pro(9.7 inch)",
                                @"iPad Pro(12.9 inch)",
                                @"iPad Pro(12.9 inch)"
                                ];
    
    struct utsname systemInfo;
    uname(&systemInfo);
    NSString *deviceString = [NSString stringWithCString:systemInfo.machine encoding:NSUTF8StringEncoding];
    if ([modelArray containsObject:deviceString]) {
        NSInteger index = [modelArray indexOfObject:deviceString];
        return [modelNameArray objectAtIndex:index];
    } else {
        return @"unknow apple device";
    }
}

+ (NSString *)networkName {
    LGReachability *reach = [LGReachability reachabilityWithHostName:@"www.apple.com"];
    if ([reach currentReachabilityStatus] == ReachableViaWiFi) {
        return @"WiFi";
    } else if ([reach currentReachabilityStatus] == ReachableViaWWAN) {
        return @"WWAN";
    }
    return @"UnKnow";
}
+ (ZCNetworkKind)networkKind {
    ZCNetworkKind networkKind = ZCNetworkKindOther;
    if ([[self networkName] isEqualToString:@"WiFi"]) {
        networkKind = ZCNetworkKindWLAN;
    } else if ([[self networkName] isEqualToString:@"WWAN"]) {
        networkKind = ZCNetworkKindCellular;
    }
    return networkKind;
}

+ (NSString *)deviceSystemVersion {
    return [[UIDevice currentDevice] systemVersion];
}

+ (NSString *)timeStamp {
    NSString *timeStamp = [NSString stringWithFormat:@"%ld", (long)([NSDate date].timeIntervalSince1970)];
    return timeStamp;
}
@end
