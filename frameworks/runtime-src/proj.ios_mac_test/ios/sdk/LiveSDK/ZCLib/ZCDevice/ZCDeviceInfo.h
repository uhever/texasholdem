//
//  ZCDeviceInfo.h
//  ZCLibProjectDEV
//
//  Created by Happy on 8/27/16.
//  Copyright © 2016 Happy. All rights reserved.
//

#import <Foundation/Foundation.h>
#include "UIKit/UIKit.h"
#import <AdSupport/AdSupport.h> //获取广告标示符的支持框架

#define ZCPara_Key_UUID @"ZBDeviceUUID"
#define ZCPara_Key_IDFA @"ZBDeviceIDFA"

#pragma mark 网络类型
typedef NS_ENUM(NSInteger, ZCNetworkKind) {
    ZCNetworkKind2G = 1,
    ZCNetworkKind3G,
    ZCNetworkKind4G,
    ZCNetworkKindWLAN, //无线局域网
    ZCNetworkKindCellular, //蜂窝网
    ZCNetworkKindOther
};

@interface ZCDeviceInfo : NSObject
/**
 *  获取identifierForVendor
 */
+ (NSString *)deviceUUIDString;

/**
 *  获取广告标示符
 */
+ (NSString *)advertisingIdentifier;

/**
 *  设备类型，比如iPhone、iPad、iPod、iPad air、iPad mini
 */
+ (NSString *)deviceBrand;
/**
 *  设备类型，比如iPhone1,1
 */
+ (NSString *)deviceModel;

/**
 *  网络类型，WiFi和WWAN
 */
+ (NSString *)networkName;
+ (ZCNetworkKind)networkKind;

/**
 *  设备系统版本号
 */

+ (NSString *)deviceSystemVersion;
/**
 *  十位长度的时间戳
 */
+ (NSString *)timeStamp;
@end
