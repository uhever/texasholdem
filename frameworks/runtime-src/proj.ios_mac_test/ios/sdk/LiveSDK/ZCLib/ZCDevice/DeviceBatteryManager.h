//
//  DeviceBatteryManager.h
//  cocos
//
//  Created by user on 2017/2/17.
//  Copyright © 2017年 refrainC. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void(^batteryChange)(float level);

@interface DeviceBatteryManager : NSObject

@property (nonatomic, copy)batteryChange batteryChange;

@property (nonatomic, assign)float currentBatteryLevel;


+ (instancetype)batteryManager;

@end
