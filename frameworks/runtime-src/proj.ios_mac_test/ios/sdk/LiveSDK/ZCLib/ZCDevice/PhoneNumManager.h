//
//  PhoneNumManager.h
//  cocos
//
//  Created by user on 2017/2/17.
//  Copyright © 2017年 refrainC. All rights reserved.
//

#import <Foundation/Foundation.h>
@class ContactModel;
@interface PhoneNumManager : NSObject

- (void)loadAllPhoneNumWithComplete:(void(^)(NSArray <ContactModel *>* contactModels,
                                             NSArray <NSString *>*phoneNumbers,
                                             BOOL isSuccess))complete;


@end
