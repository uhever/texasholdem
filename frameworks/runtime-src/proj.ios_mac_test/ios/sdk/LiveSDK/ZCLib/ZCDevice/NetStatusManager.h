//
//  NetStatusManager.h
//  cocos
//
//  Created by user on 2017/2/17.
//  Copyright © 2017年 refrainC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LGReachability.h"

typedef void(^NetStatusChange)(NetworkStatus status);

@interface NetStatusManager : NSObject

@property (nonatomic, assign) NetworkStatus status;

@property (nonatomic, copy)NetStatusChange NetStatusChange;

+ (NetStatusManager *)manager;



@end
