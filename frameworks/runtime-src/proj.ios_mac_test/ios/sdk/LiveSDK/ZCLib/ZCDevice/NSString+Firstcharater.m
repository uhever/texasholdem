//
//  NSString+Firstcharater.m
//  
//
//  Created by Samuel Lee on 16/1/22.
//
//

#import "NSString+Firstcharater.h"

@implementation NSString (Firstcharater)

/**
 *  汉字转拼音，返回大写首字母
 *
 *  @return NSString
 */
- (NSString *)firstCharater
{
    //转成了可变字符串
    NSMutableString *str = [NSMutableString stringWithString:self];
    //先转换为带声调的拼音
    CFStringTransform((CFMutableStringRef)str,NULL, kCFStringTransformMandarinLatin,NO);
    //再转换为不带声调的拼音
    CFStringTransform((CFMutableStringRef)str,NULL, kCFStringTransformStripDiacritics,NO);
    //转化为大写拼音
    NSString *pinYin = [str capitalizedString];
    //获取并返回首字母
    return [pinYin substringToIndex:1];
}

/**
 *  去掉特殊字符
 *
 *  @return NSString
 */
- (NSString*)castSpecialChar {
    NSString *str = [self stringByReplacingOccurrencesOfString:@"+86" withString:@""];
    str = [str stringByReplacingOccurrencesOfString:@"(" withString:@""];
    str = [str stringByReplacingOccurrencesOfString:@")" withString:@""];
    str = [str stringByReplacingOccurrencesOfString:@"-" withString:@""];
    str = [str stringByReplacingOccurrencesOfString:@" " withString:@""];
    str = [str stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    NSCharacterSet *set = [NSCharacterSet characterSetWithCharactersInString:@"@／：；（）¥「」＂、[]{}#%+-*=_\\|~＜＞$€^• '@#$%^&*()_'\""];
    return [[str componentsSeparatedByCharactersInSet: set]componentsJoinedByString:@""];
}

@end
