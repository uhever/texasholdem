//
//  LGLocalization.m
//  LvLuo
//
//  Created by Happy on 9/2/16.
//  Copyright © 2016 Happy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LGLocalization.h"
#import "ZCLocalData.h"

@interface LGLocalization ()
@property (nonatomic, copy) NSString *currentLanguage;
@end


@implementation LGLocalization
+ (instancetype)sharedInstance {
    static LGLocalization *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[LGLocalization alloc] init];
    });
    return sharedInstance;
}
+ (instancetype)localization {
    return [[LGLocalization alloc] init];
}

- (NSString *)localizedStringForKey:(NSString *)key value:(NSString *)value {
    value = [self localizedStringForKey:key value:value tableName:nil];
    
    return value;
}
- (NSString *)localizedStringForKey:(NSString *)key value:(NSString *)value tableName:(NSString *)tableName {
    NSBundle *localBundle = [NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource:self.currentLanguage ofType:@"lproj"]];
    NSString *resultStr = [localBundle localizedStringForKey:key value:value table:tableName];
    
    if (resultStr.length == 0) {
        return value;
    }
    return resultStr;
}

//修改当前语言，为了修改语言，还得替换rootViewcontroller
- (void)changeCurrentLanguage:(LGLanguageKind)languageKind {
    [ZCLocalData userDefaultSave:@(languageKind) key:@"AppLanguage"];
    
    Class rootVCClass = [[UIApplication sharedApplication].keyWindow.rootViewController class];
    [UIApplication sharedApplication].keyWindow.rootViewController = [[rootVCClass alloc] init];
}

- (NSString *)currentLanguage {
    // 如果项目内设置过语言，则使用项目内语言
    NSNumber *lanKindNumber = [ZCLocalData userDefaultObject:@"AppLanguage"];
    if (lanKindNumber) {
        return [self localLanguage:lanKindNumber.integerValue];
    }
    
    
    if (_isFollowSystemLanguage) {
        NSString *systemLanguage = [NSLocale preferredLanguages].firstObject;
        if ([systemLanguage hasPrefix:@"zh-Hans"]) {
            return @"zh-Hans";
        } else if ([systemLanguage hasPrefix:@"zh-Hant"]) {
            return @"zh-Hant";
        } else if ([systemLanguage hasPrefix:@"en"]) {
            return @"en";
        } else {
            return systemLanguage;
        }
    } else {
        return [self localLanguage:self.projectLanguage];
    }
}
- (LGLanguageKind)currentLanguageInt {
    NSString *language = @"zh-Hans";
    
    // 如果项目内设置过语言，则使用项目内语言
    NSNumber *lanKindNumber = [ZCLocalData userDefaultObject:@"AppLanguage"];
    if (lanKindNumber) {
        language = [self localLanguage:lanKindNumber.integerValue];
    }
    
    
    if (_isFollowSystemLanguage) {
        NSString *systemLanguage = [NSLocale preferredLanguages].firstObject;
        if ([systemLanguage hasPrefix:@"zh-Hans"]) {
            language = @"zh-Hans";
        } else if ([systemLanguage hasPrefix:@"zh-Hant"]) {
            language = @"zh-Hant";
        } else if ([systemLanguage hasPrefix:@"en"]) {
            language = @"en";
        }
    } else {
        language = [self localLanguage:self.projectLanguage];
    }

    if ([language hasPrefix:@"zh-Hans"]) {
        return LGLanguageKindZhs;
    } else if ([language hasPrefix:@"zh-Hant"]) {
        return LGLanguageKindZht;
    }
    
    return LGLanguageKindZhs;
}

#pragma mark - Private method
- (NSString *)localLanguage:(LGLanguageKind)languageKind {
    NSString *languagePlistPath = [[NSBundle mainBundle] pathForResource:@"LGLanguages" ofType:@"plist"];
    NSArray *languageArr = [[NSArray alloc] initWithContentsOfFile:languagePlistPath];
    if (languageArr.count == 0) {
        return @"zh-Hans";
    } else {
        NSDictionary *languageDic = languageArr[self.projectLanguage];
        return [languageDic objectForKey:@"en"];
    }
}
@end
