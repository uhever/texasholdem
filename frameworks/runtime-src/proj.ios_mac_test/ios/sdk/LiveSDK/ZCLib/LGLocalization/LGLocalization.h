//
//  LGLocalization.h
//  LvLuo
//
//  Created by Happy on 9/2/16.
//  Copyright © 2016 Happy. All rights reserved.
//

#import <Foundation/Foundation.h>

//comment:如果根据key取不到值，则返回comment的值
#define LGLocalizedString(key, comment) [[LGLocalization sharedInstance] localizedStringForKey:key value:comment]

typedef NS_ENUM(NSInteger, LGLanguageKind) {
    LGLanguageKindZhs, //简体中文
    LGLanguageKindZht, //繁体中文
    LGLanguageKindEn //英文
};

@interface LGLocalization : NSObject
@property (nonatomic, assign) BOOL isFollowSystemLanguage; //项目语言是否跟随系统语言而定
@property (nonatomic, assign) LGLanguageKind projectLanguage; //设置项目语言，如果isFollowSystemLanguage设置了YES，则忽略projectLanguage的设置


+ (instancetype)sharedInstance;
+ (instancetype)localization;


/**
 获取项目当前语言

 @return zh-Hans、zh-Hant或者en
 */
- (NSString *)currentLanguage;
- (LGLanguageKind)currentLanguageInt;

/**
 *  项目内修改当前语言
 *
 *  @param languageKind
 */
- (void)changeCurrentLanguage:(LGLanguageKind)languageKind;

- (NSString *)localizedStringForKey:(NSString *)key value:(NSString *)value;
- (NSString *)localizedStringForKey:(NSString *)key value:(NSString *)value tableName:(NSString *)tableName;
@end


/* 需要做的

 1.
 
*/
