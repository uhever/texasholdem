//
//  ZCArchiverManager.h
//  HuaRong
//
//  Created by Happy on 9/7/16.
//  Copyright © 2016 Happy. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ZCArchiverObject : NSObject <NSCoding>
@property (nonatomic, assign) BOOL isOpenThirdPay; //是否打开第三方支付
@end
