//
//  ZCLocalData.h
//  ZCLibProjectDEV
//
//  Created by Happy on 8/27/16.
//  Copyright © 2016 Happy. All rights reserved.
//

#import <Foundation/Foundation.h>
//#import <Bugly/Bugly.h>

@interface ZCLocalData : NSObject
/**
 *  NSUserDefaults保存、获取和删除数据
 */
+ (void)userDefaultSave:(id)object key:(NSString *)key;
+ (id)userDefaultObject:(NSString *)key;
+ (void)userDefaultRemoveObjectForKey:(NSString *)key;

/**
 *  KeyChain保存、获取和删除数据
 */
+ (BOOL)keyChainSave:(NSString *)object account:(NSString *)account;
+ (NSString *)keyChainObject:(NSString *)account;
+ (BOOL)keyChainDeleteObjectForAccount:(NSString *)account;


/**
 *  获取项目Preferences plist文件中的值
 *
 *  @param key
 *
 *  CFBundleShortVersionString, CFBundleVersion
 */
+ (id)preferencePlistObject:(NSString *)key;

+ (NSString *)pathForDocuments;
+ (NSString *)filePathFromDocuments:(NSString *)fileName;
+ (BOOL)removeLocalFile:(NSString *)filePath;
@end


// 在NSUserDefaults中保存log对应的key，后面会拼接不同的错误码
#define HRLogKeyChatroomEnter @"HRLogKeyChatroomEnter"
#define HRLogKeyChatroomExit @"HRLogKeyChatroomExit"
#define HRLogKeyStreamLive @"HRLogKeyStreamLive"
#define HRLogKeyLoginYunXin @"HRLogKeyLoginYunXin"

// 保存一下log
@interface ZCLocalData (Log)
/**
 上传错误信息到Bugly
 */
+ (void)logUploadToBugCollection:(NSInteger)errorCode errorKey:(NSString *)errorKey;

/**
 上传错误信息到Bugly

 @param errorCode
 @param errorKey
 @param des
 */
+ (void)logUploadToBugCollection:(NSInteger)errorCode errorKey:(NSString *)errorKey des:(NSString *)des;
@end
