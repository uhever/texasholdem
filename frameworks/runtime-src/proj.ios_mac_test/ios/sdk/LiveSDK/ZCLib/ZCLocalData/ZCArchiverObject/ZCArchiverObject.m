//
//  ZCArchiverManager.m
//  HuaRong
//
//  Created by Happy on 9/7/16.
//  Copyright © 2016 Happy. All rights reserved.
//

#import "ZCArchiverObject.h"

#define ZCArchiverOpenThirdPayKey @"ZCArchiverIsOpenThirdPay"

/*
 归档：[NSKeyedArchiver archiveRootObject:archiverObject toFile:ArchiverPath];
 解档：[NSKeyedUnarchiver unarchiveObjectWithFile:ArchiverPath];
 */
@implementation ZCArchiverObject
//对属性编码，归档的时候会调用
- (void)encodeWithCoder:(NSCoder *)aCoder {
//    [aCoder encodeInt:_age forKey:AGE];
//    [aCoder encodeObject:_name forKey:NAME];
//    [aCoder encodeObject:_email forKey:EMAIL];
//    [aCoder encodeObject:_password forKey:PASSWORD];
    [aCoder encodeBool:self.isOpenThirdPay forKey:ZCArchiverOpenThirdPayKey];
}

//对属性解码，解归档调用
- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];
    if (self != nil) {
//        _age = [aDecoder decodeIntForKey:AGE];
//        self.name = [aDecoder decodeObjectForKey:NAME];
//        self.email = [aDecoder decodeObjectForKey:EMAIL];
//        self.password = [aDecoder decodeObjectForKey:PASSWORD];
        self.isOpenThirdPay = [aDecoder decodeBoolForKey:ZCArchiverOpenThirdPayKey];
    }
    return self;
}
@end
