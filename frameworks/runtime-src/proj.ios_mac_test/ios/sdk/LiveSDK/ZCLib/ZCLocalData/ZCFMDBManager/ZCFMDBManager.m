//
//  ZCFMDBManager.m
//  HuaRong
//
//  Created by Happy on 8/29/16.
//  Copyright © 2016 Happy. All rights reserved.
//

#import "ZCFMDBManager.h"
#import "ZCEncrypt.h"
#import "FMDB.h"

#define OrderDataBaseName @"orderInfo" //订单数据库名称
#define ASE_Encrypt_Key @"苹果支付订单"

@interface ZCFMDBManager ()
@property (nonatomic, strong) FMDatabase *dataBase;
@end

@implementation ZCFMDBManager

static ZCFMDBManager *_manager = nil;
+ (instancetype)shareInstance {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _manager = [[ZCFMDBManager alloc] init];
        _manager.dataBase = [FMDatabase databaseWithPath:[_manager dataBasePathForAppleOrder]];
    });
    return _manager;
}

#pragma mark - 处理苹果订单
#define AppleOrderTableName @"appleOrderTable"
- (BOOL)appleOrderSave:(NSDictionary *)orderDic {
    //防止重复保存订单
    NSDictionary *orderDicSelected = [self appleOrderByOrderId:[orderDic objectForKey:@"uid"]];
    if (!orderDicSelected || orderDicSelected.count == 0) {
        return NO;
    }
    
    BOOL isSuccess = NO;
    if ([self.dataBase open]) {
        NSString *createSql = [NSString stringWithFormat:@"create table IF NOT EXISTS %@ (id integer PRIMARY KEY AUTOINCREMENT, uid text, orderId text, receipt text);", AppleOrderTableName];
        isSuccess = [self.dataBase executeUpdate:createSql];
        
        if (isSuccess) {
            NSString *uid = [ZCEncrypt encryptWithAES256:[orderDic objectForKey:@"uid"] key:ASE_Encrypt_Key];
            NSString *orderId = [ZCEncrypt encryptWithAES256:[orderDic objectForKey:@"orderId"] key:ASE_Encrypt_Key];
            NSString *receipt = [orderDic objectForKey:@"receipt"];
            
            NSString *insertSql = [NSString stringWithFormat:@"insert into %@ (uid, orderId, receipt) values (?, ?, ?);", AppleOrderTableName];
            isSuccess = [self.dataBase executeUpdate:insertSql, uid, orderId, receipt];
            
        } else {
            NSLog(@"创建table失败");
            
        }
        
        [self.dataBase close];
    }
    return isSuccess;
}

- (NSDictionary *)appleOrderByOrderId:(NSString *)orderId {
    NSMutableDictionary *resultDic = [NSMutableDictionary dictionary];
    
    if ([self.dataBase open]) {
        NSString *selectSql = [NSString stringWithFormat:@"select * from %@ where orderId = '%@';",AppleOrderTableName, [ZCEncrypt encryptWithAES256:orderId key:ASE_Encrypt_Key]];
        FMResultSet *resultSet = [self.dataBase executeQuery:selectSql];
        
        while ([resultSet next]) {
            [resultDic setObject:[ZCEncrypt decryptWithAES256:[resultSet stringForColumn:@"uid"] key:ASE_Encrypt_Key] forKey:@"uid"];
            [resultDic setObject:[ZCEncrypt decryptWithAES256:[resultSet stringForColumn:@"orderId"] key:ASE_Encrypt_Key] forKey:@"orderId"];
            [resultDic setObject:[resultSet stringForColumn:@"receipt"] forKey:@"receipt"];
        }
        
        [self.dataBase close];
    }
    
    return resultDic;
}
- (NSArray *)appleOrderByUid:(NSString *)uid {
    NSMutableArray *resultArr = [NSMutableArray array];
    
    if ([self.dataBase open]) {
        NSString *selectSql = [NSString stringWithFormat:@"select * from %@ where uid = '%@';",AppleOrderTableName, [ZCEncrypt encryptWithAES256:uid key:ASE_Encrypt_Key]];
        FMResultSet *resultSet = [self.dataBase executeQuery:selectSql];
        
        while ([resultSet next]) {
            NSMutableDictionary *resultDic = [NSMutableDictionary dictionary];

            [resultDic setObject:[ZCEncrypt decryptWithAES256:[resultSet stringForColumn:@"uid"] key:ASE_Encrypt_Key] forKey:@"uid"];
            [resultDic setObject:[ZCEncrypt decryptWithAES256:[resultSet stringForColumn:@"orderId"] key:ASE_Encrypt_Key] forKey:@"orderId"];
            [resultDic setObject:[resultSet stringForColumn:@"receipt"] forKey:@"receipt"];
            
            [resultArr addObject:resultDic];
        }
        
        [self.dataBase close];
    }
    
    return resultArr;
}

- (BOOL)appleOrderDeleteByOrderId:(NSString *)orderId {
    if ([self.dataBase open]) {
        NSString *deleteSql = [NSString stringWithFormat:@"delete from %@ where orderId = '%@';", AppleOrderTableName, [ZCEncrypt encryptWithAES256:orderId key:ASE_Encrypt_Key]];
        BOOL isSuccess = [self.dataBase executeUpdate:deleteSql];
        [self.dataBase close];
        return isSuccess;
    }
    
    return NO;
}
- (BOOL)appleOrderDeleteByUid:(NSString *)uid {
    if ([self.dataBase open]) {
        NSString *deleteSql = [NSString stringWithFormat:@"delete from %@ where orderId = '%@';", AppleOrderTableName, [ZCEncrypt encryptWithAES256:uid key:ASE_Encrypt_Key]];
        BOOL isSuccess = [self.dataBase executeUpdate:deleteSql];
        [self.dataBase close];
        return isSuccess;
    }
    
    return NO;
}


#pragma mark - 处理transactionIdentifier
#define TransactionIdentifierTableName @"transactionIdentifierTable"
- (void)table:(NSString *)tableName topLine:(NSInteger)topLine {
    if ([self.dataBase open]) {
        FMResultSet *resultSet = [self.dataBase executeQuery:[NSString stringWithFormat:@"select * from %@ limit 0,10;", TransactionIdentifierTableName]];
        while ([resultSet next]) {
            NSLog(@"appleTransactionIdentifierLook: %@", [resultSet stringForColumn:@"transactionIdentifier"]);
        }
        
        [self.dataBase close];
    }
}
- (void)appleTransactionIdentifierTraverse {
    if ([self.dataBase open]) {
        NSString *selectSql = [NSString stringWithFormat:@"select * from %@;", TransactionIdentifierTableName];
        FMResultSet *resultSet = [self.dataBase executeQuery:selectSql];
        while ([resultSet next]) {
            NSLog(@"appleTransactionIdentifierLook: %@", [resultSet stringForColumn:@"transactionIdentifier"]);
        }
        
        [self.dataBase close];
    }
}
- (BOOL)appleTransactionIdentifierCreateTable {
    NSString *createSql = [NSString stringWithFormat:@"create table IF NOT EXISTS %@ (id integer PRIMARY KEY AUTOINCREMENT, transactionIdentifier text);", TransactionIdentifierTableName];
    return [self.dataBase executeUpdate:createSql];
}
- (BOOL)appleTransactionIdentifierSave:(NSString *)transactionIdentifierValue {
    //防止重复保存transactionIdentifier
    if ([self tableRecordIsExist:transactionIdentifierValue keyName:@"transactionIdentifier" inTable:TransactionIdentifierTableName]) {
        return NO;
    }
    
    BOOL isSuccess = NO;
    if ([self.dataBase open]) {
        isSuccess = [self appleTransactionIdentifierCreateTable];
        if (isSuccess) {
            NSString *insertSql = [NSString stringWithFormat:@"insert into %@ (transactionIdentifier) values (?);", TransactionIdentifierTableName];
            isSuccess = [self.dataBase executeUpdate:insertSql, transactionIdentifierValue];
            if (!isSuccess) {
                NSLog(@"插入失败！");
            }
            
        } else {
            NSLog(@"创建transactionIdentifierTable失败");
            
        }
        
        //最多保存60条，一旦超过，则一次性删除最早的十条
        if ([self tableLineCount:TransactionIdentifierTableName] > 60) {
            [self tableDeleteTopLine:10 tableName:TransactionIdentifierTableName key:@"transactionIdentifier"];
        }
        
        [self.dataBase close];
    }
    return isSuccess;
}


#pragma mark - table的一些处理
/**
 *  判断表中是否存在某条记录
 *
 *  @param value
 *  @param key
 *  @param tableName
 *
 *  @return
 */
- (BOOL)tableRecordIsExist:(NSString *)keyValue keyName:(NSString *)keyName inTable:(NSString *)tableName {
    BOOL isExist = NO;
    if ([self.dataBase open]) {
        NSString *sql = [NSString stringWithFormat:@"select * from %@ where %@ = '%@';", tableName, keyName, keyValue];
        FMResultSet *resultSet = [self.dataBase executeQuery:sql];

        if ([resultSet next]) {
            isExist = YES;
        }
        
        [self.dataBase close];
    }
    return isExist;
}

/**
 *  删除表中前些行数记录
 *
 *  @param lineNum   删除行数
 *  @param tableName 表名
 *  @param key       可以用来查询记录的key
 *
 *  @return
 */
- (BOOL)tableDeleteTopLine:(NSInteger)lineNum tableName:(NSString *)tableName key:(NSString *)key {
    //获取前lineNum行记录
    NSString *selectSql = [NSString stringWithFormat:@"select * from %@ limit 0,%d;", tableName, (int)lineNum];
    FMResultSet *resultSet = [self.dataBase executeQuery:selectSql];
    
    NSMutableArray *orderIdArr = [NSMutableArray array];
    while ([resultSet next]) {
        [orderIdArr addObject:[resultSet stringForColumn:key]];
    }
    
    //循环删除前lineNum条记录
    for (int index = 0; index < orderIdArr.count; index++) {
        [self.dataBase executeUpdate:[NSString stringWithFormat:@"delete from %@ where %@ = '%@';", tableName, key, orderIdArr[index]]];
    }
    
    return YES;
}

/**
 *  获取table中的数据总条数
 *
 *  @param tableName 表名
 *
 *  @return
 */
- (NSInteger)tableLineCount:(NSString *)tableName {
    NSInteger lineNum = [self.dataBase intForQuery:[NSString stringWithFormat:@"select count(*) from '%@';", tableName]];
    return lineNum;
}


#pragma mark - 数据库名称
- (NSString *)dataBasePathForAppleOrder {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentPath = [paths objectAtIndex:0];

    NSString *dataBasePath = [documentPath stringByAppendingPathComponent:OrderDataBaseName];
    return dataBasePath;
}
@end
