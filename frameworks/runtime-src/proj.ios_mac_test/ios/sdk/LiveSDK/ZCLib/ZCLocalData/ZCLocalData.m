//
//  ZCLocalData.m
//  ZCLibProjectDEV
//
//  Created by Happy on 8/27/16.
//  Copyright © 2016 Happy. All rights reserved.
//

#import "ZCLocalData.h"
#import "SFHFKeychainUtils.h"
#import "ZCEncrypt.h"
#import "ZCRequest.h"

@implementation ZCLocalData
#pragma mark - NSUserDefaults
+ (void)userDefaultSave:(id)object key:(NSString *)key {
    if (!object) {
        NSLog(@"object for key:%@ is nil", key);
        return;
    }
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault setObject:object forKey:key];
    [userDefault synchronize];
}
+ (id)userDefaultObject:(NSString *)key {
    NSString *resultStr = nil;
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    resultStr = [userDefault objectForKey:key];
    
    return resultStr;
}
+ (void)userDefaultRemoveObjectForKey:(NSString *)key {
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault removeObjectForKey:key];
    [userDefault synchronize];
}


#pragma mark - KeyChain
#define APP_BUNDLE_INDENTIFIER [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleIdentifier"]
+ (BOOL)keyChainSave:(NSString *)object account:(NSString *)account {
    BOOL isSuccess = [SFHFKeychainUtils storeUsername:account andPassword:object forServiceName:APP_BUNDLE_INDENTIFIER updateExisting:YES error:nil];
    return isSuccess;
}
+ (NSString *)keyChainObject:(NSString *)account {
    NSString *resultStr = [SFHFKeychainUtils getPasswordForUsername:account andServiceName:APP_BUNDLE_INDENTIFIER error:nil];
    return resultStr;
}
+ (BOOL)keyChainDeleteObjectForAccount:(NSString *)account {
    BOOL isSuccess = [SFHFKeychainUtils deleteItemForUsername:account andServiceName:APP_BUNDLE_INDENTIFIER error:nil];
    return isSuccess;
}


#pragma mark - 获取项目Preferences plist文件中的值
+ (id)preferencePlistObject:(NSString *)key {
    if (!key) {
        return nil;
    }
    
    NSDictionary *plistDic = [[NSBundle mainBundle] infoDictionary];
    return [plistDic objectForKey:key];
}
#pragma mark -

+ (NSString *)pathForDocuments {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentPath = [paths objectAtIndex:0];
    
    return documentPath;
}
+ (NSString *)filePathFromDocuments:(NSString *)fileName {
    return [[self pathForDocuments] stringByAppendingPathComponent:fileName];
}
+ (BOOL)removeLocalFile:(NSString *)filePath {
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error = nil;
    [fileManager removeItemAtPath:filePath error:&error];
    
    if (error) {
        NSLog(@"删除本地文件%@失败", filePath);
        return NO;
    }
    
    return YES;
}
@end

@implementation ZCLocalData (Log)
+ (void)logUploadToBugCollection:(NSInteger)errorCode errorKey:(NSString *)errorKey des:(NSString *)des {
    [self IPInfomation:^(NSDictionary *ipInfoDic) {
        NSString *desStr = des ? [NSString stringWithFormat:@"%@:%@", [self errorDescriptionKey:errorKey], des]:[self errorDescriptionKey:errorKey];
        NSError *newError = [NSError errorWithDomain:errorKey code:errorCode userInfo:@{NSLocalizedDescriptionKey:desStr, @"IPInformation":[ZCEncrypt getStanderdString:ipInfoDic]}];
//        [Bugly reportError:newError];
    }];
}
+ (void)logUploadToBugCollection:(NSInteger)errorCode errorKey:(NSString *)errorKey {
    [self logUploadToBugCollection:errorCode errorKey:errorKey des:nil];
    
}

+ (NSString *)errorDescriptionKey:(NSString *)errorKey {
    if ([errorKey hasPrefix:HRLogKeyChatroomEnter]) {
        return @"进入聊天室失败";
    } else if ([errorKey hasPrefix:HRLogKeyChatroomExit]) {
        return @"退出聊天室失败";
    } else if ([errorKey hasPrefix:HRLogKeyStreamLive]) {
        return @"推流失败";
    } else if ([errorKey hasPrefix:HRLogKeyLoginYunXin]) {
        return @"登录云信失败";
    }
    
    return @"其它失败";
}

+ (void)IPInfomation:(void(^)(NSDictionary *))callback {
    if ([ZBSDKConfigure sharedInstance].ipInfoDic) {
        callback([ZBSDKConfigure sharedInstance].ipInfoDic);
        return;
    }
    
    ZCRequest *request = [[ZCRequest alloc] initWithBaseURL:nil];
    [request get:@"http://ip.taobao.com/service/getIpInfo2.php?ip=myip" parameter:@{} success:^(NSDictionary *dic) {
        NSDictionary *dataDic = dic[@"data"];
        NSLog(@"IP Information:%@", dataDic);
        [ZBSDKConfigure sharedInstance].ipInfoDic = dataDic;

//        for (NSString *key in dataDic.allKeys) {
//            NSLog(@"key:%@ -> value:%@", key, dataDic[key]);
//        }
        
        callback(dataDic);

    } failure:^(NSError *error) {
        callback(@{});

    }];
}
@end
