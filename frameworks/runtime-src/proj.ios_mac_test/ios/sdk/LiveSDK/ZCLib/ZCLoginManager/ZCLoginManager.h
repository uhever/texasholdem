//
//  ZCLoginManager.h
//  GameDemo
//
//  Created by Happy on 1/16/17.
//  Copyright © 2017 Happy. All rights reserved.
//
#define ZBLoginWeiXinIsOpen 1

#import <Foundation/Foundation.h>
#if ZBLoginWeiXinIsOpen
#import "WXApi.h"
#endif

// 登录方式
typedef NS_ENUM(NSInteger, ZBLoginType) {
    ZBLoginTypeWeiXin = 1,
    ZBLoginTypeFacebook = 2
};

@interface ZCLoginManager : NSObject
+ (instancetype)sharedInstance;

- (void)loginInWeiXin:(void(^)(NSError *error, NSDictionary *result))callback;

- (void)loginCallback:(NSError *)error result:(NSDictionary *)result loginType:(ZBLoginType)loginType;
@end
