//
//  ZCLoginManager.m
//  GameDemo
//
//  Created by Happy on 1/16/17.
//  Copyright © 2017 Happy. All rights reserved.
//

#import "ZCLoginManager.h"
#import "ZCPlatformRequestManager.h"

typedef void(^ZBLoginCallBackType)(NSError *error, NSDictionary *result);

@interface ZCLoginManager () {
    ZBLoginCallBackType _weiXinCallBack;
}
@end
@implementation ZCLoginManager

static ZCLoginManager *_manager;
+ (instancetype)sharedInstance {
    if (!_manager) {
        _manager = [[ZCLoginManager alloc] init];
    }
    
    return _manager;
}

- (void)loginInWeiXin:(void(^)(NSError *error, NSDictionary *result))callback {
    if (![WXApi isWXAppInstalled]) {
        NSError *noInstallError = [NSError errorWithDomain:@"com.zbsdk.login" code:-2 userInfo:@{NSLocalizedDescriptionKey:@"没有安装微信"}];
        callback(noInstallError, nil);
        return;
    }
    
    _weiXinCallBack = callback;
    
    SendAuthReq* req =[[SendAuthReq alloc] init];
    req.scope = @"snsapi_userinfo" ;
    req.state = @"6d4647b70fdf8592c7580218ffde00be" ;
    
    [WXApi sendReq:req];
}

- (void)loginCallback:(NSError *)error result:(NSDictionary *)result loginType:(ZBLoginType)loginType {
    if (error) {
        _weiXinCallBack(error, nil);
        
    } else {
        switch (loginType) {
            case ZBLoginTypeWeiXin:
                [ZCPlatformRequestManager requestForWeiXin:result callback:_weiXinCallBack];
                break;
                
            case ZBLoginTypeFacebook:
                break;
        }
    }
}
@end
