/****************************************************************************
 Copyright (c) 2010-2013 cocos2d-x.org
 Copyright (c) 2013-2014 Chukong Technologies Inc.
 
 http://www.cocos2d-x.org
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 ****************************************************************************/

#import <UIKit/UIKit.h>

#import "cocos2d.h"

#import "AppController.h"
#import "AppDelegate.h"
#import "RootViewController.h"
#import "platform/ios/CCEAGLView-ios.h"

// UMengAnalytics
#import "UMMobClick/MobClick.h"

// 亲加通讯云
#import "GotyeAPI.h"
USING_NS_GOTYEAPI;

@interface AppController () {
    
}


@property (strong, nonatomic) CCEAGLView *eaglView;


@end

@implementation AppController{
    
}

@synthesize eaglView = _eaglView;

#pragma mark -
#pragma mark Application lifecycle

// cocos2d application instance
static AppDelegate s_sharedApplication;


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    
    cocos2d::Application *app = cocos2d::Application::getInstance();
    app->initGLContextAttrs();
    cocos2d::GLViewImpl::convertAttrs();
    
    // Override point for customization after application launch.
    
    // Add the view controller's view to the window and display.
    window = [[UIWindow alloc] initWithFrame: [[UIScreen mainScreen] bounds]];
    self.eaglView = [CCEAGLView viewWithFrame: [window bounds]
                                  pixelFormat: (__bridge NSString*)cocos2d::GLViewImpl::_pixelFormat
                                  depthFormat: cocos2d::GLViewImpl::_depthFormat
                           preserveBackbuffer: NO
                                   sharegroup: nil
                                multiSampling: NO
                              numberOfSamples: 0 ];
    
    [self.eaglView setMultipleTouchEnabled:NO];
    
    // Use RootViewController manage CCEAGLView
    viewController = [[RootViewController alloc] initWithNibName:nil bundle:nil];
    viewController.wantsFullScreenLayout = YES;
    viewController.view = self.eaglView;
    
    // Set RootViewController to window
    if ( [[UIDevice currentDevice].systemVersion floatValue] < 6.0)
    {
        // warning: addSubView doesn't work on iOS6
        [window addSubview: viewController.view];
    }
    else
    {
        // use this method on ios6
        [window setRootViewController:viewController];
    }
    
    [window makeKeyAndVisible];
    
    [[UIApplication sharedApplication] setStatusBarHidden: YES];
    
    //保持屏幕常亮
    [[UIApplication sharedApplication] setIdleTimerDisabled:YES];
    
    
#pragma mark SDK注册-start
    
    //luaobjcbridge 清理
    NSUserDefaults *userDefaultes = [NSUserDefaults standardUserDefaults];
    [userDefaultes removeObjectForKey:@"myFunctions"];
    
    
    [[ZBManager sharedInstance] setAppId:@"1" appkey:@"4ea514d2b9d66db4f780a39ae96efb26"];
    
    // 当前包不需要支付
//        [[ZBManager sharedInstance] setAipppayAppId:@"3008168263" platformKey:@"MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCmDVTipfksf0OCatjrpGGnDBIlrKS6mi6QhIHdlQb8Xa2AI8UWDF2GIDF0Sze7cCNMwTZgaab212tdxclk9M2Wie+3APzvCUU3dWEZa4OiGwo48j196D4K/kFi/KIN6qpU0SkOsFWWPXSxKzytusv4UeCbBPp3PwHVcdLFsjb8VwIDAQAB"];
    
        [[ZBManager sharedInstance] configureProductIdentifierArr:@[@"com.zhanyou.texas.dev.diamond1",
                                                                    @"com.zhanyou.texas.dev.diamond2",
                                                                    @"com.zhanyou.texas.dev.diamond3",
                                                                    @"com.zhanyou.texas.dev.diamond4",
                                                                    @"com.zhanyou.texas.dev.diamond5",
                                                                    @"com.zhanyou.texas.dev.diamond6"]];
    [[ZBManager sharedInstance] configureWeiXinAppId:@"appid"];
    
    // 统一渠道名，所有需要指定渠道名的都用此变量
    NSString* CHANNEL_NAME = @"ios_texasdev";
    
    // 友盟
    UMConfigInstance.appKey = @"appkey";
    UMConfigInstance.channelId = CHANNEL_NAME;
    UMConfigInstance.eSType = E_UM_GAME; //仅适用于游戏场景，应用统计不用设置
    [MobClick startWithConfigure:UMConfigInstance];//配置以上参数后调用此方法初始化SDK！
    
    // 亲加云
    apiist->init("ac1cd3e3-9791-456a-834f-886bf31adec2","com.zhanyou.texas.dev");
    
    [MyApi registInitiativeEvent];
    
#pragma mark SDK注册-end
    
    // IMPORTANT: Setting the GLView should be done after creating the RootViewController
    cocos2d::GLView *glview = cocos2d::GLViewImpl::createWithEAGLView((__bridge void *)self.eaglView);
    cocos2d::Director::getInstance()->setOpenGLView(glview);
    
    app->run();
    
    return YES;
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    return [[ZBManager sharedInstance] application:application openURL:url sourceApplication:sourceApplication annotation:annotation];
}
- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<UIApplicationOpenURLOptionsKey,id> *)options {
    return [[ZBManager sharedInstance] application:app openURL:url options:options];
}

- (void)applicationWillResignActive:(UIApplication *)application {
    /*
     Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
     Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
     */
    cocos2d::Director::getInstance()->pause();
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    /*
     Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
     */
    cocos2d::Director::getInstance()->resume();
    
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    /*
     Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
     If your application supports background execution, called instead of applicationWillTerminate: when the user quits.
     */
    cocos2d::Application::getInstance()->applicationDidEnterBackground();
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    /*
     Called as part of  transition from the background to the inactive state: here you can undo many of the changes made on entering the background.
     */
    cocos2d::Application::getInstance()->applicationWillEnterForeground();
}

- (void)applicationWillTerminate:(UIApplication *)application {
    /*
     Called when the application is about to terminate.
     See also applicationDidEnterBackground:.
     */
}


#pragma mark -
#pragma mark Memory management

- (void)applicationDidReceiveMemoryWarning:(UIApplication *)application {
    /*
     Free up as much memory as possible by purging cached data objects that can be recreated (or reloaded from disk) later.
     */
    cocos2d::Director::getInstance()->purgeCachedData();
}


#pragma mark - Custom
// 向各个统计SDK发送数据事件
-(void)sendDataEvent:(NSDictionary * )params
{
    
    NSLog(@" %@ ", params);
    
    NSString * eventname = [params objectForKey:@"eventname"];
    NSString * userid = [params objectForKey:@"userid"];
    NSString * amount = [params objectForKey:@"amount"];
    NSString * orderid = [params objectForKey:@"orderid"];
    
    if ([eventname isEqualToString:@"Register"]) {
        
        [MobClick event:@"Register"];
        //        [TalkingData trackEvent:@"Register"];
        
        // fb
        //        [FBSDKAppEvents logEvent:FBSDKAppEventNameCompletedRegistration];
        //
        //        // reyun
        //        [ReYunChannel setRegisterWithAccountID: userid];
        
    } else if ([eventname isEqualToString:@"Login"]) {
        
        [MobClick event:@"Login"];
        //        [TalkingData trackEvent:@"Login"];
        
        // reyun
        //        [ReYunChannel setLoginWithAccountID:userid];
        
    } else if ([eventname isEqualToString:@"Purchase"]) {
        
        NSDictionary *dict = @{@"amount" : amount};
        [MobClick event:@"Purchase" attributes:dict counter: [amount intValue]];
        
        //fb
        //        [FBSDKAppEvents logPurchase:[amount doubleValue] currency:@"CNY"];
        
        //        NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
        //        [dic setObject:amount forKey:@"amount"];//级别区间
        //        [TalkingData trackEvent:@"Purchase" label:nil parameters: [NSDictionary dictionaryWithDictionary:dic]];
    }
}



@end

