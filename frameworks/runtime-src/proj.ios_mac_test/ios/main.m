//
//  main.m
//  simulator
//
//  Copyright __MyCompanyName__ 2011. All rights reserved.
//

#import <UIKit/UIKit.h>

int main(int argc, char *argv[]) {
    
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, @"AppController");
    }
    
}
