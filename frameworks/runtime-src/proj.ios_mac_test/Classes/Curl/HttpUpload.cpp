//
//  HttpUpload.cpp
//  GLTest
//
//  Created by zctech-011 on 2017/3/13.
//
//

#include <assert.h>
#include <curl/curl.h>
#include <curl/easy.h>
#include <stdio.h>
#include <vector>
//#include "stdafx.h"
#include "HttpUpload.hpp"



// Callback to get the response data from server.
static size_t WriteCallback(void *ptr, size_t size,
                            size_t nmemb, void *userp) {
    if (!userp)
        return 0;
    
    string *response = reinterpret_cast<string *>(userp);
    size_t real_size = size * nmemb;
    response->append(reinterpret_cast<char *>(ptr), real_size);
    cout << response << endl;
    return real_size;
}


static const char kUserAgent[] = "Breakpad/1.0 (Linux)";

// static
bool HTTPUpload::SendRequest(const string &url,
                             const map<string, string> parameters,
                             const string &upload_file,
                             const string &proxy,
                             const string &proxy_user_pwd,
                             const string &ca_certificate_file,
                             string *response_body,
                             long *response_code,
                             string *error_description) {
    if (response_code != NULL)
        *response_code = 0;
    
    if (!CheckParameters(parameters))
        return false;
    
    curl_global_init(CURL_GLOBAL_ALL);
    
    CURL *curl = curl_easy_init();
    if (error_description != NULL)
        *error_description = "No Error";
    
    if (!curl) {
        return false;
    }
    
    CURLcode err_code = CURLE_OK;
    
    (curl_easy_setopt)(curl, CURLOPT_URL, url.c_str());
    (curl_easy_setopt)(curl, CURLOPT_USERAGENT, kUserAgent);
    // Set proxy information if necessary.
    if (!proxy.empty())
        (curl_easy_setopt)(curl, CURLOPT_PROXY, proxy.c_str());
    if (!proxy_user_pwd.empty())
        (curl_easy_setopt)(curl, CURLOPT_PROXYUSERPWD, proxy_user_pwd.c_str());
    
    if (!ca_certificate_file.empty())
        (curl_easy_setopt)(curl, CURLOPT_CAINFO, ca_certificate_file.c_str());
    
    struct curl_httppost *formpost = NULL;
    struct curl_httppost *lastptr = NULL;
    
    // Add form file.
    (curl_formadd)(&formpost, &lastptr,
                   CURLFORM_COPYNAME, "file",
                   CURLFORM_FILE, upload_file.c_str(),
                   CURLFORM_END);
    
    (curl_easy_setopt)(curl, CURLOPT_HTTPPOST, formpost);
    
    curl_formadd(&formpost,
                 &lastptr,
                 CURLFORM_COPYNAME, "submit",
                 CURLFORM_COPYCONTENTS, "send",
                 CURLFORM_END);
    
    
    
    // Disable 100-continue header.
    struct curl_slist *headerlist = NULL;
    char buf[] = "Expect:";
    // struct curl_slist* (curl_slist_append)(struct curl_slist *, const char *);
    //*(void**) (&curl_slist_append) = dlsym(curl_lib, "curl_slist_append");
    headerlist = (curl_slist_append)(headerlist, buf);
    (curl_easy_setopt)(curl, CURLOPT_HTTPHEADER, headerlist);
    
    if (response_body != NULL) {
        (curl_easy_setopt)(curl, CURLOPT_WRITEFUNCTION, WriteCallback);
        (curl_easy_setopt)(curl, CURLOPT_WRITEDATA,
                           reinterpret_cast<void *>(response_body));
    }
    
    // Fail if 400+ is returned from the web server.
    (curl_easy_setopt)(curl, CURLOPT_FAILONERROR, 1);
    
    err_code = (curl_easy_perform)(curl);
    if (response_code != NULL) {
        (curl_easy_getinfo)(curl, CURLINFO_RESPONSE_CODE, response_code);
    }
    
    if (error_description != NULL)
        *error_description = (curl_easy_strerror)(err_code);
    
    (curl_easy_cleanup)(curl);
    if (formpost != NULL) {
        (curl_formfree)(formpost);
    }
    if (headerlist != NULL) {
        
        (curl_slist_free_all)(headerlist);
    }
    // dlclose(curl_lib);
    return err_code == CURLE_OK;
}

// static
bool HTTPUpload::CheckParameters(const map<string, string> parameters) {
    for (map<string, string>::const_iterator pos = parameters.begin();
         pos != parameters.end(); ++pos) {
        const string &str = pos->first;
        if (str.size() == 0)
            return false;  // disallow empty parameter names
        for (unsigned int i = 0; i < str.size(); ++i) {
            int c = str[i];
            if (c < 32 || c == '"' || c > 127) {
                return false;
            }
        }
    }  
    return true;  
}  


//重写把数据读入上传数据流函数     
size_t write_data(void *buffer, size_t size, size_t nmemb, void *userp) {  
    FILE *fptr = (FILE*)userp;  
    size_t m_size = fread(buffer, size, nmemb, fptr);  
    return m_size;  
}
