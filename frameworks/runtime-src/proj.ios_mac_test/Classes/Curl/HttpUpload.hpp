//
//  HttpUpload.hpp
//  GLTest
//
//  Created by zctech-011 on 2017/3/13.
//
//

#ifndef HttpUpload_hpp
#define HttpUpload_hpp

#include <map>
#include <string>
#include <iostream>

using std::map;
using namespace std;

class HTTPUpload {
public:
    static bool SendRequest(const string &url,
                            const map<string, string> parameters,
                            const string &upload_file,
                            const string &proxy,
                            const string &proxy_user_pwd,
                            const string &ca_certificate_file,
                            string *response_body,
                            long *response_code,
                            string *error_description);
    
    static bool MySendRequest( const string &upload_file);
    
private:
    // Checks that the given list of parameters has only printable
    // ASCII characters in the parameter name, and does not contain
    // any quote (") characters.  Returns true if so.
    static bool CheckParameters(const map<string, string> parameters);
    
    // No instances of this class should be created.
    // Disallow all constructors, destructors, and operator=.
    HTTPUpload();
    explicit HTTPUpload(const HTTPUpload &);
    void operator=(const HTTPUpload &);  
    ~HTTPUpload();  
};  

#endif /* HttpUpload_hpp */
