#ifndef __OPENGL_SCENE_H__
#define __OPENGL_SCENE_H__

#include "cocos2d.h"

USING_NS_CC;

class OpenGL : public cocos2d::Layer
{
public:
    static cocos2d::Scene* createScene();

    virtual bool init();
    
    // a selector callback
    void menuCloseCallback(cocos2d::Ref* pSender);
    
    // implement the "static create()" method manually
    CREATE_FUNC(OpenGL);
    
    
    virtual void visit(Renderer *renderer, const Mat4 &parentTransform, uint32_t parentFlags) override;
    
    void onDraw();
    
private:
    CustomCommand _command;
    
};

#endif // __OPENGL_SCENE_H__
