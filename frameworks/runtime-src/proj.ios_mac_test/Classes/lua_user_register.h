//
//  lua_user_register.hpp
//  EngineTest
//
//  Created by UHEVER on 16/10/30.
//
//

#ifndef lua_user_register_h
#define lua_user_register_h

#include "lua.h"
#include "scripting/lua-bindings/manual/Lua-BindingsExport.h"

CC_LUA_DLL  int  lua_user_register(lua_State* L);

#endif /* lua_user_register_h */
