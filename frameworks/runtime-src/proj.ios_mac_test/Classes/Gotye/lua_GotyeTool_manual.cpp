#include "lua_GotyeTool_manual.hpp"
#include "lua_GotyeTool_auto.hpp"

#include "GotyeTool.h"

#include "scripting/lua-bindings/manual/tolua_fix.h"


int lua_GotyeTool_registerEventHandler(lua_State* L)
{
    if (nullptr == L) {
        return 0;
    }
    int argc = 0;
    GotyeTool* self = nullptr;
    
    self = (GotyeTool*)tolua_tousertype(L, 1, 0);
    
    argc = lua_gettop(L) - 1;
    
    if (1 == argc) {
        int handler = (toluafix_ref_function(L, 2, 0));
        self->registerEventHandler(handler);
        return 0;
    }
    
    return 0;
    
}

int register_all_gotyetool_manual(lua_State* L)
{
    if (nullptr == L) {
        return 0;
    }
    
    lua_pushstring(L, "GotyeTool");
    lua_rawget(L, LUA_REGISTRYINDEX);
    if (lua_istable(L, -1)) {
        tolua_function(L, "registerEventHandler", lua_GotyeTool_registerEventHandler);
        
    }
    
    lua_pop(L, 1);
    
    return 0;
}

int register_gotyetool_moudle(lua_State* L)
{
    lua_getglobal(L, "_G");
    if (lua_istable(L, -1)) //stack:...,_G,
    {
        register_all_GotyeTool(L);
        register_all_gotyetool_manual(L);
    }
    lua_pop(L, 1);
    
    return 1;
}
