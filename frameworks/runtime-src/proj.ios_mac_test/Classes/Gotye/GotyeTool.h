//
//  GotyeTool.h
//
//

#ifndef _GotyeTool_H_
#define _GotyeTool_H_

#include <stdio.h>

#include "cocos2d.h"
USING_NS_CC;

#include "include/GotyeAPI.h"
USING_NS_GOTYEAPI;

using namespace std;

class GotyeTool : public Ref, public GotyeDelegate
{
public:
    GotyeTool();
    ~GotyeTool();
    
    static GotyeTool* getInstance();
    
    static void destroyInstance();
    
    // 注册lua回调
    void registerEventHandler(int handler);
    
    // 移除lua回调
    void unregisterEventHandler();
    
    // 初始化
    void init();
    
    void update(float dt);
    
    // 设置是否自动播放语音
    void setAutoPlayAudio(bool isAutoPlayAudio);
    bool getAutoPlayAudio() const;
    
    // 取消语音
    void cancelTalk();
    
#pragma mark - Veriable
private:
    
    int _eventHandler; // lua句柄
    
    bool _isAutoPlayAudio; // 是否自动播放语音
    
    bool _isCancelTalk; // 是否取消说话(仅当次)
    
public:
    
    // 退出销毁
    void exit();
    
    // 登陆
    void login(const char* username);
    
    // 登出
    void logout();
    
    // 检测是否在线
    bool isOnline();
    
    // 跟单人说话
    void talkToUser(const char* username, unsigned int whineMode = 0, bool realtime = false, unsigned int maxDuration = REC_DEFAULTDURATION);
    
    // 跟群组说话
    void talkToGroup(int groupId, unsigned int whineMode = 0, bool realtime = false, unsigned int maxDuration = REC_DEFAULTDURATION);
    
    // 跟聊天室说话
    void talkToRoom(int roomId, unsigned int whineMode = 0, bool realtime = false, unsigned int maxDuration = REC_DEFAULTDURATION);
    
    // 获取说话音量
    int getTalkingPower() const;
    
    // 停止说话
    void stopTalk();
    
    // 创建群组
    void createGroup(const char* name, const char* info, bool isPublic = true, bool needAuthentication = false);
    
    // 加入群组
    void joinGroup(int groupId);
    
    // 解散群组
    void dismissGroup(int groupId);
    
    // 进入聊天室
    void enterRoom(int roomId);
    
    // 退出聊天室
    void leaveRoom(int roomId);
    
    // 是否在聊天室
    bool isInRoom(int roomId) const;
    
    // 聊天室是否支持实时语音
    bool isRoomSupportRealtime(int roomId) const;
    

    
#pragma mark - Delegate
    
private:
    // 调用lua
    void callLuaHandler(const ValueMap& dict);
    
private:
    
    // 登陆成功回调
    virtual void onLogin(GotyeStatusCode code, const GotyeLoginUser& user);
    // 退出登陆回调
    virtual void onLogout(GotyeStatusCode code);
    // 发送消息成功回调
    virtual void onSendMessage(GotyeStatusCode code, const GotyeMessage& message);
    // 接受消息成功回调
    virtual void onReceiveMessage(const GotyeMessage& message, bool* downloadMediaIfNeed);
    // 媒体文件下载完毕回调
    virtual void onDownloadMediaInMessage(GotyeStatusCode code, const GotyeMessage& message);
    // 播放开始回调
    virtual void onPlayStart(GotyeStatusCode code, const GotyeMessage &message);
    // 实施模式播放回调
    virtual void onRealPlayStart(GotyeStatusCode code, const GotyeUser& speaker, const GotyeRoom& room);
    // 正在播放回调
    virtual void onPlaying(long position);
    // 播放完毕回调
    virtual void onPlayStop();
    // 录音开始回调
    virtual void onStartTalk(GotyeStatusCode code, GotyeChatTarget target, bool realtime);
    // 录音结束回调
    virtual void onStopTalk(GotyeStatusCode code, bool realtime, GotyeMessage& message, bool *cancelSending);
    // 创建群组成功回调
    virtual void onCreateGroup(GotyeStatusCode code, const GotyeGroup& group);
    // 加入群回调
    virtual void onJoinGroup(GotyeStatusCode code, const GotyeGroup& group);
    // 离开群回调
    virtual void onLeaveGroup(GotyeStatusCode code, const GotyeGroup& group);
    // 解散群回调
    virtual void onDismissGroup(GotyeStatusCode code, const GotyeGroup& group);
    // 进入聊天室
    virtual void onEnterRoom(GotyeStatusCode code, GotyeRoom& room);
    // 离开聊天室
    virtual void onLeaveRoom(GotyeStatusCode code, GotyeRoom& room);

};

#endif
