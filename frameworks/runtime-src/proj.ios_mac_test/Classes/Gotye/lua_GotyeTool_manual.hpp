#ifndef COCOS_SCRIPTING_LUA_BINDINGS_LUA_GOTYETOOL_MANUAL_H
#define COCOS_SCRIPTING_LUA_BINDINGS_LUA_GOTYETOOL_MANUAL_H

#ifdef __cplusplus
extern "C" {
#endif
#include "tolua++.h"
#ifdef __cplusplus
}
#endif

TOLUA_API int register_all_gotyetool_manual(lua_State* L);

TOLUA_API int register_gotyetool_moudle(lua_State* L);


#endif // COCOS_SCRIPTING_LUA_BINDINGS_LUA_GOTYETOOL_MANUAL_H
