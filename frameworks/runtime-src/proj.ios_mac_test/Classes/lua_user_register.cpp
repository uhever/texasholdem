//
//  lua_user_register.cpp
//  EngineTest
//
//  Created by UHEVER on 16/10/30.
//
//

#include "lua_user_register.h"

#include "ZCTool/lua_ZCTool_auto.hpp"

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID || CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
#include "Gotye/lua_GotyeTool_manual.hpp"
#endif

int lua_user_register(lua_State* L)
{
    
    register_all_ZCTool(L);
    
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID || CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    register_gotyetool_moudle(L);
#endif
    
    return 1;
}
