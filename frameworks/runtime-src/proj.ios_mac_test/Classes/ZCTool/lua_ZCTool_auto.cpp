#include "lua_ZCTool_auto.hpp"
#include "ZCTool.h"
#include "scripting/lua-bindings/manual/tolua_fix.h"
#include "scripting/lua-bindings/manual/LuaBasicConversions.h"

int lua_ZCTool_ZCTool_calcStrSize(lua_State* tolua_S)
{
    int argc = 0;
    ZCTool* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"ZCTool",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (ZCTool*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_ZCTool_ZCTool_calcStrSize'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 3) 
    {
        const char* arg0;
        const char* arg1;
        const char* arg2;

        std::string arg0_tmp; ok &= luaval_to_std_string(tolua_S, 2, &arg0_tmp, "ZCTool:calcStrSize"); arg0 = arg0_tmp.c_str();

        std::string arg1_tmp; ok &= luaval_to_std_string(tolua_S, 3, &arg1_tmp, "ZCTool:calcStrSize"); arg1 = arg1_tmp.c_str();

        std::string arg2_tmp; ok &= luaval_to_std_string(tolua_S, 4, &arg2_tmp, "ZCTool:calcStrSize"); arg2 = arg2_tmp.c_str();
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_ZCTool_ZCTool_calcStrSize'", nullptr);
            return 0;
        }
        const char* ret = cobj->calcStrSize(arg0, arg1, arg2);
        tolua_pushstring(tolua_S,(const char*)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "ZCTool:calcStrSize",argc, 3);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_ZCTool_ZCTool_calcStrSize'.",&tolua_err);
#endif

    return 0;
}
int lua_ZCTool_ZCTool_captureScreen(lua_State* tolua_S)
{
    int argc = 0;
    ZCTool* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"ZCTool",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (ZCTool*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_ZCTool_ZCTool_captureScreen'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 2) 
    {
        cocos2d::Node* arg0;
        const char* arg1;

        ok &= luaval_to_object<cocos2d::Node>(tolua_S, 2, "cc.Node",&arg0, "ZCTool:captureScreen");

        std::string arg1_tmp; ok &= luaval_to_std_string(tolua_S, 3, &arg1_tmp, "ZCTool:captureScreen"); arg1 = arg1_tmp.c_str();
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_ZCTool_ZCTool_captureScreen'", nullptr);
            return 0;
        }
        cobj->captureScreen(arg0, arg1);
        lua_settop(tolua_S, 1);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "ZCTool:captureScreen",argc, 2);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_ZCTool_ZCTool_captureScreen'.",&tolua_err);
#endif

    return 0;
}
int lua_ZCTool_ZCTool_getGaussianTexture(lua_State* tolua_S)
{
    int argc = 0;
    ZCTool* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"ZCTool",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (ZCTool*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_ZCTool_ZCTool_getGaussianTexture'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 1) 
    {
        cocos2d::Node* arg0;

        ok &= luaval_to_object<cocos2d::Node>(tolua_S, 2, "cc.Node",&arg0, "ZCTool:getGaussianTexture");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_ZCTool_ZCTool_getGaussianTexture'", nullptr);
            return 0;
        }
        cocos2d::Texture2D* ret = cobj->getGaussianTexture(arg0);
        object_to_luaval<cocos2d::Texture2D>(tolua_S, "cc.Texture2D",(cocos2d::Texture2D*)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "ZCTool:getGaussianTexture",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_ZCTool_ZCTool_getGaussianTexture'.",&tolua_err);
#endif

    return 0;
}
int lua_ZCTool_ZCTool_createGLScissorNode(lua_State* tolua_S)
{
    int argc = 0;
    ZCTool* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"ZCTool",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (ZCTool*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_ZCTool_ZCTool_createGLScissorNode'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 2) 
    {
        double arg0;
        double arg1;

        ok &= luaval_to_number(tolua_S, 2,&arg0, "ZCTool:createGLScissorNode");

        ok &= luaval_to_number(tolua_S, 3,&arg1, "ZCTool:createGLScissorNode");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_ZCTool_ZCTool_createGLScissorNode'", nullptr);
            return 0;
        }
        cocos2d::Node* ret = cobj->createGLScissorNode(arg0, arg1);
        object_to_luaval<cocos2d::Node>(tolua_S, "cc.Node",(cocos2d::Node*)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "ZCTool:createGLScissorNode",argc, 2);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_ZCTool_ZCTool_createGLScissorNode'.",&tolua_err);
#endif

    return 0;
}
int lua_ZCTool_ZCTool_destroyInstance(lua_State* tolua_S)
{
    int argc = 0;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif

#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertable(tolua_S,1,"ZCTool",0,&tolua_err)) goto tolua_lerror;
#endif

    argc = lua_gettop(tolua_S) - 1;

    if (argc == 0)
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_ZCTool_ZCTool_destroyInstance'", nullptr);
            return 0;
        }
        ZCTool::destroyInstance();
        lua_settop(tolua_S, 1);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d\n ", "ZCTool:destroyInstance",argc, 0);
    return 0;
#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_ZCTool_ZCTool_destroyInstance'.",&tolua_err);
#endif
    return 0;
}
int lua_ZCTool_ZCTool_getInstance(lua_State* tolua_S)
{
    int argc = 0;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif

#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertable(tolua_S,1,"ZCTool",0,&tolua_err)) goto tolua_lerror;
#endif

    argc = lua_gettop(tolua_S) - 1;

    if (argc == 0)
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_ZCTool_ZCTool_getInstance'", nullptr);
            return 0;
        }
        ZCTool* ret = ZCTool::getInstance();
        object_to_luaval<ZCTool>(tolua_S, "ZCTool",(ZCTool*)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d\n ", "ZCTool:getInstance",argc, 0);
    return 0;
#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_ZCTool_ZCTool_getInstance'.",&tolua_err);
#endif
    return 0;
}
int lua_ZCTool_ZCTool_constructor(lua_State* tolua_S)
{
    int argc = 0;
    ZCTool* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif



    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_ZCTool_ZCTool_constructor'", nullptr);
            return 0;
        }
        cobj = new ZCTool();
        cobj->autorelease();
        int ID =  (int)cobj->_ID ;
        int* luaID =  &cobj->_luaID ;
        toluafix_pushusertype_ccobject(tolua_S, ID, luaID, (void*)cobj,"ZCTool");
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "ZCTool:ZCTool",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_error(tolua_S,"#ferror in function 'lua_ZCTool_ZCTool_constructor'.",&tolua_err);
#endif

    return 0;
}

static int lua_ZCTool_ZCTool_finalize(lua_State* tolua_S)
{
    printf("luabindings: finalizing LUA object (ZCTool)");
    return 0;
}

int lua_register_ZCTool_ZCTool(lua_State* tolua_S)
{
    tolua_usertype(tolua_S,"ZCTool");
    tolua_cclass(tolua_S,"ZCTool","ZCTool","cc.Ref",nullptr);

    tolua_beginmodule(tolua_S,"ZCTool");
        tolua_function(tolua_S,"new",lua_ZCTool_ZCTool_constructor);
        tolua_function(tolua_S,"calcStrSize",lua_ZCTool_ZCTool_calcStrSize);
        tolua_function(tolua_S,"captureScreen",lua_ZCTool_ZCTool_captureScreen);
        tolua_function(tolua_S,"getGaussianTexture",lua_ZCTool_ZCTool_getGaussianTexture);
        tolua_function(tolua_S,"createGLScissorNode",lua_ZCTool_ZCTool_createGLScissorNode);
        tolua_function(tolua_S,"destroyInstance", lua_ZCTool_ZCTool_destroyInstance);
        tolua_function(tolua_S,"getInstance", lua_ZCTool_ZCTool_getInstance);
    tolua_endmodule(tolua_S);
    std::string typeName = typeid(ZCTool).name();
    g_luaType[typeName] = "ZCTool";
    g_typeCast["ZCTool"] = "ZCTool";
    return 1;
}
TOLUA_API int register_all_ZCTool(lua_State* tolua_S)
{
	tolua_open(tolua_S);
	
	tolua_module(tolua_S,nullptr,0);
	tolua_beginmodule(tolua_S,nullptr);

	lua_register_ZCTool_ZCTool(tolua_S);

	tolua_endmodule(tolua_S);
	return 1;
}

