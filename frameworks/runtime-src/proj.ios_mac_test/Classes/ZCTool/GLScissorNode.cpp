#include "GLScissorNode.hpp"

GLScissorNode::GLScissorNode()
{
    
}

GLScissorNode::~GLScissorNode()
{
    
}
GLScissorNode* GLScissorNode::create(float width, float height)
{
    GLScissorNode * ret = new (std::nothrow) GLScissorNode();
    if (ret)
    {
        ret->m_sSize.width = width;
        ret->m_sSize.height = height;
        ret->autorelease();
        ret->setAnchorPoint(Vec2(0.0f, 0.0f));
    }
    else
    {
        CC_SAFE_DELETE(ret);
    }
    return ret;
}
void GLScissorNode::visit(Renderer* renderer, const Mat4 &parentTransform, uint32_t parentFlags)
{
    _beforeVisitCmdScissor.init(_globalZOrder);
    _beforeVisitCmdScissor.func = CC_CALLBACK_0(GLScissorNode::onBeforeVisitScissor, this);
    renderer->addCommand(&_beforeVisitCmdScissor);
    Node::visit(renderer,parentTransform,parentFlags);
    _afterVisitCmdScissor.init(_globalZOrder);
    _afterVisitCmdScissor.func = CC_CALLBACK_0(GLScissorNode::onAfterVisitScissor, this);
    renderer->addCommand(&_afterVisitCmdScissor);
}
void GLScissorNode::onAfterVisitScissor()
{
    glDisable(GL_SCISSOR_TEST);
}
void GLScissorNode::onBeforeVisitScissor()
{
    glEnable(GL_SCISSOR_TEST);
    Vec2 point = convertToWorldSpace(Vec2(0,0));
    auto glview = Director::getInstance()->getOpenGLView();
    glview->setScissorInPoints(point.x,point.y,m_sSize.width * getScaleX(),m_sSize.height * getScaleY());
}

