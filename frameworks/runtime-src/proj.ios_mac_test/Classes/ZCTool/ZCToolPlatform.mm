//
//  ZCToolPlatform.mm
//  Poker
//
//  Created by zctech-011 on 2017/1/16.
//
//

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS || CC_TARGET_PLATFORM == CC_PLATFORM_MAC)

#import "ZCToolPlatform.h"
#import "MyApi.h"

const char* calcStrSizePlatform(const char* str, const char* font, const char* fontSize)
{
    
    NSString * strNew = [[NSString alloc] initWithCString:str encoding:NSUTF8StringEncoding];
    NSString * strFont = [NSString stringWithFormat:@"%s", font];
    NSString * strFontSize = [NSString stringWithFormat:@"%s", fontSize];
    NSString * _sizeStr = [MyApi calculateStringSize: strNew fontName: strFont fontNumber: strFontSize ];
    
    return [_sizeStr UTF8String];
    
}

#endif
