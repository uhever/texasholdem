//
//  GLScissorNode.hpp
//  Poker
//
//  Created by zc-c-038 on 16/11/29.
//
//

#ifndef GLScissorNode_hpp
#define GLScissorNode_hpp

#include <stdio.h>

#include "cocos2d.h"
USING_NS_CC;
class GLScissorNode:public Node
{
public:
    GLScissorNode();
    virtual ~GLScissorNode();
    static GLScissorNode* create(float width, float height);
    virtual void visit(Renderer* renderer, const Mat4 &parentTransform, uint32_t parentFlags);
    CustomCommand _afterVisitCmdScissor;
    CustomCommand _beforeVisitCmdScissor;
    void onAfterVisitScissor();
    void onBeforeVisitScissor();
private:
    cocos2d::Size m_sSize;
};


#endif /* GLScissorNode_hpp */
