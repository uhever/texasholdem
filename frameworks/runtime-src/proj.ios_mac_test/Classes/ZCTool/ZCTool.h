//
//  ZCTool.h
//
//

#ifndef _ZCTool_H_
#define _ZCTool_H_

#include "ZCToolPlatform.h"

#include "GLScissorNode.hpp"
#include "cocos2d.h"
USING_NS_CC;

class ZCTool : public Ref
{
public:
    ZCTool();
    ~ZCTool();
    
    static ZCTool* getInstance();
    
    static void destroyInstance();
    
    const char* calcStrSize(const char* str, const char* font, const char* fontSize);
    Node* createGLScissorNode(float x, float y);
    void captureScreen(Node* node, const char * textureName);
    Texture2D* getGaussianTexture(Node* node);
private:
    void setGaussianBuffer(unsigned char* img,   int width,  int height,  int channels,  int radius);
    void fastStackBlur(unsigned char* pix, unsigned int w, unsigned int h, unsigned int comp, int radius);
};

#endif
