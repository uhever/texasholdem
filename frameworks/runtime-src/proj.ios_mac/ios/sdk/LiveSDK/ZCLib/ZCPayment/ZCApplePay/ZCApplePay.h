//
//  ZCApplePay.h
//  ZCLibProjectDEV
//
//  Created by Happy on 8/28/16.
//  Copyright © 2016 Happy. All rights reserved.
//  去平台服务器创建订单，然后调用苹果的支付方法，支付成功后，将结果告诉平台服务器

#import <Foundation/Foundation.h>
#import <StoreKit/StoreKit.h>
#import "ZCPayItem.h"

#define ZCApplePayOrderIdKey @"orderId"
#define ZCApplePayReceiptKey @"receipt"
#define ZCApplePayUidKey @"uid"

@protocol ZCApplePayDelegate <NSObject>
- (void)applePayPruchaseCompletion:(NSString *)orderId error:(NSError *)error;
@end

@interface ZCApplePay : NSObject <SKPaymentTransactionObserver, SKProductsRequestDelegate>
@property (nonatomic, weak) id<ZCApplePayDelegate>payDelegate;
@property (nonatomic, strong) NSArray *productIdentifierArr; //商品ID数组


+ (ZCApplePay *)shareInstance;

/**
 *  登陆成功后，应立即注册苹果支付回调通知
 */
- (void)registerTransactionObserver;

/**
 *  调用苹果支付
 *
 *  @param payItem 支付参数，goodIndex从0开始
 */
- (void)payWithItem:(ZCPayItem *)payItem;

/**
 *  登录成功后，需要将提交平台服务器失败(平台服务器没有收到)的订单重新提交
 */
- (void)retryNoticePlatformServerTheOrderUnSubmit;
@end
