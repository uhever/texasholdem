//
//  ZCPaymentManager.h
//  ZCDemo
//
//  Created by Happy on 8/26/16.
//  Copyright © 2016 Happy. All rights reserved.
//  调用ZCApplePay中的苹果支付方法，收到支付成功回调后，去直播服务器轮询支付结果

#define ZCPayPingppIsOpen 0 //1：打开Pingpp支付；0：关闭Pingpp支付
#define ZCPayAiBeiIsOpen 0 //1：打开AiBei支付；0：关闭AiBei支付

#import <Foundation/Foundation.h>
#import "ZCApplePay.h"

#if ZCPayAiBeiIsOpen
#import <IapppayKit/IapppayKit.h>
#import <IapppayKit/IapppayOrderUtils.h>
#endif

#if ZCPayPingppIsOpen
#import "Pingpp.h"
#endif

//从支付结果中获取信息的一些Key
extern NSString *const ZCPayTypeKey; //支付方式Key
extern NSString *const ZCPayOrderIdKey; //订单Id
extern NSString *const ZCPayOrderChangeKey; //支付后，用户变动信息key


//支付结果
typedef NS_ENUM(NSInteger, ZCPayResult) {
    ZCPayResultSuccess = 100, //支付成功
    ZCPayResultFailure, //支付失败
    ZCPayResultTimeout, //支付超时
    ZCPayResultMissParameter //缺少参数
};

typedef void(^ZCPayCompletion)(NSError *error, NSDictionary *payResult);


@interface ZCPaymentManager : NSObject
@property (nonatomic, readonly) BOOL isPaying; //是否正在支付
@property (nonatomic, copy) NSString *payCallback; //支付回调地址

+ (instancetype)shareInstance;

- (void)pay:(ZCPayItem *)payItem completion:(ZCPayCompletion)completion;

@end


#if ZCPayAiBeiIsOpen
@interface ZCPaymentManager (AiBeiPay) <IapppayKitPayRetDelegate>

@end
#endif


#if ZCPayPingppIsOpen
@interface ZCPaymentManager (PingppPay)
/**
 主要为了处理Pingpp跳转支付宝、微信客户端支付的回调问题
 
 @param url url
 */
- (void)handlePingppPayResult:(NSString *)url;
@end
#endif
