//
//  ZCPaymentManager.m
//  ZCDemo
//
//  Created by Happy on 8/26/16.
//  Copyright © 2016 Happy. All rights reserved.
//

#import "ZCPaymentManager.h"

#import "ZCPlatformRequest.h"
#import "ZCLiveRequest.h"
#import "NIMProgressHUD.h"
#import "ZCLiveRequestManager.h"

NSString *const ZCPayTypeKey = @"paymentType";
NSString *const ZCPayOrderIdKey = @"paymentOrderId";
NSString *const ZCPayOrderChangeKey = @"payOrderChangeKey";

#define TimeLimitForLookOrderState 10 //轮询订单状态时长, 单位秒
#define CountForLookOrderState 5 //轮询订单状态上限次数

@interface ZCPaymentManager () <ZCApplePayDelegate> {
    BOOL _isPaying;
    NSDate *_recordDate; //订单计时器
    
    NSString *_gameGoodType; //游戏商品类型，月卡、年卡等
    NSString *_gameToken;
    
    ZCPayItem *_payItem;
}
@property (nonatomic, copy) ZCPayCompletion payCompletion;
@property (nonatomic, strong) NSMutableDictionary *payResult;
@property (nonatomic, assign) NSInteger lookOrderStateCount; //查询订单次数
@end


@implementation ZCPaymentManager
static ZCPaymentManager *paymentManager = nil;
+ (instancetype)shareInstance {
    if (!paymentManager) {
        paymentManager = [[ZCPaymentManager alloc] init];
        paymentManager.payResult = [NSMutableDictionary dictionary];
    }
    return paymentManager;
}

// 支付回调地址
- (NSString *)payCallback {
    if (!_payCallback) {
        NSString *payCallback = [ZCBaseURLForPayServer_Dev stringByAppendingString:ZCURL_Pay_CallBack];
        if ([ZBSDKConfigure sharedInstance].isOnline) {
            payCallback = [ZCBaseURLForPayServer_Dist stringByAppendingString:ZCURL_Pay_CallBack];
        }

        return payCallback;
    } else {
        return _payCallback;
    }
}


- (void)pay:(ZCPayItem *)payItem completion:(ZCPayCompletion)completion {
    _isPaying = YES;
    _payItem = payItem;
    
    self.payCompletion = completion;
    [self.payResult removeAllObjects];
    [self.payResult setObject:@(payItem.payType) forKey:ZCPayTypeKey];
    
    if (payItem.payType == ZCPayTypeAipppay) { //爱贝支付需要appid和platformKey
        if ([ZBSDKConfigure sharedInstance].aipppayAppId.length == 0 ||
            [ZBSDKConfigure sharedInstance].aipppayPlatformKey.length == 0) {
            NSError *error = [NSError errorWithDomain:ZCPayErrorDomain code:ZCPayResultMissParameter userInfo:@{NSLocalizedDescriptionKey:LGLocalizedString(@"", @"缺少爱贝支付必要的参数，请使用方法setAipppayAppId: platformKey:配置")}];
            
            completion(error, nil);
            return;
        }
    }
    
    if (!payItem.payCallback.length) {
        payItem.payCallback = self.payCallback;
    }
    
    if ([ZBSDKConfigure sharedInstance].serverProcess != ZBSDKProcessTypePlatform) {
        __weak typeof(self)weakSelf = self;
        [self createOrderFromLiveServer:^(NSError *error, NSDictionary *dic) {
            [weakSelf callThirdPaySDK:payItem];
        }];
    } else {
        [self.payResult setObject:payItem.orderId forKey:ZCPayOrderIdKey];

        [self callThirdPaySDK:_payItem];
    }
    
}

#pragma mark 苹果支付
- (void)payInApple:(ZCPayItem *)payItem {
    //设置苹果支付回调代理
    if (![ZCApplePay shareInstance].payDelegate) {
        [ZCApplePay shareInstance].payDelegate = self;
    }
    
    [[ZCApplePay shareInstance] payWithItem:payItem];
}

- (void)applePayPruchaseCompletion:(NSString *)orderId error:(NSError *)error {
    if (!error) {
        [self loolupOrderState:orderId];
        
    } else {
        if (self.payCompletion) {
            self.payCompletion(error, self.payResult);
        }
    }
}

#pragma mark 爱贝支付
- (void)payInAiBei:(ZCPayItem *)payItem {
#if ZCPayAiBeiIsOpen
    __weak typeof(self)weakSelf = self;
    NSDictionary *aippPayPara = @{ZCPlatformSessionKey:@"",
                                  @"appGoodsId":payItem.goodIndex,
                                  @"appGoodsName":payItem.goodName,
                                  @"appGoodsPrice":payItem.goodPrice,
                                  @"appCallback":payItem.payCallback,
                                  @"appOrderId":payItem.orderId,
                                  @"iappPayAppId":[ZBSDKConfigure sharedInstance].aipppayAppId};
    [ZCPlatformRequest request:ZCURL_AippPay_Init parameter:aippPayPara success:^(NSDictionary *dic) {
        [NIMProgressHUD dismiss];
        
        NSString *trandId = dic[@"requestParam"][@"transid"];
        IapppayOrderUtils *orderInfo = [[IapppayOrderUtils alloc] init];
        orderInfo.appId = [ZBSDKConfigure sharedInstance].aipppayAppId;
        NSString *trandInfo = [orderInfo getTrandIdDataWith:trandId];
        [[IapppayKit sharedInstance] makePayForTrandInfo:trandInfo openIDTokenValue:nil payResultDelegate:weakSelf];
        
    } failure:^(NSError *error) {
        self.payCompletion(error, self.payResult);
        
    }];
#else
    NSLog(@"ZCSDK AiBei支付暂未开放");
    
#endif
 
}

#pragma mark Pingpp支付
- (void)payInPingpp:(ZCPayItem *)payItem {
#if ZCPayPingppIsOpen
    NSString *channel = @"";
    if (_payItem.payType == ZCPayTypeWeiXin) {
        channel = @"wx";
    } else if (_payItem.payType == ZCPayTypeAliPay) {
        channel = @"alipay";
    }
    
    NSDictionary *pingPayPara = @{ZCPlatformSessionKey:@"",
                                  @"appGoodsId":payItem.goodIndex,
                                  @"appGoodsName":payItem.goodName,
                                  @"appGoodsPrice":payItem.goodPrice,
                                  @"appCallback":payItem.payCallback,
                                  @"appOrderId":payItem.orderId,
                                  @"channel":channel};
    [ZCPlatformRequest request:ZCURL_PingPay_Init parameter:pingPayPara success:^(NSDictionary *dic) {
        NSDictionary *chargeDic = [dic objectForKey:@"requestParam"];
        [Pingpp createPayment:chargeDic
                 appURLScheme:@"wx8d50726293f9fdf4"
               withCompletion:^(NSString *result, PingppError *error) {
                   if (_isPaying) {
                       _isPaying = NO;
                       
                       if (error) {
                           NSString *errorMsg = LGLocalizedString(@"payFailure", @"支付失败");
                           if (error.code == PingppErrCancelled) {
                               // 支付取消
                               errorMsg = [error getMsg];
                           } else if (error.code == PingppErrWxNotInstalled) {
                               // 没安装微信
                               errorMsg = LGLocalizedString(@"noInstallWeiXin", @"请先安装微信客户端");
                           }
                           
                           NSError *newError = [NSError errorWithDomain:ZCPayErrorDomain code:error.code userInfo:@{NSLocalizedDescriptionKey:errorMsg}];
                           self.payCompletion(newError, self.payResult);
                           
                       } else { //支付宝网页支付会走这个逻辑
                           [self loolupOrderState:payItem.orderId];
                           
                       }
                       
                   }
               }];
        
    } failure:^(NSError *error) {
        self.payCompletion(error, self.payResult);
    }];
#else
    NSLog(@"ZCSDK Pingpp支付暂未开放");
    
#endif
}


#pragma mark - Private method
- (NSString *)weixinScheme {
    NSDictionary *plistDic = [[NSBundle mainBundle] infoDictionary];
    NSArray *schemeArr = plistDic[@"CFBundleURLTypes"];
    NSDictionary *wxSchemeDic = schemeArr.firstObject;
    NSArray *weixinSchemeArr = wxSchemeDic[@"CFBundleURLSchemes"];
    
    return weixinSchemeArr.firstObject;
}

#pragma mark 在app服务器创建订单
// 在直播App服务器创建预订单
- (void)createOrderFromLiveServer:(void(^)(NSError *error, NSDictionary *dic))callback {
    
    NSString *typeStr = @"ios";
    if (_payItem.payType == ZCPayTypeWeiXin) {
        typeStr = @"wx";
    } else if (_payItem.payType == ZCPayTypeAliPay) {
        typeStr = @"ali";
    } else if (_payItem.payType == ZCPayTypeAipppay) {
        typeStr = @"aipp";
    }
    
    [ZCLiveRequestManager requestForCreatePreOrder:[ZBSDKConfigure sharedInstance].appid
     
                                           payType:typeStr
     
                                         goodIndex:_payItem.goodIndex
     
                                          goodType:_payItem.goodType
     
                                             token:_payItem.token
     
                                       reviewState:[NSString stringWithFormat:@"%d", ![ZBSDKConfigure sharedInstance].isReview]
     
                                       serverPoker:_payItem.serverPoker
     
                                          callback:^(NSError *error, NSDictionary *dic) {
                                              
                                              if (!error) {
                                                  [self.payResult setObject:dic[@"app_order_id"] forKey:ZCPayOrderIdKey];

                                                  _payItem.orderId = dic[@"app_order_id"];
                                                  _payItem.goodPrice = [NSString stringWithFormat:@"%d", [dic[@"amount"] intValue] * 100];
                                                  _payItem.goodName = dic[@"product_name"];
                                                  
                                                  callback(nil, dic);
                                              } else {
                                                  NSError *newError = [NSError errorWithDomain:ZCPayErrorDomain code:error.code userInfo:@{NSLocalizedDescriptionKey:LGLocalizedString(@"payFailure", @"支付失败")}];
                                                  self.payCompletion(newError, self.payResult);
                                              }
                                          }];
}


// 根据支付方式参数，调用不同的支付方法
- (void)callThirdPaySDK:(ZCPayItem *)payItem {
    if (payItem.payType == ZCPayTypeApple) {
        [self payInApple:(ZCPayItem *)payItem];
    } else if (payItem.payType == ZCPayTypeWeiXin || payItem.payType == ZCPayTypeAliPay) {
        // 处理微信支付，没有安装微信客户端的情况
        if (payItem.payType == ZCPayTypeWeiXin) {
            if (![[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"weixin://"]]) {
                NSError *newError = [NSError errorWithDomain:ZCPayErrorDomain code:ZCPayResultFailure userInfo:@{NSLocalizedDescriptionKey:LGLocalizedString(@"noInstallWeiXin", @"请先安装微信客户端")}];
                self.payCompletion(newError, self.payResult);
                return;
            }
        }
#if ZCPayPingppIsOpen
        [self payInPingpp:payItem];
#endif
    } else if (payItem.payType == ZCPayTypeAipppay) {
#if ZCPayAiBeiIsOpen
        [self payInAiBei:payItem];
#endif
    }
}


#pragma mark 从app服务器查询订单状态
- (void)loolupOrderState:(NSString *)orderId {
    // 不需要走查询订单逻辑
    if (!_payItem.isLookupOrderStatus) {
        self.payCompletion(nil, self.payResult);
        return;
    }
    
    
    [NIMProgressHUD showWithStatus:LGLocalizedString(@"lookingOrder", @"查询订单中") maskType:NIMProgressHUDMaskTypeClear];
    
    _recordDate = [NSDate date]; //开始计时
    self.lookOrderStateCount = 0;
    
    __weak typeof(self)weakSelf = self;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        NSTimer *timer = [NSTimer scheduledTimerWithTimeInterval:2 target:self selector:@selector(loopForOrderState:) userInfo:nil repeats:YES];
        
        [weakSelf loopForOrderState:timer];
    });
}
- (void)loopForOrderState:(NSTimer *)timer {
    // 超时处理
    NSString *timeLength = [NSString stringWithFormat:@"%f", [[NSDate date] timeIntervalSinceDate:_recordDate]];
    
    self.lookOrderStateCount++;
    if (timeLength.intValue >= TimeLimitForLookOrderState || self.lookOrderStateCount > CountForLookOrderState) {
        NSError *error = [NSError errorWithDomain:ZCPayErrorDomain code:ZCPayResultTimeout userInfo:@{NSLocalizedDescriptionKey:LGLocalizedString(@"payTimeout", @"支付超时")}];
        self.payCompletion(error, self.payResult);
        [timer invalidate];
        timer = nil;
        _recordDate = nil;
        return;
    }
    
    [ZCLiveRequest requestAppServer:ZCURL_Lookup_Order_State parameter:@{@"order_id":_payItem.orderId, @"token":[ZBSDKConfigure sharedInstance].token} success:^(NSDictionary *dic) {
        _recordDate = nil;

        [self.payResult setObject:dic forKey:ZCPayOrderChangeKey];
                
        [timer invalidate];
        
        self.payCompletion(nil, self.payResult);
    } failure:^(NSError *error) {
        if (error.code == -3) { //订单未完成
            //次数可以给用户点温馨小提示，以缓解用户焦急的心情
            
        } else {
            NSError *error = [NSError errorWithDomain:ZCPayErrorDomain code:ZCPayResultFailure userInfo:@{NSLocalizedDescriptionKey:LGLocalizedString(@"payFailure", @"支付失败")}];
            self.payCompletion(error, self.payResult);
            [timer invalidate];
        }
    }];
}
@end

#if ZCPayAiBeiIsOpen
@implementation ZCPaymentManager (AiBeiPay)

- (void)iapppayKitRetPayStatusCode:(IapppayKitPayRetCode)statusCode resultInfo:(NSDictionary *)resultInfo {
    
    NSLog(@"statusCode:%d, errorCode:%@, errorMsg:%@", (int)statusCode, resultInfo[@"RetCode"], resultInfo[@"ErrorMsg"]);
    
    if (statusCode == IapppayKitPayRetSuccessCode) {
        NSString *signature = resultInfo[@"Signature"];
        NSString *signatureText = [ZCEncrypt URLDecodeString:signature];
        BOOL isSuccess = [IapppayOrderUtils checkPayResult:signatureText withAppKey:[ZBSDKConfigure sharedInstance].aipppayPlatformKey];
        if (isSuccess) {
            // 支付成功，验签成功
            [self loolupOrderState:_payItem.orderId];
            
        } else {
            // 支付成功，验签失败
            NSInteger errorCode = ZCPayResultFailure;
            NSString *errorMsg = LGLocalizedString(@"payFailure", @"支付失败");
            
            NSError *error = [NSError errorWithDomain:ZCPayErrorDomain code:errorCode userInfo:@{NSLocalizedDescriptionKey:errorMsg}];
            self.payCompletion(error, self.payResult);
            
        }
        
    } else {
        NSInteger errorCode = ZCPayResultFailure;
        NSString *errorMsg = LGLocalizedString(@"payFailure", @"支付失败");
        
        if (statusCode == IapppayKitPayRetCanceledCode) {
            // 支付失败
            if (!resultInfo) {
                errorCode = [resultInfo[@"RetCode"] integerValue];
                errorMsg = resultInfo[@"ErrorMsg"];
            }
            
        } else {
            // 支付取消
            errorMsg = LGLocalizedString(@"payCancel", @"用户取消操作");
            
        }
        
        NSError *error = [NSError errorWithDomain:ZCPayErrorDomain code:errorCode userInfo:@{NSLocalizedDescriptionKey:errorMsg}];
        self.payCompletion(error, self.payResult);
    }
}

@end
#endif


#if ZCPayPingppIsOpen
@implementation ZCPaymentManager (PingppPay)

- (void)handlePingppPayResult:(NSString *)url {
    if (!_isPaying) {
        return;
    }
    _isPaying = NO;
    
    // 微信支付
    NSString *weixinUrlPrefix = [NSString stringWithFormat:@"%@://pay/?", [self weixinScheme]];
    if ([url hasPrefix:weixinUrlPrefix]) {
        NSArray *paraArr = [url componentsSeparatedByString:@"&"];
        NSString *payResult = paraArr.lastObject;
        NSArray *payCodeArr = [payResult componentsSeparatedByString:@"="];
        NSString *payCode = payCodeArr.lastObject;
        
        if (payCode.integerValue == 0) {
            [self loolupOrderState:self.orderId];
            
        } else {
            if (payCode.integerValue == -2) {
                NSError *newError = [NSError errorWithDomain:ZCPayErrorDomain code:ZCPayResultFailure userInfo:@{NSLocalizedDescriptionKey:LGLocalizedString(@"payCancel", @"用户取消操作")}];
                self.payCompletion(newError, self.payResult);
            } else {
                NSError *newError = [NSError errorWithDomain:ZCPayErrorDomain code:ZCPayResultFailure userInfo:@{NSLocalizedDescriptionKey:LGLocalizedString(@"payFailure", @"支付失败")}];
                self.payCompletion(newError, self.payResult);
            }
        }
    }
    
    //支付宝支付
    NSString *decodeUrl = [ZCEncrypt URLDecodeString:url];
    NSString *aliPaySchemePrefix = [NSString stringWithFormat:@"%@://safepay/?", [self weixinScheme]];
    if ([decodeUrl hasPrefix:aliPaySchemePrefix]) {
        NSString *jsonStr = [decodeUrl substringFromIndex:aliPaySchemePrefix.length];
        
        NSData *jsonData = [jsonStr dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary* jsonDic = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingAllowFragments error:nil];
        
        NSInteger code = [jsonDic[@"memo"][@"ResultStatus"] integerValue];
        if (code == 9000) {
            [self loolupOrderState:self.orderId];
        } else {
            if (code == 6001) {
                //取消
                NSError *newError = [NSError errorWithDomain:ZCPayErrorDomain code:ZCPayResultFailure userInfo:@{NSLocalizedDescriptionKey:LGLocalizedString(@"payCancel", @"用户取消操作")}];
                self.payCompletion(newError, self.payResult);
            } else {
                NSError *newError = [NSError errorWithDomain:ZCPayErrorDomain code:ZCPayResultFailure userInfo:@{NSLocalizedDescriptionKey:LGLocalizedString(@"payFailure", @"支付失败")}];
                self.payCompletion(newError, self.payResult);
            }
        }
    }
}

@end
#endif
