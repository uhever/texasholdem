//
//  SignalStrengthManager.m
//  cocos
//
//  Created by user on 2017/2/17.
//  Copyright © 2017年 refrainC. All rights reserved.
//

#import "SignalStrengthManager.h"
#include <dlfcn.h>
#import <UIKit/UIKit.h>
@interface SignalStrengthManager ()

@property (nonatomic, strong)NSTimer *timer;

@property (nonatomic, assign)int signalStrength;

@end

@implementation SignalStrengthManager
+ (instancetype)manager{
    static SignalStrengthManager *mgr;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        mgr = [[SignalStrengthManager alloc]init];
    });
    
    return mgr;
}

- (instancetype)init{
    if (self = [super init]) {
        
        UIApplication *app = [UIApplication sharedApplication];
        NSArray *subviews = [[[app valueForKey:@"statusBar"] valueForKey:@"foregroundView"] subviews];
        NSString *dataNetworkItemView = nil;
        
        for (id subview in subviews) {
            if([subview isKindOfClass:[NSClassFromString(@"UIStatusBarSignalStrengthItemView") class]]) {
                dataNetworkItemView = subview;
                break;
            }
        }
        
        self.signalStrength = [[dataNetworkItemView valueForKey:@"_signalStrengthBars"] intValue] * 0.2;
        
    }
    return self;
}

- (void)startObserveSignalStrength{
    
    self.timer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updateSignalStrength) userInfo:nil repeats:YES];
}

- (void)stopObserveSignalStrength{
    [self.timer invalidate];
    self.timer = nil;
}


- (void)updateSignalStrength{
    
    [self getSignalStrength];
    
}


- (void)getSignalStrength{
    
    UIApplication *app = [UIApplication sharedApplication];
    NSArray *subviews = [[[app valueForKey:@"statusBar"] valueForKey:@"foregroundView"] subviews];
    NSString *dataNetworkItemView = nil;
    
    for (id subview in subviews) {
        if([subview isKindOfClass:[NSClassFromString(@"UIStatusBarSignalStrengthItemView") class]]) {
            dataNetworkItemView = subview;
            break;
        }
    }
    
    int signalStrength = [[dataNetworkItemView valueForKey:@"_signalStrengthBars"] intValue];
    

    
    if (self.signalStrength != signalStrength) {
        !_SignalStrengthChange ? : _SignalStrengthChange(signalStrength * 0.2);
    }
    
    self.signalStrength = signalStrength * 0.2;

}

- (float)getCurrentSignalStrength{
    return self.signalStrength;
}


@end
