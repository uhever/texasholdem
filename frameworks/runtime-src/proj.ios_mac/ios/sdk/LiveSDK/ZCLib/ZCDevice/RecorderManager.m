//
//  RecorderManager.m
//  cocos
//
//  Created by user on 2017/2/17.
//  Copyright © 2017年 refrainC. All rights reserved.
//

#import "RecorderManager.h"
#import <AVFoundation/AVFoundation.h>
#include <CoreFoundation/CoreFoundation.h>


#define kSandboxPathStr [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject]

#define kMp3FileName @"myRecord.mp3"
#define kCafFileName @"myRecord.caf"
@interface RecorderManager ()<AVAudioPlayerDelegate,AVAudioRecorderDelegate>

@property (nonatomic,copy) NSString *cafPathStr;
@property (nonatomic,copy) NSString *mp3PathStr;
@property (nonatomic,strong) AVAudioRecorder *audioRecorder;//音频录音机



@end


@implementation RecorderManager

+ (instancetype)manager{
    static RecorderManager *mgr;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        mgr = [[RecorderManager alloc]init];
    });
    return mgr;
}

- (instancetype)init{
    if (self = [super init]) {
        
        
        self.cafPathStr = [kSandboxPathStr stringByAppendingPathComponent:kCafFileName];
        self.mp3PathStr =  [kSandboxPathStr stringByAppendingPathComponent:kMp3FileName];
        
    }
    
    return self;
}


- (void)startRecorderWithSuccess:(void(^)(BOOL isSuccess))success{
    NSError *error;
    AVAudioSession *audioSession=[AVAudioSession sharedInstance];
    [audioSession setCategory:AVAudioSessionCategoryRecord error:&error];
    
    if (error) {
        NSLog(@"AVAudioSession setCategory error = %@",error.userInfo);
    }
    
    if (![self.audioRecorder isRecording]) {
        BOOL isRecordSuccess = [self.audioRecorder record];
        
        if (isRecordSuccess) {
            !success ? : success(YES);
            NSLog(@"录音开始成功");
        }else{
            NSLog(@"录音开始失败");
            !success ? : success(NO);
        }
        
    }
}

/**
 *  取得录音文件设置
 *
 *  @return 录音设置
 */
-(NSDictionary *)getAudioSetting{
    //LinearPCM 是iOS的一种无损编码格式,但是体积较为庞大
    //录音设置
    NSMutableDictionary *recordSettings = [[NSMutableDictionary alloc] init];
    //录音格式 无法使用
    [recordSettings setValue :[NSNumber numberWithInt:kAudioFormatLinearPCM] forKey: AVFormatIDKey];
    //采样率
    [recordSettings setValue :[NSNumber numberWithFloat:11025.0] forKey: AVSampleRateKey];//44100.0
    //通道数
    [recordSettings setValue :[NSNumber numberWithInt:2] forKey: AVNumberOfChannelsKey];
    //线性采样位数
    //[recordSettings setValue :[NSNumber numberWithInt:16] forKey: AVLinearPCMBitDepthKey];
    //音频质量,采样质量
    [recordSettings setValue:[NSNumber numberWithInt:AVAudioQualityMin] forKey:AVEncoderAudioQualityKey];
    
    return recordSettings;
}
/**
 *  获得录音机对象
 *
 *  @return 录音机对象
 */
-(AVAudioRecorder *)audioRecorder{
    if (!_audioRecorder) {
        //创建录音文件保存路径
        NSURL *url=[NSURL URLWithString:self.cafPathStr];
        //创建录音格式设置
        NSDictionary *setting=[self getAudioSetting];
        //创建录音机
        NSError *error=nil;
        
        _audioRecorder=[[AVAudioRecorder alloc]initWithURL:url settings:setting error:&error];
        _audioRecorder.delegate=self;
        _audioRecorder.meteringEnabled=YES;//如果要监控声波则必须设置为YES
        if (error) {
            NSLog(@"创建录音机对象时发生错误，错误信息：%@",error.localizedDescription);
            return nil;
        }
    }
    return _audioRecorder;
}


- (void)startRecorder{
    
    NSError *error;
    AVAudioSession *audioSession=[AVAudioSession sharedInstance];
    [audioSession setCategory:AVAudioSessionCategoryPlayAndRecord error:&error];
    
    if (error) {
        NSLog(@"AVAudioSession setCategory error = %@",error.userInfo);
    }
    
    if (![self.audioRecorder isRecording]) {
        BOOL isRecordSuccess = [self.audioRecorder record];
        
        if (isRecordSuccess) {
            NSLog(@"录音成功开始");
        }else{
            NSLog(@"录音开始失败");
        }
        
    }
    
}

- (void)stopRecorder{
    if ([self.audioRecorder isRecording]) {
        [self.audioRecorder stop];
    }
    
}
- (void)audioRecorderDidFinishRecording:(AVAudioRecorder *)recorder successfully:(BOOL)flag{
    
    NSLog(@"录音完成");
    
    !_recorderComplete ? : _recorderComplete(YES);
    
}


- (void)audioRecorderEncodeErrorDidOccur:(AVAudioRecorder *)recorder error:(NSError * __nullable)error{
    NSLog(@"录音编码error %@",error.userInfo);
}


- (void)audioRecorderBeginInterruption:(AVAudioRecorder *)recorder{
    
    !_interruptionBlock ? : _interruptionBlock();
    
}

- (void)audioRecorderEndInterruption:(AVAudioRecorder *)recorder withOptions:(NSUInteger)flags{
    !_interruptionBlock ? : _interruptionBlock();
}

- (void)audioRecorderEndInterruption:(AVAudioRecorder *)recorder withFlags:(NSUInteger)flags {
    !_interruptionBlock ? : _interruptionBlock();
}

- (void)audioRecorderEndInterruption:(AVAudioRecorder *)recorder{
    !_interruptionBlock ? : _interruptionBlock();
}


@end
