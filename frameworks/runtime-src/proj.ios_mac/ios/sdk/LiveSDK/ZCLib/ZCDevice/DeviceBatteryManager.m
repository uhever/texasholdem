//
//  DeviceBatteryManager.m
//  cocos
//
//  Created by user on 2017/2/17.
//  Copyright © 2017年 refrainC. All rights reserved.
//

#import "DeviceBatteryManager.h"
#import <UIKit/UIKit.h>
@implementation DeviceBatteryManager

+ (instancetype)batteryManager{
    
    static DeviceBatteryManager *mgr;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        mgr = [[DeviceBatteryManager alloc]init];
    });
    return mgr;
}


- (float)currentBatteryLevel{
    
    UIDevice *myDevice = [UIDevice currentDevice];
    [myDevice setBatteryMonitoringEnabled:YES];
    float batteryLevel = [myDevice batteryLevel];
    
    return batteryLevel;
}

- (instancetype)init{
    self = [super init];
    if (self) {
        
        UIDevice *device = [UIDevice currentDevice];
        device.batteryMonitoringEnabled = YES;
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(checkBattery:) name:UIDeviceBatteryLevelDidChangeNotification object:nil];
    }
    
    return self;
    
}


- (void)checkBattery:(NSNotification *)noti{
    
    !_batteryChange ? : _batteryChange(self.currentBatteryLevel);
}

@end
