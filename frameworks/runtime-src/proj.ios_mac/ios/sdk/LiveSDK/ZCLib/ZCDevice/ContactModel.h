//
//  ContactModel.h
//  cocos
//
//  Created by user on 2017/2/17.
//  Copyright © 2017年 refrainC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ContactModel : NSObject

@property (copy, nonatomic) NSString *phone; // 电话

@property (nonatomic, copy)NSString *name; //姓名

@property (copy, nonatomic) NSString *firstCharater; // 索引字母
@end
