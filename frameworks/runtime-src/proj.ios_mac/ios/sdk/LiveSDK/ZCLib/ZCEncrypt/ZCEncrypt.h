//
//  ZCEncrypt.h
//  ZCLibProjectDEV
//
//  Created by Happy on 8/27/16.
//  Copyright © 2016 Happy. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ZCEncrypt : NSObject
/**
 *  sha1加密
 */
+ (NSString *)encryptWithsha1:(NSString *)input;


/**
 *  AES加解密
 *
 *  @param input 需要加解密的字符串
 *  @param key   key
 *
 */
+ (NSString *)encryptWithAES256:(NSString *)input key:(NSString *)key;
+ (NSString *)decryptWithAES256:(NSString *)input key:(NSString *)key;


/**
 *  URL encode or decode
 *
 *  @param URLString 待处理的url
 *
 */
+ (NSString *)URLEncodeString:(NSString *)URLString;
+ (NSString *)URLDecodeString:(NSString *)URLString;


/**
 String Base64 encode or decode
 
 @param string 参数
 */
+ (NSString *)base64EncodedString:(NSString *)string;
+ (NSString *)base64DecodedString:(NSString *)string;


/**
 *  获取字典对象对应的json串
 */
+ (NSString*)getJsonString:(NSDictionary*)dic;
/**
 *  获取"key1=value1&key2=value2"格式的字符串
 */
+ (NSString *)getKeyValuedString:(NSDictionary *)dic;
@end
