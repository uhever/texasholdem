//
//  ZCPlatformRequestManager.h
//  GameDemo
//
//  Created by Happy on 12/16/16.
//  Copyright © 2016 Happy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ZCPlatformRequest.h"

extern NSString *const ZBLoginErrorDomain;

// 从微信登录结果中获取value的两个key
extern NSString *const ZBLoginResultKeyForLogin;
extern NSString *const ZBLoginResultKeyForProfile;

// 登录错误
typedef NS_ENUM(NSInteger, ZBLoginError) {
    ZBLoginErrorBlankPhoneNum = -1, //手机号不能为空
    ZBLoginErrorBlankAuthCode = -2, //验证码不能为空
    ZBLoginErrorSendAuthCodeFrequent = -3, //60秒内不能重复发送
    ZBLoginErrorOther
};


typedef void(^ZCPlatformRequestCallback)(NSError *error, NSDictionary *result);

@interface ZCPlatformRequestManager : NSObject
+ (void)requestForVisitor:(ZCPlatformRequestCallback)callback;

+ (void)requestForSendAuthCode:(NSString *)phoneNum callback:(ZCPlatformRequestCallback)callback;
+ (void)requestForPhoneLogin:(NSString *)phoneNum authCode:(NSString *)authCode callback:(ZCPlatformRequestCallback)callback;


/**
 微信登录平台服务器

 @param wxCode 微信code
 @param wxAppid 微信appid
 @param callback 结果dic为@{ZBLoginResultKeyForLogin:@"登录平台服务器获取的session、uid和authkey"; ZBLoginResultKeyForProfile:@"用户在微信的昵称、头像等信息"}
 */
+ (void)requestForWeiXinLogin:(NSString *)wxCode wxAppid:(NSString *)wxAppid callback:(ZCPlatformRequestCallback)callback;

+ (void)requestForFacebookLogin:(NSDictionary *)params callback:(ZCPlatformRequestCallback)callback;


+ (void)requestForPingppPay:(NSDictionary *)params callback:(ZCPlatformRequestCallback)callback;


@end
