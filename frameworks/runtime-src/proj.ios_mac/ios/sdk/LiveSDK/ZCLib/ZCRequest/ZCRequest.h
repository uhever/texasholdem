//
//  ZCRequest.h
//  ZCLibProjectDEV
//
//  Created by Happy on 8/27/16.
//  Copyright © 2016 Happy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFNetworking.h"
#import "ZCRequestConfig.h"
#import "ZBSDKConfigure.h"
#import "LGLocalization.h"
#import "ZCLocalData.h"
#import "ZCDeviceInfo.h"
#import "ZCEncrypt.h"
#import "ZCUtils.h"

extern NSString *const ZCRequestEncryptKey;
extern NSString *const ZCRequestErrorDomain;

typedef void(^RequestSuccess)(NSDictionary *dic);
typedef void(^RequestFailure)(NSError *error);
typedef NS_ENUM(NSInteger, ZCRequestEncryptType) {
    ZCRequestEncryptTypeNone, //无加密
    ZCRequestEncryptTypeUTF8, //UTF8编码
    ZCRequestEncryptTypeUTF8AndBase64 //UTF8编码后base64
};

// 请求错误码解说
typedef NS_ENUM(NSInteger, ZCRequestErrorCode) {
    ZCRequestErrorCodeNoNetwork = -1004, //无网络
};


@interface ZCRequest : NSObject
@property (nonatomic, strong) AFHTTPSessionManager *httpOperationManager;


- (id)initWithBaseURL:(NSString *)baseURL;
- (void)post:(NSString *)URLString parameter:(NSDictionary *)paramter success:(RequestSuccess)success failure:(RequestFailure)failure;
- (void)get:(NSString *)URLString parameter:(NSDictionary *)parameter success:(RequestSuccess)success failure:(RequestFailure)failure;

+ (NSString *)deviceInfo;
+ (NSString *)languageParameter;

/**
 获取加密后data

 @param encryptType 加密方式
 @return 加密后的data
 */
+ (NSData *)requestBodyData:(NSDictionary *)encryptType;
@end

/*
 base url后边记着添加‘\’，relative url前边不要添加'\'
 */
