//
//  ZCLiveRequestManager.h
//  GameDemo
//
//  Created by Happy on 12/16/16.
//  Copyright © 2016 Happy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ZCLiveRequest.h"

typedef void(^ZCLiveRequestCallback)(NSError *error, NSDictionary *dic);

@interface ZCLiveRequestManager : ZCLiveRequest
// 登录直播UC服务器
+ (void)requestForLoginUCServer:(NSDictionary *)dic callback:(ZCLiveRequestCallback)callback;
// 登录直播App服务器
+ (void)requestForLoginAppServer:(NSDictionary *)dic callback:(ZCLiveRequestCallback)callback;

// 登录直播UC和App服务器
+ (void)requestForLoginLiveServer:(NSDictionary *)dic callback:(ZCLiveRequestCallback)callback;


/**
 创建预订单
 @param zcAppId 平台AppId
 @param payType 支付类型（ios:苹果内支付, wx:微信支付, ali:支付宝支付, aipp:爱贝支付。默认为ios）
 @param goodIndex 商品索引，从1开始
 @param goodType 商品类型（默认为0）
 @param token token
 @param reviewState 与审核状态相反（默认为0）
 @param serverPoker 游戏大厅服务器端口号，不需要为空即可
 */
+ (void)requestForCreatePreOrder:(NSString *)zcAppId
                         payType:(NSString *)payType
                       goodIndex:(NSString *)goodIndex
                        goodType:(NSString *)goodType
                           token:(NSString *)token
                     reviewState:(NSString *)reviewState
                     serverPoker:(NSString *)serverPoker
                        callback:(ZCLiveRequestCallback)callback;
@end
