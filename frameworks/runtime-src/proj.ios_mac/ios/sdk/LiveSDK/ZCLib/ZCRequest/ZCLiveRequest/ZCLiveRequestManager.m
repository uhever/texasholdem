//
//  ZCLiveRequestManager.m
//  GameDemo
//
//  Created by Happy on 12/16/16.
//  Copyright © 2016 Happy. All rights reserved.
//

#import "ZCLiveRequestManager.h"

@implementation ZCLiveRequestManager
+ (void)postForGameLogin:(NSDictionary *)dic callback:(ZCLiveRequestCallback)callback {
    [self requestForLoginUCServer:dic callback:^(NSError *error, NSDictionary *dic) {
        if (error) {
            callback(error, nil);
            
        } else {
            callback(nil, dic);
        }
    }];
}
+ (void)requestForLoginLiveServer:(NSDictionary *)dic callback:(ZCLiveRequestCallback)callback {
    NSDictionary *profileDic = dic[@"com.zblogin.LoginResultKeyForProfile"] ? dic[@"com.zblogin.LoginResultKeyForProfile"]:@{};
    NSDictionary *loginDic = dic[@"com.zblogin.LoginResultKeyForLogin"];

    [self requestForLoginUCServer:loginDic callback:^(NSError *error, NSDictionary *dic) {
        if (error) {
            callback(error, nil);
        } else {
            NSMutableDictionary *appServerParams = profileDic.mutableCopy;
            [appServerParams addEntriesFromDictionary:dic];
            [self requestForLoginAppServer:appServerParams callback:callback];
        }
    }];
}
+ (void)requestForLoginUCServer:(NSDictionary *)dic callback:(ZCLiveRequestCallback)callback {
    NSMutableDictionary *newParams = dic.mutableCopy;
    [newParams addEntriesFromDictionary:@{@"channel":[ZBSDKConfigure sharedInstance].projectChannelId,
                                          @"idfa":@"",
                                          @"appid":[ZBSDKConfigure sharedInstance].appid,
                                          @"mac":@"",
                                          @"android_id":@""}];
    
    [ZCLiveRequest requestUCServer:ZCURL_Login_UCServer parameter:newParams success:^(NSDictionary *dic) {
        callback(nil, @{@"uctoken":dic[@"token"]});
        
    } failure:^(NSError *error) {
        callback(error, nil);
        
    }];
}

+ (void)requestForLoginAppServer:(NSDictionary *)dic callback:(ZCLiveRequestCallback)callback {
    NSMutableDictionary *mutParams = dic.mutableCopy;
    mutParams[@"idfa"] = @"";

    [ZCLiveRequest requestAppServer:ZCURL_Login_LiveServer parameter:mutParams success:^(NSDictionary *dic) {
        [ZCLocalData userDefaultSave:dic[@"token"] key:ZCLiveRequestLiveAppServerTokenKey];
        [ZCLocalData userDefaultSave:dic[@"uid"] key:ZCLiveRequestLiveAppServerUIDKey];

        callback(nil, dic);
        
    } failure:^(NSError *error) {
        callback(error, nil);
        
    }];
}

+ (void)requestForCreatePreOrder:(NSString *)zcAppId
                         payType:(NSString *)payType
                       goodIndex:(NSString *)goodIndex
                        goodType:(NSString *)goodType
                           token:(NSString *)token
                     reviewState:(NSString *)reviewState
                     serverPoker:(NSString *)serverPoker
                        callback:(ZCLiveRequestCallback)callback {
    NSMutableDictionary *parameter = @{@"appid":zcAppId,
                                       @"pay_type":payType.length?payType:@"ios",
                                       @"id":goodIndex,
                                       @"type":goodType.length?goodType:@"0",
                                       @"server_review":reviewState.length?reviewState:@"1",
                                       @"token":token,
                                       @"server_poker":serverPoker.length?serverPoker:@""}.mutableCopy;
    
    [ZCLiveRequest requestPayServer:ZCURL_Create_Prepay_Order parameter:parameter success:^(NSDictionary *dic) {
        callback(nil, dic);
        
    } failure:^(NSError *error) {
        callback(error, nil);
        
    }];
}
@end
