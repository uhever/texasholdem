//
//  ZCPlatformRequestManager.m
//  GameDemo
//
//  Created by Happy on 12/16/16.
//  Copyright © 2016 Happy. All rights reserved.
//

#import "ZCPlatformRequestManager.h"

NSString *const ZBLoginErrorDomain = @"com.zblogin.result";
NSString *const ZBLoginResultKeyForLogin = @"com.zblogin.LoginResultKeyForLogin";
NSString *const ZBLoginResultKeyForProfile = @"com.zblogin.LoginResultKeyForProfile";

@implementation ZCPlatformRequestManager
+ (void)requestForVisitor:(ZCPlatformRequestCallback)callback {
    NSDictionary *params = @{@"uuid": [ZCDeviceInfo deviceUUIDString],
                             @"devInfo":@""};
    
    [ZCPlatformRequest request:ZCURL_Trial_Login parameter:params success:^(NSDictionary *dic) {
        callback(nil, dic);
        
    } failure:^(NSError *error) {
        callback(error, nil);
        
    }];
}

+ (void)requestForSendAuthCode:(NSString *)phoneNum callback:(ZCPlatformRequestCallback)callback {
    NSDictionary *params = @{@"mobileNumber": phoneNum};
    [ZCPlatformRequest request:ZCURL_Send_AuthCode parameter:params success:^(NSDictionary *dic) {
        callback(nil, dic);
        
    } failure:^(NSError *error) {
        
        NSError *newError = error;
        if (error.code == 60010) { //60秒内不能重复发送
            newError = [NSError errorWithDomain:ZBLoginErrorDomain code:ZBLoginErrorSendAuthCodeFrequent userInfo:@{NSLocalizedDescriptionKey:@"60秒内不能重复发送"}];
            
        }
        
        callback(newError, nil);
    }];

}
+ (void)requestForPhoneLogin:(NSString *)phoneNum authCode:(NSString *)authCode callback:(ZCPlatformRequestCallback)callback {
    NSDictionary *params = @{@"mobileNumber": phoneNum,
                             @"seccode" : authCode,
                             @"devInfo" : @""};
    
    [ZCPlatformRequest request:ZCURL_Mobile_Login parameter:params success:^(NSDictionary *dic) {
        callback(nil, dic);
        
    } failure:^(NSError *error) {
        callback(error, nil);

    }];
}

+ (void)requestForWeiXinLogin:(NSString *)wxCode wxAppid:(NSString *)wxAppid callback:(ZCPlatformRequestCallback)callback {
    NSDictionary *params = @{@"code":wxCode,
                             @"devInfo":@"",
                             @"wcAppId":wxAppid,
                             @"isUserInfo":@"1"};
    
    [ZCPlatformRequest request:ZCURL_Weixin_Login parameter:params success:^(NSDictionary *dic) {
        NSDictionary *paramsForLogin = @{@"uId":[dic objectForKey:@"uId"],
                                         @"session":[dic objectForKey:@"session"],
                                         @"authkey":[dic objectForKey:@"authkey"]};
        
        NSMutableDictionary *wxInfoDic = [NSMutableDictionary dictionary];
        if ([dic objectForKey:@"name"]) {
            [wxInfoDic setObject:[dic objectForKey:@"name"] forKey:@"nickname"];
        }
        if ([dic objectForKey:@"sex"]) {
            [wxInfoDic setObject:[NSString stringWithFormat:@"%@", [dic objectForKey:@"sex"]] forKey:@"sex"];
        }
        if ([dic objectForKey:@"hdImage"]) {
            [wxInfoDic setObject:[dic objectForKey:@"hdImage"] forKey:@"icon"];
        }
        if ([dic objectForKey:@"city"]) {
            [wxInfoDic setObject:[dic objectForKey:@"city"] forKey:@"city"];
        }
        if ([dic objectForKey:@"country"]) {
            [wxInfoDic setObject:[dic objectForKey:@"country"] forKey:@"country"];
        }
        if ([dic objectForKey:@"province"]) {
            [wxInfoDic setObject:[dic objectForKey:@"province"] forKey:@"province"];
        }
        callback(nil, @{ZBLoginResultKeyForLogin:paramsForLogin, ZBLoginResultKeyForProfile:wxInfoDic});
        
    } failure:^(NSError *error) {
        callback(error, nil);
        
    }];
}

+ (void)requestForFacebookLogin:(NSDictionary *)params callback:(ZCPlatformRequestCallback)callback {
    NSDictionary *parameters = @{@"fbAppId":params[@"fbAppId"],
                                 @"devInfo":@"devInfo",
                                 @"isUserInfo":@"1"};
    
    [ZCPlatformRequest request:ZCURL_Facebook_Login parameter:parameters success:^(NSDictionary *dic) {
        callback(nil, dic);
        
    } failure:^(NSError *error) {
        callback(error, nil);
        
    }];
}


//{ZCPlatformSessionKey:@"",
//    @"appGoodsId":[NSString stringWithFormat:@"%@", payDic[@"product_id"]],
//    @"appGoodsName":payDic[@"product_name"],
//    @"appGoodsPrice":[NSString stringWithFormat:@"%d", [payDic[@"amount"] intValue]*100],
//    @"appCallback":[ZCPaymentManager shareInstance].payCallback,
//    @"appOrderId":payDic[@"app_order_id"],
//    @"channel":[self platformPayChannel]}
+ (void)requestForPingppPay:(NSDictionary *)params callback:(ZCPlatformRequestCallback)callback {
    
}
@end

