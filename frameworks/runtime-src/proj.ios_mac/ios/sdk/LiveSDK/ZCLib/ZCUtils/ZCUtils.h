//
//  ZCUtils.h
//  HuaRong
//
//  Created by Happy on 8/31/16.
//  Copyright © 2016 Happy. All rights reserved.
//  一些实用工具

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface ZCUtils : NSObject
/**
 *  通过十六进制获取UIColor
 *
 *  @param color 十六进制颜色
 *  @param alpha 透明度
 *
 *  @return UIColor
 */
+ (UIColor *)colorWithHexString:(NSString *)hexString alpha:(CGFloat)alpha;
+ (UIColor *)colorWithHexString:(NSString *)hexString;


/**
 *  获取特定颜色的字符串富文本
 */
+ (NSAttributedString *)textAttributedString:(NSString *)text color:(UIColor *)color;
/**
 *  获取特定Bounds的图片富文本
 */
+ (NSAttributedString *)imageAttributedString:(NSString *)imageName bounds:(CGRect)bounds;
/**
 *  根据不同颜色组合富文本
 *
 *  @param textArr 富文本数组
 *  @param colorArr 颜色数组，如果为空，则默认白色
 *
 *  @return
 */
+ (NSAttributedString *)attributedStr:(NSArray *)textArr color:(NSArray *)colorArr;


/**
 *  图片等比例压缩处理
 *
 *  @param sourceImage 源图片
 *  @param targetWidth 目标尺寸
 *  @param compression 压缩比例系数(大于0，小于等于1，最好0.3~0.7之间，过小可能会有黑边)
 *
 *  @return 图片的data数据
 */
+ (NSData *)imageCompress:(UIImage *)sourceImage targetWidth:(CGFloat)targetWidth compression:(CGFloat)compression;
/**
 *  根据color创建UIImage
 *
 *  @param color
 *
 *  @return
 */
+ (UIImage *)imageFromColor:(UIColor *)color rect:(CGRect)rect;
+ (UIImage *)imageFromHexColor:(NSString *)hexColor rect:(CGRect)rect;

@end
