//
//  ZCShare.h
//  ZCPlatformSDK
//
//  Created by Happy on 6/22/16.
//  Copyright © 2016 ZhanCheng. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ZCBShareContent.h"

#define ZBShareWeiXinIsOpen 1
#define ZBShareWeiBoIsOpen 0
#define ZBShareQQIsOpen 0
#define ZBShareFaceBookIsOpen 0

#if ZBShareWeiXinIsOpen
#import "WXApi.h"
#endif

#if ZBShareQQIsOpen
#import <TencentOpenAPI/TencentOAuth.h>
#endif

#if ZBShareWeiBoIsOpen
#import "WeiboSDK.h"
#endif

#if ZBShareFaceBookIsOpen
#import "FBSDKCoreKit/FBSDKCoreKit.h"
#import <FBSDKShareKit/FBSDKShareKit.h>
#endif

// 分享方式
typedef NS_ENUM(NSInteger, ZCBShareType) {
    ZCBShareTypeWeiXin = 1,
//    ZCBShareTypeWeiQQ = 2,
//    ZCBShareTypeWeiBo = 3,
    ZCBShareTypeFacebook = 4
};


@class ZCBShareContent;
@interface ZCShare : NSObject
@property (nonatomic, copy) NSString *shareId; //游戏的分享功能Id
@property (nonatomic, copy) NSString *weiXinAppId;

+ (ZCShare *)shareInstance;

/**
 *  分享文本，目前只支持微信分享
 *

 **
 *  分享图片
 *
 *  可以直接指定要分享的图片或者其连接地址
 *

 **
 *  分享链接
 *
 *  微信分享可以指定Title、缩略图(不超过32k)和网址；Facebook分享不能指定缩略图。
 *

 **
 *  分享视频
 */
- (void)share:(ZCBShareContent *)shareContent shareType:(ZCBShareType)shareType callback:(void(^)(NSError *error, NSDictionary *result))callback;

@end
