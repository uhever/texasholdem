//
//  ZBSDKConfigure.m
//  GameDemo
//
//  Created by Happy on 12/16/16.
//  Copyright © 2016 Happy. All rights reserved.
//

#import "ZBSDKConfigure.h"
#import "ZCLocalData.h"

@interface ZBSDKConfigure ()
@end


@implementation ZBSDKConfigure

static ZBSDKConfigure *manager = nil;
+ (instancetype)sharedInstance {
    if (!manager) {
        manager = [[ZBSDKConfigure alloc] init];
    }
    
    return manager;
}

- (NSString *)projectChannelId {
    if (_projectChannelId) {
        return _projectChannelId;
    }
    return @"";
}

- (NSString *)appProductId {
    if (_appProductId) {
        return _appProductId;
    }
    return @"";
}

- (NSString *)uid {
//    if ([HRSettings shareInstance].userInfo.uid) {
//        return [HRSettings shareInstance].userInfo.uid;
//    }
    
    return @"";
}


- (int)isReview {
    if ([ZBSDKConfigure sharedInstance].serverProcess == ZBSDKProcessTypeLiveApp) {
        return _isReview;
    } else {
        return 1;
    }
}
@end
