//
//  ZCLoginManager.h
//  GameDemo
//
//  Created by Happy on 1/16/17.
//  Copyright © 2017 Happy. All rights reserved.
//
#import <Foundation/Foundation.h>


#define ZBLoginWeiXinIsOpen 1
#define ZBLoginWeiBoIsOpen 0
#define ZBLoginQQIsOpen 0
#define ZBLoginFacebookIsOpen 0


#if ZBLoginWeiXinIsOpen
#import "WXApi.h"
#endif

#if ZBLoginQQIsOpen
#import <TencentOpenAPI/TencentOAuth.h>
#endif

#if ZBLoginWeiBoIsOpen
#import "WeiboSDK.h"
#endif

#if ZBLoginFacebookIsOpen
#import "FBSDKCoreKit/FBSDKCoreKit.h"
#import "FBSDKLoginKit/FBSDKLoginKit.h"
#endif

#import "ZCPlatformRequestManager.h"


// 登录方式
typedef NS_ENUM(NSInteger, ZBLoginType) {
    ZBLoginTypeWeiXin = 1,
    ZBLoginTypeFacebook = 2
};

@interface ZCLoginManager : NSObject
@property (nonatomic, copy) NSString *weiXinAppId;

+ (instancetype)sharedInstance;

- (void)loginInWeiXin:(void(^)(NSError *error, NSDictionary *result))callback;

- (void)loginInFacebook:(void(^)(NSError *error, NSDictionary *dic))callback;

@end
