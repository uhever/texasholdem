//
//  ZCThirdDelegate.h
//  GameDemo
//
//  Created by Happy on 1/16/17.
//  Copyright © 2017 Happy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ZCShare.h"
#import "ZCLoginManager.h"

typedef void(^ZCLoginCallback)(NSError *error, NSDictionary *result, ZBLoginType loginType);
typedef void(^ZCShareCallback)(NSError *error, NSDictionary *result, ZCBShareType shareType);

@interface ZCThirdDelegate : NSObject
+ (ZCThirdDelegate *)sharedInstance;

@property (nonatomic, copy) ZCLoginCallback loginCallback;
@property (nonatomic, copy) ZCShareCallback shareCallback;

@end

#if ZBShareWeiXinIsOpen | ZBLoginWeiXinIsOpen
@interface ZCThirdDelegate (WeiXinShare) <WXApiDelegate>

@end
#endif

#if ZBShareQQIsOpen | ZBLoginQQIsOpen
@interface ZCThirdDelegate (QQShare) <WXApiDelegate>

@end
#endif

#if ZBShareWeiBoIsOpen | ZBLoginWeiBoIsOpen
@interface ZCThirdDelegate (WeiBoShare) <WXApiDelegate>

@end
#endif

#if ZBShareFaceBookIsOpen | ZBLoginFacebookIsOpen
@interface ZCThirdDelegate (FacebookShare) <FBSDKSharingDelegate>

@end
#endif
