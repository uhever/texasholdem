//
//  NSDATAtostring.m
//  pgdemo
//
//  Created by zctech on 14-2-24.
//
//

#import "NSDATAtostring.h"

@implementation NSData(ToNSString)
-(NSString *)ConvertToNSString{
    
    NSMutableString *strTemp = [NSMutableString stringWithCapacity:[self length]*2];
    
    const unsigned char *szBuffer = [self bytes];
    
    for (NSInteger i=0; i < [self length]; ++i) {
        
        [strTemp appendFormat:@"%02x",(unsigned long)szBuffer[i]];
        
    }
    
    return [[strTemp retain] autorelease];
    
}
@end
