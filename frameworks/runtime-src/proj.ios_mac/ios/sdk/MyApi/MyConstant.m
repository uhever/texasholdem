//
//  MyConstant.m
//  Poker
//
//  Created by zctech-011 on 2016/12/5.
//
//

#import "MyConstant.h"

static bool isClickRotate = NO;
static bool isRotating = NO;
static bool isFirstRotate = NO;

NSString *orientation = @"";

@implementation MyConstant

+ (void)setClickRotate:(bool)clickRotate
{
    isClickRotate = clickRotate;
}

+ (bool)getClickRotate
{
    return isClickRotate;
}


+ (void)setRotating:(bool)rotating
{
    isRotating = rotating;
}

+ (bool)getRotating
{
    return isRotating;
}

+ (void)setFirstRotate:(bool)firstRotate
{
    isFirstRotate = firstRotate;
}

+ (bool)getFitstRotate
{
    return isFirstRotate;
}

+ (void)setOrentation:(NSString *)_orentation
{
    orientation = _orentation;
}
+ (NSString *)getOrentation
{
    return orientation;
}

@end
