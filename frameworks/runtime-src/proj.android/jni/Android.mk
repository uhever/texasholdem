LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE := cocos2dlua_shared

LOCAL_MODULE_FILENAME := libcocos2dlua

LOCAL_SRC_FILES := \
../../Classes/AppDelegate.cpp \
hellolua/main.cpp \
../../Classes/lua_user_register.cpp \
../../Classes/ZCTool/ZCToolPlatform.cpp \
../../Classes/ZCTool/ZCTool.cpp \
../../Classes/ZCTool/lua_ZCTool_auto.cpp \
../../Classes/ZCTool/GLScissorNode.cpp \
../../Classes/Gotye/lua_GotyeTool_manual.cpp \
../../Classes/Gotye/lua_GotyeTool_auto.cpp \
../../Classes/Gotye/GotyeTool.cpp \




LOCAL_C_INCLUDES := $(LOCAL_PATH)/../../Classes

# _COCOS_HEADER_ANDROID_BEGIN
# _COCOS_HEADER_ANDROID_END

LOCAL_WHOLE_STATIC_LIBRARIES += gotyeapi_static


LOCAL_STATIC_LIBRARIES := cocos2d_lua_static

# _COCOS_LIB_ANDROID_BEGIN
# _COCOS_LIB_ANDROID_END

include $(BUILD_SHARED_LIBRARY)

$(call import-add-path,$(LOCAL_PATH))
$(call import-module, gotyeapi)

$(call import-module,scripting/lua-bindings/proj.android)



# _COCOS_LIB_IMPORT_ANDROID_BEGIN
# _COCOS_LIB_IMPORT_ANDROID_END
