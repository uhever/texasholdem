package org.cocos2dx.plugin;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.cocos2dx.lib.Cocos2dxGLSurfaceView;
import org.cocos2dx.lib.Cocos2dxLuaJavaBridge;
import org.json.JSONException;
import org.json.JSONObject;

import com.zhanyouall.poker.AppController;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

public class UserSDK implements SDKHelper {
	private Activity context;
	private static boolean isInit = false;
	public static boolean isLogin = false;
	private static final int RQF_PAY = 1;

	private static final int RQF_LOGIN = 2;
	private static final String TAG = "alipay";

	public UserSDK(Activity context) {
		this.context = context;
	}

	@Override
	public void initSDK() {
		
		isLogin = false;
		isInit = true;
		final AppController SanguoContext = (AppController) context;
		SanguoContext.doCallbackByName("initSDK", "true");

	}

	public static boolean isLandscape(Activity context) {
		Configuration config = context.getResources().getConfiguration();
		int orientation = config.orientation;

		if (orientation != Configuration.ORIENTATION_LANDSCAPE
				&& orientation != Configuration.ORIENTATION_PORTRAIT) {
			orientation = Configuration.ORIENTATION_PORTRAIT;
		}

		return (orientation == Configuration.ORIENTATION_LANDSCAPE);
	}


	@Override
	public void loginSDK() {
		AppController SanguoContext = (AppController) context;
	}

	@Override
	public void logout() {
		isLogin = false;
		AppController SanguoContext = (AppController) context;
	}


	@Override
	public void doSdkPay(final String params) {
		Log.d("zhancheng", params);
		final AppController SanguoContext = (AppController) context;
		SanguoContext.runOnUiThread(new Runnable() {
			
			@Override
			public void run() {
				
				// 将Lua传递过来的支付信息，按&截取成多个字段
				String[] param = params.split("&");
				
				String productID = param[0];// 商品id
				int money = Integer.parseInt(param[1]);// 商品价格
				String orderid = param[2];// 商户订单id
				String productName = param[3];// 商品名称
				String productDesc = param[4];// 商品描述
				String sdkUid = param[5];//SDK平台的用户ID
				String gameNickname = param[6];//游戏昵称
				try {
					gameNickname = URLDecoder.decode(gameNickname,"UTF-8");
				} catch (UnsupportedEncodingException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				String gameUid = param[7];//游戏uid
				String notify_uriString = "";
				try {
					notify_uriString = java.net.URLDecoder.decode(param[8], "utf-8");
					//Log.i("sdkpay", "notify_uriString = " + notify_uriString);
				} catch (UnsupportedEncodingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
		});
	}
	
	public void switchLanguage(final String params) {
		final AppController SanguoContext = (AppController) context;
		SanguoContext.runOnUiThread(new Runnable() {
			
			@Override
			public void run() {
				
				// 暂时支持 简体、繁体、英文
				Locale objLanguage = Locale.ENGLISH;
				if(params.equals("cn")) {
					objLanguage = Locale.SIMPLIFIED_CHINESE;
				} else if(params.equals("tw")) {
					objLanguage = Locale.TRADITIONAL_CHINESE;
				}
			}
		});
	}
	
	public void switchRegion(final String params) {
		final AppController SanguoContext = (AppController) context;
		SanguoContext.runOnUiThread(new Runnable() {
			
			@Override
			public void run() {
			}
		});
	}
	
	//uid,name,level,gender,serverid,servername,gold,vip,country
	public void submitExtInfo(String params) {
		String[] param = params.split(",");
		String roleName="";
		String serverName="";
		try {
			roleName = URLDecoder.decode(param[1], "utf-8");
			serverName = URLDecoder.decode(param[5], "utf-8");
		} catch (UnsupportedEncodingException e1) {
			e1.printStackTrace();
		}
		final JSONObject jsonExData = new JSONObject();
		try {
			jsonExData.put("appUId", param[0]);
			jsonExData.put("roleId", param[3]);
			jsonExData.put("roleName", roleName);
			jsonExData.put("roleLevel", param[2]);
			jsonExData.put("roleVip", param[7]);
			jsonExData.put("serverId", param[4]);
			jsonExData.put("serverName", serverName);
			
			
			final AppController SanguoContext = (AppController) context;
			SanguoContext.runOnUiThread(new Runnable() {

				@Override
				public void run() {
				}

			});
			
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		
	}

	@Override
	public void exitSDK() {
		final AppController SanguoContext = (AppController) context;
		SanguoContext.runOnUiThread(new Runnable() {

			@Override
			public void run() {
			}

		});
		
	}
	
	// 客服中心
	public void showCustomerServiceCenter() {
		final AppController SanguoContext = (AppController) context;
		SanguoContext.runOnUiThread(new Runnable() {

			@Override
			public void run() {
			}

		});
		
	}

	@Override
	public void switchAccount() {
		// TODO Auto-generated method stub
		
	}


}
