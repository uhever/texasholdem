/*
 * Copyright (C) 2010 The MobileSecurePay Project
 * All right reserved.
 * author: shiqun.shi@alipay.com
 * 
 *  提示：如何获取安全校验码和合作身份者id
 *  1.用您的签约支付宝账号登录支付宝网站(www.alipay.com)
 *  2.点击“商家服务”(https://b.alipay.com/order/myorder.htm)
 *  3.点击“查询合作者身份(pid)”、“查询安全校验码(key)”
 */

package org.cocos2dx.plugin;

//
// 请参考 Android平台安全支付服务(msp)应用开发接口(4.2 RSA算法签名)部分，并使用压缩包中的openssl RSA密钥生成工具，生成一套RSA公私钥。
// 这里签名时，只需要使用生成的RSA私钥。
// Note: 为安全起见，使用RSA私钥进行签名的操作过程，应该尽量放到商家服务器端去进行。
public final class Keys {

	// 合作身份者id，以2088开头的16位纯数字
	public static final String DEFAULT_PARTNER = "2088701005282595";

	// 收款支付宝账号
	public static final String DEFAULT_SELLER = "gumulieren@gmail.com";

	// 商户私钥，自助生成
	public static final String PRIVATE = "MIICXAIBAAKBgQDWxnF2QiloHNh72yIBa/pjZwl6Ku1J5YELUKiyvSJKoo3TIsXd"
			+ "2phYwBAtMrXYu/lR8yFjE7ZQTGg8xZN2B5znnK8fwcfQJd2iQMU9BEsmeopvDnLF"
			+ "6L3EjgrXIeyrxxxLWRD0yfrFL2YRhf0ATaBc8uvZOyThZ73yTRG6z/Pk/QIDAQAB"
			+ "AoGAdpSqIHj3E5yZigi7Bb3wSOGiwejhimNElFK8UbNLM5n8ZA5W4N7ILngcdlgB"
			+ "PuDKDw0F6gYAKuWEx8juWcRqRBOYeZlY6UJa1dErjd7IvYFZy6bS8EUEou5RpsPO"
			+ "DVBXs4R6THDwfAeYirDTBk/5STseTie6Pq45m8zObytrGQECQQDx6bElvmB99xTz"
			+ "IFSw72h1MuIz8ADRL10F17xst7Q7/1c/OZClTAZyyhz2zRyW9w0figdfLXXYX+OQ"
			+ "Xt7gjL/ZAkEA40gybmZasN73ZjLduZZK4epkCnXtnh2LkKg6wt6zhaTg4lUmJNFK"
			+ "0fi0tI8YnOlRRCG9cNITX+DYTKwYe897xQJAP6uTPWzi9qRudiMfR0ZrxtTyplrX"
			+ "zDbcE+qZJEAhht0IuJJNGMRuug2qG+cWTEGPyapJkGplpuJuAZSGsrE42QJBAKWR"
			+ "rJtcGOkMJHHg0EEoSTi2RVaRfh5XK5qf0t6u2VycNe7rVLQxey3m3c1DCQ0VssvN"
			+ "QHH1t4N7Clcgb12SUnkCQHjFkmgURBrANrvyNUIswA/nEDiFPtFz8MwmFC9o8jbF"
			+ "yc+YBuAO14uWXvexGtJ37M7jj83p2wLYCPveFr5t61U=";

	// 支付宝公钥
	public static final String PUBLIC = "hq8s4x8fue2fkg017au6sw3uwbud7o55";

}
