package org.cocos2dx.plugin;

import android.app.Activity;

public interface SDKHelper {

	public void initSDK();

	public void loginSDK();

	public void exitSDK();

	public void logout();

	public void switchAccount();

	public void doSdkPay(final String params);

}
