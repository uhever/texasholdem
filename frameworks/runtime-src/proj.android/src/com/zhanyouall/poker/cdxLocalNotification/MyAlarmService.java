package com.zhanyouall.poker.cdxLocalNotification;

import java.util.Calendar;
import java.util.Date;

import com.zhanyouall.poker.AppController;
import com.zhanyouall.poker.R;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.widget.Toast;

public class MyAlarmService extends Service {
	public static final String TITLE = "ALARM_TITLE";
	public static final String SUBTITLE = "ALARM_SUBTITLE";
	public static final String TICKER_TEXT = "ALARM_TICKER";
	public static final String NOTIFICATION_ID = "NOTIFICATION_ID";
	public static final String TIMESTAMP = "TIMESTAMP";
	/* Contains time in 24hour format 'HH:mm' e.g. '04:30' or '18:23' */
	public static final String HOUR_OF_DAY = "HOUR_OF_DAY";
	public static final String MINUTE = "MINUTES";
	public static String intentTest = "just";

	@Override
	public void onCreate() {
		// // TODO Auto-generated method stub
		// Toast.makeText(this, "MyAlarmService.onCreate()", Toast.LENGTH_LONG)
		// .show();
	}

	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		// Toast.makeText(this, "MyAlarmService.onBind()", Toast.LENGTH_LONG)
		// .show();
		return null;
	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub

	}

	public void showNotificaiton(Intent intent) {

		final Bundle bundle = intent.getExtras();

		// Retrieve notification details from the intent
		final String tickerText = bundle.getString(TICKER_TEXT);
		final String notificationTitle = bundle.getString(TITLE);
		final String notificationSubText = bundle.getString(SUBTITLE);
		final String notificationId = bundle.getString(NOTIFICATION_ID);
		final Long timestamp = bundle.getLong(TIMESTAMP);
		Calendar currentCal = Calendar.getInstance();
		System.out.println("当前事件 " + currentCal.getTimeInMillis());
		System.out.println("闹钟事件 " + timestamp.toString());
		// if (currentCal.getTimeInMillis()>timestamp){
		// return;
		// }
		// if (currentHour>alarmHour) {
		/*
		 * If you set a repeating alarm at 11:00 in the morning and it should
		 * trigger every morning at 08:00 o'clock, it will immediately fire.
		 * E.g. Android tries to make up for the 'forgotten' reminder for that
		 * day. Therefore we ignore the event if Android tries to 'catch up'.
		 */
		// Log.d("LocalNotification AlarmReceiver",
		// "AlarmReceiver, ignoring alarm since it is due");
		// return;
		// }
		final NotificationManager mNotificationManager = (NotificationManager) getApplication()
				.getSystemService(Context.NOTIFICATION_SERVICE);
		final Intent notifyIntent = new Intent(getApplication(), AppController.class);
		notifyIntent.addFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT
				| Intent.FLAG_ACTIVITY_SINGLE_TOP);
		final int id = Integer.parseInt(notificationId);
		PendingIntent contentIntent = PendingIntent.getActivity(
				getApplication(), id, notifyIntent,
				Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
		int icon = R.drawable.icon;

		CharSequence tickerText1 = tickerText;

		long when = System.currentTimeMillis();
		Date as = new Date(when);

		@SuppressWarnings("deprecation")
		Notification notification = new Notification(icon, tickerText1, when);
		notification.flags = Notification.FLAG_AUTO_CANCEL;
		notification.setLatestEventInfo(getApplication(), notificationTitle,
				notificationSubText, contentIntent);
		notification.sound = Uri.parse("android.resource://"
				+ getApplication().getPackageName() + "/"
				+ R.raw.notificationaudio);
		mNotificationManager.notify(id, notification);
	}

	@Override
	public void onStart(Intent intent, int startId) {
		// TODO Auto-generated method stub

		super.onStart(intent, startId);

	}

	@Override
	public boolean onUnbind(Intent intent) {
		// TODO Auto-generated method stub
		Toast.makeText(this, "MyAlarmService.onUnbind()", Toast.LENGTH_LONG)
				.show();
		return super.onUnbind(intent);
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		// TODO Auto-generated method stub
		boolean mStartCompatibility = getApplicationInfo().targetSdkVersion < Build.VERSION_CODES.ECLAIR;
		try {
			if (intent != null) {
				showNotificaiton(intent);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return mStartCompatibility ? START_STICKY_COMPATIBILITY : START_STICKY;

	}

}