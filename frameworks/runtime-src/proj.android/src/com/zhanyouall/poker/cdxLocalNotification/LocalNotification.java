package com.zhanyouall.poker.cdxLocalNotification;

import java.util.Calendar;

import org.json.JSONArray;
import org.json.JSONException;

import com.zhanyouall.poker.AppController;
import com.zhanyouall.poker.R;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Log;
import android.widget.Toast;

/**
 * This plugin utilizes the Android AlarmManager in combination with StatusBar
 * notifications. When a local notification is scheduled the alarm manager takes
 * care of firing the event. When the event is processed, a notification is put
 * in the Android status bar.
 * 
 * @author Daniel van 't Oever (original author)
 * 
 * @author Wang Zhuochun(https://github.com/zhuochun)
 */
public class LocalNotification {

	public static final String PLUGIN_NAME = "Sanguo";
	/**
	 * Delegate object that does the actual alarm registration. Is reused by the
	 * AlarmRestoreOnBoot class.
	 */
	private AlarmHelper alarm = null;
	private AppController context = null;

	public LocalNotification(Context context) {
		this.context = (AppController) context;
	}

	public boolean execute(String action, JSONArray args) throws JSONException {
		boolean success = false;

		if (action.equalsIgnoreCase("add")) {

			success = this.add(args.getString(0), args.getString(1),
					args.getString(2), args.getString(3), args.getJSONArray(4),
					args.getInt(5));
			persistAlarm(args.getString(0), args);
		} else if (action.equalsIgnoreCase("cancel")) {

			success = this.cancelNotification(args.getString(0));

		} else if (action.equalsIgnoreCase("cancelall")) {

			success = this.cancelAllNotifications();
			unpersistAlarmAll();
		}

		return success;
	}

	/**
	 * Set an alarm
	 */
	public boolean add(String id, String title, String subtitle, String ticker,
			JSONArray date, int ClockType) {
		Calendar calendar = Calendar.getInstance();

		alarm = new AlarmHelper(context);
		if (date.length() != 0) {
			try {
				calendar.set(date.getInt(0), date.getInt(1), date.getInt(2),
						date.getInt(3), date.getInt(4), date.getInt(5));

			} catch (JSONException e) {

			}
		}

		boolean result = alarm.addAlarm(ClockType, title, subtitle, ticker, id,
				calendar);

		return result;
	}

	public boolean cancelNotification(String id) {
		if (unpersistAlarm(id)) {
			alarm = new AlarmHelper(context);

			boolean result = alarm.cancelAlarm(id);

			return result;

		}
		// return false;
		else {

			return false;
		}
	}

	public boolean cancelAllNotifications() {

		final boolean result;
		alarm = new AlarmHelper(context);

		final SharedPreferences alarmSettings = context.getSharedPreferences(
				PLUGIN_NAME, Context.MODE_PRIVATE);

		result = alarm.cancelAll(alarmSettings);
		unpersistAlarmAll();
		return result;
	}

	/**
	 * Persist the information of this alarm to the Android Shared Preferences.
	 * This will allow the application to restore the alarm upon device reboot.
	 * Also this is used by the cancelAllNotifications method.
	 * 
	 * @see #cancelAllNotifications()
	 * 
	 * @param optionsArr
	 *            The assumption is that parseOptions has been called already.
	 * 
	 * @return true when successfull, otherwise false
	 */
	private boolean persistAlarm(String id, JSONArray args) {

		final Editor alarmSettingsEditor = context.getSharedPreferences(
				PLUGIN_NAME, Context.MODE_PRIVATE).edit();

		alarmSettingsEditor.putString(id, args.toString());

		return alarmSettingsEditor.commit();
	}

	/**
	 * Remove a specific alarm from the Android shared Preferences
	 * 
	 * @param alarmId
	 *            The Id of the notification that must be removed.
	 * 
	 * @return true when successfull, otherwise false
	 */
	private boolean unpersistAlarm(String id) {
		final SharedPreferences alarmSettings = context.getSharedPreferences(
				PLUGIN_NAME, Context.MODE_PRIVATE);
		final Editor alarmSettingsEditor = alarmSettings.edit();
		alarmSettingsEditor.remove(id);
		return alarmSettingsEditor.commit();

	}

	/**
	 * Clear all alarms from the Android shared Preferences
	 * 
	 * @return true when successfull, otherwise false
	 */
	private boolean unpersistAlarmAll() {

		final Editor alarmSettingsEditor = context.getSharedPreferences(
				PLUGIN_NAME, Context.MODE_PRIVATE).edit();

		alarmSettingsEditor.clear();

		return alarmSettingsEditor.commit();
	}
	
	
	public void addNotification(String notice){

		Log.e("java", "addNotification: " + notice);

		String id = "1";
		String text = context.getString(R.string.app_name);// 标题
		String ticker = "";// 提示（提醒时的标题）
		String subText = "text";// 内容
		String clockName = "daily";// 重复类型
		String triggerTime = "0";

		// noticeData.id .. "||" .. noticeData.date .. "||" .. noticeData.msg ..
		// "||" .. noticeData.myRepeat
		String[] Parameter = notice.split("=", 4);
		try {
			id = Parameter[0];
			triggerTime = Parameter[1];
			subText = Parameter[2];
			clockName = Parameter[3];
			ticker = Parameter[2];
		} catch (Exception e) {
			Log.e("error", "参数错误");
			id = "1";
			subText = "text";// 内容
			clockName = "daily";
			triggerTime = "0";

		}
		int Clocktype = 0;// 闹钟类型 0 一次性 1 每天 2 每周
		if (clockName.equalsIgnoreCase("daily")) {
			Clocktype = 1;

		} else if (clockName.equalsIgnoreCase("weekly")) {
			Clocktype = 2;
		}

		JSONArray a = new JSONArray();
		try {
			a.put(0, id);
			a.put(1, text);
			a.put(2, subText);
			a.put(3, ticker);
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		long myCaltime = 1;
		try {
			myCaltime = Long.parseLong(triggerTime);
		} catch (Exception e) {

			Calendar currentCal = Calendar.getInstance();
			myCaltime = currentCal.getTimeInMillis();
			Log.e("wrong", "时间格式错误,当前系统时间");
		}

		Calendar currentCal = Calendar.getInstance();
		if (myCaltime != 1) {
			currentCal.setTimeInMillis(myCaltime * 1000);
		}

		int currentYear = currentCal.get(Calendar.YEAR);
		int currentMonth = currentCal.get(Calendar.MONTH);
		int currentDay = currentCal.get(Calendar.DAY_OF_MONTH);
		int currentHour = currentCal.get(Calendar.HOUR_OF_DAY);
		int currentMin = currentCal.get(Calendar.MINUTE);
		int curretnSecond = currentCal.get(Calendar.SECOND);
		JSONArray dateJson = new JSONArray();
		try {
			dateJson.put(0, currentYear);
			dateJson.put(1, currentMonth);
			dateJson.put(2, currentDay);
			dateJson.put(3, currentHour);
			dateJson.put(4, currentMin);
			dateJson.put(5, curretnSecond);
			a.put(4, dateJson);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			a.put(5, Clocktype);
		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		try {
			Log.i("java", "闹钟真正提醒时间" + currentCal.toString());
			execute("add", a);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
	}
	
}
