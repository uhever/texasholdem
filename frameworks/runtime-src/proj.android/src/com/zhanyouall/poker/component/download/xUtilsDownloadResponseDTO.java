package com.zhanyouall.poker.component.download;



public class xUtilsDownloadResponseDTO {
	
	public static final int FLAG_START = 1000;
	public static final int FLAG_LOADING = 1001;
	public static final int FLAG_SUCCESS = 1002;
	public static final int FLAG_FAIL = 1003;
	public static final int FLAG_CANCEL = 1004;
	public static final int FLAG_STOP = 1005;
	
	private int flag;
	private double percent;
	private String msg;
	private String extra;
	private Object extraObj;
	
	public int getFlag() {
		return flag;
	}
	public void setFlag(int flag) {
		this.flag = flag;
	}
	public double getPercent() {
		return percent;
	}
	public void setPercent(double percent) {
		this.percent = percent;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getExtra() {
		return extra;
	}
	public void setExtra(String extra) {
		this.extra = extra;
	}
	public Object getExtraObj() {
		return extraObj;
	}
	public void setExtraObj(Object extraObj) {
		this.extraObj = extraObj;
	}
	
	
	

}
