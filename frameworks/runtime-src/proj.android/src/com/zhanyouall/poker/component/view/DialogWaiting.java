package com.zhanyouall.poker.component.view;




import java.util.Timer;
import java.util.TimerTask;

import com.zhanyouall.poker.AppController;
import com.zhanyouall.poker.R;

import android.app.Dialog;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;

/**
 * 转圈等待的Dialog
 * @author zctech
 *
 */
public class DialogWaiting extends Dialog {
	
    private int id; 
    private ImageView image;	
	private Animation animation;
	private Timer dialogTimer;
    private TimerTask dialogTimerTask;
    
    private DialogWaiting dialogWaiting;

    private AppController sActivity;

	public DialogWaiting(Context context, int id) {
		super(context, R.style.zc_dialog_waiting);// 创建自定义样式dialog  
		LayoutInflater inflater = LayoutInflater.from(context);  
        View v = inflater.inflate(R.layout.zc_dialog_waiting_contentview, null);// 得到加载view  
        RelativeLayout layout = (RelativeLayout) v.findViewById(R.id.zc_dialog_waiting_view);// 加载布局  
        // main.xml中的ImageView  
        image = (ImageView) v.findViewById(R.id.zc_dialog_waiting_img);  
//        TextView tipTextView = (TextView) v.findViewById(R.id.progress_msg);// 提示文字
//        tipTextView.setText(msg);// 设置加载信息
        
        // 加载动画  
        animation = AnimationUtils.loadAnimation(context, R.anim.zc_dialog_waiting_animation);  
        // 使用ImageView显示动画  
//        spaceshipImage.startAnimation(hyperspaceJumpAnimation); 
        	  
   
        this.setCancelable(false);// 不可以用“返回键”取消  
        this.setContentView(layout, new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.FILL_PARENT, RelativeLayout.LayoutParams.FILL_PARENT));// 设置布局  
        dialogWaiting = this;
        this.id = id;
        sActivity = (AppController)context;
        Log.e("zhancheng", "onCreateDialog DialogWaiting");
	}
	
	@Override
	public void show() {
		// TODO Auto-generated method stub
		super.show();
		
		//60s后自动关闭
		if(dialogTimer ==null)
			dialogTimer = new Timer();
		dialogTimerTask = new TimerTask() {				
				@Override
				public void run() {
					Log.e("zhancheng", "auto dismiss dialog");
//					dialogWaiting.dismiss();
					sActivity.handler.sendEmptyMessage(sActivity.DIALOG_WAITING);//由Activity自己管理关闭
				}
			};
		dialogTimer.schedule(dialogTimerTask, 30000);

	}
	
	@Override
	public void dismiss() {
		// TODO Auto-generated method stub
		super.dismiss();
		
		//处理计时器
		if(dialogTimer!=null){
			if(dialogTimerTask!=null)
			Log.e("zhancheng", "dialogTimerTask.cancel()");
			dialogTimerTask.cancel();//关闭计时器
			dialogTimerTask=null;
		}
	}
	

	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}


	public ImageView getImage() {
		return image;
	}

	public void setImage(ImageView image) {
		this.image = image;
	}

	public Animation getAnimation() {
		return animation;
	}

	public void setAnimation(Animation animation) {
		this.animation = animation;
	}

	


}
