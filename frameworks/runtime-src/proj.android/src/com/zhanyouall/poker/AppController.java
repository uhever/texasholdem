/****************************************************************************
Copyright (c) 2010-2012 cocos2d-x.org

http://www.cocos2d-x.org

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
 ****************************************************************************/
package com.zhanyouall.poker;

import java.io.File;
import java.io.FileInputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import org.cocos2dx.lib.*;
import org.cocos2dx.plugin.UserSDK;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.NotificationManager;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.AssetFileDescriptor;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;

import android.view.ViewGroup;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;

import android.telephony.TelephonyManager;
import android.util.Log;

import android.view.WindowManager;
import android.widget.Toast;


import com.zhanyouall.poker.cdxLocalNotification.LocalNotification;
import com.zhanyouall.poker.component.download.XZip;
import com.zhanyouall.poker.component.download.xUtilsDownloadManager;
import com.zhanyouall.poker.component.download.xUtilsDownloadResponseDTO;
import com.zhanyouall.poker.component.download.xUtilsDownloadManager.IDownloadManagerFeedback;
import com.zhanyouall.poker.component.view.ActivityWebView;
import com.zhanyouall.poker.component.view.DialogDownload;
import com.zhanyouall.poker.component.view.DialogUnzip;
import com.zhanyouall.poker.component.view.DialogWaiting;
import com.zhanyouall.poker.component.view.VideoView;
import com.zhanyouall.poker.BuildConfig;
import com.zhanyouall.poker.R;

public class AppController extends Cocos2dxActivity {

	// 播放视频相关
	boolean cur_surfaceCreated = false;
	public int cur_posttion;
	AssetFileDescriptor cur_fd;
	int curCallBack = 0;
	String fileString;
	// 数据统计
	public static boolean isNetworkOk = false;
	public static AppController sActivity = null;
	static UserSDK userSDKHelper = null;
	public static LocalNotification LocalNotificationHelper = null;
	private static MyApi myApiHelper = null;

	
	// reyun
	boolean reyunAvtIsActive = true;

	public static final int DIALOG_WAITING = 1000;
	public static final int DIALOG_WAITING_CLOSE = 1001; 
	private static DialogWaiting dialogWaiting;
	
	public static final int DIALOG_CONFIRM = 2000;
	public static final int DIALOG_CONFIRM_CLOSE = 2001;
	public static final int DIALOG_CONFIRM_TYPE_EXIT = 2002;
	public static final int DIALOG_CONFIRM_TYPE_REDOWNLOAD = 2003;
	public static final int DIALOG_CONFIRM_TYPE_REUNZIPPROCCESS = 2004;
	public static int dialogConfirmType = -1;
	private static AlertDialog dialogConFirm;
	
	public static final int DIALOG_NETWORK = 2500;
	public static final int DIALOG_NETWORK_CLOSE = 2501;
	private static AlertDialog dialogNetwork;
	
	public static final int DIALOG_DOWNLOAD = 3000;
	public static final int DIALOG_DOWNLOAD_UPDATE = 3001;
	public static final int DIALOG_DOWNLOAD_CLOSE = 3002;
	private static DialogDownload dialogDownload;
	
	public static final int DIALOG_UNZIP = 4000;
	public static final int DIALOG_UNZIP_CLOSE = 4002;
	private static DialogUnzip dialogUnzip;	
	
	public static final int LUA_GOTOWEBVIEW = 7000;
	
	public static final int UNZIP_PROCCESS_RESULT = 8000; 
	public static final int UNZIP_PROCCESS_RETRY = 8001;
	
	private static final int UMENG_PUSH_API = 9000;
	private static final int GET_DEVICE_INFO = 92;
	public static SharedPreferences sp;
	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		sActivity = this;
		
		ApplicationInfo appInfo = null;
		try {
			 appInfo = this.getPackageManager()
					.getApplicationInfo(getPackageName(),
							PackageManager.GET_META_DATA);
		} catch (NameNotFoundException e1) {
			e1.printStackTrace();
		}

		try {
			Class.forName("android.os.AsyncTask");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}

		getWindow().setFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON,
				WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		
		String appChannelName = appInfo.metaData.getString("UMENG_CHANNEL");
		LocalNotificationHelper = new LocalNotification(this);
		myApiHelper = new MyApi(this);
		userSDKHelper = new UserSDK(this);
		
		
		sp = PreferenceManager.getDefaultSharedPreferences(this);

		dialogWaiting = (DialogWaiting)onCreateDialog(DIALOG_WAITING, null);
		
		dialogConFirm = (AlertDialog)onCreateDialog(DIALOG_CONFIRM, null);
		
		dialogNetwork = (AlertDialog)onCreateDialog(DIALOG_NETWORK, null);
		
		dialogDownload = (DialogDownload)onCreateDialog(DIALOG_DOWNLOAD, null);
		
		dialogUnzip = (DialogUnzip)onCreateDialog(DIALOG_UNZIP, null);
		

	}
	

    /**
     * 显示对话框
     */
    public void showDialogNow(final int dialogType, final Bundle bd){
    	Log.e("zhancheng", "showDialogNow begin:"+dialogType);
    	try {
    		//如果有确认框先关闭
			sActivity.runOnUiThread(new Runnable() {				
				@Override
				public void run() {
					switch(dialogType){
					case DIALOG_WAITING:
						if(dialogWaiting!=null&&!dialogWaiting.isShowing()){
							sActivity.showDialog(DIALOG_WAITING, null);
						}
						break;
					case DIALOG_CONFIRM:
						if(dialogConFirm!=null&&!dialogConFirm.isShowing()){
							sActivity.showDialog(DIALOG_CONFIRM, bd);
						}					
					break;
					
					case DIALOG_NETWORK:
						if(dialogNetwork!=null&&!dialogNetwork.isShowing()){
							sActivity.showDialog(DIALOG_NETWORK, bd);
						}					
					break;					
						
					case DIALOG_DOWNLOAD:
						if(dialogDownload!=null&&!dialogDownload.isShowing()){
							sActivity.showDialog(DIALOG_DOWNLOAD, null);
						}			
						break;
					case DIALOG_UNZIP:
						if(dialogUnzip!=null&&!dialogUnzip.isShowing()){
							sActivity.showDialog(DIALOG_UNZIP, null);
						}			
						break;	
					default:
						Log.e("zhancheng", "showDialogNow dialogType error");
						break;
					}
										
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
			Log.e("zhancheng", "showDialogNow fatal error");
		}
    }
    @Override
   	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
   		
   		switch (requestCode) {
		case LUA_GOTOWEBVIEW:
			myApiHelper.doCallbackByName("WebViewCallBack", resultCode+"");  
			break;
		case DIALOG_NETWORK:
			userSDKHelper.initSDK();
			break;
		default:
			super.onActivityResult(requestCode, resultCode, data);
			break;
		}
   	}
    /**
     * 关闭对话框
     */
    public void dismissDialogNow(int dialogType){
    	Log.e("zhancheng", "dismissDialogNow:"+dialogType);
    	switch(dialogType){
		case DIALOG_WAITING:
			handler.sendEmptyMessage(DIALOG_WAITING_CLOSE);
			break;
		case DIALOG_CONFIRM:
			handler.sendEmptyMessage(DIALOG_CONFIRM_CLOSE);	
			break;
		case DIALOG_NETWORK:
			handler.sendEmptyMessage(DIALOG_NETWORK_CLOSE);
			break;
		case DIALOG_DOWNLOAD:
			handler.sendEmptyMessage(DIALOG_DOWNLOAD_CLOSE);					
			break;
		case DIALOG_UNZIP:
			handler.sendEmptyMessage(DIALOG_UNZIP_CLOSE);
			break;
		default:
			Log.e("zhancheng", "showDialogNow dialogType error");
			break;
		}
    	
    	
    }
    
    private static void dialogDownloadUpdate(int percent){
    	Message msg = new Message();
    	Bundle bd = new Bundle();
    	bd.putInt("percent", percent);
    	msg.setData(bd);
    	msg.what = DIALOG_DOWNLOAD_UPDATE;
    	handler.sendMessage(msg);
    }
    
    
    public static Handler handler = new Handler(){
    	public void handleMessage(Message msg) {
    		Log.e("zhancheng", "handleMessage msg.what:"+msg.what);
    		switch (msg.what) {
    		case GET_DEVICE_INFO:
    			final String result = msg.getData().getString("data");
    			final int callbackId = msg.getData().getInt("callback");
//    			Toast.makeText(sActivity, result.substring(result.lastIndexOf("=")+1), Toast.LENGTH_SHORT).show();
    			sActivity.runOnGLThread(new Runnable() {

    				@Override
    				public void run() {
    					Cocos2dxLuaJavaBridge.callLuaFunctionWithString(callbackId,
    							result);
    					// 当回调不再使用时，需要释放lua function
    					Cocos2dxLuaJavaBridge.releaseLuaFunction(callbackId);
    				}

    			});
    			break;
			case DIALOG_WAITING_CLOSE:
				//关闭等待框
				Log.e("zhancheng", "handleMessage dismissDialog DIALOG_WAITING");

				if(dialogWaiting!=null&&dialogWaiting.isShowing()){
					sActivity.dismissDialog(DIALOG_WAITING);

				}
				break;
				
			case UMENG_PUSH_API:
				//推送api
				String tagString = (String) msg.obj;

				if (tagString.indexOf(",") != -1) {
					String[] tag = tagString.split(",");
					for (int i = 0; i < tag.length; i++) {
						try {
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}

				} else {
					try {
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				
				break;
			case DIALOG_CONFIRM_CLOSE:
				Log.e("zhancheng", "handleMessage dismissDialog DIALOG_CONFIRM_CLOSE");
				if(dialogConFirm!=null&&dialogConFirm.isShowing()){
					sActivity.dismissDialog(DIALOG_CONFIRM);
				}				
				break;
				
			case DIALOG_NETWORK_CLOSE:
				Log.e("zhancheng", "handleMessage dismissDialog DIALOG_NETWORK_CLOSE");
				if(dialogConFirm!=null&&dialogConFirm.isShowing()){
					sActivity.dismissDialog(DIALOG_NETWORK);
				}
				
				break;
			case DIALOG_DOWNLOAD_CLOSE:
				//下载框关闭
				Log.e("zhancheng", "handleMessage dismissDialog DIALOG_DOWNLOAD");

				if(dialogDownload!=null&&dialogDownload.isShowing()){
					sActivity.dismissDialog(DIALOG_DOWNLOAD);

				}
				
				break;
				
			case DIALOG_DOWNLOAD_UPDATE:
				//下载框更新
				
				if(dialogDownload!=null&&dialogDownload.isShowing()){
					int per = msg.getData().getInt("percent");
					dialogDownload.getProgressBar().setProgress(per);
				}
				
				break;
			case DIALOG_UNZIP_CLOSE:
				Log.e("zhancheng", "handleMessage dismissDialog DIALOG_UNZIP_CLOSE");

				if(dialogUnzip!=null&&dialogUnzip.isShowing()){
					sActivity.dismissDialog(DIALOG_UNZIP);
				}
				
				break;
			case UNZIP_PROCCESS_RESULT:
				if(msg.getData().getBoolean("isOk")){
	    			sp.edit().putInt("downScripts_state", 3).commit();
	    			sActivity.dismissDialogNow(DIALOG_UNZIP);
	    			Toast.makeText(sActivity, R.string.unzip_success, Toast.LENGTH_LONG).show();
	    			downloadExtraResources("", "");
				}else{
					
					dialogUnzip.getBtCtrl().setEnabled(true);
					dialogUnzip.getBtReset().setEnabled(true);
				}
				break;
				
			case UNZIP_PROCCESS_RETRY:
				//关闭当前解压对话框
				sActivity.dismissDialogNow(DIALOG_UNZIP);
				//0.1秒后重试
				handler.postDelayed(new Runnable() {
					
					@Override
					public void run() {
						
						String saveFileName = sp.getString("downloadScriptsInfo_fileName", "error");
						String saveUrl = sp.getString("downloadScriptsInfo_url", "error");
						
						//为了让下载器走startup方法
						sp.edit().putString("downloadScriptsInfo_fileName", "error").commit();
						sp.edit().putString("downloadScriptsInfo_url", "error").commit();
						dialogUnzip.getTv().setText(R.string.unziping);
						downloadExtraResources(saveUrl, saveFileName);
						
					}
				}, 100);
				break;
			default:
				break;
			}
    	};
    };
    

    @Override
	@Deprecated
	protected void onPrepareDialog(int id, Dialog dialog, Bundle args) {
		// TODO Auto-generated method stub
		Log.e("zhancheng", "onPrepareDialog:"+ id);
		
		switch(id){
		case DIALOG_WAITING:
			Log.e("zhancheng", "onPrepareDialog DIALOG_WAITING");
			if(dialogWaiting!=null&&dialogWaiting.getAnimation()!=null&&dialogWaiting.getImage()!=null){
				dialogWaiting.getImage().startAnimation(dialogWaiting.getAnimation());
			}
			break;
		case DIALOG_CONFIRM:
			if(args!=null&&args.getString("msg")!=null){
				dialogConFirm.setMessage(args.getString("msg"));
			}
			break;
		case DIALOG_UNZIP:
			dialogUnzip.getTv().setText(R.string.unziping);
			break;
		default:
			super.onPrepareDialog(id, dialog, args);	
			break;

		}

	}
    
    @Override
	@Deprecated
	protected Dialog onCreateDialog(int id, Bundle args) {
    	Log.e("zhancheng", "onCreateDialog");

		try {	
			switch(id){
			case DIALOG_WAITING:
				if(dialogWaiting==null)
					return new DialogWaiting(this, DIALOG_WAITING);
				else
					return dialogWaiting;

			case DIALOG_CONFIRM:
				if(dialogConFirm==null){
				AlertDialog.Builder builder = new Builder(this); 
		        builder.setMessage("notice");
		        builder.setPositiveButton(R.string.confirm, 
		                new android.content.DialogInterface.OnClickListener() { 
		                    @Override
		                    public void onClick(DialogInterface dialog, int which) {
		                    	Log.e("zhancheng", "dialogConFirm onConfirm:"+dialogConfirmType);
		                    	switch(dialogConfirmType){
		                    	case DIALOG_CONFIRM_TYPE_EXIT: 
		                    		System.exit(0);
		                    		break;
		                    	case DIALOG_CONFIRM_TYPE_REDOWNLOAD:
		                    		dialogDownloadUpdate(0);
									xUtilsDownloadManager.getInstance(sActivity).startupDownload();
									Log.e("zhancheng", "暂停－>继续");
									dialogDownload.setIsStop(false);
									dialogDownload.getBtCtrl().setText(R.string.pause);
									dialogDownload.getBtCtrl().setEnabled(true);
		                    		break;
		                    	case DIALOG_CONFIRM_TYPE_REUNZIPPROCCESS:
									sp.edit().putInt("downScripts_state", -1).commit();																	
									dialogUnzip.getBtCtrl().setEnabled(false);
									dialogUnzip.getBtReset().setEnabled(false);
									handler.sendEmptyMessage(UNZIP_PROCCESS_RETRY);
		                    		break;
		                    	default:
		                    		
		                    		break;
		                    	}
		                    	
		                    }
						
		                }); 
		        builder.setNegativeButton(R.string.cancel, 
		                new android.content.DialogInterface.OnClickListener() { 
		                    @Override
		                    public void onClick(DialogInterface dialog, int which) { 
		                        dialog.dismiss(); 
		                    } 
		                }); 
		        Log.e("zhancheng", "onCreateDialog DIALOG_CONFIRM dialogExit");

				return builder.create();
				}else{
					return dialogConFirm;
				}
				
			case DIALOG_NETWORK:
				if(dialogNetwork==null){
					AlertDialog.Builder builder = new Builder(this); 
			        builder.setMessage(R.string.network_unconnected);
			        builder.setPositiveButton(R.string.network_retry, 
			                new android.content.DialogInterface.OnClickListener() { 
			                    @Override
			                    public void onClick(DialogInterface dialog, int which) {
			                    	Log.e("zhancheng", "dialogNetwork onConfirm:");
			                    	
			                    	handler.postDelayed(new Runnable() {
										
										@Override
										public void run() {
											userSDKHelper.initSDK();
										}
									}, 100);
			                    	sActivity.dismissDialogNow(DIALOG_NETWORK);
			                    	
			                    }
							
			                }); 
			        builder.setNegativeButton(R.string.set_network, 
			                new android.content.DialogInterface.OnClickListener() { 
			                    @Override
			                    public void onClick(DialogInterface dialog, int which) { 
									Intent wifiSettingsIntent = new Intent("android.settings.WIFI_SETTINGS");
									sActivity.startActivityForResult(wifiSettingsIntent, DIALOG_NETWORK);
			                        
			                    } 
			                }); 
			        builder.setCancelable(false);
			        Log.e("zhancheng", "onCreateDialog DIALOG_NETWORK dialogExit");

					return builder.create();
					}else{
						return dialogNetwork;
					}
				
			case DIALOG_DOWNLOAD:
				if(dialogDownload==null){
					dialogDownload = new DialogDownload(this, DIALOG_DOWNLOAD);
					dialogDownload.getProgressBar().setMax(100);
					dialogDownload.getBtCtrl().setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View arg0) {
							// TODO Auto-generated method stub
							if(dialogDownload.getIsStop()){
								Log.e("zhancheng", "暂停－>继续");
								xUtilsDownloadManager.getInstance(sActivity).continueDownload();
								dialogDownload.setIsStop(false);
								dialogDownload.getBtCtrl().setText(R.string.pause);			

							}else{
								Log.e("zhancheng", "继续－>暂停");
								xUtilsDownloadManager.getInstance(sActivity).stopDownload();
								dialogDownload.setIsStop(true);
								dialogDownload.getBtCtrl().setText(R.string.go_on);	
							}

						}
					});
					dialogDownload.getBtReset().setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View arg0) {
							
							Bundle bd = new Bundle();
							bd.putString("msg", sActivity.getResources().getString(R.string.reset_download));
							dialogConfirmType = DIALOG_CONFIRM_TYPE_REDOWNLOAD;
							sActivity.showDialogNow(DIALOG_CONFIRM, bd);
							
							
						}
					});
					return dialogDownload;
				}else{
					return dialogDownload;
				}
				
			case DIALOG_UNZIP:
				if(dialogUnzip==null){
					dialogUnzip = new DialogUnzip(this, DIALOG_UNZIP);  
					dialogUnzip.getTv().setText(R.string.unziping);
					dialogUnzip.getBtCtrl().setText(R.string.network_retry);
					dialogUnzip.getBtCtrl().setEnabled(false);
					dialogUnzip.getBtCtrl().setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View arg0) {
														
							dialogUnzip.getBtCtrl().setEnabled(false);
							dialogUnzip.getBtReset().setEnabled(false);
							handler.sendEmptyMessage(UNZIP_PROCCESS_RETRY);
						}
					});
					dialogUnzip.getBtReset().setText(R.string.download_and_unzip_again);
					dialogUnzip.getBtReset().setEnabled(false);
					dialogUnzip.getBtReset().setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View arg0) {
							
							Bundle bd = new Bundle();
							bd.putString("msg", sActivity.getResources().getString(R.string.download_and_unzip_again_hint));
							dialogConfirmType = DIALOG_CONFIRM_TYPE_REUNZIPPROCCESS;
							sActivity.showDialogNow(DIALOG_CONFIRM, bd);

						}
					});
					return dialogUnzip;
				
				}else{
					return dialogUnzip;
				}				
				
			default:
				Log.e("zhancheng", "onCreateDialog id 有误！");
				return super.onCreateDialog(id, null);
			}
			
				
		} catch (Exception e) {
			e.printStackTrace();
			Log.e("zhancheng", "onCreateDialog 生成dialog发生异常！");
			return null;
		}
		
		
	}
    
    
    /**
     * 下载新版本
     * 调用范例：downNewVersion("http://cdn.bdwsw.zhanchenggame.com/package/bdwsw1.1.1.apk", "1.1.1");
     * 使用下载器单例下载，提供一个下载对话框
     * @param url 下载链接
     * @param version 版本号
     */
    public static void downNewVersion(String version, String url){
    	
    	Log.e("zhancheng", "downNewVersion start");

    	sActivity.showDialogNow(DIALOG_DOWNLOAD, null);
    	sActivity.dismissDialogNow(DIALOG_CONFIRM);
    	dialogDownload.setIsStop(false);
    	xUtilsDownloadManager.getInstance(sActivity).downloadNewApk(
    			url,
    			version, 
    			new IDownloadManagerFeedback() {
			
			@Override
			public void feedback(xUtilsDownloadResponseDTO dto) {
				// TODO Auto-generated method stub
				switch(dto.getFlag()){
				case xUtilsDownloadResponseDTO.FLAG_START:
					dialogDownload.getTv().setText(R.string.prepare_download);
					break;
				case xUtilsDownloadResponseDTO.FLAG_LOADING:
					int per = (int)dto.getPercent();
					dialogDownload.getTv().setText(sActivity.getResources().getString(R.string.downloading)+
													sActivity.getResources().getString(R.string.degree_of_completion)+
													per+sActivity.getResources().getString(R.string.persent));
					dialogDownloadUpdate(per);
					break;
				case xUtilsDownloadResponseDTO.FLAG_SUCCESS:
					dialogDownloadUpdate(100);
					dialogDownload.getTv().setText(R.string.has_completed);
					dialogDownload.getBtCtrl().setEnabled(false);
					
					Intent intent = new Intent();    
			        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);    
			        intent.setAction(android.content.Intent.ACTION_VIEW);    
			        
			        Uri uri = Uri.fromFile(new File(dto.getExtra()));  
			        intent.setDataAndType(uri,"application/vnd.android.package-archive"); 
			        sActivity.startActivity(intent);

					break;
				case xUtilsDownloadResponseDTO.FLAG_FAIL:
					dialogDownload.getTv().setText(R.string.download_fail);
					dialogDownload.getBtCtrl().setText(R.string.network_retry);
					dialogDownload.setIsStop(true);
					break;
				case xUtilsDownloadResponseDTO.FLAG_STOP:
					dialogDownload.getTv().setText(R.string.download_pause);
					dialogDownload.setIsStop(true);
					dialogDownload.getBtCtrl().setText(R.string.go_on);
					break;
				case xUtilsDownloadResponseDTO.FLAG_CANCEL:
					break;
					
					
				}
			}
		});

    }
    
    
    /**
     * 提供给lua层的游戏资源重新下载接口
     */
    public static void downloadExtraResourcesRetry(){
    	sp.edit().putInt("downScripts_state", -1).commit();	
    	unzipRetry();
    }
    
    /**
     * 提供给lua层的下载重试接口
     */
    public static void downloadRetry(){
    	xUtilsDownloadManager.getInstance(sActivity).setInDownloadScriptsTask(true);
    	xUtilsDownloadManager.getInstance(sActivity).continueDownload();
    }
    
    /**
     * 提供给lua层的解压重试接口
     */
    public static void unzipRetry(){
    	String saveFileName = sp.getString("downloadScriptsInfo_fileName", "error");
    	String saveUrl = sp.getString("downloadScriptsInfo_url", "error");

    	//为了让下载器走startup方法
    	sp.edit().putString("downloadScriptsInfo_fileName", "error").commit();
    	sp.edit().putString("downloadScriptsInfo_url", "error").commit();
//    	dialogUnzip.getTv().setText("解壓中，請耐心等待。。。");
    	downloadExtraResources(saveUrl, saveFileName);
    }
    
    
    /**
     * 下载并将资源解压到目录
     * 调用范例：downScripts("http://cdn.bdwsw.zhanchenggame.com/package/scripts.zip", "download1.1.1");
     * @param url 下载路径
     * @param fileName 解压目标目录（只用写包名路径下的即可）
     */
    public static void downloadExtraResources(final String url, final String fileName){
    	
    	xUtilsDownloadManager.getInstance(sActivity).setInDownloadScriptsTask(true);
    	
    	Log.e("zhancheng", "downloadExtraResources start");
    	
    	int state = sp.getInt("downScripts_state", -1);
    	Log.e("zhancheng", "downScripts state:"+state);
    	   	
    	    	
    	switch(state){
    	case 3:
    		//已经完成，可以直接进入游戏
    		Log.e("zhancheng", "downScripts 3:进入游戏");
    		myApiHelper.doCallbackByName("zipSuccess", "");
    		break;
    		
    	case 2:
    		
    		myApiHelper.doCallbackByName("downloadExtraResourcesCallback", "unzipProccess,99");
    		
    		//下载完成，解压未完成   		
	
    		//拉起解压对话框
    		
//    		sActivity.showDialogNow(DIALOG_UNZIP, null);
//    		sActivity.dismissDialogNow(DIALOG_CONFIRM);
    		
    		String src = sp.getString("downScripts_src", "error");
    		String target = sp.getString("downloadScriptsInfo_fileName", "error");
    		
    		File f = new File(src);
    		if("error".equals(target)||"error".equals(src)||!f.exists()){
    			
    			myApiHelper.doCallbackByName("downloadExtraResourcesCallback", "unzipFail,99");
    			
//    			Log.e("zhancheng", "downScripts 文件丢失，请重新下载!");
//    			dialogUnzip.getTv().setText("文件丟失，請重新下載!");
//    			dialogUnzip.getBtReset().setEnabled(true);
	        	sp.edit().putString("downloadScriptsInfo_url", url).commit();
	        	sp.edit().putString("downloadScriptsInfo_fileName", fileName).commit();

    			return;
    		}
    		//解压目录
    		target = sActivity.getFilesDir()+"/"+target;
    		
    		//如果存在（上次解压失败留下来的）则删除存在文件夹
    		f = new File(target);
    		if(f.exists())
    			XZip.deleteFileAndFolder(f);
    		
    		//重新跑解压流程
    		  		
    		
    		final String src1 = src;
			final String target1 = target;
    		
			new Thread(new Runnable() {
				
				@Override
				public void run() {
					
					Message msg = new Message();
					msg.what = UNZIP_PROCCESS_RESULT;
					Bundle bd = new Bundle();
					bd.putBoolean("isOk", true);
					boolean isOk = true;
					try {
						XZip.UnZipFolder(src1, target1);
						Log.e("zhancheng", "downScripts 解压成功");
					} catch (Exception e) {
						bd.putBoolean("isOk", false);
						Log.e("zhancheng", "downScripts 解压出错");
						isOk = false;
					}finally{
						//无论如何都要有结果
//						msg.setData(bd);
//						handler.sendMessage(msg);
						
						if(isOk){
							sp.edit().putInt("downScripts_state", 3).commit();
			    			downloadExtraResources("", "");
						}else{
							sp.edit().putString("downloadScriptsInfo_url", url).commit();
				        	sp.edit().putString("downloadScriptsInfo_fileName", fileName).commit();
							myApiHelper.doCallbackByName("downloadExtraResourcesCallback", "unzipFail,99");							
						}
					}
					
				}
			}).start();

    		
    		break;
    	default:
    		
    		//未下载或下载未完成或重置
//    		sActivity.showDialogNow(DIALOG_DOWNLOAD, null);
    		sActivity.dismissDialogNow(DIALOG_CONFIRM);
    		
        	dialogDownload.setIsStop(false);
        	
    		xUtilsDownloadManager.getInstance(sActivity).downloadScripts(
    				url,
    				fileName,
    				new IDownloadManagerFeedback() {
				
				@Override
				public void feedback(xUtilsDownloadResponseDTO dto) {
					switch(dto.getFlag()){
					case xUtilsDownloadResponseDTO.FLAG_START:
						
						myApiHelper.doCallbackByName("downloadExtraResourcesCallback", "downloadStart,0");
												
//						dialogDownload.getTv().setText("下載資源准備中。。。");
						break;
					case xUtilsDownloadResponseDTO.FLAG_LOADING:
						int per = (int)dto.getPercent();
//						dialogDownload.getTv().setText("下載資源中。。。完成度:"+per+"%");
//						dialogDownloadUpdate(per);
						
						myApiHelper.doCallbackByName("downloadExtraResourcesCallback", "downloadLoading,"+per);
						
						break;
					case xUtilsDownloadResponseDTO.FLAG_SUCCESS:
//						dialogDownloadUpdate(100);
//						dialogDownload.getTv().setText("已完成");
//						dialogDownload.getBtCtrl().setEnabled(false);
//						
//						sActivity.dismissDialogNow(DIALOG_DOWNLOAD);
						myApiHelper.doCallbackByName("downloadExtraResourcesCallback", "downloadSuccess,99");
						sp.edit().putInt("downScripts_state", 2).commit();
						sp.edit().putString("downScripts_src", dto.getExtra()).commit();
						
						
						String saveFileName2 = sp.getString("downloadScriptsInfo_fileName", "error");
				    	String saveUrl2 = sp.getString("downloadScriptsInfo_url", "error");
				    	downloadExtraResources(saveUrl2, saveFileName2);
						
						break;
					case xUtilsDownloadResponseDTO.FLAG_FAIL:
						xUtilsDownloadManager.getInstance(sActivity).setInDownloadScriptsTask(false);
						myApiHelper.doCallbackByName("downloadExtraResourcesCallback", "downloadFail,99");
//						dialogDownload.getTv().setText("下載資源失敗，請重試。。。");
//						dialogDownload.getBtCtrl().setText("重試");
//						dialogDownload.setIsStop(true);
						break;
					case xUtilsDownloadResponseDTO.FLAG_STOP:
//						xUtilsDownloadManager.getInstance(sActivity).setInDownloadScriptsTask(false);
//						myApiHelper.doCallbackByName("downloadExtraResourcesCallback", "downloadFail,99");
//						dialogDownload.getTv().setText("下載資源暫停中。。。");
//						dialogDownload.setIsStop(true);
//						dialogDownload.getBtCtrl().setText("繼續");
						break;
					case xUtilsDownloadResponseDTO.FLAG_CANCEL:
						break;
						
						
					}
				}
			});
    		
    		
    		break;
    	
    	}
    }


	// 添加推送的tag api
	public static void setPushTag(String tags) {

		Message msg = new Message();
		msg.what = UMENG_PUSH_API;
		msg.obj = tags;
		handler.sendMessage(msg);

	}
	
	public static void gotoWebView(String url, int callBack){
		myApiHelper.registerCallBackByName("WebViewCallBack", callBack);
		Intent in = new Intent(sActivity, ActivityWebView.class);
		Log.e("zhancheng", url);
		in.putExtra("url", url);
		sActivity.startActivityForResult(in, LUA_GOTOWEBVIEW);	
	}


	public Cocos2dxGLSurfaceView onCreateView() {
		Cocos2dxGLSurfaceView glSurfaceView = new Cocos2dxGLSurfaceView(this);

		glSurfaceView.setEGLConfigChooser(8, 8, 8, 8, 16, 8);
		return glSurfaceView;
	}

	public static Object getInstance() {

		return sActivity;
	}

	// 注册Lua回调
	public static void RegisterCallBackByName(String name, int functionID) {

		myApiHelper.registerCallBackByName(name, functionID);

	}

	// 删除Lua回调
	public static void RemoveCallBackByName(String name) {

		myApiHelper.removeCallBackByName(name);

	}

	// 执行已注册的Lua回调
	public void doCallbackByName(String name, String params) {

		myApiHelper.doCallbackByName(name, params);

	}

	// 打开浏览器 url为链接地址 需要加http
	public static void openUrl(String url) {

		Uri content_uri = Uri.parse(url); // url为你要链接的地址
		Intent intent = new Intent(Intent.ACTION_VIEW, content_uri);
		sActivity.startActivity(intent);

	}

	// 获取设备信息
	public static void getDeviceInfo(final int callbackId) {

		try {

			final String macStr = getMacID();
			final String model = Cocos2dxHelper.getDeviceModel();
			final String androidId = Settings.Secure.getString(
					sActivity.getContentResolver(), Settings.Secure.ANDROID_ID);
			final String currentapiVersion = android.os.Build.VERSION.RELEASE;
			final WifiManager wifiManager = (WifiManager) sActivity
					.getSystemService(WIFI_SERVICE);
			String ssid = wifiManager.getConnectionInfo().getSSID();
			if (ssid == null)
				ssid = "";
			else {
				ssid = ssid.replaceAll("\"", "");
				ssid = ssid.replaceAll("=", "_");
			}
			final String ssidFinal = ssid;
			String luaStr = macStr + "=" + model + "=" + androidId + "=" + currentapiVersion + "=" + ssidFinal;
			
			//TODO 获取advertestingID
//			sActivity.runOnGLThread(new Runnable() {
//
//				@Override
//				public void run() {
//					Cocos2dxLuaJavaBridge.callLuaFunctionWithString(callbackId,
//							macStr + "=" + model + "=" + androidId + "="
//									+ currentapiVersion + "=" + ssidFinal);
//					// 当回调不再使用时，需要释放lua function
//					Cocos2dxLuaJavaBridge.releaseLuaFunction(callbackId);
//				}
//
//			});

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public static String getNetworkType(Context context){
		String type = "other";
		ConnectivityManager connectManager = (ConnectivityManager) context
		        .getSystemService(Context.CONNECTIVITY_SERVICE);
		 
		 NetworkInfo info = connectManager.getActiveNetworkInfo();
		 if(info!=null && info.isConnected()){
			 if(info.getType() == ConnectivityManager.TYPE_WIFI){
				 type = "wifi";
			 }
			 else if(info.getType() == ConnectivityManager.TYPE_MOBILE){
				 switch (info.getSubtype()) {
				 case TelephonyManager.NETWORK_TYPE_GPRS:
				 case TelephonyManager.NETWORK_TYPE_EDGE:
				 case TelephonyManager.NETWORK_TYPE_CDMA:
				 case TelephonyManager.NETWORK_TYPE_1xRTT:
				 case TelephonyManager.NETWORK_TYPE_IDEN:
					 type = "2G";
					 break;

				 case TelephonyManager.NETWORK_TYPE_UMTS:
				 case TelephonyManager.NETWORK_TYPE_EVDO_0:
				 case TelephonyManager.NETWORK_TYPE_EVDO_A:
				 case TelephonyManager.NETWORK_TYPE_HSDPA:
				 case TelephonyManager.NETWORK_TYPE_HSUPA:
				 case TelephonyManager.NETWORK_TYPE_HSPA:
				 case TelephonyManager.NETWORK_TYPE_EVDO_B:
				 case TelephonyManager.NETWORK_TYPE_EHRPD:
				 case TelephonyManager.NETWORK_TYPE_HSPAP:
					 type = "3G";
					 break;

				 case TelephonyManager.NETWORK_TYPE_LTE:
					 type = "4G";
					 break;
				}
			 }
		 }
		 return type;
	}
	public static String getMacID() {
		String Mac = "";

		WifiManager wifi = (WifiManager) sActivity
				.getSystemService(Context.WIFI_SERVICE);
		WifiInfo info = wifi.getConnectionInfo();
		Mac = info.getMacAddress();

		if (Mac == null || Mac.length() == 0) {
			Log.e("java", "获取android mac地址失败");
			try {

				String path = "sys/class/net/wlan0/address";
				if ((new File(path)).exists()) {
					FileInputStream fis = new FileInputStream(path);
					byte[] buffer = new byte[8192];
					int byteCount = fis.read(buffer);
					if (byteCount > 0) {
						Mac = new String(buffer, 0, byteCount, "utf-8");
					}
				}
				Log.v("mac***wifi**mac11**", "" + Mac);
				if (Mac == null || Mac.length() == 0) {
					path = "sys/class/net/eth0/address";
					FileInputStream fis_name = new FileInputStream(path);
					byte[] buffer_name = new byte[8192];
					int byteCount_name = fis_name.read(buffer_name);
					if (byteCount_name > 0) {
						Mac = new String(buffer_name, 0, byteCount_name,
								"utf-8");
					}
				}
				Log.v("mac***eth0**mac11**", "" + Mac);

				if (Mac.length() == 0 || Mac == null) {
					return "";
				}
			} catch (Exception io) {
				Log.v("mac**exception*", "" + io.toString());
				Log.i("solo", "1111111111111");
				return "00:00:00:00:00:00";
			}
		}
		return Mac;

	}

	// 取消所有本地通知
	public static void cancelAllNotifications() {

		LocalNotificationHelper.cancelAllNotifications();
	}

	// 取消指定的本地通知
	public static void cancelNotification(String id) {

		LocalNotificationHelper.cancelNotification(id);
	}

	public static void cancelNotificaionTops() {
		final NotificationManager mNotificationManager = (NotificationManager) sActivity
				.getSystemService(Context.NOTIFICATION_SERVICE);
		mNotificationManager.cancelAll();
	}

	// 添加一个本地通知
	public static void addNotification(String notice) {
		LocalNotificationHelper.addNotification(notice);
	}

	public void onResume() {
		super.onResume();
		if (curVideoView != null && curVideoView.mMediaPlayer != null) {
			if (curVideoView.surfaceCreated) {
				curVideoView.mMediaPlayer.seekTo(cur_posttion);
				curVideoView.mMediaPlayer.start();
			}
		}
		if(xUtilsDownloadManager.getInstance(this).isInDownloadScriptsTask()){
			downloadRetry();
		}
		
		
		

		if( !reyunAvtIsActive){ // app 从后台唤醒到前台
			reyunAvtIsActive =true;
		}
	}
	public void onPause() {
		super.onPause();
		Log.e("zhancheng", "onPause");
		if (curVideoView != null && curVideoView.mMediaPlayer != null) {
			cur_posttion = curVideoView.mMediaPlayer.getCurrentPosition();
			curVideoView.mMediaPlayer.pause();
		}
		if(xUtilsDownloadManager.getInstance(this).isInDownloadScriptsTask()){
			xUtilsDownloadManager.getInstance(this).quitManager();
		}

		

	}
	// 播放视频
	public static void playVideo(final String name, final int luaCallback) {
		Log.d("zhancheng", "playVideo");

		sActivity.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				// VideoView videoView = new VideoView(sActivity,name);
				//
				// ViewGroup group = (ViewGroup) sActivity.getWindow()
				// .getDecorView();
				// group.addView(videoView);
				// videoView.setZOrderMediaOverlay(true);
				curVideoView = new VideoView(sActivity, name);
				curVideoView.setLuaOnFinishCallback(luaCallback);
				// 设置显示视频View
				ViewGroup group = (ViewGroup) sActivity.getWindow()
						.getDecorView();
				group.addView(curVideoView);
				curVideoView.setZOrderMediaOverlay(true);
				// 播放视频
				curVideoView.start();

			}
		});

	}

	private static VideoView curVideoView;
	private static boolean isOpenTestin;
	public static String shareId;

	public static void stopVideo() {
		Log.d("zhancheng", "stopVideo");
		if (curVideoView.mMediaPlayer == null)
			return;
		curVideoView.stopPlayback();

		final AppController instance = AppController.sActivity;
		instance.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				ViewGroup group = (ViewGroup) instance.getWindow()
						.getDecorView();
				group.removeView(curVideoView);
			}
		});

	}

	public static void initSDK(final int luaCallback) {
		RegisterCallBackByName("initSDK", luaCallback);
        userSDKHelper.initSDK();
	}

	// 登录SDK平台
	public static void loginSDK() {
		userSDKHelper.loginSDK();
	}

	// 登录SDK平台(原本提供给luabinding的，已废弃)
	public void openUC(String a) {

		userSDKHelper.loginSDK();

	}
	

	// 退出SDK平台(给返回键用的)
	public static void exitSDK() {
		
//		Bundle bd = new Bundle();
//		bd.putString("msg", "您確認要退出嗎？");
//		dialogConfirmType = DIALOG_CONFIRM_TYPE_EXIT;
//		sActivity.showDialogNow(DIALOG_CONFIRM, bd);
		
//		sActivity.runOnUiThread(new Runnable() {
//			
//			@Override
//			public void run() {
//				AlertDialog.Builder builder = new Builder(sActivity); 
//		        builder.setIcon(R.drawable.icon);  
//		        builder.setTitle(R.string.app_name); 
//		        builder.setMessage("您確認要退出嗎？");
//		        builder.setPositiveButton("確定", 
//		                new android.content.DialogInterface.OnClickListener() { 
//		                    @Override
//		                    public void onClick(DialogInterface dialog, int which) {
//		                    	System.exit(0);
//		                    }
//						
//		                }); 
//		        builder.setNegativeButton("取消", 
//		                new android.content.DialogInterface.OnClickListener() { 
//		                    @Override
//		                    public void onClick(DialogInterface dialog, int which) { 
//		                        dialog.dismiss(); 
//		                    } 
//		                }); 
//		        builder.create().show();
//			}
//		});
		userSDKHelper.exitSDK();
		
	}

	// 登出账号(360没有退出逻辑，直接通知lua)
	public static void logout() {
//		myApiHelper.doCallbackByName("logoutResult", "");
		sActivity.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				userSDKHelper.logout();
			}
		});	
	}

	// 切换账号(暂未使用)
	public static void switchAccount() {
		userSDKHelper.switchAccount();
	}

	// 调用支付
	public static void doSdkPay(final String params) {
		userSDKHelper.doSdkPay(params);
	}

	// 切换语言
	public static void switchLanguage(final String params) {
		userSDKHelper.switchLanguage(params);
	}
	
	// 切换地区
	public static void switchRegion(final String params) {
		userSDKHelper.switchRegion(params);
	}

	// 进入客服中心
	public static void showCustomerServiceCenter() {
		userSDKHelper.showCustomerServiceCenter();
	}
	public static void share(final String param){
		sActivity.runOnUiThread(new Runnable() {
			
			@Override
			public void run() {
				String[] params = param.split(",");
				shareId = params[0];
				String shareType = params[1];
				String shareContentType = params[2];
				String source = params[3];
//				source = "http://www.wsdejx.com/";
//				source = sActivity.getFilesDir().getAbsolutePath() + "/weixin.jpg";
				if("wx".equals(shareType)){
					if("image".equals(shareContentType)){
					}else if("text".equals(shareContentType)){
					}
				}else if("fb".equals(shareType)){
					if("link".equals(shareContentType)){
					}
				}
			}
		});
			
		
	}
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub

		if (!curVideoView.mIsPlaying) {
			super.onBackPressed();
		} else {

		}
	}

	public static void submitUserInfo(String params){
		userSDKHelper.submitExtInfo(params);
	}	

	public static void sendDataEvent(String params) {
		Log.d("zhancheng", "onEvent: " + params);
		String[] param = params.split(",");

		String event_id = param[0];// 事件id
		String uid = param[1];// 用户id

		if (event_id.equals("Login")) {


		} 
		else if (event_id.equals("Register")) {


		} 
		else if (event_id.equals("Purchase")) {

			String productMoeny = param[3];// 商品价格
			String orderid = param[2];// 商户订单id
			
			Map<String, String> map_value = new HashMap<String, String>();
			map_value.put("amount", productMoeny);
			
		}

	}
	
	// 切换屏幕
	public static void landscapAction() {
		sActivity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);	
	}
	
	public static void portraitAction() {
		sActivity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);	
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		xUtilsDownloadManager.getInstance(this).quitManager();

		super.onDestroy();
		
	}




	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		
			//app 进入后台
			reyunAvtIsActive = false;
		}



	public static String getFreeSpace() {

		String freeSpaceString = ((double) (GameConfig.getAbleSize(sActivity
				.getApplicationContext().getFilesDir().getAbsolutePath())) / 1024 / 1024)
				+ "";
		Log.e("zhancheng", "freeSpaceString ======= " + freeSpaceString);

		return freeSpaceString;
		// sActivity.doCallbackByName("getFreeSpace", freeSpaceString);

	}

	public static void getSystemTime() {
		String dateString = MyDate.getDate();
		Log.e("zhancheng", "系统时间" + "\n" + dateString);

		sActivity.doCallbackByName("getSystemTime", dateString);

	}

	public static String getAndroidLabelSize(String labelString,
			String fontname, String fontsize) {
		// typedef
		// enum{kCCTextAlignmentLeft,kCCTextAlignmentCenter,kCCTextAlignmentRight,}
		// kCCTextAlignmentCenter默认
		Bitmap bitmap = MyBitmap.createTextBitmap(labelString, fontname,
				Integer.parseInt(fontsize), 1, 0, 0);

		String sizeString = bitmap.getWidth() + "," + bitmap.getHeight();
		
		bitmap.recycle();
		bitmap = null;
		
//		Log.e("zhancheng", labelString + "\n" + sizeString);
		return sizeString;

	}

	public static void isAvilible(String packageName) {
		final PackageManager packageManager = sActivity.getPackageManager();// 获取packagemanager
		List<PackageInfo> pinfo = packageManager.getInstalledPackages(0);// 获取所有已安装程序的包信息
		List<String> pName = new ArrayList<String>();// 用于存储所有已安装程序的包名
		// 从pinfo中将包名字逐一取出，压入pName list中
		if (pinfo != null) {
			for (int i = 0; i < pinfo.size(); i++) {
				String pn = pinfo.get(i).packageName;
				pName.add(pn);
			}
		}
		String[] param = packageName.split("&");

		String isHaveString = "false";

		for (int i = 0; i < param.length; i++) {
			if (pName.contains(param[i])) {

				isHaveString = "true";
				break;
			}
		}
		sActivity.doCallbackByName("isAvilible", isHaveString);

	}


	public String encodeGB(String string) {
		// 转换中文编码
		String split[] = string.split("/");
		for (int i = 1; i < split.length; i++) {
			try {
				split[i] = URLEncoder.encode(split[i], "GB2312");
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
			split[0] = split[0] + "/" + split[i];
		}
		split[0] = split[0].replaceAll("\\+", "%20");// 处理空格
		return split[0];
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub
		
		if(keyCode == KeyEvent.KEYCODE_BACK){
			exitSDK();
			
		    return true;
		}		
		return super.onKeyDown(keyCode, event);
	}
	

	static {
//		System.loadLibrary("game");
//		System.loadLibrary("cocos2dlua");
	}

}

class LuaGLSurfaceView extends Cocos2dxGLSurfaceView {

	public LuaGLSurfaceView(Context context) {
		super(context);
	}

	public boolean onKeyDown(int keyCode, KeyEvent event) {
		
		return super.onKeyDown(keyCode, event);
	}
}
