
--------------------------------
-- @module TableViewCell
-- @extend Widget
-- @parent_module ccui

--------------------------------
-- Cleans up any resources linked to this cell and resets <code>idx</code> property.
-- @function [parent=#TableViewCell] reset 
-- @param self
-- @return TableViewCell#TableViewCell self (return value: ccui.TableViewCell)
        
--------------------------------
-- The index used internally by SWTableView and its subclasses
-- @function [parent=#TableViewCell] getIdx 
-- @param self
-- @return int#int ret (return value: int)
        
--------------------------------
-- 
-- @function [parent=#TableViewCell] setIdx 
-- @param self
-- @param #int uIdx
-- @return TableViewCell#TableViewCell self (return value: ccui.TableViewCell)
        
--------------------------------
-- 
-- @function [parent=#TableViewCell] create 
-- @param self
-- @return TableViewCell#TableViewCell ret (return value: ccui.TableViewCell)
        
--------------------------------
-- 
-- @function [parent=#TableViewCell] TableViewCell 
-- @param self
-- @return TableViewCell#TableViewCell self (return value: ccui.TableViewCell)
        
return nil
