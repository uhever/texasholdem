
--------------------------------
-- @module ZCTool
-- @extend Ref
-- @parent_module 

--------------------------------
-- 
-- @function [parent=#ZCTool] calcStrSize 
-- @param self
-- @param #char str
-- @param #char font
-- @param #char fontSize
-- @return char#char ret (return value: char)
        
--------------------------------
-- 
-- @function [parent=#ZCTool] captureScreen 
-- @param self
-- @param #cc.Node node
-- @param #char textureName
-- @return ZCTool#ZCTool self (return value: ZCTool)
        
--------------------------------
-- 
-- @function [parent=#ZCTool] getGaussianTexture 
-- @param self
-- @param #cc.Node node
-- @return Texture2D#Texture2D ret (return value: cc.Texture2D)
        
--------------------------------
-- 
-- @function [parent=#ZCTool] createGLScissorNode 
-- @param self
-- @param #float x
-- @param #float y
-- @return Node#Node ret (return value: cc.Node)
        
--------------------------------
-- 
-- @function [parent=#ZCTool] destroyInstance 
-- @param self
-- @return ZCTool#ZCTool self (return value: ZCTool)
        
--------------------------------
-- 
-- @function [parent=#ZCTool] getInstance 
-- @param self
-- @return ZCTool#ZCTool ret (return value: ZCTool)
        
--------------------------------
-- 
-- @function [parent=#ZCTool] ZCTool 
-- @param self
-- @return ZCTool#ZCTool self (return value: ZCTool)
        
return nil
