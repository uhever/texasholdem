
--------------------------------
-- @module TableView
-- @extend ScrollView
-- @parent_module ccui

--------------------------------
-- 
-- @function [parent=#TableView] handleMoveLogic 
-- @param self
-- @param #cc.Touch pTouch
-- @return TableView#TableView self (return value: ccui.TableView)
        
--------------------------------
-- determines how cell is ordered and filled in the view.
-- @function [parent=#TableView] setVerticalFillOrder 
-- @param self
-- @param #int order
-- @return TableView#TableView self (return value: ccui.TableView)
        
--------------------------------
-- Removes a cell at a given index<br>
-- param idx index to find a cell
-- @function [parent=#TableView] removeCellAtIndex 
-- @param self
-- @param #int idx
-- @return TableView#TableView self (return value: ccui.TableView)
        
--------------------------------
-- 
-- @function [parent=#TableView] handlePressLogic 
-- @param self
-- @param #cc.Touch pTouch
-- @return TableView#TableView self (return value: ccui.TableView)
        
--------------------------------
-- Updates the content of the cell at a given index.<br>
-- param idx index to find a cell
-- @function [parent=#TableView] updateCellAtIndex 
-- @param self
-- @param #int idx
-- @return TableView#TableView self (return value: ccui.TableView)
        
--------------------------------
-- 
-- @function [parent=#TableView] initWithViewSize 
-- @param self
-- @param #size_table size
-- @param #cc.Node container
-- @return bool#bool ret (return value: bool)
        
--------------------------------
-- 
-- @function [parent=#TableView] getScriptHandler 
-- @param self
-- @param #int nScriptEventType
-- @return int#int ret (return value: int)
        
--------------------------------
-- 
-- @function [parent=#TableView] moveInnerContainer 
-- @param self
-- @param #vec2_table deltaMove
-- @param #bool canStartBounceBack
-- @return TableView#TableView self (return value: ccui.TableView)
        
--------------------------------
-- reloads data from data source.  the view will be refreshed.
-- @function [parent=#TableView] reloadData 
-- @param self
-- @return TableView#TableView self (return value: ccui.TableView)
        
--------------------------------
-- Dequeues a free cell if available. nil if not.<br>
-- return free cell
-- @function [parent=#TableView] dequeueCell 
-- @param self
-- @return TableViewCell#TableViewCell ret (return value: ccui.TableViewCell)
        
--------------------------------
-- 
-- @function [parent=#TableView] getVerticalFillOrder 
-- @param self
-- @return int#int ret (return value: int)
        
--------------------------------
-- 
-- @function [parent=#TableView] handleReleaseLogic 
-- @param self
-- @param #cc.Touch pTouch
-- @return TableView#TableView self (return value: ccui.TableView)
        
--------------------------------
-- Inserts a new cell at a given index<br>
-- param idx location to insert
-- @function [parent=#TableView] insertCellAtIndex 
-- @param self
-- @param #int idx
-- @return TableView#TableView self (return value: ccui.TableView)
        
--------------------------------
-- Returns an existing cell at a given index. Returns nil if a cell is nonexistent at the moment of query.<br>
-- param idx index<br>
-- return a cell at a given index
-- @function [parent=#TableView] cellAtIndex 
-- @param self
-- @param #int idx
-- @return TableViewCell#TableViewCell ret (return value: ccui.TableViewCell)
        
--------------------------------
-- 
-- @function [parent=#TableView] _updateContentSize 
-- @param self
-- @return TableView#TableView self (return value: ccui.TableView)
        
--------------------------------
-- 
-- @function [parent=#TableView] unregisterAllScriptHandler 
-- @param self
-- @return TableView#TableView self (return value: ccui.TableView)
        
--------------------------------
-- 
-- @function [parent=#TableView] reloadLayout 
-- @param self
-- @return TableView#TableView self (return value: ccui.TableView)
        
--------------------------------
-- 
-- @function [parent=#TableView] moveToCell 
-- @param self
-- @param #unsigned int idx
-- @param #unsigned int width
-- @return TableView#TableView self (return value: ccui.TableView)
        
--------------------------------
-- js ctor<br>
-- lua new
-- @function [parent=#TableView] TableView 
-- @param self
-- @return TableView#TableView self (return value: ccui.TableView)
        
return nil
