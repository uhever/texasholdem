
--------------------------------
-- @module GotyeTool
-- @extend Ref,GotyeDelegate
-- @parent_module 

--------------------------------
-- 
-- @function [parent=#GotyeTool] leaveRoom 
-- @param self
-- @param #int roomId
-- @return GotyeTool#GotyeTool self (return value: GotyeTool)
        
--------------------------------
-- 
-- @function [parent=#GotyeTool] unregisterEventHandler 
-- @param self
-- @return GotyeTool#GotyeTool self (return value: GotyeTool)
        
--------------------------------
-- 
-- @function [parent=#GotyeTool] stopTalk 
-- @param self
-- @return GotyeTool#GotyeTool self (return value: GotyeTool)
        
--------------------------------
-- 
-- @function [parent=#GotyeTool] getTalkingPower 
-- @param self
-- @return int#int ret (return value: int)
        
--------------------------------
-- 
-- @function [parent=#GotyeTool] talkToUser 
-- @param self
-- @param #char username
-- @param #unsigned int whineMode
-- @param #bool realtime
-- @param #unsigned int maxDuration
-- @return GotyeTool#GotyeTool self (return value: GotyeTool)
        
--------------------------------
-- 
-- @function [parent=#GotyeTool] talkToGroup 
-- @param self
-- @param #int groupId
-- @param #unsigned int whineMode
-- @param #bool realtime
-- @param #unsigned int maxDuration
-- @return GotyeTool#GotyeTool self (return value: GotyeTool)
        
--------------------------------
-- 
-- @function [parent=#GotyeTool] joinGroup 
-- @param self
-- @param #int groupId
-- @return GotyeTool#GotyeTool self (return value: GotyeTool)
        
--------------------------------
-- 
-- @function [parent=#GotyeTool] isInRoom 
-- @param self
-- @param #int roomId
-- @return bool#bool ret (return value: bool)
        
--------------------------------
-- 
-- @function [parent=#GotyeTool] init 
-- @param self
-- @return GotyeTool#GotyeTool self (return value: GotyeTool)
        
--------------------------------
-- 
-- @function [parent=#GotyeTool] exit 
-- @param self
-- @return GotyeTool#GotyeTool self (return value: GotyeTool)
        
--------------------------------
-- 
-- @function [parent=#GotyeTool] isOnline 
-- @param self
-- @return bool#bool ret (return value: bool)
        
--------------------------------
-- 
-- @function [parent=#GotyeTool] enterRoom 
-- @param self
-- @param #int roomId
-- @return GotyeTool#GotyeTool self (return value: GotyeTool)
        
--------------------------------
-- 
-- @function [parent=#GotyeTool] cancelTalk 
-- @param self
-- @return GotyeTool#GotyeTool self (return value: GotyeTool)
        
--------------------------------
-- 
-- @function [parent=#GotyeTool] update 
-- @param self
-- @param #float dt
-- @return GotyeTool#GotyeTool self (return value: GotyeTool)
        
--------------------------------
-- 
-- @function [parent=#GotyeTool] dismissGroup 
-- @param self
-- @param #int groupId
-- @return GotyeTool#GotyeTool self (return value: GotyeTool)
        
--------------------------------
-- 
-- @function [parent=#GotyeTool] logout 
-- @param self
-- @return GotyeTool#GotyeTool self (return value: GotyeTool)
        
--------------------------------
-- 
-- @function [parent=#GotyeTool] talkToRoom 
-- @param self
-- @param #int roomId
-- @param #unsigned int whineMode
-- @param #bool realtime
-- @param #unsigned int maxDuration
-- @return GotyeTool#GotyeTool self (return value: GotyeTool)
        
--------------------------------
-- 
-- @function [parent=#GotyeTool] getAutoPlayAudio 
-- @param self
-- @return bool#bool ret (return value: bool)
        
--------------------------------
-- 
-- @function [parent=#GotyeTool] isRoomSupportRealtime 
-- @param self
-- @param #int roomId
-- @return bool#bool ret (return value: bool)
        
--------------------------------
-- 
-- @function [parent=#GotyeTool] setAutoPlayAudio 
-- @param self
-- @param #bool isAutoPlayAudio
-- @return GotyeTool#GotyeTool self (return value: GotyeTool)
        
--------------------------------
-- 
-- @function [parent=#GotyeTool] createGroup 
-- @param self
-- @param #char name
-- @param #char info
-- @param #bool isPublic
-- @param #bool needAuthentication
-- @return GotyeTool#GotyeTool self (return value: GotyeTool)
        
--------------------------------
-- 
-- @function [parent=#GotyeTool] login 
-- @param self
-- @param #char username
-- @return GotyeTool#GotyeTool self (return value: GotyeTool)
        
--------------------------------
-- 
-- @function [parent=#GotyeTool] destroyInstance 
-- @param self
-- @return GotyeTool#GotyeTool self (return value: GotyeTool)
        
--------------------------------
-- 
-- @function [parent=#GotyeTool] getInstance 
-- @param self
-- @return GotyeTool#GotyeTool ret (return value: GotyeTool)
        
--------------------------------
-- 
-- @function [parent=#GotyeTool] GotyeTool 
-- @param self
-- @return GotyeTool#GotyeTool self (return value: GotyeTool)
        
return nil
