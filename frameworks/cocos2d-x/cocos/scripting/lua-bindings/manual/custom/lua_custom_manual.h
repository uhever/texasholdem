//
//  lua_custom_manual.hpp
//  cocos2d_lua_bindings
//
//  Created by UHEVER on 16/10/30.
//
//

#ifndef COCOS2DX_SCRIPT_LUA_COCOS2DX_SUPPORT_LUA_CUSTOM_MANUAL_H
#define COCOS2DX_SCRIPT_LUA_COCOS2DX_SUPPORT_LUA_CUSTOM_MANUAL_H

#ifdef __cplusplus
extern "C" {
#endif
#include "tolua++.h"
#ifdef __cplusplus
}
#endif

/**
 * @addtogroup lua
 * @{
 */

/**
 * Call this function can import the lua bindings for the custom module.
 * If you don't want to use the custom module in the lua, you only don't call this registering function.
 * If you don't register the custom module, the package size would become smaller .
 * The current mechanism,this registering function is called in the lua_module_register.h
 */
TOLUA_API int register_custom_module(lua_State* L);

// end group
/// @}

#endif /* COCOS2DX_SCRIPT_LUA_COCOS2DX_SUPPORT_LUA_CUSTOM_MANUAL_H */
