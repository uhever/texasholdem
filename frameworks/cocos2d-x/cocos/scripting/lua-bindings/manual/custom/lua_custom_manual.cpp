//
//  lua_custom_manual.cpp
//  cocos2d_lua_bindings
//
//  Created by UHEVER on 16/10/30.
//
//

#include "scripting/lua-bindings/manual/custom/lua_custom_manual.h"
extern "C" {
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS || CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID || CC_TARGET_PLATFORM == CC_PLATFORM_WIN32 || CC_TARGET_PLATFORM == CC_PLATFORM_MAC)
#include "scripting/lua-bindings/manual/custom/lua_custom_extension.h"
#endif
}

int register_custom_module(lua_State* L)
{
    lua_getglobal(L, "_G");
    if (lua_istable(L,-1))//stack:...,_G,
    {
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS || CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID || CC_TARGET_PLATFORM == CC_PLATFORM_WIN32 || CC_TARGET_PLATFORM == CC_PLATFORM_MAC)
        luaopen_lua_custom_extensions(L);
#endif
        
    }
    lua_pop(L, 1);
    
    return 1;
}
