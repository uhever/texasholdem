//
//  lua_custom_extension.h
//  cocos2d_lua_bindings
//
//  Created by UHEVER on 16/10/30.
//
//

#ifndef LUA_CUSTOM_EXTENSION_H
#define LUA_CUSTOM_EXTENSION_H

#if defined(_USRDLL)
#define LUA_EXTENSIONS_DLL     __declspec(dllexport)
#else         /* use a DLL library */
#define LUA_EXTENSIONS_DLL
#endif

#if __cplusplus
extern "C" {
#endif
    
#include "lauxlib.h"
    
    /// @cond
    void LUA_EXTENSIONS_DLL luaopen_lua_custom_extensions(lua_State *L);
    /// @endcond
    
#if __cplusplus
}
#endif

#endif /* LUA_CUSTOM_EXTENSION_H */
