//
//  lua_custom_extension.c
//  cocos2d_lua_bindings
//
//  Created by UHEVER on 16/10/30.
//
//

#include "scripting/lua-bindings/manual/custom/lua_custom_extension.h"

#if __cplusplus
extern "C" {
#endif
    // lfs
#include "filesystem/lfs.h"
    // cjson
#include "cjson/lua_cjson.h"
    // base64
#include "base64/lbase64.c"
    // md5
#include "md5/md5lib.c"
    // lpack
#include "lpack/lpack.h"
    // zlib
#include "zlib/lua_zlib.h"
    
    static luaL_Reg luax_custom_exts[] = {
        {"lfs", luaopen_lfs},
        {"cjson", luaopen_cjson},
        {"base64", luaopen_base64},
        {"md5.core", luaopen_md5_core},
        {"pack", luaopen_pack},
        {"zlib", luaopen_zlib},
        {NULL, NULL}
    };
    
    void luaopen_lua_custom_extensions(lua_State *L)
    {
        // load extensions
        luaL_Reg* lib = luax_custom_exts;
        lua_getglobal(L, "package");
        lua_getfield(L, -1, "preload");
        for (; lib->func; lib++)
        {
            lua_pushcfunction(L, lib->func);
            lua_setfield(L, -2, lib->name);
        }
        lua_pop(L, 2);
        
    }
    
#if __cplusplus
} // extern "C"
#endif
