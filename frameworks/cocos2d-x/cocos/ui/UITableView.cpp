/****************************************************************************
 Copyright (c) 2012 cocos2d-x.org
 Copyright (c) 2010 Sangwoo Im
 
 http://www.cocos2d-x.org
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 ****************************************************************************/

#include "ui/UITableView.h"
#include "ui/UITableViewCell.h"
#include "ui/UIScrollView.h"



NS_CC_BEGIN

namespace ui {
    
    
    TableView* TableView::create()
    {
        return TableView::create(nullptr, Size::ZERO);
    }
    
    TableView* TableView::create(TableViewDataSource* dataSource, Size size)
    {
        return TableView::create(dataSource, size, nullptr);
    }
    
    TableView* TableView::create(TableViewDataSource* dataSource, Size size, Node *container)
    {
        TableView *table = new (std::nothrow) TableView();
        table->initWithViewSize(size, container);
        table->autorelease();
        table->setDataSource(dataSource);
        table->_updateCellPositions();
        table->_updateContentSize();
        
        return table;
    }
    
    bool TableView::initWithViewSize(Size size, Node* container/* = nullptr*/)
    {
        if (ScrollView::init()) // reg check
        {
            CC_SAFE_DELETE(_indices);
            _indices        = new (std::nothrow) std::set<ssize_t>();
            _vordering      = VerticalFillOrder::BOTTOM_UP;
            this->setDirection(Direction::VERTICAL);
            
            //        ScrollView::setDelegate(this); // reg check
            
            this->setContentSize(size);
            
            return true;
        }
        return false;
    }
    
    TableView::TableView()
    : _touchedCell(nullptr)
    , _indices(nullptr)
    , _dataSource(nullptr)
    , _tableViewDelegate(nullptr)
    , _oldDirection(Direction::NONE)
    , _isUsedCellsDirty(false)
    {
        setBounceEnabled(true);
    }
    
    TableView::~TableView()
    {
        CC_SAFE_DELETE(_indices);
    }
    
    void TableView::setVerticalFillOrder(VerticalFillOrder fillOrder)
    {
        if (_vordering != fillOrder)
        {
            _vordering = fillOrder;
            if (!_cellsUsed.empty())
            {
                this->reloadData();
            }
        }
    }
    
    TableView::VerticalFillOrder TableView::getVerticalFillOrder()
    {
        return _vordering;
    }
    
    void TableView::reloadData()
    {
        _oldDirection = Direction::NONE;
        
        for(const auto &cell : _cellsUsed) {
            if(_tableViewDelegate != nullptr) {
                _tableViewDelegate->tableCellWillRecycle(this, cell);
            }
            
            _cellsFreed.pushBack(cell);
            
            cell->reset();
            if (cell->getParent() == this->getInnerContainer()) // reg check
            {
                this->getInnerContainer()->removeChild(cell, true); // reg check
            }
        }
        
        _indices->clear();
        _cellsUsed.clear();
        
        this->_updateCellPositions();
        this->_updateContentSize();
        if (_dataSource->numberOfCellsInTableView(this) > 0)
        {
            //        this->scrollViewDidScroll(this); // reg check
            this->reloadLayout();
            
        }
    }
    
    TableViewCell *TableView::cellAtIndex(ssize_t idx)
    {
        if (_indices->find(idx) != _indices->end())
        {
            for (const auto& cell : _cellsUsed)
            {
                if (cell->getIdx() == idx)
                {
                    return cell;
                }
            }
        }
        
        return nullptr;
    }
    
    void TableView::updateCellAtIndex(ssize_t idx)
    {
        if (idx == CC_INVALID_INDEX)
        {
            return;
        }
        long countOfItems = _dataSource->numberOfCellsInTableView(this);
        if (0 == countOfItems || idx > countOfItems-1)
        {
            return;
        }
        
        TableViewCell* cell = this->cellAtIndex(idx);
        if (cell)
        {
            this->_moveCellOutOfSight(cell);
        }
        cell = _dataSource->tableCellAtIndex(this, idx);
        this->_setIndexForCell(idx, cell);
        this->_addCellIfNecessary(cell);
    }
    
    void TableView::insertCellAtIndex(ssize_t idx)
    {
        if (idx == CC_INVALID_INDEX)
        {
            return;
        }
        
        long countOfItems = _dataSource->numberOfCellsInTableView(this);
        if (0 == countOfItems || idx > countOfItems-1)
        {
            return;
        }
        
        long newIdx = 0;
        
        auto cell = cellAtIndex(idx);
        if (cell)
        {
            newIdx = _cellsUsed.getIndex(cell);
            // Move all cells behind the inserted position
            for (long i = newIdx; i < _cellsUsed.size(); i++)
            {
                cell = _cellsUsed.at(i);
                this->_setIndexForCell(cell->getIdx()+1, cell);
            }
        }
        
        //insert a new cell
        cell = _dataSource->tableCellAtIndex(this, idx);
        this->_setIndexForCell(idx, cell);
        this->_addCellIfNecessary(cell);
        
        this->_updateCellPositions();
        this->_updateContentSize();
    }
    
    void TableView::removeCellAtIndex(ssize_t idx)
    {
        if (idx == CC_INVALID_INDEX)
        {
            return;
        }
        
        long uCountOfItems = _dataSource->numberOfCellsInTableView(this);
        if (0 == uCountOfItems || idx > uCountOfItems-1)
        {
            return;
        }
        
        ssize_t newIdx = 0;
        
        TableViewCell* cell = this->cellAtIndex(idx);
        if (!cell)
        {
            return;
        }
        
        newIdx = _cellsUsed.getIndex(cell);
        
        //remove first
        this->_moveCellOutOfSight(cell);
        
        _indices->erase(idx);
        this->_updateCellPositions();
        
        for (ssize_t i = _cellsUsed.size()-1; i > newIdx; i--)
        {
            cell = _cellsUsed.at(i);
            this->_setIndexForCell(cell->getIdx()-1, cell);
        }
    }
    
    TableViewCell *TableView::dequeueCell()
    {
        TableViewCell *cell;
        
        if (_cellsFreed.empty()) {
            cell = nullptr;
        } else {
            cell = _cellsFreed.at(0);
            cell->retain();
            _cellsFreed.erase(0);
            cell->autorelease();
        }
        return cell;
    }
    
    void TableView::_addCellIfNecessary(TableViewCell * cell)
    {
        if (cell->getParent() != this->getInnerContainer()) // reg check
        {
            this->getInnerContainer()->addChild(cell); // reg check
        }
        _cellsUsed.pushBack(cell);
        _indices->insert(cell->getIdx());
        _isUsedCellsDirty = true;
    }
    
    void TableView::_updateContentSize()
    {
        Size size = Size::ZERO;
        ssize_t cellsCount = _dataSource->numberOfCellsInTableView(this);
        
        if (cellsCount > 0)
        {
            float maxPosition = _vCellsPositions[cellsCount];
            
            switch (this->getDirection())
            {
                case Direction::HORIZONTAL:
                    //                size = Size(maxPosition, _viewSize.height); // reg
                    size = Size(maxPosition, _contentSize.height); // reg
                    break;
                default:
                    //                size = Size(_viewSize.width, maxPosition); // reg
                    size = Size(_contentSize.width, maxPosition); // reg
                    break;
            }
        }
        
        //    this->setContentSize(size);
        this->setInnerContainerSize(size);
        
        if (_oldDirection != _direction)
        {
            if (_direction == Direction::HORIZONTAL)
            {
                //			this->setContentOffset(Vec2(0,0)); // reg
                this->scrollChildren(Vec2(0, 0)); // reg
            }
            else
            {
                //			this->setContentOffset(Vec2(0,this->minContainerOffset().y)); // reg
                this->scrollChildren(Vec2(0, 0)); // reg
            }
            _oldDirection = _direction;
        }
        
    }
    
    Vec2 TableView::_offsetFromIndex(ssize_t index)
    {
        Vec2 offset = this->__offsetFromIndex(index);
        
        const Size cellSize = _dataSource->tableCellSizeForIndex(this, index);
        if (_vordering == VerticalFillOrder::TOP_DOWN)
        {
            offset.y = this->getInnerContainer()->getContentSize().height - offset.y - cellSize.height; // reg check
        }
        return offset;
    }
    
    Vec2 TableView::__offsetFromIndex(ssize_t index)
    {
        Vec2 offset;
        Size  cellSize;
        
        switch (this->getDirection())
        {
            case Direction::HORIZONTAL:
                offset.set(_vCellsPositions[index], 0.0f);
                break;
            default:
                offset.set(0.0f, _vCellsPositions[index]);
                break;
        }
        
        return offset;
    }
    
    long TableView::_indexFromOffset(Vec2 offset)
    {
        long index = 0;
        const long maxIdx = _dataSource->numberOfCellsInTableView(this) - 1;
        
        if (_vordering == VerticalFillOrder::TOP_DOWN)
        {
            offset.y = this->getInnerContainer()->getContentSize().height - offset.y; // reg check
        }
        index = this->__indexFromOffset(offset);
        if (index != -1)
        {
            index = MAX(0, index);
            if (index > maxIdx)
            {
                index = CC_INVALID_INDEX;
            }
        }
        
        return index;
    }
    
    long TableView::__indexFromOffset(Vec2 offset)
    {
        long low = 0;
        long high = _dataSource->numberOfCellsInTableView(this) - 1;
        float search;
        switch (this->getDirection())
        {
            case Direction::HORIZONTAL:
                search = offset.x;
                break;
            default:
                search = offset.y;
                break;
        }
        
        while (high >= low)
        {
            long index = low + (high - low) / 2;
            float cellStart = _vCellsPositions[index];
            float cellEnd = _vCellsPositions[index + 1];
            
            if (search >= cellStart && search <= cellEnd)
            {
                return index;
            }
            else if (search < cellStart)
            {
                high = index - 1;
            }
            else
            {
                low = index + 1;
            }
        }
        
        if (low <= 0) {
            return 0;
        }
        
        return -1;
    }
    
    void TableView::_moveCellOutOfSight(TableViewCell *cell)
    {
        if(_tableViewDelegate != nullptr) {
            _tableViewDelegate->tableCellWillRecycle(this, cell);
        }
        
        _cellsFreed.pushBack(cell);
        _cellsUsed.eraseObject(cell);
        _isUsedCellsDirty = true;
        
        _indices->erase(cell->getIdx());
        cell->reset();
        
        if (cell->getParent() == this->getInnerContainer()) // reg check
        {
            this->getInnerContainer()->removeChild(cell, true);; // reg check
        }
    }
    
    void TableView::_setIndexForCell(ssize_t index, TableViewCell *cell)
    {
        cell->setAnchorPoint(Vec2(0.0f, 0.0f));
        cell->setPosition(this->_offsetFromIndex(index));
        cell->setIdx(index);
    }
    
    void TableView::_updateCellPositions()
    {
        long cellsCount = _dataSource->numberOfCellsInTableView(this);
        _vCellsPositions.resize(cellsCount + 1, 0.0);
        
        if (cellsCount > 0)
        {
            float currentPos = 0;
            Size cellSize;
            for (int i=0; i < cellsCount; i++)
            {
                _vCellsPositions[i] = currentPos;
                cellSize = _dataSource->tableCellSizeForIndex(this, i);
                switch (this->getDirection())
                {
                    case Direction::HORIZONTAL:
                        currentPos += cellSize.width;
                        break;
                    default:
                        currentPos += cellSize.height;
                        break;
                }
            }
            _vCellsPositions[cellsCount] = currentPos;//1 extra value allows us to get right/bottom of the last cell
        }
        
    }
    
    // reg zhangzhenquan
    void TableView::moveInnerContainer(const Vec2& deltaMove, bool canStartBounceBack)
    {
        ScrollView::moveInnerContainer(deltaMove, canStartBounceBack);
        reloadLayout();
    }
    
    void TableView::moveToCell(unsigned int idx,unsigned int width) {
        
        float moveDistance = (idx-1)*width;
        //    if (m_pTableViewHeader) {
        //        moveDistance = moveDistance + m_pTableViewHeader->getSize().height;
        //    }
        float pos = this->getInnerContainer()->getContentSize().height + _contentSize.height;
        
        switch (this->getDirection())
        {
            case Direction::HORIZONTAL:
                
                _innerContainer->setPosition(Vec2(-moveDistance + pos,0));
                //            checkNeedBounce();
                reloadLayout();
                
                break;
            default:
                _innerContainer->setPosition(Vec2(0,pos + moveDistance));
                //            checkNeedBounce();
                reloadLayout();
                break;
        }
        
        
    }
    
    
    void TableView::reloadLayout()
    {
        long countOfItems = _dataSource->numberOfCellsInTableView(this);
        if (0 == countOfItems)
        {
            return;
        }
        
        if (_isUsedCellsDirty)
        {
            _isUsedCellsDirty = false;
            std::sort(_cellsUsed.begin(), _cellsUsed.end(), [](TableViewCell *a, TableViewCell *b) -> bool{
                return a->getIdx() < b->getIdx();
            });
        }
        
        //    if(_tableViewDelegate != nullptr) {
        //        _tableViewDelegate->scrollViewDidScroll(this); // reg warning
        //    }
        
        Vec2 contentOffset = this->getInnerContainer()->getPosition(); // reg
        
        ssize_t startIdx = 0, endIdx = 0, idx = 0, maxIdx = 0;
        //    Vec2 offset = this->getContentOffset() * -1; // reg
        
        Vec2 offset = contentOffset * -1; // reg Vec2运算符重载
        maxIdx = MAX(countOfItems-1, 0);
        
        if (_vordering == VerticalFillOrder::TOP_DOWN)
        {
            //        offset.y = offset.y + _viewSize.height/this->getContainer()->getScaleY(); // reg
            offset.y = offset.y + _contentSize.height/this->getInnerContainer()->getScaleY(); // reg
        }
        startIdx = this->_indexFromOffset(offset);
        if (startIdx == CC_INVALID_INDEX)
        {
            startIdx = countOfItems - 1;
        }
        
        if (_vordering == VerticalFillOrder::TOP_DOWN)
        {
            //        offset.y -= _viewSize.height/this->getContainer()->getScaleY(); // reg
            offset.y -= _contentSize.height/this->getInnerContainer()->getScaleY(); // reg
        }
        else
        {
            //        offset.y += _viewSize.height/this->getContainer()->getScaleY(); // reg
            offset.y += _contentSize.height/this->getInnerContainer()->getScaleY(); // reg
        }
        //    offset.x += _viewSize.width/this->getContainer()->getScaleX(); // reg
        offset.x += _contentSize.width/this->getInnerContainer()->getScaleX(); // reg
        
        endIdx   = this->_indexFromOffset(offset);
        if (endIdx == CC_INVALID_INDEX)
        {
            endIdx = countOfItems - 1;
        }
        
        #if 0 // For Testing.
            Ref* pObj;
            int i = 0;
            CCARRAY_FOREACH(_cellsUsed, pObj)
            {
                TableViewCell* pCell = static_cast<TableViewCell*>(pObj);
                log("cells Used index %d, value = %d", i, pCell->getIdx());
                i++;
            }
            log("---------------------------------------");
            i = 0;
            CCARRAY_FOREACH(_cellsFreed, pObj)
            {
                TableViewCell* pCell = static_cast<TableViewCell*>(pObj);
                log("cells freed index %d, value = %d", i, pCell->getIdx());
                i++;
            }
            log("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        #endif
        
        if (!_cellsUsed.empty())
        {
            auto cell = _cellsUsed.at(0);
            idx = cell->getIdx();
            
            while(idx < startIdx)
            {
                this->_moveCellOutOfSight(cell);
                if (!_cellsUsed.empty())
                {
                    cell = _cellsUsed.at(0);
                    idx = cell->getIdx();
                }
                else
                {
                    break;
                }
            }
        }
        if (!_cellsUsed.empty())
        {
            auto cell = _cellsUsed.back();
            idx = cell->getIdx();
            
            while(idx <= maxIdx && idx > endIdx)
            {
                this->_moveCellOutOfSight(cell);
                if (!_cellsUsed.empty())
                {
                    cell = _cellsUsed.back();
                    idx = cell->getIdx();
                }
                else
                {
                    break;
                }
            }
        }
        
        for (long i = startIdx; i <= endIdx; i++)
        {
            if (_indices->find(i) != _indices->end())
            {
                continue;
            }
            this->updateCellAtIndex(i);
        }
    }
    
    // reg warning
    void TableView::handlePressLogic(Touch *touch)
    {
        ScrollView::handlePressLogic(touch);
        
        Vec2 pointInView = this->convertToNodeSpace(touch->getLocation()); // reg warning
        
        Vec2 containerPoint = this->getInnerContainer()->getPosition();
        containerPoint = containerPoint * -1;
        Vec2 point = Vec2(containerPoint.x+pointInView.x, containerPoint.y+pointInView.y);
        m_pressPoint = touch->getLocation(); // reg warning
        long index = this->_indexFromOffset(point);
        if (index == CC_INVALID_INDEX)
        {
            _touchedCell = NULL;
        }
        else
        {
            _touchedCell  = this->cellAtIndex(index);
        }
        
        
        if (_touchedCell && _tableViewDelegate != NULL) {
            _tableViewDelegate->tableCellHighlight(this, _touchedCell);
            
        }
        
    }
    
    // reg warning
    void TableView::handleMoveLogic(Touch *touch)
    {
        ScrollView::handleMoveLogic(touch);
        if (_touchedCell && (fabs(m_pressPoint.x-touch->getLocation().x)>10 || fabs(m_pressPoint.y-touch->getLocation().y)>10)) // reg warning
        {
            
            if(_tableViewDelegate != NULL)
            {
                _tableViewDelegate->tableCellUnhighlight(this, _touchedCell);
            }
            
            _touchedCell = NULL;
        }
    }
    
    // reg warning
    void TableView::handleReleaseLogic(Touch *touch)
    {
        ScrollView::handleReleaseLogic(touch);
        
        if (_touchedCell){
            
            Rect bb = this->getClippingRect();
            
            
            bb.origin = this->getWorldPosition();
            
            
            Vec2 point = touch->getLocation(); // reg warning
            
            if (bb.containsPoint(point) && _tableViewDelegate != NULL)
            {
                _tableViewDelegate->tableCellUnhighlight(this, _touchedCell);
                _tableViewDelegate->tableCellTouched(this, _touchedCell);
            }
            
            _touchedCell = NULL;
        }
        m_pressPoint = Vec2(0,0);
    }
    
    
    
    
    // reg all
    void TableView::registerScriptHandler(int nFunID,int nScriptEventType)
    {
        this->unregisterScriptHandler(nScriptEventType);
        m_mapScriptHandler[nScriptEventType] = nFunID;
        
    }
    void TableView::unregisterScriptHandler(int nScriptEventType)
    {
        std::map<int,int>::iterator iter = m_mapScriptHandler.find(nScriptEventType);
        
        if (m_mapScriptHandler.end() != iter)
        {
            m_mapScriptHandler.erase(iter);
        }
    }
    int  TableView::getScriptHandler(int nScriptEventType)
    {
        std::map<int,int>::iterator iter = m_mapScriptHandler.find(nScriptEventType);
        
        if (m_mapScriptHandler.end() != iter)
            return iter->second;
        
        return 0;
    }
    
    void TableView::unregisterAllScriptHandler()
    {
        
        //    unregisterScriptHandler(ScriptHandlerMgr::HandlerType::UITABLECELL_TOUCHED);
        //    unregisterScriptHandler(ScriptHandlerMgr::HandlerType::UITABLECELL_HIGHLIGHT);
        //    unregisterScriptHandler(ScriptHandlerMgr::HandlerType::UITABLECELL_UNHIGHLIGHT);
        //    unregisterScriptHandler(ScriptHandlerMgr::HandlerType::UITABLECELL_WILL_RECYCLE);
        //    unregisterScriptHandler(ScriptHandlerMgr::HandlerType::UITABLECELL_SIZE_FOR_INDEX);
        //    unregisterScriptHandler(ScriptHandlerMgr::HandlerType::UITABLECELL_AT_INDEX);
        //    unregisterScriptHandler(ScriptHandlerMgr::HandlerType::UITABLEVIEW_NUMS_OF_CELLS);
    }
    
} // nameSpace ui Ended

NS_CC_END
