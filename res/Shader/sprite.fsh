varying vec4 v_fragmentColor;
varying vec2 v_texCoord;
uniform float time;
void main()
{
    if(time < 0.01 || time > 2.0)
    {
        gl_FragColor = texture2D(CC_Texture0, v_texCoord) * v_fragmentColor;
    }
    else
    {
        vec4 color = texture2D(CC_Texture0, v_texCoord) * v_fragmentColor;
        float srcA = 1.0 - (time * 0.2);
        float srcB = (time * 0.2);
        vec4 color1 = vec4(color.rgb * srcA + vec3(0.125, 0.031, 0.0) * srcB, color.a);
        if(color.a > 0.1)
            gl_FragColor =  color1;
        else
            gl_FragColor =  color;
    }
}
